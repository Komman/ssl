BUILD_DIR=BUILD_DIRd
LAST_GLM_VERSION=3.4
GLM_DIR=glfw-$LAST_GLM_VERSION

#Get dependencies
echo "=============================="
echo "=== INSTALLING DEPENDENCIES =="
echo "=============================="
echo ""
echo ">sudo apt-get install freeglut3-dev"
echo ">sudo apt-get install libglew-dev"
echo ">sudo apt-get install libglm-dev"
echo ""
sudo apt-get install freeglut3-dev
sudo apt-get install libglew-dev
sudo apt-get install libglm-dev

#Get GLM library
echo "=============================="
echo "===  INSTALLING GLM LIBRAY  =="
echo "=============================="
echo ""
echo ">mkdir $BUILD_DIR"
echo ">cd $BUILD_DIR"
echo ">wget https://github.com/glfw/glfw/releases/download/3.4/$GLM_DIR.zip"
echo ">unzip glfw-3.4.zip"
echo ">cd $GLM_DIR"
echo ">sudo apt-get install cmake xorg-dev libglu1-mesa-dev"
echo ">cmake -G \"Unix Makefiles\""
echo ">make"
echo ">sudo make install"
echo ""
mkdir $BUILD_DIR
cd $BUILD_DIR
wget https://github.com/glfw/glfw/releases/download/3.4/$GLM_DIR.zip
unzip glfw-3.4.zip
cd $GLM_DIR

#Build it
sudo apt-get install cmake xorg-dev libglu1-mesa-dev
cmake -G "Unix Makefiles"
make
sudo make install

#Delete build
echo "=============================="
echo "===      DELETING BUILD     =="
echo "=============================="
echo ""
echo "cd ../.."
echo "rm -r $BUILD_DIR"
cd ../..
rm -r $BUILD_DIR