#!/bin/bash

echo "MAKE SURE IT IS COMPILED WITH -pg OPTION"

DATA_FILE="profile_data.txt"
IMG_FILE="profile_image"

if [ "$1" != "" ]
then
	IMG_FILE="$1"
fi

IMG_FILE="$IMG_FILE.png"

rm -f gmon.out
rm -f $IMG_FILE
rm -f $DATA_FILE

echo "Making img file in $IMG_FILE"
echo "Waiting 1sec and start profile..."
sleep 1

rm -f $DATA_FILE
./run
gprof ./run gmon.out > $DATA_FILE
cat $DATA_FILE | gprof2dot | dot -Tpng -o $IMG_FILE
