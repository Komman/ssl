#!/bin/bash

shader_files=$(find -name "*generated_shaders*")
command="rm -r $shader_files"

if [ "$shader_files" == "" ]
then
	echo "No shader files to clean"
	exit
fi

echo "Cleaning: $shader_files" | sed 's/\ /\n/g'

$($command)
