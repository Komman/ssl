#ifndef _FACECHITECTURE_HPP_
#define _FACECHITECTURE_HPP_

#include <unordered_map>

#include <glm/glm.hpp>

#include "../utils/plan_graph.hpp"
#include "../utils/vectored_map.hpp"

#include "reliefed_plan_dual_graph.hpp"

class Facechitecture
{
public:
	using hashEdge = Graph::hashEdge;
	using Edge = Graph::Edge;
	using dualEdge = PlanDualGraph::dualEdge;

	struct verticalFace
	{
		// On the first face
		uint v1;
		uint v2;
		// On the second face
		uint v3;
		uint v4;
	};

public:
	// dual must be dual of g (must have same vertices in faces)
	Facechitecture(PlanGraph* g, ReliefedPlanDualGraph* dual, uint outer_face);

	// FACES
	std::vector<uint> faces() const;
	glm::vec3 face_position(uint face) const;
	std::vector<uint> adjacent_faces(uint face) const;
	// Returns the closest distance from the center to any vertices of the face 
	float face_radius(uint face);

	// VERTICES & EDGES LISTS
	const std::vector<glm::vec3>& vertices() const;
	std::vector<uint> vertices(uint face) const;
	std::vector<glm::vec3> vertices_pos(uint face) const;
	std::vector<Edge> edges(uint face) const;
	std::vector<uint> original_vertices() const;
	// O(_vertices.size())
	std::vector<uint> yaligned_vertices(uint original_vertex) const;

	// POSITIONING
	glm::vec3 edge_extdirN(const Edge& e);
	glm::vec3 original_vertex_position(uint face, uint vertex) const;
	glm::vec3 position(uint vertex) const;
	glm::vec3 operator[](uint vertex) const;
	std::vector<glm::vec3> position(const std::vector<uint>& vertices) const;
	std::vector<glm::vec3> operator[](const std::vector<uint>& vertices) const;

	// VERTICES & EDGES & FACES
	uint face(uint vertex) const;
	uint original_vertex(uint vertex) const;
	float height(uint face);
	void set_height(uint face, float h);
	void up_face(uint face, float h);
	void up_vertices(const std::vector<uint>& vertices, float h);
	uint vertex(uint face, uint original_vertex) const;

	// FACE ADJENCY
	std::vector<verticalFace> horizontal_faces(uint ext_face) const; 
	// The edge on which the adjency is, and it opposite face
	const std::unordered_map<Edge, uint, hashEdge>& face_adjency(uint face) const;
	bool adjacent(uint face1, uint face2) const;

	// GRAPH
	PlanGraph& vertex_graph();
	ReliefedPlanDualGraph& face_graph();

	// OCCUPED EDGES/FACES (FOR FACE BUILDING)
	const std::unordered_set<Edge, hashEdge>& occupied_edges() const;
	const std::unordered_map<uint, int>&      occupied_faces() const;
	void occupy_edge(const Edge& e);
	void occupy_face(uint face, int idoccupying);
	bool is_occupied(const Edge& e) const;
	int face_build_id(uint face) const;

protected:
	void check_vertex(uint vertex) const;

private:
	PlanGraph* _graph;
	ReliefedPlanDualGraph* _dual;

	std::vector<glm::vec3> _vertices;
	std::vector<std::pair<uint, uint>> _vertices_ref;
	std::unordered_map<Edge, uint, hashEdge> _index_from_face_orivertex;
	VectoredMap<glm::vec3> _faces_center;
	std::vector<dualEdge> _face_sharing_edges;
	VectoredMap<std::unordered_map<Edge, uint, hashEdge>> _onedge_face_adjency;
	std::unordered_set<Edge, hashEdge> _occupied_edges;
	std::unordered_map<uint, int> _occupied_faces;
};

#endif //_FACECHITECTURE_HPP_
