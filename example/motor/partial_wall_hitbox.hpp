#ifndef _PARTIAL_WALL_HITBOX_HPP_
#define _PARTIAL_WALL_HITBOX_HPP_

#include "shaped_hitbox_fusion.hpp"
#include "bevel_hitbox.hpp"

struct partialWallInfo
{
	partialEnlargementInfos<glm::vec3> pwall;
	glm::vec3 sized_direction;
};

class PartialWallHitbox : public ShapedHitboxFusion<partialWallInfo>
{
public:
	PartialWallHitbox(const partialWallInfo& shape);

private:
	void build();
};


#endif //_PARTIAL_WALL_HITBOX_HPP_
