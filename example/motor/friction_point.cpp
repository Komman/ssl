#include "friction_point.hpp"

using namespace glm;

FrictionPoint::FrictionPoint(const glm::vec3& position,
							 float friction_coeff)
	: PhysicalPoint(position),
	  _friction_coeff(friction_coeff)
{

}

FrictionPoint::FrictionPoint(const glm::vec3& position,
				  			 const glm::vec3& speed,
				  			 const glm::vec3& acceleration,
				  			 float friction_coeff)
	: PhysicalPoint(position, speed, acceleration),
	  _friction_coeff(friction_coeff)
{

}

void FrictionPoint::animate(float dt)
{
	vec3 old_acc = this->get_acceleration();
	this->set_acceleration(old_acc - this->get_speed()*_friction_coeff);
	
	this->PhysicalPoint::animate(dt);

	this->set_acceleration(old_acc);
}
