#ifndef _PACKED_PARTICLE_DISPATCHOR_HPP_
#define _PACKED_PARTICLE_DISPATCHOR_HPP_

#include "particle_dispatchor.hpp"

class PackedParticleDispatchor : public BasicParticleDispatchor
{
public:
	PackedParticleDispatchor(const std::string& name);

	void add_particle(const Particle& particle) override;
	void animate(float dt) override;

	unsigned int size() const override;

	// LBAC = Light on Bloom Affecion Coeff
	const ssl::ArrayBuffer<glm::vec4>& get_dep_LBAC()     const;
	const ssl::ArrayBuffer<glm::vec4>& get_colors()       const;
	const ssl::ArrayBuffer<glm::vec3>& get_start_speeds() const;
	const ssl::ArrayBuffer<glm::vec3>& get_start_accelerations() const;
	const ssl::ArrayBuffer<glm::vec4>& get_infos()        const;

private:
	void check_sizes() const;
	void update_buffers();
	unsigned int finished_count();

	bool is_finished(const glm::vec4& info);

private:
	ssl::ArrayBuffer<glm::vec4> _dep_LBAC;
	ssl::ArrayBuffer<glm::vec4> _colors;
	ssl::ArrayBuffer<glm::vec3> _start_speeds;
	ssl::ArrayBuffer<glm::vec3> _start_accelerations;
	/*
		_infos[i]
			.x: start time
			.y: last time
			.z: friction_coeff
			.w: size
	*/
	ssl::ArrayBuffer<glm::vec4> _infos;

	unsigned int _start_check_adding;

	ssl::Uniform<float> _time;
};

#endif //_PACKED_PARTICLE_DISPATCHOR_HPP_
