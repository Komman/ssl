#include "height_grid.hpp"

HeightGrid::HeightGrid(const glm::vec2& bottom, const glm::vec2& top, uint line_cases_amout)
	: SpaceGrid(bottom, top, line_cases_amout),
      _heights({0.0f})
{
	
}

int HeightGrid::place_height(float h)
{
	auto e = _indices.find(h);
	if(e != _indices.end())
	{
		return _heights[e->second] + ISTART;
	}
	else
	{
		int i = _heights.size();
		_heights.push_back(h);
		_indices[h] = i;

		return i + ISTART;
	}
}

void HeightGrid::set_point(const glm::vec2& coord, float h)
{

}

void HeightGrid::set_circle(const glm::vec2& center, float radius, float h)
{

}

void HeightGrid::set_thin_line(const glm::vec2& start, const glm::vec2& stop, float h)
{

}

void HeightGrid::set_empty_polygon(const std::vector<glm::vec2>& points, float h)
{

}

void HeightGrid::set_filled_polygon(const std::vector<glm::vec2>& points, const glm::vec2& fill_start, float h)
{

}

void HeightGrid::set_convex_polygon(const std::vector<glm::vec2>& points, float h)
{

}

void HeightGrid::fill(const glm::vec2& start, float h, bool (*is_border)(float))
{

}

void HeightGrid::fill(const glm::vec2& start, float h, const std::vector<float>& border_values)
{

}

void HeightGrid::fill(const glm::vec2& start, float h)
{

}

void HeightGrid::fill_force(const glm::vec2& start, float h)
{

}


bool HeightGrid::collision_area(const std::vector<glm::vec2>& area) const
{
	return true;
}

bool HeightGrid::collision_circle(const glm::vec2& center, float radius) const
{
	return true;
}

bool HeightGrid::collision_thin_line(const glm::vec2& start, const glm::vec2& stop) const
{
	return true;
}

bool HeightGrid::collision_empty_polygon(const std::vector<glm::vec2>& points) const
{
	return true;
}

bool HeightGrid::collision_filling(const glm::vec2& start, float h)
{
	return true;
}

bool HeightGrid::collision_filled_polygon(const std::vector<glm::vec2>& points, const glm::vec2& fill_start, float h)
{
	return true;
}

bool HeightGrid::collision_convex_polygon(const std::vector<glm::vec2>& points, float h)
{
	return true;
}



