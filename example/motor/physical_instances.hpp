#ifndef _PHYSICAL_INSTANCES_HPP_
#define _PHYSICAL_INSTANCES_HPP_

#include "stored_instances_buffers.hpp"
#include "basic_hitbox.hpp"
#include "hitbox_context.hpp"

template<uint FIRST_INSTANCE_BUFFER, typename... Types>
class PhysicalInstances : public StoredInstancesBuffers<FIRST_INSTANCE_BUFFER, Types...>
{
public:
	template<typename Type>
	struct storedDrawType
	{
		using type = ssl::draw_type;
	};

public:
	PhysicalInstances(HitboxContext& hbx_context, ssl::draw_type element_draw_type, typename storedDrawType<Types>::type ... buffers_draw_type);

	const std::vector<std::unique_ptr<BasicHitbox>>& get_hitboxes() const;

	// Returns the index of the hitbox added
	uint add_hitbox_only(std::unique_ptr<BasicHitbox>&& hbx);
	uint add_static_hitbox_only(std::unique_ptr<BasicHitbox>&& hbx);

protected:
	std::vector<std::unique_ptr<BasicHitbox>> _hitboxes;
	HitboxContext* _hbx_context;
};





template<uint FIRST_INSTANCE_BUFFER, typename... Types>
PhysicalInstances<FIRST_INSTANCE_BUFFER, Types...>::PhysicalInstances(HitboxContext& hbx_context, ssl::draw_type element_draw_type, typename storedDrawType<Types>::type ... buffers_draw_type)
	: StoredInstancesBuffers<FIRST_INSTANCE_BUFFER, Types...>(element_draw_type, buffers_draw_type...),
	  _hbx_context(&hbx_context)
{

}

template<uint FIRST_INSTANCE_BUFFER, typename... Types>
const std::vector<std::unique_ptr<BasicHitbox>>& PhysicalInstances<FIRST_INSTANCE_BUFFER, Types...>::get_hitboxes() const
{
	return _hitboxes;
}

template<uint FIRST_INSTANCE_BUFFER, typename... Types>
uint PhysicalInstances<FIRST_INSTANCE_BUFFER, Types...>::add_hitbox_only(std::unique_ptr<BasicHitbox>&& hbx)
{
	uint ret = _hitboxes.size();
	_hitboxes.push_back(std::move(hbx));
	_hbx_context->add_hitbox(_hitboxes[_hitboxes.size()-1].get());
	return ret;
}

template<uint FIRST_INSTANCE_BUFFER, typename... Types>
uint PhysicalInstances<FIRST_INSTANCE_BUFFER, Types...>::add_static_hitbox_only(std::unique_ptr<BasicHitbox>&& hbx)
{
	uint ret = _hitboxes.size();
	_hitboxes.push_back(std::move(hbx));
	_hbx_context->add_static_hitbox(_hitboxes[_hitboxes.size()-1].get());
	return ret;
}




#endif //_PHYSICAL_INSTANCES_HPP_
