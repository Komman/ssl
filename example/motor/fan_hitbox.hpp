#ifndef _FAN_HITBOX_HPP_
#define _FAN_HITBOX_HPP_

#include "shaped_hitbox_fusion.hpp"

struct fanInfo
{
	std::vector<glm::vec3> cycle;
	glm::vec3 origin;
};

class FanHitbox : public ShapedHitboxFusion<fanInfo>
{
public:
	// The shape is composed of prisms centered on the barycenter of the cycle
	FanHitbox(const fanInfo& shape);

private:
	void build();
};


#endif //_FAN_HITBOX_HPP_
