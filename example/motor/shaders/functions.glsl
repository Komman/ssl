
/*
    Locals variables
*/

float epsilon = 0.001f;
float PI = 3.1415926535;

vec3 bibolar_proj(in vec3 bibolar_center, in vec2 bibo_plan_coord, in vec3 looking_directionN)
{
	vec3 new_x = normalize(cross(looking_directionN, vec3(0,1,0)));
	vec3 new_y = normalize(cross(looking_directionN, new_x));

	new_x *= bibo_plan_coord.x;
	new_y *= bibo_plan_coord.y;

	return bibolar_center + new_x + new_y;
}

/*
	return.x : 0 if no intersectin, and 1 else
	return.y : distance from start
*/
vec2 line_plan_intersection(in vec3 line_start, in vec3 line_dirN, in vec3 plan_point, in vec3 plan_normalN)
{	
	float t = dot(plan_normalN, plan_point - line_start);
	float denom = dot(plan_normalN, line_dirN);

	if(abs(denom) < 0.001)
	{
		return vec2(0,0);
	}
	
	return vec2(1, t/denom);
}

float angle3D(in vec3 v1, in vec3 v2)
{
	return local::angle3D_N(normalize(v1), normalize(v2));
}

float angle3D_N(in vec3 v1N, in vec3 v2N)
{
	return acos(dot(v1N, v2N));
}

float atan2(in float y, in float x)
{
    bool s = (abs(x) > abs(y));
    return mix(local::PI/2.0 - atan(x,y), atan(y,x), s);
}

float modulinear(in float x, in float max_val)
{
	float mod = fract(x/(2.0f*max_val)) * 2.0f;
	return (mod <= 1.0) ? (mod) : (2.0f - mod);
}

vec3 modulinear_v3(in vec3 x, in vec3 max_val)
{
	vec3  mod  = fract(x/(2.0f*max_val)) * 2.0f;
	bvec3 low1 = lessThan(mod, vec3(1.0f));
	return (mod)*vec3(low1) + (vec3(2.0f) - mod)*vec3(not(low1));
}

float normalizedcos(in float x)
{
	return (cos(x) + 1.0f)/2.0f;
}
float normalizedsin(in float x)
{
	return (sin(x) + 1.0f)/2.0f;
}
vec3 normalizedcos3(in vec3 x)
{
	return (cos(x) + vec3(1.0f))/2.0f;
}
vec3 normalizedsin3(in vec3 x)
{
	return (sin(x) + vec3(1.0f))/2.0f;
}

float depth_from_proj(in vec4 proj_coord)
{
	return local::depth_from_projdepth(proj_coord.z/proj_coord.w);
}

float depth_from_projdepth(in float depth)
{
	return (gl_DepthRange.far - gl_DepthRange.near)*0.5f*depth + (gl_DepthRange.far + gl_DepthRange.near)*0.5f;
}

float depth_from_depth(in float depth01)
{
	return gl_DepthRange.near + (gl_DepthRange.far - gl_DepthRange.near)*depth01;
}

vec4 camera_transform(in vec3 vertext_position)
{

	return uniform::MVP*vec4(vertext_position,  1.0);
}

float camdist(in vec3 point)
{
	return length(uniform::camera_position-point);
}

float absolute_bloom_coeff(in vec3 position)
{
	float downscale_loss = 64.0;
	return (local::camdist(pos)+downscale_loss)/downscale_loss;
}

vec3 safe_normalize(in vec3 v, in vec3 safe)
{
	return (length(v)>local::epsilon) ? normalize(v) : safe;
}

vec3 safe_cross(in vec3 v1, in vec3 v2, in vec3 safe)
{
	vec3 cr = cross(v1, v2);
	return (length(cr) > local::epsilon) ? cr : safe;
}

vec3  native_normal(in vec3 position_variable)
{
	return local::native_normal_hard(position_variable);
}


vec3  native_normal_hard(in vec3 position_variable)
{
	return cross(dFdx(position_variable)*1000.0f, dFdy(position_variable)*1000.0f);
}

float old_normal_coeff(in vec3 position, in vec3 source_position)
{
	vec3 cam_to_frag = normalize(position - source_position);
    float nco = dot(cam_to_frag, normalize(local::native_normal(position)));
    
    return nco;
}


float normal_multiplior(in vec3 position, in vec3 normalN, in vec3 source_position)
{
    return clamp(dot(-normalN, normalize(position - source_position)), 0.0, 1.0);
}

void storm_illum(inout vec4 color, in vec3 position)
{
	if(uniform::storm_touch == vec3(0.0,0.0,0.0))
	{
		return;
	}

	float division_precision = 0.0001;

	float d = distance(position, uniform::storm_touch) + division_precision;
	float distance_coeff = clamp(uniform::storm_power/d, 0.0, 1.0f/division_precision);
	float normal_c = local::old_normal_coeff(position, uniform::storm_touch+vec3(0.0,50.0,0.0));
	float storm_normal_coeff = 0.2;

	float coeff = (normal_c*storm_normal_coeff + (1.0f-storm_normal_coeff))*distance_coeff;
	coeff = clamp(coeff, 1.0f, uniform::storm_power);

	color.xyz += vec3(sqrt(distance_coeff*division_precision));
	color *= coeff;
}

float bibolar_sphere_view_radius(in vec3 cam_coord, in vec3 sphere_center, in vec3 looking_directionN, in float sphere_radius)
{
	vec3 cam_to_center = sphere_center - cam_coord;
	float cc = length(cam_to_center);

	float tmpeq = cc/dot(cam_to_center, looking_directionN);

	return cc * sqrt(tmpeq*tmpeq - 1.0f) / sphere_radius;
}

vec3 proj_on_plan(in vec3 v, in vec3 plan_normalN)
{
	return v - dot(v, plan_normalN)*plan_normalN;
}

vec3 sphere_proj(in vec3 cam_coord, in vec3 looking_directionN, in vec3 sphere_center, in float sphere_radius)
{
	return local::sphere_proj_custom(cam_coord, looking_directionN, sphere_center, sphere_radius, 1.0f);
}

// surface = 1.0f  :first surface
// surface = -1.0f :behind
vec3 sphere_proj_custom(in vec3 cam_coord, in vec3 looking_directionN, in vec3 sphere_center, in float sphere_radius, in float surface)
{
	vec3 point_to_sphere = sphere_center - cam_coord;
	vec3 to_sphereN      = normalize(point_to_sphere);
	float D              = length(point_to_sphere);
	float cosalpha       = dot(looking_directionN, to_sphereN);


	if(length(point_to_sphere-looking_directionN*D) > sphere_radius)
	{
		return vec3(0.0);
	}
	else
	{
		float D_x_cosalpha = D*cosalpha;
		float y  = D_x_cosalpha - surface*sqrt(D_x_cosalpha*D_x_cosalpha + sphere_radius*sphere_radius - D*D);

		return cam_coord + looking_directionN*y;
	}
}

float cleanacos(in float x)
{
	return acos(clamp(x, -0.999999, 0.999999));
}

float total_angle2D(in vec2 v1N)
{
	return local::cleanacos(dot(v1N, vec2(1.0,0.0)))*sign(v1N.y);
}

vec3 rotate_redirect2D(in vec3 v,
					   in vec3 original_dirN,
					   in vec3 new_dirN)
{
	vec3 c = cross(original_dirN, new_dirN);

	if(length(c) <= local::epsilon)
	{
		return (dot(original_dirN, new_dirN) >= 0.0) ? vec3(v.x, v.y, v.z) : vec3(-v.x, v.y, -v.z);
	}

	c = normalize(c);
	vec3 t = normalize(cross(c, original_dirN));
	vec3 q = normalize(cross(c, new_dirN));

	return   dot(v, original_dirN)*new_dirN
		   + dot(v, c)*c
		   + dot(v, t)*q;
}
