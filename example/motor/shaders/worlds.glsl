#include "basic_shaders.glsl"

void holer_vertex(in vec3 in_pos, out vec3 useless_pos)
{
	gl_Position   = local::camera_transform(in_pos);
	// gl_Position.z = gl_Position.w;
	useless_pos   = in_pos;
}

void empty_fragment_identifor(in vec3 useless_pos, out vec4 color, out vec4 bloom_color)
{
	// color = vec4(1.0,0.0,0.0,1.0);
	// bloom_color = vec4(0.0,0.0,0.0,1.0);
	//Do nothing
}
void empty_fragment_holer(in vec3 useless_pos, out vec4 color, out vec4 bloom_color)
{
	bloom_color = vec4(0.0);
	gl_FragDepth = 1.0f;
	//Do nothing
}