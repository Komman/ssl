#include "textures.glsl"

int sum_kernel_coeffs_linear()
{
    int k = abs(uniform::bloom_kernel)+1;
    return k*k;
}
int sum_kernel_coeffs_squared()
{
    int k = abs(uniform::bloom_kernel);
    return (k*(k+1)*(2*k+1))/3 + (k+1)*(k+1); //[n(n+1)(2n+1)] / 6
}

float bloom_linear_coeff(in float kernel, in float i)
{
    return kernel + 1.0 - abs(i);
}
float bloom_square_coeff(in float kernel, in float i)
{
    float lin = local::bloom_linear_coeff(kernel, i);
    return lin*lin;
}


int sum_kernel_coeffs()
{
    if(uniform::bloom_kernel > 0)
    {
        return local::sum_kernel_coeffs_linear();
    }
    else
    {
        return local::sum_kernel_coeffs_squared();
    }
    
}
float bloom_coeff(in float kernel, in float i)
{
    if(uniform::bloom_kernel > 0)
    {
        return local::bloom_linear_coeff(kernel, i);
    }
    else
    {
        return local::bloom_square_coeff(kernel, i);
    }
}

float bloom_gaussian_weight[5] = {0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216};

void bloom_loop(in sampler2D original,
                in vec2  tex_coord,
                in vec2  axe, 
                in vec2  dep, 
                in float i, 
                in float kernel,
                in float total_coeffs,
                inout vec4 tmp_color)
{
    vec4 texvalue = texture(original, tex_coord + axe * dep*i);
    float spread_coeff = local::bloom_coeff(kernel, i)/total_coeffs;
    tmp_color += texvalue*spread_coeff;
}

vec4 bloom_pass(in sampler2D original, in vec2 tex_coord, in vec2 axe)
{
    int kernel = abs(uniform::bloom_kernel);
    float kernelf = float(kernel);
    int total_coeffs = local::sum_kernel_coeffs();
    vec2 dep = local::get_texture_pixel_size(uniform::bloom_downscaled_even);

    vec4 tmp_color = texture(original, tex_coord)*local::bloom_coeff(kernelf, 0.0)/float(total_coeffs); 

    for(int i=-kernel;i<0;i++)
    {
        local::bloom_loop(original, tex_coord, axe, dep, float(i), kernelf, float(total_coeffs), tmp_color);
    }
    for(int i=1;i<=kernel;i++)
    {
        local::bloom_loop(original, tex_coord, axe, dep, float(i), kernelf, float(total_coeffs), tmp_color);
    } 


    return tmp_color;
}

void bloom_downscale(in vec2 tex_coord, out vec4 color)
{
    vec2 corrected_tex_coord = local::get_tex_coord(tex_coord)/float(uniform::bloom_downsclae_divisor);
    if(uniform::bloom_compute_kernel <= 0) // no kernel
    {
        color = texture(uniform::bloom_downscaled_even, corrected_tex_coord)*uniform::bloom_color_mutiplier;
    }
    else
    {
        if(uniform::bloom_compute_kernel == 1) // kernel horizontal
        {
            color = local::bloom_pass(uniform::bloom_downscaled_even, corrected_tex_coord, vec2(1.0, 0.0))*uniform::bloom_color_mutiplier;
        }
        if(uniform::bloom_compute_kernel == 2) // kernel vertical
        {
            color = local::bloom_pass(uniform::bloom_downscaled_even, corrected_tex_coord, vec2(0.0, 1.0))*uniform::bloom_color_mutiplier;
        }
    }
}

