#include "textures.glsl"

float rand(vec2 co){
    return fract(sin(dot(co, vec2(12.9898, 78.233))) * 43758.5453);
}

void post_process(in vec2 frag_uv, out vec4 color)
{
    
    vec2 tex_coord = local::get_tex_coord(frag_uv);

    vec2 oui = vec2(1.0f)/vec2(1920,1080);

    color = texture(uniform::shared_frame_texture          , tex_coord);
    color+= texture(uniform::shared_bloom_processed_texture, tex_coord);
    color+= texture(uniform::shared_bloom_texture          , tex_coord);

    // vec4 blend_color = local::texture_multisample(uniform::shared_blending_texture, tex_coord);
    vec4 blend_color = texture(uniform::shared_blending_texture, tex_coord);
    color.xyz = mix(color.xyz, blend_color.xyz, blend_color.w);

    color += (length(frag_uv) < 0.005) ? vec4(0.1f) : vec4(0.0);
}
