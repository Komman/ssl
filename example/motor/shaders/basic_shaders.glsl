#include "functions.glsl"
#include "textures.glsl"

void vertex_only(in vec3 in_pos, out vec3 out_pos)
{
    gl_Position  = local::camera_transform(in_pos);
    out_pos = in_pos;
}

void simple_vertex(in  vec3 in_position_s, in vec4 in_color_s,
                   out vec3 out_position_s, out vec4 out_color_s)
{
    local::vertex_only(in_position_s, out_position_s);
    out_color_s    = in_color_s;
}  


void simple_vertex3(in  vec3 in_position_s, in vec3 in_color_s,
                    out vec3 out_position_s, out vec3 out_color_s)
{
    local::vertex_only(in_position_s, out_position_s);
    out_color_s    = in_color_s;
}  

void instance_vertex(in  vec3 in_position, in vec4 in_color, in::instance vec3 dep,
                     out vec3 out_position, out vec4 out_color)
{
    local::simple_vertex(in_position + dep, in_color, out_position, out_color);
} 

void instance_yrot_vertex(in  vec3 in_position, in vec4 in_color, in::instance vec3 dep, in::instance float angle
                          out vec3 out_position, out vec4 out_color)
{
    local::simple_vertex(in_position + dep, in_color, out_position, out_color);
}  

void white_fragment(in vec3 pos, out vec4 color)
{
    color = vec4(1.0,1.0,1.0,1.0);
}

void simple_fragment(in  vec3 frag_position, in vec3 frag_color,
                     out vec4 final_color) 
{
    final_color = vec4(frag_color, 1.0f);
    // local::many_vertices(frag_position, frag_color, final_color);
}

void many_vertices(in  vec3 frag_position, in vec4 frag_color,
                    out vec4 final_color) 
{
    final_color = vec4(clamp(frag_color.xyz, vec3(0.0) ,vec3(1.0)), 1.0);
}
