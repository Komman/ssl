#include "textures.glsl"

int digit_grid[11*3*5] = {
	1,1,1,
	1,0,1,
	1,0,1,
	1,0,1,
	1,1,1,

	0,0,1,
	0,0,1,
	0,0,1,
	0,0,1,
	0,0,1,

	1,1,1,
	0,0,1,
	1,1,1,
	1,0,0,
	1,1,1,

	1,1,1,
	0,0,1,
	1,1,1,
	0,0,1,
	1,1,1,

	1,0,1,
	1,0,1,
	1,1,1,
	0,0,1,
	0,0,1,

	1,1,1,
	1,0,0,
	1,1,1,
	0,0,1,
	1,1,1,

	1,1,1,
	1,0,0,
	1,1,1,
	1,0,1,
	1,1,1,

	1,1,1,
	0,0,1,
	0,0,1,
	0,0,1,
	0,0,1,

	1,1,1,
	1,0,1,
	1,1,1,
	1,0,1,
	1,1,1,

	1,1,1,
	1,0,1,
	1,1,1,
	0,0,1,
	1,1,1,

	0,0,0,
	0,0,0,
	0,0,0,
	0,0,0,
	0,1,0,
};

void number_displayor_vertex(in vec2 coord, out vec2 uv)
{
	vec2 computed_coord = uniform::FloatNumbersDisplayor_location;
	computed_coord += coord * uniform::FloatNumbersDisplayor_size;

	gl_Position = vec4(local::get_gl_coord(computed_coord), 0.0,1.0);

	uv = coord;
}

int number_coord_self_digit(int number, vec2 suv)
{
	if(any(greaterThan(suv, vec2(1.0)) || any(lessThan(suv, vec2(0.0)))))
	{
		return 0;
	}

	int x = int(floor(suv.x*3.0f));
	int y = int(floor((1.0f-suv.y)*5.0f));

	return local::digit_grid[number*15 + y*3 + x];
}

void float_displayor_fragment(in vec2 uv, out vec4 color)
{
	float margin = 0.2f;
	int tocolor = 0;
	int oversize = int(uniform::FloatNumbersDisplayor_digits_size);
	int dot_pos = uniform::FloatNumbersDisplayor_uniform_digit_amount.x;
	int cases_amount = dot_pos +  uniform::FloatNumbersDisplayor_uniform_digit_amount.y + 1;

	float modu = uv.x * float(cases_amount);
	int case_id = int(floor(modu));
	
	vec2 moduv = vec2(fract(modu), uv.y);
	moduv -= vec2(margin);
	moduv *= 1.0f/(1.0f - 2.0f*margin);

	if(case_id == dot_pos)
	{
		tocolor = local::number_coord_self_digit(10, moduv);
	}
	else
	{
		case_id = (case_id > dot_pos) ? (oversize/2 + case_id - dot_pos - 1) : case_id;

		int todraw = uniform::FloatNumbersDisplayor_digits[case_id];

		if(todraw == -1)
		{
			discard;
		}

		tocolor = local::number_coord_self_digit(todraw, moduv);
	}

	if(tocolor == 0)
	{
		discard;
	}
	color = vec4(uniform::FloatNumbersDisplayor_color, 1.0);
}
