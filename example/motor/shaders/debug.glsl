#include "basic_shaders.glsl"
#include "functions.glsl"

void sphere_vertex(in  vec3 pos,
				   in::instance vec4 center_U_radius, 
				   out vec4 fcenter_U_radius,
	 			   out vec3 fpos)
{
	fcenter_U_radius = center_U_radius;

	local::vertex_only(center_U_radius.xyz + pos*center_U_radius.w*2.0f, fpos);
}

void sphere_fragment(in vec4 fcenter_U_radius, in vec3 fpos, out vec4 color)
{
	vec3 cam_to_fragN = normalize(fpos - uniform::camera_position);
	vec3 real_pos = local::sphere_proj(uniform::camera_position,
									   cam_to_fragN,
									   fcenter_U_radius.xyz,
									   fcenter_U_radius.w);

	if(real_pos == vec3(0.0))
	{
		discard;
	}

	float freqgrid = 62.0;
	float rel_y = (real_pos.y-fcenter_U_radius.y)/fcenter_U_radius.w;

	float cosco  = cos(rel_y*freqgrid/2.0);

	if(cosco < 0.5)
	{

		real_pos = local::sphere_proj_custom(uniform::camera_position,
					   cam_to_fragN,
					   fcenter_U_radius.xyz,
					   fcenter_U_radius.w,
					   -1.0f);
		rel_y = (real_pos.y-fcenter_U_radius.y)/fcenter_U_radius.w;

		cosco = cos(rel_y*freqgrid);
	
		if(cosco < 0.5)
		{
			discard;
		}
	}

	gl_FragDepth = local::depth_from_proj(local::camera_transform(real_pos));
	
	color = vec4(1.0, cosco/8.0f, abs(1.0f+rel_y)/4.0f, 1.0);
}

void cagedelim_vertex(in vec3 rel_pos,
                      in vec3 color,
                      in::instance vec3 mincorner,
                      in::instance vec3 size,
                      out vec3 fcolor)
{
    gl_Position = local::camera_transform(mincorner + rel_pos*size);

    fcolor = color;
}

void cagedelim_fragment(in vec3 fcolor,
	                    out vec4 final_color)
{
    final_color = vec4(fcolor, 1);
}

void prism_fragment(in vec3 pos, out vec4 color)
{
	float coeff = 1.61;	
	if((int(pos.x*coeff)%2 + int(pos.y*coeff)%2 + int(pos.z*coeff)%2)%2 ==0)
	{
		discard;
	}
	vec3 cospos = cos(pos/3.0f);
	color = vec4(0.5f+dot(cospos, cospos)*0.2f,0,0,1);
}
