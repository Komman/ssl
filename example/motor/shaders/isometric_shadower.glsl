#include "basic_shaders.glsl"


vec2 isometric_texture_coordinate(
	in vec3 position,
	in vec3 source,
	in vec3 furthest,
	in vec2 plan_size)
{
	vec3 middle    = (furthest - source)/2.0f;
	vec3 dirlight  = furthest - source;
	vec3 dirlightN = normalize(dirlight);
	// X,Y on the plan
	vec3 dirxN = normalize(cross(dirlightN, vec3(0,1,0)));
	vec3 diryN = normalize(cross(dirxN, dirlightN));

	return vec2(
		0.5f+dot(dirxN, position - source)/plan_size.x,
		0.5f+dot(diryN, position - source)/plan_size.y
	);
}

float isoshadower_distance_general(
	in vec3 position,
	in vec3 firstpost,
	in vec3 source,
	in vec3 furthest)
{
	vec3 dirlightN  = normalize(furthest - source);

	float dfirst = dot(dirlightN, firstpost - source);
	float dshpos = dot(dirlightN, position - source);

	return dshpos - dfirst;
}

// vec3 isoshadower_firstpos(
// 	in sampler2D coord_texture,
// 	in vec2 texpos,
// 	in vec3 source,
// 	in vec3 furthest)
// {
// 	vec4 texvalueC = texture(coord_texture, texpos);
// 	vec4 texvalueN;
// 	{
// 		vec2  incoord   = local::tex_inpixel_coord(coord_texture, texpos);
// 		int   trinumber = local::subtriangle_number(incoord);
// 		ivec2 offset    = local::subtriangle_neigbour_offsets[trinumber];
// 		texvalueN       = texture(coord_texture, texpos + local::offset_to_depx(offset, coord_texture)/2.0f);
// 	}
// 	int triangleC = floatBitsToInt(texvalueC.w);
// 	int triangleN = floatBitsToInt(texvalueN.w);

// 	if(triangleC == -1 || triangleC == triangleN || local::isoshadower_distance_general(texvalueC.xyz, texvalueN.xyz, source, furthest) > 0.0f)
// 	{
// 		return texvalueC.xyz;
// 	}
// 	else
// 	{
// 		return texvalueN.xyz;
// 	}	
vec3 isoshadower_firstpos(
	in sampler2D coord_texture,
	in vec2 texpos,
	in vec3 source,
	in vec3 furthest)
{
	return texture(coord_texture, texpos).xyz;
}

void isoshadower_normalXYN(
	in vec3 position,
	in vec3 dirlightN,
	out vec3 normalXN,
	out vec3 normalYN)
{
	vec3 normal = local::native_normal(position);
	vec3 dirx = cross(normal);
	vec3 diry = cross(normal);
}

float isoshadower_distance(
	in vec3 position,
	in vec3 source,
	in vec3 furthest,
	in vec2 plan_size,
	in sampler2D coord_texture)
{
	vec2 texpos = local::isometric_texture_coordinate(position, source, furthest, plan_size);

	if(all(greaterThan(texpos, vec2(0)) && lessThan(texpos, vec2(1))))
	{
		vec3 firstpost = local::isoshadower_firstpos(coord_texture, texpos, source, furthest);

		return local::isoshadower_distance_general(position, firstpost, source, furthest);
	}

	return -1.0f;
}

// float isoshadower_distance2(
// 	in vec3 position,
// 	in vec3 source,
// 	in vec3 furthest,
// 	in vec2 plan_size,
// 	in sampler2D coord_texture)
// {
// 	vec2 texpos = local::isometric_texture_coordinate(position, source, furthest, plan_size);

// 	if(all(greaterThan(texpos, vec2(0)) && lessThan(texpos, vec2(1))))
// 	{
// 		vec3 dirlightN  = normalize(furthest - source);
// 		vec3 firstpost = vec3(0);
// 		// vec3 firstpost = texture(coord_texture, texpos).xyz;

// 		vec4 right, up, left, down;
// 		bool before;
// 		float amount = 0.0f;
// 		local::neighbourgs4(coord_texture, texpos, right, up, left, down);

// 		before = all(right.xzy == furthest);
// 		amount += before ? 1.0f : 0.0f;
// 		firstpost += 

// 		float dfirst = dot(dirlightN, firstpost - source);
// 		float dshpos = dot(dirlightN, position - source);

// 		return dshpos - dfirst;
// 	}

// 	return -1.0f;
// }


void isometric_vertex(in vec3 position, out vec3 fposition)
{
	vec3 middle    = (uniform::isoshadow_furthest + uniform::isoshadow_source)/2.0f;
	vec3 dirlight  = uniform::isoshadow_furthest - uniform::isoshadow_source;
	vec3 dirlightN = normalize(dirlight);
	// X,Y on the plan
	vec3 dirxN = normalize(cross(dirlightN, vec3(0,1,0)));
	vec3 diryN = normalize(cross(dirxN, dirlightN));

	gl_Position = vec4(
		dot(dirxN, position - uniform::isoshadow_source)/uniform::isoshadow_plansize.x*2.0f,
		dot(diryN, position - uniform::isoshadow_source)/uniform::isoshadow_plansize.y*2.0f,
		dot(dirlightN, position - middle)/length(dirlight),
		1.0
	);

	fposition = position;
}

/* 
	DEP
*/


void instanced_isometric_vertex(in vec3 position, in::instance vec3 dep, out vec3 fposition)
{
	local::isometric_vertex(position + dep, fposition);	
}

/* 
	DEP(3) & HEIGHT(1)

	ORIENTATION2D_N(2) & SIZEXZ(2)
*/

void instanced_isometric_planar_vertex(
	in vec3 rel_pos,
	in::instance vec4 start_height,
	in::instance vec4 orientation2DN_sizeXZ,
	out vec3 fposition)
{
	vec3 new_relpos = vec3(rel_pos.x*orientation2DN_sizeXZ.z, rel_pos.y*start_height.w, rel_pos.z*orientation2DN_sizeXZ.w);

    new_relpos = local::rotate_redirect2D(new_relpos, vec3(0,0,1), vec3(orientation2DN_sizeXZ.x, 0, orientation2DN_sizeXZ.y));
    new_relpos = start_height.xyz + new_relpos;

	local::isometric_vertex(new_relpos, fposition);	
}


/* 
	DEP(3) & HEIGHT(1)

	ORIENTATION_N(3) & LENGTH(1)
*/

void instanced_isometric_O3L1_vertex(
	in vec3 rel_pos,
	in::instance vec4 start_height,
	in::instance vec4 orientation3DN_length,
	out vec3 fposition)
{
	vec3 new_relpos = vec3(rel_pos.x, rel_pos.y*start_height.w, rel_pos.z*orientation3DN_length.w);
    float depth = new_relpos.z;

    new_relpos = local::rotate_redirect2D(new_relpos, vec3(0,0,1), orientation3DN_length.xyz);
    new_relpos = start_height.xyz + new_relpos;

    local::isometric_vertex(new_relpos, fposition);	
}

/* 
	DEP(3) & HEIGHT(1)

	SIZE(1)
*/

void instanced_isometric_tower_vertex(
	in vec3 rel_pos,
	in::instance vec4 center_height,
	in::instance float size,
	out vec3 fposition)
{
	vec3 pos = center_height.xyz
          + vec3(rel_pos.x, 0, rel_pos.z)*size
          + vec3(0, min(rel_pos.y, 1.0),0)*center_height.w
          + ((rel_pos.y >= 1.0f) ? vec3(0, rel_pos.y-1.0, 0)*size : vec3(0));

    local::isometric_vertex(pos, fposition);	
}

/* 
	DEP(3) & HEIGHT(1)

	ORIENTATION(3) & SIZE(1)
*/

void instanced_isometric_O3tower_vertex(
	in vec3 rel_pos,
	in::instance vec4 center_height,
	in::instance vec4 orientation3DN_size,
	out vec3 fposition)
{
	vec3 new_relpos = local::rotate_redirect2D(rel_pos, vec3(1,0,0), orientation3DN_size.xyz);
    vec3 pos = center_height.xyz
          + vec3(new_relpos.x, abs(new_relpos.y) ,new_relpos.z)*orientation3DN_size.w/2.0f
          + vec3(0, (new_relpos.y < -0.01f) ? abs(center_height.w) : 0,0);

    local::isometric_vertex(pos, fposition);	
}

void isometric_fragment(in vec3 fposition, out vec4 pos_id)
{
	vec3 dirlight  = uniform::isoshadow_furthest - uniform::isoshadow_source;
	vec3 dirlightN = normalize(dirlight);
	
	// vec3 normalN = normalize(local::native_normal(fposition));
	// float cosa = -dot(dirlightN, normalN);
	// float d = max(uniform::isoshadow_plansize.x/float(textureSize(uniform::isoshadow_texture,0).x),
	// 			  uniform::isoshadow_plansize.y/float(textureSize(uniform::isoshadow_texture,0).y));
	// float p;
	// if(abs(cosa) <= 0.0001)
	// {
	// 	p = 0;
	// }
	// else
	// {
	// 	p = d*sqrt(1.0f-cosa*cosa)/cosa;
	// }


	vec3 newpos = fposition;// + dirlightN*p;
	// gl_FragDepth = local::depth_from_depth((dot(dirlightN, newpos - uniform::isoshadow_source))/length(dirlight));

	int triangle_indentifor = uniform::isoshadow_triangle_indentifor.x;
	
	if(uniform::isoshadow_triangle_indentifor.y == 0)
	{
		triangle_indentifor += gl_PrimitiveID/uniform::isoshadow_triangle_indentifor.z;
	}
	else
	{
		// Keep one identifor
	}

	pos_id = vec4(newpos, intBitsToFloat(triangle_indentifor+1));
}
