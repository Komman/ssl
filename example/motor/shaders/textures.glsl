void frame_vert(in vec2 uv, out vec2 frag_uv)
{
	gl_Position = vec4(uv.x, uv.y, 0.0, 1.0);
	frag_uv = uv;
}

void texture_vertex(in vec2 uv, out vec2 frag_uv)
{
	gl_Position = vec4((uv.x-0.5)*2.0f, (uv.y-0.5)*2.0f, 0.0, 1.0);
	frag_uv = uv;
}

void text_vertex(in vec2 screenpos, in vec2 uv, out vec2 frag_uv)
{
	frag_uv = uv;
	gl_Position = vec4((screenpos.x-0.5f)*2.0f, (screenpos.y-0.5f)*2.0f, 0.0f, 1.0f);
}

vec3 colorfromcode[8] = {
	vec3(0),
	vec3(1,0,0),
	vec3(0,1,0),
	vec3(0.5,1,0.5),
	vec3(0,0,1),
	vec3(1,0,1),
	vec3(0.5,0.5,1),
	vec3(1)
};

void text_fragment(in vec2 frag_uv, out vec4 color)
{
	if(uniform::textbox_drawtype == 0)
	{
		vec2  font_size = textureSize(uniform::text_bmp, 0);
		float char_amount = font_size.x/font_size.y;
		float charcoord = frag_uv.x*float(uniform::textbox_size);
		float charx     = fract(charcoord);
		int   number = int(charcoord);
		uvec2 chartodraw = uniform::textbox[number];

		color = texture(uniform::text_bmp, vec2((float(chartodraw.x-32.0f)+charx)/char_amount, frag_uv.y), 0);
		color.w = 1.0f;

		if(color.xyz == vec3(1))
		{
			discard;
			// color.xyz = vec3(0);
			// color.w = 0.5f;
		}
		else
		{
			color.xyz = local::colorfromcode[chartodraw.y];
			color.w=1.0f;
		}	
	}
	else
	{
		color.xyz = vec3(0);
		color.w = 0.5f;
	}
	
}

/* 
	Returns the number on this figure:
	 _____
	|\ 0 /|  ^
	| \ / |  |
	|1 X 3|  | 1
	| / \ |  |
	|/_2_\|  V

	<----->
	   1

	Coord between 0 and 1 
*/
int subtriangle_SA_table[2][2] = {
	{0, 1},
	{3, 2}
};

vec4 bilinear_interpolate4(vec4 a, vec4 b, vec4 c, vec4 d, float s, float t)
{
	vec4 x = mix(a, b, t);
	vec4 y = mix(c, d, t);
	return mix(x, y, s);

}
float bilinear_interpolate1(float a, float b, float c, float d, float s, float t)
{
	float x = mix(a, b, t);
	float y = mix(c, d, t);
	return mix(x, y, s);

}

vec4 tetxure_bilinear_interpolate(in sampler2D tex, vec2 coord)
{
	// Get the texture size
    vec2 texSize = textureSize(tex, 0);
    
    // Convert texture coordinates to pixel space
    vec2 p = coord * texSize - 0.5;
    vec2 f = fract(p);
    
    // Sample the four surrounding texels
    vec4 tl = texture(tex, (p - vec2(0.5, 0.5)) / texSize);
    vec4 tr = texture(tex, (p + vec2(0.5, -0.5)) / texSize);
    vec4 bl = texture(tex, (p + vec2(-0.5, 0.5)) / texSize);
    vec4 br = texture(tex, (p + vec2(0.5, 0.5)) / texSize);
    
    // Bilinear interpolation
    return mix(mix(tl, tr, f.x), mix(bl, br, f.x), f.y);
}

int subtriangle_number(vec2 coord)
{
	bool S = (coord.x > coord.y);
	bool A = (coord.x + coord.y < 1);

	return local::subtriangle_SA_table[int(S)][int(A)];
}

// Center is 0, extrm is 1
float subtriangle_coeff(vec2 incoord, int trinumber)
{
	float xy = (trinumber%2 == 0) ? incoord.y : incoord.x;
	
	return abs(xy-0.5f)*2.0f;
}

// return value is between 0 and 1
vec2 inpixel_coord(in vec2 size, in vec2 coord)
{
	return fract(coord*size);
}

vec2 tex_inpixel_coord(in sampler2D stexture, in vec2 coord)
{
	return local::inpixel_coord(textureSize(stexture, 0), coord);
}

ivec2 subtriangle_neigbour_offsets[4] = {
	ivec2(0,1),
	ivec2(-1,0),
	ivec2(0,-1),
	ivec2(1,0)
};

ivec2 subtriangle_neigbour_offset(vec2 incoord)
{
	return local::subtriangle_neigbour_offsets[local::subtriangle_number(incoord)];
}

vec2 offset_to_dep(in ivec2 offset, in vec2 size)
{
	return vec2(offset)/size;
}

vec2 offset_to_depx(in ivec2 offset, in sampler2D stexture)
{
	return local::offset_to_dep(offset, textureSize(stexture, 0));
}

vec4 texture_subtriangle_neigbour_offset(in sampler2D stexture, in vec2 texture_coord)
{
	vec2 dec = vec2(local::subtriangle_neigbour_offset(local::tex_inpixel_coord(stexture, texture_coord)));
	dec = dec/vec2(textureSize(stexture, 0));

	return texture(stexture, texture_coord + dec);
}

void black_frag(in vec2 frag_uv, out vec4 color)
{
	color = vec4(0.0,0.0,0.0,1.0);
}

vec2 get_tex_coord(in vec2 frag_uv)
{
	return frag_uv/2.0f+1.0f/2.0f;
}

vec2 get_gl_coord(in vec2 frag_uv)
{
	return (frag_uv - vec2(0.5f))*2.0f;
}

vec2 get_texture_pixel_size(in sampler2D original)
{
	return vec2(1.0, 1.0)/vec2(textureSize(original, 0));
}
	
vec4 kernel3x3(in sampler2D tex, in vec2 tex_coord, float center_coeff)
{
	vec4 ret = texture(tex, tex_coord)*center_coeff
			 + textureOffset(tex, tex_coord, ivec2(-1 ,-1))*(1.0f - center_coeff)/8.0f
			 + textureOffset(tex, tex_coord, ivec2(-1 , 0))*(1.0f - center_coeff)/8.0f
			 + textureOffset(tex, tex_coord, ivec2(-1 , 1))*(1.0f - center_coeff)/8.0f
			 + textureOffset(tex, tex_coord, ivec2( 0 ,-1))*(1.0f - center_coeff)/8.0f
			 + textureOffset(tex, tex_coord, ivec2( 0 , 1))*(1.0f - center_coeff)/8.0f
			 + textureOffset(tex, tex_coord, ivec2( 1 ,-1))*(1.0f - center_coeff)/8.0f
			 + textureOffset(tex, tex_coord, ivec2( 1 , 0))*(1.0f - center_coeff)/8.0f
			 + textureOffset(tex, tex_coord, ivec2( 1 , 1))*(1.0f - center_coeff)/8.0f;

	return ret;
}

void neighbourgs4(in sampler2D tex, in vec2 tex_coord,
				  out vec4 right,
				  out vec4 up,
				  out vec4 left,
				  out vec4 down)
{
	right = textureOffset(tex, tex_coord, ivec2(+1,+0));
	up	= textureOffset(tex, tex_coord, ivec2(+0,+1));
	left  = textureOffset(tex, tex_coord, ivec2(-1,+0));
	down  = textureOffset(tex, tex_coord, ivec2(+0,-1));
}

vec4 texture_multisample(sampler2DMS texMS, vec2 coord)
{
	int tex_samples = uniform::shared_MS_samples_amont;
	vec4 color = vec4(0.0);

	ivec2 icoord = ivec2(coord * textureSize(texMS));

	for (int i = 0; i < tex_samples; i++)
	{
		color += texelFetch(texMS, icoord, i);
	}

	color /= float(tex_samples);

	return color;
}

void texture_fragment(in vec2 uvcoord, out vec4 color)
{
	if(any(greaterThan(uvcoord, vec2(1))) || any(lessThan(uvcoord, vec2(0))))
	{
		discard;
	}

	color = abs(texture(uniform::bound_texture, uvcoord))/200.0f;
}
