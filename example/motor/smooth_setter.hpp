#ifndef _SMOOTH_SETTER_HPP_
#define _SMOOTH_SETTER_HPP_

#include <glm/glm.hpp>
#include "../utils/geom.hpp"

/*
	Returns a value that dynamically changes
	in order to tends towards a value that
	can dynamically change

	For now, linear
*/
template<typename Type>
class SmoothSetter
{
public:
	/*
		speed: distance that will be reached in 1s
	*/
	SmoothSetter(Type initial_value, float speed);

	// returns if the value has changed or not
	bool animate(float dt);
	bool objective_reached() const;

	Type get_value() const;
	Type get_objective_value() const;
	void set_objective_value(Type value);
	void set_value_force(Type value);

	void set_speed(float speed);

private:
	Type  _objective_value;
	Type  _current_value;
	float _speed;
};








template<typename Type>
SmoothSetter<Type>::SmoothSetter(Type initial_value, float speed)
	: _objective_value(initial_value), _current_value(initial_value), _speed(speed)
{

}

template<typename Type>
Type SmoothSetter<Type>::get_value() const
{
	return _current_value;
}

template<typename Type>
Type SmoothSetter<Type>::get_objective_value() const
{
	return _objective_value;
}

template<typename Type>
bool SmoothSetter<Type>::objective_reached() const
{
	return (_objective_value == _current_value);
}


template<typename Type>
void SmoothSetter<Type>::set_objective_value(Type value)
{
	_objective_value = value;
}

template<typename Type>
void SmoothSetter<Type>::set_value_force(Type value)
{
	_current_value = value;
}

template<typename Type>

bool SmoothSetter<Type>::animate(float dt)
{
	float d    = dt*_speed;
	Type  diff = (_objective_value-_current_value);

	if(diff == Type(0.0f))
	{
		return false;
	}

	if(glm::length(diff) <= d)
	{
		_current_value = _objective_value;
	}
	else
	{
		Type s = glm::normalize(diff);
		
		_current_value += s*d;
	}

	return true;
}

template<typename Type>
void SmoothSetter<Type>::set_speed(float speed)
{
	_speed = speed;
}

#endif //_SMOOTH_SETTER_HPP_
