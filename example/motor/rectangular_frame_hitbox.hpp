#ifndef _RECTANGULAR_FRAME_HITBOX_HPP_
#define _RECTANGULAR_FRAME_HITBOX_HPP_

#include "shaped_hitbox_fusion.hpp"
#include "paver_hitbox.hpp"

struct rectangularFrameInfo
{
	glm::vec3 center;
	glm::vec3 radius_x;
	glm::vec3 height_dir; // Sized
	glm::vec3 radius_z;
	glm::vec2 thicknesses_coeffs;
};

class RectangularFrameHitbox : public ShapedHitboxFusion<rectangularFrameInfo>
{
public:
	RectangularFrameHitbox(const rectangularFrameInfo& shape);

private:
	void build();
};

#endif //_RECTANGULAR_FRAME_HITBOX_HPP_
