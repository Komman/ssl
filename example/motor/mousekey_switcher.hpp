#ifndef _MOUSEKEY_SWITCHER_HPP_
#define _MOUSEKEY_SWITCHER_HPP_

#include "basic_mousekey_target.hpp"

class MousekeySwitcher
{
public:
	MousekeySwitcher();

	void animate(float dt);
	void target_switch(BasicMousekeyTarget* target);

	const BasicMousekeyTarget* get_target() const;
	BasicMousekeyTarget* get_target();
	const BasicMousekeyTarget* const* get_switch_ptr() const;

private:
	BasicMousekeyTarget* _target;
};

#endif //_MOUSEKEY_SWITCHER_HPP_
