#ifndef _TRITUBE_HPP_
#define _TRITUBE_HPP_

#include "../../ssl/ssl.hpp"

using namespace ssl;

class Tritube : public ssl::ElementDrawableObject
{
public:

	Tritube(const glm::vec3& position, 
			const glm::vec3& start_normal,
			float start_size);

	void born(const glm::vec3& position, 
			  const glm::vec3& start_normal,
			  float start_size);

	void grow(const glm::vec3& direction, float size);

	glm::vec3 get_step_position(uint position);

	const glm::vec3& extremity();
	const glm::vec3& start();

	virtual const std::vector<const BasicBuffer*>& get_buffers()  const;
	virtual const ElementBuffer&                   get_elements() const;

protected:

	const ArrayBuffer<glm::vec3>& get_vertexs();

private:

	glm::vec3 _start;
	glm::vec3 _extremity;

	ArrayBuffer<glm::vec3> _vertexs;
	ElementBuffer          _indices;

	const std::vector<const BasicBuffer*> _buffers_cache;

};

#endif //_TRITUBE_HPP_
