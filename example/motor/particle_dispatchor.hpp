#ifndef _PARTICLE_DISPATCHOR_HPP_
#define _PARTICLE_DISPATCHOR_HPP_

#include "../../ssl/ssl.hpp"

#include "particle.hpp"

class BasicParticleDispatchor
{
public:
	BasicParticleDispatchor(){}

	virtual void add_particle(const Particle& particle) =0;
	virtual void animate(float dt) =0;

	virtual unsigned int size() const =0;

	static float get_particle_time_percentage(const Particle& particle);
};

class ParticleDispatchor : public BasicParticleDispatchor
{
public:
	ParticleDispatchor();

	void add_particle(const Particle& particle) override;
	void animate(float dt) override;

	const ssl::ArrayBuffer<glm::vec4>& get_dep_LBAC() const;
	const ssl::ArrayBuffer<glm::vec4>& get_colors() const;
	const ssl::ArrayBuffer<glm::vec2>& get_infos() const;

	unsigned int size() const override;

	void explode(const glm::vec3& center, float amplitude, float distance_factor);

private:
	void check_sizes() const;	
	void fill_buffers();

	static glm::vec2 get_infos_vector(const Particle& particle);
	static glm::vec2 get_infos_vector(float time_percentage, float size);

private:
	ssl::ArrayBuffer<glm::vec4> _dep_LBAC;
	ssl::ArrayBuffer<glm::vec4> _colors;
	/* 
	_infos[i]:
		.x: time percentage	
		.y: size	
	*/
	ssl::ArrayBuffer<glm::vec2> _infos;

	std::vector<Particle> _particles;
};

#endif //_PARTICLE_DISPATCHOR_HPP_
