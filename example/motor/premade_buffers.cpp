#include "premade_buffers.hpp"


namespace premade_buffers
{
	static AlignedCube* _orthonormal_cube = NULL;

	void init()
	{
		if(_orthonormal_cube != NULL)
		{
			ssl::err("premade_buffers::init() : already initialized");
		}

		_orthonormal_cube = new AlignedCube(glm::vec3(0.0), 1.0f);
	}

	void free()
	{
		if(_orthonormal_cube == NULL)
		{
			ssl::err("premade_buffers::free() : not initialized");
		}

		delete _orthonormal_cube;
	}

	// dimension 1x1x1 on coord (0,0,0)
	AlignedCube& orthonormal_cube()
	{
		return *_orthonormal_cube;
	}
};
