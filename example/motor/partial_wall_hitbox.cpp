#include "partial_wall_hitbox.hpp"


#include "bevel_hitbox.hpp"

#include "../utils/geom.hpp"
#include "../utils/mutils.hpp"

using namespace glm;
using namespace std;

PartialWallHitbox::PartialWallHitbox(const partialWallInfo& shape)
	: ShapedHitboxFusion(shape)
{
	this->build();
}

void PartialWallHitbox::build()
{
	const auto& wall  = this->get_shape();
	const auto& pwall = wall.pwall;

	bool positive = geom::point_in_polygon3D(
		pwall.points[pwall.base.cycle[0]],
		pwall.compute_enlarged()
	);

	if(pwall.enlargement.faces.size() == 0)
	{
		std::vector<triIndex> triangles;

		if(positive)
		{
			triangles = geom::rubban_triangulation(
				pwall.points,
				pwall.enlarged.cycle,
				pwall.base.cycle
			);
		}
		else
		{
			triangles = geom::rubban_triangulation(
				pwall.points,
				pwall.base.cycle,
				pwall.enlarged.cycle
			);
		} 

		for(const triIndex& tri : triangles)
		{
			this->fusion_hitbox<BevelHitbox>(bevelShape{
				.triangle_base   = {
					pwall.points[tri.i1],
					pwall.points[tri.i2],
					pwall.points[tri.i3]
			   	},
				.sized_direction = wall.sized_direction
			});
		}
	}
	else
	{
		std::vector<std::vector<glm::vec3>> faces = pwall.compute_faces();
		
		for(uint f=0; f<faces.size(); f++)
		{
			std::vector<triIndex> triangles = geom::cycle_triangulation(
				faces[f]
			);

			for(const triIndex& tri : triangles)
			{
				this->fusion_hitbox<BevelHitbox>(bevelShape{
					.triangle_base   = {
						faces[f][tri.i1],
						faces[f][tri.i2],
						faces[f][tri.i3]
				   	},
					.sized_direction = wall.sized_direction
				});
			}
		}
	}
}