#ifndef _GAME_UTILS_HPP_
#define _GAME_UTILS_HPP_

#include "../utils/mutils.hpp"
#include "../../ssl/ssl.hpp"



namespace utils
{
	template<typename T>
	void swapop(ssl::ArrayBuffer<T>& v, unsigned int index);

	glm::vec3 random_vec3(const glm::vec3& ranges_min = glm::vec3(0.0f),
						  const glm::vec3& ranges_max = glm::vec3(1.0f));

	glm::vec2 random_vec2(const glm::vec2& ranges_min = glm::vec2(0.0f),
						  const glm::vec2& ranges_max = glm::vec2(1.0f));

	// Not perfectly uniform probability law
	glm::vec3 random_directionN();
	glm::vec3 rotate_redirect(const glm::vec3& v, const glm::vec3& original_dirN, const glm::vec3& new_dirN);
	std::vector<glm::vec2> extract_xy(const std::vector<glm::vec3>& vs);
};




namespace utils
{
	template<typename T>
	void swapop(ssl::ArrayBuffer<T>& v, unsigned int index)
	{
		#ifdef SSL_DEBUG
		if(index >= v.size())
		{
			ssl::err("swapop: try to pop index " + std::to_string(index) + " while size is " + std::to_string(v.size()));
		}
		#endif

		if(index < v.size()-1)
		{
			v.change_subvalue(index, v[v.size()-1]);
		}

		v.pop_value();
	}
};

#endif //_GAME_UTILS_HPP_
