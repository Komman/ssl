#ifndef _HANOI_HITBOX_HPP_
#define _HANOI_HITBOX_HPP_

#include "hitbox_fusion.hpp"

struct hanoiTower
{
	std::vector<glm::vec2> thick_heights;
	glm::vec3 bot_center;
	spaceBase base;
};

class HanoiHitbox : public HitboxFusion
{
public:
	// The shape is composed of bevels centered on the barycenter of the cycle
	HanoiHitbox(const hanoiTower& shape);
	// Among the same vertices repartition as building::hanoi
	HanoiHitbox(const std::vector<glm::vec3>& hanoied_vertices);

};

#endif //_HANOI_HITBOX_HPP_
