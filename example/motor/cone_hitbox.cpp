#include "cone_hitbox.hpp"

#include "primary_hitbox.hpp"

ConeHitbox::ConeHitbox(const coneInfo& shape, uint side_faces)
	: ShapedHitboxFusion(shape),
	  _side_faces(side_faces)
{
	if(glm::length(glm::cross(shape.to_top, shape.base_orientationN)) <= 0.001)
	{
		mot::err("ConeHitbox::ConeHitbox(): cone base is colinar to is bot->top vector");
	}

	this->build();
}

void ConeHitbox::build()
{
	const auto& cone = this->get_shape();

	glm::vec3 rot_axisN = glm::cross(cone.base_orientationN, cone.to_top);
	rot_axisN = glm::normalize(glm::cross(cone.base_orientationN, rot_axisN));

	std::vector<glm::vec3> base_points = geom::circle_points(
		glm::vec3(0),
		cone.base_orientationN,
		rot_axisN,
		cone.radius,
		_side_faces
	);

	for(uint i=0; i<_side_faces; i++)
	{
		this->fusion_hitbox<PrismHitbox>(prismInfo{.points={
			cone.bottom,
			cone.bottom + base_points[i],
			cone.bottom + base_points[(i+1)%_side_faces],
			cone.bottom + cone.to_top
	   }});
	}
		
}
