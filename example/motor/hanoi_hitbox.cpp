#include "hanoi_hitbox.hpp"

// #include "../../ssl/src/utils.hpp"
#include "../utils/geom.hpp"

#include "building.hpp"
#include "paver_hitbox.hpp"

using namespace std;
using namespace glm;

HanoiHitbox::HanoiHitbox(const hanoiTower& shape)
	: HitboxFusion()
{
	const auto& xy   = shape.thick_heights;
	const auto& base = shape.base;

	for(int i=0; i<(int)(xy.size())-1; i++)
	{			
		if(xy[i+0].y == xy[i+1].y)
			continue;

		float x1 = xy[i+0].x/2.0f; 
		float x2 = xy[i+1].x/2.0f; 

		this->fusion_hitbox<PaverHitbox>(exactPaverInfo{
			.square_base_up   = {
				shape.bot_center + base.diry*xy[i].y + base.dirx*x1 + base.dirz*x1,
				shape.bot_center + base.diry*xy[i].y - base.dirx*x1 + base.dirz*x1,
				shape.bot_center + base.diry*xy[i].y - base.dirx*x1 - base.dirz*x1,
				shape.bot_center + base.diry*xy[i].y + base.dirx*x1 - base.dirz*x1,
			},
			.square_base_down = {
				shape.bot_center + base.diry*xy[i+1].y + base.dirx*x2 + base.dirz*x2,
				shape.bot_center + base.diry*xy[i+1].y - base.dirx*x2 + base.dirz*x2,
				shape.bot_center + base.diry*xy[i+1].y - base.dirx*x2 - base.dirz*x2,
				shape.bot_center + base.diry*xy[i+1].y + base.dirx*x2 - base.dirz*x2,
			}	
		});
	}
}

HanoiHitbox::HanoiHitbox(const std::vector<glm::vec3>& hanoied_vertices)
	: HitboxFusion()
{
	if(hanoied_vertices.size()%4 != 0)
	{
		mot::err("HanoiHitbox::HanoiHitbox(): hanoied_vertices are not vertices of a hanoi tower");
	}

	for(int i=0; i<(int)(hanoied_vertices.size()/4)-1; i++)
	{		
		int j = i+1;

		this->fusion_hitbox<PaverHitbox>(exactPaverInfo{
			.square_base_up   = {
				hanoied_vertices[4*i+0],
				hanoied_vertices[4*i+1],
				hanoied_vertices[4*i+2],
				hanoied_vertices[4*i+3],
			},
			.square_base_down = {
				hanoied_vertices[4*j+0],
				hanoied_vertices[4*j+1],
				hanoied_vertices[4*j+2],
				hanoied_vertices[4*j+3],
			}	
		});

	}
}
