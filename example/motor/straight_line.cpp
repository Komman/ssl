#include "straight_line.hpp"

#include "motor_debug.hpp"

using namespace std;
using namespace glm;

StraightLine::StraightLine(const glm::vec2& point1, const glm::vec2& point2)
{
	if(point1.x == point2.x)
	{
		_a_inf = true;
		_a = point1.x;
		_b = point1.x;
	}
	else
	{
		_a_inf = false;

		vec2 A;
		vec2 B;

		if(point1.x < point2.x)
		{
			A = point1;
			B = point2;
		}
		else
		{
			B = point1;
			A = point2;
		}

		_a = (point2.y - point1.y)/(point2.x - point1.x);
		_b = A.y - _a * A.x;
	}
}

StraightLine::StraightLine(singleAxis axis, float value)
	: StraightLine(vec2(value,value), (axis == X_AXIS) ? vec2(value + 1.0f, value) : vec2(value, value + 1.0f))
{

}

glm::vec2 StraightLine::intersection(const StraightLine& l)
{
	if(this->is_parrallel(l))
	{
		mot::err("StraightLine::intersection(): called while lines are parrallel");
	}

	if(l._a_inf || _a_inf)
	{
		if(l._a_inf)
		{
			return vec2(l._b, _a * l._b + _b);
		}
		else
		{
			return vec2(_b, l._a * _b + l._b);
		}
	}
	else
	{
		vec2 ret;
		ret.x = (_b - l._b) / (l._a - _a);
		ret.y = _a * ret.x + _b;

		return ret;
	}
}

bool StraightLine::is_parrallel(const StraightLine& l)
{
	
	if(l._a_inf || _a_inf)
	{
		if(l._a_inf && _a_inf) 
		{
			return true;	
		}
		else
		{
			return false;
		}
	}
	else
	{
		return (l._a == _a);
	}
}

