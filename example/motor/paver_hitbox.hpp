#ifndef _PAVER_HITBOX_HPP_
#define _PAVER_HITBOX_HPP_

#include "bevel_hitbox.hpp"
#include "../utils/geom.hpp"

class PaverHitbox : public HitboxFusion
{
public:
	PaverHitbox(const paverInfo& paver);
	PaverHitbox(const ycentredPaverInfo& paver);
	PaverHitbox(const exactPaverInfo& paver);
	PaverHitbox(const transPaverInfo& paver);
	virtual ~PaverHitbox() {}
};

#endif //_PAVER_HITBOX_HPP_
