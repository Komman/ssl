
#ifndef _PRIMARY_HITBOX_HPP_
#define _PRIMARY_HITBOX_HPP_

#include "../utils/geom.hpp"
#include "basic_hitbox.hpp"
#include "collisions.hpp"

template<hitbox_primary::Shape SHAPE_TYPE>
class PrimaryHitbox : public BasicHitbox
{
public:
	PrimaryHitbox(const shapeInfo<SHAPE_TYPE>& shape);
	PrimaryHitbox(const glm::vec3& translation, const shapeInfo<SHAPE_TYPE>& shape);
	PrimaryHitbox(const hbxTransformation& transformation, const shapeInfo<SHAPE_TYPE>& shape);

	void reset_shape(const shapeInfo<SHAPE_TYPE>& shape);

	const shapeInfo<SHAPE_TYPE>& get_relative_shape() const;
	const shapeInfo<SHAPE_TYPE>& get_absolute_shape() const;
	
	// No need to call this functions, this class handles every absolute changes
	void invalidate_absolute() const = delete;

	storedHitbox copy() const override;	
	hitbox_primary::Shape get_primary_type() const override;
	
protected:
	spaceBox compute_shape_relative_space_box() const override;
	
	bool in_shape_collision(const BasicHitbox&  hbx) const override;
	bool in_shape_collision(const SphereHitbox& hbx) const override;
	bool in_shape_collision(const PrismHitbox&  hbx) const override;

	collisionImpact shape_impact(const BasicHitbox&  hitbox, const glm::vec3& self_directionN, float distance) const override;
	collisionImpact shape_impact(const SphereHitbox& hitbox, const glm::vec3& self_directionN, float distance) const override;
	collisionImpact shape_impact(const PrismHitbox&  hitbox, const glm::vec3& self_directionN, float distance) const override;

	void shape_invalidate_absolute() const override;

protected:

private:
	shapeInfo<SHAPE_TYPE> _shape;
	mutable datedStruct<shapeInfo<SHAPE_TYPE>> _abs_shape;
};



template<hitbox_primary::Shape SHAPE_TYPE>
PrimaryHitbox<SHAPE_TYPE>::PrimaryHitbox(const shapeInfo<SHAPE_TYPE>& shape)
	: PrimaryHitbox(glm::vec3(0), shape)
{

}

template<hitbox_primary::Shape SHAPE_TYPE>
PrimaryHitbox<SHAPE_TYPE>::PrimaryHitbox(const glm::vec3& translation, const shapeInfo<SHAPE_TYPE>& shape)
	: BasicHitbox(translation),
	  _shape(shape),
	  _abs_shape(shape)
{

}

template<hitbox_primary::Shape SHAPE_TYPE>
PrimaryHitbox<SHAPE_TYPE>::PrimaryHitbox(const hbxTransformation& transformation, const shapeInfo<SHAPE_TYPE>& shape)
	: BasicHitbox(transformation),
	  _shape(shape),
	  _abs_shape(shape)
{

}

template<hitbox_primary::Shape SHAPE_TYPE>
const shapeInfo<SHAPE_TYPE>& PrimaryHitbox<SHAPE_TYPE>::get_relative_shape() const
{
	return _shape;
}

template<hitbox_primary::Shape SHAPE_TYPE>
const shapeInfo<SHAPE_TYPE>& PrimaryHitbox<SHAPE_TYPE>::get_absolute_shape() const
{
	if(!(_abs_shape.updated))
	{
		_abs_shape = _shape;
		collisions::transform<SHAPE_TYPE>(_abs_shape, this->abs_transformation());
		_abs_shape.updated = true;
	}

	return _abs_shape;
}

template<hitbox_primary::Shape SHAPE_TYPE>
spaceBox PrimaryHitbox<SHAPE_TYPE>::compute_shape_relative_space_box() const
{
	return collisions::minmax_coord<SHAPE_TYPE>(_shape);
}
	
template<hitbox_primary::Shape SHAPE_TYPE>
bool PrimaryHitbox<SHAPE_TYPE>::in_shape_collision(const BasicHitbox&  hbx) const
{
	return hbx.in_collision(*this);
}

template<hitbox_primary::Shape SHAPE_TYPE>
bool PrimaryHitbox<SHAPE_TYPE>::in_shape_collision(const SphereHitbox& hbx) const
{
	return collisions::primary_collision<SHAPE_TYPE, hitbox_primary::SPHERE>(this->get_absolute_shape(), hbx.get_absolute_shape());
}

template<hitbox_primary::Shape SHAPE_TYPE>
bool PrimaryHitbox<SHAPE_TYPE>::in_shape_collision(const PrismHitbox&  hbx) const
{
	return collisions::primary_collision<SHAPE_TYPE, hitbox_primary::PRISM>(this->get_absolute_shape(), hbx.get_absolute_shape());
}

template<hitbox_primary::Shape SHAPE_TYPE>
collisionImpact PrimaryHitbox<SHAPE_TYPE>::shape_impact(const BasicHitbox&  hitbox, const glm::vec3& self_directionN, float distance) const
{
	return geom::opposite_impact(hitbox.impact(*this, -self_directionN, distance));
}

template<hitbox_primary::Shape SHAPE_TYPE>
collisionImpact PrimaryHitbox<SHAPE_TYPE>::shape_impact(const SphereHitbox& hitbox, const glm::vec3& self_directionN, float distance) const
{
	return collisions::primary_impact<SHAPE_TYPE, hitbox_primary::SPHERE>(this->get_absolute_shape(), self_directionN, distance, hitbox.get_absolute_shape());
}

template<hitbox_primary::Shape SHAPE_TYPE>
collisionImpact PrimaryHitbox<SHAPE_TYPE>::shape_impact(const PrismHitbox&  hitbox, const glm::vec3& self_directionN, float distance) const
{
	return collisions::primary_impact<SHAPE_TYPE, hitbox_primary::PRISM>(this->get_absolute_shape(), self_directionN, distance, hitbox.get_absolute_shape());
}

template<hitbox_primary::Shape SHAPE_TYPE>
void PrimaryHitbox<SHAPE_TYPE>::reset_shape(const shapeInfo<SHAPE_TYPE>& shape)
{
	_shape = shape;
	_abs_shape.updated = false;;

	this->relative_spacebox_invalidate();
}

template<hitbox_primary::Shape SHAPE_TYPE>
void PrimaryHitbox<SHAPE_TYPE>::shape_invalidate_absolute() const
{
	_abs_shape.updated = false;
}

template<hitbox_primary::Shape SHAPE_TYPE>
BasicHitbox::storedHitbox PrimaryHitbox<SHAPE_TYPE>::copy() const
{
	return std::make_unique<PrimaryHitbox<SHAPE_TYPE>>(this->transformation(), _shape);
}

template<hitbox_primary::Shape SHAPE_TYPE>
hitbox_primary::Shape PrimaryHitbox<SHAPE_TYPE>::get_primary_type() const
{
	return SHAPE_TYPE;
}


#endif //_PRIMARY_HITBOX_HPP_
