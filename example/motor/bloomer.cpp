#include "bloomer.hpp"

using namespace std;
using namespace glm;

static inline const ivec2& adapted_window_size()
{
	static ivec2 _return = window::size()/(int)(32)*(int)(32);
	return _return;
}

Bloomer::Bloomer(UniformTexture2D<glm::vec4>& bloom_in_texture,
				 const std::string& prename,
				 const std::string& final_bloom_texture_name,
				 const bloomParameter& parameters)
	: 	_gen("motor/shaders/bloom.glsl", {{"processed_texture", final_bloom_texture_name},
										  {"bloom_downscaled_even", prename + "downscaled_even"},
										  {"bloom_downscaled_odd", prename + "downscaled_odd"},
										  {"bloom_color_mutiplier", prename + "color_mutiplier"},
										  {"bloom_downsclae_divisor", prename + "downsclae_divisor"},
										  {"bloom_kernel", prename + "kernel"},
										  {"bloom_little_processed_texture", prename + "little_processed_texture"},
										  {"bloom_compute_kernel", prename + "compute_kernel"}}),
		_kernel(prename + "kernel", parameters.big_bloom_kernel),
		_downsclae_divisor(prename + "downsclae_divisor", 0),	
		_compute_kernel(prename + "compute_kernel", 1),
		_color_mutiplier(prename + "color_mutiplier", 1.0f),
		_texture(bloom_in_texture),
		_downscaled_even_texture(prename + "downscaled_even", adapted_window_size()/2, GL_LINEAR, GL_CLAMP_TO_BORDER),
		_downscaled_odd_texture(prename  + "downscaled_odd" , adapted_window_size()/4, GL_LINEAR, GL_CLAMP_TO_BORDER),
		_little_processed_texture(prename  + "little_processed_texture"   , adapted_window_size()/8, GL_LINEAR, GL_CLAMP_TO_BORDER),
		_processed_texture(final_bloom_texture_name   , adapted_window_size()/8, GL_LINEAR, GL_CLAMP_TO_BORDER),
		_downscaler(_gen, "frame_vert", "bloom_downscale"),
		_cleaner(_gen, "frame_vert", "black_frag"),
		_downscale_even(&_downscaled_even_texture),
		_downscale_odd(&_downscaled_odd_texture),
		_texture_framebuffer(&_texture),
		_little_processed_fb(&_little_processed_texture),
		_processed_fb(&_processed_texture),
		_downscaled_tetxure_binding(_downscaler.get_texture_binding(prename + "downscaled_even")),
		_big_kernel(parameters.big_bloom_kernel),
		_little_kernel(parameters.little_bloom_kernel),
		_downscale_count(0),
		_little_coeff(parameters.little_bloom_proportion),
		_enable(true)
{
	if((glm::ivec2)_texture.size() != window::size() ||
	   _texture.get_minmag_filter() != GL_LINEAR ||
	   _texture.get_wrap() != GL_CLAMP_TO_BORDER)
	{
		err("Wrong texture parameters given for the Bloomer::Bloomer()");
	}

	_downscaler.depth(false);
	_downscaler.blending(false);
	_downscaler.set_blend_equation(GL_FUNC_ADD);
	_downscaler.set_blend_function(GL_ONE, GL_ONE);

	_little_processed_fb.set_clear_color(vec4(BLACK, 1.0f));
	_processed_fb.set_clear_color(vec4(BLACK, 1.0f));
	_texture_framebuffer.set_clear_color(vec4(BLACK, 1.0f));
	_downscale_even.set_clear_color(vec4(BLACK, 1.0f));
	_downscale_odd.set_clear_color(vec4(BLACK, 1.0f));
	this->clean_textures();
	_texture_framebuffer.clear();
	_little_processed_fb.clear();
	_processed_fb.clear();

	_gen.free();
}

void Bloomer::bloom_enable()
{
	_enable = true;
}

void Bloomer::bloom_disable()
{
	_enable = false;
}



const UniformTexture2D<vec4>& Bloomer::bloom_texture() const
{
	return _texture;
}


const UniformTexture2D<vec4>& Bloomer::processed_tetxure() const
{
	return _processed_texture;
}


void Bloomer::change_little_coeff(float proportion)
{
	_little_coeff = proportion;	
}
void Bloomer::change_little_kernel(int new_kernel)
{
	_little_kernel = new_kernel;	
}
void Bloomer::change_big_kernel(int new_kernel)
{
	_big_kernel = new_kernel;	
}

void Bloomer::clean_textures()
{
	_downscale_even.clear();
	_downscale_odd.clear();
}

float Bloomer::little_coeff()
{
	return _little_coeff; 
}

int Bloomer::little_kernel()
{
	return _little_kernel; 
}

int Bloomer::big_kernel()
{
	return _big_kernel; 
}


void Bloomer::init_bloom_pass()
{
	this->clean_textures();

	_little_processed_fb.clear();
	_processed_fb.clear();

	_downscale_count = 0;
	_color_mutiplier = 1.0f;
}

void Bloomer::downscale_pass(const UniformTexture2D<vec4>& src_texture,
						 	 const FrameBuffer<glm::vec4>& dst_framebuffer)
{

	_compute_kernel = 0;
	_downscaler.custom_view_port(uvec2(0,0), adapted_window_size()/(2<<_downscale_count));
	_downsclae_divisor = (1<<_downscale_count)/(adapted_window_size().x/src_texture.size().x);

	_downscaler.change_texture_binding(_downscaled_tetxure_binding, src_texture);
	_downscaler.draw_in_framebuffer(dst_framebuffer, buffers::screenfill());

	_downscale_count++;
}

void Bloomer::blur_pass(const UniformTexture2D<vec4>& src_texture,
						const FrameBuffer<glm::vec4>& dst_framebuffer,
						bool vertical)
{	
	_compute_kernel = (vertical ? 2 : 1);
	
	if(&dst_framebuffer != &_processed_fb)
	{
		_downscaler.custom_view_port(uvec2(0,0), adapted_window_size()/(1<<_downscale_count));
	}
	else
	{
		_downscaler.disable_custom_view_port();
	}
	
	_downsclae_divisor = (1<<_downscale_count)/(adapted_window_size().x/src_texture.size().x);

	_downscaler.change_texture_binding(_downscaled_tetxure_binding, src_texture);
	_downscaler.draw_in_framebuffer(dst_framebuffer, buffers::screenfill());
}

void Bloomer::end_bloom_pass()
{
	_texture_framebuffer.clear();
}

void Bloomer::bloom()
{
	if(_enable)
	{
		this->init_bloom_pass();

		this->downscale_pass(_texture, _downscale_even);

		this->downscale_pass(_downscaled_even_texture, _downscale_odd);
		_downscale_even.clear();

		this->downscale_pass(_downscaled_odd_texture, _downscale_even);
		_downscale_odd.clear();

		_kernel = -_little_kernel;
		this->blur_pass(_downscaled_even_texture, _little_processed_fb, false);

		this->downscale_pass(_downscaled_even_texture, _downscale_odd);
		_cleaner.custom_view_port(uvec2(0,0), adapted_window_size()/16);
		_cleaner.draw_in_framebuffer(_downscale_even, buffers::screenfill());

		this->downscale_pass(_downscaled_odd_texture, _downscale_even);
		_cleaner.custom_view_port(uvec2(0,0), adapted_window_size()/16);
		_cleaner.draw_in_framebuffer(_downscale_odd, buffers::screenfill());

		_kernel = _big_kernel;
		this->blur_pass(_downscaled_even_texture, _downscale_odd, false);
		_color_mutiplier = 1.0f- _little_coeff;
		this->blur_pass(_downscaled_odd_texture, _processed_fb, true);
		
		//TOOPT: disable blending and add big and little bloom colors
		// at the last post processing step
		_downscaler.blending(true);
		_color_mutiplier = _little_coeff;
		_downscale_count=3;
		_kernel = -_little_kernel;
		this->blur_pass(_little_processed_texture, _processed_fb, true);
		_downscaler.blending(false);


		this->end_bloom_pass();	
	}
	else
	{
		_processed_fb.clear();
	}
	
}

