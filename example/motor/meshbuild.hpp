#ifndef _MESHBUILD_HPP_
#define _MESHBUILD_HPP_

#include "../../ssl/src/array_buffer.hpp"
#include "../../ssl/src/element_buffer.hpp"

#include "../utils/geom.hpp"

struct partialEnlargementIndexing
{
	// base.edge represent if the edge is taken in the enlargement
	partialCycle<uint> base;
	// enlarged.edge represent if the edge is taken in the enlargement
	partialCycle<uint> enlarged;
	std::vector<uint> outer_added;
	partialEnlargement<uint> enlargement;
};

namespace meshbuild
{
	partialEnlargementInfos<glm::vec3> partial_enlargement_infos(
		ssl::ArrayBuffer<glm::vec3>& vertices,
		const partialEnlargementIndexing& enlargement
	);
	partialEnlargement<glm::vec3> partial_enlargement(
		ssl::ArrayBuffer<glm::vec3>& vertices,
		const partialEnlargementIndexing& enlargement
	);
	partialCycle<glm::vec3> partial_cycle(
		ssl::ArrayBuffer<glm::vec3>& vertices,
		const partialCycle<uint>& cycle
	);
	partialEnlargement<uint> place_conspace_enlargement(
		ssl::ArrayBuffer<glm::vec3>& vertices,
		const partialCycle<uint>& cycle,
	    float distance,
	    float curveprecision_distance
	);
	// Returns min and max indices of vertices added
	std::pair<uint, uint> extrude_cycle(
		ssl::ArrayBuffer<glm::vec3>& vertices,
    	ssl::ElementBuffer& elements,
		const std::vector<uint>& cycle,
    	const glm::vec3& sized_direction,
    	bool flip_face
    );
	
	partialCycle<uint> extrude_cycle(
		ssl::ArrayBuffer<glm::vec3>& vertices,
    	ssl::ElementBuffer& elements,
		const partialCycle<uint>& cycle,
    	const glm::vec3& sized_direction,
    	bool flip_face
    );
	
	partialEnlargement<uint> extrude_cycle(
		ssl::ArrayBuffer<glm::vec3>& vertices,
    	ssl::ElementBuffer& elements,
		const partialEnlargement<uint>& cycle,
    	const glm::vec3& sized_direction,
    	bool flip_face
    );
	// Can be optimized
	std::pair<uint, uint> extrude_cycle(
		ssl::ArrayBuffer<glm::vec3>& vertices,
    	ssl::ElementBuffer& elements,
		const std::pair<uint, uint>& cycle,
    	const glm::vec3& sized_direction,
    	bool flip_face
    );

	// extrusion must have the same size than cycle
	std::pair<uint, uint> extrude_cycle(
		ssl::ArrayBuffer<glm::vec3>& vertices,
    	ssl::ElementBuffer& elements,
		const std::vector<uint>& cycle,
    	const std::vector<glm::vec3>& extrusion,
    	bool flip_face
    );
	// Can be optimized
	std::pair<uint, uint> extrude_cycle(
		ssl::ArrayBuffer<glm::vec3>& vertices,
    	ssl::ElementBuffer& elements,
		const std::pair<uint, uint>& cycle,
    	const std::vector<glm::vec3>& extrusion,
    	bool flip_face
    );
	// Returns min and max indices of vertices added
	// Cycle must be in trigonometric order
	std::pair<uint, uint> bissectrice_enlarge_cycle(
		ssl::ArrayBuffer<glm::vec3>& vertices,
    	ssl::ElementBuffer& elements,
		const std::vector<uint>& cycle,
    	float distance,
    	const glm::vec3& translation,
    	bool flip_face
    );
	// Can be optimized
	std::pair<uint, uint> bissectrice_enlarge_cycle(
		ssl::ArrayBuffer<glm::vec3>& vertices,
    	ssl::ElementBuffer& elements,
		const std::pair<uint, uint>& cycle,
    	float distance,
    	const glm::vec3& translation,
    	bool flip_face
    );
	// Returns min and max indices of vertices added
	// Cycle must be in trigonometric order
	std::pair<uint, uint> conspace_enlarge_cycle(
		ssl::ArrayBuffer<glm::vec3>& vertices,
    	ssl::ElementBuffer& elements,
		const std::vector<uint>& cycle,
    	float distance,
    	const glm::vec3& translation,
    	bool flip_face,
    	float curveprecision_distance,
    	bool fill = true
    );
    // return partial cycle enlargement but fill as if it was not
	partialEnlargementIndexing conspace_enlarge_cycle_ignore_partial(
		ssl::ArrayBuffer<glm::vec3>& vertices,
    	ssl::ElementBuffer& elements,
		const partialCycle<uint>& cycle,
    	float distance,
    	bool flip_face,
    	float curveprecision_distance,
    	bool fill = true
    );
	// Can be optimized
	std::pair<uint, uint> conspace_enlarge_cycle(
		ssl::ArrayBuffer<glm::vec3>& vertices,
    	ssl::ElementBuffer& elements,
		const std::pair<uint, uint>& cycle,
    	float distance,
    	const glm::vec3& translation,
    	bool flip_face,
    	float curveprecision_distance,
    	bool fill = true
    );

	// Cycle must be convex in order to have a nice face
	uint fan_cycle(ssl::ArrayBuffer<glm::vec3>& vertices,
		ssl::ElementBuffer& elements,
		const std::vector<uint>& cycle,
		const glm::vec3& fan_center,
		bool flip_face
	);
	// Can be optimized
	uint fan_cycle(ssl::ArrayBuffer<glm::vec3>& vertices,
		ssl::ElementBuffer& elements,
		const std::pair<uint, uint>& cycle,
		const glm::vec3& fan_center,
		bool flip_face
	);


	void fill_cycles(
		const ssl::ArrayBuffer<glm::vec3>& vertices,
		ssl::ElementBuffer& elements,
	    const partialEnlargement<uint>& cycle,
	    bool flip_face
	);
	void fill_cycle(
		const ssl::ArrayBuffer<glm::vec3>& vertices,
		ssl::ElementBuffer& elements,
	    const std::vector<uint>& cycle,
	    bool flip_face
	);
	void fill_cycle(
		const ssl::ArrayBuffer<glm::vec3>& vertices,
		ssl::ElementBuffer& elements,
	    const std::pair<uint, uint>& cycle,
	    bool flip_face
	);
	// outcycle and incycle must be in same trigo order on their plan
	void fill_rubban(
		const ssl::ArrayBuffer<glm::vec3>& vertices,
		ssl::ElementBuffer& elements,
	    const std::vector<uint>& outcycle,
	    const std::vector<uint>& incycle,
	    bool flip_face
	 );
	void fill_rubban(
		const ssl::ArrayBuffer<glm::vec3>& vertices,
		ssl::ElementBuffer& elements,
	    const std::pair<uint, uint>& outcycle,
	    const std::pair<uint, uint>& incycle,
	    bool flip_face
	);

	std::pair<uint, uint> relative_pointdirected_extruding(ssl::ArrayBuffer<glm::vec3>& vertices,
		ssl::ElementBuffer& elements,
		const std::vector<uint>& cycle,
		const glm::vec3& point,
		float coeff,
		bool flip_face
	);
	std::pair<uint, uint> relative_pointdirected_extruding(ssl::ArrayBuffer<glm::vec3>& vertices,
		ssl::ElementBuffer& elements,
		const std::pair<uint, uint>& cycle,
		const glm::vec3& point,
		float coeff,
		bool flip_face
	);
	std::pair<uint, uint> pointdirected_extruding(ssl::ArrayBuffer<glm::vec3>& vertices,
		ssl::ElementBuffer& elements,
		const std::vector<uint>& cycle,
		const glm::vec3& point,
		float d,
		const glm::vec3& translation,
		bool flip_face,
		bool fill = true
	);
	std::pair<uint, uint> pointdirected_extruding(ssl::ArrayBuffer<glm::vec3>& vertices,
		ssl::ElementBuffer& elements,
		const std::pair<uint, uint>& cycle,
		const glm::vec3& point,
		float d,
		const glm::vec3& translation,
		bool flip_face,
		bool fill = true
	);

	/*
		V1, V2, V3 and V4 are the outline vertices

		V4 = 7------------6 = V3
		     |            | 
		     |  2------3  | 
		     |  |      |  | 
		     |  |      |  | 
		     |  |      |  | 
		V1 = 0--1      4--5 = V2
	*/

	std::array<uint, 8> doored_fill(
		ssl::ArrayBuffer<glm::vec3>& vertices,
		ssl::ElementBuffer& elements,
		const std::array<uint, 4>& outline,
		float door_center_coeffx,
		float height,
		float width,
		bool flip_face
	);


	void translate(
		ssl::ArrayBuffer<glm::vec3>& vertices,
		const std::vector<uint>& indices,
		const glm::vec3& translation
	);
	std::pair<uint,uint> copytranslate(ssl::ArrayBuffer<glm::vec3>& vertices,
		const std::vector<uint>& indices,
		const glm::vec3& translation
	);
	std::pair<uint,uint> copytranslate(ssl::ArrayBuffer<glm::vec3>& vertices,
		const std::pair<uint,uint>& indices,
		const glm::vec3& translation
	);
};

#endif //_MESHBUILD_HPP_
