#ifndef _TIMED_MESHED_RAY_LIGHT_HPP_
#define _TIMED_MESHED_RAY_LIGHT_HPP_

#include "meshed_ray_luminor.hpp"

#include "linear_valuator.hpp"

class TimedStaticMeshedRayLight : public StoredMeshedRayLight
{
public:
	TimedStaticMeshedRayLight(const std::vector<glm::vec3>& vertices,
							  const glm::vec3& color,
						      const std::vector<uint>& indices,
							  float duration);
	TimedStaticMeshedRayLight(const std::vector<glm::vec3>& vertices,
							  const std::vector<glm::vec3>& colors,
						      const std::vector<uint>& indices,
							  float duration);

	const std::vector<rayVertex>& get_vertices() const;
	const std::vector<uint>&      get_elements() const;

	void animate(float dt);
	bool has_expired() const;

protected:
	virtual const LinearValuator<float>& get_intensity_evolution() const =0;

private:
	const std::vector<glm::vec3> _original_colors;

	std::vector<rayVertex> _vertices;
	std::vector<uint>      _elements;

	float _time_left;
};

#endif //_TIMED_MESHED_RAY_LIGHT_HPP_
