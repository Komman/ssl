#ifndef _CAGE_DELIM_HPP_
#define _CAGE_DELIM_HPP_

#include "stored_instances_buffers.hpp"
#include "angular_symmetry.hpp"
#include "basic_hitbox.hpp"


class CageDelim : public StoredInstancesBuffers<2,
												glm::vec3,
												glm::vec3,
												glm::vec3,
												glm::vec3>
{
public:
	CageDelim(float thickness, const glm::vec3& color);
	virtual ~CageDelim() {}

	void add_cage(const spaceBox& box);
	void clear();

private:
	enum bufferRole {RELATIVE_POS, COLORS, MINCORNER, SIZE};

	void build_base();
	uint place_vertex(AngularSymmetry<glm::vec3>& asym, const glm::vec3& pos, const glm::vec3& color);
	uint upsym(uint index);

private:
	float _thickness;
	glm::vec3 _color;
};

#endif //_CAGE_DELIM_HPP_
