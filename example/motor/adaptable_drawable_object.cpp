#include "adaptable_drawable_object.hpp"

using namespace std;
using namespace glm;

/*
	AdaptableDrawableObject
*/

AdaptableDrawableObject::AdaptableDrawableObject(const std::vector<const BasicBuffer*>& buffers)
	: _buffers(buffers)
{

}

const std::vector<const BasicBuffer*>& AdaptableDrawableObject::get_buffers() const
{
	return _buffers;
}

void AdaptableDrawableObject::change_buffers(const std::vector<const BasicBuffer*>& buffers)
{
	_buffers = buffers;
}


/*
	AdaptableElementDrawableObject
*/


AdaptableElementDrawableObject::AdaptableElementDrawableObject(const ElementBuffer* elements,
							   								   const std::vector<const BasicBuffer*>& buffers)
	: _elements(elements),
	  _buffers(buffers)

{

}


void AdaptableElementDrawableObject::change_buffers(const std::vector<const BasicBuffer*>& buffers)
{
	_buffers = buffers;
}

void AdaptableElementDrawableObject::change_elements(const ElementBuffer* elements)
{
	_elements = elements;
}


const std::vector<const BasicBuffer*>& AdaptableElementDrawableObject::get_buffers()  const
{
	return _buffers;
}

const ElementBuffer& AdaptableElementDrawableObject::get_elements() const
{
	return *_elements;
}

