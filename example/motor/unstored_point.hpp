#ifndef _UNSTORED_POINT_HPP_
#define _UNSTORED_POINT_HPP_

#include "physical_point.hpp"

class UnstoredPoint : public BasicPoint
{
public:	
	UnstoredPoint(const glm::vec3* position);

	const glm::vec3& get_position() const override;

private:
	glm::vec3 const* _position_ptr;

};

class UnstoredDirectedPoint : public BasicDirectedPoint
{
public:	
	UnstoredDirectedPoint(const glm::vec3* position, const glm::vec3* speed);

	const glm::vec3& get_position() const override;
	const glm::vec3& get_speed()    const override;

private:
	glm::vec3 const* _position_ptr;
	glm::vec3 const* _speed_ptr;

};

#endif //_UNSTORED_POINT_HPP_
