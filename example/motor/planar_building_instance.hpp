#ifndef _PLANAR_BUILDING_INSTANCE_HPP_
#define _PLANAR_BUILDING_INSTANCE_HPP_

#include "physical_instances.hpp"
#include "planar_building.hpp"
#include "motor_debug.hpp"

template<typename StoreType, typename... UninstancedTypes>
class PlanarInstances : public PhysicalInstances<sizeof...(UninstancedTypes),
														 UninstancedTypes...,
														 glm::vec4,
														 glm::vec4>
{
public:
	template<typename Type>
	struct storedDrawType
	{
		using type = ssl::draw_type;
	};
	enum buffersRole {DEP_SIZEY = sizeof...(UninstancedTypes), DIR2D_SIZEXZ};


	/*
		=== THE INSTANCE CLASS ===
	*/
	struct Instance
	{
	public:
		Instance(PlanarInstances* owner, uint inst_index, uint hbx_index, const StoreType& storage);
		
		StoreType& storage();
		const StoreType& storage() const;

		planarYPlace get_place()  const;
		BasicHitbox* get_hbx()    const;
		glm::vec3    get_center() const {return this->get_place().center;}

		void tp(const glm::vec3& new_pos);

	private:
		PlanarInstances* const _owner;
		const uint _inst_index;
		const uint _hbx_index;
		StoreType _storage;
	};
	/*
		===========================
	*/

public:
	PlanarInstances(HitboxContext& hbx_context,
						   ssl::draw_type element_draw_type,
						   typename storedDrawType<UninstancedTypes>::type ... buffers_draw_type,
						   ssl::draw_type dep_sizey_type,
						   ssl::draw_type dir2D_sizexy_type);

	std::vector<Instance>& get_instances();

	// Returns the index of the instance
	uint add_instance(const planarYPlace& place, const StoreType& storage = StoreType());
	uint add_static_instance(const planarYPlace& place, const StoreType& storage = StoreType());

	uint add_instance(const planarYPlace& place, bool static_hbx, const StoreType& storage = StoreType());

protected:
	virtual std::unique_ptr<BasicHitbox> build_hitbox(const planarYPlace& place) const {return NULL;}
	virtual void add_instance_AO(const planarYPlace& place) {};

	// Returns the instance index
	uint add_mesh_only(const planarYPlace& place);

private:
	std::vector<Instance> _instances;
};

struct EMPTY_STRUCT {};

template<typename... UninstancedTypes>
using PlanarBuildingInstances = PlanarInstances<EMPTY_STRUCT, UninstancedTypes...>;
	


template<typename StoreType, typename... UninstancedTypes>
PlanarInstances<StoreType, UninstancedTypes...>::PlanarInstances(HitboxContext& hbx_context, ssl::draw_type element_draw_type, typename storedDrawType<UninstancedTypes>::type ... buffers_draw_type, ssl::draw_type dep_sizey_type, ssl::draw_type dir2D_sizexy_type)
	: PhysicalInstances<sizeof...(UninstancedTypes), UninstancedTypes..., glm::vec4, glm::vec4>(hbx_context, element_draw_type, buffers_draw_type..., dep_sizey_type, dir2D_sizexy_type)
{

}

template<typename StoreType, typename... UninstancedTypes>
uint PlanarInstances<StoreType, UninstancedTypes...>::add_instance(const planarYPlace& place, bool static_hbx, const StoreType& storage)
{
	uint inst = this->add_mesh_only(place);
	uint hbxid;

	std::unique_ptr<BasicHitbox> hbx = this->build_hitbox(place);

	if(hbx != NULL)
	{
		if(static_hbx)
		{
			hbxid = this->add_static_hitbox_only(std::move(hbx));
		}
		else
		{
			hbxid = this->add_hitbox_only(std::move(hbx));
		}
	}

	uint ret = _instances.size();
	_instances.push_back(Instance(this, inst, hbxid, storage));

	this->add_instance_AO(place);
	return ret;
}

template<typename StoreType, typename... UninstancedTypes>
uint PlanarInstances<StoreType, UninstancedTypes...>::add_instance(const planarYPlace& place, const StoreType& storage)
{
	return this->add_instance(place, false, storage);
}

template<typename StoreType, typename... UninstancedTypes>
uint PlanarInstances<StoreType, UninstancedTypes...>::add_static_instance(const planarYPlace& place, const StoreType& storage)
{
	return this->add_instance(place, true, storage);
}

template<typename StoreType, typename... UninstancedTypes>
uint PlanarInstances<StoreType, UninstancedTypes...>::add_mesh_only(const planarYPlace& place)
{
	if((this->template get_array_buffer<DEP_SIZEY>()).size() != (this->template get_array_buffer<DIR2D_SIZEXZ>()).size())
	{
		mot::err("PlanarInstances::add_mesh_only(): buffers not aligned");
	}
	uint ret = this->template get_array_buffer<DEP_SIZEY>().size();

	this->template add_array_value<DEP_SIZEY>(glm::vec4(place.center, place.size.y));
	this->template add_array_value<DIR2D_SIZEXZ>(glm::vec4(place.dirN.x, place.dirN.y, place.size.x, place.size.z));

	return ret;
}

template<typename StoreType, typename... UninstancedTypes>
std::vector<typename PlanarInstances<StoreType, UninstancedTypes...>::Instance>& PlanarInstances<StoreType, UninstancedTypes...>::get_instances()
{
	return _instances;
}

template<typename StoreType, typename... UninstancedTypes>
PlanarInstances<StoreType, UninstancedTypes...>::Instance::Instance(PlanarInstances* owner, uint inst_index, uint hbx_index, const StoreType& storage)
	: _owner(owner),
	  _inst_index(inst_index),
	  _hbx_index(hbx_index),
	  _storage(storage)
{

}

template<typename StoreType, typename... UninstancedTypes>
planarYPlace PlanarInstances<StoreType, UninstancedTypes...>::Instance::get_place() const
{
	glm::vec4 depsizey    = (_owner->template get_array_buffer<DEP_SIZEY>()   )[_inst_index];
	glm::vec4 dir2Dsizexz = (_owner->template get_array_buffer<DIR2D_SIZEXZ>())[_inst_index];

	return planarYPlace{
		.center = glm::vec3(depsizey.x, depsizey.y, depsizey.z),
		.size = glm::vec3(dir2Dsizexz.z, depsizey.w, dir2Dsizexz.w),
		.dirN = glm::vec2(dir2Dsizexz.x, dir2Dsizexz.y)
	};
}

template<typename StoreType, typename... UninstancedTypes>
BasicHitbox* PlanarInstances<StoreType, UninstancedTypes...>::Instance::get_hbx() const
{
	return _owner->get_hitboxes()[_hbx_index].get();
}

template<typename StoreType, typename... UninstancedTypes>
void PlanarInstances<StoreType, UninstancedTypes...>::Instance::tp(const glm::vec3& newpos) 
{
	auto& buff = (_owner->template get_array_buffer<DEP_SIZEY>());
	buff.change_subvalue(_inst_index, glm::vec4(newpos.x, newpos.y, newpos.z, buff[_inst_index].w));
	_owner->get_hitboxes()[_hbx_index]->tp(newpos);
}

template<typename StoreType, typename... UninstancedTypes>
StoreType& PlanarInstances<StoreType, UninstancedTypes...>::Instance::storage()
{
	return _storage;
}
	
template<typename StoreType, typename... UninstancedTypes>
const StoreType& PlanarInstances<StoreType, UninstancedTypes...>::Instance::storage() const
{
	return _storage;
}

#endif //_PLANAR_BUILDING_INSTANCE_HPP_
