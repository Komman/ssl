#include "drawer_pack_controller.hpp"

#include "gutils.hpp"

DrawerPackController::DrawerPackController()
	: _drawers()
{

}

unsigned int DrawerPackController::add_drawer(Drawer* drawer)
{
	unsigned int ret = this->size();
	
	_drawers.push_back(drawer);
	
	return ret;
}

unsigned int DrawerPackController::size()
{
	return _drawers.size();
}

Drawer* DrawerPackController::pop_drawer(unsigned int index)
{
	#ifdef SSL_DEBUG
	if(index >= _drawers.size())
	{
		err("DrawerPackController::pop_drawer( " + std::to_string(index) + " ): index out of range: size=" + std::to_string(_drawers.size()));
	}
	#endif

	Drawer* ret = _drawers[index];

	utils::swapop(_drawers, index);

	return ret;
}

void DrawerPackController::clear()
{
	_drawers.clear();
}


// Control Functions

#define DRAWER_PACK_GENERATE_CONTROL(function, ...)\
	for(auto d : _drawers)\
	{\
		d->function(__VA_ARGS__);\
	}

void DrawerPackController::depth(bool enable)
{
	DRAWER_PACK_GENERATE_CONTROL(depth, enable)
}

void DrawerPackController::culling(bool enable)
{
	DRAWER_PACK_GENERATE_CONTROL(culling, enable)
}

void DrawerPackController::blending(bool enable)
{
	DRAWER_PACK_GENERATE_CONTROL(blending, enable)
}

void DrawerPackController::custom_view_port(const glm::uvec2& origin, const glm::uvec2& size)
{
	DRAWER_PACK_GENERATE_CONTROL(custom_view_port, origin, size)
}

void DrawerPackController::disable_custom_view_port()
{
	DRAWER_PACK_GENERATE_CONTROL(disable_custom_view_port,)
}

void DrawerPackController::set_blend_function(GLenum sfactor, GLenum dfactor)
{
	DRAWER_PACK_GENERATE_CONTROL(set_blend_function, sfactor, dfactor)
}

void DrawerPackController::set_blend_equation(GLenum mode)
{
	DRAWER_PACK_GENERATE_CONTROL(set_blend_equation, mode)
}

void DrawerPackController::depth_mask(bool enable)
{
	DRAWER_PACK_GENERATE_CONTROL(depth_mask, enable)
}

void DrawerPackController::depth_function(GLenum func)
{
	DRAWER_PACK_GENERATE_CONTROL(depth_function, func)
}

void DrawerPackController::set_stencil_params(const Drawer::stencilParams& params)
{
	DRAWER_PACK_GENERATE_CONTROL(set_stencil_params, params)
}

void DrawerPackController::stencil_testing(bool enable)
{
	DRAWER_PACK_GENERATE_CONTROL(stencil_testing, enable)
}

void DrawerPackController::stencil_mask(GLuint mask)
{
	DRAWER_PACK_GENERATE_CONTROL(stencil_mask, mask)
}

void DrawerPackController::stencil_function(GLenum func, GLint ref, GLuint mask)
{
	DRAWER_PACK_GENERATE_CONTROL(stencil_function, func, ref, mask)
}

void DrawerPackController::stencil_op(GLenum sfail, GLenum dpfail, GLenum dppass)
{
	DRAWER_PACK_GENERATE_CONTROL(stencil_op, sfail, dpfail, dppass)
}

void DrawerPackController::draw_object(const DrawableObject& obj)
{
	for(auto d : _drawers)
	{
		d->draw_object(obj);
	}

}

void DrawerPackController::draw_object(const ElementDrawableObject& obj)
{
	for(auto d : _drawers)
	{
		d->draw_object(obj);
	}
}

void DrawerPackController::draw_object(const BasicFrameBuffer& target_fb, const DrawableObject& obj)
{
	for(auto d : _drawers)
	{
		d->draw_object(target_fb, obj);
	}
}

void DrawerPackController::draw_object(const BasicFrameBuffer& target_fb, const ElementDrawableObject& obj)
{
	for(auto d : _drawers)
	{
		d->draw_object(target_fb, obj);
	}
}