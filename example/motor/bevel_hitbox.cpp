#include "bevel_hitbox.hpp"


BevelHitbox::BevelHitbox(const complexBevelShape& bevel)
	: ShapedHitboxFusion(bevel)
{
	squarePyramidShape base;

	base.base[0] = bevel.triangle_base_up[0];
	base.base[1] = bevel.triangle_base_up[1];
	base.base[2] = bevel.triangle_base_down[1];
	base.base[3] = bevel.triangle_base_down[0];

	base.top = bevel.triangle_base_up[2];

	this->fusion_hitbox<SquarePyramidHitbox>(base);


	prismInfo last_prism = {.points = {
		bevel.triangle_base_down[0],
		bevel.triangle_base_down[1],
		bevel.triangle_base_down[2],
		bevel.triangle_base_up[2]
	}};

	this->fusion_hitbox<PrismHitbox>(last_prism);
}

BevelHitbox::BevelHitbox(const bevelShape& bevel)
	: BevelHitbox(complexBevelShape{
		{
			bevel.triangle_base[0],
			bevel.triangle_base[1],
			bevel.triangle_base[2]
		},
		{
			bevel.triangle_base[0] + bevel.sized_direction,
			bevel.triangle_base[1] + bevel.sized_direction,
			bevel.triangle_base[2] + bevel.sized_direction
		}
	})
{
	
}

