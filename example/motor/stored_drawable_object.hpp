#ifndef _STORED_DRAWABLE_OBJECT_HPP_
#define _STORED_DRAWABLE_OBJECT_HPP_

#include "../../ssl/src/element_buffer.hpp"
#include "../../ssl/src/array_buffer.hpp"
#include "../../ssl/src/drawable_object.hpp"

using namespace ssl;

template<typename... Types>	
class StoredDrawableObject : public ElementDrawableObject
{
public:
	using tupleBrutType = std::tuple<Types...>;
	using tupleType     = std::tuple<ArrayBuffer<Types>...>;

	template<uint Index>
	struct brutType
	{
		using type = typename std::tuple_element<Index, tupleBrutType>::type;
	};
	template<uint Index>
	struct arrayBufferType
	{
		using type = typename std::tuple_element<Index, tupleType>::type;
	};

	template<typename Type>
	struct storedDrawType
	{
		using type = ssl::draw_type;
	};

public:
	StoredDrawableObject(ssl::draw_type element_draw_type, typename storedDrawType<Types>::type ... buffers_draw_type);

	// size of the first array buffer
	uint size() const;
	template<uint TypeNumber>
	const typename brutType<TypeNumber>::type & get_vertex(uint index) const;
	template<uint TypeNumber>
	typename arrayBufferType<TypeNumber>::type & get_array_buffer();
	template<uint TypeNumber>
	const typename arrayBufferType<TypeNumber>::type & get_array_buffer() const;
	template<uint TypeNumber>
	void add_array_value(const typename brutType<TypeNumber>::type& value);


	uint add_vertex(const Types& ... vertex_values);
	void replace_vertex(uint index, const Types& ... vertex_values);

	const std::vector<const BasicBuffer*>& get_buffers()  const override;
	const ElementBuffer&                   get_elements() const override;
	ElementBuffer& get_elements();

protected:
	ElementBuffer _elements;

private:
	template<std::size_t... Is>
	void fill_buffers_ptrs(std::index_sequence<Is...>);
	template<std::size_t... Is>
	uint add_vertex(const Types& ... vertex_values, std::index_sequence<Is...>);
	template<std::size_t... Is>
	void replace_vertex(uint index, const Types& ... vertex_values, std::index_sequence<Is...>);

private:
	tupleType _buffers;

	std::vector<const BasicBuffer*> _buffers_ptrs;
};





template<typename... Types>	
StoredDrawableObject<Types...>::StoredDrawableObject(ssl::draw_type element_draw_type, typename storedDrawType<Types>::type ... buffers_draw_type)
	: _elements(element_draw_type),
	  _buffers(buffers_draw_type...)
{
	this->fill_buffers_ptrs(std::make_index_sequence<sizeof...(Types)>{});
}


template<typename... Types>	
template<std::size_t... Is>
void StoredDrawableObject<Types...>::fill_buffers_ptrs(std::index_sequence<Is...>)
{
	((_buffers_ptrs.push_back(&(std::get<Is>(_buffers)))), ...);
}

template<typename... Types>	
const std::vector<const BasicBuffer*>& StoredDrawableObject<Types...>::get_buffers()  const
{
	return _buffers_ptrs;
}

template<typename... Types>	
const ElementBuffer&                   StoredDrawableObject<Types...>::get_elements() const
{
	return _elements;
}

template<typename... Types>	
ElementBuffer& StoredDrawableObject<Types...>::get_elements()
{
	return _elements;
}

template<typename... Types>	
uint StoredDrawableObject<Types...>::size() const
{
	return std::get<0>(_buffers).size();
}


template<typename... Types>	
template<uint TypeNumber>
const typename StoredDrawableObject<Types...>::template brutType<TypeNumber>::type& StoredDrawableObject<Types...>::get_vertex(uint index) const
{
	return std::get<TypeNumber>(_buffers)[index];
}

template<typename... Types>	
uint StoredDrawableObject<Types...>::add_vertex(const Types& ... vertex_values)
{
	return this->add_vertex(vertex_values..., std::make_index_sequence<sizeof...(Types)>{});
}

template<typename... Types>	
template<std::size_t... Is>
uint StoredDrawableObject<Types...>::add_vertex(const Types& ... vertex_values, std::index_sequence<Is...>)
{
	uint old_size = this->size();

	((std::get<Is>(_buffers).add_value(vertex_values)), ...);

	return old_size;
}

template<typename... Types>	
void StoredDrawableObject<Types...>::replace_vertex(uint index, const Types& ... vertex_values)
{
	if(index > this->size())
	{
		ssl::err("StoredDrawableObject<Types...>::replace_vertex(): index out of range");
	}

	this->replace_vertex(index, vertex_values..., std::make_index_sequence<sizeof...(Types)>{});
}

template<typename... Types>	
template<std::size_t... Is>
void StoredDrawableObject<Types...>::replace_vertex(uint index, const Types& ... vertex_values, std::index_sequence<Is...>)
{
	((std::get<Is>(_buffers).change_subvalue(index, vertex_values)), ...);
}

template<typename... Types>	
template<uint TypeNumber>
typename StoredDrawableObject<Types...>::template arrayBufferType<TypeNumber>::type & StoredDrawableObject<Types...>::get_array_buffer()
{
	return std::get<TypeNumber>(_buffers);
}

template<typename... Types>	
template<uint TypeNumber>
const typename StoredDrawableObject<Types...>::template arrayBufferType<TypeNumber>::type & StoredDrawableObject<Types...>::get_array_buffer() const
{
	return std::get<TypeNumber>(_buffers);
}

template<typename... Types>	
template<uint TypeNumber>
void StoredDrawableObject<Types...>::add_array_value(const typename brutType<TypeNumber>::type& value)
{
	this->get_array_buffer<TypeNumber>().add_value(value);
}



#endif //_STORED_DRAWABLE_OBJECT_HPP_
