#include "physic.hpp"

namespace physic
{
	shootingDirection compute_shot_direction(
			const glm::vec3& position,
			const glm::vec3& target,
			float initial_speed,
			float gravity)
	{
		shootingDirection ret;

		glm::vec3 to_target  = target - position;	
		glm::vec3 to_targetX = glm::vec3(to_target.x, 0.0f, to_target.z);

		float xb = glm::length(to_targetX);
		float yb = to_target.y;

		float g = -gravity;
		float V = initial_speed;

		float A = g*xb*xb/(2.0f*yb*V*V);
		float B = xb*xb/yb/yb;

		glm::vec3 axeX = glm::normalize(to_targetX);
		glm::vec3 axeY = glm::vec3(0.0,1.0,0.0);

		float delta = (2.0f*A-B)*(2.0f*A-B) - 4.0f*(B+1.0f)*A*A;

		if(delta >= 0.0f)
		{
			ret.possible = true;
			
			float x1 = (-(2.0f*A - B) - sqrt(delta))/(2.0f*(B+1.0f));
			float x2 = (-(2.0f*A - B) + sqrt(delta))/(2.0f*(B+1.0f));

			float sqrtx1 = sqrt(x1);
			float sqrtx2 = sqrt(x2);

			float alpha1 = acos(sqrtx1);
			float alpha2 = acos(sqrtx2);

			float vx1 = sqrtx1;
			float vy1 = sin(alpha1);

			float vx2 = sqrtx2;
			float vy2 = sin(alpha2);
			
			ret.bell_directionN   = glm::normalize(axeX*vx1 + axeY*vy1);	
			ret.direct_directionN = glm::normalize(axeX*vx2 + axeY*vy2*((yb > 0) ? 1.0f : -1.0f));	
		}
		else
		{
			ret.possible = false;
		}

		return ret;
	}
};
