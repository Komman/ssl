#ifndef _FRICTION_POINT_HPP_
#define _FRICTION_POINT_HPP_

#include "physical_point.hpp"

class FrictionPoint : public PhysicalPoint
{
public:
	FrictionPoint(const glm::vec3& position,
				  float friction_coeff);

	FrictionPoint(const glm::vec3& position,
				  const glm::vec3& speed,
				  const glm::vec3& acceleration,
				  float friction_coeff);

	void animate(float dt) override;

	GETTER_BUILDER_COPY(float, friction_coeff)

private:
	float _friction_coeff;

};

#endif //_FRICTION_POINT_HPP_
