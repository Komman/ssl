#ifndef _CONTROLLED_DRAWER_HPP_
#define _CONTROLLED_DRAWER_HPP_

#include "drawer_pack_controller.hpp"

class ControlledDrawer : public Drawer
{
public:
	ControlledDrawer(const BasicShaderParser&        parser,
			   		 const std::string&              vertex_shader_name,
			   		 const std::string&              fragment_shader_name,
			   		 const std::vector<std::string>& fragment_modifiers,
			   		 DrawerPackController&           controllor);

	ControlledDrawer(const BasicShaderParser& parser,
			   		 const std::string&       vertex_shader_name,
			   		 const std::string&       fragment_shader_name,
			   		 DrawerPackController&    controllor)
		: ControlledDrawer(parser, vertex_shader_name, fragment_shader_name, {}, controllor) {}
	virtual ~ControlledDrawer() {}

};

#endif //_CONTROLLED_DRAWER_HPP_
