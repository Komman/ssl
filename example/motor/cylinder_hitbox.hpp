#ifndef _CYLINDER_HITBOX_HPP_
#define _CYLINDER_HITBOX_HPP_

#include "shaped_hitbox_fusion.hpp"
#include "bevel_hitbox.hpp"

struct cylinderInfo
{
	glm::vec3 bottom;
	glm::vec3 to_top;
	glm::vec3 base_orientationN;
	float radius;
};

class CylinderHitbox : public ShapedHitboxFusion<cylinderInfo>
{
public:
	// side_faces, must be >= 3
	CylinderHitbox(const cylinderInfo& shape, uint side_faces);

private:
	void build();

private:
	uint _side_faces; 
};

#endif //_CYLINDER_HITBOX_HPP_
