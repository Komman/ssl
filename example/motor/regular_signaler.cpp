#include "regular_signaler.hpp"

RegularSignaler::RegularSignaler(float frequency)
	: _frequency(frequency),
	  _modular_time(0.0)
{

}

void  RegularSignaler::change_frequency(float frequency)
{
	_frequency = frequency;
}

float RegularSignaler::get_frequency()
{
	return _frequency;
}

uint RegularSignaler::animate(float dt)
{
	_modular_time += dt;

	uint ret = (uint)(_modular_time * _frequency);

	_modular_time -= (float)(ret) / _frequency;

	return ret;
}
