#include "hitbox_context.hpp"

#include <iostream>
#include <glm/gtx/string_cast.hpp>

using namespace std;
using namespace glm;

HitboxContext::HitboxContext(const staticHitboxDisc& static_range)
	: _static_range(static_range)
{
	if(_static_range.array_size == 0)
	{
		mot::err("HitboxContext::HitboxContext(): _static_range.array_size must be > 0");
	}

	for(uint x=0; x<static_range.array_size; x++)
	{
		_hbxs_array.push_back({});

		for(uint y=0; y<static_range.array_size; y++)
		{
			_hbxs_array[_hbxs_array.size()-1].push_back({});
		}
	}
}

void HitboxContext::add_hitbox(const BasicHitbox* hitbox_ptr)
{
	_hitboxes.push_back(hitbox_ptr);	
}

void HitboxContext::add_static_hitbox(const BasicHitbox* hitbox_ptr)
{
	_static_hitboxes.push_back(hitbox_ptr);	

	std::vector<uvec2> cases = this->submap(hitbox_ptr->absolute_space_box());

	for(uvec2 c : cases)
	{
		_hbxs_array[c.x][c.y].push_back(hitbox_ptr);
	}
}
	
void HitboxContext::add_hitboxes(const std::vector<BasicHitbox::storedHitbox>& hitboxes)
{
	for(uint i=0; i<hitboxes.size(); i++)
	{
		this->add_hitbox(hitboxes[i].get());	
	}
}
	
void HitboxContext::add_static_hitboxes(const std::vector<BasicHitbox::storedHitbox>& hitboxes)
{
	for(uint i=0; i<hitboxes.size(); i++)
	{
		this->add_static_hitbox(hitboxes[i].get());	
	}
}

bool HitboxContext::in_collision(const BasicHitbox& hbx) const
{
	for(auto h : this->close_static_hitboxes(hbx))
	{
		if(h == &hbx)
			continue;

		if(hbx.in_collision(*h))
		{
			return true;
		}
	}

	for(auto h : _hitboxes)
	{
		if(h == &hbx)
			continue;
		
		if(hbx.in_collision(*h))
		{
			return true;
		}
	}

	return false;
}


contextImpact HitboxContext::impact(const BasicHitbox& hbx, float dt) const
{
	glm::vec3 distance = hbx.get_speed()*dt;

	const BasicHitbox* rhbx = NULL;
	collisionImpact ret;
	ret.impact = false;
	ret.distance = 2.0*glm::length(distance);

	for(auto h : this->close_static_hitboxes(hbx, dt))
	{
		if(h == &hbx)
			continue;

		auto imp = hbx.impact(*h, dt);
		if(imp.impact && imp.distance < ret.distance)
		{
			ret = imp;
			rhbx = h;
		}
	}
	for(auto h : _hitboxes)
	{
		if(h == &hbx)
			continue;

		auto imp = hbx.impact(*h, dt);
		if(imp.impact && imp.distance < ret.distance)
		{
			ret = imp;
			rhbx = h;
		}
	}

	return {rhbx, ret};
}

contextImpact HitboxContext::impact(const BasicHitbox& hbx, const glm::vec3& self_directionN, float distance) const
{
	collisionImpact ret;

	const BasicHitbox* rhbx = NULL;
	ret.impact = false;
	ret.distance = 2.0*distance;

	for(auto h : this->close_static_hitboxes(hbx, self_directionN, distance))
	{
		if(h == &hbx)
			continue;

		auto imp = hbx.impact(*h, self_directionN, distance);
		ret = geom::first_impact(ret, imp);
	}
	for(auto h : _hitboxes)
	{
		if(h == &hbx)
			continue;

		auto imp = hbx.impact(*h, self_directionN, distance);
		ret = geom::first_impact(ret, imp);
	}

	return {rhbx, ret};
}

bool HitboxContext::inmap(const glm::vec3& pos) const
{
	return pos.x >= _static_range.range.min.x
		&& pos.z >= _static_range.range.min.z
		&& pos.x <= _static_range.range.max.x
		&& pos.z <= _static_range.range.max.z;
}

glm::uvec2 HitboxContext::map_coord(const glm::vec3& pos) const
{
	// if(!this->inmap(pos))
	// {
	// 	mot::err("HitboxContext::map_coord(): pos out of map");
	// }

	vec2 xz = geom::projplan(pos);
	vec2 xzmin = geom::projplan(_static_range.range.min);
	vec2 xzmax = geom::projplan(_static_range.range.max);

	uvec2 ret = uvec2((xz-xzmin)/(xzmax-xzmin)*float(_static_range.array_size));

	// if(ret.x < 0 || ret.y >= _static_range.array_size)
	// {
	// 	mot::err("HitboxContext::map_coord(): impossible");
	// }

	return glm::clamp(ret, uvec2(0), uvec2(_static_range.array_size-1u));
}

std::vector<glm::uvec2> HitboxContext::submap(const spaceBox& box) const
{
	std::vector<glm::uvec2> ret;

	uvec2 bmin = this->map_coord(box.min);
	uvec2 bmax = this->map_coord(box.max);

	for(uint x=bmin.x; x<=bmax.x; x++)
	{
		for(uint y=bmin.y; y<=bmax.y; y++)
		{
			ret.push_back(uvec2(x,y));
		}
	}

	return ret;
}

std::vector<const BasicHitbox*> HitboxContext::subhbxs(const std::vector<glm::uvec2>& sub) const
{
	std::vector<const BasicHitbox*> ret;

	for(uvec2 p : sub)
	{
		for(const BasicHitbox* hbx : _hbxs_array[p.x][p.y])
		{
			ret.push_back(hbx);
		}
	}

	return ret;
}

#include "debug_draw.hpp"

std::vector<const BasicHitbox*> HitboxContext::close_static_hitboxes(const BasicHitbox& hbx) const
{
	return this->subhbxs(this->submap(hbx.absolute_space_box()));
}

std::vector<const BasicHitbox*> HitboxContext::close_static_hitboxes(const BasicHitbox& hbx, float dt) const
{
	spaceBox b = hbx.absolute_space_box();
	b.cover_translation(hbx.get_speed()*dt);
	std::vector<glm::uvec2> sm = this->submap(b);

	// for(auto c : sm)
	// {
	// 	cout<<to_string(c)<<endl;
	// }

	// for(const auto& h : this->subhbxs(sm))
	// {
	// 	debug_draw::hitbox(*h);
	// }
 
	return this->subhbxs(sm);
}

std::vector<const BasicHitbox*> HitboxContext::close_static_hitboxes(const BasicHitbox& hbx, const glm::vec3& self_directionN, float distance) const
{
	spaceBox b = hbx.absolute_space_box();
	b.cover_translation(self_directionN*distance);
	std::vector<glm::uvec2> sm = this->submap(b);

	// for(auto c : sm)
	// {
	// 	cout<<to_string(c)<<endl;
	// }

	// for(const auto& h : this->subhbxs(sm))
	// {
	// 	debug_draw::hitbox(*h);
	// }
 
	return this->subhbxs(sm);
}
