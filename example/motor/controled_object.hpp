#ifndef _CONTROLED_OBJECT_HPP_
#define _CONTROLED_OBJECT_HPP_

#include "hitbox_context.hpp"
#include "unstored_point.hpp"
#include "target.hpp"
#include "basic_hitbox.hpp"

class ControledObject
{
public:
	ControledObject(const glm::vec3& position);

	void set_context(const HitboxContext* context);
	void set_speed(const glm::vec3& speed);
	void tp(const glm::vec3& new_position);

	const glm::vec3& get_position();
	const glm::vec3& get_speed();

	struct cMove
	{
		contextImpact ctximpact;
		glm::vec3     move;
	};

	// Returns the resulted move
	cMove move(const glm::vec3& dp);

public:
	virtual const BasicHitbox& get_hitbox() const =0;

private:
	glm::vec3 _position;
	glm::vec3 _speed;

	const HitboxContext* _context;
};

#endif //_CONTROLED_OBJECT_HPP_
