#ifndef _DUAL_MULTI_GRAPH_HPP_
#define _DUAL_MULTI_GRAPH_HPP_

#include <unordered_map>

#include "bordered_graph.hpp"

class DualMultiGraph
{
public:
	using Edge = BasicGraph::Edge;

	struct faceEdge
	{
		Edge edge;
		Edge face_edge;
	};

public:
	DualMultiGraph(const BorderedGraph& g);

	const BorderedGraph& graph() const;

	uint get_border_face() const;
	std::vector<uint> all_faces() const;
	std::vector<uint> all_faces_noborder() const;

	const glm::vec2& position(uint face) const;
	std::vector<glm::vec2> face_vertices_positions(uint face) const;

	// In trigonometric order
	const std::vector<uint>& face_vertices(uint face) const;
	const std::vector<Edge>& face_edges(uint face) const;
	// Edges bewteen two faces
	// empty vector if faces are not adjacent
	// {face1 < face2}
	const std::vector<Edge>& faces_edges(uint face1, uint face2) const;
	std::vector<DualMultiGraph::faceEdge> all_face_edges() const;

	/*
		If face edges are removed, then two faces
		that share an edge can be not adjacent
	*/
	bool adjacent(uint face1, uint face2) const;
	std::vector<uint> adjacent_faces(uint face) const;
	// inside means without the borderface
	std::vector<uint> inborder_adjacent_faces(uint face) const;

	// Returns the two faces that shares this edge
	Edge adjacent_faces(const Edge& e) const;
	uint opposite_face(const Edge& e, uint face) const;

protected:
	void check_face(uint face) const;
	Edge order(const Edge& e) const;

private:
	void compute_faces();

	struct faceInfos
	{
		glm::vec2 middle;
		std::vector<uint> vertices;
		std::vector<Edge> edges;
	};

private:
	void add_face_adjency(uint f1, uint f2, const Edge& e);

private:
	BorderedGraph _graph;

	uint _border_face;
	std::vector<faceInfos> _faces;

	/*
		_face_adjency[f1][f2] is the vector of edges
		that separate face f1 from face f2.

		If it is empty, then f1 and f2 are not adjacent. 
		This matrix is only triangular inferior filled:
		we must have f1 < f2
	*/
	std::vector<std::vector<std::vector<Edge>>> _face_adjency;

	// To a give endge, returns the two faces that are adjacent to it
	std::unordered_map<Edge, Edge, Graph::hashEdge> _face_edges;
};

#endif //_DUAL_MULTI_GRAPH_HPP_
