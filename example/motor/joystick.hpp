#ifndef _JOYSTICK_HPP_
#define _JOYSTICK_HPP_

#include <vector>

#include "../../ssl/src/window.hpp"
#include "../../ssl/src/view.hpp"

#include "basic_mousekey_target.hpp"

class BasicJoystick
{
public:
	BasicJoystick(const BasicMousekeyTarget* mousekey);
	virtual ~BasicJoystick() {}

	const BasicMousekeyTarget* mousekey_ptr() const;
	bool key_pressed(int key) const;

	virtual glm::vec3 compute_directionN() const =0;


private:
	const BasicMousekeyTarget* _mousekey;
};

enum joystickKeysIndices {
	JOYSTICK_CONTROL_RIGHT=0,
	JOYSTICK_CONTROL_LEFT,
	JOYSTICK_CONTROL_FRONT,
	JOYSTICK_CONTROL_BACK,
	JOYSTICK_CONTROL_UP,
	JOYSTICK_CONTROL_DOWN,

	JOYSTICK_NUMBER_CONTROLS
};

class Joystick : public BasicJoystick
{
public:
	Joystick(const BasicMousekeyTarget* mousekey, int keys[JOYSTICK_NUMBER_CONTROLS], const ssl::View* view);

	glm::vec3 compute_directionN() const;

private:
	glm::vec3 directionN_from_pressed(const std::vector<joystickKeysIndices>& pressed) const;

private:
	const ssl::View* _view;
	std::vector<int> _control_keys;

};

#endif //_JOYSTICK_HPP_
