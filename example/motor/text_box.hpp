#ifndef _TEXT_BOX_HPP_
#define _TEXT_BOX_HPP_

#include <string>
#include <glm/glm.hpp>
#include "../../ssl/ssl.hpp"

#include "window_console.hpp"

using namespace ssl;

struct textCharBox
{
	char c;
	glm::vec3 color;
};

struct textCharGPU
{
	char c;
	// From 0 to 7
	char colorcode;
};

class TextBox
{
public:
	TextBox(const std::string& text_bitmap_path,
		    uint max_char_amount,
			const std::string& uniform_name,
			float spacing = 1.05f);

	static std::vector<textCharGPU> translate_string_gpu(const std::string& str);
	static std::vector<textCharBox> translate_string(const std::vector<textCharGPU>& str);
	static std::vector<textCharBox> translate_string(const std::string& str);

	void draw_box(Drawer& text_drawer, const glm::vec2& minpos, const glm::vec2& maxpos);
	void draw_text(const std::string& text, Drawer& text_drawer, const glm::vec2& screen_pos, float text_size);
	void draw_text(const std::vector<textCharGPU>& text, Drawer& text_drawer, const glm::vec2& screen_pos, float text_size);
	void draw_texts(const std::vector<std::string>& texts, Drawer& text_drawer, const glm::vec2& screen_pos, float text_size);

	void draw_console(const WindowConsole& console, Drawer& text_drawer, const glm::vec2& screen_pos, float text_size);

private:
	void set_box(const glm::vec2& min_coord, const glm::vec2& max_coord);
	void set_uniform_text(const std::string& str);
	void set_uniform_text_chars(const std::vector<textCharGPU>& gpuchars);
	void draw_stored_text(Drawer& text_drawer, const glm::vec2& screen_pos, float text_size);
	float get_yratio() const;

private:
	uint  _char_amount;
	uint  _max_lines;
	float _spacing;

	Uniform<int> _draw_type;

	UniformArray<glm::uvec2> _text;
	ArrayBuffer<glm::vec2>   _box;
	ArrayBuffer<glm::vec2>   _uvs;
	ElementBuffer            _indices;

	UniformTexture2D<glm::vec3> _bitmap;
};

#endif //_TEXT_BOX_HPP_
