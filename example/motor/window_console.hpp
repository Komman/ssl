#ifndef _WINDOW_CONSOLE_HPP_
#define _WINDOW_CONSOLE_HPP_

#include "../../ssl/src/window.hpp"
#include "console.hpp"
#include "basic_mousekey_target.hpp"
#include "mousekey_switcher.hpp"


class WindowConsole : public Console
{
public:
	WindowConsole(MousekeySwitcher* switcher, int write_key, float lasting);

	bool typing() const;

	// Store the current mousekey
	void open();
	// And give it back the control 
	void exit();

	void add_letter(char c);
	void remove_last_letter();
	void erase_command();
	void replace_current_command(const std::string& str);

	void send_current_command();

	const BasicMousekeyTarget& mousekey() const;
	const std::string& current_text() const;
	int write_key() const;
	void print_current_text() const;
	
private:
	int _write_key;

	BasicMousekeyTarget _mousekey;
	
	MousekeySwitcher*    _switcher;
	BasicMousekeyTarget* _exit_mousekey;

	std::string _command;
};

namespace CONSOLE
{	
	WindowConsole& get();
	// GLFW Must be initialized
	void set(WindowConsole* console);
	// Enable/disable the consolekey reaction, that allows the main
	// console to take the mouskey to write command
	void disable_callback();
	void enable_callback();

	void say(const std::string& msg);

	void CONTROL_N(const std::string& name, void* fptr);
	template<typename ArgType>
	void COMMAND(const std::string& command, void(*func)(ArgType x));
};

#define CONSOLE_CONTROL(var) CONSOLE::get().set_modifiable(#var, &var)





namespace CONSOLE
{
	template<typename ArgType>
	void COMMAND(const std::string& command, void(*func)(ArgType x))
	{
		get().create_command(command, func);
	}
};

#endif //_WINDOW_CONSOLE_HPP_
