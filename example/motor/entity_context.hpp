#ifndef _ENTITY_CONTEXT_HPP_
#define _ENTITY_CONTEXT_HPP_

#include "../motor/player.hpp"

class EntityContext
{
public:
	EntityContext();
	void set_player(Player* player);

	// energy in speed*mass*distance
	void energy_explosion(const glm::vec3& center, float energy, float constant_radius);

	
public:
	// Non NULL
	static EntityContext* const global;

private:
	Player* _player;
};


#endif //_ENTITY_CONTEXT_HPP_
