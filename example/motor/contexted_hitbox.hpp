#ifndef _CONTEXTED_HITBOX_HPP_
#define _CONTEXTED_HITBOX_HPP_

#include "hitbox_context.hpp"

template<typename HitboxType>
class Contexted : public HitboxType
{
public:
	template<typename... ContructorArgs>
	Contexted(HitboxContext& context, ContructorArgs... args)
		: HitboxType(args...)
	{
		context.add_hitbox(this);
	}
};

#endif //_CONTEXTED_HITBOX_HPP_
