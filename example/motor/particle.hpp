#ifndef _PARTICLE_HPP_
#define _PARTICLE_HPP_

#include "friction_point.hpp"

class Particle : public FrictionPoint
{
public:
	// color.w is bloom coeff, distance-dependant if negative
	Particle(const glm::vec3& position,
		     const glm::vec4& color,
			 float size,
			 float friction_coeff,
			 float last_time);

	Particle(const glm::vec3& position,
			 const glm::vec3& speed,
			 const glm::vec3& acceleration,
			 const glm::vec4& color,
			 float size,
			 float friction_coeff,
			 float last_time);

	void animate(float dt) override;
	bool is_still_alive() const;

	GETTER_BUILDER(glm::vec4, color)
	GETTER_BUILDER_COPY(float, last_time);
	GETTER_BUILDER_COPY(float, left_time);
	GETTER_BUILDER_COPY(float, size);

	void  set_light_bloom_affection(float coeff);
	float get_light_bloom_affection() const;

private:
	/*
		color.xyz : color
		color.w   : bloom multiplier
	*/
	glm::vec4 _color;
	float _size;
	float _last_time;
	float _left_time;
	float _lightonbloom_coeff;
};

#endif //_PARTICLE_HPP_
