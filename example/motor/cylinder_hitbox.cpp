#include "cylinder_hitbox.hpp"

#include "motor_debug.hpp"

CylinderHitbox::CylinderHitbox(const cylinderInfo& shape, uint side_faces)
	: ShapedHitboxFusion(shape),
	  _side_faces(side_faces)
{
	if(glm::length(glm::cross(shape.to_top, shape.base_orientationN)) <= 0.001)
	{
		mot::err("CylinderHitbox::CylinderHitbox(): cylinder base is colinar to is bot->top vector");
	}

	this->build();
}

void CylinderHitbox::build()
{
	const auto& cylinder = this->get_shape();

	glm::vec3 rot_axisN = glm::cross(cylinder.base_orientationN, cylinder.to_top);
	rot_axisN = glm::normalize(glm::cross(cylinder.base_orientationN, rot_axisN));

	std::vector<glm::vec3> base_points = geom::circle_points(
		glm::vec3(0),
		cylinder.base_orientationN,
		rot_axisN,
		cylinder.radius,
		_side_faces
	);

	for(uint i=0; i<_side_faces; i++)
	{
		this->fusion_hitbox<BevelHitbox>(bevelShape{
			.triangle_base   = {
									cylinder.bottom + base_points[i],
									cylinder.bottom + base_points[(i+1)%_side_faces],
									cylinder.bottom,
							   },
			.sized_direction = cylinder.to_top
		});
	}
}
