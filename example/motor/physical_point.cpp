#include "physical_point.hpp"

PhysicalPoint::PhysicalPoint(const glm::vec3& position)
	: _position(position),
	  _speed(0.0f),
	  _acceleration(0.0f)
{

}
	
PhysicalPoint::PhysicalPoint(const glm::vec3& position,
				  			 const glm::vec3& speed,
				  			 const glm::vec3& acceleration)
	: _position(position),
	  _speed(speed),
	  _acceleration(acceleration)
{

}

void PhysicalPoint::animate(float dt)
{
	_speed    += _acceleration * dt;
	_position += _speed * dt;
}

void PhysicalPoint::tp(const glm::vec3& position)
{
	this->set_position(position);
}

void PhysicalPoint::move(const glm::vec3& dposition)
{
	this->set_position(this->get_position() + dposition);
}

const glm::vec3& PhysicalPoint::get_position() const
{
	return _position;
}

const glm::vec3& PhysicalPoint::get_speed()    const
{
	return _speed;
}


void PhysicalPoint::set_position(const glm::vec3& position)
{
	_position = position;
}

void PhysicalPoint::set_speed(const glm::vec3& speed)
{
	_speed = speed;
}

void PhysicalPoint::set_acceleration(const glm::vec3& acceleration)
{
	_acceleration = acceleration;
}
