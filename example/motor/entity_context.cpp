#include "entity_context.hpp"

#include "../utils/geom.hpp"

using namespace std;
using namespace glm;

EntityContext::EntityContext()
{

}

void EntityContext::set_player(Player* player)
{
	_player = player;
}

// #include "window_console.hpp"

void EntityContext::energy_explosion(const glm::vec3& center, float energy, float distdim)
{
	if(_player != NULL)
	{
		vec4 dirNl = geom::safe_normalize_lenght(_player->get_position() - center); 
		float constdist = glm::max(1.0f, dirNl.w/distdim);
		float resenergy = energy/(constdist);

		// CONSOLE::say("div: " + std::to_string(constdist));

		if((_player->get_position() - center).y > -distdim)
		{
			_player->energy_impulsion(vec3(0,1,0), resenergy*1.0f/3.0f);
			_player->energy_impulsion(vec3(dirNl.x, glm::max(0.0f, dirNl.y), dirNl.z), resenergy*2.0f/3.0f);
		}
		else
		{
			_player->energy_impulsion(vec3(dirNl), resenergy);
		}
	}
}

static EntityContext _global_instance;

EntityContext* const EntityContext::global = &_global_instance;
