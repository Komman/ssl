#ifndef _CONE_HITBOX_HPP_
#define _CONE_HITBOX_HPP_

#include "shaped_hitbox_fusion.hpp"

struct coneInfo
{
	glm::vec3 bottom;
	glm::vec3 to_top;
	glm::vec3 base_orientationN;
	float radius;
};

class ConeHitbox : public ShapedHitboxFusion<coneInfo>
{
public:
	// side_faces, must be >= 3
	ConeHitbox(const coneInfo& shape, uint side_faces);

private:
	void build();

private:
	uint _side_faces; 
};

#endif //_CONE_HITBOX_HPP_
