#ifndef _THICK_GRAPH_HPP_
#define _THICK_GRAPH_HPP_

#include "bordered_graph.hpp"


/*
	A ThickGraph ensures that every pair
	edge-edge or vertex-edge or vertex-vertex
	are at least separated of "thickness". 

	For now, a ThickGraph is necessarly not 
	oriented (but it can be fixed).
*/

class ThickGraph : public BorderedGraph
{
public:
	ThickGraph(const BorderedGraph& g, float thickness);

	/*
		A ThickGraph cannot be modfied with these methods
		because they can unthick it
	*/
	// void delaunay_triangulation() = delete;
	// void set_chicken_triangulation() = delete;
	// uint add_vertex() = delete;
	// void add_edge(const Edge& e) = delete;

private:
	void remove_close_vertices();
	void remove_close_edges();

private:
	float _thickness;
};

#endif //_THICK_GRAPH_HPP_
