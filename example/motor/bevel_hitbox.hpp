#ifndef _BEVEL_HITBOX_HPP_
#define _BEVEL_HITBOX_HPP_

#include "shaped_hitbox_fusion.hpp"
#include "square_pyramid_hitbox.hpp"

struct bevelShape
{
	glm::vec3 triangle_base[3];
	glm::vec3 sized_direction;
};
struct complexBevelShape
{
	glm::vec3 triangle_base_up[3];
	glm::vec3 triangle_base_down[3];
};

class BevelHitbox : public ShapedHitboxFusion<complexBevelShape>
{
public:
	BevelHitbox(const complexBevelShape& bevel);
	BevelHitbox(const bevelShape& bevel);

};

#endif //_BEVEL_HITBOX_HPP_
