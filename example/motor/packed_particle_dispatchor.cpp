
#include "packed_particle_dispatchor.hpp"

using namespace std;
using namespace glm;
using namespace ssl;

PackedParticleDispatchor::PackedParticleDispatchor(const std::string& name)
	: BasicParticleDispatchor(),
	  _dep_LBAC(STATIC_DRAW),
	  _colors(STATIC_DRAW),
	  _start_speeds(STATIC_DRAW),
	  _start_accelerations(STATIC_DRAW),
	  _infos(STATIC_DRAW),
	  _start_check_adding(0),
	  _time(name + "_time", 0.0f)
{

}

void PackedParticleDispatchor::add_particle(const Particle& particle)
{
	#ifdef SSL_DEBUG
	this->check_sizes();
	#endif

	vec4 dep_LBAC= vec4(particle.get_position(), particle.get_light_bloom_affection());
	vec4 color   = particle.get_color();
	vec3 speed   = particle.get_speed();
	vec3 acceleration = particle.get_acceleration();
	vec4 infos   = vec4(
		_time.get_value(),
		particle.get_left_time(),
		particle.get_friction_coeff(),
		particle.get_size()
	);

	unsigned int i = _infos.size();
	for(i=_start_check_adding; i<_infos.size(); i++)
	{
		if(this->is_finished(_infos[i]))
		{
			_dep_LBAC.change_subvalue(i, dep_LBAC);
			_colors.change_subvalue(i, color);
			_start_speeds.change_subvalue(i, speed);
			_start_accelerations.change_subvalue(i, acceleration);
			_infos.change_subvalue(i, infos);
			
			break;
		}
	}

	if(i == _infos.size())
	{
		_dep_LBAC.add_value(dep_LBAC);
		_colors.add_value(color);
		_start_speeds.add_value(speed);
		_start_accelerations.add_value(acceleration);
		_infos.add_value(infos);
	}

	_start_check_adding = i+1;
}

void PackedParticleDispatchor::animate(float dt)
{
	_time += dt;
	_start_check_adding = 0;

	#ifdef SSL_DEBUG
	this->check_sizes();
	#endif

	if(_infos.size() > 0)
	{
		if(this->is_finished(_infos[_infos.size() - 1]))
		{
			bool lasts_finshed = true;

			int lasts_count = sqrt(_infos.size());
			lasts_count = std::max((int)_infos.size() - 1 - lasts_count, 0);
			
			for(int i=(int)_infos.size() - 2; i> lasts_count; i--)
			{
				lasts_finshed = lasts_finshed && this->is_finished(_infos[i]);
			}
			
			if(lasts_finshed)
			{
				this->update_buffers();
			}
		}
	}

	// To prevent time to be not precise enough if it is too big
	if(this->size() == 0)
	{
		_time = 0.0f;
	}
}

bool PackedParticleDispatchor::is_finished(const glm::vec4& info)
{
	return (info.x + info.y < _time.get_value());
}


unsigned int PackedParticleDispatchor::finished_count()
{
	unsigned int s = 0;

	for(unsigned int i=0; i<_infos.size(); i++)
	{
		s+=this->is_finished(_infos[i]);
	}

	return s;
}


void PackedParticleDispatchor::update_buffers()
{
	#ifdef SSL_DEBUG
	this->check_sizes();
	#endif
	
	for(unsigned int i=0; i<_dep_LBAC.size(); i++)
	{
		if(this->is_finished(_infos[i]))
		{
			utils::swapop(_dep_LBAC           , i);			
			utils::swapop(_colors             , i);			
			utils::swapop(_start_speeds       , i);			
			utils::swapop(_start_accelerations, i);			
			utils::swapop(_infos              , i);			

			i--;
		}		
	}

	_dep_LBAC.instant_update_GPU();
	_colors.instant_update_GPU();
	_start_speeds.instant_update_GPU();
	_start_accelerations.instant_update_GPU();
	_infos.instant_update_GPU();
}

void PackedParticleDispatchor::check_sizes() const
{
	if(_dep_LBAC.size() != _start_speeds.size() || _dep_LBAC.size() != _start_accelerations.size() || _dep_LBAC.size() != _infos.size() || _colors.size() != _dep_LBAC.size())
	{
		err("PackedParticleDispatchor:check_sizes(): different buffer size");
	}
}


unsigned int PackedParticleDispatchor::size() const
{
	#ifdef SSL_DEBUG
	this->check_sizes();
	#endif

	return _dep_LBAC.size();
}

const ssl::ArrayBuffer<glm::vec4>& PackedParticleDispatchor::get_dep_LBAC() const
{
	return _dep_LBAC;
}

const ssl::ArrayBuffer<glm::vec4>& PackedParticleDispatchor::get_colors() const
{
	return _colors;
}

const ssl::ArrayBuffer<glm::vec3>& PackedParticleDispatchor::get_start_speeds() const
{
	return _start_speeds;
}


const ssl::ArrayBuffer<glm::vec3>& PackedParticleDispatchor::get_start_accelerations() const
{
	return _start_accelerations;
}

const ssl::ArrayBuffer<glm::vec4>& PackedParticleDispatchor::get_infos() const
{
	return _infos;
}
