#include "basic_hitbox.hpp"

#include <iostream>

using namespace glm;
using namespace std;

static const std::vector<BasicHitbox::storedHitbox>  _empty_subhitboxes = {};

BasicHitbox::BasicHitbox(const hbxTransformation& transformation)
	: _parent(NULL),
	  _speed(vec3(0)),
	  _transformation({transformation.translation}),
	  _relative_space_box(spaceBox{transformation.translation, transformation.translation}),
	  _old_dt(0),
	  _abs_space_box(spaceBox{transformation.translation, transformation.translation}),
	  _abs_transformation(_transformation)
{

}

void BasicHitbox::update_dt(float dt) const
{
	_old_dt = dt;
}

const spaceBox& BasicHitbox::get_minmax_coord() const
{
	return this->absolute_space_box();
}

BasicHitbox::storedHitbox BasicHitbox::build_symmetrical(const infinitePlan& sym_plan) const
{
	auto ret = this->copy();
	ret->rotate(geom::symmetrical(this->get_rotation(), sym_plan.normalN));
	ret->tp(geom::symmetrical(this->get_ref_pos(), sym_plan));
	return ret;
}


hitbox_primary::Shape BasicHitbox::get_primary_type() const
{
	return hitbox_primary::NO_PRIMARY;	
}

BasicHitbox::BasicHitbox(const glm::vec3& translation)
	: BasicHitbox(hbxTransformation{.translation = translation, .rotation = glm::mat3(1.0f), .scale = 1.0f})
{

}

const std::vector<BasicHitbox::storedHitbox>& BasicHitbox::subhitboxes() const
{
	return _empty_subhitboxes;
}


std::string BasicHitbox::get_str() const
{
	return "Hbx(" + std::to_string((long int)(this)) + ")";
}


void BasicHitbox::transform_relative(const hbxTransformation& transformation)
{
	#ifdef HITBOX_DEBUG
	cout<<this->get_str()<<"->transform_relative("<<_transformation.to_string()<<")"<<endl;
	#endif

	if(_transformation != transformation)
	{
		_transformation = transformation;
		this->relative_spacebox_invalidate();
		this->invalidate_absolute();
	}
}

float BasicHitbox::get_homothety() const
{
	return _transformation.scale;
}
const glm::vec3& BasicHitbox::get_ref_pos() const
{
	return _transformation.translation;
}

const glm::mat3& BasicHitbox::get_rotation() const
{
	return _transformation.rotation;
}

void BasicHitbox::transform_absolute(const hbxTransformation& transformation)
{
	mot::err("TODO: BasicHitbox::transform_absolute()");
	// It is only a call to transform_relative() with
	// a relative transformation that will result in 
	// the absolute transformation passed as argument
}

void BasicHitbox::invalidate_absolute() const
{
	if(_abs_transformation.updated || _abs_space_box.updated)
	{
		_abs_transformation.updated = false;
		_abs_space_box.updated      = false;

		// printf("COMPUTE VIRTUAL CALL shape_invalidate_absolute() %p\n", this);
		this->shape_invalidate_absolute();
	}
}

void BasicHitbox::relative_spacebox_invalidate() const
{
	#ifdef HITBOX_DEBUG
	cout<<this->get_str()<<"->relative_spacebox_invalidate()"<<endl;
	#endif

	_relative_space_box.updated = false;
	_abs_space_box.updated      = false;

	if(_parent != NULL)
	{
		spaceBox transformed = this->extern_space_box();

		if(!(transformed < _parent->relative_space_box()))
		{
			_parent->relative_spacebox_invalidate();
		}
	}
}

void BasicHitbox::set_speed(const glm::vec3& speed)
{
	_speed = speed;
	this->relative_spacebox_invalidate();
}

void BasicHitbox::tp(const glm::vec3& new_pos)
{
	hbxTransformation nt = _transformation;
	nt.translation = new_pos;

	this->transform_relative(nt);
}

void BasicHitbox::move(const glm::vec3& dp)
{
	hbxTransformation nt = _transformation;
	nt.translation += dp;

	this->transform_relative(nt);
}

void BasicHitbox::homothety(float coeff)
{
	hbxTransformation nt = _transformation;
	nt.scale = coeff;

	this->transform_relative(nt);
}

void BasicHitbox::rotate(const glm::mat3& new_base)
{
	hbxTransformation nt = _transformation;
	nt.rotation = new_base;
	this->transform_relative(nt);
}

// #define HITBOX_SEE_SPACEBOX_USED
// #define HITBOX_SEE_SPACEBOX_USED_COLLISION
// #define HITBOX_SEE_SPACEBOX_USED_IMPACT

#ifdef HITBOX_SEE_SPACEBOX_USED
#define HITBOX_SEE_SPACEBOX_USED_COLLISION
#define HITBOX_SEE_SPACEBOX_USED_IMPACT
#endif

#if defined	 HITBOX_SEE_SPACEBOX_USED_COLLISION || defined HITBOX_SEE_SPACEBOX_USED_IMPACT
#include "debug_draw.hpp"
#endif

bool BasicHitbox::in_collision(const SphereHitbox& hbx) const
{
	#ifdef HITBOX_DEBUG
	cout<<this->get_str()<<"->in_collision((SphereHitbox)"<<((const BasicHitbox&)(hbx)).get_str()<<")"<<endl;
	#endif

	#ifdef HITBOX_SEE_SPACEBOX_USED_COLLISION
	debug_draw::space_box(hbx);
	debug_draw::space_box(*this);
	#endif

	if(!(this->absolute_spacebox_overlap((const BasicHitbox&)(hbx))))
	{
		#ifdef HITBOX_DEBUG
		cout<<"  (in_collision)<No absolute spacebox collisions !>"<<endl;
		#endif

		return false;
	}

	#ifdef HITBOX_DEBUG
	cout<<"  (in_collision)<Absolute spacebox collisions, so call to in_shape_collision("<<((const BasicHitbox&)(hbx)).get_str()<<")>"<<endl;
	#endif

	// printf("COMPUTE VIRTUAL CALL in_shape_collision() %p\n", this);
	return this->in_shape_collision(hbx);
}

bool BasicHitbox::in_collision(const PrismHitbox& hbx)  const
{
	#ifdef HITBOX_DEBUG
	cout<<this->get_str()<<"->in_collision((PrismHitbox)"<<((const BasicHitbox&)(hbx)).get_str()<<")"<<endl;
	#endif

	#ifdef HITBOX_SEE_SPACEBOX_USED_COLLISION
	debug_draw::space_box(hbx);
	debug_draw::space_box(*this);
	#endif

	if(!(this->absolute_spacebox_overlap((const BasicHitbox&)(hbx))))
	{
		#ifdef HITBOX_DEBUG
		cout<<"  (in_collision)<No absolute spacebox collisions !>"<<endl;
		#endif

		return false;
	}

	#ifdef HITBOX_DEBUG
	cout<<"  (in_collision)<Absolute spacebox collisions, so call to in_shape_collision("<<((const BasicHitbox&)(hbx)).get_str()<<")>"<<endl;
	#endif

	// printf("COMPUTE VIRTUAL CALL in_shape_collision() %p\n", this);
	return this->in_shape_collision(hbx);
}

bool BasicHitbox::in_collision(const BasicHitbox& hbx) const
{
	#ifdef HITBOX_DEBUG
	cout<<this->get_str()<<"->in_collision((BasicHitbox)"<<hbx.get_str()<<")"<<endl;
	#endif

	#ifdef HITBOX_SEE_SPACEBOX_USED_COLLISION
	debug_draw::space_box(hbx);
	debug_draw::space_box(*this);
	#endif

	if(!(this->absolute_spacebox_overlap(hbx)))
	{
		#ifdef HITBOX_DEBUG
		cout<<"  (in_collision)<No absolute spacebox collisions !>"<<endl;
		#endif

		return false;
	}

	#ifdef HITBOX_DEBUG
	cout<<"  (in_collision)<Absolute spacebox collisions, so call to in_shape_collision("<<((const BasicHitbox&)(hbx)).get_str()<<")>"<<endl;
	#endif

	// printf("COMPUTE VIRTUAL CALL in_shape_collision() %p\n", this);
	return this->in_shape_collision(hbx);
}

bool BasicHitbox::in_collision(const std::vector<storedHitbox>& hbxs)  const
{
	for(const auto& h : hbxs)
	{
		if(this->in_collision(*h))
		{
			return true;
		}
	}

	return false;
}

const glm::vec3& BasicHitbox::get_speed() const
{
	return _speed;
}

collisionImpact BasicHitbox::impact(const BasicHitbox& hitbox, float dt) const
{
	this->update_dt(dt);
	hitbox.update_dt(dt);

	vec3 composed = (_speed - hitbox.get_speed());

	float d = glm::length(composed);
	if(d <= 0.000001f)
	{
		return this->impact(hitbox, geom::safe_normalize(_speed), 0);
	}

	vec3 composedN = composed/d;

	collisionImpact ret = this->impact(hitbox, composedN, d*dt);

	ret.point = (ret.point - composedN*ret.distance) + _speed/d*ret.distance; 
	return ret;
}

collisionImpact BasicHitbox::impact(const BasicHitbox& hitbox , const glm::vec3& self_directionN, float distance) const
{
	#ifdef HITBOX_DEBUG
	cout<<this->get_str()<<"->impact((BasicHitbox)"<<hitbox.get_str()<<")"<<endl;
	#endif

	#ifdef HITBOX_SEE_SPACEBOX_USED_IMPACT
	debug_draw::space_box(hitbox);
	debug_draw::space_box(*this);
	#endif

	if(!(this->absolute_spacebox_impact(hitbox, self_directionN, distance)))
	{
		return geom::NO_IMPACT;
	}

	// printf("COMPUTE VIRTUAL CALL shape_impact() %p\n", this);
	return this->shape_impact(hitbox, self_directionN, distance);
}

collisionImpact BasicHitbox::impact(const SphereHitbox& hitbox, const glm::vec3& self_directionN, float distance) const
{
	#ifdef HITBOX_DEBUG
	cout<<this->get_str()<<"->impact((SphereHitbox)"<<((const BasicHitbox&)(hitbox)).get_str()<<")"<<endl;
	#endif

	#ifdef HITBOX_SEE_SPACEBOX_USED_IMPACT
	debug_draw::space_box(hitbox);
	debug_draw::space_box(*this);
	#endif

	if(!(this->absolute_spacebox_impact((const BasicHitbox&)(hitbox), self_directionN, distance)))
	{
		return geom::NO_IMPACT;
	}

	// printf("COMPUTE VIRTUAL CALL shape_impact() %p\n", this);
	return this->shape_impact(hitbox, self_directionN, distance);
}

collisionImpact BasicHitbox::impact(const PrismHitbox& hitbox , const glm::vec3& self_directionN, float distance) const
{
	#ifdef HITBOX_DEBUG
	cout<<this->get_str()<<"->impact((PrismHitbox)"<<((const BasicHitbox&)(hitbox)).get_str()<<")"<<endl;
	#endif

	#ifdef HITBOX_SEE_SPACEBOX_USED_IMPACT
	debug_draw::space_box(hitbox);
	debug_draw::space_box(*this);
	#endif

	if(!(this->absolute_spacebox_impact((const BasicHitbox&)(hitbox), self_directionN, distance)))
	{
		return geom::NO_IMPACT;
	}

	// printf("COMPUTE VIRTUAL CALL shape_impact() %p\n", this);
	return this->shape_impact(hitbox, self_directionN, distance);
}

bool BasicHitbox::absolute_spacebox_overlap(const BasicHitbox& hbx) const
{
	spaceBox selfsb = this->absolute_space_box();
	spaceBox target = hbx.absolute_space_box();

	bool ret = geom::space_box_collision(selfsb, target);

	#ifdef HITBOX_DEBUG
	cout<<this->get_str()<<"->absolute_spacebox_overlap((PrismHitbox)"<<hbx.get_str()<<")"<<endl;
	cout<<"  (absolute_spacebox_overlap)<Absolute space boxes are : "<<selfsb.to_string()<<" and "<<target.to_string()<<">"<<endl;
	cout<<"  (absolute_spacebox_overlap)<So collision:"<<int(ret)<<">"<<endl;
	#endif

	return ret;
}

bool BasicHitbox::absolute_spacebox_impact(const BasicHitbox& hbx, const glm::vec3& self_directionN, float distance) const
{
	return geom::space_box_collision(geom::moving_spacebox(this->absolute_space_box(), self_directionN*distance), hbx.absolute_space_box());
}

spaceBox BasicHitbox::compute_absolute_space_box() const
{
	const hbxTransformation & abstrans  = this->abs_transformation();
	const spaceBox          & rspacebox = this->relative_space_box();
	
	spaceBox transformed = abstrans.transform(rspacebox);

	#ifdef HITBOX_DEBUG
	cout<<this->get_str()<<"->compute_absolute_space_box()"<<endl;
	cout<<"  (compute_absolute_space_box)<Absolute transormation is "<<abstrans.to_string()<<">"<<endl;
	cout<<"  (compute_absolute_space_box)<Relative spacebox is "<<rspacebox.to_string()<<">"<<endl;
	cout<<"  (compute_absolute_space_box)<So absolute spacebox is "<<transformed.to_string()<<">"<<endl;
	#endif

	return transformed;
}

hbxTransformation BasicHitbox::compute_absolute_transform() const
{
	#ifdef HITBOX_DEBUG
	cout<<this->get_str()<<"->compute_absolute_transform()"<<endl;
	#endif

	if(_parent == NULL)
	{
		return this->transformation();
	}
	else
	{
		return (_parent->abs_transformation()) + (this->transformation());
	}
}


// Returns the space box with abslute coords
const spaceBox& BasicHitbox::absolute_space_box() const
{
	if(_abs_space_box.updated == false)
	{
		// printf("COMPUTING ABSOLUTE SPACEBOX %p)\n", this);

		_abs_space_box = this->compute_absolute_space_box();
		_abs_space_box.updated = true;
	}

	return _abs_space_box;
}

// Returns the space box relatively to the parent hbx
spaceBox BasicHitbox::extern_space_box() const
{
	return this->transformation().transform(this->relative_space_box());
}

// Returns the space box relatively to the parent hbx but without its transformation
const spaceBox& BasicHitbox::relative_space_box() const
{
	#ifdef HITBOX_DEBUG
	cout<<this->get_str()<<"->relative_space_box() ";
	#endif

	if(_relative_space_box.updated == false)
	{
		#ifdef HITBOX_DEBUG
		cout<<"<Not up to date, so call to compute_shape_relative_space_box()"<<endl;
		#endif

		// printf("VIRTUAL CALL compute_shape_relative_space_box() %p)\n", this);

		_relative_space_box = geom::moving_spacebox(this->compute_shape_relative_space_box(), _speed*_old_dt);
		_relative_space_box.updated = true;
	}
	else
	{
		#ifdef HITBOX_DEBUG
		cout<<"<Already up to date>"<<endl;;
		#endif
	}

	return _relative_space_box;
}
// Returns the transformation relatively to the parent hbx
const hbxTransformation& BasicHitbox::transformation()     const
{
	return _transformation;
}
// Returns the absolute transformation
const hbxTransformation& BasicHitbox::abs_transformation() const
{
	if(_abs_transformation.updated == false)
	{
		// printf("COMPUTING ABSOLUTE TRANSFORMATION %p)\n", this);
		
		_abs_transformation = this->compute_absolute_transform();
		_abs_transformation.updated = true;
	}

	return _abs_transformation;
}

void BasicHitbox::attach_parent(const BasicHitbox* parent)
{
	_parent = parent;
}

