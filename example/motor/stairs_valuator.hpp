#ifndef _STAIRS_VALUATOR_HPP_
#define _STAIRS_VALUATOR_HPP_

#include "basic_timed_valuator.hpp"

template<typename T>
class StairsValuator : public BasicTimedValuator<T>
{
public:
	StairsValuator();

protected:
	T normal_evaluate(uint index, float percentage) const override;

};




template<typename T>
StairsValuator<T>::StairsValuator()
	: BasicTimedValuator<T>()
{

}

template<typename T>
T StairsValuator<T>::normal_evaluate(uint index, float percentage) const
{	
	return this->indexed_value(index + 0).value;
}



#endif //_STAIRS_VALUATOR_HPP_
