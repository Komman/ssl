#include "dynamic_lightor.hpp"

#include <glm/gtx/string_cast.hpp>

using namespace std;
using namespace glm;
	
DynamicLightor::DynamicLightor(const std::string& name, uint max_size)
	: BasicDynamicLightor(),
	  _name(name),
	  _positions_saturation(UNIFORM_PRE_NAME + name + POSITIONS_ADD_NAME, max_size, {}),
	  _colors_intens(UNIFORM_PRE_NAME + name + COLORS_INTENS_ADD_NAME, max_size, {}),
	  _indices_owner(max_size, -1),
	  _lights_indices(max_size, -1),
	  _available_indices(),
	  _data_has_changed(true)
{
	_available_indices.reserve(max_size);
	for(uint i=0; i<max_size; i++)
	{
		_available_indices.push_back(i);
	}
}	

void DynamicLightor::pre_error() const
{
	cout<<TERM::RED<<"Planning to crash very soon ! A litle log message juste before:"<<TERM::NOCOL<<endl;
	this->print();
}


void DynamicLightor::check_sizes() const
{
	bool problem =   (_positions_saturation.size() != _colors_intens.size())
				  || ((int)_available_indices.size() != (int)_positions_saturation.max_size() - (int)_positions_saturation.size())
				  || _positions_saturation.max_size() != _lights_indices.size();

	#ifdef SSL_EXPENSIVE_DEBUG
	uint lastm1 = 0;
	for(lastm1=0; lastm1<_indices_owner.size();lastm1++)
	{
		if(_indices_owner[lastm1] == -1)
		{
			break;
		}
		if(_indices_owner[lastm1] >= (int)_lights_indices.size())
		{
			this->pre_error();
			err("DynamicLightor::check_sizes(): Index out of bond in _indices_owner");
		}
		if(_lights_indices[_indices_owner[lastm1]] == -1)
		{
			this->pre_error();
			err("DynamicLightor::check_sizes(): An owner isn't owned in _indices_owner");
		}
	}

	if(lastm1 != _positions_saturation.size())
	{
		this->pre_error();

		err("DynamicLightor::check_sizes(): _indices_owner doesnt own nicely the positions:\n_positions_saturation.size() = "
			+ std::to_string(_positions_saturation.size()) + " and the size of the owner covering is "
			+ std::to_string(lastm1));
	}
	#endif

	#ifdef SSL_EXPENSIVE_DEBUG
	for(auto i : _available_indices)
	{
		if(i >= _lights_indices.size())
		{
			this->pre_error();
			err("DynamicLightor::check_sizes(): Wrong index in _available_indices");
		}
		if(_lights_indices[i] != -1)
		{
			this->pre_error();
			err("DynamicLightor::check_sizes(): An available index is not available");
		}
	}
	#endif

	#ifdef SSL_EXPENSIVE_DEBUG
	vector<bool> allright(_positions_saturation.size(), false);
	for(auto x : _lights_indices)
	{
		if(x != -1)
		{
			if(x >= (int)_positions_saturation.size())
			{
				this->pre_error();
				err("DynamicLightor::check_sizes(): Index out of bound in _lights_indices");
			}
			if(allright[x])
			{
				this->pre_error();
				err("DynamicLightor::check_sizes(): Two same indices in _lights_indices");
			}
			allright[x] = true;
		}
	}
	for(auto v : allright)
	{
		if(!v)
		{
			this->pre_error();
			err("DynamicLightor::check_sizes():  index in _lights_indices");
		}
	}
	#endif

	if(problem)
	{	
		this->pre_error();

		err(std::string("DynamicLightor::check_sizes() : Sizes error:")
			+ "\n_positions_saturation.size() = " + to_string(_positions_saturation.size())
			+ "\n_positions_saturation.max_size() = " + to_string(_positions_saturation.max_size())
			+ "\n_colors_intens.size() = " + to_string(_colors_intens.size())
			+ "\n_available_indices.size() = " + to_string(_available_indices.size()));
	}
}


uint DynamicLightor::max_size() const
{
	#ifdef SSL_DEBUG
	this->check_sizes();
	#endif
	
	return _positions_saturation.max_size();
}

uint DynamicLightor::size()     const
{
	#ifdef SSL_DEBUG
	this->check_sizes();
	#endif
	
	return _positions_saturation.size();
}

glm::vec4 DynamicLightor::pack_color_intens(const glm::vec3& color, float intensity)
{
	return vec4(color, intensity);
}

glm::vec4 DynamicLightor::pack_position_saturation(const glm::vec3& pos, float saturation)
{
	return vec4(pos, saturation);
}


uint DynamicLightor::add_light(const dynamicLight& light)
{
	#ifdef SSL_DEBUG
	this->check_sizes();
	#endif
	
	if(_available_indices.size() == 0 || _positions_saturation.size() == _positions_saturation.max_size())
	{
		err("DynamicLightor::add_light(): not more lights slots availables");
	}

	uint index = _available_indices[_available_indices.size() - 1];
	_available_indices.pop_back();

	#ifdef SSL_DEBUG
	if(_lights_indices[index] != -1)
	{
		err("DynamicLightor::add_light(): try to add on a non-available slot");
	}
	if(_indices_owner[_positions_saturation.size()] != -1)
	{
		err("DynamicLightor::add_light(): try to add on a slot that has already an owner");
	}
	#endif

	_lights_indices[index] = _positions_saturation.size();
	_indices_owner[_positions_saturation.size()] = index;

	_positions_saturation.add_value(pack_position_saturation(light.position, light.saturation));
	_colors_intens.add_value(pack_color_intens(light.color, light.intensity));


	#ifdef SSL_DEBUG
	this->check_sizes();
	#endif

	_data_has_changed = true;

	return index;
}

void DynamicLightor::remove_light(uint index)
{
	#ifdef SSL_DEBUG
	this->check_sizes();
	#endif
	
	if(index >= _indices_owner.size())
	{
		err("DynamicLightor::remove_light(): index greater than max size");
	}

	if(_lights_indices[index] < (int)_positions_saturation.size() - 1)
	{
		_positions_saturation.set_value(_lights_indices[index], _positions_saturation[_positions_saturation.size() - 1]);
		_colors_intens.set_value(_lights_indices[index], _colors_intens[_colors_intens.size() - 1]);
	
		_indices_owner[_lights_indices[index]] = _indices_owner[_positions_saturation.size() - 1];
		_lights_indices[_indices_owner[_positions_saturation.size() - 1]] = _lights_indices[index];
	}

	_indices_owner[_positions_saturation.size() - 1] = -1;
	_lights_indices[index] = -1;
	_available_indices.push_back(index);

	_positions_saturation.pop_value();
	_colors_intens.pop_value();

	#ifdef SSL_DEBUG
	this->check_sizes();
	#endif

	_data_has_changed = true;
}

void DynamicLightor::check_index(uint index) const
{
	if(index >= _lights_indices.size() || _lights_indices[index] == -1)
	{
		err("DynamicLightor::check_index(): try to access to an non-existing light");
	}
}


void DynamicLightor::set_light_light(uint index, const dynamicLight& light)
{
	this->check_index(index);

	_positions_saturation.set_value(_lights_indices[index], pack_position_saturation(light.position, light.saturation));
	_colors_intens.set_value(_lights_indices[index], pack_color_intens(light.color, light.intensity));
	
	_data_has_changed = true;
}

void DynamicLightor::set_light_position(uint index, const glm::vec3& position)
{
	this->check_index(index);

	_positions_saturation.set_value(_lights_indices[index], pack_position_saturation(position, _positions_saturation[_lights_indices[index]].w));
	
	_data_has_changed = true;
}

void DynamicLightor::set_light_color(uint index, const glm::vec3& color)
{
	this->check_index(index);

	_colors_intens.set_value(_lights_indices[index], pack_color_intens(color, _colors_intens[_lights_indices[index]].w));

	_data_has_changed = true;
}

void DynamicLightor::set_light_intensity(uint index, float intensity)
{
	this->check_index(index);

	vec3 color = vec3(
			_colors_intens[_lights_indices[index]].x,
			_colors_intens[_lights_indices[index]].y,
			_colors_intens[_lights_indices[index]].z
		);
	_colors_intens.set_value(_lights_indices[index], pack_color_intens(color, intensity));

	_data_has_changed = true;
}

void DynamicLightor::set_light_saturation(uint index, float saturation)
{
	this->check_index(index);

	vec3 pos = vec3(
			_positions_saturation[_lights_indices[index]].x,
			_positions_saturation[_lights_indices[index]].y,
			_positions_saturation[_lights_indices[index]].z
		);
	_positions_saturation.set_value(_lights_indices[index], pack_position_saturation(pos, saturation));

	_data_has_changed = true;
}

void DynamicLightor::print() const
{
	cout<<"DynamicLightor: "<<TERM::ORANGE<<_name<<TERM::NOCOL<<":\n{"<<endl;
	cout<<" "<<"_lights_indices:"; utils::print_vector(_lights_indices); cout<<endl;
	cout<<" "<<"_available_indices:"; utils::print_vector(_available_indices); cout<<endl;

	cout<<" lights: {"<<endl;
	for(uint i=0; i<_positions_saturation.size(); i++)
	{
		vec3 pos = vec3(
			_positions_saturation[i].x,
			_positions_saturation[i].y,
			_positions_saturation[i].z
		);

		cout<<"  (pos:"<<glm::to_string(pos)<<", col:";

		vec3 color = vec3(
			_colors_intens[i].x,
			_colors_intens[i].y,
			_colors_intens[i].z
		);
		cout<<glm::to_string(color)<<", intens:"<<_colors_intens[i].w<<endl;
		cout<<", saturation:"<<_positions_saturation[i].w<<")"<<endl;
	}
	cout<<" }"<<endl;
	cout<<" "<<"_indices_owner:"; utils::print_vector(_indices_owner); cout<<endl<<"}"<<endl;
}

void DynamicLightor::update_GPU()
{
	if(_data_has_changed)
	{
		_positions_saturation.update_GPU();
		_colors_intens.update_GPU();

		_data_has_changed = false;
	}
}

float DynamicLightor::get_light_intensity(uint index) const
{
	this->check_index(index);

	return _colors_intens[_lights_indices[index]].w;
}

dynamicLight dynamicLight::operator*(float x)
{
	dynamicLight ret;

	ret.position   = this->position   * x;
	ret.color      = this->color      * x;
	ret.intensity  = this->intensity  * x;
	ret.saturation = this->saturation * x;

	return ret;
};

dynamicLight dynamicLight::operator+(const dynamicLight& l)
{
	dynamicLight ret;
	
	ret.position   = this->position   + l.position  ;
	ret.color      = this->color      + l.color     ;
	ret.intensity  = this->intensity  + l.intensity ;
	ret.saturation = this->saturation + l.saturation;

	return ret;
};
