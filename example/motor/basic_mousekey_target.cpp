#include "basic_mousekey_target.hpp"

#include "../../ssl/src/window.hpp"

#include <iostream>

BasicMousekeyTarget::BasicMousekeyTarget(const BasicMousekeyTarget* const* switcher)
	: _switcher(switcher)
{

}

bool BasicMousekeyTarget::enable() const
{
	return ((*_switcher) == this);
}

const BasicMousekeyTarget* const* BasicMousekeyTarget::get_switcher() const
{
	return _switcher;
}

void BasicMousekeyTarget::enter()
{
	ssl::mouse::set_visibility(true);
}

void BasicMousekeyTarget::exit()
{

}

void BasicMousekeyTarget::animate(float dt)
{
	
}

bool BasicMousekeyTarget::key_pressed(int key) const
{
	return (this->enable() && ssl::keyboard::key_pressed(key));
}

