#ifndef _DYNAMIC_LIGHT_HPP_
#define _DYNAMIC_LIGHT_HPP_

#include "dynamic_lightor.hpp"

class DynamicLight
{
public:
	DynamicLight(BasicDynamicLightor& lightor, const dynamicLight& light);
	~DynamicLight();

	DynamicLight(DynamicLight&& light);
	DynamicLight& operator=(DynamicLight&& light);

	void set_light(const dynamicLight& light);
	void set_position(const glm::vec3& position);
	void set_color(const glm::vec3& color);
	void set_intensity(float intensity);

	float get_intensity();

	DynamicLight& operator=(const dynamicLight& light);

	void destroy();
	bool is_destroyed();

	uint get_lightor_index() const;

private:
	DynamicLight& operator=(const DynamicLight&);
	DynamicLight(const DynamicLight&);

	void check_destroy_before_use();

private:
	BasicDynamicLightor& _lightor;
	uint                 _lightor_index;
	
	bool _destroyed;
};

#endif //_DYNAMIC_LIGHT_HPP_
