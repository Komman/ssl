#ifndef _PLANAR_BUILDING_HPP_
#define _PLANAR_BUILDING_HPP_

#include "../utils/space_grid.hpp"
#include "../utils/geom.hpp"

struct planarPlace
{
	glm::vec2 center;
	glm::vec2 size;
	glm::vec2 dirN;
};

struct planarYPlace
{
	glm::vec3 center;
	glm::vec3 size;
	glm::vec2 dirN;
};

struct valuedCircleInfo
{
	circleInfo circle;
	int        value;
};	

struct valuedPolygon2DInfo
{
	polygon2DInfo polygon;
	int           value;
};	

circleInfo tranform_relative_2D(const circleInfo& circle, const planarPlace& relative);
polygon2DInfo tranform_relative_2D(const polygon2DInfo& polygon, const planarPlace& relative);

class PlanarBuilding
{
public:
	PlanarBuilding(const std::vector<valuedCircleInfo>& circles, const std::vector<valuedPolygon2DInfo>& polygons);

	// Circle doesn't support different sizing between .x and .y 
	void add_circles(const std::vector<valuedCircleInfo>& circles);
	void add_polygon(const std::vector<valuedPolygon2DInfo>& polygons);

	// the planarPlace.dirN is the Z-direction
	bool is_placable(SpaceGrid& spacegrid, const planarPlace& p);
	void place(SpaceGrid& spacegrid, const planarPlace& p);

private:
	std::vector<valuedCircleInfo>    _circles;
	std::vector<valuedPolygon2DInfo> _polygons;
};

#endif //_PLANAR_BUILDING_HPP_
