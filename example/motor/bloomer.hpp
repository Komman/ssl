#ifndef _BLOOMER_HPP_
#define _BLOOMER_HPP_

#include "../../ssl/ssl.hpp"

using namespace ssl;

struct bloomParameter
{
	int   big_bloom_kernel; 
    int   little_bloom_kernel; 
    float little_bloom_proportion;
};

class Bloomer
{
public:
	Bloomer(UniformTexture2D<glm::vec4>& bloom_in_texture,
			const std::string& prename,
			const std::string& final_bloom_texture_name,
			const bloomParameter& parameters);

	const UniformTexture2D<glm::vec4>& bloom_texture() const;
	const UniformTexture2D<glm::vec4>& processed_tetxure() const;

	void bloom_enable();
	void bloom_disable();
	void clean_textures();

	void bloom();

	void change_little_coeff(float proportion);
	void change_little_kernel(int new_kernel);
	void change_big_kernel(int new_kernel);

	float little_coeff();
	int little_kernel();
	int big_kernel();

private:
	void init_bloom_pass();
	void end_bloom_pass();
	void downscale_pass(const UniformTexture2D<glm::vec4>& src_texture,
				   		const FrameBuffer<glm::vec4>& dst_framebuffer);
	void blur_pass(const UniformTexture2D<glm::vec4>& src_texture,
				   const FrameBuffer<glm::vec4>& dst_framebuffer,
				   bool vertical);

private:

	ShaderGenerator _gen;

	Uniform<int>   _kernel;
	Uniform<int>   _downsclae_divisor;
	Uniform<int>   _compute_kernel;
	Uniform<float> _color_mutiplier;

	UniformTexture2D<glm::vec4>& _texture;
	UniformTexture2D<glm::vec4>  _downscaled_even_texture;
	UniformTexture2D<glm::vec4>  _downscaled_odd_texture;
	UniformTexture2D<glm::vec4>  _little_processed_texture;
	UniformTexture2D<glm::vec4>  _processed_texture;

	Drawer _downscaler;
	Drawer _cleaner;

	FrameBuffer<glm::vec4> _downscale_even;
	FrameBuffer<glm::vec4> _downscale_odd;
	FrameBuffer<glm::vec4> _texture_framebuffer;
	FrameBuffer<glm::vec4> _little_processed_fb;
	FrameBuffer<glm::vec4> _processed_fb;

	textureBinding _downscaled_tetxure_binding;

	int   _big_kernel;
	int   _little_kernel;
	int   _downscale_count;
	float _little_coeff;

	bool _enable;
};

#endif //_BLOOMER_HPP_
