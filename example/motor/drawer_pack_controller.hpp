#ifndef _DRAWER_PACK_CONTROLLER_HPP_
#define _DRAWER_PACK_CONTROLLER_HPP_

#include "../../ssl/ssl.hpp"

using namespace ssl;

class DrawerPackController
{
public:
	DrawerPackController();

	// Returns the index of the added drawer
	unsigned int add_drawer(Drawer* drawer);
	// Returns the poped drawer
	Drawer*      pop_drawer(unsigned int index);

	void clear();
	unsigned int size();


	// Control Functions

	void depth(bool enable);
	void culling(bool enable);
	void blending(bool enable);
	void custom_view_port(const glm::uvec2& origin, const glm::uvec2& size);
	void disable_custom_view_port();
	void set_blend_function(GLenum sfactor = global_drawing::DEFAULT::SFACTOR,
							GLenum dfactor = global_drawing::DEFAULT::DFACTOR); 
	void set_blend_equation(GLenum mode); 
	void depth_mask(bool enable);
	void depth_function(GLenum func);
	void set_stencil_params(const Drawer::stencilParams& params);
	void stencil_testing(bool enable);
	void stencil_mask(GLuint mask); // 8 bits
	void stencil_function(GLenum func, GLint ref, GLuint mask = 0xFF);
	void stencil_op(GLenum sfail, GLenum dpfail, GLenum dppass);



	// Drawing Control Functions

	template<typename... Buffers>
	void draw(const Buffers & ... buff);

	template<typename... Buffers>
	void draw_elements(const ElementBuffer& elements, const Buffers & ... buff);

	template<typename... Buffers>
	void draw(const BasicFrameBuffer& target_fb,
			  const Buffers & ... buff);
	template<typename... Buffers>
	void draw_elements(const BasicFrameBuffer& target_fb,
					   const ElementBuffer& elements,
					   const Buffers & ... buff);

	void draw_object(const DrawableObject& obj);
	void draw_object(const ElementDrawableObject& obj);
	void draw_object(const BasicFrameBuffer& target_fb, const DrawableObject& obj);
	void draw_object(const BasicFrameBuffer& target_fb, const ElementDrawableObject& obj);


	template<typename... Buffers>
	void draw_elements_in_framebuffer(const BasicFrameBuffer& target_fb,
									  const ElementBuffer& elements,
									  const Buffers & ... buff);

	template<typename... Buffers>
	void draw_in_framebuffer(const BasicFrameBuffer& target_fb,
							 const Buffers & ... buff);

private:
	std::vector<Drawer*> _drawers;
};







template<typename... Buffers>
void DrawerPackController::draw(const Buffers & ... buff)
{
	for(auto d : _drawers)
	{
		d->draw(buff...);
	}
}

template<typename... Buffers>
void DrawerPackController::draw_elements(const ElementBuffer& elements, const Buffers & ... buff)
{
	for(auto d : _drawers)
	{
		d->draw_elements(elements, buff...);
	}
}

template<typename... Buffers>
void DrawerPackController::draw(const BasicFrameBuffer& target_fb,
		  						const Buffers & ... buff)
{
	for(auto d : _drawers)
	{
		d->draw(target_fb, buff...);
	}
}

template<typename... Buffers>
void DrawerPackController::draw_elements(const BasicFrameBuffer& target_fb,
				   						 const ElementBuffer& elements,
				   						 const Buffers & ... buff)
{
	for(auto d : _drawers)
	{
		d->draw_elements(target_fb, elements, buff...);
	}
}

template<typename... Buffers>
void DrawerPackController::draw_elements_in_framebuffer(const BasicFrameBuffer& target_fb,
								  						const ElementBuffer& elements,
								  						const Buffers & ... buff)
{
	for(auto d : _drawers)
	{
		d->draw_elements_in_framebuffer(target_fb, elements, buff...);
	}
}

template<typename... Buffers>
void DrawerPackController::draw_in_framebuffer(const BasicFrameBuffer& target_fb,
						 					   const Buffers & ... buff)
{
	for(auto d : _drawers)
	{
		d->draw_in_framebuffer(target_fb, buff...);
	}
}

#endif //_DRAWER_PACK_CONTROLLER_HPP_
