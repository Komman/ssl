#include "wall_hitbox.hpp"

#include "bevel_hitbox.hpp"

#include "../utils/geom.hpp"
#include "../utils/mutils.hpp"

using namespace glm;
using namespace std;

WallHitbox::WallHitbox(const wallInfo& shape)
	: ShapedHitboxFusion(shape)
{
	this->build();
}

void WallHitbox::build()
{
	const auto& wall = this->get_shape();
	std::vector<glm::vec3> excycle = wall.excycle;
	std::vector<glm::vec3> incycle = wall.incycle;


	std::vector<vec3> points = excycle;
	for(const auto& p : incycle)
	{
		points.push_back(p);
	}

	std::vector<triIndex> triangles = geom::rubban_triangulation(points,
		utils::uirange(0, excycle.size()-1),
		utils::uirange(excycle.size(), points.size()-1)
	);

	for(const triIndex& tri : triangles)
	{
		this->fusion_hitbox<BevelHitbox>(bevelShape{
			.triangle_base   = {
				points[tri.i1],
				points[tri.i2],
				points[tri.i3]
		   	},
			.sized_direction = wall.sized_direction
		});
	}
}