#ifndef _PHYSIC_HPP_
#define _PHYSIC_HPP_

#include <glm/glm.hpp>

namespace physic
{
	struct shootingDirection
	{
		bool possible;
		glm::vec3 direct_directionN;
		glm::vec3 bell_directionN;
	};

	shootingDirection compute_shot_direction(
			const glm::vec3& position,
			const glm::vec3& target,
			float initial_speed,
			float gravity);
	
};

#endif //_PHYSIC_HPP_
