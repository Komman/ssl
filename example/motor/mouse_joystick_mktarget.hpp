#ifndef _MOUSE_JOYSTICK_MKTARGET_HPP_
#define _MOUSE_JOYSTICK_MKTARGET_HPP_

#include <glm/glm.hpp>
#include "basic_mousekey_target.hpp"

class MouseJoystickMKTarget : public BasicMousekeyTarget
{
public:
	MouseJoystickMKTarget(const BasicMousekeyTarget* const* switcher);

	void enter() override;
	void exit() override;

	void animate(float dt) override;
	const glm::dvec2& frame_move() const;

private:
	glm::dvec2 _set_mouse_pixel;
	glm::dvec2 _frame_move;
};

#endif //_MOUSE_JOYSTICK_MKTARGET_HPP_
