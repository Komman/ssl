#ifndef _UNIFORM_TEXTURED_VECTOR_HPP_
#define _UNIFORM_TEXTURED_VECTOR_HPP_

#include "../../ssl/ssl.hpp"
using namespace ssl;


template<typename Type>
class UniformTexturedVector
{
public:
	UniformTexturedVector(const std::string& name,
						  uint max_size,
						  const std::vector<Type>& initial_data = {});

	// Returns the index
	uint add_value(const Type& value);
	void pop_value();

	void set_value(uint index, const Type& value);
	void set_data(const std::vector<Type>& data);

	void clear();
	//TOOPT: Change the texture on a specific range
	void update_GPU();

	uint max_size() const;
	uint size() const;
	const std::vector<Type>& get_data() const;
	const Type& get_value(uint index)   const;
	const Type& operator[](uint index)  const; 

public:
	static inline std::string SIZE_ADD_NAME = "_size";

private:
	UniformTexture1D<Type> _texture;
	Uniform<int>           _uniform_size;
	std::vector<Type>      _data;
};


template<typename Type>
UniformTexturedVector<Type>::UniformTexturedVector(const std::string& name,
					  uint max_size,
					  const std::vector<Type>& initial_data)
	: _texture(name, max_size, GL_NEAREST, GL_REPEAT),
	  _uniform_size(name + SIZE_ADD_NAME, initial_data.size()),
	  _data(initial_data)
{

}

template<typename Type>
uint UniformTexturedVector<Type>::add_value(const Type& value)
{
	uint ret = _data.size();
	_data.push_back(value);

	return ret;
}

template<typename Type>
void UniformTexturedVector<Type>::pop_value()
{
	_data.pop_back();
}

template<typename Type>
void UniformTexturedVector<Type>::set_value(uint index, const Type& value)
{
	#ifdef SSL_DEBUG
	if(index >= _data.size())
	{
		ssl::err("UniformTexturedVector<Type>::set_value(): index out of bound: "
			     + std::to_string(index) + "/" + std::to_string(_data.size()));
	}
	#endif

	_data[index] = value;
}

template<typename Type>
void UniformTexturedVector<Type>::set_data(const std::vector<Type>& data)
{
	_data = data;
}

template<typename Type>
void UniformTexturedVector<Type>::clear()
{
	_data.clear();
}

template<typename Type>
void UniformTexturedVector<Type>::update_GPU()
{
	_texture.change_data(_data);
	_uniform_size = _data.size();
}


template<typename Type>
const std::vector<Type>& UniformTexturedVector<Type>::get_data() const
{
	return _data;
}

template<typename Type>
const Type& UniformTexturedVector<Type>::get_value(uint index) const
{
	#ifdef SSL_DEBUG
	if(index >= _data.size())
	{
		ssl::err("UniformTexturedVector<Type>::get_value(): index out of bound : " 
			     + std::to_string(index) + "/" + std::to_string(_data.size()));
	}
	#endif

	return _data[index];
}

template<typename Type>
const Type& UniformTexturedVector<Type>::operator[](uint index) const
{
	return this->get_value(index);
}

template<typename Type>
uint UniformTexturedVector<Type>::max_size() const
{
	return _texture.size();
}

template<typename Type>
uint UniformTexturedVector<Type>::size() const
{
	return _data.size();
}



#endif //_UNIFORM_TEXTURED_VECTOR_HPP_
