#include "basic_console.hpp"

#include <iomanip>
#include <sstream>

#include "motor_debug.hpp"
#include "../utils/mutils.hpp"

using namespace std;

BasicConsole::BasicConsole(float msg_lasting)
	: _time(0),
	  _lasting(msg_lasting),
	  _oldest_index(0)
{

}

std::vector<std::string> BasicConsole::msgs() const
{
	std::vector<std::string> ret;

	for(uint i=this->oldest_msg_index(); i<_msgs.size(); i++)
	{
		std::stringstream stream;
		stream << std::fixed << std::setprecision(2) << _msgs[i].time_in;
		
		ret.push_back("[" + stream.str() + "]" + _msgs[i].msg);
	}

	utils::invert(ret);
	return this->msgs_ranged(this->oldest_msg_index(), _msgs.size());
}

std::vector<std::string> BasicConsole::msgs_ranged(uint startmsg, uint endmsg) const
{
	std::vector<std::string> ret;

	for(uint i=startmsg; i<endmsg; i++)
	{
		std::stringstream stream;
		stream << std::fixed << std::setprecision(2) << _msgs[i].time_in;
		
		ret.push_back("[" + stream.str() + "]" + _msgs[i].msg);
	}

	utils::invert(ret);
	return ret;
}

void BasicConsole::animate(float dt)
{
	_time += (double)(dt);

	if(_msgs.size() > 0)
	{
		// cout<<_msgs[_oldest_index].time_in + _lasting<<" vs "<<_time<<endl;
		while((_oldest_index < _msgs.size()) && (_msgs[_oldest_index].time_in + _lasting < _time))
		{
			_oldest_index++;
		}
	}
}

void BasicConsole::send_msg(const std::string& msg)
{
	this->add_msg(msg);

	std::string resp = this->response(msg);

	if(resp != "")
	{
		this->add_msg(resp);
		_msgs[_msgs.size()-1].responded = true;
	}
}

uint BasicConsole::oldest_msg_index() const
{
	return _oldest_index;
}

std::string BasicConsole::last_sent_msg(uint old_index) const
{
	string ret = "";
	int i=_msgs.size();
	while(i > 0)
	{
		i--;

		if(!(_msgs[i].responded))
		{
			ret = _msgs[i].msg;
			
			if(old_index == 0)
			{
				break;
			}
			else
			{
				old_index--;
			}
		}
	}

	return ret;
}

void BasicConsole::add_msg(const std::string& msg)
{
	_msgs.push_back(consoleMsg{
		.time_in = (float)(_time),
		.msg     = msg,
		.responded = false
	});
}

void BasicConsole::print_ranged(uint startmsg, uint endmsg) const
{
	if(startmsg > endmsg || endmsg >= _msgs.size())
	{
		mot::err("BasicConsole::print_ranged(): wrong indices : "
			 + to_string(startmsg) + " -> " + to_string(endmsg));
	}

	for(uint i=startmsg; i<=endmsg; i++)
	{
		const auto& m = _msgs[i]; 
		cout<<"["<<std::setprecision(2)<<std::fixed<<m.time_in<<"]: "<<m.msg<<endl;
	}
}

void BasicConsole::print_all() const
{
	if(_msgs.size() == 0 )
		return;

	this->print_ranged(0, _msgs.size()-1);
}


const std::vector<consoleMsg> BasicConsole::all_msgs() const
{
	return _msgs;
}

std::string BasicConsole::response(const std::string& msg)
{
	return "";
}

void BasicConsole::print() const
{
	if(_msgs.size() == 0 || _oldest_index == _msgs.size())
		return;

	this->print_ranged(this->oldest_msg_index(), _msgs.size()-1);
}


