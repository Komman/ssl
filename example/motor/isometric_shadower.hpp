#ifndef _ISOMETRIC_SHADOWER_HPP_
#define _ISOMETRIC_SHADOWER_HPP_

#include <string>
#include <memory>
#include <glm/glm.hpp>

#include "../../ssl/src/frame_buffer.hpp"
#include "../../ssl/src/array_buffer.hpp"
#include "../../ssl/src/uniform.hpp"
#include "../../ssl/src/drawer.hpp"
#include "../../ssl/src/shader_generator.hpp"

using namespace ssl;

class IsometricShadower
{
public:
	static const inline std::string TEXTURE_ADDNAME  = "_texture";
	static const inline std::string SOURCE_ADDNAME   = "_source";
	static const inline std::string FURTHEST_ADDNAME = "_furthest";
	static const inline std::string TRIIDFOR_ADDNAME = "_triangle_indentifor";
	static const inline std::string PLANSIZE_ADDNAME = "_plansize";

	/*
		If strategy is for shadow antialiasing:
		- PRIMITIVE_ID: triangles will be grouped by group of STRAGEY_ARG
		- CONSTANT: there will be only on triangle group for the draw call,
		            the strategy arg is ignored
	*/

	enum triangleIdStrategy {PRIMITIVE_ID=0, CONSTANT};
 
public:
	IsometricShadower(const std::string& name,
					  const glm::uvec2&  texture_size,
					  const glm::vec3&   source_start,
					  const glm::vec3&   furthest_point,
					  const glm::vec2&   plansize);

	void clear();
	void replace_source(const glm::vec3& source_start,
					    const glm::vec3& furthest_point,
					    const glm::vec2& plansize);
	void compute_triangle_antialiasing();

	float isometric_depth(const glm::vec3& pos);

	void add_mesh(const ArrayBuffer<glm::vec3>& mesh,
				  const ElementBuffer& elements,
				  triangleIdStrategy strategy,
				  int strategy_arg);
	void add_instanced_mesh(const ElementBuffer& elements,
							const ArrayBuffer<glm::vec3>& mesh,
							const ArrayBuffer<glm::vec3>& instances_dep,
							triangleIdStrategy strategy,
							int strategy_arg);
	void add_instanced_planar_mesh(const ElementBuffer& elements,
								   const ArrayBuffer<glm::vec3>& mesh,
								   const ArrayBuffer<glm::vec4>& pos_height,
								   const ArrayBuffer<glm::vec4>& dir2D_sizexz,
								   triangleIdStrategy strategy,
								   int strategy_arg);
	void add_instanced_O3L1_mesh(const ElementBuffer& elements,
								 const ArrayBuffer<glm::vec3>& mesh,
								 const ArrayBuffer<glm::vec4>& dep_height,
								 const ArrayBuffer<glm::vec4>& orientation3DN_length,
								 triangleIdStrategy strategy,
								 int strategy_arg);
	void add_instanced_tower_mesh(const ElementBuffer& elements,
								  const ArrayBuffer<glm::vec3>& mesh,
								  const ArrayBuffer<glm::vec4>& dep_height,
								  const ArrayBuffer<float>& size,
								  triangleIdStrategy strategy,
								  int strategy_arg);
	void add_instanced_O3tower_mesh(const ElementBuffer& elements,
								    const ArrayBuffer<glm::vec3>& mesh,
								    const ArrayBuffer<glm::vec4>& dep_height,
								    const ArrayBuffer<glm::vec4>& orientation3DN_size,
								    triangleIdStrategy strategy,
								    int strategy_arg);

	const UniformTexture2D<glm::vec4>& get_coordinates_id_texture() const;

private:
	UniformTexture2D<glm::vec4> _coordinates_id;

	FrameBuffer<glm::vec4> _framebuffer;

	Uniform<glm::ivec3> _triangle_indentifor; //x: absolute dec, y: (0: use gl_PrimitiveID, 1: use constant), z: depend of x 
	Uniform<glm::vec3> _source_start;
	Uniform<glm::vec3> _furthest_point;
	Uniform<glm::vec2> _plan_size;

	ShaderGenerator _gen;
	Drawer _drawer;
	Drawer _instances_drawer;
	Drawer _planar_instances_drawer;
	Drawer _O3L1_instances_drawer;
	Drawer _tower_instances_drawer;
	Drawer _O3tower_instances_drawer;
};

#endif //_ISOMETRIC_SHADOWER_HPP_
