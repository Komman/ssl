#ifndef _HOLED_WALL_HITBOX_HPP_
#define _HOLED_WALL_HITBOX_HPP_

#include "shaped_hitbox_fusion.hpp"
#include "paver_hitbox.hpp"
#include "special_shapes.hpp"

class HoledWallHitbox : public ShapedHitboxFusion<dooredWall>
{
public:
	HoledWallHitbox(const dooredWall& shape);

private:
	void build();
};

#endif //_HOLED_WALL_HITBOX_HPP_
