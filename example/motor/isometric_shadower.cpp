#include "isometric_shadower.hpp"

#include "../utils/mutils.hpp"

using namespace std;
using namespace glm;

IsometricShadower::IsometricShadower(const std::string& name,
					  const glm::uvec2&  texture_size,
					  const glm::vec3&   source_start,
					  const glm::vec3&   furthest_point,
					  const glm::vec2&   plansize)
	: _coordinates_id(name + TEXTURE_ADDNAME, texture_size, GL_LINEAR, GL_CLAMP_TO_EDGE),
	  _framebuffer(&_coordinates_id),
	  _triangle_indentifor(name + TRIIDFOR_ADDNAME, ivec3(0)),
	  _source_start(name + SOURCE_ADDNAME, source_start),
	  _furthest_point(name + FURTHEST_ADDNAME, furthest_point),
	  _plan_size(name + PLANSIZE_ADDNAME, plansize),
	  _gen("motor/shaders/isometric_shadower.glsl", {
	  	{"isoshadow_furthest", name + FURTHEST_ADDNAME},
	  	{"isoshadow_source"  , name + SOURCE_ADDNAME},
	  	{"isoshadow_plansize", name + PLANSIZE_ADDNAME},
	  	{"isoshadow_triangle_indentifor", name + TRIIDFOR_ADDNAME},
	  }),
	  _drawer(_gen, "isometric_vertex", "isometric_fragment"),
	  _instances_drawer(_gen, "instanced_isometric_vertex", "isometric_fragment"),
	  _planar_instances_drawer(_gen, "instanced_isometric_planar_vertex", "isometric_fragment"),
	  _O3L1_instances_drawer(_gen, "instanced_isometric_O3L1_vertex", "isometric_fragment"),
	  _tower_instances_drawer(_gen, "instanced_isometric_tower_vertex", "isometric_fragment"),
	  _O3tower_instances_drawer(_gen, "instanced_isometric_O3tower_vertex", "isometric_fragment")
{
	_drawer.depth(true);
	_drawer.depth_function(GL_LESS);
	_instances_drawer.depth(true);
	_instances_drawer.depth_function(GL_LESS);
	_planar_instances_drawer.depth(true);
	_planar_instances_drawer.depth_function(GL_LESS);
	_O3L1_instances_drawer.depth(true);
	_O3L1_instances_drawer.depth_function(GL_LESS);
	_tower_instances_drawer.depth(true);
	_tower_instances_drawer.depth_function(GL_LESS);
	_O3tower_instances_drawer.depth(true);
	_O3tower_instances_drawer.depth_function(GL_LESS);

	this->replace_source(source_start, furthest_point, plansize);
}

void IsometricShadower::replace_source(const glm::vec3& source_start,
					    const glm::vec3& furthest_point,
					 	const glm::vec2& plansize)
{
	_source_start   = source_start;
	_furthest_point = furthest_point;
	_plan_size = plansize;
	_framebuffer.set_clear_color(glm::vec4(furthest_point, 0));
}

void IsometricShadower::clear()
{
	_framebuffer.clear();
}

void IsometricShadower::add_mesh(const ArrayBuffer<glm::vec3>& mesh, const ElementBuffer& elements,
								 triangleIdStrategy strategy,
								 int strategy_arg)
{
	_drawer.draw_elements_in_framebuffer(_framebuffer, elements, mesh);
	_triangle_indentifor.change_value(ivec3(_triangle_indentifor.get_value().x + elements.size()/3, strategy, strategy_arg));
}

void IsometricShadower::add_instanced_mesh(const ElementBuffer& elements,
							const ArrayBuffer<glm::vec3>& mesh,
							const ArrayBuffer<glm::vec3>& instances_dep,
							triangleIdStrategy strategy,
							int strategy_arg)

{
	_instances_drawer.draw_elements_in_framebuffer(_framebuffer, elements, mesh, instances_dep);
	_triangle_indentifor.change_value(ivec3(_triangle_indentifor.get_value().x + elements.size()*instances_dep.size()/3, strategy, strategy_arg));
}

void IsometricShadower::add_instanced_planar_mesh(const ElementBuffer& elements,
							   const ArrayBuffer<glm::vec3>& mesh,
							   const ArrayBuffer<glm::vec4>& pos_height,
							   const ArrayBuffer<glm::vec4>& dir2D_sizexz,
							   triangleIdStrategy strategy,
							   int strategy_arg)
{
	_planar_instances_drawer.draw_elements_in_framebuffer(_framebuffer, elements, mesh, pos_height, dir2D_sizexz);
	_triangle_indentifor.change_value(ivec3(_triangle_indentifor.get_value().x + elements.size()*pos_height.size()/3, strategy, strategy_arg));
}

void IsometricShadower::add_instanced_O3L1_mesh(const ElementBuffer& elements,
								 const ArrayBuffer<glm::vec3>& mesh,
								 const ArrayBuffer<glm::vec4>& dep_height,
								 const ArrayBuffer<glm::vec4>& orientation3DN_length,
								 triangleIdStrategy strategy,
								 int strategy_arg)
{
	_O3L1_instances_drawer.draw_elements_in_framebuffer(_framebuffer, elements, mesh, dep_height, orientation3DN_length);
	_triangle_indentifor.change_value(ivec3(_triangle_indentifor.get_value().x + elements.size()*dep_height.size()/3, strategy, strategy_arg));
}

void IsometricShadower::add_instanced_tower_mesh(const ElementBuffer& elements,
								  const ArrayBuffer<glm::vec3>& mesh,
								  const ArrayBuffer<glm::vec4>& dep_height,
								  const ArrayBuffer<float>& size,
								  triangleIdStrategy strategy,
								  int strategy_arg)
{
	_tower_instances_drawer.draw_elements_in_framebuffer(_framebuffer, elements, mesh, dep_height, size);
	_triangle_indentifor.change_value(ivec3(_triangle_indentifor.get_value().x + elements.size()*dep_height.size()/3, strategy, strategy_arg));
}

void IsometricShadower::add_instanced_O3tower_mesh(const ElementBuffer& elements,
								    const ArrayBuffer<glm::vec3>& mesh,
								    const ArrayBuffer<glm::vec4>& dep_height,
								    const ArrayBuffer<glm::vec4>& orientation3DN_size,
									triangleIdStrategy strategy,
									int strategy_arg)
{
	_O3tower_instances_drawer.draw_elements_in_framebuffer(_framebuffer, elements, mesh, dep_height, orientation3DN_size);
	_triangle_indentifor.change_value(ivec3(_triangle_indentifor.get_value().x + elements.size()*dep_height.size()/3, strategy, strategy_arg));
}

float IsometricShadower::isometric_depth(const glm::vec3& pos)
{
	vec3 dirlightN  = glm::normalize(_furthest_point.get_value() - _source_start.get_value());
	
	return glm::dot(dirlightN, pos - _source_start.get_value());
}

void IsometricShadower::compute_triangle_antialiasing()
{
	const float DONTTOUCH = glm::intBitsToFloat(-1);
	const Texture<glm::vec4>& tex = _coordinates_id.get_texture();

	std::vector<vec4> tex1D = tex.download_data();
	std::vector<vec4> newtex1D = tex1D;
	auto size = _coordinates_id.size();

	// AutoPrintRecorder::START();

	for(uint x=1; x<size.x-1; x++)
	{
		for(uint y=1; y<size.y-1; y++)
		{
			int index = x + y*size.x;
			int triangle = glm::floatBitsToInt(tex1D[index].w);

			int neis[4] = {
				glm::floatBitsToInt(tex1D[index + (int)size.x].w),
				glm::floatBitsToInt(tex1D[index - 1].w),
				glm::floatBitsToInt(tex1D[index - (int)size.x].w),
				glm::floatBitsToInt(tex1D[index + 1].w),
			};

			bool sames[4] = {
				(neis[0] == triangle),
				(neis[1] == triangle),
				(neis[2] == triangle),
				(neis[3] == triangle),
			};

			bool horiz = (neis[0] == neis[2]);
			bool verti = (neis[1] == neis[3]);

			if(   (horiz && sames[0])
			   || (verti && sames[1]))
			{
				newtex1D[index].w = DONTTOUCH;
			}
		}
	}

	// AutoPrintRecorder::STOP();
	// exit(0);

	for(uint x=0; x<size.x; x++)
	{
		newtex1D[x].w = DONTTOUCH;
		newtex1D[x + (size.y-1)*size.x].w = DONTTOUCH;
	}
	for(uint y=0; y<size.y; y++)
	{
		newtex1D[y*size.x].w = DONTTOUCH;
		newtex1D[y*size.x + size.x-1].w = DONTTOUCH;
	}

	_coordinates_id.change_data(uvec3(size, 1), newtex1D.data());
}

const UniformTexture2D<glm::vec4>& IsometricShadower::get_coordinates_id_texture() const
{
	return _coordinates_id;
}

