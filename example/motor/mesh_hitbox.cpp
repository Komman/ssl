#include "mesh_hitbox.hpp"

#include "motor_debug.hpp"
#include "bevel_hitbox.hpp"

MeshHitobox::MeshHitobox(const meshSurface& shape, float min_radius)
	: ShapedHitboxFusion(shape),
	  _min_radius(min_radius)
{
	if(shape.part.second < shape.part.first)
	{
		mot::err("MeshHitobox::MeshHitobox(): shape.part.first < shape.part.first");
	}
	if((shape.part.second - shape.part.first)%3 != 0)
	{
		mot::err("MeshHitobox::MeshHitobox(): no good elements amount for triangles:\n(shape.part.second - shape.part.first)%3 != 0\n("
			+ std::to_string(shape.part.second) + "-"
			+ std::to_string(shape.part.first) 
			+ ")%3 != 0");
	}
	this->build();
}

void MeshHitobox::build()
{
	const auto& shape = this->get_shape();

	for(uint i=shape.part.first; i<shape.part.second; i+=3)
	{
		if(i+2 >= shape.elements->size())
		{
			mot::err("MeshHitobox::build(): element index out of bounds");
		}

		uint e1 = (*shape.elements)[i+0];
		uint e2 = (*shape.elements)[i+1];
		uint e3 = (*shape.elements)[i+2];

		if(e1 >= shape.mesh_points->size()
		|| e2 >= shape.mesh_points->size()
		|| e3 >= shape.mesh_points->size())
		{
			mot::err("MeshHitobox::build(): an element if out of bound");
		}

		glm::vec3 pts[] = {
			(*shape.mesh_points)[e1],
			(*shape.mesh_points)[e2],
			(*shape.mesh_points)[e3]
		};

		bool skip = false;
		for(uint i=0; i<3; i++)
		{
			if(glm::distance(pts[i], pts[(i+1)%3]) < _min_radius)
			{
				skip = true;
				break;
			}
		}

		if(skip)
			continue;

		this->fusion_hitbox<BevelHitbox>(bevelShape{
			.triangle_base   = {
				pts[0],
				pts[1],
				pts[2]
		   	},
		   	.sized_direction = geom::safe_normalize(glm::cross(
		   		pts[1] - pts[0],
		   		pts[2] - pts[0]
		   	))*shape.width
		});
	}
}