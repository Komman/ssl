#ifndef _STATIC_LIGHTOR_HPP_
#define _STATIC_LIGHTOR_HPP_

#include "../../ssl/ssl.hpp"

using namespace ssl;

struct ligthingInfo
{
	glm::vec3 color;
	float range;
	float intensity;
	float saturation;
};

class StaticLigthor
{
public:
	StaticLigthor(const std::string& name,
				  const glm::vec2&   bottom, 
				  const glm::vec2&   top, 
				  const glm::uvec2&  size,
				  unsigned int max_ligths,
				  unsigned int max_ligths_references);

	void add_ligth(const glm::vec3& position, const ligthingInfo& ligth);
	void compute_static_ligth_map();

	void set_ligths_activation(float coeff);
	void print() const;

	uint lights_amount() const;
	uint lights_left() const;

protected:

	static inline std::string max_ligths_addname  = "_max_ligths";
	static inline std::string activation_addname  = "_activation";
	
	static inline std::string bottom_addname  = "_bottom";
	static inline std::string top_addname     = "_top";
	static inline std::string texture_addname = "_ligthmap";

	static inline std::string ligths_positions_range_addname = "_ligths_positions_range";
	static inline std::string ligths_colors_addname    = "_ligths_colors";
	static inline std::string ligths_intensity_saturation_addname = "_ligths_intensity_saturation";

	static inline std::string ligths_references_addname = "_ligths_references";

	struct ligthInfo
	{
		glm::vec3    position;
		ligthingInfo ligth;
	};

	struct indexedSets
	{
		std::vector<std::vector<int>> sets;
		std::vector<std::vector<int>> indices_map;

		void print(bool map = false) const;
	};

	// Returns if the ligth has been put
	bool put_ligth_index(const glm::ivec2& pos, unsigned int ligth_index);
	// Returns the index in the reference list
	int add_references(const std::vector<int>& ligth_ref, std::vector<int>& vector_ligths_references);
	indexedSets build_indexed_sets_equality();
	indexedSets build_indexed_sets_inclusion();
	void fill_references_textures(const indexedSets& sets);

private:
	const std::string _name;

	Uniform<glm::vec2> _bottom;
	Uniform<glm::vec2> _top;

	glm::uvec2 _size;
	unsigned int _max_ligths;
	unsigned int _max_ligths_references;

	std::vector<ligthInfo> _ligths;
	std::vector<std::vector<std::vector<int>>> _ligths_map;

	Uniform<int>   _global_max_ligth;
	Uniform<float> _ligths_activation;

	UniformTexture1D<glm::vec4> _ligths_positions_range;
	UniformTexture1D<glm::vec3> _ligths_colors;
	UniformTexture1D<glm::vec2> _ligths_intensity_saturation;

	UniformTexture1D<int> _ligths_references;
	UniformTexture2D<int> _indices_map;
};

#endif //_STATIC_LIGHTOR_HPP_
