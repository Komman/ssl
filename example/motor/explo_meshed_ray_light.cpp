#include "explo_meshed_ray_light.hpp"

#include <glm/gtx/rotate_vector.hpp>

using namespace glm;

ExploMeshedRayLight::ExploMeshedRayLight(const glm::vec3& center,
						const glm::vec3& traject,
						float angle,
						const glm::vec3& color,
						float duration)
	: TimedStaticMeshedRayLight(
		build_vertices(center, traject, angle),
		std::vector<vec3>({color, vec3(0.0), vec3(0.0)}),
		{0,1,2},
		duration),
	  _intensity()
{
	_intensity.add_value(1.0f, 0.0f);
	_intensity.add_value(1.0f, 1.0f);
	_intensity.add_value(4.0f, 1.0f);
	_intensity.add_value(0.0f, 0.0f);
	
	_intensity.force_total_duration(duration);
}

std::vector<glm::vec3> ExploMeshedRayLight::build_vertices(const glm::vec3& center,
												 		   const glm::vec3& traject,
												 		   float angle)
{
	std::vector<glm::vec3> ret;

	vec3 rotate_axis = glm::cross(traject, vec3(0.0,1.0,0.0));

	ret.push_back(center);
	ret.push_back(center + glm::rotate(traject, +angle/2.0f, rotate_axis));
	ret.push_back(center + glm::rotate(traject, -angle/2.0f, rotate_axis));

	return ret;
}

const LinearValuator<float>& ExploMeshedRayLight::get_intensity_evolution() const
{
	return _intensity;
}
