#include "joystick.hpp"

#include <iostream>


BasicJoystick::BasicJoystick(const BasicMousekeyTarget* mousekey)
	: _mousekey(mousekey)
{

}

const BasicMousekeyTarget* BasicJoystick::mousekey_ptr() const
{
	return _mousekey;
}


bool BasicJoystick::key_pressed(int key) const
{
	if(_mousekey == NULL)
	{
		return ssl::keyboard::key_pressed(key);
	}
	return _mousekey->key_pressed(key);
}


Joystick::Joystick(const BasicMousekeyTarget* mousekey, int keys[JOYSTICK_NUMBER_CONTROLS], const ssl::View* view)
	: BasicJoystick(mousekey),
	  _view(view),
	  _control_keys()
{
	for(uint i=0; i<JOYSTICK_NUMBER_CONTROLS; i++)
	{
		_control_keys.push_back(keys[i]);
	}
}

glm::vec3 Joystick::compute_directionN() const
{
	std::vector<joystickKeysIndices> pressed_keys_indices;

	for(unsigned int i=0; i<_control_keys.size(); i++)
	{
		if(this->key_pressed(_control_keys[i]))
		{
			pressed_keys_indices.push_back((joystickKeysIndices)i);
		}
	}

	return this->directionN_from_pressed(pressed_keys_indices);
}

// #define PRINT_KEY

#ifdef PRINT_KEY
#define PRINT_KEY_PRESSED(KEY) std::cout<<"Key pressed: "<<#KEY<<std::endl;
#else
#define PRINT_KEY_PRESSED(KEY) 
#endif

glm::vec3 Joystick::directionN_from_pressed(const std::vector<joystickKeysIndices>& pressed) const
{
	glm::vec3 direction = glm::vec3(0.0, 0.0, 0.0);

	//TOOPT (0) (with function pointer)
	for(joystickKeysIndices k : pressed)
	{
		if(k == JOYSTICK_CONTROL_RIGHT)
		{
			PRINT_KEY_PRESSED(JOYSTICK_CONTROL_RIGHT)
			direction+= _view->get_right_vector();
		}
		if(k == JOYSTICK_CONTROL_LEFT)
		{
			PRINT_KEY_PRESSED(JOYSTICK_CONTROL_LEFT)
			direction-= _view->get_right_vector();
		}
		if(k == JOYSTICK_CONTROL_UP)
		{
			PRINT_KEY_PRESSED(JOYSTICK_CONTROL_UP)
			direction+= _view->get_up_head();
		}
		if(k == JOYSTICK_CONTROL_DOWN)
		{
			PRINT_KEY_PRESSED(JOYSTICK_CONTROL_DOWN)
			direction-= _view->get_up_head();
		}
		if(k == JOYSTICK_CONTROL_FRONT)
		{
			PRINT_KEY_PRESSED(JOYSTICK_CONTROL_FRONT)
			direction+= glm::cross(_view->get_up_head(), _view->get_right_vector());
		}
		if(k == JOYSTICK_CONTROL_BACK)
		{
			PRINT_KEY_PRESSED(JOYSTICK_CONTROL_BACK)
			direction-= glm::cross(_view->get_up_head(), _view->get_right_vector());
		}
	}

	float dirl = glm::length(direction);

	if(dirl > 0.0001f)
	{
		return direction/dirl;
	}
	else
	{
		return glm::vec3(0);
	}
}
