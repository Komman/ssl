#include "holed_wall_hitbox.hpp"

#include "../motor/building.hpp"

HoledWallHitbox::HoledWallHitbox(const dooredWall& shape)
	: ShapedHitboxFusion(shape)
{
	this->build();
}

void HoledWallHitbox::build()
{
	std::vector<glm::vec3> points = building::doored_wall::v(this->get_shape());

	this->fusion_hitbox<PaverHitbox>(exactPaverInfo{
		.square_base_up = {
			points[0],
			points[4],
			points[7],
			points[3]
		},
		.square_base_down = {
			points[8+0],
			points[8+4],
			points[8+7],
			points[8+3]
		}
	});

	this->fusion_hitbox<PaverHitbox>(exactPaverInfo{
		.square_base_up = {
			points[3],
			points[7],
			points[6],
			points[2]
		},
		.square_base_down = {
			points[8+3],
			points[8+7],
			points[8+6],
			points[8+2]
		}
	});

	this->fusion_hitbox<PaverHitbox>(exactPaverInfo{
		.square_base_up = {
			points[6],
			points[5],
			points[1],
			points[2]
		},
		.square_base_down = {
			points[8+6],
			points[8+5],
			points[8+1],
			points[8+2]
		}
	});
}
