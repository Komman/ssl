#include "unstored_point.hpp"


UnstoredPoint::UnstoredPoint(const glm::vec3* position)
	: BasicPoint(),
	  _position_ptr(position)
{

}

const glm::vec3& UnstoredPoint::get_position() const
{
	return *_position_ptr;
}



UnstoredDirectedPoint::UnstoredDirectedPoint(const glm::vec3* position, const glm::vec3* speed)
	: BasicDirectedPoint(),
	  _position_ptr(position),
	  _speed_ptr(speed)
{

}

const glm::vec3& UnstoredDirectedPoint::get_position() const
{
	return *_position_ptr;
}


const glm::vec3& UnstoredDirectedPoint::get_speed() const
{
	return *_speed_ptr;
}

