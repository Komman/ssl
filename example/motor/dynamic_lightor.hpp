#ifndef _DYNAMIC_LIGHTOR_HPP_
#define _DYNAMIC_LIGHTOR_HPP_

#include "../../ssl/ssl.hpp"

#include "uniform_textured_vector.hpp"

using namespace ssl;

struct dynamicLight
{
	glm::vec3 position;
	glm::vec3 color;
	float intensity;
	float saturation;

	dynamicLight operator*(float x);
	dynamicLight operator+(const dynamicLight& l);
};

class BasicDynamicLightor
{
public:
	BasicDynamicLightor(){}
	
	virtual void update_GPU() =0;

	virtual uint add_light(const dynamicLight& light) =0;
	virtual void remove_light(uint index) =0;

	virtual void set_light_light(uint index, const dynamicLight& light) =0;
	virtual void set_light_position(uint index, const glm::vec3& position) =0;
	virtual void set_light_color(uint index, const glm::vec3& color) =0;
	virtual void set_light_intensity(uint index, float intensity) =0;
	virtual void set_light_saturation(uint index, float saturation) =0;

	virtual uint max_size() const =0;
	virtual uint size()     const =0;

	virtual float get_light_intensity(uint index) const =0;

	// Returns the number of lights that can be added
	uint available_light_slots() const {return this->max_size() - this->size();}
};


class DynamicLightor : public BasicDynamicLightor
{
public:
	DynamicLightor(const std::string& name, uint max_size);
	virtual ~DynamicLightor(){}

	void update_GPU() override;

	// Returns an index for followings light manipulations
	uint add_light(const dynamicLight& light) override;
	void remove_light(uint index) override;

	void set_light_light(uint index, const dynamicLight& light)    override;
	void set_light_position(uint index, const glm::vec3& position) override;
	void set_light_color(uint index, const glm::vec3& color)       override;
	void set_light_intensity(uint index, float intensity)          override;
	void set_light_saturation(uint index, float saturation)        override;

	uint max_size() const override;
	uint size()     const override;

	float get_light_intensity(uint index) const override;


	void print() const;

public:
	static inline std::string UNIFORM_PRE_NAME = "DynamicLightor_"; 
	
	static inline std::string POSITIONS_ADD_NAME     = "_positions_saturation"; 
	static inline std::string COLORS_INTENS_ADD_NAME = "_colors_intens"; 

private:
	void check_sizes() const;
	void check_index(uint index) const;
	void pre_error() const;
	static glm::vec4 pack_color_intens(const glm::vec3& color, float intensity);
	static glm::vec4 pack_position_saturation(const glm::vec3& pos, float saturation);

private:
	std::string _name; 

	/*
		_positions_saturation.xyz : position
							 .w   : saturation
	*/ 
	UniformTexturedVector<glm::vec4> _positions_saturation;
	/*
		_colors_intens.xyz : color
				      .w   : intensity

		with : (intensity == -1)   =>   no more dynamic lights
	*/
	UniformTexturedVector<glm::vec4> _colors_intens;

	std::vector<int>  _indices_owner;
	std::vector<int>  _lights_indices;
	std::vector<uint> _available_indices;

	bool _data_has_changed;
};

#endif //_DYNAMIC_LIGHTOR_HPP_
