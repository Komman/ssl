#include "window_console.hpp"

#include "motor_debug.hpp"

#include "../../ssl/ssl.hpp"


namespace CONSOLE
{
	static WindowConsole* _main_console = NULL;

	void char_callback(GLFWwindow* w, unsigned int codepoint)
	{
		if(_main_console != NULL && _main_console->typing())
		{
			_main_console->add_letter((char)(codepoint));
		}	
	}
	void key_callback(GLFWwindow* w, int key, int scancode, int action, int mods)
	{
		static uint msg_searching = 0;

		if(_main_console != NULL)
		{
			if(_main_console->typing())
			{
				if(action == GLFW_PRESS || action == GLFW_REPEAT)
				{
					const auto& allmsgs = _main_console->all_msgs();

					if(key == SSL_KEY(ENTER))
					{
						_main_console->send_current_command();
						_main_console->exit();
					}
					if(key == SSL_KEY(ESCAPE))
					{
						_main_console->exit();
					}
					if(key == SSL_KEY(BACKSPACE))
					{
						_main_console->remove_last_letter();
					}
					if(key == SSL_KEY(UP))
					{
						if(allmsgs.size() > 0)
						{
							_main_console->replace_current_command(_main_console->last_sent_msg(msg_searching));
							msg_searching++;
						}
					}
					if(key == SSL_KEY(DOWN))
					{
						if(allmsgs.size() > 0)
						{
							_main_console->replace_current_command(_main_console->last_sent_msg(msg_searching));
							msg_searching = std::max(1u, msg_searching)-1;
						}
					}
				}
			}
			else
			{
				if(action == GLFW_PRESS || action == GLFW_REPEAT)
				{
					if(key == _main_console->write_key())
					{
						_main_console->open();
						msg_searching = 0;
					}
				}
			}
		}	
	}
	
	WindowConsole& get()
	{
		if(_main_console == NULL)
		{
			mot::err("CONSOLE::get(): no main console set");
		}

		return *_main_console;
	}

	void set(WindowConsole* console)
	{
		_main_console = console;
		CONSOLE::enable_callback();
	}

	void enable_callback()
	{
		ssl::window::set_char_callback(char_callback);
		ssl::window::set_key_callback(key_callback);
	}

	void disable_callback()
	{
		ssl::window::set_char_callback(NULL);
		ssl::window::set_key_callback(NULL);
	}

	void CONTROL_N(const std::string& name, void* fptr)
	{
		CONSOLE::get().set_modifiable(name, fptr);
	}

	void say(const std::string& msg)
	{
		CONSOLE::get().send_msg(msg);
	}

};



void WindowConsole::replace_current_command(const std::string& str)
{
	_command = str;
}


WindowConsole::WindowConsole(MousekeySwitcher* switcher, int write_key, float lasting)
	: Console(lasting),
	  _write_key(write_key),
	  _mousekey(switcher->get_switch_ptr()),
	  _switcher(switcher),
	  _exit_mousekey(NULL),
	  _command("")
{
	
}

void WindowConsole::send_current_command()
{
	this->send_msg(_command);
	this->erase_command();
}

void WindowConsole::open()
{
	_exit_mousekey = _switcher->get_target();
	_switcher->target_switch(&_mousekey);
	this->erase_command();
}

void WindowConsole::exit()
{
	_switcher->target_switch(_exit_mousekey);
}

void WindowConsole::erase_command()
{
	_command = "";
}

bool WindowConsole::typing() const
{
	return _mousekey.enable();
}

const BasicMousekeyTarget& WindowConsole::mousekey() const
{
	return _mousekey;
}

int WindowConsole::write_key() const
{
	return _write_key;
}

const std::string& WindowConsole::current_text() const
{
	return _command;
}

void WindowConsole::print_current_text() const
{
	std::cout<<_command<<std::endl;
}

void WindowConsole::add_letter(char c)
{
	_command.push_back(c);
}

void WindowConsole::remove_last_letter()
{
	if(_command.size() > 0)
	{
		_command.pop_back();
	}
}	



