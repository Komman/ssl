#ifndef _INSTANCES_MANAGER_HPP_
#define _INSTANCES_MANAGER_HPP_

#include "../motor/motor_debug.hpp"

#include "stored_instances_buffers.hpp"

template<typename InstanceStruct, uint FIRST_INSTANCE_BUFFER, typename... Types>
class InstancesManager : public StoredInstancesBuffers<FIRST_INSTANCE_BUFFER, Types...>
{
public:
	using MotherClass = StoredDrawableObject<Types...>;

	template<typename Type>
	struct storedDrawType
	{
		using type = ssl::draw_type;
	};

public:
	InstancesManager(ssl::draw_type element_draw_type, typename storedDrawType<Types>::type ... buffers_draw_type);

	void animate(float dt);
	void add_instance(const InstanceStruct& params);

	// Reference to a vector elements
	const InstanceStruct& get_instance_infos(uint instance_index) const;

protected:
	/*
		Add one value to every instance buffer.
	*/
	virtual void add_instance_in_buffers(const InstanceStruct& infos) =0;
	/*
		Returns if the instance in index instance_index in 
		the instances buffers must be deleted from this
		instance manager
	*/
	virtual bool has_expired(uint instance_index) const {return false;};

	/*
		This method is called just before deleting
		the instance at instance_index.
	*/
	virtual void delete_instance(uint instance_index) {}
	/*
		This method is called at every call of
		the animate() method.
	*/
	virtual void animate_instance(uint instance_index) {}

private:
	void swapop_instance(uint instance_index);

private:
	std::vector<InstanceStruct> _data;
};


#define InstancesManagerMACRO(retType) template<typename InstanceStruct, uint FIRST_INSTANCE_BUFFER, typename... Types>\
									   retType InstancesManager<InstanceStruct, FIRST_INSTANCE_BUFFER, Types...>

InstancesManagerMACRO()::InstancesManager(ssl::draw_type element_draw_type, typename storedDrawType<Types>::type ... buffers_draw_type)
	: StoredInstancesBuffers<FIRST_INSTANCE_BUFFER, Types...>(element_draw_type, buffers_draw_type...),
	  _data()
{

}

InstancesManagerMACRO(void)::animate(float dt)
{
	for(uint i=0; i<this->instances_amount(); i++)
	{
		this->animate_instance(i);
		
		if(this->has_expired(i))
		{
			this->swapop_instance(i);

			i--;
		}
	}
}

InstancesManagerMACRO(void)::add_instance(const InstanceStruct& infos)
{
	_data.push_back(infos);
	this->add_instance_in_buffers(_data[_data.size()-1]);	
}

InstancesManagerMACRO(const InstanceStruct&)::get_instance_infos(uint instance_index) const
{
	if(instance_index >= _data.size())
	{
		mot::err("InstancesManager::get_instance_infos(): wrong index");
	}

	return _data[instance_index];
}

InstancesManagerMACRO(void)::swapop_instance(uint instance_index)
{
	this->delete_instance(instance_index);
	this->swapop(instance_index);
	utils::swapop(_data, instance_index);
}


#undef InstancesManagerMACRO

#endif //_INSTANCES_MANAGER_HPP_
