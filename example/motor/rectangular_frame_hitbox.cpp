#include "rectangular_frame_hitbox.hpp"


// struct rectangularFrameInfo
// {
// 	glm::vec3 center;
// 	glm::vec3 radius_x;
// 	glm::vec3 height_dir;
// 	glm::vec3 radius_z;
// 	glm::vec2 thicknesses_coeffs;
// };


RectangularFrameHitbox::RectangularFrameHitbox(const rectangularFrameInfo& shape)
	: ShapedHitboxFusion(shape)
{
	this->build();
}

void RectangularFrameHitbox::build()
{
	const rectangularFrameInfo& shape = this->get_shape();

	this->fusion_hitbox<PaverHitbox>(paverInfo{
		.bottom  = shape.center + shape.radius_x + shape.radius_z,
		.sized_x = -2.0f*shape.radius_x,
		.sized_y = shape.height_dir,
		.sized_z = -shape.radius_z*shape.thicknesses_coeffs.y
	});
	this->fusion_hitbox<PaverHitbox>(paverInfo{
		.bottom  = shape.center - shape.radius_x - shape.radius_z,
		.sized_x = 2.0f*shape.radius_x,
		.sized_y = shape.height_dir,
		.sized_z = shape.radius_z*shape.thicknesses_coeffs.y
	});

	this->fusion_hitbox<PaverHitbox>(paverInfo{
		.bottom  = shape.center - shape.radius_x - shape.radius_z,
		.sized_x = shape.radius_x*shape.thicknesses_coeffs.y,
		.sized_y = shape.height_dir,
		.sized_z = 2.0f*shape.radius_z
	});
	this->fusion_hitbox<PaverHitbox>(paverInfo{
		.bottom  = shape.center + shape.radius_x + shape.radius_z,
		.sized_x = -shape.radius_x*shape.thicknesses_coeffs.y,
		.sized_y = shape.height_dir,
		.sized_z = -2.0f*shape.radius_z
	});
}

