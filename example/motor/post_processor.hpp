#ifndef _POST_PROCESSOR_HPP_
#define _POST_PROCESSOR_HPP_

#include "../../ssl/ssl.hpp"

#include "bloomer.hpp"

class PostProcessor
{
public:
	struct SharedFramebuffers
	{
		FrameBufferMultisample<glm::vec4>   		 * frameMS;
		FrameBuffer<glm::vec4>              		 * frame;
		FrameBuffer<glm::vec4>              		 * frame_blending;
		FrameBuffer<glm::vec4, glm::vec4>            * frame_blending_and_bloom;
		FrameBuffer<glm::vec4, glm::vec4>   		 * frame_and_bloom;
		FrameBufferMultisample<glm::vec4, glm::vec4> * frame_and_bloomMS;
		FrameBuffer<glm::vec4>                       * bloom_frame;
		FrameBufferMultisample<glm::vec4>            * bloom_frameMS;
	};

public:
	PostProcessor(const glm::vec4& clear_color,
				  Bloomer& bloomer,
				  const SharedFramebuffers& needed_framebuffers);

	void clear();
	void draw_multisampled();

	void apply_bloom();
	void screen_pass();

	// this->direct_full_process() = this->apply_bloom() + this->screen_pass() 
	void direct_full_process();

	void blur_texture();

	FrameBufferMultisample<glm::vec4>& frameMS();
	FrameBuffer<glm::vec4>& frame();
	FrameBuffer<glm::vec4>& blending_frame();
	FrameBuffer<glm::vec4, glm::vec4>& bloomed_blending_frame();
	FrameBuffer<glm::vec4, glm::vec4>& frame_and_bloom();
	FrameBuffer<glm::vec4>& bloom_frame();
	FrameBufferMultisample<glm::vec4, glm::vec4>& frame_and_bloomMS();
	FrameBufferMultisample<glm::vec4>& bloom_frameMS();

	Bloomer& bloomer();

protected:

private:

	Bloomer& _bloomer;

	ShaderGenerator _shader_generator;

	Drawer _post_processor;

	FrameBufferMultisample<glm::vec4> & _frameMS;
	FrameBuffer<glm::vec4>            & _frame;
	FrameBuffer<glm::vec4>            & _frame_blending;
	FrameBuffer<glm::vec4, glm::vec4> & _frame_blending_and_bloom;
	FrameBuffer<glm::vec4, glm::vec4> & _frame_and_bloom;
	FrameBufferMultisample<glm::vec4, glm::vec4> & _frame_and_bloomMS;
	FrameBuffer<glm::vec4>                       & _bloom_frame;
	FrameBufferMultisample<glm::vec4>            & _bloom_frameMS;

};

#endif //_POST_PROCESSOR_HPP_
