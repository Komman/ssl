#ifndef _SWITCHABLE_OBJECT_HPP_
#define _SWITCHABLE_OBJECT_HPP_

class SwitchableObject
{
public:
	SwitchableObject(const SwitchableObject* const* switcher);

	bool enable() const;
	const SwitchableObject* const* get_switcher() const;

private:
	const SwitchableObject* const* _switcher;
};

#endif //_SWITCHABLE_OBJECT_HPP_
