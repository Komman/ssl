#include "meshed_ray_luminor.hpp"

#include "gutils.hpp"

/*
	MeshedRayLight
*/

MeshedRayLight::MeshedRayLight(MeshedRayLuminor& luminor)
	: _luminor(luminor),
	  _enable(false)
{

}

MeshedRayLight::MeshedRayLight(MeshedRayLight& ray)
	: _luminor(ray._luminor),
	  _enable(ray._enable)
{
	if(_enable)
	{
		this->enable();
	}
}

MeshedRayLight::MeshedRayLight(MeshedRayLight&& ray)
	: _luminor(ray._luminor),
	  _enable(ray._enable)
{
	ray.disable();

	if(_enable)
	{
		this->enable();
	}
}

MeshedRayLight::~MeshedRayLight()
{
	this->disable();
}

bool MeshedRayLight::is_enable() const
{
	return _enable;
}

void MeshedRayLight::enable()
{
	if(!_enable)
	{
		_luminor.control_ray(this);
		_enable = true;
	}
}

void MeshedRayLight::disable()
{
	if(_enable)
	{
		_luminor.uncontrol_ray(this);
		_enable = false;
	}
}


/*
	MeshedRayLuminor
*/

MeshedRayLuminor::MeshedRayLuminor()
	: StoredVAObject(DYNAMIC_DRAW, DYNAMIC_DRAW, DYNAMIC_DRAW),
	  _rays(),
	  _ext_rays()
{

}

void MeshedRayLuminor::add_ray(std::unique_ptr<StoredMeshedRayLight>&& ray)
{
	_rays.push_back(std::move(ray));
}

void MeshedRayLuminor::control_ray(MeshedRayLight* ray)
{
	_ext_rays.insert(ray);
}

void MeshedRayLuminor::uncontrol_ray(MeshedRayLight* ray)
{
	_ext_rays.erase(ray);
}

void MeshedRayLuminor::animate_rays(float dt)
{
	for(int i=0; i<(int)_rays.size(); i++)
	{
		_rays[i]->animate(dt);
	}

	for(auto& ray : _ext_rays)
	{
		ray->animate(dt);
	}
}

void MeshedRayLuminor::delete_expired_rays()
{
	for(int i=0; i<(int)_rays.size(); i++)
	{
		if(_rays[i]->has_expired())
		{
			utils::swapop(_rays, i);
			i--;
		}
	}
}

void MeshedRayLuminor::build()
{
	uint current_index = 0;

	this->get_array_buffer<0>().clear();
	this->get_array_buffer<1>().clear();
	_elements.clear();

	for(int i=0; i<(int)_rays.size(); i++)
	{
		for(const auto& ray: _rays[i]->get_vertices())
		{
			this->add_vertex(ray.position, ray.color);
		}
		for(uint index: _rays[i]->get_elements())
		{
			_elements.add_value(index + current_index);
		}

		current_index = _elements.size();
	}

	for(auto& rayi : _ext_rays)
	{
		for(const auto& ray: rayi->get_vertices())
		{
			this->add_vertex(ray.position, ray.color);
		}
		for(uint index: rayi->get_elements())
		{
			_elements.add_value(index + current_index);
		}

		current_index = _elements.size();
	}
}

void MeshedRayLuminor::animate(float dt)
{
	this->animate_rays(dt);
	this->delete_expired_rays();
	this->build();
}

uint MeshedRayLuminor::ray_amount() const
{
	return _rays.size();
}

