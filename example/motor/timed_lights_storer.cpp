#include "timed_lights_storer.hpp"

#include "gutils.hpp"

TimedLightsStorer::TimedLightsStorer()
	: _lights(),
	  _time(0.0f)
{

}

void TimedLightsStorer::animate(float dt)
{
	_time += dt;

	for(uint i=0; i<_lights.size(); i++)
	{
		float local_time = _time - _lights[i].start_time;

		if(local_time > _lights[i].light_evlotion->total_animation_time())
		{
			utils::swapop(_lights, i);
			i--;
		}
		else
		{
			_lights[i].light_reference = _lights[i].light_evlotion->evaluate(local_time);
		}
	}

	// To prevent time to be not precise enough if it is too big
	if(_lights.size() == 0)
	{
		_time = 0.0f;
	}
}

void TimedLightsStorer::add_light(DynamicLightor& lightor,
			   std::unique_ptr<BasicTimedValuator<dynamicLight>>&& light_evlotion)
{
	LightStorage s(lightor, std::move(light_evlotion), _time);

	_lights.push_back(std::move(s));
}

uint TimedLightsStorer::size() const
{
	return _lights.size();
}


TimedLightsStorer::LightStorage::LightStorage(DynamicLightor& lightor, std::unique_ptr<BasicTimedValuator<dynamicLight>>&& light_evlotion, float time)
	: light_reference(lightor, light_evlotion->evaluate(0.0f)),
	  start_time(time),
	  light_evlotion(std::move(light_evlotion))
{

}

