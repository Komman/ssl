#ifndef _TIMED_LIGHTS_STORER_HPP_
#define _TIMED_LIGHTS_STORER_HPP_

#include "dynamic_light.hpp"
#include "basic_timed_valuator.hpp"

class TimedLightsStorer
{
public:
	TimedLightsStorer();

	void animate(float dt);

	void add_light(DynamicLightor& lightor,
				   std::unique_ptr<BasicTimedValuator<dynamicLight>>&& light_intesity);
	uint size() const;

private:
	struct LightStorage
	{
		LightStorage(DynamicLightor& lightor, std::unique_ptr<BasicTimedValuator<dynamicLight>>&& light_intesity, float time);

		DynamicLight light_reference;
		float        start_time;
		std::unique_ptr<BasicTimedValuator<dynamicLight>> light_evlotion;
	};

private:
	std::vector<LightStorage> _lights;

	float _time;
};

#endif //_TIMED_LIGHTS_STORER_HPP_
