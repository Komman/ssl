#ifndef _PLAYER_HPP_
#define _PLAYER_HPP_

#include "../../ssl/src/uniform_view.hpp"
#include "../motor/target.hpp"
#include "../utils/mutils.hpp"
#include "joystick.hpp"
#include "joystick2D.hpp"
#include "mouse_joystick.hpp"
#include "hitbox_context.hpp"

using namespace ssl;

class Player
{
public:
	static constexpr float NEAR       = 0.05;
	static constexpr float FAR        = 2000.0;
	static constexpr float VIEW_FIELD = 70.0f * 3.1415926535f/180.0f; //view field in degrees

	enum gameMode {NATURAL=0, CREATIVE};

public:
	Player(const MouseJoystickMKTarget* controls_source, const glm::vec3& position, float speed, float mass);

	void set_context(const HitboxContext* context);

	void look_at(const glm::vec3& look_at_position);
	void tp(const glm::vec3& npos);
	void move(float dt);

	void enable_collision();
	void disable_collision();

	bool key_pressed(int key) const;

	const glm::vec3&   get_position()  const;
	const glm::vec3&   get_direction() const;
	const glm::vec3&   get_vspeed()    const;
	const BasicTarget& get_target()    const; 
	GETTER_BUILDER_COPY(float, speed);
	SETTER_BUILDER_COPY(float, speed);
	GETTER_BUILDER_COPY(float, mass);
	SETTER_BUILDER_COPY(float, mass);
	SETTER_BUILDER_COPY(float, jump_impulse);

	void add_extern_speed(const glm::vec3& evs);
	// energy in speed*mass
	void energy_impulsion(const glm::vec3& dirN, float energy);

	void gamemode(gameMode mode);

	const UniformView& get_view() const;

	void frame_update(float dt);

public:
	virtual BasicHitbox& get_hitbox() =0;

private:
	glm::vec3 nohbx_move(float dt);
	void move_mayfloaterror(float dt);
	void hit_ground();
	void hit_object(const glm::vec3& normalN);
	void leave_ground();

private:
	UniformView   _view;
	Joystick      _creative_translator;
	Joystick2D    _natural_translator;
	MouseJoystick _head;

	gameMode _mode;
	bool _collision_enable;

	bool  _onground;
	float _gravity;
	float _jump_impulse;

	glm::vec3 _position;
	glm::vec3 _vspeed_natural;
	glm::vec3 _vspeed;
	glm::vec3 _acceleration;

	float _speed;
	float _mass;
	PointerTarget _selftarget;

	const HitboxContext* _context;

	float _safe_collision_space;

public:
	//FOR DEBUGGING
	glm::vec3     _falsepos;

	void set_false_target(bool falsetarget);
};

#endif //_PLAYER_HPP_
