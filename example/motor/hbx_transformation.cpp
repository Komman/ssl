#include "hbx_transformation.hpp"

#include <iostream>
#include <glm/gtx/string_cast.hpp>

hbxTransformation hbxTransformation::operator+(const hbxTransformation& transformation) const
{
	return hbxTransformation{
		.translation = transformation.rotation*(this->translation*transformation.scale) + transformation.translation,
		.rotation    = this->rotation*transformation.rotation,
		.scale       = this->scale*transformation.scale
	};
}

bool hbxTransformation::operator!=(const hbxTransformation& transformation) const
{
	return !((*this) == transformation);
}

bool hbxTransformation::operator==(const hbxTransformation& transformation) const
{
	return (this->translation == transformation.translation
		 && this->scale       == transformation.scale
		 && this->rotation    == transformation.rotation);
}

glm::vec3 hbxTransformation::transform(const glm::vec3& point) const
{
	return this->translation + this->rotation*(point*this->scale);
}

spaceBox hbxTransformation::transform(const spaceBox& space_box) const
{
	//TOOPT: I think it could be optimized here

	glm::vec3 newmin = this->translation + this->rotation*(space_box.min*this->scale);
	glm::vec3 newmax = this->translation + this->rotation*(space_box.max*this->scale);
	glm::vec3 diago  = (space_box.max - space_box.min)*this->scale;

	glm::vec3 pts[8] = {
		newmin,
		newmin+(this->rotation[0])*diago.x,
		newmin+(this->rotation[1])*diago.y,
		newmin+(this->rotation[2])*diago.z,
		newmax,
		newmax-(this->rotation[0])*diago.x,
		newmax-(this->rotation[1])*diago.y,
		newmax-(this->rotation[2])*diago.z,
	};

	return spaceBox{
		.min = glm::min(glm::min(glm::min(pts[0], pts[1]), glm::min(pts[2], pts[3])), glm::min(glm::min(pts[4+0], pts[4+1]), glm::min(pts[4+2], pts[4+3]))),
		.max = glm::max(glm::max(glm::max(pts[0], pts[1]), glm::max(pts[2], pts[3])), glm::max(glm::max(pts[4+0], pts[4+1]), glm::max(pts[4+2], pts[4+3])))
	};
}

std::string hbxTransformation::to_string() const
{
	return glm::to_string(translation);
}

