#ifndef _HITBOX_FUSION_HPP_
#define _HITBOX_FUSION_HPP_

#include "../utils/geom.hpp"
#include "basic_hitbox.hpp"
#include "collisions.hpp"
#include "primary_hitbox.hpp"

class HitboxFusion : public BasicHitbox
{
public:
	HitboxFusion();
	HitboxFusion(const glm::vec3& translation);
	HitboxFusion(const hbxTransformation& transformation);

	// Returns a ptr to the fusionned hitbox 
	template<typename HitboxType, typename... ConstructorArgs>
	BasicHitbox* fusion_hitbox(ConstructorArgs&&... args);
	BasicHitbox* fusion_hitbox(storedHitbox&& hbx);
	
	void fusion_symmetric(const infinitePlan& plan);

	// No need to call this functions, this class handles every absolute changes
	void invalidate_absolute() const = delete;

	storedHitbox copy() const override;	

	const std::vector<BasicHitbox::storedHitbox>& subhitboxes() const;
	std::vector<const BasicHitbox*> get_primary_hitboxes() const;
	

protected:
	spaceBox compute_shape_relative_space_box() const override;
	
	bool in_shape_collision(const BasicHitbox&  hbx) const override;
	bool in_shape_collision(const SphereHitbox& hbx) const override;
	bool in_shape_collision(const PrismHitbox&  hbx) const override;

	collisionImpact shape_impact(const BasicHitbox& hitbox , const glm::vec3& self_directionN, float distance) const override;
	collisionImpact shape_impact(const SphereHitbox& hitbox, const glm::vec3& self_directionN, float distance) const override;
	collisionImpact shape_impact(const PrismHitbox& hitbox , const glm::vec3& self_directionN, float distance) const override;

	void shape_invalidate_absolute() const override;
	bool is_absolute_valid() const;

private:
	template<typename HitboxType>
	bool subcalls_in_collision(const HitboxType& hbx) const;
	template<typename HitboxType>
	collisionImpact subcalls_impact(const HitboxType& hbx, const glm::vec3& self_directionN, float distance) const;

private:
	std::vector<storedHitbox> _subhitboxes;
};


template<typename HitboxType>
bool HitboxFusion::subcalls_in_collision(const HitboxType& hbx) const
{
	if(_subhitboxes.size() == 0)
	{
		return false;
	}

	for(const auto& shbx : _subhitboxes)
	{
		if(shbx->in_collision(hbx))
		{
			return true;
		}
	}

	return false;
}

template<typename HitboxType>
collisionImpact HitboxFusion::subcalls_impact(const HitboxType& hbx, const glm::vec3& self_directionN, float distance) const
{
	if(_subhitboxes.size() == 0)
	{
		return geom::NO_IMPACT;
	}

	collisionImpact ret = geom::NO_IMPACT;

	for(const auto& h : _subhitboxes)
	{
		auto himp = h->impact(hbx, self_directionN, distance);

		if(himp.impact)
		{
			if(ret.impact)
			{
				bool before = himp.distance < ret.distance;

				ret = before ? himp : ret;
			}
			else
			{
				ret = himp;
			}
		}
	}

	return ret;
}	

template<typename HitboxType, typename... ConstructorArgs>
BasicHitbox* HitboxFusion::fusion_hitbox(ConstructorArgs&&... args)
{
	auto h = std::make_unique<HitboxType>(args...);
	return this->fusion_hitbox((storedHitbox&&)(std::move(h)));
}


#endif //_HITBOX_FUSION_HPP_
