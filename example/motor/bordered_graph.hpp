#ifndef _BORDERED_GRAPH_HPP_
#define _BORDERED_GRAPH_HPP_

#include <unordered_set>

#include "../utils/plan_graph.hpp"

/*
	A BorderedGraph is a graph that stores
	a sequence of vertices called "border".
*/

class BorderedGraph : public PlanGraph
{
public:
	BorderedGraph(const PlanGraph& g, const std::vector<uint>& border);
	BorderedGraph(const BorderedGraph& g);

	bool in_border(uint v);
	bool in_border(const Edge& e);

	// Except for border ones
	void remove_crossing_edges();
	
	/*
		Add a cycle on its border passed in the
		constructor, and removes every edges that
		touch the border and that are not IN the
		area delimited buy the border.

		This operation keeps planarity.
	*/
	void apply_border();

	const std::vector<uint>& get_border() const; 
	const std::unordered_set<uint>& get_borderset() const;

private:
	void compute_borderset();

private:
	std::vector<uint> _border;

	mutable std::unordered_set<uint> _borderset;
};


#endif //_BORDERED_GRAPH_HPP_
