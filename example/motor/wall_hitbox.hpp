#ifndef _WALL_HITBOX_HPP_
#define _WALL_HITBOX_HPP_

#include "shaped_hitbox_fusion.hpp"

struct wallInfo
{
	std::vector<glm::vec3> excycle;
	std::vector<glm::vec3> incycle;
	glm::vec3 sized_direction;
};

class WallHitbox : public ShapedHitboxFusion<wallInfo>
{
public:
	WallHitbox(const wallInfo& shape);

private:
	void build();
};


#endif //_WALL_HITBOX_HPP_
