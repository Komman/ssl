#include "collisions.hpp"
#include "motor_debug.hpp"
#include "../utils/geom.hpp"

#warning TO DELETE HERE
#include "../game/effects.hpp"
#include "debug_draw.hpp"
#include "window_console.hpp"


using namespace glm;

namespace collisions
{
	/*
		MINMAX COORD
	*/

	template<>
	spaceBox minmax_coord<hitbox_primary::SPHERE>(const shapeInfo<hitbox_primary::SPHERE>& shape)
	{
		return {
			.min = shape.center - vec3(shape.radius),
			.max = shape.center + vec3(shape.radius),
		};
	}

	template<>
	spaceBox minmax_coord<hitbox_primary::PRISM>(const shapeInfo<hitbox_primary::PRISM>& shape)
	{
		spaceBox box;

		box.min = shape.points[0];
		box.min = glm::min(box.min, shape.points[1]);
		box.min = glm::min(box.min, shape.points[2]);
		box.min = glm::min(box.min, shape.points[3]);
		box.max = shape.points[0];
		box.max = glm::max(box.max, shape.points[1]);
		box.max = glm::max(box.max, shape.points[2]);
		box.max = glm::max(box.max, shape.points[3]);

		return box;
	}


	/*
		SYMMETRICAL
	*/

	template<>
	shapeInfo<hitbox_primary::SPHERE> symmetrical<hitbox_primary::SPHERE>(const shapeInfo<hitbox_primary::SPHERE>& shape, const infinitePlan& sym_plan)
	{
		shapeInfo<hitbox_primary::SPHERE> s;
		s.center = geom::symmetrical(shape.center, sym_plan);
		return s;
	}

	template<>
	shapeInfo<hitbox_primary::PRISM> symmetrical<hitbox_primary::PRISM>(const shapeInfo<hitbox_primary::PRISM>& shape, const infinitePlan& sym_plan)
	{
		shapeInfo<hitbox_primary::PRISM> p;
		for(uint i=0; i<4; i++)
		{
			p.points[i] = geom::symmetrical(shape.points[i], sym_plan);
		}
		return p;
	}

	/*
		HOMEOTETHIES
	*/

	template<>
	void homothety<hitbox_primary::SPHERE>(shapeInfo<hitbox_primary::SPHERE>& shape, const glm::vec3& center, float coeff)
	{
		shape.center = geom::homothety(shape.center, center, coeff);
		shape.radius = shape.radius*coeff;
	}

	template<>
	void homothety<hitbox_primary::PRISM>(shapeInfo<hitbox_primary::PRISM>& shape, const glm::vec3& center, float coeff)
	{
		for(uint i=0; i<4; i++)
		{
			shape.points[i] = geom::homothety(shape.points[i], center, coeff);
		}
	}


	/*
		ROTATE
	*/

	template<>
	void rotate<hitbox_primary::SPHERE>(shapeInfo<hitbox_primary::SPHERE>& shape,  const glm::mat3& rotation)
	{
		shape.center = rotation*shape.center;
	}

	template<>
	void rotate<hitbox_primary::PRISM>(shapeInfo<hitbox_primary::PRISM>& shape,  const glm::mat3& rotation)
	{
		for(uint i=0; i<4; i++)
		{
			shape.points[i] = rotation*shape.points[i];
		}
	}


	/*
		TP
	*/

	template<>
	void tp<hitbox_primary::SPHERE>(shapeInfo<hitbox_primary::SPHERE>& shape, const glm::vec3& ref_pos, const glm::vec3& pos)
	{
		shape.center = pos+(shape.center-ref_pos);
	}


	template<>
	void tp<hitbox_primary::PRISM>(shapeInfo<hitbox_primary::PRISM>& shape, const glm::vec3& ref_pos, const glm::vec3& pos)
	{
		for(uint i=0; i<4; i++)
		{
			shape.points[i] = pos + (shape.points[i] - ref_pos);
		}
	}


	/*
		TRANSFORM
	*/

	template<>
	void transform<hitbox_primary::SPHERE>(shapeInfo<hitbox_primary::SPHERE>& shape, const hbxTransformation& trans)
	{
		collisions::homothety<hitbox_primary::SPHERE>(shape, vec3(0), trans.scale);
		collisions::rotate<hitbox_primary::SPHERE>(shape, trans.rotation);
		collisions::tp<hitbox_primary::SPHERE>(shape, vec3(0), trans.translation);
	}


	template<>
	void transform<hitbox_primary::PRISM>(shapeInfo<hitbox_primary::PRISM>& shape, const hbxTransformation& trans)
	{
		collisions::homothety<hitbox_primary::PRISM>(shape, vec3(0), trans.scale);
		collisions::rotate<hitbox_primary::PRISM>(shape, trans.rotation);
		collisions::tp<hitbox_primary::PRISM>(shape, vec3(0), trans.translation);
	}

	/*
		CENTERS
	*/

	template<>
	glm::vec3 barycenter<hitbox_primary::SPHERE>(const shapeInfo<hitbox_primary::SPHERE>& shape)
	{
		return shape.center;
	}

	template<>
	glm::vec3 barycenter<hitbox_primary::PRISM>(const shapeInfo<hitbox_primary::PRISM>& shape)
	{
		return (shape.points[0] + shape.points[1] + shape.points[2] + shape.points[3])/4.0f;
	}

	/*
		COLLISIONS
	*/

	template<>
	bool primary_collision<hitbox_primary::SPHERE, hitbox_primary::SPHERE>(const shapeInfo<hitbox_primary::SPHERE>& shape1,const shapeInfo<hitbox_primary::SPHERE>& shape2)
	{
		return (glm::distance(shape1.center, shape2.center) <= (shape1.radius + shape2.radius));
	}

	template<>
	bool primary_collision<hitbox_primary::SPHERE, hitbox_primary::PRISM>(const shapeInfo<hitbox_primary::SPHERE>& shape1,const shapeInfo<hitbox_primary::PRISM>& shape2)
	{
		for(uint i=0; i<6; i++)
		{
			if(geom::segment_sphere_collision(shape2.points[geom::PRISM_EDGES[i][0]],
											  shape2.points[geom::PRISM_EDGES[i][1]],
											  shape1))
			{
				return true;
			}
		}

		for(uint i=0; i<4; i++)
		{
			vec3 pr = geom::plan_projection(shape1.center,
										    shape2.points[geom::PRISM_FACES[i][0]],
										    shape2.points[geom::PRISM_FACES[i][1]],
										    shape2.points[geom::PRISM_FACES[i][2]]);
			
			// effects::clouds().add_particle(Particle(pr,
			// 								 	  utils::random_vec3(vec3(-1.0f),vec3(1.0f))*0.2f,
			// 									  vec3(0),
			// 									  vec4(0,1,0,2),
			// 									  0.3,
			// 									  0.1,
			// 									  1));
			vec3 to_proj = pr - shape1.center;

			float h = glm::length(to_proj);

			if(h <= shape1.radius)
			{
				if(geom::point_in_triangle(shape1.center, 
										   shape2.points[geom::PRISM_FACES[i][0]],
										   shape2.points[geom::PRISM_FACES[i][1]],
										   shape2.points[geom::PRISM_FACES[i][2]]))
				{
					// effects::clouds().add_particle(Particle(pr,
					// 						 	  utils::random_vec3(vec3(-1.0f),vec3(1.0f))*0.2f,
					// 							  vec3(0),
					// 							  vec4(0,0,1,2),
					// 							  0.3,
					// 							  0.1,
					// 							  1));
					return true;
				}
			}

			if(geom::point_in_prism(shape1.center, shape2))
			{
				return true;
			}
		}

		return false;
	}

	template<>
	bool primary_collision<hitbox_primary::PRISM, hitbox_primary::SPHERE>(const shapeInfo<hitbox_primary::PRISM>& shape1,const shapeInfo<hitbox_primary::SPHERE>& shape2)
	{
		return primary_collision<hitbox_primary::SPHERE, hitbox_primary::PRISM>(shape2, shape1);
	}

	bool collision_prism_prism_uggly_implementation(const shapeInfo<hitbox_primary::PRISM>& shape1,const shapeInfo<hitbox_primary::PRISM>& shape2)
	{
		if(geom::point_in_prism(shape1.points[0], shape2))
		{
			return true;
		}
		
		#warning Function not fully implemented
		for(uint line=0; line<6; line++)
		{
			for(uint face=0; face<4; face++)
			{
				vec3 triangle[3] = {
					shape2.points[geom::PRISM_FACES[face][0]],
					shape2.points[geom::PRISM_FACES[face][1]],
					shape2.points[geom::PRISM_FACES[face][2]]
				};
				
				if(geom::segment_triangle_collision(shape1.points[geom::PRISM_EDGES[line][0]],
													shape1.points[geom::PRISM_EDGES[line][1]],
													triangle))
				{
					return true;
				}
			}
		}

		return false;
	}

	template<>
	bool primary_collision<hitbox_primary::PRISM, hitbox_primary::PRISM>(const shapeInfo<hitbox_primary::PRISM>& shape1,const shapeInfo<hitbox_primary::PRISM>& shape2)
	{
		return    collision_prism_prism_uggly_implementation(shape1, shape2)
			   || collision_prism_prism_uggly_implementation(shape2, shape1);
	}


	/*
		IMPACTS
	*/

	template<>
	collisionImpact primary_impact<hitbox_primary::SPHERE, hitbox_primary::SPHERE>(const shapeInfo<hitbox_primary::SPHERE>& shape1, const glm::vec3& self_directionN, float distance, const shapeInfo<hitbox_primary::SPHERE>& shape2)
	{
		float B = 2.0f*glm::dot(shape1.center - shape2.center, self_directionN);
		float C = glm::distance(shape1.center, shape2.center);
		float R = shape1.radius + shape2.radius;
		C = C*C - R*R;
		float delta = B*B - 4.0f*C;

		collisionImpact ret;
		if(delta < 0)
		{
			ret.impact = false;
		}
		else
		{		
			float x = (-B-sqrt(delta))/2.0f;

			if(x > distance)
			{
				ret.impact  = false;
			}
			else
			{
				ret.impact   = true;
				ret.distance = x;
				
				glm::vec3 colsphere_center = shape1.center + self_directionN*x;
				glm::vec3 dirN = shape2.center - colsphere_center;

				if(glm::length(dirN) <= 0.0001)
				{
					ret.point = colsphere_center;
					ret.normalN = -self_directionN;
				}
				else
				{
					dirN = glm::normalize(dirN);

					ret.point   = colsphere_center + dirN*shape1.radius;
					ret.normalN = -dirN;
				}
			}						
		}	

		return ret;
	}

	template<>
	collisionImpact primary_impact<hitbox_primary::SPHERE, hitbox_primary::PRISM>(const shapeInfo<hitbox_primary::SPHERE>& sphere, const glm::vec3& self_directionN, float distance, const shapeInfo<hitbox_primary::PRISM>& prism)
	{
		collisionImpact ret = geom::NO_IMPACT;
		for(uint i=0; i<4; i++)
		{
			// collisionImpact impact = geom::prism_face_normal();
			infinitePlan plan = {
				.point   = prism.points[geom::PRISM_FACES[i][0]], 
				.normalN = geom::prism_face_normal(prism, i)
			};
			collisionImpact impact = geom::sphere_plan_impact(sphere, plan, self_directionN, distance);
			
			vec3 triangle[3] = {
				prism.points[geom::PRISM_FACES[i][0]],
				prism.points[geom::PRISM_FACES[i][1]],
				prism.points[geom::PRISM_FACES[i][2]],
			};	

			if(geom::point_in_triangle(impact.point, triangle))
			{
				ret = geom::first_impact(ret, impact);
			}
		}
		for(uint i=0; i<6; i++)
		{	
			collisionImpact impact = geom::sphere_segment_impact_noextrm(sphere, prism.points[geom::PRISM_EDGES[i][0]], prism.points[geom::PRISM_EDGES[i][1]], self_directionN, distance);
			
			ret = geom::first_impact(ret, impact);
		}
		for(uint i=0; i<4; i++)
		{
			collisionImpact impact = geom::sphere_point_impact(sphere, prism.points[i], self_directionN*distance);
		
			ret = geom::first_impact(ret, impact);
		}
		return ret;
	}

	template<>
	collisionImpact primary_impact<hitbox_primary::PRISM, hitbox_primary::SPHERE>(const shapeInfo<hitbox_primary::PRISM>& shape1, const glm::vec3& self_directionN, float distance, const shapeInfo<hitbox_primary::SPHERE>& shape2)
	{
		collisionImpact ret = primary_impact<hitbox_primary::SPHERE, hitbox_primary::PRISM>(shape2, -self_directionN, distance, shape1);
		return geom::opposite_impact(ret);
	}

	collisionImpact primary_impact_uggly_implementation(const shapeInfo<hitbox_primary::PRISM>& shape1, const glm::vec3& self_directionN, float distance, const shapeInfo<hitbox_primary::PRISM>& shape2, bool edgetest)
	{
		collisionImpact ret = geom::NO_IMPACT;


		// Points against face
		for(uint i=0; i<4; i++)
		{
			for(uint f=0; f<4; f++)
			{
				vec3 triangle[3] = {
						shape2.points[geom::PRISM_FACES[f][0]],
						shape2.points[geom::PRISM_FACES[f][1]],
						shape2.points[geom::PRISM_FACES[f][2]]
				};
				infinitePlan faceplan = {
					.point   = shape2.points[geom::PRISM_FACES[f][0]],
					.normalN = geom::plan_normal(triangle, shape2.points[f])
				};
				collisionImpact impact = geom::point_plane_impact(shape1.points[i], self_directionN, distance, faceplan);
					
				if(geom::point_in_triangle(impact.point, triangle)
				&& impact.distance >= 0
				&& impact.distance <= distance)
				{
					ret = geom::first_impact(ret, impact);
				}
			}
		}

		if(!edgetest)
		{
			return ret;
		}

		// Edge against edges
		for(uint es=0; es<6; es++)
		{
			for(uint ed=0; ed<6; ed++)
			{
				vec3 s1 = shape1.points[geom::PRISM_EDGES[es].x];
				vec3 s2 = shape1.points[geom::PRISM_EDGES[es].y];
				
				vec3 d1 = shape2.points[geom::PRISM_EDGES[ed].x];
				vec3 d2 = shape2.points[geom::PRISM_EDGES[ed].y];
			
				directedLine3D ls = {
					s1,
					geom::safe_normalize(s2 - s1)
				};
				directedLine3D ld = {
					d1,
					geom::safe_normalize(d2 - d1)
				};

				collisionImpact impact = geom::line_line_impact(ls, ld, self_directionN);
			
				if(impact.impact && impact.distance==0)
				{
					// If (impact.distance==0), lines are aligned
					ret.distance = glm::min(
						glm::distance(ret.point, shape1.points[geom::PRISM_EDGES[es][0]]),
						glm::distance(ret.point, shape1.points[geom::PRISM_EDGES[es][1]])
					);
					
					if(ret.distance < distance && geom::point_on_segment(d1, d2, ret.point))
					{
						ret = geom::first_impact_epsilonkeep(ret, impact, 0.0001f);
					}
				}
				else
				{
					// Else it is classical segment-segment collision
					vec3 impactSp = impact.point - self_directionN*impact.distance;
					vec3 impactDp = impact.point;

					if(impact.impact
					&& geom::point_on_segment(s1, s2, impactSp)
					&& geom::point_on_segment(d1, d2, impactDp)
					&& impact.distance >= 0
					&& impact.distance <= distance)
					{					
						ret = geom::first_impact_epsilonkeep(ret, impact, 0.0001f);
					}
				}
			}
		}


		return ret;
	}

	template<>
	collisionImpact primary_impact<hitbox_primary::PRISM, hitbox_primary::PRISM>(const shapeInfo<hitbox_primary::PRISM>& shape1, const glm::vec3& self_directionN, float distance, const shapeInfo<hitbox_primary::PRISM>& shape2)
	{
		collisionImpact i1 = primary_impact_uggly_implementation(shape1, self_directionN, distance, shape2, true);
		collisionImpact i2 = geom::opposite_impact(primary_impact_uggly_implementation(shape2, -self_directionN, distance, shape1, false));

		return geom::first_impact(i1, i2);
	}
};
