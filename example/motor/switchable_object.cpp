#include "switchable_object.hpp"

#include <iostream>

SwitchableObject::SwitchableObject(const SwitchableObject* const* switcher)
	: _switcher(switcher)
{

}

bool SwitchableObject::enable() const
{
	return ((*_switcher) == this);
}

const SwitchableObject* const* SwitchableObject::get_switcher() const
{
	return _switcher;
}

