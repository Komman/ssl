#ifndef _GEO_CYLINDER_HITBOX_HPP_
#define _GEO_CYLINDER_HITBOX_HPP_

#include "shaped_hitbox_fusion.hpp"
#include "bevel_hitbox.hpp"

struct geoCylinderInfo
{
	std::vector<glm::vec3> cycle;
	glm::vec3 sized_direction;
};

class GeoCylinderHitbox : public ShapedHitboxFusion<geoCylinderInfo>
{
public:
	// The shape is composed of bevels centered on the barycenter of the cycle
	GeoCylinderHitbox(const geoCylinderInfo& shape);

private:
	void build();
};

#endif //_GEO_CYLINDER_HITBOX_HPP_
