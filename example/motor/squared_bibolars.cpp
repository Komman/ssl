#include "squared_bibolars.hpp"

#include <glm/gtx/string_cast.hpp>

using namespace std;
using namespace glm;
using namespace squareBibolarsBuffers;

SquaredBibolars::SquaredBibolars(const std::string& name)
	: SquaredBibolars(name, {})
{

}

SquaredBibolars::SquaredBibolars(const std::string& name, const std::vector<squareBibolar>& initial_instances)
	: TimedInstances(name, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW)
{
	this->get_array_buffer<RELATIVE_POS>().add_value(vec2(+1.0f, +1.0f));
	this->get_array_buffer<RELATIVE_POS>().add_value(vec2(+1.0f, -1.0f));
	this->get_array_buffer<RELATIVE_POS>().add_value(vec2(-1.0f, -1.0f));
	this->get_array_buffer<RELATIVE_POS>().add_value(vec2(-1.0f, +1.0f));

	_elements.add_rectangle(0,1,2,3);

	for(const auto& inst : initial_instances)
	{
		this->add_instance(inst);
	}
}

void SquaredBibolars::add_retarded_instance(const squareBibolar& bib, float retard)
{
	this->get_array_buffer<CENTER>().add_value(bib.center);
	this->get_array_buffer<SIZE_U_STIME_U_DURATION>().add_value(
		glm::vec4(bib.size.x, bib.size.y, this->get_time() + retard, bib.duration)
	);
	this->get_array_buffer<COLOR>().add_value(bib.color);
}


void SquaredBibolars::add_instance_in_buffers(const squareBibolar& bib)
{
	this->add_retarded_instance(bib, 0.0f);
}

float SquaredBibolars::get_instance_start_time(uint instance_index) const
{
	return this->get_array_buffer<SIZE_U_STIME_U_DURATION>().get_subvalue(instance_index).z;
}

float SquaredBibolars::get_instance_duration(uint instance_index)   const
{
	return this->get_array_buffer<SIZE_U_STIME_U_DURATION>().get_subvalue(instance_index).w;
}

void SquaredBibolars::print() const
{
	cout<<"SquaredBibolars: {insts: "<<this->instances_amount()<<endl;
	for(uint i=0; i<this->instances_amount(); i++)
	{
		cout<<" {center: "<<glm::to_string(this->get_array_buffer<CENTER>().get_subvalue(i));
		cout<<", infos: "<<glm::to_string(this->get_array_buffer<SIZE_U_STIME_U_DURATION>().get_subvalue(i));

		cout<<"}"<<endl;
	}
	cout<<"}"<<endl;
}

