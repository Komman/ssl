#ifndef _RELIEFED_PLAN_DUAL_GRAPH_HPP_
#define _RELIEFED_PLAN_DUAL_GRAPH_HPP_

#include <unordered_map>

#include "../utils/plan_dual_graph.hpp"

class ReliefedPlanDualGraph : public PlanDualGraph
{
public:
	ReliefedPlanDualGraph();
	ReliefedPlanDualGraph(const PlanDualGraph& g);

	void add_vertex(const planDualFace& f, float h);

	float face_height(uint face) const;
	void set_face_height(uint face, float h);

private:
	std::unordered_map<uint, float> _heights;

};


#endif //_RELIEFED_PLAN_DUAL_GRAPH_HPP_
