#include "shared_ressources.hpp"

using namespace glm;
using namespace std;

SharedRessources::SharedRessources(const glm::vec4&      init_background_color,
					 			   const bloomParameter& bloom_params)
	: _textures(),
	  _framebuffers(_textures),
	  _tools(_textures, _framebuffers, init_background_color, bloom_params)
{

}

SharedRessources::SharedTextures::SharedTextures()
	: depth_buffer(window::size()),
	  depth_bufferMS(window::size()),
	  frame_texture("shared_frame_texture", window::size(), GL_LINEAR),
	  blending_texture("shared_blending_texture", window::size(), GL_LINEAR),
	  bloom_texture("shared_bloom_texture", window::size(), GL_LINEAR, GL_CLAMP_TO_BORDER),
	  frame_textureMS("shared_frame_textureMS", window::size()),
	  frame_bloomMS("shared_bloom_textureMS", window::size())
{

}


SharedRessources::SharedFramebuffers::SharedFramebuffers(SharedTextures& textures)
	: frameMS(&textures.depth_bufferMS, &textures.frame_textureMS),
	  frame(&textures.depth_buffer, &textures.frame_texture),
	  frame_blending(&textures.depth_buffer, &textures.blending_texture),
	  frame_blending_and_bloom(&textures.depth_buffer, &textures.blending_texture, &textures.bloom_texture),
	  frame_and_bloom(&textures.depth_buffer, &textures.frame_texture, &textures.bloom_texture),
	  frame_and_bloomMS(&textures.depth_bufferMS, &textures.frame_textureMS, &textures.frame_bloomMS),
	  bloom_frame(&textures.depth_buffer, &textures.bloom_texture),
	  bloom_frameMS(&textures.depth_bufferMS, &textures.frame_bloomMS)
{

}

SharedRessources::SharedTools::SharedTools(SharedTextures& textures,
										   SharedFramebuffers& framebuffers,
										   const glm::vec4&      init_background_color,
					 					   const bloomParameter& bloom_params)
	: samples_amount_MS("MS_samples_amont", ANTIALIASING),
	  background_color("background_color", init_background_color),
	  bloomer(textures.bloom_texture, "bloom_", "shared_bloom_processed_texture", bloom_params),
	  post_processor(init_background_color, bloomer, {
	  	.frameMS                  = &(framebuffers.frameMS),
		.frame                    = &(framebuffers.frame),
		.frame_blending           = &(framebuffers.frame_blending),
		.frame_blending_and_bloom = &(framebuffers.frame_blending_and_bloom),
		.frame_and_bloom          = &(framebuffers.frame_and_bloom),
		.frame_and_bloomMS        = &(framebuffers.frame_and_bloomMS),
		.bloom_frame              = &(framebuffers.bloom_frame),
		.bloom_frameMS            = &(framebuffers.bloom_frameMS)}),
	  game_drawer(post_processor)
{

}

SharedRessources::SharedTextures& SharedRessources::textures()
{
	return _textures;
}

SharedRessources::SharedFramebuffers& SharedRessources::framebuffers()
{
	return _framebuffers;
}

SharedRessources::SharedTools& SharedRessources::tools()
{
	return _tools;
}
