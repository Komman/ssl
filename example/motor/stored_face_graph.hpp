#ifndef _STORED_FACE_GRAPH_HPP_
#define _STORED_FACE_GRAPH_HPP_

#include "bordered_graph.hpp"

template<typename FaceGraphType, typename StorageType>
class StoredFaceGraph : public FaceGraphType
{
public:
	StoredFaceGraph(const BorderedGraph& g);

	const StorageType& get_storage(uint face) const;
	void store(uint face, StorageType&& role);

protected:
	StorageType& value(uint face);	

private:
	std::vector<StorageType> _storage;	
};


template<typename FaceGraphType, typename StorageType>
StoredFaceGraph<FaceGraphType, StorageType>::StoredFaceGraph(const BorderedGraph& g)
	: FaceGraphType(g),
	  _storage()
{
	for(uint i=0; i<this->all_faces().size(); i++)
	{
		_storage.push_back(StorageType());
	}
}

template<typename FaceGraphType, typename StorageType>
const StorageType& StoredFaceGraph<FaceGraphType, StorageType>::get_storage(uint face) const
{
	this->check_face(face);

	return _storage[face];
}

template<typename FaceGraphType, typename StorageType>
void StoredFaceGraph<FaceGraphType, StorageType>::store(uint face, StorageType&& role)
{
	this->check_face(face);

	_storage[face] = std::move(role);
}

template<typename FaceGraphType, typename StorageType>
StorageType& StoredFaceGraph<FaceGraphType, StorageType>::value(uint face)
{
	this->check_face(face);

	return _storage[face];
}


#endif //_STORED_FACE_GRAPH_HPP_
