#ifndef _MESHED_RAY_LUMINOR_HPP_
#define _MESHED_RAY_LUMINOR_HPP_

#include "../motor/stored_vao_object.hpp"

#include "stored_drawable_object.hpp"

#include <memory>
#include <unordered_set>

using namespace ssl;

class MeshedRayLuminor;

class BasicMeshedRayLight
{
public:
	struct rayVertex
	{
		glm::vec3 position;
		glm::vec3 color;
	};

public:
	BasicMeshedRayLight() {}
	virtual ~BasicMeshedRayLight(){}

	virtual const std::vector<rayVertex>& get_vertices() const =0;
	virtual const std::vector<uint>&      get_elements() const =0;

	virtual void animate(float dt) =0;
};

class StoredMeshedRayLight : public BasicMeshedRayLight
{
public:
	virtual bool has_expired() const =0;
};

class MeshedRayLight : public BasicMeshedRayLight
{
public:
	MeshedRayLight(MeshedRayLuminor& luminor);
	MeshedRayLight(MeshedRayLight&& ray);
	MeshedRayLight(MeshedRayLight& ray);
	virtual ~MeshedRayLight();

	void enable();
	void disable();

	bool is_enable() const;


private:
	MeshedRayLuminor& _luminor;
	bool _enable;
};

class MeshedRayLuminor : public StoredVAObject<StoredDrawableObject<glm::vec3, glm::vec3>>
{
public:
	MeshedRayLuminor();
	virtual ~MeshedRayLuminor() {}

	void animate(float dt);
	void add_ray(std::unique_ptr<StoredMeshedRayLight>&& ray);
	
	void control_ray(MeshedRayLight* ray);
	void uncontrol_ray(MeshedRayLight* ray);

	uint ray_amount() const;

private:
	void animate_rays(float dt);
	void delete_expired_rays();
	void build();

private:
	std::vector<std::unique_ptr<StoredMeshedRayLight>> _rays;

	std::unordered_set<MeshedRayLight*> _ext_rays;
};

#endif //_MESHED_RAY_LUMINOR_HPP_
