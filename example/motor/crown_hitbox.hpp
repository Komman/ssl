#ifndef _CROWN_HITBOX_HPP_
#define _CROWN_HITBOX_HPP_

#include "shaped_hitbox_fusion.hpp"
#include "paver_hitbox.hpp"

struct crownInfo
{
	glm::vec3 bottom_center;
	glm::vec3 to_top_center;
	glm::vec3 base_orientationN;
	float radius_start;
	float radius_end;
};

class CrownHitbox : public ShapedHitboxFusion<crownInfo>
{
public:
	// side_faces, must be >= 3
	CrownHitbox(const crownInfo& shape, uint side_faces);

private:
	void build();

private:
	uint _side_faces; 
};

#endif //_CROWN_HITBOX_HPP_
