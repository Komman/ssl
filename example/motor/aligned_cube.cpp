#include "aligned_cube.hpp"

using namespace std;
using namespace glm;


/*
	3----------2
	|\         |\
	| 0----------1
	| |        | |
	| |        | |
	7-|------- 6 |
	 \|         \|
	  4----------5

*/

AlignedCube::AlignedCube(const glm::vec3& position,
						 float size)
	: AlignedCube(position, vec3(size, size, size))
{

}

AlignedCube::AlignedCube(const glm::vec3& position,
						 const glm::vec3& size)
	: _position(position),
	  _size(size),
	  _points(STATIC_DRAW),
	  _buffers({&_points}),
	  _indices(STATIC_DRAW)
{
	vec3 dsize = size/2.0f;
	_points.add_value(position+vec3(-dsize.x, +dsize.y, -dsize.z));
	_points.add_value(position+vec3(+dsize.x, +dsize.y, -dsize.z));
	_points.add_value(position+vec3(+dsize.x, +dsize.y, +dsize.z));
	_points.add_value(position+vec3(-dsize.x, +dsize.y, +dsize.z));
	_points.add_value(position+vec3(-dsize.x, -dsize.y, -dsize.z));
	_points.add_value(position+vec3(+dsize.x, -dsize.y, -dsize.z));
	_points.add_value(position+vec3(+dsize.x, -dsize.y, +dsize.z));
	_points.add_value(position+vec3(-dsize.x, -dsize.y, +dsize.z));

	_indices.add_hexahedron({0,1,2,3,4,5,6,7});
}	

const std::vector<const BasicBuffer*>& AlignedCube::get_buffers()  const
{
	return _buffers;
}

const ElementBuffer& AlignedCube::get_elements() const
{
	return _indices;
}

const ArrayBuffer<glm::vec3>& AlignedCube::get_points()   const
{
	return _points;
}
