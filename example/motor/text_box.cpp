#include "text_box.hpp"

#include "motor_debug.hpp"
#include "image_loader.hpp"
#include "../../ssl/src/color.hpp"


static char _default_color = 7;

static glm::vec3 terms_colors[8] = {
	glm::vec3(0),
	glm::vec3(1,0,0),
	glm::vec3(0,1,0),
	glm::vec3(0.5,1,0.5),
	glm::vec3(0,0,1),
	glm::vec3(1,0,1),
	glm::vec3(0.5,0.5,1),
	glm::vec3(1),
};

using namespace glm;
using namespace std;

TextBox::TextBox(const std::string& text_bitmap_path, uint char_amount, const std::string& uniform_name, float spacing)
	: _char_amount(char_amount),
	  _max_lines(20),
	  _spacing(spacing),
	  _draw_type(uniform_name + "_drawtype", 0),
	  _text(uniform_name, char_amount, {}),
	  _box({vec2(0,0), vec2(1,0), vec2(1,1), vec2(0,1)}, DYNAMIC_DRAW),
	  _uvs({vec2(0,1), vec2(1,1), vec2(1,0), vec2(0,0)}, STATIC_DRAW),
	  _indices({0,1,2, 0,2,3}, STATIC_DRAW),
	  _bitmap("text_bmp", uvec2(1), GL_NEAREST, GL_CLAMP_TO_EDGE)
{
	ImageLoader textppm(text_bitmap_path);

	std::vector<float> colorsf = textppm.float_RGB();
	_bitmap.change_data(uvec3(textppm.size().x, textppm.size().y, 1), colorsf.data());
}

void TextBox::draw_box(Drawer& text_drawer, const glm::vec2& minpos, const glm::vec2& maxpos)
{
	// DRAW BOX
	this->set_box(glm::clamp(minpos, vec2(0), vec2(1)),
				  glm::clamp(maxpos, vec2(0), vec2(1)));

	_draw_type = 1;
	text_drawer.blending(true);
   	text_drawer.set_blend_function(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
   	text_drawer.set_blend_equation(GL_FUNC_ADD);
	text_drawer.draw_elements(_indices, _box, _uvs);
}

float TextBox::get_yratio() const
{
	return float(window::size().x)/float(window::size().y);
}

void TextBox::draw_stored_text(Drawer& text_drawer, const glm::vec2& screen_pos, float text_size)
{
	if(_text.get_value().size() == 0)
		return;

	this->set_box(glm::clamp(screen_pos, vec2(0), vec2(1)),
				  glm::clamp((screen_pos + glm::vec2(text_size*float(_text.get_value().size()), text_size*this->get_yratio())), vec2(0), vec2(1)));

	_draw_type = 0;
	text_drawer.blending(false);
	text_drawer.draw_elements(_indices, _box, _uvs);
}


void TextBox::draw_text(const std::string& text, Drawer& text_drawer, const glm::vec2& screen_pos, float text_size)
{
	this->set_uniform_text(text);
	this->draw_stored_text(text_drawer, screen_pos, text_size);	
}

void TextBox::draw_text(const std::vector<textCharGPU>& text, Drawer& text_drawer, const glm::vec2& screen_pos, float text_size)
{
	this->set_uniform_text_chars(text);
	this->draw_stored_text(text_drawer, screen_pos, text_size);	
}

void TextBox::draw_console(const WindowConsole& console, Drawer& text_drawer, const glm::vec2& screen_pos, float text_size)
{
	if(console.typing())
	{		
		this->draw_texts(console.msgs_ranged(
						 	std::max(0, (int)console.all_msgs().size()-(int)_max_lines),
						 	(int)console.all_msgs().size()
						 ),
						 text_drawer,
						 screen_pos + vec2(0,text_size*_spacing*this->get_yratio()),
						 text_size
		);
		this->draw_box(text_drawer, screen_pos, screen_pos + vec2(1, text_size*_spacing*this->get_yratio()));
		this->draw_text(console.current_text(), text_drawer, screen_pos, text_size);
	}
	else
	{
		this->draw_texts(console.msgs(), text_drawer, screen_pos + vec2(0,text_size*_spacing), text_size);
	}
		
}

void TextBox::draw_texts(const std::vector<std::string>& texts, Drawer& text_drawer, const glm::vec2& screen_pos, float text_size)
{
	if(texts.size() == 0)
		return;

	int maxlength = 0;
	std::vector<textCharGPU> gpuchars[texts.size()];

	for(uint i=0; i<texts.size(); i++)
	{
		gpuchars[i] = translate_string_gpu(texts[i]);

		if((int)gpuchars[i].size() > maxlength)
		{
			maxlength = gpuchars[i].size();
		}
	}

	this->draw_box(text_drawer, screen_pos, (screen_pos + vec2(float(maxlength), float(texts.size())*_spacing*this->get_yratio())*text_size));

	for(uint i=0; i<texts.size(); i++)
	{
		vec2 pos = screen_pos + vec2(0, (text_size*_spacing*this->get_yratio())*float(i));
		if(pos.y > 1)
		{
			break;
		}

		this->draw_text(gpuchars[i], text_drawer, pos, text_size);
	}
}

void TextBox::set_box(const glm::vec2& min_coord, const glm::vec2& max_coord)
{
	_box.change_value({
		vec2(min_coord.x, min_coord.y),
		vec2(max_coord.x, min_coord.y),
		vec2(max_coord.x, max_coord.y),
		vec2(min_coord.x, max_coord.y)
	});
}

void TextBox::set_uniform_text_chars(const std::vector<textCharGPU>& gpuchars)
{
	std::vector<glm::uvec2> values;
	for(uint i=0; i<gpuchars.size(); i++)
	{
		if(i >= _text.max_size())
		{
			break;
		}

		values.push_back(glm::uvec2(gpuchars[i].c, gpuchars[i].colorcode));
	}

	_text.change_value(values);
}


void TextBox::set_uniform_text(const std::string& str)
{
	std::vector<textCharGPU> gpuchars = translate_string_gpu(str);

	this->set_uniform_text_chars(gpuchars);
}


std::vector<textCharBox> TextBox::translate_string(const std::string& str)
{
	return translate_string(translate_string_gpu(str));
}

std::vector<textCharGPU> TextBox::translate_string_gpu(const std::string& str)
{
	std::vector<textCharGPU> ret;
	char color = _default_color;

	for(uint i=0; i<str.size(); i++)
	{
		int c = str[i];
		if(c != 27)
		{
			ret.push_back({str[i], color});
		}
		else
		{
			i+=3;

			if(i < str.size() && str[i] == 109)
			{
				color = _default_color;
			}
			else
			{
				i+=2;

				if(i < str.size())
				{
					int colocode = (int)(str[i]) - 48;

					if(colocode < 0 || colocode > 7)
					{
						mot::err("TextBox::translate_string_gpu(): wrong character");
					}

					color = colocode;
					i++;
				}
			}
		}
	}

	return ret;
}

std::vector<textCharBox> TextBox::translate_string(const std::vector<textCharGPU>& str)
{
	std::vector<textCharBox> ret;

	for(auto cc : str)
	{
		ret.push_back({cc.c, terms_colors[(int)cc.colorcode]});
	}

	return ret;
}

