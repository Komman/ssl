#ifndef _LINEAR_VALUATOR_HPP_
#define _LINEAR_VALUATOR_HPP_

#include "basic_timed_valuator.hpp"

template<typename T>
class LinearValuator : public BasicTimedValuator<T>
{
public:
	LinearValuator();

protected:
	T normal_evaluate(uint index, float percentage) const override;

};




template<typename T>
LinearValuator<T>::LinearValuator()
	: BasicTimedValuator<T>()
{

}

template<typename T>
T LinearValuator<T>::normal_evaluate(uint index, float percentage) const
{
	T prec = this->indexed_value(index + 0).value;
	T next = this->indexed_value(index + 1).value;
	
	return next*percentage + prec*(1.0f - percentage);
}



#endif //_LINEAR_VALUATOR_HPP_
