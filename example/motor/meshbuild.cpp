#include "meshbuild.hpp"

#include "../utils/mutils.hpp"
#include "../utils/geom.hpp"
#include "../utils/space_grid.hpp"

#include "motor_debug.hpp"

using namespace std;
using namespace glm;

namespace meshbuild
{
	partialEnlargementInfos<glm::vec3> partial_enlargement_infos(
		ssl::ArrayBuffer<glm::vec3>& vertices,
		const partialEnlargementIndexing& enlargement)
	{
		partialEnlargementInfos<glm::vec3> ret;

		std::unordered_map<uint, uint> mapping;

		for(uint v : enlargement.base.cycle)
		{
			mapping[v] = ret.points.size();
			ret.points.push_back(vertices[v]);
		}
		uint I = ret.points.size();

		for(uint v : enlargement.enlarged.cycle)
		{
			mapping[v] = ret.points.size();
			ret.points.push_back(vertices[v]);
		}

		if(I < 3 || ret.points.size() - I < 3)
		{
			mot::err("mshbuild::partial_enlargement_infos(): enlargement is not a cycle");
		}

		ret.base.cycle = utils::uirange(0, I-1);
		ret.base.edge  = enlargement.base.edge;

		ret.enlarged.cycle = utils::uirange(I, ret.points.size() - 1);
		ret.enlarged.edge  = enlargement.enlarged.edge;

		for(uint v : enlargement.outer_added)
		{
			auto it = mapping.find(v);

			if(it == mapping.end())
			{
				mot::err("meshbuild::partial_enlargement_infos(): a vertex is not on the base ou enlarged cycle");
			}

			ret.outer_added.push_back(it->second);
		}	

		for(uint v : enlargement.enlargement.outline.cycle)
		{
			auto it = mapping.find(v);

			if(it == mapping.end())
			{
				mot::err("meshbuild::partial_enlargement_infos(): a vertex is not on the base ou enlarged cycle");
			}

			ret.enlargement.outline.cycle.push_back(it->second);
		}	
		ret.enlargement.outline.edge = enlargement.enlargement.outline.edge;


		for(uint f=0; f<enlargement.enlargement.faces.size(); f++)
		{
			std::vector<uint> face;

			for(uint i=0; i<enlargement.enlargement.faces[f].size(); i++)
			{
				auto it = mapping.find(enlargement.enlargement.faces[f][i]);

				if(it == mapping.end())
				{
					mot::err("meshbuild::partial_enlargement_infos(): a vertex is not on the base ou enlarged cycle");
				}

				face.push_back(it->second);
			}
			ret.enlargement.faces.push_back(face);
		}	

		return ret;
	}

	partialEnlargement<glm::vec3> partial_enlargement(
		ssl::ArrayBuffer<glm::vec3>& vertices,
		const partialEnlargementIndexing& enlargement
	)
	{
		partialEnlargement<glm::vec3> ret;

		ret.outline = partial_cycle(vertices, enlargement.enlargement.outline);
		
		for(uint f=0; f<enlargement.enlargement.faces.size(); f++)
		{
			std::vector<glm::vec3> face;
			for(uint i=0; i<enlargement.enlargement.faces[f].size(); i++)
			{
				face.push_back(vertices[enlargement.enlargement.faces[f][i]]);
			}
			ret.faces.push_back(face);
		}

		return ret;
	}
	partialCycle<glm::vec3> partial_cycle(
		ssl::ArrayBuffer<glm::vec3>& vertices,
		const partialCycle<uint>& cycle)
	{
		partialCycle<glm::vec3> ret;

		ret.cycle = vertices.subset(cycle.cycle);
		ret.edge  = cycle.edge;
		ret.check();

		return ret;
	}

	static std::vector<uint> enlargement_to_vertices_mapping(
		ssl::ArrayBuffer<glm::vec3>& vertices,
		const partialCycle<uint>& original_cycle,
		const partialEnlargementInfos<glm::vec3>& en,
		bool only_outline)
	{
		// MAPPING FROM en.points INDEXING TO vertices ARRAY BUFFER INDEXING 
		std::vector<uint> mapping(en.points.size(), std::numeric_limits<uint>::max());

		if(only_outline)
		{
			for(uint i=0; i<en.outer_added.size(); i++)
			{
				mapping[en.outer_added[i]] = vertices.size();
				vertices.add_value(en.points[en.outer_added[i]]);
			}
		}
		else
		{
			for(uint i=0; i<en.enlarged.size(); i++)
			{
				mapping[en.enlarged.cycle[i]] = vertices.size();
				vertices.add_value(en.points[en.enlarged.cycle[i]]);
			}
		}
		for(uint i=0; i<en.base.size(); i++)
		{
			mapping[en.base.cycle[i]] = original_cycle.cycle[i];
		}

		return mapping;
	}

	static partialEnlargement<uint> map_enlargement_on_vertice(
		const partialEnlargementInfos<glm::vec3>& en,
		const std::vector<uint>& mapping)
	{
		partialEnlargement<uint> ret;

		ret.outline = en.enlargement.outline;
		ret.faces   = en.enlargement.faces;

		for(uint i=0; i<ret.outline.cycle.size(); i++)
		{
			uint tomap = ret.outline.cycle[i];

			#ifdef MOTOR_DEBUG
			if(tomap >= mapping.size() || mapping[tomap] == std::numeric_limits<uint>::max())
			{
				mot::err("meshbuild::map_enlargement_on_vertice(): mapping not complete");
			}
			#endif

			ret.outline.cycle[i] = mapping[tomap];
		}

		for(uint f=0; f<ret.faces.size(); f++)
		{
			for(uint i=0; i<ret.faces[f].size(); i++)
			{
				uint tomap = ret.faces[f][i];

				#ifdef MOTOR_DEBUG
				if(tomap >= mapping.size() || mapping[tomap] == std::numeric_limits<uint>::max())
				{
					mot::err("meshbuild::map_enlargement_on_vertice(): mapping not complete");
				}
				#endif

				ret.faces[f][i] = mapping[tomap];
			}
		}

		return ret;
	}

	static partialCycle<uint> map_partial_cycle(const partialCycle<uint>& cycle, const std::vector<uint>& mapping)
	{
		partialCycle<uint> ret;

		ret.cycle = utils::map_vector(cycle.cycle, mapping);
		ret.edge  = cycle.edge;

		return ret;
	}

	static partialEnlargementIndexing map_enlargement_infos_on_vertice(
		const partialEnlargementInfos<glm::vec3>& en,
		const std::vector<uint>& mapping)
	{
		partialEnlargementIndexing ret;

		ret.enlargement = map_enlargement_on_vertice(en, mapping);

		ret.outer_added = utils::map_vector(en.outer_added, mapping);
		ret.base     = map_partial_cycle(en.base, mapping);
		ret.enlarged = map_partial_cycle(en.enlarged, mapping);

		return ret;
	}

	partialEnlargement<uint> place_conspace_enlargement(
		ssl::ArrayBuffer<glm::vec3>& vertices,
		const partialCycle<uint>& cycle,
	    float distance,
	    float curveprecision_distance)
	{
		cycle.check();

		partialEnlargement<uint> ret;
		partialEnlargementInfos<glm::vec3> en = geom::conspace_enlarge_cycle(
			partial_cycle(vertices, cycle),
			distance,
			curveprecision_distance
		);

		#ifdef MOTOR_DEBUG
		if(en.base.size() != cycle.cycle.size())
		{
			mot::err("meshbuild::place_conspace_enlargement(): wrong conspace_enlarge_cycle cycle sizes: "
				+ std::to_string(en.base.size()) + " != " + std::to_string(cycle.cycle.size()));
		}
		#endif

		std::vector<uint> mapping = enlargement_to_vertices_mapping(vertices, cycle, en, true);

		return map_enlargement_on_vertice(en, mapping);
	}

	std::pair<uint, uint> extrude_cycle(ssl::ArrayBuffer<glm::vec3>& vertices,
								    	ssl::ElementBuffer& elements,
										const std::vector<uint>& cycle,
								    	const glm::vec3& sized_direction,
								    	bool flip_face)
	{
		if(cycle.size() == 0)
		{
			mot::err("meshbuil:extrude_cycle() cycle must be of size > 0");
		}
		uint firsti = vertices.size();

		for(uint i=0; i<cycle.size(); i++)
		{
			uint j = (i+1)%cycle.size();

			vertices.add_value(vertices[cycle[i]] + sized_direction);

			elements.add_rectangle(cycle[i], cycle[j], firsti + j, firsti + i, flip_face);
		}

		return {firsti, vertices.size()-1};
	}

	partialCycle<uint> extrude_cycle(ssl::ArrayBuffer<glm::vec3>& vertices,
								    	ssl::ElementBuffer& elements,
										const partialCycle<uint>& cycle,
								    	const glm::vec3& sized_direction,
								    	bool flip_face)
	{
		cycle.check();
		partialCycle<uint> ret;
		uint firsti = vertices.size();

		for(uint i=0; i<cycle.cycle.size(); i++)
		{
			uint j = (i+1)%cycle.cycle.size();

			ret.cycle.push_back(vertices.size());
			vertices.add_value(vertices[cycle.cycle[i]] + sized_direction);

			if(cycle.edge[i])
			{
				ret.edge.push_back(true);
				elements.add_rectangle(cycle.cycle[i], cycle.cycle[j], firsti + j, firsti + i, flip_face);
			}
			else
			{
				ret.edge.push_back(false);
			}
		}

		ret.check();

		return ret;
	}

	partialEnlargement<uint> extrude_cycle(
		ssl::ArrayBuffer<glm::vec3>& vertices,
    	ssl::ElementBuffer& elements,
		const partialEnlargement<uint>& cycle,
    	const glm::vec3& sized_direction,
    	bool flip_face
    )
    {
    	partialEnlargement<uint> ret;

    	ret.outline = extrude_cycle(
    		vertices,
    		elements,
    		cycle.outline,
    		sized_direction,
    		flip_face
    	);

    	if(ret.outline.size() != cycle.outline.size())
    	{
    		mot::err("meshbuil::extrude_cycle(): extrusion has added vertices");
    	}

    	// Mapping old indices to new
    	std::unordered_map<uint, uint> mapping;

    	for(uint i=0; i<ret.outline.cycle.size(); i++)
    	{
    		mapping[cycle.outline.cycle[i]] = ret.outline.cycle[i];
    	}

    	for(uint f=0; f<cycle.faces.size(); f++)
    	{
    		ret.faces.push_back({});

    		#ifdef MOTOR_DEBUG
    		if(cycle.faces[f].size() < 3)
    		{
    			mot::err("meshbuild::extrude_cycle(): a face has a size <3");
    		}
    		#endif

    		for(uint i=0; i<cycle.faces[f].size(); i++)
    		{
    			auto it = mapping.find(cycle.faces[f][i]);

    			if(it != mapping.end())
    			{
    				ret.faces.back().push_back(it->second);
    			}
    			else
    			{
    				uint index = vertices.size();
    				vertices.add_value(vertices[cycle.faces[f][i]] + sized_direction);

    				mapping[cycle.faces[f][i]] = index;
    				ret.faces.back().push_back(index);
    			}
    		}
    	}

		return ret;
    }

	std::pair<uint, uint> extrude_cycle(ssl::ArrayBuffer<glm::vec3>& vertices,
								    	ssl::ElementBuffer& elements,
										const std::pair<uint, uint>& cycle,
								    	const glm::vec3& sized_direction,
								    	bool flip_face)
	{
		return extrude_cycle(vertices, elements, utils::uirange(cycle.first, cycle.second), sized_direction, flip_face);
	}

	std::pair<uint, uint> extrude_cycle(ssl::ArrayBuffer<glm::vec3>& vertices,
								    	ssl::ElementBuffer& elements,
										const std::pair<uint, uint>& cycle,
								    	const std::vector<glm::vec3>& extrusion,
								    	bool flip_face)
	{
		return extrude_cycle(vertices, elements, utils::uirange(cycle.first, cycle.second), extrusion, flip_face);
	}

	std::pair<uint, uint> extrude_cycle(ssl::ArrayBuffer<glm::vec3>& vertices,
								    	ssl::ElementBuffer& elements,
										const std::vector<uint>& cycle,
								    	const std::vector<glm::vec3>& extrusion,
								    	bool flip_face)
	{
		if(cycle.size() == 0)
		{
			mot::err("meshbuil:extrude_cycle() cycle must be of size > 0");
		}
		if(cycle.size() != extrusion.size())
		{
			mot::err("meshbuild::extrude_cycle(): extrusion must have the same size than cycle");
		}
		uint firsti = vertices.size();

		for(uint i=0; i<cycle.size(); i++)
		{
			uint j = (i+1)%cycle.size();

			vertices.add_value(extrusion[i]);

			elements.add_rectangle(cycle[i], cycle[j], firsti + j, firsti + i, flip_face);
		}

		return {firsti, vertices.size()-1};
	}

	// Returns min and max indices of vertices added
	std::pair<uint, uint> bissectrice_enlarge_cycle(ssl::ArrayBuffer<glm::vec3>& vertices,
								    	ssl::ElementBuffer& elements,
										const std::vector<uint>& cycle,
								    	float distance,
								    	const glm::vec3& translation,
								    	bool flip_face)
	{
		if(cycle.size() == 0)
		{
			mot::err("meshbuil:bissectrice_enlarge_cycle() cycle must be of size > 0");
		}
		uint firsti = vertices.size();

		std::vector<glm::vec3> points = vertices.subset(cycle);
		points = geom::bissectrice_enlarge_cycle(points, translation, distance);

		if(points.size() != cycle.size())
		{
			mot::err("bissectrice_enlarge_cycle(): internal size problem: " + std::to_string(points.size()) + " != " + std::to_string(cycle.size()));
		}

		for(uint i=0; i<cycle.size(); i++)
		{
			uint j = (i+1)%cycle.size();

			vertices.add_value(points[i]);
			elements.add_rectangle(cycle[i], cycle[j], firsti + j, firsti + i, flip_face);
		}

		return {firsti, vertices.size()-1};
	}
	// Can be optimized
	std::pair<uint, uint> bissectrice_enlarge_cycle(ssl::ArrayBuffer<glm::vec3>& vertices,
								    	ssl::ElementBuffer& elements,
										const std::pair<uint, uint>& cycle,
								    	float distance,
								    	const glm::vec3& translation,
								    	bool flip_face)
	{
		return bissectrice_enlarge_cycle(vertices, elements, utils::uirange(cycle.first, cycle.second), distance, translation, flip_face);
	}

	// Returns min and max indices of vertices added
	std::pair<uint, uint> conspace_enlarge_cycle(ssl::ArrayBuffer<glm::vec3>& vertices,
								    	ssl::ElementBuffer& elements,
										const std::vector<uint>& cycle,
								    	float distance,
								    	const glm::vec3& translation,
								    	bool flip_face,
								    	float curveprecision_distance,
								    	bool fill)
	{
		if(cycle.size() == 0)
		{
			mot::err("meshbuil:conspace_enlarge_cycle() cycle must be of size > 0");
		}
		uint firsti = vertices.size();

		std::vector<glm::vec3> points = vertices.subset(cycle);
		
		if(distance != 0.0f)
		{
			points = geom::conspace_enlarge_cycle(points, translation, distance, curveprecision_distance);
		}
		else
		{
			return extrude_cycle(
				vertices,
				elements,
				cycle, 
				translation,
				flip_face
			);
		}

		std::vector<uint> newcycle;
		for(const auto& p : points)
		{
			newcycle.push_back(vertices.size());
			vertices.add_value(p);
		}

		if(fill)
		{
			if(distance > 0.0f)
			{
				fill_rubban(vertices, elements, newcycle, cycle, !flip_face);
			}
			else
			{
				fill_rubban(vertices, elements, cycle, newcycle, flip_face);
			}
		}

		return {firsti, vertices.size()-1};
	}

	partialEnlargementIndexing conspace_enlarge_cycle_ignore_partial(
		ssl::ArrayBuffer<glm::vec3>& vertices,
    	ssl::ElementBuffer& elements,
		const partialCycle<uint>& cycle,
    	float distance,
    	bool flip_face,
    	float curveprecision_distance,
    	bool fill
    )
    {
    	cycle.check();

		partialEnlargementInfos<glm::vec3> en = geom::conspace_enlarge_cycle(
			partial_cycle(vertices, cycle),
			distance,
			curveprecision_distance
		);

		#ifdef MOTOR_DEBUG
		if(en.base.size() != cycle.cycle.size())
		{
			mot::err("meshbuild::conspace_enlarge_cycle_ignore_partial(): wrong conspace_enlarge_cycle cycle sizes: "
				+ std::to_string(en.base.size()) + " != " + std::to_string(cycle.cycle.size()));
		}
		#endif

		std::vector<uint> mapping = enlargement_to_vertices_mapping(vertices, cycle, en, false);
		partialEnlargementIndexing ret = map_enlargement_infos_on_vertice(en, mapping);

		if(fill)
		{
			if(distance > 0.0f)
			{
				fill_rubban(vertices, elements, ret.enlarged.cycle, cycle.cycle, !flip_face);
			}
			else
			{
				fill_rubban(vertices, elements, cycle.cycle, ret.enlarged.cycle, flip_face);
			}
		}

		return ret;
    }

	// Can be optimized
	std::pair<uint, uint> conspace_enlarge_cycle(ssl::ArrayBuffer<glm::vec3>& vertices,
								    	ssl::ElementBuffer& elements,
										const std::pair<uint, uint>& cycle,
								    	float distance,
								    	const glm::vec3& translation,
								    	bool flip_face,
								    	float curveprecision_distance,
								    	bool fill)
	{
		return conspace_enlarge_cycle(vertices, elements, utils::uirange(cycle.first, cycle.second), distance, translation, flip_face, curveprecision_distance, fill);
	}

	uint fan_cycle(ssl::ArrayBuffer<glm::vec3>& vertices,
				   ssl::ElementBuffer& elements,
				   const std::vector<uint>& cycle,
				   const glm::vec3& fan_center,
				   bool flip_face)
	{
		uint firsti = vertices.size();
		vertices.add_value(fan_center);

		for(uint i=0; i<cycle.size(); i++)
		{
			uint j = (i+1)%cycle.size();
			elements.add_triangle(cycle[i], cycle[j], firsti, flip_face);
		}

		return firsti;
	}
	
	uint fan_cycle(ssl::ArrayBuffer<glm::vec3>& vertices,
				   ssl::ElementBuffer& elements,
				   const std::pair<uint, uint>& cycle,
				   const glm::vec3& fan_center,
				   bool flip_face)
	{
		return fan_cycle(vertices, elements, utils::uirange(cycle.first, cycle.second), fan_center, flip_face);
	}

	void fill_cycles(
		const ssl::ArrayBuffer<glm::vec3>& vertices,
		ssl::ElementBuffer& elements,
	    const partialEnlargement<uint>& cycle,
	    bool flip_face
	)
	{
		for(const std::vector<uint>& f : cycle.faces)
		{
			fill_cycle(
				vertices,
				elements,
				f,
				flip_face
			);
		}
	}

	void fill_cycle(const ssl::ArrayBuffer<glm::vec3>& vertices,
					ssl::ElementBuffer& elements,
				    const std::vector<uint>& cycle,
				    bool flip_face)
	{
		vector<glm::vec3> points;

		for(uint i=0; i<cycle.size(); i++)
		{
			points.push_back(vertices[cycle[i]]);
		}

		vector<triIndex> triangles = geom::cycle_triangulation(points);
	
		for(const triIndex& t : triangles)
		{
			elements.add_triangle(cycle[t.i1], cycle[t.i2], cycle[t.i3], flip_face);
		}
	}

	void fill_cycle(const ssl::ArrayBuffer<glm::vec3>& vertices,
					ssl::ElementBuffer& elements,
				    const std::pair<uint, uint>& cycle,
				    bool flip_face)
	{
		return fill_cycle(vertices, elements, utils::uirange(cycle.first, cycle.second), flip_face);
	}

	void fill_rubban(const ssl::ArrayBuffer<glm::vec3>& vertices,
					ssl::ElementBuffer& elements,
				    const std::vector<uint>& outcycle,
				    const std::vector<uint>& incycle,
				    bool flip_face)
	{
		vector<triIndex> triangles = geom::rubban_triangulation(vertices.get_value(), outcycle, incycle);
	
		for(const triIndex& t : triangles)
		{
			elements.add_triangle(t.i1, t.i2, t.i3, flip_face);
		}
	}

	void fill_rubban(const ssl::ArrayBuffer<glm::vec3>& vertices,
					ssl::ElementBuffer& elements,
				    const std::pair<uint, uint>& outcycle,
				    const std::pair<uint, uint>& incycle,
				    bool flip_face)
	{
		return fill_rubban(vertices, elements, utils::uirange(outcycle.first, outcycle.second), utils::uirange(incycle.first, incycle.second), flip_face);
	}

	std::pair<uint, uint> relative_pointdirected_extruding(ssl::ArrayBuffer<glm::vec3>& vertices,
								 		  ssl::ElementBuffer& elements,
				    			 		  const std::vector<uint>& cycle,
				    			 		  const glm::vec3& point,
				    			 		  float coeff,
				    			 		  bool flip_face)
	{
		if(cycle.size() == 0)
		{
			mot::err("meshbuil:relative_pointdirected_extruding() cycle must be of size > 0");
		}
		uint firsti = vertices.size();

		for(uint i=0; i<cycle.size(); i++)
		{
			uint j = (i+1)%cycle.size();
			vec3 dir = (point - vertices[cycle[i]]);
			vec3 p;

			if(glm::length(dir) < 0.0001)
			{
				p = point;
			}
			else
			{
				p =  vertices[cycle[i]] + glm::normalize(dir)*glm::length(dir)*coeff;
			}

			vertices.add_value(p);
			elements.add_rectangle(cycle[i], cycle[j], firsti + j, firsti + i, flip_face);
		}

		return {firsti, vertices.size()-1};
	}

	std::pair<uint, uint> relative_pointdirected_extruding(ssl::ArrayBuffer<glm::vec3>& vertices,
								 		  ssl::ElementBuffer& elements,
								 		  const std::pair<uint, uint>& cycle,
								 		  const glm::vec3& point,
								 		  float coeff,
								 		  bool flip_face)
	{
		return relative_pointdirected_extruding(vertices, elements, utils::uirange(cycle.first, cycle.second), point, coeff, flip_face);
	}

	std::pair<uint, uint> pointdirected_extruding(ssl::ArrayBuffer<glm::vec3>& vertices,
								 ssl::ElementBuffer& elements,
								 const std::vector<uint>& cycle,
								 const glm::vec3& point,
								 float d,
								 const glm::vec3& translation,
								 bool flip_face,
								 bool fill)
	{
		if(cycle.size() == 0)
		{
			mot::err("meshbuil:pointdirected_extruding() cycle must be of size > 0");
		}
		uint firsti = vertices.size();

		for(uint i=0; i<cycle.size(); i++)
		{
			uint j = (i+1)%cycle.size();
			vec3 dir = (point - vertices[cycle[i]]);
			vec3 p;

			if(glm::length(dir) < 0.0001)
			{
				p = point;
			}
			else
			{
				p =  vertices[cycle[i]] + glm::normalize(dir)*d;
			}

			vertices.add_value(p + translation);
			if(fill)
			{
				elements.add_rectangle(cycle[i], cycle[j], firsti + j, firsti + i, flip_face);
			}
		}

		return {firsti, vertices.size()-1};
	}

	std::pair<uint, uint> pointdirected_extruding(ssl::ArrayBuffer<glm::vec3>& vertices,
								 ssl::ElementBuffer& elements,
								 const std::pair<uint, uint>& cycle,
								 const glm::vec3& point,
								 float d,
								 const glm::vec3& translation,
								 bool flip_face,
								 bool fill)
	{
		return pointdirected_extruding(vertices, elements, utils::uirange(cycle.first, cycle.second), point, d, translation, flip_face, fill);
	}

	void translate(ssl::ArrayBuffer<glm::vec3>& vertices,  const std::vector<uint>& indices, const glm::vec3& translation)
	{
		for(uint i : indices)
		{
			if(i >= vertices.size())
			{
				mot::err("translate(): index out of range");
			}

			vertices.change_subvalue(i, vertices[i] + translation);
		}
	}

	std::pair<uint, uint> copytranslate(ssl::ArrayBuffer<glm::vec3>& vertices,  const std::vector<uint>& indices, const glm::vec3& translation)
	{
		std::pair<uint, uint> ret;
		ret.first = vertices.size();

		for(uint i : indices)
		{
			if(i >= vertices.size())
			{
				mot::err("translate(): index out of range");
			}

			vertices.add_value(vertices[i] + translation);
		}
		
		ret.second = ret.first + indices.size()-1;
		return ret;
	}

	std::pair<uint, uint> copytranslate(ssl::ArrayBuffer<glm::vec3>& vertices,  const std::pair<uint, uint>& indices, const glm::vec3& translation)
	{
		return copytranslate(vertices, utils::uirange(indices.first, indices.second), translation);
	}

	std::array<uint, 8> doored_fill(
		ssl::ArrayBuffer<glm::vec3>& vertices,
		ssl::ElementBuffer& elements,
		const std::array<uint, 4>& outline,
		float door_center_coeffx,
		float height,
		float width,
		bool flip_face
	)
	{
		vec3 dirXN = (vertices[outline[1]] - vertices[outline[0]]);
		vec3 dirYN = (vertices[outline[3]] - vertices[outline[0]]);
	
		float lx = glm::length(dirXN);
		float ly = glm::length(dirYN);

		if(lx < width || ly < height || width < geom::EPSILON || height < geom::EPSILON)
		{
			mot::err("meshbuild::doored_fill(): door dimensions impossible");
		}
		
		dirXN = dirXN/lx;
		dirYN = dirYN/ly;

		std::array<uint, 8> ret;
		ret[0] = outline[0];
		ret[5] = outline[1];
		ret[6] = outline[2];
		ret[7] = outline[3];

		ret[1] = vertices.size();
		vertices.add_value(vertices[outline[0]] + dirXN*(lx*door_center_coeffx - width/2.0f));
		ret[4] = vertices.size();
		vertices.add_value(vertices[outline[0]] + dirXN*(lx*door_center_coeffx + width/2.0f));
		ret[2] = vertices.size();
		vertices.add_value(vertices[outline[0]] + dirXN*(lx*door_center_coeffx - width/2.0f) + dirYN*height);
		ret[3] = vertices.size();
		vertices.add_value(vertices[outline[0]] + dirXN*(lx*door_center_coeffx + width/2.0f) + dirYN*height);
	
		meshbuild::fill_cycle(
			vertices,
			elements,
			vector<uint>(ret.begin(), ret.end()),
			flip_face
		);

		return ret;
	}
};


