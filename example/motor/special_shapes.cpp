#include "special_shapes.hpp"

bool dooredWall::buildable() const
{
	glm::vec3 dirxN = bot2 - bot1;
	float l = glm::length(dirxN);
	dirxN = dirxN/l;

	float x1 = l*door_xpos - door_size/2.0f;
	float x2 = l*door_xpos + door_size/2.0f;

	if(door_height >= height)
		return false;

	if(x1 <= 0.0f)
		return false;

	if(x2 >= l)
		return false;

	return true;
}

glm::vec3 dooredWall::inside_door_bot1() const
{
	glm::vec3 b1 = bot1 + inside_dir1N*thickness;
	glm::vec3 b2 = bot2 + inside_dir2N*thickness;

	glm::vec3 dirxN = (b2 - b1);
	float l = glm::length(dirxN);
	dirxN = dirxN/l;

	return b1 + dirxN*(l*door_xpos - door_size/2.0f);
}

glm::vec3 dooredWall::inside_door_bot2() const
{
	glm::vec3 b1 = bot1 + inside_dir1N*thickness;
	glm::vec3 b2 = bot2 + inside_dir2N*thickness;

	glm::vec3 dirxN = (b2 - b1);
	float l = glm::length(dirxN);
	dirxN = dirxN/l;

	return b1 + dirxN*(l*door_xpos + door_size/2.0f);
}

glm::vec3 dooredWall::inside_dirN() const
{
	glm::vec3 ret = glm::normalize(glm::cross(this->sized_x(), diryN));

	if(glm::dot(ret, inside_dir1N) < 0)
	{
		ret = -ret;
	}

	return ret;
}

glm::vec3 dooredWall::door_bot1() const
{
	glm::vec3 dirxN = bot2 - bot1;
	float l = glm::length(dirxN);
	dirxN = dirxN/l;

	return bot1 + dirxN*(l*door_xpos - door_size/2.0f);
}

glm::vec3 dooredWall::door_bot2() const
{
	glm::vec3 dirxN = bot2 - bot1;
	float l = glm::length(dirxN);
	dirxN = dirxN/l;

	return bot1 + dirxN*(l*door_xpos + door_size/2.0f);
}

glm::vec3 dooredWall::sized_x() const
{
	return bot2 - bot1;
}

glm::vec3 dooredWall::dirxN() const
{
	return glm::normalize(this->sized_x());
}

