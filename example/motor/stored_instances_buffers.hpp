#ifndef _STORED_INSTANCES_BUFFERS_HPP_
#define _STORED_INSTANCES_BUFFERS_HPP_

#include "stored_drawable_object.hpp"
#include "gutils.hpp"
#include "tmplt.hpp"


template<uint FIRST_INSTANCE_BUFFER, typename... Types>
class StoredInstancesBuffers : public StoredDrawableObject<Types...>
{
public:
	using MotherClass = StoredDrawableObject<Types...>;

	template<typename Type>
	struct storedDrawType
	{
		using type = ssl::draw_type;
	};

	using InstanceBuffersIndices = tmplt::make_index_range<FIRST_INSTANCE_BUFFER, sizeof...(Types)>;

public:
	StoredInstancesBuffers(ssl::draw_type element_draw_type, typename storedDrawType<Types>::type ... buffers_draw_type);

	uint instances_amount() const;
	
	virtual VertexArrayObject compute_vao() const override;		
	
protected:
	void check_sizes() const;

	void swapop(uint instance_index);
	template<std::size_t... Is>
	void swapop_specifics_buffers(uint instance_index, std::index_sequence<Is...>);
	template<std::size_t... Is>
	void check_specifics_sizes(std::index_sequence<Is...>) const;
};


#define StoredInstancesBuffersMACRO(retType) template<uint FIRST_INSTANCE_BUFFER, typename... Types>\
									         retType StoredInstancesBuffers<FIRST_INSTANCE_BUFFER, Types...>

StoredInstancesBuffersMACRO()::StoredInstancesBuffers(ssl::draw_type element_draw_type, typename storedDrawType<Types>::type ... buffers_draw_type)
	: StoredDrawableObject<Types...>(element_draw_type, buffers_draw_type...)
{

}

StoredInstancesBuffersMACRO(uint)::instances_amount() const
{
	return (MotherClass::template get_array_buffer<FIRST_INSTANCE_BUFFER>()).size();
}

StoredInstancesBuffersMACRO(VertexArrayObject)::compute_vao() const
{
	std::vector<bufferDivisor> buffersd;

	for(uint i=0; i<this->get_buffers().size(); i++)
	{
		uint div = (i >= FIRST_INSTANCE_BUFFER) ? 1 : 0;
		buffersd.push_back({this->get_buffers()[i], div});
	}

	return VertexArrayObject(buffersd, &(this->get_elements()));
}

template<uint FIRST_INSTANCE_BUFFER, typename... Types>
template<std::size_t... Is>
void StoredInstancesBuffers<FIRST_INSTANCE_BUFFER, Types...>::swapop_specifics_buffers(uint instance_index, std::index_sequence<Is...>)
{
	#ifdef SSL_DEBUG
	if(instance_index >= this->instances_amount())
	{
		ssl::err("StoredInstancesBuffers::swapop_specifics_buffers(): index out of bound");
	}
	#endif

	((utils::swapop((MotherClass::template get_array_buffer<Is>()), instance_index)),...);
}

template<uint FIRST_INSTANCE_BUFFER, typename... Types>
template<std::size_t... Is>
void StoredInstancesBuffers<FIRST_INSTANCE_BUFFER, Types...>::check_specifics_sizes(std::index_sequence<Is...>) const
{
	bool all_good = true;

	((all_good = all_good && ((MotherClass::template get_array_buffer<Is>()).size() == (MotherClass::template get_array_buffer<FIRST_INSTANCE_BUFFER>()).size())),...);

	if(all_good)
	{
		ssl::err("StoredInstancesBuffers::check_specifics_size():bad sizes");
	}
}

StoredInstancesBuffersMACRO(void)::swapop(uint instance_index)
{
	this->swapop_specifics_buffers(instance_index, InstanceBuffersIndices{});
}

StoredInstancesBuffersMACRO(void)::check_sizes() const
{
	this->check_specifics_size(InstanceBuffersIndices{});
}

#undef StoredInstancesBuffersMACRO


#endif //_STORED_INSTANCES_BUFFERS_HPP_
