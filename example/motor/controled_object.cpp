#include "controled_object.hpp"

using namespace glm;
using namespace std;

ControledObject::ControledObject(const glm::vec3& position)
	: _position(position),
	  _speed(vec3(0)),
	  _context(NULL)
{

}

void ControledObject::set_context(const HitboxContext* context)
{
	_context = context;
}

void ControledObject::set_speed(const glm::vec3& speed)
{
	_speed = speed;
}

void ControledObject::tp(const glm::vec3& new_position)
{
	_position = new_position;
}

const glm::vec3& ControledObject::get_position()
{
	return _position;
}

const glm::vec3& ControledObject::get_speed()
{
	return _speed;
}

ControledObject::cMove ControledObject::move(const glm::vec3& dp)
{
	vec3 np = _position + dp;

	if(_context == NULL)
	{
		_position = np;
		return {{NULL, geom::NO_IMPACT}, dp};
	}
	else
	{	
		contextImpact impact = _context->impact(this->get_hitbox(), geom::safe_normalize(dp), glm::length(dp));

		if(impact.impact.impact)
		{
			return {impact, glm::normalize(dp)*impact.impact.distance};
		}
		else
		{
			_position = np;
			return {impact, dp};
		}
	}
}

