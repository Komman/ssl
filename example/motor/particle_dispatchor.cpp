#include "particle_dispatchor.hpp"

float BasicParticleDispatchor::get_particle_time_percentage(const Particle& particle)
{
	return particle.get_left_time()/particle.get_last_time();
}


using namespace glm;
using namespace std;



ParticleDispatchor::ParticleDispatchor()
	: BasicParticleDispatchor(),
	  _dep_LBAC({}, ssl::DYNAMIC_DRAW),
	  _colors({}, ssl::DYNAMIC_DRAW),
	  _infos({}, ssl::DYNAMIC_DRAW),
	  _particles()
{

}

glm::vec2 ParticleDispatchor::get_infos_vector(const Particle& particle)
{
	return get_infos_vector(get_particle_time_percentage(particle), particle.get_size());
}

glm::vec2 ParticleDispatchor::get_infos_vector(float time_percentage, float size)
{
	return glm::vec2(time_percentage, size);
}

void ParticleDispatchor::add_particle(const Particle& particle)
{
	_particles.push_back(particle);

	_dep_LBAC.add_value(vec4(particle.get_position(), particle.get_light_bloom_affection()));
	_colors.add_value(particle.get_color());
	_infos.add_value(get_infos_vector(particle));
}

void ParticleDispatchor::animate(float dt)
{
	for(auto& particle : _particles)
	{
		particle.animate(dt);
	}

	this->fill_buffers();
}

void ParticleDispatchor::fill_buffers()
{
	this->check_sizes();

	for(unsigned int i=0; i<_particles.size(); i++)
	{
		const auto& particle = _particles[i];

		if(particle.is_still_alive())
		{
			_dep_LBAC.change_subvalue(i, vec4(particle.get_position(), particle.get_light_bloom_affection()));
			_colors.change_subvalue(i, particle.get_color());
			_infos.change_subvalue(i, get_infos_vector(particle));
		}
		else
		{
			utils::swapop(_particles , i);			
			utils::swapop(_dep_LBAC  , i);			
			utils::swapop(_colors    , i);			
			utils::swapop(_infos     , i);			

			i--;
		}		
	}
}

const ssl::ArrayBuffer<glm::vec4>& ParticleDispatchor::get_dep_LBAC() const
{
	return _dep_LBAC;
}

const ssl::ArrayBuffer<glm::vec4>& ParticleDispatchor::get_colors() const
{
	return _colors;
}

const ssl::ArrayBuffer<glm::vec2>& ParticleDispatchor::get_infos() const
{
	return _infos;
}

void ParticleDispatchor::check_sizes() const
{
	#ifdef SSL_DEBUG
	if(_particles.size() != _dep_LBAC.size() || _particles.size() != _infos.size() || _particles.size() != _colors.size())
	{
		ssl::err("ParticleDispatchor::fill_buffers(): buffer size error");
	}
	#endif
}

unsigned int ParticleDispatchor::size() const
{
	this->check_sizes();

	return _particles.size();
}

void ParticleDispatchor::explode(const glm::vec3& center, float amplitude, float distance_factor)
{
	for(auto& p : _particles)
	{
		auto  vd = p.get_position() - center;
		float d  = glm::length(vd);
		vec3 added_speed = glm::normalize(vd) * amplitude / (d*d*distance_factor + 1.0f);
	
		p.set_speed(p.get_speed() + added_speed);
	}
}

