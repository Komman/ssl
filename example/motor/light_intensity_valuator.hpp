#ifndef _LIGHT_INTENSITY_VALUATOR_HPP_
#define _LIGHT_INTENSITY_VALUATOR_HPP_

#include "basic_timed_valuator.hpp"
#include "dynamic_light.hpp"

/*
	The valuator class must herit from :
	BasicTimedValuator<dynamicLight>
*/

template<typename Valuator>
class LightIntensityValuator : public Valuator
{
public:
	// field dynamicLight::intensity is useless here
	LightIntensityValuator(const dynamicLight& light);

	void add_value(float duration, float intensity);

private:
	dynamicLight _light;
};




template<typename Valuator>
LightIntensityValuator<Valuator>::LightIntensityValuator(const dynamicLight& light)
	: Valuator(),
	  _light(light)
{

}

template<typename Valuator>
void LightIntensityValuator<Valuator>::add_value(float duration, float intensity)
{	
	_light.intensity = intensity;

	this->Valuator::add_value(duration, _light);
}


#endif //_LIGHT_INTENSITY_VALUATOR_HPP_
