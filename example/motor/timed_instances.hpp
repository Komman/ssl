#ifndef _TIMED_INSTANCES_HPP_
#define _TIMED_INSTANCES_HPP_

#include "instances_manager.hpp"

template<typename InstanceStruct, uint FIRST_INSTANCE_BUFFER, typename... Types>
class TimedInstances : public InstancesManager<InstanceStruct, FIRST_INSTANCE_BUFFER, Types...>
{
public:
	using MotherClass = InstancesManager<InstanceStruct, FIRST_INSTANCE_BUFFER, Types...>;

	template<typename Type>
	struct storedDrawType
	{
		using type = ssl::draw_type;
	};

public:
	TimedInstances(const std::string& name, ssl::draw_type element_draw_type, typename storedDrawType<Types>::type ... buffers_draw_type);

	void animate(float dt);
	float get_time() const;

protected:
	virtual float get_instance_start_time(uint instance_index) const =0;
	virtual float get_instance_duration(uint instance_index)   const =0;
	
	bool has_expired(uint instance_index) const override;

private:
	Uniform<float> _time;
};


#define TimedInstancesMACRO(retType) template<typename InstanceStruct, uint FIRST_INSTANCE_BUFFER, typename... Types>\
									 retType TimedInstances<InstanceStruct, FIRST_INSTANCE_BUFFER, Types...>



TimedInstancesMACRO()::TimedInstances(const std::string& name, ssl::draw_type element_draw_type, typename storedDrawType<Types>::type ... buffers_draw_type)
	: InstancesManager<InstanceStruct, FIRST_INSTANCE_BUFFER, Types...>(element_draw_type, buffers_draw_type...),
	  _time(name + "_time", 0.0f)
{

}

TimedInstancesMACRO(void)::animate(float dt)
{
	_time += dt;

	this->MotherClass::animate(dt);

	// To prevent time to be not precise enough if it is too big
	if(this->instances_amount() == 0)
	{
		_time = 0.0f;
	}
}

TimedInstancesMACRO(float)::get_time() const
{
	return _time.get_value();
}

TimedInstancesMACRO(bool)::has_expired(uint instance_index) const
{
	float duration = this->get_instance_duration(instance_index);

	if(duration < 0.0f)
		return false;

	return (this->get_time() - this->get_instance_start_time(instance_index) >= duration);
}




#undef TimedInstancesMACRO

#endif //_TIMED_INSTANCES_HPP_
