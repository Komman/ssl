#ifndef _COLLISIONS_HPP_
#define _COLLISIONS_HPP_

#include <iostream>	

#include "../utils/geom.hpp"
#include "basic_hitbox.hpp"

template<hitbox_primary::Shape HITBOX_TYPE>
struct shapeInfoStruct {};

template<>
struct shapeInfoStruct<hitbox_primary::SPHERE> {using type = sphereInfo;}; 

template<>
struct shapeInfoStruct<hitbox_primary::PRISM>  {using type = prismInfo;}; 

template<hitbox_primary::Shape HITBOX_TYPE>
using shapeInfo = typename shapeInfoStruct<HITBOX_TYPE>::type;

namespace collisions
{
	using primaryShape = hitbox_primary::Shape;

	template<primaryShape HITBOX1>
	spaceBox minmax_coord(const shapeInfo<HITBOX1>& shape);

	template<primaryShape HITBOX1>
	glm::vec3 barycenter(const shapeInfo<HITBOX1>& shape);

	template<primaryShape HITBOX1>
	shapeInfo<HITBOX1> symmetrical(const shapeInfo<HITBOX1>& shape, const infinitePlan& sym_plan);

	template<primaryShape HITBOX1>
	void tp(shapeInfo<HITBOX1>& shape, const glm::vec3& ref_pos, const glm::vec3& pos);

	template<primaryShape HITBOX1>
	void transform(shapeInfo<HITBOX1>& shape, const hbxTransformation& transform);

	template<primaryShape HITBOX1>
	void homothety(shapeInfo<HITBOX1>& shape, const glm::vec3& center, float coeff);

	template<primaryShape HITBOX1>
	void rotate(shapeInfo<HITBOX1>& shape,  const glm::mat3& rotation);

	template<primaryShape HITBOX1, primaryShape HITBOX2>
	bool primary_collision(const shapeInfo<HITBOX1>& shape1,const shapeInfo<HITBOX2>& shape2);

	template<primaryShape HITBOX1, primaryShape HITBOX2>
	collisionImpact primary_impact(const shapeInfo<HITBOX1>& shape1, const glm::vec3& self_directionN, float distance, const shapeInfo<HITBOX2>& shape2);


};

#endif //_COLLISIONS_HPP_
