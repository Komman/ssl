#ifndef _BASIC_MOUSEKEY_TARGET_HPP_
#define _BASIC_MOUSEKEY_TARGET_HPP_

class BasicMousekeyTarget 
{
public:
	BasicMousekeyTarget(const BasicMousekeyTarget* const* switcher);

	bool enable() const;
	const BasicMousekeyTarget* const* get_switcher() const;

	virtual void enter();
	virtual void exit();

	virtual void animate(float dt);

	bool key_pressed(int key) const;
	
private:
	const BasicMousekeyTarget* const* _switcher;
};

#endif //_BASIC_MOUSEKEY_TARGET_HPP_
