#include "geo_cylinder_hitbox.hpp"

#include "../../ssl/src/utils.hpp"
#include "../utils/mutils.hpp"

using namespace glm;

GeoCylinderHitbox::GeoCylinderHitbox(const geoCylinderInfo& shape)
	: ShapedHitboxFusion(shape)
{
	this->build();
}

void GeoCylinderHitbox::build()
{
	const auto& geocylinder = this->get_shape();
	const auto& cycle = geocylinder.cycle;

	std::vector<triIndex> triangles = geom::cycle_triangulation(cycle);

	for(uint i=0; i<triangles.size(); i++)
	{
		this->fusion_hitbox<BevelHitbox>(bevelShape{
			.triangle_base   = {
				cycle[triangles[i].i1],
				cycle[triangles[i].i2],
				cycle[triangles[i].i3]
		   	},
			.sized_direction = geocylinder.sized_direction
		});
	}
}
