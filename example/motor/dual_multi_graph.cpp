#include "dual_multi_graph.hpp"

#include "../../ssl/src/utils.hpp"

#include "../utils/geom.hpp"
#include "../utils/mutils.hpp"

#include "motor_debug.hpp"

using namespace std;
using namespace glm;

DualMultiGraph::DualMultiGraph(const BorderedGraph& g)
	: _graph(g)

{
	this->compute_faces();
}

void DualMultiGraph::check_face(uint face) const
{
	if(face >= _faces.size())
	{
		mot::err("DualMultiGraph::check_face(): face out of bound: "
			+ std::to_string(face) + " / " + std::to_string(_faces.size()));
	}
}

const std::vector<uint>& DualMultiGraph::face_vertices(uint face) const
{
	this->check_face(face);

	return _faces[face].vertices;
}

const std::vector<DualMultiGraph::Edge>& DualMultiGraph::face_edges(uint face) const
{
	this->check_face(face);

	return _faces[face].edges;
}

const glm::vec2& DualMultiGraph::position(uint face) const
{
	this->check_face(face);

	return _faces[face].middle;
}

std::vector<glm::vec2> DualMultiGraph::face_vertices_positions(uint face) const
{
	std::vector<glm::vec2> ret;
	auto vertices = this->face_vertices(face);

	for(uint v : vertices)
	{
		ret.push_back(this->graph()[v]);
	}

	return ret;
}


std::vector<DualMultiGraph::faceEdge> DualMultiGraph::all_face_edges() const
{
	std::vector<DualMultiGraph::faceEdge> ret;

	for(uint f1 = 0; f1 < _faces.size(); f1++)
	{
		for(uint f2 = f1+1; f2 < _faces.size(); f2++)
		{
			for(const Edge& e : this->faces_edges(f1, f2))
			{
				ret.push_back({
					.edge = e,
					.face_edge = {f1, f2}
				});
			}
		}
	}

	return ret;
}

const std::vector<DualMultiGraph::Edge>& DualMultiGraph::faces_edges(uint face1, uint face2) const
{
	this->check_face(face1);
	this->check_face(face2);

	Edge fe = this->order({face1, face2});

	return _face_adjency[fe.first][fe.second];
}

DualMultiGraph::Edge DualMultiGraph::order(const Edge& e) const
{
	if(e.first <= e.second)
	{
		return e;
	}
	else
	{
		return {e.second, e.first};
	}
}


void DualMultiGraph::add_face_adjency(uint f1, uint f2, const Edge& e)
{
	#ifdef MOTOR_DEBUG
	this->check_face(f1);
	this->check_face(f2);
	#endif

	Edge fe = this->order({f1, f2});

	_face_adjency[fe.first][fe.second].push_back(e);
}

bool DualMultiGraph::adjacent(uint face1, uint face2) const
{
	return (this->faces_edges(face1, face2).size() > 0);
}

std::vector<uint> DualMultiGraph::inborder_adjacent_faces(uint face) const
{
	std::vector<uint> ret;

	for(uint f=0; f<_faces.size(); f++)
	{
		if(f != face && f != this->get_border_face())
		{
			if(this->adjacent(f, face))
			{
				ret.push_back(f);
			}
		}
	}

	return ret;	
}

std::vector<uint> DualMultiGraph::adjacent_faces(uint face) const
{
	std::vector<uint> ret;

	for(uint f=0; f<_faces.size(); f++)
	{
		if(f != face)
		{
			if(this->adjacent(f, face))
			{
				ret.push_back(f);
			}
		}
	}

	return ret;	
}

uint DualMultiGraph::opposite_face(const Edge& e, uint face) const
{
	Edge ff = this->adjacent_faces(e);

	if(ff.first != face && ff.second != face)
	{
		mot::err("DualMultiGraph::opposite_face(): required face " 
			 + to_string(face) + " is not adjacent to required edge"
			 + e.to_string());
	}

	if(ff.first == face)
	{
		return ff.second;
	}
	else
	{
		return ff.first;
	}
}

DualMultiGraph::Edge DualMultiGraph::adjacent_faces(const Edge& e) const
{
	auto facesit = _face_edges.find(this->order(e));

	if(facesit == _face_edges.end())
	{
		mot::err("DualMultiGraph::adjacent_faces(): edge " + e.to_string()+ " is not an edge");
	}

	return facesit->second;
}

void DualMultiGraph::compute_faces()
{
	// _graph.print_ppm("castle_graph.ppm", 500);

	vector<vector<uint>> faces = _graph.all_faces();
	_border_face = std::numeric_limits<uint>::max();

	for(const vector<uint>& f : faces)
	{
		vector<glm::vec2> cycle  = _graph[f];
		vector<uint>      cyclei = f;

		if(!geom::trigo_way(cycle))
		{
			utils::invert(cyclei);
		}

		if(utils::unorderd_vector_equals(f, _graph.get_border()))
		{
			if(_border_face != std::numeric_limits<uint>::max())
			{
				mot::err("DualMultiGraph::compute_faces(): Two border faces found");
			}

			_border_face = _faces.size();
		}

		_faces.push_back({
			.middle   = _graph.center(cyclei),
			.vertices = f,
			.edges    = edges_cycle(cyclei)
		});
	}

	if(_border_face == std::numeric_limits<uint>::max())
	{
		mot::err("DualMultiGraph::compute_faces(): No border face found");
	}

	_face_adjency = vector<vector<vector<Edge>>>(
		_faces.size(),
		vector<vector<Edge>>(
			_faces.size(),
			vector<Edge>(0)
		)
	);

	vector<Edge> edges     = _graph.all_edges(); 
	vector<uint> all_faces = this->all_faces();

	for(const auto& e : edges)
	{
		uint onfaces = 0;
		
		for(uint f1 : all_faces)
		{
			std::vector<Edge> f1edges = this->face_edges(f1);

			for(uint f2 : all_faces)
			{
				if(f1 < f2)
				{
					std::vector<Edge> f2edges = this->face_edges(f2);
					
					if((utils::in_vector(f1edges, e) || utils::in_vector(f1edges, Graph::mirror(e)))
					&& (utils::in_vector(f2edges, e) || utils::in_vector(f2edges, Graph::mirror(e))))
					{
						this->add_face_adjency(f1, f2, e);
						_face_edges[this->order(e)] = {f1, f2};

						onfaces++;
					}
				}
			}
		}

		if(onfaces > 1)
		{
			mot::err("DualMultiGraph::compute_faces: edge on more than one face couple");
		}
		if(onfaces < 1)
		{
			mot::err("DualMultiGraph::compute_faces: edge on less than one face couple");
		}
	}
}

uint DualMultiGraph::get_border_face() const
{
	return _border_face;
}

std::vector<uint> DualMultiGraph::all_faces() const
{
	std::vector<uint> ret(_faces.size(), 0);

	for(uint i=0; i<_faces.size(); i++)
	{
		ret[i] = i;
	}

	return ret;
}

std::vector<uint> DualMultiGraph::all_faces_noborder() const
{
	std::vector<uint> ret = this->all_faces();

	utils::swapop(ret, this->get_border_face());

	return ret;
}


const BorderedGraph& DualMultiGraph::graph() const
{
	return _graph;
}


