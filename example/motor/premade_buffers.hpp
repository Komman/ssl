#ifndef _PREMADE_BUFFERS_HPP_
#define _PREMADE_BUFFERS_HPP_

#include "aligned_cube.hpp"

namespace premade_buffers
{
	void init();
	void free();


	// dimension 1x1x1 on coord (0,0,0)
	AlignedCube& orthonormal_cube();
};

#endif //_PREMADE_BUFFERS_HPP_
