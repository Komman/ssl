#include "post_processor.hpp"

using namespace std;
using namespace glm;

PostProcessor::PostProcessor(const glm::vec4& clear_color,
							 Bloomer& bloomer,
							 const SharedFramebuffers& needed_framebuffers)
	: _bloomer(bloomer),
	  _shader_generator("motor/shaders/post_processing.glsl"),
	  _post_processor(_shader_generator, "frame_vert", "post_process"),
	  _frameMS(*(needed_framebuffers.frameMS)),
	  _frame(*(needed_framebuffers.frame)),
	  _frame_blending(*(needed_framebuffers.frame_blending)),
	  _frame_blending_and_bloom(*(needed_framebuffers.frame_blending_and_bloom)),
	  _frame_and_bloom(*(needed_framebuffers.frame_and_bloom)),
	  _frame_and_bloomMS(*(needed_framebuffers.frame_and_bloomMS)),
	  _bloom_frame(*(needed_framebuffers.bloom_frame)),
	  _bloom_frameMS(*(needed_framebuffers.bloom_frameMS))

{
	_shader_generator.free();

	_post_processor.depth(false);

	window::clear_color(vec4(0.0));
	_frameMS.set_clear_color(clear_color);
	_frame.set_clear_color(clear_color);
	_frame_and_bloom.set_clear_color(vec4(0.0));
	_bloom_frameMS.set_clear_color(vec4(0.0));
	_frame_blending.set_clear_color(vec4(0.0));
	_frame_blending_and_bloom.set_clear_color(vec4(0.0));
}

void PostProcessor::clear()
{
	// window::clear();
	// _frame.clear();
	// _frame_and_bloom.clear();

	// _bloom_frame.clear();
	// _frame_blending_and_bloom.clear();
	// _frame_and_bloomMS.clear();


	_frameMS.clear();
	_frame_blending.clear();
	_bloom_frameMS.clear();
}

void PostProcessor::draw_multisampled()
{
	_frameMS.draw_content_in_framebuffer(_frame, true, true);
	_bloom_frameMS.draw_content_in_framebuffer(_bloom_frame, false, false);
}

void PostProcessor::apply_bloom()
{
	_bloomer.bloom();
}

void PostProcessor::screen_pass()
{
	_post_processor.draw(buffers::screenfill());
}

void PostProcessor::direct_full_process()
{
	this->apply_bloom();
	this->screen_pass();
}

FrameBuffer<glm::vec4>& PostProcessor::bloom_frame() 
{
	return _bloom_frame;	
}

FrameBufferMultisample<glm::vec4>& PostProcessor::bloom_frameMS() 
{
	return _bloom_frameMS;	
}

Bloomer& PostProcessor::bloomer()
{
	return _bloomer;
}


FrameBufferMultisample<glm::vec4>& PostProcessor::frameMS() 
{
	return _frameMS;
}
FrameBuffer<glm::vec4>& PostProcessor::frame() 
{
	return _frame;
}

FrameBuffer<glm::vec4>& PostProcessor::blending_frame() 
{
	return _frame_blending;
}

FrameBuffer<glm::vec4, glm::vec4>& PostProcessor::bloomed_blending_frame()
{
	return _frame_blending_and_bloom;
}

FrameBuffer<glm::vec4, glm::vec4>& PostProcessor::frame_and_bloom() 
{
	return _frame_and_bloom;
}

FrameBufferMultisample<glm::vec4, glm::vec4>& PostProcessor::frame_and_bloomMS() 
{
	return _frame_and_bloomMS;
}
