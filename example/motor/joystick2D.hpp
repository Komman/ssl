#ifndef _JOYSTICK2D_HPP_
#define _JOYSTICK2D_HPP_

#include "joystick.hpp"

struct jumpSneakAction
{
	bool jump;
	bool sneak;
};

class Joystick2D : public BasicJoystick
{
public:
	Joystick2D(const BasicMousekeyTarget* mousekey, int keys[JOYSTICK_NUMBER_CONTROLS], const ssl::View* view);

	glm::vec3 compute_directionN() const;

	jumpSneakAction get_jump_sneak() const; 

private:
	glm::vec3 directionN_from_pressed(const std::vector<joystickKeysIndices>& pressed) const;

private:
	const ssl::View* _view;
	std::vector<int> _control_keys;

};

#endif //_JOYSTICK2D_HPP_
