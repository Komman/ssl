#ifndef _SHAPED_HITBOX_FUSION_HPP_
#define _SHAPED_HITBOX_FUSION_HPP_

#include "hitbox_fusion.hpp"

template<typename shapeStruct>
class ShapedHitboxFusion : public HitboxFusion
{
public:
	ShapedHitboxFusion(const shapeStruct& shape);

	const shapeStruct& get_shape() const;

private:
	shapeStruct _shape;
};







template<typename shapeStruct>
ShapedHitboxFusion<shapeStruct>::ShapedHitboxFusion(const shapeStruct& shape)
	: HitboxFusion(),
	  _shape(shape)
{

}

template<typename shapeStruct>
const shapeStruct& ShapedHitboxFusion<shapeStruct>::get_shape() const
{
	return _shape;
}


#endif //_SHAPED_HITBOX_FUSION_HPP_
