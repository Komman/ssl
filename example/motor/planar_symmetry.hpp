#ifndef _PLANAR_SYMMETRY_HPP_
#define _PLANAR_SYMMETRY_HPP_

#include "../../ssl/ssl.hpp"
#include <glm/gtx/projection.hpp>
#include "../utils/geom.hpp"

#include <tuple>

using namespace ssl;

// The ArrayBuffers must not be pop back while using PlanarSymmetry 
template<typename... Types>
class PlanarSymmetry
{
public:
	/*
		Both the coord_buffer and all the symmetricals_buffers must
		have the same  size while using PlanarSymmetry methods.
	*/
	PlanarSymmetry(const infinitePlan& plan,
				  ArrayBuffer<glm::vec3>* coord_buffer,
				  ElementBuffer*          indices_buffer,
				  ArrayBuffer<Types>*...  symmetricals_buffers);

	// Returns the index of the original point
	unsigned int place_symmetrical( const glm::vec3& position, const Types& ... other_values);
	unsigned int place_asymmetrical(const glm::vec3& position, const Types& ... other_values);

	// Returns the index of the symmetry of the point
	unsigned int symmetrical(unsigned int index);
	glm::vec3    symmetrical(const glm::vec3& pos);

	void link_triangle( unsigned int i1, unsigned int i2, unsigned int i3, bool flip = false);
	void link_rectangle(unsigned int i1, unsigned int i2, unsigned int i3, unsigned int i4, bool flip = false);

protected:
	using tupleType  = std::tuple<ArrayBuffer<Types>*...>;

	enum symIndexInfo {ASYMMETRICAL, ORIGINAL, SYMMETRICAL};

protected:

	// Returns index info
	symIndexInfo check_index(unsigned int index);

private:

	template<std::size_t... Index>
	void add_other_value(const std::tuple<const Types&...>& t, std::index_sequence<Index...>);
	void add_all_other_values(const std::tuple<const Types&...>& t);

private:

	const infinitePlan      _plan;
	ArrayBuffer<glm::vec3>* _coord_buffer;
	ElementBuffer*          _indices;

	std::tuple<ArrayBuffer<Types>* ...>  _symbuffers;

	std::map<unsigned int, symIndexInfo> _index_infos;
};





template<typename... Types>
PlanarSymmetry<Types...>::PlanarSymmetry(const infinitePlan& plan,
									   ArrayBuffer<glm::vec3>* coord_buffer,
									   ElementBuffer         * indices_buffer,
									   ArrayBuffer<Types>*... symmetricals_buffers)
	: _plan(plan),
	  _coord_buffer(coord_buffer),
	  _indices(indices_buffer),
	  _symbuffers(symmetricals_buffers...)
{

}

template<typename... Types>
template<std::size_t... Index>
void PlanarSymmetry<Types...>::add_other_value(const std::tuple<const Types&...>& t, std::index_sequence<Index...>)
{
	((std::get<Index>(_symbuffers)->add_value(std::get<Index>(t)))	, ...);
}

template<typename... Types>
void PlanarSymmetry<Types...>::add_all_other_values(const std::tuple<const Types&...>& t)
{
	this->add_other_value(t, std::make_index_sequence<sizeof...(Types)>{});
}

template<typename... Types>
glm::vec3 PlanarSymmetry<Types...>::symmetrical(const glm::vec3& pos)
{
	return geom::symmetrical(pos, _plan);
}


template<typename... Types>
unsigned int PlanarSymmetry<Types...>::place_symmetrical(const glm::vec3& position, const Types& ... other_values)
{
	unsigned int i = _coord_buffer->size();
	_coord_buffer->add_value(position);
	_coord_buffer->add_value(this->symmetrical(position));	
	this->add_all_other_values(std::make_tuple<const Types&...>(other_values...));
	this->add_all_other_values(std::make_tuple<const Types&...>(other_values...));


	_index_infos[i+0]=ORIGINAL;
	_index_infos[i+1]=SYMMETRICAL;

	return i;
}

template<typename... Types>
unsigned int PlanarSymmetry<Types...>::place_asymmetrical(const glm::vec3& position, const Types& ... other_values)
{
	unsigned int i = _coord_buffer->size();
	_coord_buffer->add_value(position);
	this->add_all_other_values(std::make_tuple<const Types&...>(other_values...));

	_index_infos[i+0]=ASYMMETRICAL;

	return i;
}

template<typename... Types>
unsigned int PlanarSymmetry<Types...>::symmetrical(unsigned int index)
{
	auto checked = this->check_index(index);
	if(checked == ASYMMETRICAL)
	{
		return index;
	}
	if(checked == ORIGINAL)
	{
		return index + 1;
	}
	if(checked == SYMMETRICAL)
	{
		return index - 1;
	}

	err("PlanarSymmetry<Types...>::symmetrical(): untyped check_index() returned");
	return 0;// Shut up the compiler
}

template<typename... Types>
void PlanarSymmetry<Types...>::link_triangle( unsigned int i1, unsigned int i2, unsigned int i3, bool flip)
{
	bool to_link_symetric = (this->check_index(i1) == ORIGINAL || this->check_index(i1) == ASYMMETRICAL) && 
							(this->check_index(i2) == ORIGINAL || this->check_index(i2) == ASYMMETRICAL) &&
							(this->check_index(i3) == ORIGINAL || this->check_index(i3) == ASYMMETRICAL) ;

	to_link_symetric = to_link_symetric &&
					   (this->check_index(i1) == ORIGINAL 
	 			      || this->check_index(i2) == ORIGINAL
	 			      || this->check_index(i3) == ORIGINAL);

	_indices->add_triangle(i1,
						   i2,
						   i3,
						   flip);
	if(to_link_symetric)
	{
		_indices->add_triangle(this->symmetrical(i1),
							   this->symmetrical(i3),
							   this->symmetrical(i2),
							   flip);
	}
}


template<typename... Types>
void PlanarSymmetry<Types...>::link_rectangle(unsigned int i1, unsigned int i2, unsigned int i3, unsigned int i4, bool flip)
{
	bool to_link_symetric = (this->check_index(i1) == ORIGINAL || this->check_index(i1) == ASYMMETRICAL) && 
							(this->check_index(i2) == ORIGINAL || this->check_index(i2) == ASYMMETRICAL) &&
							(this->check_index(i3) == ORIGINAL || this->check_index(i3) == ASYMMETRICAL) &&
							(this->check_index(i4) == ORIGINAL || this->check_index(i4) == ASYMMETRICAL);
	
	to_link_symetric = to_link_symetric &&
					   (this->check_index(i1) == ORIGINAL 
	 			      || this->check_index(i2) == ORIGINAL
	 			      || this->check_index(i3) == ORIGINAL
	 			      || this->check_index(i4) == ORIGINAL);

	_indices->add_rectangle(i1,
						    i2,
						    i3,
						    i4,
						    flip);
	if(to_link_symetric)
	{
		_indices->add_rectangle(this->symmetrical(i1),
							    this->symmetrical(i4),
							    this->symmetrical(i3),
							    this->symmetrical(i2),
							    flip);
	}
}

template<typename... Types>
typename PlanarSymmetry<Types...>::symIndexInfo PlanarSymmetry<Types...>::check_index(unsigned int index)
{
	auto it = _index_infos.find(index);
	
	#ifdef SSL_DEBUG
	if(it == _index_infos.end())
	{
		err("PlanarSymmetry<Types...>::symmetrical(): unreferenced index: " + std::to_string(index));
	}

	if(it->second == SYMMETRICAL)
	{
		if(index == 0)
		{
			err("IMPOSSIBLE HAPPENS HERE: " + SSL_FILE_AND_LINE);
		}
	}
	#endif

	return it->second;
}


#endif //_PLANAR_SYMMETRY_HPP_
