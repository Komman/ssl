#ifndef _GAME_DRAWER_HPP_
#define _GAME_DRAWER_HPP_

#include <map>

#include "post_processor.hpp"
#include "adaptable_drawable_object.hpp"

/*
	Almost abstracts the processing drawing order.
	
	Here the drawcalls are commanded, and then reproduced in
	the commanded order, but organizing for making compatible
	blooming, drawing, multisampling and blendind.

	Thus, drawer's draw params are saved when the draw is ordered.
	However, at the draw calls time, Uniforms<> variables have
	the value at this time and not when the drawcall is ordered. 

	When the global_draw() is called, here is the drawing order:
	1- Multisampling drawcalls
	2- Bloom multisampling drawcalls
	3- Only multisampling-bloom's framebuffer drawcalls
	4- Drawing multisampling attachement into non-multisampling attachements
	5- Drawcalls that only write on normal frambuffer (don't erase MS bloom)
	6- Bloom drawcalls
	7- Only bloom's framebuffer drawcalls
	8- Blend drawcalls
	9- Bloomed blend drawcalls
   10- Bloom pass
   11- Additif draws (on the blooming texture that is not used anymore)
   12- Post processing
*/

class GameDrawer
{
public:
	enum gameDrawTypes {MS_DRAW=0, BLOOM_MS_DRAW, NORMAL_DRAW, BLOOM_DRAW, BLENDING_DRAW, BLOOMED_BLENDING_DRAW, BLOOM_MS_ONLY_DRAW, BLOOM_ONLY_DRAW, ADDITIF_DRAW,
						DRAW_TYPES_COUNT};

public:
	GameDrawer(PostProcessor& post);

	void clear_framebuffer(gameDrawTypes drawing_type, GLbitfield mask = ssl::DEFAULT::CLEAR_MASK);

	void draw_MS(Drawer& drawer, const VertexArrayObject& vao);
	template<typename... Buffers>
	void draw_MS(Drawer& drawer, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_MS(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_MS(Drawer& drawer, const DrawableObject& obj);
	template<typename... Buffers>
	void draw_MS(Drawer& drawer, const ElementDrawableObject& obj);

	void draw_bloomed_MS(Drawer& drawer, const VertexArrayObject& vao);
	template<typename... Buffers>
	void draw_bloomed_MS(Drawer& drawer, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_bloomed_MS(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_bloomed_MS(Drawer& drawer, const DrawableObject& obj);
	template<typename... Buffers>
	void draw_bloomed_MS(Drawer& drawer, const ElementDrawableObject& obj);

	void draw_normal_unsafe(Drawer& drawer, const VertexArrayObject& vao);
	template<typename... Buffers>
	void draw_normal_unsafe(Drawer& drawer, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_normal_unsafe(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_normal_unsafe(Drawer& drawer, const DrawableObject& obj);
	template<typename... Buffers>
	void draw_normal_unsafe(Drawer& drawer, const ElementDrawableObject& obj);

	void draw_bloomed(Drawer& drawer, const VertexArrayObject& vao);
	template<typename... Buffers>
	void draw_bloomed(Drawer& drawer, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_bloomed(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_bloomed(Drawer& drawer, const DrawableObject& obj);
	template<typename... Buffers>
	void draw_bloomed(Drawer& drawer, const ElementDrawableObject& obj);

	void draw_blend(Drawer& drawer, const VertexArrayObject& vao);
	template<typename... Buffers>
	void draw_blend(Drawer& drawer, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_blend(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_blend(Drawer& drawer, const DrawableObject& obj);
	template<typename... Buffers>
	void draw_blend(Drawer& drawer, const ElementDrawableObject& obj);

	void draw_bloomed_blend(Drawer& drawer, const VertexArrayObject& vao);
	template<typename... Buffers>
	void draw_bloomed_blend(Drawer& drawer, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_bloomed_blend(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_bloomed_blend(Drawer& drawer, const DrawableObject& obj);
	template<typename... Buffers>
	void draw_bloomed_blend(Drawer& drawer, const ElementDrawableObject& obj);

	void draw_bloomed_MS_only(Drawer& drawer, const VertexArrayObject& vao);
	template<typename... Buffers>
	void draw_bloomed_MS_only(Drawer& drawer, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_bloomed_MS_only(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_bloomed_MS_only(Drawer& drawer, const DrawableObject& obj);
	template<typename... Buffers>
	void draw_bloomed_MS_only(Drawer& drawer, const ElementDrawableObject& obj);

	void draw_bloomed_only(Drawer& drawer, const VertexArrayObject& vao);
	template<typename... Buffers>
	void draw_bloomed_only(Drawer& drawer, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_bloomed_only(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_bloomed_only(Drawer& drawer, const DrawableObject& obj);
	template<typename... Buffers>
	void draw_bloomed_only(Drawer& drawer, const ElementDrawableObject& obj);

	void draw_additif(Drawer& drawer, const VertexArrayObject& vao);
	template<typename... Buffers>
	void draw_additif(Drawer& drawer, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_additif(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_additif(Drawer& drawer, const DrawableObject& obj);
	template<typename... Buffers>
	void draw_additif(Drawer& drawer, const ElementDrawableObject& obj);

	void draw_typed(gameDrawTypes type, Drawer& drawer, const VertexArrayObject& vao);
	template<typename... Buffers>
	void draw_typed(gameDrawTypes type, Drawer& drawer, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_typed(gameDrawTypes type, Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers);
	template<typename... Buffers>
	void draw_typed(gameDrawTypes type, Drawer& drawer, const DrawableObject& obj);
	template<typename... Buffers>
	void draw_typed(gameDrawTypes type, Drawer& drawer, const ElementDrawableObject& obj);

	void draw_multisampled();

	void apply_bloom();
	void screen_pass();

	/*
		post_processing = 
		- apply_bloom
		- draw ADDITIF_DRAW
		- screen_pass
	*/
	void post_processing();



	PostProcessor& get_post_processor() const;
	BasicFrameBuffer& get_typed_frame(gameDrawTypes type);

	// Draw commanded drawcalls of a certain type
	void typed_draw(gameDrawTypes type);

	// Draw all commanded drawcalls 
	void global_draw();

	// Abort all drawcalls
	void abort_draws();
	void abort_typed_draws(gameDrawTypes type);

	// clear all drawing frames
	void clear_frames();

	void record_enable();
	void record_disable();
	void record_start(const std::string& task);
	void record_stop();
	void record_print();

	bool recording() const;

protected:
	LabeledRecorder& recorder();

private:

	struct basicDraw
	{
		Drawer* drawer;
		Drawer::drawParams params;
		std::string name;
	};

	struct bufferDraw : public basicDraw
	{
		AdaptableDrawableObject object;
	};

	struct elementDraw : public basicDraw
	{
		AdaptableElementDrawableObject object;
	};

	struct vaoDraw : public basicDraw
	{
		const VertexArrayObject* object;
	};

	enum drawCommandType {OBJ_DRAW, ELEMENT_OBJ_DRAW, VAO_OBJ_DRAW, CLEAR};
	using drawIndex = std::pair<drawCommandType, unsigned int>;

	struct drawCalls
	{
		std::vector<drawIndex>   order;
		std::vector<bufferDraw>  objects_draws;
		std::vector<elementDraw> element_objects_draws;
		std::vector<vaoDraw>     vao_draws;
		std::vector<GLbitfield>  clears;
	};

	static const inline std::string NO_TASK_NAME = "";

private:

	PostProcessor&  _postproc;
	drawCalls       _drawcalls[DRAW_TYPES_COUNT];

	LabeledRecorder _recorder;
	std::string     _current_task_name;
	bool            _recording;
	
};




/*
	TYPED
*/

template<typename... Buffers>
void GameDrawer::draw_typed(gameDrawTypes type, Drawer& drawer, const Buffers&... buffers)
{
	bufferDraw d = {
		&drawer,
		drawer.get_draws_params(),
		_current_task_name,
		AdaptableDrawableObject(&buffers...)
	};

	auto& used_drawcall = _drawcalls[type].objects_draws;

	_drawcalls[type].order.push_back({OBJ_DRAW, used_drawcall.size()});
	used_drawcall.push_back(std::move(d));
}

template<typename... Buffers>
void GameDrawer::draw_typed(gameDrawTypes type, Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers)
{
	elementDraw d = {
		&drawer,
		drawer.get_draws_params(),
		_current_task_name,
		AdaptableElementDrawableObject(&elements, &buffers...)
	};

	auto& used_drawcall = _drawcalls[type].element_objects_draws;

	_drawcalls[type].order.push_back({ELEMENT_OBJ_DRAW, used_drawcall.size()});
	used_drawcall.push_back(std::move(d));
}

template<typename... Buffers>
void GameDrawer::draw_typed(gameDrawTypes type, Drawer& drawer, const DrawableObject& obj)
{
	bufferDraw d = {
		&drawer,
		drawer.get_draws_params(),
		_current_task_name,
		AdaptableDrawableObject(obj.get_buffers())
	};

	auto& used_drawcall = _drawcalls[type].objects_draws;

	_drawcalls[type].order.push_back({OBJ_DRAW, used_drawcall.size()});
	used_drawcall.push_back(std::move(d));
}

template<typename... Buffers>
void GameDrawer::draw_typed(gameDrawTypes type, Drawer& drawer, const ElementDrawableObject& obj)
{
	elementDraw d = {
		&drawer,
		drawer.get_draws_params(),
		_current_task_name,
		AdaptableElementDrawableObject(&(obj.get_elements()), obj.get_buffers())
	};

	auto& used_drawcall = _drawcalls[type].element_objects_draws;

	_drawcalls[type].order.push_back({ELEMENT_OBJ_DRAW, used_drawcall.size()});
	used_drawcall.push_back(std::move(d));
}


/*
	MS
*/

template<typename... Buffers>
void GameDrawer::draw_MS(Drawer& drawer, const Buffers&... buffers)
{
	this->draw_typed(MS_DRAW, drawer, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_MS(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers)
{
	this->draw_typed(MS_DRAW, drawer, elements, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_MS(Drawer& drawer, const DrawableObject& obj)
{
	this->draw_typed(MS_DRAW, drawer, obj);
}

template<typename... Buffers>
void GameDrawer::draw_MS(Drawer& drawer, const ElementDrawableObject& obj)
{
	this->draw_typed(MS_DRAW, drawer, obj);
}

/*
	BLOOMED MS
*/


template<typename... Buffers>
void GameDrawer::draw_bloomed_MS(Drawer& drawer, const Buffers&... buffers)
{
	this->draw_typed(BLOOM_MS_DRAW, drawer, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_bloomed_MS(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers)
{
	this->draw_typed(BLOOM_MS_DRAW, drawer, elements, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_bloomed_MS(Drawer& drawer, const DrawableObject& obj)
{
	this->draw_typed(BLOOM_MS_DRAW, drawer, obj);
}

template<typename... Buffers>
void GameDrawer::draw_bloomed_MS(Drawer& drawer, const ElementDrawableObject& obj)
{
	this->draw_typed(BLOOM_MS_DRAW, drawer, obj);
}


/*
	NORMAL
*/


template<typename... Buffers>
void GameDrawer::draw_normal_unsafe(Drawer& drawer, const Buffers&... buffers)
{
	this->draw_typed(NORMAL_DRAW, drawer, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_normal_unsafe(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers)
{
	this->draw_typed(NORMAL_DRAW, drawer, elements, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_normal_unsafe(Drawer& drawer, const DrawableObject& obj)
{
	this->draw_typed(NORMAL_DRAW, drawer, obj);
}

template<typename... Buffers>
void GameDrawer::draw_normal_unsafe(Drawer& drawer, const ElementDrawableObject& obj)
{
	this->draw_typed(NORMAL_DRAW, drawer, obj);
}


/*
	BLOOMED
*/


template<typename... Buffers>
void GameDrawer::draw_bloomed(Drawer& drawer, const Buffers&... buffers)
{
	this->draw_typed(BLOOM_DRAW, drawer, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_bloomed(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers)
{
	this->draw_typed(BLOOM_DRAW, drawer, elements, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_bloomed(Drawer& drawer, const DrawableObject& obj)
{
	this->draw_typed(BLOOM_DRAW, drawer, obj);
}

template<typename... Buffers>
void GameDrawer::draw_bloomed(Drawer& drawer, const ElementDrawableObject& obj)
{
	this->draw_typed(BLOOM_DRAW, drawer, obj);
}

/*
	BLENDING
*/


template<typename... Buffers>
void GameDrawer::draw_blend(Drawer& drawer, const Buffers&... buffers)
{
	this->draw_typed(BLENDING_DRAW, drawer, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_blend(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers)
{
	this->draw_typed(BLENDING_DRAW, drawer, elements, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_blend(Drawer& drawer, const DrawableObject& obj)
{
	this->draw_typed(BLENDING_DRAW, drawer, obj);
}

template<typename... Buffers>
void GameDrawer::draw_blend(Drawer& drawer, const ElementDrawableObject& obj)
{
	this->draw_typed(BLENDING_DRAW, drawer, obj);
}

/*
	BLOOMED BLENDING
*/


template<typename... Buffers>
void GameDrawer::draw_bloomed_blend(Drawer& drawer, const Buffers&... buffers)
{
	this->draw_typed(BLOOMED_BLENDING_DRAW, drawer, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_bloomed_blend(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers)
{
	this->draw_typed(BLOOMED_BLENDING_DRAW, drawer, elements, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_bloomed_blend(Drawer& drawer, const DrawableObject& obj)
{
	this->draw_typed(BLOOMED_BLENDING_DRAW, drawer, obj);
}

template<typename... Buffers>
void GameDrawer::draw_bloomed_blend(Drawer& drawer, const ElementDrawableObject& obj)
{
	this->draw_typed(BLOOMED_BLENDING_DRAW, drawer, obj);
}


/*
	BLOOM MS ONLY
*/


template<typename... Buffers>
void GameDrawer::draw_bloomed_MS_only(Drawer& drawer, const Buffers&... buffers)
{
	this->draw_typed(BLOOM_MS_ONLY_DRAW, drawer, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_bloomed_MS_only(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers)
{
	this->draw_typed(BLOOM_MS_ONLY_DRAW, drawer, elements, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_bloomed_MS_only(Drawer& drawer, const DrawableObject& obj)
{
	this->draw_typed(BLOOM_MS_ONLY_DRAW, drawer, obj);
}

template<typename... Buffers>
void GameDrawer::draw_bloomed_MS_only(Drawer& drawer, const ElementDrawableObject& obj)
{
	this->draw_typed(BLOOM_MS_ONLY_DRAW, drawer, obj);
}

/*
	BLOOM ONLY
*/


template<typename... Buffers>
void GameDrawer::draw_bloomed_only(Drawer& drawer, const Buffers&... buffers)
{
	this->draw_typed(BLOOM_ONLY_DRAW, drawer, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_bloomed_only(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers)
{
	this->draw_typed(BLOOM_ONLY_DRAW, drawer, elements, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_bloomed_only(Drawer& drawer, const DrawableObject& obj)
{
	this->draw_typed(BLOOM_ONLY_DRAW, drawer, obj);
}

template<typename... Buffers>
void GameDrawer::draw_bloomed_only(Drawer& drawer, const ElementDrawableObject& obj)
{
	this->draw_typed(BLOOM_ONLY_DRAW, drawer, obj);
}


/*
	ADDITIF
*/


template<typename... Buffers>
void GameDrawer::draw_additif(Drawer& drawer, const Buffers&... buffers)
{
	this->draw_typed(ADDITIF_DRAW, drawer, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_additif(Drawer& drawer, const ElementBuffer& elements, const Buffers&... buffers)
{
	this->draw_typed(ADDITIF_DRAW, drawer, elements, buffers...);
}

template<typename... Buffers>
void GameDrawer::draw_additif(Drawer& drawer, const DrawableObject& obj)
{
	this->draw_typed(ADDITIF_DRAW, drawer, obj);
}

template<typename... Buffers>
void GameDrawer::draw_additif(Drawer& drawer, const ElementDrawableObject& obj)
{
	this->draw_typed(ADDITIF_DRAW, drawer, obj);
}


#endif //_GAME_DRAWER_HPP_
