#include "dynamic_light.hpp"

using namespace std;

DynamicLight::DynamicLight(BasicDynamicLightor& lightor, const dynamicLight& light)
	: _lightor(lightor),
	  _lightor_index(lightor.add_light(light)),
	  _destroyed(false)
{
	// cout<<TERM::GREEN<<"BONSOIR : DynamicLight("<<_lightor_index<<")"<<TERM::NOCOL<<endl;
}

inline void DynamicLight::check_destroy_before_use()
{
	if(_destroyed)
	{
		ssl::err("DynamicLight: try to use a destroyed light");
		return;
	}
}

bool DynamicLight::is_destroyed()
{
	return _destroyed;
}


void DynamicLight::set_light(const dynamicLight& light)
{
	this->check_destroy_before_use();

	_lightor.set_light_light(_lightor_index, light);
}

void DynamicLight::set_position(const glm::vec3& position)
{
	this->check_destroy_before_use();
	
	_lightor.set_light_position(_lightor_index, position);
}

void DynamicLight::set_color(const glm::vec3& color)
{
	this->check_destroy_before_use();

	_lightor.set_light_color(_lightor_index, color);
}

void DynamicLight::set_intensity(float intensity)
{
	this->check_destroy_before_use();
	
	_lightor.set_light_intensity(_lightor_index, intensity);
}

DynamicLight& DynamicLight::operator=(const dynamicLight& light)
{
	this->set_light(light);
	return *this;
}

void DynamicLight::destroy()
{
	this->check_destroy_before_use();

	_lightor.remove_light(_lightor_index);

	_destroyed = true;
}

DynamicLight::~DynamicLight()
{
	if(!_destroyed)
	{
		// cout<<TERM::RED<<"BONSOIR : ~DynamicLight ("<<_lightor_index<<")"<<TERM::NOCOL<<endl;
		this->destroy();
	}
}

DynamicLight::DynamicLight(DynamicLight&& light)
	: _lightor(light._lightor),
	  _lightor_index(light._lightor_index),
	  _destroyed(light._destroyed)
{
	// cout<<TERM::RED<<"BONSOIR : DynamicLight&& ("<<light._lightor_index<<")"<<TERM::NOCOL<<endl;

	light._destroyed = true;
}

DynamicLight& DynamicLight::operator=(DynamicLight&& light)
{
	// cout<<TERM::RED<<"BONSOIR : DynamicLight("<<_lightor_index<<")= &&("<<light._lightor_index<<")"<<TERM::NOCOL<<endl;
	
	if(!_destroyed) _lightor.remove_light(_lightor_index);

	_lightor = light._lightor;
	_lightor_index = light._lightor_index;
	_destroyed = light._destroyed;

	light._destroyed = true;

	return *this;
}

uint DynamicLight::get_lightor_index() const
{
	return _lightor_index;
}

float DynamicLight::get_intensity()
{
	return _lightor.get_light_intensity(_lightor_index);
}

