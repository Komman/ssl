#include "vertical_shadower.hpp"

using namespace glm;

VerticalShadower::VerticalShadower(const glm::uvec2& size, float real_size)
	: _shadowmap_proj_size("shadowmap_proj_size", real_size),
	  _enlargement("shadowmap_enlargement", 1.0f),
	  _circle_center("vertical_shadower_circle_center", vec2(0.0f)),
	  _circle_radius("vertical_shadower_circle_radius", 0.0f),
	  _map("vertical_shadowmap", size, GL_LINEAR),
	  _preparing_map("shadowmap_preparing_map", size, GL_LINEAR),
	  _generator("shaders/upshadow.glsl"),
	  _circle_projector(_generator, "circle_projection", "down_prof_circle_fragment"),
	  _map_projector(_generator, "vertical_projection", "down_prof_fragment"),
	  _map_projector_instance(_generator, "vertical_projection_instance", "down_prof_fragment"),
	  _blur(_generator, "frame_vert", "shadow_blur_float"),
	  _frame_buffer(&_preparing_map),
	  _final_frame_buffer(&_map),
	  _pos(std::vector<glm::vec3>(4, vec3(0)), DYNAMIC_DRAW),
	  _indices({0,1,2, 0,2,3}, STATIC_DRAW)
{
	_map_projector.culling(false);
	_map_projector_instance.culling(false);
	_circle_projector.culling(false);
	_blur.culling(false);

	_frame_buffer.set_clear_color(glm::vec4(0.0));
	_final_frame_buffer.set_clear_color(glm::vec4(0.0));
}

void VerticalShadower::add_circle(const glm::vec3& position, float radius)
{
	_circle_center = vec2(position.x, position.z);
	_circle_radius = radius;

	_pos.change_subvalue(0, position + vec3(-radius, 0, -radius));
	_pos.change_subvalue(1, position + vec3(+radius, 0, -radius));
	_pos.change_subvalue(2, position + vec3(+radius, 0, +radius));
	_pos.change_subvalue(3, position + vec3(-radius, 0, +radius));

	_circle_projector.draw_elements_in_framebuffer(_frame_buffer,_indices, _pos);
}

void VerticalShadower::add_rectangle(const glm::vec3 positions[4])
{
	_pos.change_subvalue(0, positions[0]);
	_pos.change_subvalue(1, positions[1]);
	_pos.change_subvalue(2, positions[2]);
	_pos.change_subvalue(3, positions[3]);

	_map_projector.draw_elements_in_framebuffer(_frame_buffer, _indices, _pos);
}


void VerticalShadower::add_rectangle(const glm::vec3& start, const glm::vec3& stop, const glm::vec2& size)
{
	vec3 diryN = glm::normalize(stop - start);
	vec3 dirxN = glm::normalize(glm::cross(diryN, vvv::y()));

	vec3 poss[4] = {
		start - diryN*size.y - dirxN*size.x/2.0f,
		start - diryN*size.y + dirxN*size.x/2.0f,
		stop  + diryN*size.y + dirxN*size.x/2.0f,
		stop  + diryN*size.y - dirxN*size.x/2.0f
	};

	this->add_rectangle(poss);
}

void VerticalShadower::add_square(const glm::vec3& position, const glm::vec3& orientationN, const glm::vec2& size)
{
	vec3 diryN = orientationN;
	vec3 dirxN = glm::normalize(glm::cross(diryN, vvv::y()));

	vec3 poss[4] = {
		position - diryN*size.y/2.0f - dirxN*size.x/2.0f,
		position - diryN*size.y/2.0f + dirxN*size.x/2.0f,
		position + diryN*size.y/2.0f + dirxN*size.x/2.0f,
		position + diryN*size.y/2.0f - dirxN*size.x/2.0f
	};

	this->add_rectangle(poss);
}

void VerticalShadower::add_shadow(const BasicBuffer*  positions)
{
	_map_projector.draw_in_framebuffer(_frame_buffer, *positions);
}

void VerticalShadower::add_shadow_elements(const BasicBuffer*  positions, const ElementBuffer& indices)
{
	_map_projector.draw_elements_in_framebuffer(_frame_buffer, indices, *positions);
}

void VerticalShadower::add_shadow_elements_instance(const BasicBuffer*  positions, const BasicBuffer* instances, const ElementBuffer& indices, float enlargement)
{
	if(_enlargement.get_value() != enlargement)
	{
		_enlargement = enlargement;
	}
	
	_map_projector_instance.draw_elements_in_framebuffer(_frame_buffer, indices, *positions, *instances);
}

void VerticalShadower::clear_shadow()
{
	_frame_buffer.clear();
	_final_frame_buffer.clear();
}

void VerticalShadower::compute_shadowmap()
{
	_map.copy(_preparing_map);
	_blur.draw_in_framebuffer(_final_frame_buffer, buffers::screenfill());
}