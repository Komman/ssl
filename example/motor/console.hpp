#ifndef _CONSOLE_HPP_
#define _CONSOLE_HPP_

#include <unordered_map>

#include "../../ssl/src/color.hpp"
#include "../../ssl/src/utils.hpp"
#include "../utils/mutils.hpp"

#include "basic_console.hpp"
#include "motor_debug.hpp"

#define CONSOLE_TYPED_FUNC(structName)\
	template<typename Type>\
	friend struct structName;\
	template<typename Type>\
	struct structName{\
		static std::string f(const std::string&, Type, Console&);\
	};


class Console : public BasicConsole
{
public:
	static inline std::string SET_COMMANT = "/set";
	static inline std::string ADD_COMMANT = "/add";

public:
	Console(float msg_lasting);

	void set_modifiable(const std::string& var_name, void* addr);
	template<typename ArgType>
	void create_command(const std::string& command, void(*func)(ArgType arg));

protected:
	std::string response(const std::string& msg);
	// Parsed
	virtual std::string response(const std::vector<std::string>& msg);

protected:
	static std::string error(const std::string& msg);

private:
	static inline std::string VARIABLE_COLOR = ssl::TERM::BLUE;
	static inline std::string INCORRECT_ARGS = "Incorrect argument number: ";
	/*
		*var* for string
		$var$ for value
	*/
	static inline std::string SET_COMMANT_USE = "/set *variable* $value$";

	// Return "" if succed, error msg else
	template<template<typename> typename FuncType>
	std::string function_guess_type(const std::string& mainarg, const std::string& value);

	CONSOLE_TYPED_FUNC(set_typed)
	CONSOLE_TYPED_FUNC(add_typed)
	CONSOLE_TYPED_FUNC(execute_command)

	struct createdCommand
	{
		void* func;
		uint  argsize;
	};

	

private:
	std::unordered_map<std::string, void*> _vars;
	std::unordered_map<std::string, createdCommand> _commands;
};



template<typename Type>
std::string Console::set_typed<Type>::f(const std::string& var, Type v, Console& c)
{
	auto it = c._vars.find(var);
	if(it == c._vars.end())
	{
		return error("Variable *" + ssl::TERM::colored(var, VARIABLE_COLOR) + "* undefined");
	}
	*((Type*)(it->second)) = v; 
	return "";
}

template<typename Type>
std::string Console::add_typed<Type>::f(const std::string& var, Type v, Console& c)
{
	auto it = c._vars.find(var);
	if(it == c._vars.end())
	{
		return error("Variable *" + ssl::TERM::colored(var, VARIABLE_COLOR) + "* undefined");
	}

	*((Type*)(it->second)) += v; 

	return "";
}

template<typename Type>
std::string Console::execute_command<Type>::f(const std::string& command, Type arg, Console& c)
{
	auto it = c._commands.find(command);
	if(it == c._commands.end())
	{
		return error("Command *" + ssl::TERM::colored(command, VARIABLE_COLOR) + "* undefined");
	}
	if(it->second.argsize != sizeof(Type))
	{
		return error("Command *" + ssl::TERM::colored(command, VARIABLE_COLOR) + ", expected arg of size "
					 + std::to_string(it->second.argsize) + " and get arg of size " + std::to_string(sizeof(Type)));
	}

	((void(*)(Type))(it->second.func))(arg); 

	return "";
}

template<template<typename> typename FuncStruct>
std::string Console::function_guess_type(const std::string& mainarg, const std::string& value)
{
	if(value.size() == 0)
	{
		return this->error("Empty value");
	}

	if(value[value.size()-1] == 'i')
	{
		mot::err("TODO");
		return "";
	}

	if(value[value.size()-1] == ')')
	{
		return FuncStruct<glm::vec3>::f(mainarg, utils::tovec3(value), *this);
	}

	if(utils::is_float(value))
	{
		return FuncStruct<float>::f(mainarg, std::stof(value), *this);
	}

	return this->error("Unrecognized type in expression: \"" + value + "\"");
}


template<typename ArgType>
void Console::create_command(const std::string& command, void(*func)(ArgType arg))
{
	_commands[command] = {
		.func    = (void*)func,
		.argsize = sizeof(ArgType)
	};
}

#endif //_CONSOLE_HPP_
