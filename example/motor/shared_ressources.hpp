#ifndef _SHARED_RESSOURCES_HPP_
#define _SHARED_RESSOURCES_HPP_

#include "../motor/bloomer.hpp"
#include "../motor/post_processor.hpp"
#include "../motor/game_drawer.hpp"



class SharedRessources
{
public:
	struct SharedTextures
	{
		SharedTextures();

		DepthStencilBuffer 	                 depth_buffer;
		DepthStencilBufferMultisample        depth_bufferMS;
		UniformTexture2D<glm::vec4>          frame_texture;
		UniformTexture2D<glm::vec4>          blending_texture;
		UniformTexture2D<glm::vec4>          bloom_texture;
		UniformTextureMultisample<glm::vec4> frame_textureMS;
		UniformTextureMultisample<glm::vec4> frame_bloomMS;
	};

	struct SharedFramebuffers
	{
		SharedFramebuffers(SharedTextures& textures);
		
		FrameBufferMultisample<glm::vec4>            frameMS;
		FrameBuffer<glm::vec4>                       frame;
		FrameBuffer<glm::vec4>                       frame_blending;
		FrameBuffer<glm::vec4, glm::vec4>            frame_blending_and_bloom;
		FrameBuffer<glm::vec4, glm::vec4>            frame_and_bloom;
		FrameBufferMultisample<glm::vec4, glm::vec4> frame_and_bloomMS;
		FrameBuffer<glm::vec4>                       bloom_frame;
		FrameBufferMultisample<glm::vec4>            bloom_frameMS;
	};

	struct SharedTools
	{
		SharedTools(SharedTextures& textures,
					SharedFramebuffers& framebuffers,
					const glm::vec4&      init_background_color,
					const bloomParameter& bloom_params);
		
		Uniform<int>       samples_amount_MS;
		Uniform<glm::vec4> background_color;

		Bloomer       bloomer;
		PostProcessor post_processor;
		GameDrawer    game_drawer;
	};

public:
	SharedRessources(const glm::vec4&      init_background_color,
					 const bloomParameter&  );

	SharedTextures&     textures();
	SharedFramebuffers& framebuffers();
	SharedTools&        tools();

protected:
	SharedTextures     _textures;
	SharedFramebuffers _framebuffers;
	SharedTools        _tools;
};

#endif //_SHARED_RESSOURCES_HPP_
