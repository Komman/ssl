#include "particle.hpp"

Particle::Particle(const glm::vec3& position,
		     	   const glm::vec4& color,
			 	   float size,
			 	   float friction_coeff,
			 	   float last_time)
	: FrictionPoint(position, friction_coeff),
	  _color(color),
	  _size(size),
	  _last_time(last_time),
	  _left_time(last_time),
	  _lightonbloom_coeff(1.0f)
{

}

Particle::Particle(const glm::vec3& position,
			 	   const glm::vec3& speed,
			 	   const glm::vec3& acceleration,
		   		   const glm::vec4& color,
			 	   float size,
			 	   float friction_coeff,
			 	   float last_time)
	: FrictionPoint(position, speed, acceleration, friction_coeff),
	  _color(color),
	  _size(size),
	  _last_time(last_time),
	  _left_time(last_time),
	  _lightonbloom_coeff(1.0f)
{

}

void Particle::animate(float dt)
{
	_left_time -= dt;
	
	if(this->is_still_alive())
	{
		this->FrictionPoint::animate(dt);
	}
	else
	{
		_left_time = 0.0f;
	}
}

bool Particle::is_still_alive() const
{
	return (_left_time > 0.0f);
}

void Particle::set_light_bloom_affection(float coeff)
{
	_lightonbloom_coeff = coeff;
}

float Particle::get_light_bloom_affection() const
{
	return _lightonbloom_coeff;
}


