#include "bordered_graph.hpp"
#include "../utils/mutils.hpp"

#include "../motor/motor_debug.hpp"

BorderedGraph::BorderedGraph(const PlanGraph& g, const std::vector<uint>& border)
	: PlanGraph(g),
	  _border(border)
{
	this->compute_borderset();
}

BorderedGraph::BorderedGraph(const BorderedGraph& g)
	: PlanGraph(g),
	  _border(g._border)
{
	this->compute_borderset();
}

bool BorderedGraph::in_border(const Edge& e)
{
	return this->in_border(e.first) && this->in_border(e.second);
}

bool BorderedGraph::in_border(uint v)
{
	#ifdef MOTOR_DEBUG
	if(!this->check_vertex(v))
	{
		mot::err("BorderedGraph::in_border(): vertex " + std::to_string(v) + " does not exist");
	}
	#endif

	return (_borderset.find(v) != _borderset.end());
}

void BorderedGraph::remove_crossing_edges()
{
	auto edges = this->all_edges();

	for(auto e : edges)
	{
		if(!this->in_border(e) && this->is_crossing_edge(e))
		{
			this->remove_edge(e);
		}
	}
}


void BorderedGraph::apply_border()
{
	this->add_path(_border);
	this->remove_border(_border);
}

void BorderedGraph::compute_borderset()
{
	_borderset = utils::vectounset(_border);
}


const std::vector<uint>& BorderedGraph::get_border() const
{
	return _border;
}

const std::unordered_set<uint>& BorderedGraph::get_borderset() const
{
	return _borderset;
}


