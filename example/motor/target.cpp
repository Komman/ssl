#include "target.hpp"

PointerTarget::PointerTarget(const glm::vec3* position_ptr, const glm::vec3* speed_ptr)
	: _position(position_ptr), _speed(speed_ptr)
{

}

void PointerTarget::set_position_ptr(const glm::vec3* position_ptr)
{
	_position = position_ptr;
}

void PointerTarget::set_speed_ptr(const glm::vec3* speed_ptr)
{
	_speed = speed_ptr;
}

glm::vec3 PointerTarget::position() const
{
	return *_position;
}

glm::vec3 PointerTarget::speed() const
{
	return *_speed;
}