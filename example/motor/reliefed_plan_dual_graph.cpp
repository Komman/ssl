#include "reliefed_plan_dual_graph.hpp"

#include "../motor/motor_debug.hpp"

ReliefedPlanDualGraph::ReliefedPlanDualGraph()
	: PlanDualGraph()
{

}

ReliefedPlanDualGraph::ReliefedPlanDualGraph(const PlanDualGraph& g)
	: PlanDualGraph()
{
	auto vertices = g.all_vertices();
	auto edges = g.all_edges();

	for(auto v : vertices)
	{
		this->add_vertex(g[v], 0.0f);
	}
	for(auto e : edges)
	{
		this->add_edge(e);
	}
}

void ReliefedPlanDualGraph::set_face_height(uint face, float h)
{
	_heights[face] = h;
}

void ReliefedPlanDualGraph::add_vertex(const planDualFace& f, float h)
{
	uint v = this->PlanDualGraph::add_vertex(f);
	_heights[v] = h;
}

float ReliefedPlanDualGraph::face_height(uint face) const
{
	auto f = _heights.find(face);
	if(f == _heights.end())
	{
		mot::err(" ReliefedPlanDualGraph::face_height(uint face): face not heighted");
	}

	return f->second;
}

