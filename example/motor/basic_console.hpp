#ifndef _BASIC_CONSOLE_HPP_
#define _BASIC_CONSOLE_HPP_

#include <iostream>
#include <string>
#include <vector>

struct consoleMsg
{
	float time_in;
	std::string msg;
	bool  responded;
};


class BasicConsole
{
public:
	// In second
	BasicConsole(float msg_lasting);

	void animate(float dt);	
	void send_msg(const std::string& msg);

	uint oldest_msg_index() const;
	const std::vector<consoleMsg> all_msgs() const;
	std::vector<std::string> msgs() const;
	std::vector<std::string> msgs_ranged(uint startmsg, uint endmsg) const;
	std::string last_sent_msg(uint old_index) const;

	void print_ranged(uint startmsg, uint endmsg) const;
	void print_all() const;
	void print() const;

protected:
	virtual std::string response(const std::string& msg);

private:
	void add_msg(const std::string& msg);

private:
	std::vector<consoleMsg> _msgs;

	double _time;
	float  _lasting;

	mutable uint _oldest_index;
};

#endif //_BASIC_CONSOLE_HPP_
