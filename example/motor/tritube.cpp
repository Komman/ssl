#include "tritube.hpp"


#include <math.h>
#include <glm/gtx/rotate_vector.hpp>

using namespace ssl;
using namespace std;
using namespace glm;

Tritube::Tritube(const glm::vec3& position,
				 const glm::vec3& start_normal,
				 float start_size)
	: _start(position),
	  _extremity(position),
	  _vertexs(DYNAMIC_DRAW),
	  _indices(DYNAMIC_DRAW),
	  _buffers_cache({&_vertexs})
{
		
	this->born(position, start_normal, start_size);
}

void Tritube::born(const glm::vec3& position, 
				   const glm::vec3& start_normal,
				   float start_size)
{
	_start = position;
	_extremity = position;

	_vertexs.change_value({});
	_indices.change_value({});

	vec3 start_orientation = vvv::x();

	_vertexs.add_value(position + glm::rotate(start_orientation, 0.0f*float(M_PI)*2.0f/3.0f, start_normal));
	_vertexs.add_value(position + glm::rotate(start_orientation, 1.0f*float(M_PI)*2.0f/3.0f, start_normal));
	_vertexs.add_value(position + glm::rotate(start_orientation, 2.0f*float(M_PI)*2.0f/3.0f, start_normal));

	_indices.add_triangle(0,1,2);
}


void Tritube::grow(const glm::vec3& direction, float size)
{
	vec3 middle(0.0);
	uint vertex_size_begin = _vertexs.size();

	if(vertex_size_begin<3)
	{
		err("Tritube wrong initialized");
	}

	for(uint i=0;i<3;i++)
	{
		middle += _vertexs[vertex_size_begin-3 + i];
	}
	middle/=3.0f;
	for(uint i=0;i<3;i++)
	{
		vec3 point_dir =_vertexs[vertex_size_begin-3 + i] - middle;
		_vertexs.add_value(middle + direction + glm::normalize(point_dir)*size);
	}

	_indices.add_rectangle(vertex_size_begin-2, vertex_size_begin-3, vertex_size_begin+0, vertex_size_begin+1);
	_indices.add_rectangle(vertex_size_begin-1, vertex_size_begin-2, vertex_size_begin+1, vertex_size_begin+2);
	_indices.add_rectangle(vertex_size_begin-3, vertex_size_begin-1, vertex_size_begin+2, vertex_size_begin+0);
	
	_extremity = middle;
}

glm::vec3 Tritube::get_step_position(uint position)
{
	if(3*position+2 >= _vertexs.size())
	{
		err("call to Tritube::get_step_position() with an position out of range");
	}
	return (_vertexs[3*position+0]
		   +_vertexs[3*position+1]
		   +_vertexs[3*position+2])/3.0f;
}

const glm::vec3& Tritube::extremity()
{
	return _extremity;	
}

const glm::vec3& Tritube::start()
{
	return _start;
}

const ArrayBuffer<glm::vec3>& Tritube::get_vertexs()
{
	return _vertexs;
}


const std::vector<const BasicBuffer*>& Tritube::get_buffers()  const
{
	return _buffers_cache;
}
const ElementBuffer&                   Tritube::get_elements() const
{	
	return _indices;
}

