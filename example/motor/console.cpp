#include "console.hpp"



using namespace ssl;
using namespace std;


Console::Console(float msg_lasting)
	: BasicConsole(msg_lasting)
{

}

std::string Console::response(const std::string& msg)
{
	std::vector<std::string> parsed = utils::split_novoid(msg, " ");

	// cout<<TERM::CYAN<<"In: "<<TERM::NOCOL;
	// for(const auto& m : parsed)
	// {
	// 	cout<<TERM::colored("[", TERM::ORANGE)<<m<<TERM::colored("] ", TERM::ORANGE);
	// }
	// cout<<endl;

	return this->response(parsed);
}

std::string Console::error(const std::string& msg) 
{
	return TERM::RED + "Error: " + TERM::NOCOL + msg;
}

void Console::set_modifiable(const std::string& var_name, void* addr)
{
	_vars[var_name] = addr;
}

std::string Console::response(const std::vector<std::string>& parsed)
{
	if(parsed.size() == 0)
	{
		return this->error("No command");
	}

	if(parsed[0] == SET_COMMANT)
	{
		if(parsed.size() != 3)
		{
			return this->error(INCORRECT_ARGS + TERM::colored(SET_COMMANT_USE, TERM::ORANGE));
		}

		return function_guess_type<set_typed>(parsed[1], parsed[2]);
	}

	if(parsed[0] == ADD_COMMANT)
	{
		if(parsed.size() != 3)
		{
			return this->error(INCORRECT_ARGS + TERM::colored(SET_COMMANT_USE, TERM::ORANGE));
		}

		return function_guess_type<add_typed>(parsed[1], parsed[2]);
	}

	if(_commands.find(parsed[0]) != _commands.end())
	{
		if(parsed.size() != 2)
		{
			return this->error(INCORRECT_ARGS + TERM::colored(parsed[0], TERM::ORANGE));
		}

		return function_guess_type<execute_command>(parsed[0], parsed[1]);
	}

	return "";
}




