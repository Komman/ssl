#ifndef _BUILDING_HPP_
#define _BUILDING_HPP_

#include "../../ssl/src/array_buffer.hpp"
#include "../../ssl/src/element_buffer.hpp"

#include "../utils/geom.hpp"
#include "../motor/angular_symmetry.hpp"
#include "../motor/special_shapes.hpp"

using namespace ssl;

/*
	Functions returns the number of vertices added
	The name of the function is either:
	- "v": it means it fills only the vertices
	- "pos": returns a vector of vec3 that are the 
	positions of the required shape
	- something else: it means it fills vertices
	and faces according to the name of the function
*/

template<typename Type>
struct arrayBufferArg
{
	ArrayBuffer<Type>& buf;
	Type value;
};

template<typename... Types>
void valued_fill(uint amount, const arrayBufferArg<Types>&... array_buffer);

namespace building
{

	namespace square_base
	{
		/*
		  	
			^ plan.diryN
			|
			| 
			|  plan.dirxN
			O------->

            3---------2 ^
            |         | |
            |    X    | | size.y
            |         | |
			0---------1	V
			<--------->
			    size.x

			X : centered_plan.poit

		*/
		uint v(ArrayBuffer<glm::vec3>& vertices,
			   const orientedPlan& centered_plan,
			   const glm::vec2& size);
		
		// Top of the plan filled
		uint top(ArrayBuffer<glm::vec3>& vertices,
				 ElementBuffer& indices,
				 const orientedPlan& centered_plan,
				 const glm::vec2& size);

		std::vector<glm::vec3> pos(const orientedPlan& centered_plan,
							       const glm::vec2& size);

	};

	namespace paver
	{
		/*
			4----------5
			|\         |\
			| 7----------6
			| |        | |
			| |        | |
			0-|------- 1 |
			 \|         \|
			  3----------2

			^ plan.diryN
			|
			| 
			|  plan.dirxN
			O------->
			 \
			 _\|  plan.dirzN  
		*/
		uint v(ArrayBuffer<glm::vec3>& vertices,
			   const paverInfoN& paver);
		uint v(ArrayBuffer<glm::vec3>& vertices,
			   const paverInfo& paver);
		uint v(ArrayBuffer<glm::vec3>& vertices,
			   const centredPaverInfo& paver);
		uint v(ArrayBuffer<glm::vec3>& vertices,
			   const transPaverInfo& paver);
		uint v(ArrayBuffer<glm::vec3>& vertices,
			   const ycentredPaverInfo& paver);

		// on the y axis
		template<typename PaverType>
		uint sided(ArrayBuffer<glm::vec3>& vertices,
				   ElementBuffer& indices,
			   	   const PaverType& paver);
		template<typename PaverType>
		uint topsided(ArrayBuffer<glm::vec3>& vertices,
				      ElementBuffer& indices,
			   	      const PaverType& paver);
		template<typename PaverType>
		uint fullsided(ArrayBuffer<glm::vec3>& vertices,
				      ElementBuffer& indices,
			   	      const PaverType& paver);
	};

	namespace crown
	{
		/*
			bottom: (sized_y low)

			3-------------2
			| 7---------6 |
            | |         | |
            | |    X    | | 
            | |         | |
			| 4---------5 |
			0-------------1

			the top is the same shifted by 8
		*/
		uint squared_v(ArrayBuffer<glm::vec3>& vertices,
			   	   	   const orientedPlan& centered_plan,
					   const glm::vec2& size,
			   	   	   float height,
			   	   	   float wall_thickness);
		uint squared_topsided(ArrayBuffer<glm::vec3>& vertices,
				   			  ElementBuffer& indices,
			   	   	   	   	  const orientedPlan& centered_plan,
					   	   	  const glm::vec2& size,
			   	   	   	   	  float height,
			   	   	   	   	  float wall_thickness);
	};

	namespace cone
	{
		/*
			first index is the top, and the rest is the base
		*/
		uint v(ArrayBuffer<glm::vec3>& vertices,
			   const glm::vec3& base_center,
			   const glm::vec3& to_top,
			   const glm::vec3& base_aligementN,
			   float radius,
			   uint  discr);

		uint sided(ArrayBuffer<glm::vec3>& vertices,
				   ElementBuffer& indices,
				   const glm::vec3& base_center,
				   const glm::vec3& to_top,
				   const glm::vec3& base_aligementN,
				   float radius,
				   uint  discr);

		uint full_ext(ArrayBuffer<glm::vec3>& vertices,
				   	  ElementBuffer& indices,
				   	  const glm::vec3& base_center,
				   	  const glm::vec3& to_top,
				   	  const glm::vec3& base_aligementN,
				   	  float radius,
				   	  uint  discr);

	};

	namespace relief
	{
		/*
			relief.x : radius
			relief.y : height
		*/
		template<typename... Types>
		struct stageInfo
		{
			glm::vec2 relief;
			std::tuple<Types...> values;
		};

		template<typename... Types>
		uint v(AngularSymmetry<Types...>& sym,
			   const glm::vec3& base_directionN,
			   const std::vector<stageInfo<Types...>>& stages);
		
		template<typename... Types>
		uint sided(AngularSymmetry<Types...>& sym,
				   const glm::vec3& base_directionN,
				   const std::vector<stageInfo<Types...>>& stages);
	};

	namespace extruding
	{
		namespace convex
		{
			uint v(ArrayBuffer<glm::vec3>& vertices,
		  		   const std::vector<uint>& oredered_convex_shape_indices,
		  		   const glm::vec3& sized_extr_dir);

			uint sided(ArrayBuffer<glm::vec3>& vertices,
					   ElementBuffer& indices,
				  	   const std::vector<uint>& oredered_convex_shape_indices,
				  	   const glm::vec3& sized_extr_dir);
		};
		
	};

	// From bot_center following the spaceBase.diryN
	namespace hanoi
	{
		namespace squared
		{
			uint v(ArrayBuffer<glm::vec3>& vertices,
				   const std::vector<glm::vec2>& thick_heights,
				   const glm::vec3& bot_center,
				   const spaceBase& base);
			uint sided(ArrayBuffer<glm::vec3>& vertices,
					   ElementBuffer& indices,
					   const std::vector<glm::vec2>& thick_heights,
					   const glm::vec3& bot_center,
					   const spaceBase& base);
			uint topsided(ArrayBuffer<glm::vec3>& vertices,
					   ElementBuffer& indices,
					   const std::vector<glm::vec2>& thick_heights,
					   const glm::vec3& bot_center,
					   const spaceBase& base);

			// Fill with 0 last component
			uint v(ArrayBuffer<glm::vec4>& vertices,
				   const std::vector<glm::vec2>& thick_heights,
				   const glm::vec3& bot_center,
				   const spaceBase& base);
			uint sided(ArrayBuffer<glm::vec4>& vertices,
					   ElementBuffer& indices,
					   const std::vector<glm::vec2>& thick_heights,
					   const glm::vec3& bot_center,
					   const spaceBase& base);
		};
		namespace cycled
		{
			uint v(ArrayBuffer<glm::vec3>& vertices,
				   const std::vector<glm::vec2>& thick_heights,
				   const glm::vec3& bot_center,
				   uint points_amount,
				   const glm::vec3& dirY,
				   const glm::vec3& dirX);
			uint sided(ArrayBuffer<glm::vec3>& vertices,
					   ElementBuffer& indices,
					   const std::vector<glm::vec2>& thick_heights,
					   const glm::vec3& bot_center,
					   uint points_amount,
					   const glm::vec3& dirY,
					   const glm::vec3& dirX);
		};
	};


	namespace arc
	{
		/*
			^ y
			|
			|
			o----> x
			
			          (1,1)    index: (points_amount-1)
			         /\
			        /  \
			       /    |
			      /      \
			     /        |     <--- arc
			    /          \
			   /            |
			  /\ alpha      |
			 / _\_ _ _ _ _ _|     index : 0
                             (1,0)
		*/
		std::vector<glm::vec2> v(float angle, int points_amount, const glm::vec2& size = glm::vec2(1,1));

		// indexing : even is right, odd is left, last is top
		std::vector<glm::vec2> doublev(float angle, int points_amount, const glm::vec2& size = glm::vec2(1,1));
		
		/*
			Included set of indices:

				On BACK face;
					extern face: 
						right: even in [0, 2*(points_amount-1) - 1]
						left:  odd  in [0, 2*(points_amount-1) - 1]
						top: 2*(points_amount-1)+0

					intern face:
						right: even in [2*(points_amount-1)+1, (points_amount-1)*4+0]
						left:  odd  in [2*(points_amount-1)+1, (points_amount-1)*4+0]
						top: 4*(points_amount-1)+1

				On FRONT frace;
					extern face: 
						right: even in [4*(points_amount-1)+2, 6*(points_amount-1)+1]
						left:  odd  in [4*(points_amount-1)+2, 6*(points_amount-1)+1]
						top: 6*(points_amount-1)+2

					intern face:
						right: even in [6*(points_amount-1)+3, 8*(points_amount-1)+2]
						left:  odd  in [6*(points_amount-1)+3, 8*(points_amount-1)+2]
						top: 8*(points_amount-1)+3

		*/
		std::vector<glm::vec3> thicked_vertices(
							const glm::vec3& bottom,
				   			const glm::vec3& xaxisN,
				   			const glm::vec3& yaxisN,
				   			const glm::vec3& sizedZ,
							float thickness,
							float angle,
							int points_amount,
							const glm::vec2& size = glm::vec2(1,1));

		uint thickedowntoped(ArrayBuffer<glm::vec3>& vertices,
				   			 ElementBuffer& indices,
				   			 const glm::vec3& bottom,
				   			 const glm::vec3& xaxisN,
				   			 const glm::vec3& yaxisN,
				   			 const glm::vec3& sizedZ,
							 float thickness,
							 float angle,
							 int points_amount,
							 const glm::vec2& size = glm::vec2(1,1));
	};

	namespace doored_wall
	{
		/*
			outside :

			3------------2
			|            |
			|  7------6  |
			|  |      |  |
			|  |      |  |
			|  |      |  |
			0--4      5--1

			Sames indices +8 on the inside 
		*/

		std::vector<glm::vec3> v(const dooredWall& door);

		uint v(ArrayBuffer<glm::vec3>& vertices,
			   const dooredWall& door);

		uint topsided(ArrayBuffer<glm::vec3>& vertices,
					  ElementBuffer& indices,
					  const dooredWall& door,
					  bool flip = false);

		uint frontsided(ArrayBuffer<glm::vec3>& vertices,
						ElementBuffer& indices,
						const dooredWall& door,
						bool flip = false);

		uint extfrontsided(ArrayBuffer<glm::vec3>& vertices,
						   ElementBuffer& indices,
						   const dooredWall& door,
						   bool flip = false);

		uint infrontsided(ArrayBuffer<glm::vec3>& vertices,
						  ElementBuffer& indices,
						  const dooredWall& door,
						  bool flip = false);

		uint frontbridged(ArrayBuffer<glm::vec3>& vertices,
						  ElementBuffer& indices,
						  const dooredWall& door,
						  bool flip = false);
	};
};













template<typename... Types>
void valued_fill(uint amount, const arrayBufferArg<Types>&... array_buffer)
{
	for(uint i=0; i<amount; ++i)
	{
		((array_buffer.buf.add_value(array_buffer.value)), ...);
	}
}

namespace building
{
	namespace paver
	{
		template<typename PaverType>
		uint sided(ArrayBuffer<glm::vec3>& vertices,
				   ElementBuffer& indices,
			   	   const PaverType& paver)
		{
			uint i = vertices.size();

			indices.add_rectangle(i+0, 
								  i+1,
								  i+5,
								  i+4, true);
			indices.add_rectangle(i+3, 
								  i+7,
								  i+4,
								  i+0);
			indices.add_rectangle(i+2, 
								  i+6,
								  i+7,
								  i+3);
			indices.add_rectangle(i+1, 
								  i+5,
								  i+6,
								  i+2);

			return v(vertices, paver);
		}

		template<typename PaverType>
		uint topsided(ArrayBuffer<glm::vec3>& vertices,
				      ElementBuffer& indices,
			   	      const PaverType& paver)
		{
			uint i = vertices.size();
			auto ret =  sided(vertices, indices, paver);

			indices.add_rectangle(i+7, i+6, i+5, i+4);

			return ret;
		}


		template<typename PaverType>
		uint fullsided(ArrayBuffer<glm::vec3>& vertices,
				      ElementBuffer& indices,
			   	      const PaverType& paver)
		{
			uint i = vertices.size();
			auto ret = topsided(vertices, indices, paver);

			indices.add_rectangle(i+0, i+1, i+2, i+3);

			return ret;
		}
	};

	namespace relief
	{
		template<typename... Types>
		uint v(AngularSymmetry<Types...>& sym,
			   const glm::vec3& base_directionN,
			   const std::vector<stageInfo<Types...>>& stages)
		{
			uint start  = sym.coord_buffer().size();
			for(const auto& r : stages)
			{
				sym.place_symmetrical(
					sym.infos().axe_point + base_directionN*r.relief.x 
										  + sym.infos().axe_directionN*r.relief.y,
					r.values
				);
			}

			return sym.coord_buffer().size() - start;
		}

		template<typename... Types>
		uint sided(AngularSymmetry<Types...>& sym,
			       const glm::vec3& base_directionN,
			       const std::vector<stageInfo<Types...>>& stages)
		{
			uint start = sym.coord_buffer().size();
			uint ret = v(sym, base_directionN, stages);

			for(uint i=0; i<stages.size()-1; i++)
			{
				uint cur  = start + i*sym.infos().divisor;
				uint next = cur + sym.infos().divisor;

				sym.link_rectangle(cur,
								   sym.symmetrical(1, cur),
								   sym.symmetrical(1, next),
								   next);
			}

			return ret;
		}
	};
};

#endif //_BUILDING_HPP_
