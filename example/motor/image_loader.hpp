#ifndef _IMAGE_LOADER_HPP_
#define _IMAGE_LOADER_HPP_

#include <iostream>
#include <string>
#include <vector>
#include <glm/glm.hpp>
#include <fstream>

class ImageLoader
{
public:
	ImageLoader(const std::string& path);

	std::vector<float> float_RGB() const;

	const glm::uvec2& size() const; 
	const std::vector<uint> data() const;

private:
	void load_image(std::ifstream& file);

private:
	glm::uvec2 _size;
	std::vector<uint> _data;

	uint _max_value;
};

#endif //_IMAGE_LOADER_HPP_
