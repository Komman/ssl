#ifndef _EXPLO_MESHED_RAY_LIGHT_HPP_
#define _EXPLO_MESHED_RAY_LIGHT_HPP_

#include "timed_meshed_ray_light.hpp"

class ExploMeshedRayLight : public TimedStaticMeshedRayLight
{
public:
	ExploMeshedRayLight(const glm::vec3& center,
						const glm::vec3& traject,
						float angle,
						const glm::vec3& color,
						float duration);

protected:
	const LinearValuator<float>& get_intensity_evolution() const override;


private:
	static std::vector<glm::vec3> build_vertices(const glm::vec3& center,
												 const glm::vec3& traject,
												 float angle);
private:
	LinearValuator<float> _intensity;
};

#endif //_EXPLO_MESHED_RAY_LIGHT_HPP_
