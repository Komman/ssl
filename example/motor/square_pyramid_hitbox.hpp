#ifndef _SQUARE_PYRAMID_HITBOX_HPP_
#define _SQUARE_PYRAMID_HITBOX_HPP_

#include "hitbox_fusion.hpp"

struct squarePyramidShape
{
	/*
	 base:
           1-------0
          /       /
         /       / 
        2-------3 
	*/
	// The base vertex forms the cylcled base 
	glm::vec3 base[4];
	glm::vec3 top;
};
struct alignedSquarePyramidShape
{
	glm::vec3 base_center;
	glm::vec3 dir_base_XN;
	glm::vec3 dir_base_YN;
	glm::vec2 base_size_XY;
	glm::vec3 top;
};

class SquarePyramidHitbox : public HitboxFusion
{
public:
	SquarePyramidHitbox(const squarePyramidShape& shape);
	SquarePyramidHitbox(const alignedSquarePyramidShape& shape);

private:
	static squarePyramidShape convert(const alignedSquarePyramidShape& shape);
};

#endif //_SQUARE_PYRAMID_HITBOX_HPP_
