#include "gutils.hpp"
#include <glm/gtx/string_cast.hpp>

using namespace std;
using namespace glm;

namespace utils
{
	glm::vec3 random_vec3(const glm::vec3& ranges_min,
						  const glm::vec3& ranges_max)
	{
		return glm::vec3(
				utils::random_float(ranges_min.x, ranges_max.x),
				utils::random_float(ranges_min.y, ranges_max.y),
				utils::random_float(ranges_min.z, ranges_max.z)
			);
	}

	glm::vec2 random_vec2(const glm::vec2& ranges_min,
						  const glm::vec2& ranges_max)
	{
		return glm::vec2(
				utils::random_float(ranges_min.x, ranges_max.x),
				utils::random_float(ranges_min.y, ranges_max.y)
			);
	}

	glm::vec3 random_directionN()
	{
		vec3 ret = random_vec3(vec3(-1.0), vec3(1.0));

		if(glm::length(ret) <= 0.0001)
			return vec3(0.0);
		return glm::normalize(ret);
	}
	

	glm::vec3 rotate_redirect(const glm::vec3& v, const glm::vec3& original_dirN, const glm::vec3& new_dirN)
	{
		glm::vec3 c = glm::cross(original_dirN, new_dirN);

		if(glm::length(c) <= ssl::EPSILON)
		{
			return v;
		}

		c = glm::normalize(c);
		glm::vec3 t = glm::normalize(glm::cross(c, original_dirN));
		glm::vec3 q = glm::normalize(glm::cross(c, new_dirN));

		return   glm::dot(v, original_dirN)*new_dirN
			   + glm::dot(v, c)*c
			   + glm::dot(v, t)*q;
	}

	std::vector<glm::vec2> extract_xy(const std::vector<glm::vec3>& vs)
	{
		std::vector<glm::vec2> ret;

		for(const auto& p : vs)
		{
			ret.push_back(vec2(p.x, p.y));
		}

		return ret;
	}

};



