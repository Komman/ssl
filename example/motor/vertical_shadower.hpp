#ifndef _VERTICAL_SHADOWER_HPP_
#define _VERTICAL_SHADOWER_HPP_

#include "../../ssl/ssl.hpp"

using namespace ssl;

class VerticalShadower
{
public:
	VerticalShadower(const glm::uvec2& size, float real_size);

	void clear_shadow();
	void add_circle(const glm::vec3& position, float radius);
	void add_rectangle(const glm::vec3 positions[4]);
	// size : .x : thickness, y: before start and after stop
	void add_rectangle(const glm::vec3& start   , const glm::vec3& stop        , const glm::vec2& size);
	void add_square(const glm::vec3& position, const glm::vec3& orientationN, const glm::vec2& size);
	void add_shadow(const BasicBuffer * positions);
	void add_shadow_elements(const BasicBuffer * positions, const ElementBuffer& indices);
	void add_shadow_elements_instance(const BasicBuffer*  positions, const BasicBuffer* instances, const ElementBuffer& indices, float enlargement = 1.0f);
	void compute_shadowmap();

private:
	Uniform<float>          _shadowmap_proj_size;
	Uniform<float>          _enlargement;
	Uniform<glm::vec2>      _circle_center;
	Uniform<float>          _circle_radius;
	UniformTexture2D<float> _map;
	UniformTexture2D<float> _preparing_map;

	ShaderGenerator _generator;
	Drawer _circle_projector;
	Drawer _map_projector;
	Drawer _map_projector_instance;
	Drawer _blur;
	FrameBuffer<float> _frame_buffer;
	FrameBuffer<float> _final_frame_buffer;

	ArrayBuffer<glm::vec3> _pos;
	ElementBuffer          _indices;
};

#endif //_VERTICAL_SHADOWER_HPP_
