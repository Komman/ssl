#include "thick_graph.hpp"

#include <iostream>

#include "../utils/geom.hpp"
#include "../utils/mutils.hpp"

using namespace glm;
using namespace std;

ThickGraph::ThickGraph(const BorderedGraph& g, float thickness)
	: BorderedGraph(g),
	  _thickness(thickness)
{
	this->remove_close_vertices();
	this->remove_close_edges();
	this->remove_crossing_edges();
}

void ThickGraph::remove_close_vertices()
{
	bool finished = true;

	do
	{
		auto vertices = this->all_vertices();

		finished = true;

		while(vertices.size() > 0)
		{
			uint i = rand()%vertices.size();
			uint u = vertices[i];
			utils::swapop(vertices, i);

			if(!finished)
				break;

			if(this->in_border(u))
				continue;

			for(uint v : vertices)
			{
				float d = glm::distance(this->value(u), this->value(v));

				if(d < _thickness)
				{
					this->unoriented_fusion_vertices_no_selfloop(v, u);
					finished = false;
					break;
				}
			}
		}
	}
	while(!finished);
}

void ThickGraph::remove_close_edges()
{
	auto edges = this->all_edges();
	auto vertices = this->all_vertices();

	while(edges.size() > 0)
	{
		uint i = rand()%edges.size();
		Edge e = edges[i];
		utils::swapop(edges, i);

		if(!this->check_edge(e))
			continue;

		vertices = this->all_vertices();

		while(vertices.size() > 0)
		{
			uint j = rand()%vertices.size();
			uint v = vertices[j];
			utils::swapop(vertices, j);

			if(v != e.first && v != e.second)
			{
				vec2 p1 = this->value(e.first);
				vec2 p2 = this->value(e.second);
				vec2 pv = this->value(v);

				float d = geom::distance_point_segment(p1, p2, pv);

				if(d < _thickness)
				{
					if(this->in_border(e))
					{
						if(!this->in_border(v))
						{
							this->remove_vertex(v);
						}
					}
					else
					{
						this->remove_edge(e);

						float L  = glm::distance(p1, p2);
						float d1 = glm::distance(p1, pv);
						float d2 = glm::distance(pv, p2);

						if(d1 < L && this->nearest_vertex_distance({e.first, v}) < _thickness)
						{
							this->add_edge({e.first, v});
						}
						if(d2 < L && this->nearest_vertex_distance({v, e.first}) < _thickness)
						{
							this->add_edge({v, e.first});
						}

						break;
					}
				}
			}
		}
	}
}
