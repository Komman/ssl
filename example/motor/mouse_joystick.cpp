#include "mouse_joystick.hpp"

MouseJoystick::MouseJoystick(const MouseJoystickMKTarget* mouse, float sensibility)
	: _mouse(mouse),
	  _sensibility(sensibility),
	  _control(false),
	  _set_mouse_pixel(0)
{

}

const MouseJoystickMKTarget* MouseJoystick::get_mouse_control() const
{
	return _mouse;
}


glm::vec2 MouseJoystick::compute_angles()
{
	return glm::vec2(_mouse->frame_move())*_sensibility;
}
