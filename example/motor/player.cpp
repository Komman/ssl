#include "player.hpp"

#include <glm/gtx/projection.hpp>

using namespace glm;
using namespace std;

static int keys[JOYSTICK_NUMBER_CONTROLS] = {
	SSL_KEY(D),
	SSL_KEY(Q),
	SSL_KEY(Z),
	SSL_KEY(S),
	SSL_KEY(SPACE),
	SSL_KEY(LEFT_SHIFT)
};

Player::Player(const MouseJoystickMKTarget* controls_source, const glm::vec3& position, float speed, float mass)
	: 
	  _view(position,
	  	    NEAR,
	  	    FAR,
	  	    VIEW_FIELD,
	  	    std::string("MVP"),
	  	    std::string("camera_position"),
			std::string("camera_direction"),
			std::string("camera_right_vector"),
			std::string("camera_up_head")),
	  _creative_translator(controls_source, keys, &_view),
	  _natural_translator(controls_source, keys, &_view),
	  _head(controls_source, 0.001),
	  _mode(NATURAL),
	  _collision_enable(true),
	  _onground(false),
	  _gravity(-20.81),
	  _jump_impulse(7.0),
	  _position(position),
	  _vspeed_natural(0),
	  _vspeed(0),
	  _acceleration(vec3(0,_gravity,0)),
	  _speed(speed),
	  _mass(mass),
	  _selftarget(&_position, &_vspeed),
	  _context(NULL),
	  _safe_collision_space(0.3),
	  _falsepos(_position)
{

}

void Player::enable_collision()
{
	_collision_enable = true;
}

void Player::disable_collision()
{
	_collision_enable = false;
}

void Player::gamemode(gameMode mode)
{
	if(mode == CREATIVE)
	{
		this->set_speed(40.0f);
	}

	_mode = mode;
}

bool Player::key_pressed(int key) const
{
	return _head.get_mouse_control()->key_pressed(key);
}

void Player::set_context(const HitboxContext* context)
{
	_context = context;
}

const BasicTarget& Player::get_target() const
{
	return _selftarget;
}

const UniformView& Player::get_view() const
{
	return _view;
}



#warning TODELETE
#include "window_console.hpp"
#include <glm/gtx/string_cast.hpp>
#include "../game/effects.hpp"

void Player::move(float dt)
{
	#warning wrost case 5 collisions tests here, may be heavy
	vec3 m = this->get_position();
	this->move_mayfloaterror(dt);
	m = this->get_position() - m;

	if(_collision_enable && _context->in_collision(this->get_hitbox()))
	{
		CONSOLE::say(TERM::colored("Collision Floating point error", TERM::ORANGE));
		_position -= m*2.0f;
	}
}

void Player::frame_update(float dt)
{
	this->move(dt);
	this->get_hitbox().tp(this->get_position());
	_view.tp(this->get_position());

	vec2 angles = _head.compute_angles();
	_view.rotate(-angles);
	// TODO: rotate hitbox

	_view.update_uniforms();

	#warning FOR DEBUG HERE
	if(keyboard::key_pressed('R'))
	{
		effects::debug_point(_falsepos, 0.5f, vec4(1,0.5,0.5,2));
	}
}

void Player::hit_ground()
{
	_vspeed_natural = vec3(0);
	_onground = true;
}

void Player::hit_object(const glm::vec3& normalN)
{
	_vspeed_natural = geom::reflect(_vspeed_natural/2.0f, normalN);
}

void Player::leave_ground()
{
	_onground = false;
}

void Player::add_extern_speed(const glm::vec3& evs)
{
	_vspeed_natural += evs;
	this->leave_ground();
}

void Player::energy_impulsion(const glm::vec3& dirN, float energy)
{
	this->add_extern_speed(dirN*energy/_mass);
	// CONSOLE::say("impulsion: " + glm::to_string(dirN*energy/_mass));
}

glm::vec3 Player::nohbx_move(float dt)
{
	if(_mode == CREATIVE)
	{
		vec3 directionN = _creative_translator.compute_directionN();
		return directionN*_speed;
	}
	if(_mode == NATURAL)
	{
		vec3 ret = vec3(0);
		vec3 directionN = _natural_translator.compute_directionN();

		if(_onground)
		{
			auto legaction = _natural_translator.get_jump_sneak();
			if(legaction.jump)
			{
				this->add_extern_speed(vvv::y()*_jump_impulse);
			}
		}
		else
		{
			_vspeed_natural += _acceleration*dt;
		}
			
		
		ret += directionN*_speed + _vspeed_natural;

		return ret;
	}
	return vec3(0);
}

void Player::move_mayfloaterror(float dt)
{
	_vspeed = this->nohbx_move(dt)*dt;

	vec4  directionN_d = geom::safe_normalize_lenght(_vspeed);
	vec3  directionN = vec3(directionN_d);
	vec3  trymove    = _vspeed;
	float original_d = directionN_d.w;

	if(!_collision_enable)
	{
		_position += trymove;
		return;
	}

	if(_context->in_collision(this->get_hitbox()))
	{
		_position += trymove;
		CONSOLE::say(TERM::colored("Collision Bug", TERM::ORANGE));
		return;
	}

	contextImpact cimpact = _context->impact(this->get_hitbox(), directionN, glm::length(trymove) + _safe_collision_space);

	if(cimpact.impact.impact)
	{
		this->hit_object(cimpact.impact.normalN);

		trymove = vec3(0);

		// effects::debug_dirpoint(cimpact.impact.point, cimpact.impact.normalN*10.0f, 0.2, vec4(0.5,1,0.5,2));

		vec3 onplan = geom::safe_normalize(glm::cross(cimpact.impact.normalN, directionN));
		vec3 amongwall = glm::cross(onplan,  cimpact.impact.normalN);
		
		// effects::debug_dirpoint(cimpact.impact.point, onplan*10.0f, 0.2, vec4(1.0,0.2,0.2,2));
		// effects::debug_dirpoint(cimpact.impact.point, amongwall*10.0f, 0.2, vec4(0.2,0.2,1.0,2));

		vec3 onewallmove = geom::safe_proj(directionN, amongwall);
		trymove = onewallmove*(original_d + _safe_collision_space);

		// effects::debug_dirpoint(cimpact.impact.point, trymove*10.0f, 0.4, vec4(0.2,1.0,0.2,2));

		vec4 tml = geom::safe_normalize_lenght(trymove);
		contextImpact nimpact = _context->impact(this->get_hitbox(), vec3(tml), tml.w + _safe_collision_space);

		if(nimpact.impact.impact)
		{
			trymove = vec3(tml)*std::max((nimpact.impact.distance-_safe_collision_space), 0.0f);
			// effects::debug_point(cimpact.impact.point, 2.6, vec4(1,0.1,0.1,3));
		}
		else
		{
			trymove = onewallmove*original_d;
		}
	}
	
	_position += trymove;
	this->get_hitbox().tp(this->get_position());

	auto gimpact = _context->impact(this->get_hitbox(), -vvv::y(), _safe_collision_space*1.1f).impact.impact;
	
	if((trymove.y <= 0.0f)
		&& ((_vspeed_natural.x*_vspeed_natural.x + _vspeed_natural.z*_vspeed_natural.z) < 3.0f*_speed*_speed)
		&& gimpact)
	{
		this->hit_ground();
	}
	
	if(!gimpact)
	{
		this->leave_ground();
	}


	#warning FOR DEBUG HERE
	if(keyboard::key_pressed('R'))
	{
		_falsepos += trymove;
	}
}

void Player::set_false_target(bool falsetarget)
{
	if(falsetarget)
	{
		_selftarget.set_position_ptr(&_falsepos);
	}
	else
	{
		_selftarget.set_position_ptr(&_position);
	}
}

void Player::look_at(const glm::vec3& look_at_position)
{
	_view.look_at(look_at_position);
}

void Player::tp(const glm::vec3& npos)
{
	_position = npos;
	_falsepos = npos;
}

const glm::vec3& Player::get_position()  const
{
	return _position;
}

const glm::vec3& Player::get_direction() const
{
	return _view.get_direction();
}

const glm::vec3& Player::get_vspeed()     const
{
	return _vspeed;
}

