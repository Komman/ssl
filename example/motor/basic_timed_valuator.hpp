#ifndef _BASIC_TIMED_VALUATOR_HPP_
#define _BASIC_TIMED_VALUATOR_HPP_

#include "motor_debug.hpp"

/*
	Give a well interpolated value given some discrete points
	- The first value has time 0.0f
	- use add_value() to add a specific value at a specific time
	- once the values added, use force_total_duration() to adapt
	the by homeotoecia total time of the animation and all values
	start times and durations.
*/

template<typename T>
class BasicTimedValuator
{
public:
	struct timedValue
	{
		float time;
		float duration;
		T value;
	};

public:
	BasicTimedValuator(const std::vector<timedValue>& values = {});

	void clear();
	void add_value(float duration, const T& value);
	// Keeps time relative proportion (homeotoecia on durations)
	void force_total_duration(float total_duration);

	/*
		If time < 0, returns the initial value
		If time > total duration, returns the last value
	*/
	T evaluate(float time) const;
	void print() const;
	float total_animation_time() const;
	uint values_amount() const;

protected:
	const timedValue& indexed_value(uint index) const; 
	/*
		- index is in [0, values_amount() - 2]
		- percentage is in [0,1], and represent the percentage
		of the step.
	*/
	virtual T normal_evaluate(uint index, float percentage) const =0;

private:
	uint force_research_index(float time) const;

private:
	std::vector<timedValue> _values;
	float _total_time;

	mutable float _last_time;
	mutable int   _last_index;
};







template<typename T>
BasicTimedValuator<T>::BasicTimedValuator(const std::vector<timedValue>& values)
	: _values(values),
	  _total_time(0.0f),
	  _last_time(0.0f),
	  _last_index(0)
{

}

template<typename T>
void BasicTimedValuator<T>::clear()
{
	_values.clear();
	_total_time = 0.0f;
	_last_time  = 0.0f;
	_last_index = 0;
}

template<typename T>
void BasicTimedValuator<T>::add_value(float duration, const T& value)
{
	_values.push_back({_total_time, duration, value});

	_total_time += duration;
}

template<typename T>
float BasicTimedValuator<T>::total_animation_time() const
{
	return _total_time;
}

template<typename T>
void BasicTimedValuator<T>::force_total_duration(float total_duration)
{
	float time_coeff = total_duration / this->total_animation_time();
		
	for(uint i=0; i<_values.size(); i++)
	{
		_values[i].duration *= time_coeff;
		_values[i].time     *= time_coeff;
	}

	_total_time = total_duration;
}

template<typename T>
uint BasicTimedValuator<T>::force_research_index(float time) const
{
	#ifdef SSL_DEBUG
	if(_values.size() == 0)
	{
		mot::err("BasicTimedValuator: call to force_research_index() while there are no timed values added");
	}
	#endif

	if(time < 0.0f)
	{
		return 0;
	}

	uint index;
	for(index=0; index<_values.size(); index++)
	{
		if(time < _values[index].time + _values[index].duration)
		{
			break;
		}
	}

	return std::min(index, _values.size() - 1);
}

template<typename T>
T BasicTimedValuator<T>::evaluate(float time) const
{
	#ifdef SSL_DEBUG
	if(_values.size() == 0)
	{
		mot::err("BasicTimedValuator: call to evaluate() while there are no timed values added");
	}
	#endif

	int dir = (time >= _last_time) ? (1) : (-1);

	if(time < 0.0f)
	{
		return _values[0].value;
	}
	if(time >= _total_time)
	{
		return _values[_values.size() - 1].value;
	}
	_last_time = time;


	while(_last_index >= 0 && _last_index < (int)_values.size())
	{
		if(time >= _values[_last_index].time && time < (_values[_last_index].time + _values[_last_index].duration))
		{
			break;
		}

		_last_index += dir;
	}	

	if(_last_index	 >= (int)_values.size() - 1)
	{
		_last_index = (int)_values.size() - 1;
		return _values[_last_index].value;
	}

	#ifdef SSL_DEBUG
	if(_last_index < 0)
	{
		mot::err("BasicTimedValuator: _last_index < 0");
	}
	if(_last_index >= (int)_values.size() - 1)
	{
		mot::err("BasicTimedValuator: case should have been carried before: _last_index >= (int)_values.size() - 1");
	}
	#endif

	float coeff = (time - _values[_last_index].time)/(_values[_last_index].duration);

	return this->normal_evaluate(_last_index, coeff);
}

template<typename T>
const typename BasicTimedValuator<T>::timedValue& BasicTimedValuator<T>::indexed_value(uint index) const
{
	#ifdef SSL_DEBUG
	if(index >= _values.size())
	{
		mot::err("BasicTimedValuator::indexed_value() index out of bound : "
			     + std::to_string(index) + "/" + std::to_string(_values.size()));
	}
	#endif

	return _values[index];
}

template<typename T>
uint BasicTimedValuator<T>::values_amount() const
{
	return _values.size();
}


template<typename T>
void BasicTimedValuator<T>::print() const
{
	std::cout<<"{\n"<<" _values: {";

	for(const auto& v : _values)
	{
		std::cout<<"(time: "<<v.time<<", dur: "<<v.duration<<", value: ";
		// std::cout<<v.value;
		std::cout<<"X";
		std::cout<<"), ";
	}

	std::cout<<"}\n total_time: "<<_total_time<<"\n last_time: "<<_last_time<<"\n last_index: "<<_last_index<<"\n}"<<std::endl;
}


#endif //_BASIC_TIMED_VALUATOR_HPP_
