#include "hitbox_fusion.hpp"

HitboxFusion::HitboxFusion()
	: HitboxFusion(glm::vec3(0))
{

}


HitboxFusion::HitboxFusion(const glm::vec3& translation)
	: BasicHitbox(translation),
	  _subhitboxes()
{

}

HitboxFusion::HitboxFusion(const hbxTransformation& transformation)
	: BasicHitbox(transformation),
	  _subhitboxes()
{

}

const std::vector<BasicHitbox::storedHitbox>& HitboxFusion::subhitboxes() const
{
	return _subhitboxes;
}


std::vector<const BasicHitbox*> HitboxFusion::get_primary_hitboxes() const
{
	std::vector<const BasicHitbox*> hbxs = {this};
	std::vector<const BasicHitbox*> ret;

	while(hbxs.size() > 0)
	{
		const BasicHitbox* hbx = hbxs[hbxs.size() - 1];
		hbxs.pop_back();

		for(const auto& h : hbx->subhitboxes())
		{
			if(h->get_primary_type() == hitbox_primary::NO_PRIMARY)
			{
				hbxs.push_back(h.get());
			}
			else
			{
				ret.push_back(h.get());
			}
		}
	}

	return ret;
}

void HitboxFusion::fusion_symmetric(const infinitePlan& plan)
{
	auto f1 = std::make_unique<HitboxFusion>();
	auto f2 = this->build_symmetrical(plan);

	for(auto& h : _subhitboxes)
	{
		f1->fusion_hitbox(std::move(h));
	}
	_subhitboxes.clear();

	this->fusion_hitbox(std::move(f1));
	this->fusion_hitbox(std::move(f2));
}



HitboxFusion::storedHitbox HitboxFusion::copy() const
{
	auto ret = std::make_unique<HitboxFusion>(this->transformation());

	for(const auto& shbx : _subhitboxes)
	{
		auto h = shbx->copy();
		ret->fusion_hitbox(std::move(h));
	}

	return ret;	
}

BasicHitbox* HitboxFusion::fusion_hitbox(storedHitbox&& hbx)
{
	hbx->attach_parent(this);
	_subhitboxes.push_back(std::move(hbx));

	this->relative_spacebox_invalidate();

	return _subhitboxes[_subhitboxes.size()-1].get();
}

bool HitboxFusion::in_shape_collision(const BasicHitbox&  hbx) const
{
	return this->subcalls_in_collision(hbx);
}

bool HitboxFusion::in_shape_collision(const SphereHitbox& hbx) const
{
	return this->subcalls_in_collision(hbx);
}

bool HitboxFusion::in_shape_collision(const PrismHitbox&  hbx) const
{
	return this->subcalls_in_collision(hbx);
}

collisionImpact HitboxFusion::shape_impact(const BasicHitbox& hitbox , const glm::vec3& self_directionN, float distance) const
{
	return this->subcalls_impact(hitbox, self_directionN, distance);
}

collisionImpact HitboxFusion::shape_impact(const SphereHitbox& hitbox, const glm::vec3& self_directionN, float distance) const
{
	return this->subcalls_impact(hitbox, self_directionN, distance);
}

collisionImpact HitboxFusion::shape_impact(const PrismHitbox& hitbox , const glm::vec3& self_directionN, float distance) const
{
	return this->subcalls_impact(hitbox, self_directionN, distance);
}

spaceBox HitboxFusion::compute_shape_relative_space_box() const
{
	if(_subhitboxes.size() == 0)
	{
		const glm::vec3& translation = this->transformation().translation;

		return spaceBox{translation, translation};
	}

	spaceBox ret = _subhitboxes[0]->extern_space_box();

	for(const auto& shbx : _subhitboxes)
	{
		ret = ret | shbx->extern_space_box();
	}

	return ret;
}

void HitboxFusion::shape_invalidate_absolute() const
{
	for(const auto& shbx : _subhitboxes)
	{
		shbx->invalidate_absolute();
	}
}


