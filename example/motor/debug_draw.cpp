#include "debug_draw.hpp"
#include "premade_buffers.hpp"
#include "cage_delim.hpp"
#include <glm/gtx/string_cast.hpp>

using namespace glm;
using namespace ssl;

namespace debug_draw
{
	static ShaderGenerator* _gen = NULL;

	static UniformTexture2D<float>* _tobind_texture = NULL;

	static Drawer* _sphere_drawer = NULL; 
	static Drawer* _cagedelim_drawer = NULL; 
	static Drawer* _prism_drawer = NULL; 
	static Drawer* _texture_drawer = NULL; 

	static ArrayBuffer<glm::vec4>* _sphere_center_U_radius = NULL;
	static bool _sphere_drawn = false;

	static CageDelim* _cagedeim = NULL;
	static bool _cagedelim_drawn = false;

	static ElementBuffer* _prism_elements = NULL;
	static ArrayBuffer<glm::vec3>* _prism_vertices = NULL;
	static bool _prism_drawn = false;

	void init()
	{
		if(_gen != NULL)
		{
			mot::err("call to debug_draw::init() while already initialized");
		}

		_tobind_texture = new UniformTexture2D<float>("bound_texture", uvec2(1));

		_gen = new ShaderGenerator("motor/shaders/debug.glsl");
	
		_sphere_center_U_radius = new ArrayBuffer<glm::vec4>(DYNAMIC_DRAW);
		_cagedeim               = new CageDelim(0.02, vec3(1,0,0));

		_sphere_drawer = new Drawer(*_gen, "sphere_vertex", "sphere_fragment");
		_sphere_drawer->depth(true);
		_sphere_drawer->culling(false);
		_sphere_drawer->depth_function(GL_LEQUAL);

		_cagedelim_drawer = new Drawer(*_gen, "cagedelim_vertex", "cagedelim_fragment");
		_cagedelim_drawer->depth(true);
		_cagedelim_drawer->depth_function(GL_LEQUAL);

		_prism_drawer = new Drawer(*_gen, "vertex_only", "prism_fragment");
		_prism_drawer->depth(true);
		_prism_drawer->culling(false);
		_prism_drawer->depth_function(GL_LEQUAL);

		_texture_drawer = new Drawer(*_gen, "texture_vertex", "texture_fragment");

		_prism_elements = new ElementBuffer(DYNAMIC_DRAW);
		_prism_vertices = new ArrayBuffer<glm::vec3>(DYNAMIC_DRAW);
	}

	void free()
	{
		if(_gen == NULL)
		{
			mot::err("call to debug_draw::free() while not initialized");
		}

		delete _gen;
		delete _tobind_texture;
		delete _sphere_center_U_radius;
		delete _sphere_drawer;
		delete _cagedeim;
		delete _cagedelim_drawer;
		delete _texture_drawer;
		delete _prism_drawer;
	}

	void draw(GameDrawer& game_drawer)
	{
		if(_sphere_drawn)
		{
			_sphere_center_U_radius->clear();
		}
		if(_cagedelim_drawn)
		{
			_cagedeim->clear();
		}
		if(_prism_drawn)
		{
			_prism_elements->clear();
			_prism_vertices->clear();
		}

		game_drawer.draw_MS(*_sphere_drawer, premade_buffers::orthonormal_cube().get_elements(), *(premade_buffers::orthonormal_cube().get_buffers()[0]), *_sphere_center_U_radius);
		_sphere_drawn = true;
		game_drawer.draw_MS(*_cagedelim_drawer, (ElementDrawableObject&)(*_cagedeim));
		_cagedelim_drawn = true;
		game_drawer.draw_MS(*_prism_drawer, *_prism_elements, *_prism_vertices);
		_prism_drawn = true;
	}


	void sphere(const sphereInfo& sphere)
	{
		if(_sphere_drawn)
		{
			_sphere_center_U_radius->clear();
			_sphere_drawn = false;
		}

		_sphere_center_U_radius->add_value(vec4(sphere.center, sphere.radius));
	}

	void sphere(const SphereHitbox& sphere_hbx)
	{
		sphere(sphere_hbx.get_absolute_shape());
	}

	void prism(const prismInfo& prism)
	{
		if(_prism_drawn)
		{
			_prism_elements->clear();
			_prism_vertices->clear();
			_prism_drawn = false;
		}

		uint first = _prism_vertices->size();
		_prism_vertices->add_value(prism.points[0]);
		_prism_vertices->add_value(prism.points[1]);
		_prism_vertices->add_value(prism.points[2]);
		_prism_vertices->add_value(prism.points[3]);

		_prism_elements->add_triangle(first+0, first+1, first+2);
		_prism_elements->add_triangle(first+0, first+1, first+3);
		_prism_elements->add_triangle(first+0, first+2, first+3);
		_prism_elements->add_triangle(first+1, first+2, first+3);
	}

	void prism(const PrismHitbox& prism_hbx)
	{
		prism(prism_hbx.get_absolute_shape());
	}

	void fusion_hbx(const HitboxFusion& fusion)	
	{
		auto prim = fusion.get_primary_hitboxes();

		for(const BasicHitbox* h : prim)
		{
			if(h->get_primary_type() == hitbox_primary::SPHERE)
			{
				sphere(*((const SphereHitbox*)(h)));
			}
			if(h->get_primary_type() == hitbox_primary::PRISM)
			{
				prism(*((const PrismHitbox*)(h)));
			}
		}
	}

	void hitbox(const BasicHitbox& hbx)	
	{
		auto prim = hbx.get_primary_type();

		if(prim == hitbox_primary::NO_PRIMARY)
		{
			fusion_hbx((const HitboxFusion&)hbx);
		}
		if(prim == hitbox_primary::SPHERE)
		{
			sphere((const SphereHitbox&)hbx);
		}
		if(prim == hitbox_primary::PRISM)
		{
			prism((const PrismHitbox&)hbx);
		}
	}

	void space_box(const spaceBox& space_box)
	{
		if(_cagedelim_drawn == true)
		{
			_cagedeim->clear();
			_cagedelim_drawn = false;
		}

		_cagedeim->add_cage(space_box);
	}

	void space_box(const BasicHitbox& hitbox)
	{
		space_box(hitbox.get_minmax_coord());
	}

	Drawer& texture_drawer()
	{
		return *_texture_drawer;
	}

	void texture(const UniformTexture2D<glm::vec3>& texture)
	{
		auto binding = _texture_drawer->get_texture_binding("bound_texture");
		_texture_drawer->change_texture_binding(binding, texture);
	
		_texture_drawer->draw(ssl::buffers::screenfill());
	}

	void texture(const UniformTexture2D<glm::vec4>& texture)
	{
		auto binding = _texture_drawer->get_texture_binding("bound_texture");
		_texture_drawer->change_texture_binding(binding, texture);
	
		_texture_drawer->draw(ssl::buffers::screenfill());
	}

	void line(const triLine& line)
	{
		if(_prism_drawn)
		{
			_prism_elements->clear();
			_prism_vertices->clear();
			_prism_drawn = false;
		}

		vec3 ZaxiN = glm::normalize(line.p2 - line.p1);
		vec3 YaxiN = vec3(0,1,0);
		vec3 Xaxis = glm::normalize(glm::cross(ZaxiN, YaxiN));
		YaxiN = glm::normalize(glm::cross(ZaxiN, Xaxis));
	
		vec3 dir1 = YaxiN/2.0f;
		vec3 dir2 = (-YaxiN+Xaxis)/2.0f;
		vec3 dir3 = (-YaxiN-Xaxis)/2.0f;

		uint first = _prism_vertices->size();
		_prism_vertices->add_value(line.p1 + dir1*line.radius);
		_prism_vertices->add_value(line.p1 + dir2*line.radius);
		_prism_vertices->add_value(line.p1 + dir3*line.radius);

		_prism_vertices->add_value(line.p2 + dir1*line.radius);
		_prism_vertices->add_value(line.p2 + dir2*line.radius);
		_prism_vertices->add_value(line.p2 + dir3*line.radius);
	
		_prism_elements->add_triangle(first+0, first+1, first+2);
		_prism_elements->add_triangle(first+3, first+4, first+5);

		_prism_elements->add_rectangle(first+0, first+1, first+4, first+3);
		_prism_elements->add_rectangle(first+1, first+2, first+5, first+4);
		_prism_elements->add_rectangle(first+2, first+0, first+3, first+5);
	}

	void graph(const PlanGraph& g, float thickness, const glm::vec3& dep, float size_coeff)
	{
		auto vertices = g.all_vertices();

		for(auto u : vertices)
		{
			vec3 pu = dep + vec3(g[u].x, 0, g[u].y)*size_coeff;

			sphere({pu, thickness*2.0f});

			for(auto v : vertices)
			{
				vec3 pv = dep + vec3(g[v].x, 0, g[v].y)*size_coeff;
				
				if(u < v && g.connected(u,v))
				{
					line({pu, pv, thickness});
				}
			}
		}
	}

	void rgraph(const PlanGraph& g, const ReliefedPlanDualGraph& dual, float thickness, const glm::vec3& dep, float size_coeff)
	{
		auto faces = dual.all_vertices();

		for(uint f : faces)
		{
			float h = dual.face_height(f);

			planDualFace face = dual[f];

			for(Graph::Edge e : face.edges)
			{	
				vec3 pu = dep + vec3(g[e.first].x, h, g[e.first].y)*size_coeff;
				vec3 pv = dep + vec3(g[e.second].x, h, g[e.second].y)*size_coeff;

				sphere({pu, thickness*2.0f});
				line({pu, pv, thickness});
			}
		}
	}


};
