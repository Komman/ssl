#ifndef _TMPLT_HPP_
#define _TMPLT_HPP_

namespace tmplt
{
	template<std::size_t N, std::size_t... Seq>
	constexpr std::index_sequence<N + Seq...> add(std::index_sequence<Seq...>) {return {};};

	template<std::size_t Min, std::size_t Max>
	using make_index_range = decltype(add<Min>(std::make_index_sequence<Max - Min>{}));

	template<typename TupleType, std::size_t... Is>
	auto subtuple(const TupleType& tuple, std::index_sequence<Is...>)
	{	
		return std::make_tuple(std::get<Is>(tuple)...);
	}
};

#endif //_TMPLT_HPP_
