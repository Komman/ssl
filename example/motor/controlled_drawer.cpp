#include "controlled_drawer.hpp"

ControlledDrawer::ControlledDrawer(const BasicShaderParser&        parser,
							   	   const std::string&              vertex_shader_name,
							   	   const std::string&              fragment_shader_name,
							   	   const std::vector<std::string>& fragment_modifiers,
							   	   DrawerPackController&           controllor)
	: Drawer(parser, vertex_shader_name, fragment_shader_name, fragment_modifiers)
{
	controllor.add_drawer(this);
}