#ifndef _STRAIGHT_LINE_HPP_
#define _STRAIGHT_LINE_HPP_

#include <glm/glm.hpp>

/*
	2D here
*/
class StraightLine
{
public:
	enum singleAxis {X_AXIS = 0, Y_AXIS};

public:
	StraightLine(const glm::vec2& point1, const glm::vec2& point2);
	StraightLine(singleAxis axis, float value);

	// Throw error if parrallel
	glm::vec2 intersection(const StraightLine& l);
	bool      is_parrallel(const StraightLine& l);

	glm::vec2 operator&&(const StraightLine& l) {return this->intersection(l);}

private:
	// y = ax + b
	float _a;
	float _b;

	bool _a_inf; // In this case: x = _b;
};

#endif //_STRAIGHT_LINE_HPP_
