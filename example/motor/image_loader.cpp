#include "image_loader.hpp"

#include "motor_debug.hpp"

#include <glm/gtx/string_cast.hpp>

using namespace std;

ImageLoader::ImageLoader(const std::string& path)
	: _size(0),
	  _data(),
	  _max_value(1)
{
	ifstream file(path);

	if(!(file.is_open()))
	{
		mot::err("ImageLoader(): can't open file " + path);
	}

	this->load_image(file);

	file.close();
}

std::vector<float> ImageLoader::float_RGB() const
{
	std::vector<float> ret;

	for(uint v : _data)
	{
		ret.push_back(float(v)/float(_max_value));
	}

	return ret;
}

static bool _printinfos = false;

void ImageLoader::load_image(std::ifstream& file)
{
	string str;
	file>>str;
	if(str != "P3")
	{
		mot::err("ImageLoader::load_image(): can load only PPM P3 files, and here, try to load: " + str);
	}

	file>>_size.x;
	file>>_size.y;
	file>>_max_value;

	if(_printinfos)
	{
		cout<<"File dimension: "<<glm::to_string(_size)<<endl;
		cout<<"With max value "<<_max_value<<endl;
	}

	for(uint i=0; i<_size.x*_size.y*3; i++)
	{
		_data.push_back(0);
		file>>(_data[_data.size()-1]);
	}
}


const std::vector<uint> ImageLoader::data() const
{
	return _data;
}

const glm::uvec2& ImageLoader::size() const
{
	return _size;
}

