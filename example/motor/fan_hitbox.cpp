#include "fan_hitbox.hpp"

using namespace glm;

FanHitbox::FanHitbox(const fanInfo& shape)
	: ShapedHitboxFusion(shape)
{
	this->build();
}

void FanHitbox::build()
{
	const auto& fan = this->get_shape();
	const auto& cycle = fan.cycle;

	vec3 center = geom::mean(cycle);

	for(uint i=0; i<cycle.size(); i++)
	{
		this->fusion_hitbox<PrismHitbox>(prismInfo{.points  = {
			center,
			fan.origin,
			cycle[i],
			cycle[(i+1)%cycle.size()]
		}});
	}
}
