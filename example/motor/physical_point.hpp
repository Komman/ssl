#ifndef _PHYSICAL_POINT_HPP_
#define _PHYSICAL_POINT_HPP_

#include <glm/glm.hpp>

#include "gutils.hpp"

class BasicPoint
{
public:
	BasicPoint() {}

	virtual const glm::vec3& get_position() const =0;
};

class BasicDirectedPoint : public BasicPoint
{
public:
	BasicDirectedPoint() : BasicPoint() {}

	virtual const glm::vec3& get_speed()    const =0;
};

class BasicPhysicalPoint : public BasicDirectedPoint
{
public:
	BasicPhysicalPoint() : BasicDirectedPoint() {}
	virtual ~BasicPhysicalPoint() {}

	virtual void animate(float dt) =0;
	virtual void tp(const glm::vec3& position) =0;
	virtual void move(const glm::vec3& dposition) =0;
};

class PhysicalPoint : public BasicPhysicalPoint
{
public:
	PhysicalPoint(const glm::vec3& position);

	PhysicalPoint(const glm::vec3& position,
				  const glm::vec3& speed,
				  const glm::vec3& acceleration);
	virtual ~PhysicalPoint() {}

	void animate(float dt) 			      override;
	void tp(const glm::vec3& position)    override;
	void move(const glm::vec3& dposition) override;

	const glm::vec3& get_position() const override;
	const glm::vec3& get_speed()    const override;

	GETTER_BUILDER(glm::vec3, acceleration)

	void set_position(const glm::vec3& position);
	void set_speed(const glm::vec3& speed);
	void set_acceleration(const glm::vec3& acceleration);

private:
	glm::vec3 _position;
	glm::vec3 _speed;
	glm::vec3 _acceleration;
};

#endif //_PHYSICAL_POINT_HPP_
