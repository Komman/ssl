#include "building.hpp"
#include "motor_debug.hpp"


using namespace glm;

namespace building
{
	namespace square_base
	{
		uint v(ArrayBuffer<glm::vec3>& vertices,
			   const orientedPlan& centered_plan,
			   const glm::vec2& size)
		{
			vertices.add_value(centered_plan.point
							   - centered_plan.dirxN*size.x/2.0f
							   - centered_plan.diryN*size.y/2.0f);
			vertices.add_value(centered_plan.point
							   + centered_plan.dirxN*size.x/2.0f
							   - centered_plan.diryN*size.y/2.0f);
			vertices.add_value(centered_plan.point
							   + centered_plan.dirxN*size.x/2.0f
							   + centered_plan.diryN*size.y/2.0f);
			vertices.add_value(centered_plan.point
							   - centered_plan.dirxN*size.x/2.0f
							   + centered_plan.diryN*size.y/2.0f);
			return 4;
		}
		
		// Top of the plan filled
		uint top(ArrayBuffer<glm::vec3>& vertices,
				 ElementBuffer& indices,
				 const orientedPlan& centered_plan,
				 const glm::vec2& size)
		{
			uint start = vertices.size();
			
			indices.add_rectangle(start+0,
								  start+1,
								  start+2,
								  start+3, 
								  true);

			return v(vertices, centered_plan, size);
		}

		std::vector<glm::vec3> pos(const orientedPlan& centered_plan,
						    	   const glm::vec2& size)
		{
			std::vector<vec3> ret;

			ret.push_back(centered_plan.point
						  - centered_plan.dirxN*size.x/2.0f
						  - centered_plan.diryN*size.y/2.0f);
			ret.push_back(centered_plan.point
						  + centered_plan.dirxN*size.x/2.0f
						  - centered_plan.diryN*size.y/2.0f);
			ret.push_back(centered_plan.point
						  + centered_plan.dirxN*size.x/2.0f
						  + centered_plan.diryN*size.y/2.0f);
			ret.push_back(centered_plan.point
						  - centered_plan.dirxN*size.x/2.0f
						  + centered_plan.diryN*size.y/2.0f);

			return ret;
		}
	};

	namespace paver
	{
		uint v(ArrayBuffer<glm::vec3>& vertices,
			   const paverInfoN& paver)
		{
			orientedPlan p1 = {
				.point = paver.bottom 
						+ (paver.dir_xN*paver.size.x + paver.dir_zN*paver.size.z)/2.0f,
				.dirxN = paver.dir_xN,
				.diryN = paver.dir_zN
			};
			orientedPlan p2 = {
				.point = paver.bottom 
						+ (paver.dir_xN*paver.size.x + paver.dir_zN*paver.size.z)/2.0f
						+  paver.dir_yN*paver.size.y,
				.dirxN = paver.dir_xN,
				.diryN = paver.dir_zN
			};

			uint c1 = square_base::v(vertices, p1, vec2(paver.size.x, paver.size.z));
			uint c2 = square_base::v(vertices, p2, vec2(paver.size.x, paver.size.z));

			return c1 + c2;
		}

		uint v(ArrayBuffer<glm::vec3>& vertices,
			   const paverInfo& paver)
		{
			return v(vertices, paverInfoN{
				.bottom  = paver.bottom,
				.dir_xN = glm::normalize(paver.sized_x),
				.dir_yN = glm::normalize(paver.sized_y),
				.dir_zN = glm::normalize(paver.sized_z),
				.size   = vec3(glm::length(paver.sized_x),
							   glm::length(paver.sized_y),
							   glm::length(paver.sized_z))
			});
		}

		uint v(ArrayBuffer<glm::vec3>& vertices,
			   const centredPaverInfo& paver)
		{
			return v(vertices, paverInfo{
				.bottom  = paver.center - (paver.sized_x + paver.sized_y + paver.sized_z)/2.0f,
				.sized_x = paver.sized_x,
				.sized_y = paver.sized_y,
				.sized_z = paver.sized_z
			});
		}


		uint v(ArrayBuffer<glm::vec3>& vertices,
			   const transPaverInfo& paver)
		{
			vertices.add_value(paver.square_base[0]);
			vertices.add_value(paver.square_base[1]);
			vertices.add_value(paver.square_base[2]);
			vertices.add_value(paver.square_base[3]);
			vertices.add_value(paver.square_base[0] + paver.sized_direction);
			vertices.add_value(paver.square_base[1] + paver.sized_direction);
			vertices.add_value(paver.square_base[2] + paver.sized_direction);
			vertices.add_value(paver.square_base[3] + paver.sized_direction);

			return 8;
		}

		uint v(ArrayBuffer<glm::vec3>& vertices,
			   const ycentredPaverInfo& paver)
		{
			return v(vertices, geom::to_paver_info(paver));
		}
	};

	namespace crown
	{
		uint squared_v(ArrayBuffer<glm::vec3>& vertices,
			   	   	   const orientedPlan& centered_plan,
					   const glm::vec2& size,
			   	   	   float height,
			   	   	   float wall_thickness)
		{
			uint start = vertices.size();
			auto second_plan = centered_plan;
			second_plan.point += glm::normalize(glm::cross(second_plan.dirxN, second_plan.diryN))*height,
			
			building::square_base::v(vertices, centered_plan, size);
			building::square_base::v(vertices, centered_plan, size - vec2(wall_thickness));
			building::square_base::v(vertices, second_plan, size);
			building::square_base::v(vertices, second_plan, size - vec2(wall_thickness));
		
			return vertices.size() - start;
		}

		uint squared_topsided(ArrayBuffer<glm::vec3>& vertices,
				   			  ElementBuffer& indices,
			   	   	   	   	  const orientedPlan& centered_plan,
					   	   	  const glm::vec2& size,
			   	   	   	   	  float height,
			   	   	   	   	  float wall_thickness)
		{
			uint i = vertices.size();

			for(uint j=0; j<2; j++)
			{
				indices.add_rectangle(
					i+0+j*4,
					i+1+j*4,
					i+9+j*4,
					i+8+j*4,
					j==1
				);
				indices.add_rectangle(
					i+1 +j*4,
					i+2 +j*4,
					i+10+j*4,
					i+9 +j*4,
					j==1
				);
				indices.add_rectangle(
					i+2 +j*4,
					i+3 +j*4,
					i+11+j*4,
					i+10+j*4,
					j==1
				);
				indices.add_rectangle(
					i+3 +j*4,
					i+0 +j*4,
					i+8 +j*4,
					i+11+j*4,
					j==1
				);
			}

			for(uint j=0; j<4; j++)
			{
				indices.add_rectangle(
					i+8 + 0+j,
					i+8 + 0+(j+1)%4,
					i+8 + 4+(j+1)%4,
					i+8 + 4+j
				);
			}
			
			return squared_v(vertices, centered_plan, size, height, wall_thickness);
		}
	};

	namespace cone
	{	
		uint v(ArrayBuffer<glm::vec3>& vertices,
			   const glm::vec3& base_center,
			   const glm::vec3& to_top,
			   const glm::vec3& base_aligementN,
			   float radius,
			   uint  discr)
		{
			uint start = vertices.size();

			vertices.add_value(base_center+to_top);

			vec3 yaxisN = glm::normalize(to_top);
			float dangle = 2.0f*float(M_PI)/(discr);

			for(uint i=0; i<discr; i++)
			{
				vertices.add_value(geom::circle_point(
					base_center, 
					base_aligementN,
					yaxisN,
					dangle*float(i),
					radius
				));
			}

			return vertices.size() - start;
		}

		uint sided(ArrayBuffer<glm::vec3>& vertices,
				   ElementBuffer& indices,
				   const glm::vec3& base_center,
				   const glm::vec3& to_top,
				   const glm::vec3& base_aligementN,
				   float radius,
				   uint  discr)
		{
			uint start = vertices.size();

			v(vertices, base_center, to_top, base_aligementN, radius, discr);

			for(uint i=0; i<discr; i++)
			{
				indices.add_triangle(start,
									 start+1+((i+1)%discr),
									 start+1+((i+2)%discr));
			}

			return vertices.size() - start;
		}

		uint full_ext(ArrayBuffer<glm::vec3>& vertices,
				   	  ElementBuffer& indices,
				   	  const glm::vec3& base_center,
				   	  const glm::vec3& to_top,
				   	  const glm::vec3& base_aligementN,
				   	  float radius,
				   	  uint  discr)
		{
			uint start = vertices.size();

			sided(vertices, indices, base_center, to_top, base_aligementN, radius, discr);

			for(uint i=0; i<discr-2; i++)
			{
				indices.add_triangle(start+1+ 0,
									 start+1+ ((1+i)%discr),
									 start+1+ ((2+i)%discr), true);
			}

			return vertices.size() - start;
		}
	};

	namespace extruding
	{
		namespace convex
		{
			uint v(ArrayBuffer<glm::vec3>& vertices,
			  	   const std::vector<uint>& oredered_convex_shape_indices,
			  	   const glm::vec3& sized_extr_dir)
			{
				uint start = vertices.size();

				for(uint e : oredered_convex_shape_indices)
				{
					vertices.add_value(vertices[e] + sized_extr_dir);
				}

				return vertices.size() - start;
			}

			uint sided(ArrayBuffer<glm::vec3>& vertices,
					   ElementBuffer& indices,
				  	   const std::vector<uint>& oredered_convex_shape_indices,
				  	   const glm::vec3& sized_extr_dir)
			{
				uint start = vertices.size();
				v(vertices, oredered_convex_shape_indices, sized_extr_dir);

				auto& v = oredered_convex_shape_indices;
				uint n  = v.size();

				for(uint i=0; i<n; i++)
				{				
					indices.add_rectangle(
						v[(i+0)%n],
						v[(i+1)%n],
						start + (i+1)%n,
						start + (i+0)%n,
						true
					);
				}

				return vertices.size() - start;
			}
		};
	};

	namespace hanoi
	{
		namespace squared
		{
			uint v(ArrayBuffer<glm::vec3>& vertices,
				   const std::vector<vec2>& thick_heights,
				   const glm::vec3& bot_center,
				   const spaceBase& base)
			{
				uint s = thick_heights.size();
				for(uint i=0; i<s; i++)
				{
					vertices.add_value(bot_center - base.dirx*thick_heights[i].x/2.0f - base.dirz*thick_heights[i].x/2.0f + base.diry*thick_heights[i].y);
					vertices.add_value(bot_center + base.dirx*thick_heights[i].x/2.0f - base.dirz*thick_heights[i].x/2.0f + base.diry*thick_heights[i].y);
					vertices.add_value(bot_center + base.dirx*thick_heights[i].x/2.0f + base.dirz*thick_heights[i].x/2.0f + base.diry*thick_heights[i].y);
					vertices.add_value(bot_center - base.dirx*thick_heights[i].x/2.0f + base.dirz*thick_heights[i].x/2.0f + base.diry*thick_heights[i].y);
				
				}

				return thick_heights.size()*4;
			}

			uint sided(ArrayBuffer<glm::vec3>& vertices,
					   ElementBuffer& indices,
					   const std::vector<vec2>& thick_heights,
					   const glm::vec3& bot_center,
					   const spaceBase& base)
			{
				uint start = vertices.size();
				uint ret = v(vertices, thick_heights, bot_center, base);
				uint s   = thick_heights.size();

				for(uint i=0; i<s-1; i++)
				{
					uint j = (i+1)%s;

					for(uint k=0; k<4; k++)
					{
						indices.add_rectangle(start+4*i+k, start+4*i+(k+1)%4, start+4*j+(k+1)%4, start+4*j+k, true);
					}
				}

				return ret;
			}

			uint topsided(ArrayBuffer<glm::vec3>& vertices,
					   ElementBuffer& indices,
					   const std::vector<vec2>& thick_heights,
					   const glm::vec3& bot_center,
					   const spaceBase& base)
			{
				uint start = vertices.size();
				uint ret = v(vertices, thick_heights, bot_center, base);
				uint s   = thick_heights.size();

				for(uint i=0; i<s-1; i++)
				{
					uint j = (i+1)%s;

					for(uint k=0; k<4; k++)
					{
						indices.add_rectangle(start+4*i+k, start+4*i+(k+1)%4, start+4*j+(k+1)%4, start+4*j+k, true);
					}
				}

				indices.add_rectangle(
					start+(s-1)*4+0,
					start+(s-1)*4+1,
					start+(s-1)*4+2,
					start+(s-1)*4+3,
					true
				);

				return ret;
			}


			// WITH VEC4
			uint v(ArrayBuffer<glm::vec4>& vertices,
				   const std::vector<vec2>& thick_heights,
				   const glm::vec3& bot_center,
				   const spaceBase& base)
			{
				uint s = thick_heights.size();
				for(uint i=0; i<s; i++)
				{
					vertices.add_value(vec4(bot_center - base.dirx*thick_heights[i].x/2.0f - base.dirz*thick_heights[i].x/2.0f + base.diry*thick_heights[i].y, 0.0f));
					vertices.add_value(vec4(bot_center + base.dirx*thick_heights[i].x/2.0f - base.dirz*thick_heights[i].x/2.0f + base.diry*thick_heights[i].y, 0.0f));
					vertices.add_value(vec4(bot_center + base.dirx*thick_heights[i].x/2.0f + base.dirz*thick_heights[i].x/2.0f + base.diry*thick_heights[i].y, 0.0f));
					vertices.add_value(vec4(bot_center - base.dirx*thick_heights[i].x/2.0f + base.dirz*thick_heights[i].x/2.0f + base.diry*thick_heights[i].y, 0.0f));
				
				}

				return thick_heights.size()*4;
			}

			uint sided(ArrayBuffer<glm::vec4>& vertices,
					   ElementBuffer& indices,
					   const std::vector<vec2>& thick_heights,
					   const glm::vec3& bot_center,
					   const spaceBase& base)
			{
				uint start = vertices.size();
				uint ret = v(vertices, thick_heights, bot_center, base);
				uint s   = thick_heights.size();

				for(uint i=0; i<s-1; i++)
				{
					uint j = (i+1)%s;

					for(uint k=0; k<4; k++)
					{
						indices.add_rectangle(start+4*i+k, start+4*i+(k+1)%4, start+4*j+(k+1)%4, start+4*j+k, true);
					}
				}

				return ret;
			}
		};
		namespace cycled
		{
			uint v(ArrayBuffer<glm::vec3>& vertices,
				   const std::vector<glm::vec2>& thick_heights,
				   const glm::vec3& bot_center,
				   uint points_amount,
				   const glm::vec3& dirY,
				   const glm::vec3& dirX)
			{
				uint start = vertices.size();
				uint s = thick_heights.size();

				for(uint i=0; i<s; i++)
				{
					auto points = geom::circle_points(
						bot_center + dirY*thick_heights[i].y,
						dirX,
						dirY,
						thick_heights[i].x,
						points_amount
					);

					for(const auto& p : points)
					{
						vertices.add_value(p);
					}
				}

				return vertices.size() - start;
			}

			uint sided(ArrayBuffer<glm::vec3>& vertices,
					   ElementBuffer& indices,
					   const std::vector<glm::vec2>& thick_heights,
					   const glm::vec3& bot_center,
					   uint points_amount,
					   const glm::vec3& dirY,
					   const glm::vec3& dirX)
			{
				uint start = vertices.size();
				uint ret = v(vertices, thick_heights, bot_center, points_amount, dirY, dirX);
				uint s   = thick_heights.size();

				for(uint i=0; i<s-1; i++)
				{
					uint j = (i+1)%s;

					for(uint k=0; k<points_amount; k++)
					{
						uint l = (k+1)%points_amount;

						indices.add_rectangle(start+points_amount*i+k,
											  start+points_amount*i+l,
											  start+points_amount*j+l,
											  start+points_amount*j+k,
											  false);
					}
				}

				return ret;
			}
		};
	};



	namespace arc
	{
		/*
			^ y
			|
			|
			o----> x
			
			          (1,1)
			      __/\
			     /    \
			   /       \
			  /         \
			 |           \
			/             \
			| 1            \
			|        alpha /\
			| 0        _ _/_ \

			(0,0)
		*/
		std::vector<glm::vec2> v(float angle, int points_amount, const glm::vec2& size)
		{
			if(points_amount <= 1)
			{
				mot::err("building::arc::v(): points_amount must be >=2");
			}
			if(angle < 0 || angle > float(M_PI/2.0f))
			{
				mot::err("building::arc::v(): angle must be in [0, PI/2]");
			}

			vec2 resize = vec2(1.0f-cos(angle), sin(angle));
			std::vector<glm::vec2> ret;

			for(int i=0; i<points_amount; i++)
			{
				vec2 point = geom::circle_point(vec2(0,0),
												vec2(1,0),
												float(i)/float(points_amount-1)*angle,
												1.0f);
				ret.push_back((point - vec2(cos(angle), 0))*size/resize);
			}

			return ret;
		}

		std::vector<glm::vec2> doublev(float angle, int points_amount, const glm::vec2& size)
		{
			std::vector<glm::vec2> right = v(angle, points_amount);

			std::vector<glm::vec2> ret;

			for(uint i=0; i< right.size()-1; i++)
			{
				vec2 halfcurve = right[i] * vec2(0.5,1);
				ret.push_back(vec2(0.5 + halfcurve.x, halfcurve.y)*size);
				ret.push_back(vec2(0.5 - halfcurve.x, halfcurve.y)*size);
			}

			ret.push_back(vec2(0.5,1)*size);

			return ret;
		}

		std::vector<glm::vec3> thicked_vertices(
				   			const glm::vec3& bottom,
				   			const glm::vec3& xaxisN,
				   			const glm::vec3& yaxisN,
				   			const glm::vec3& sizedZ,
							float thickness,
							float angle,
							int points_amount,
							const glm::vec2& size)
		{
			std::vector<vec2> pods2D = doublev(angle, points_amount, size);

			std::vector<glm::vec3> ret;

			uint startX  = 0;
			uint startE  = 4*(points_amount-1)+2;

			for(uint i=0; i<pods2D.size(); i++)
			{
				ret.push_back(bottom + xaxisN*pods2D[i].x + yaxisN*pods2D[i].y);
			}

			for(uint i=0; i<pods2D.size(); i++)
			{
				ret.push_back(bottom + xaxisN*(size.x/2.0f + (pods2D[i].x-size.x/2.0f)*(1.0f - thickness)) + yaxisN*pods2D[i].y*(1.0f - thickness));
			}

			for(uint i=0; i<(startE-startX); i++)
			{
				ret.push_back(ret[startX + i] + sizedZ);
			}

			return ret;
		}

		uint thickedowntoped(ArrayBuffer<glm::vec3>& vertices,
				   			 ElementBuffer& indices,
				   			 const glm::vec3& bottom,
				   			 const glm::vec3& xaxisN,
				   			 const glm::vec3& yaxisN,
				   			 const glm::vec3& sizedZ,
							 float thickness,
							 float angle,
							 int points_amount,
							 const glm::vec2& size)
		{
			std::vector<vec3> positions = thicked_vertices(bottom, xaxisN, yaxisN, sizedZ, thickness, angle, points_amount, size);

			uint startX = vertices.size();
			uint startL  = startX + 2*(points_amount-1)+1;
			uint startE  = startX + 4*(points_amount-1)+2;
			uint startEL = startX + 6*(points_amount-1)+3;
			uint topdi = 2*(points_amount-1);

			for(const auto& p : positions)
			{
				vertices.add_value(p);
			}
			
			for(uint i=0; i<(uint)(points_amount-1); i++)
			{
				indices.add_rectangle(startX + 2*i,
									  startX + std::min(2*(i+1), topdi),
									  startE + std::min(2*(i+1), topdi),
									  startE + 2*i,
									  false);

				indices.add_rectangle(startX + 2*i+1,
									  startX + std::min(2*(i+1)+1, topdi),
									  startE + std::min(2*(i+1)+1, topdi),
									  startE + 2*i+1,
									  true);

				indices.add_rectangle(startL + 2*i,
									  startL + std::min(2*(i+1), topdi),
									  startEL + std::min(2*(i+1), topdi),
									  startEL + 2*i,
									  true);

				indices.add_rectangle(startL + 2*i+1,
									  startL + std::min(2*(i+1)+1, topdi),
									  startEL + std::min(2*(i+1)+1, topdi),
									  startEL + 2*i+1,
									  false);

				indices.add_rectangle(startE + 2*i,
									  startE + std::min(2*(i+1), topdi),
									  startEL + std::min(2*(i+1), topdi),
									  startEL + 2*i,
									  false);

				indices.add_rectangle(startE + 2*i+1,
									  startE + std::min(2*(i+1)+1, topdi),
									  startEL + std::min(2*(i+1)+1, topdi),
									  startEL + 2*i+1,
									  true);
			}

			return positions.size();
		}
	}


	namespace doored_wall
	{
		/*
			outside :
			
			3------------2
			|            |
			|  7------6  |
			|  |      |  |
			|  |      |  |
			|  |      |  |
			0--4      5--1

			Sames indices +8 on the inside 
		*/

		std::vector<glm::vec3> v(const dooredWall& door)
		{
			std::vector<glm::vec3> ret;
			ret.reserve(16);

			ret.push_back(door.bot1);
			ret.push_back(door.bot2);
			ret.push_back(door.bot2 + door.diryN*door.height);
			ret.push_back(door.bot1 + door.diryN*door.height);

			ret.push_back(door.door_bot1());
			ret.push_back(door.door_bot2());
			ret.push_back(door.door_bot2() + door.diryN*door.door_height);
			ret.push_back(door.door_bot1() + door.diryN*door.door_height);

			ret.push_back(door.bot1 + door.inside_dir1N*door.thickness);
			ret.push_back(door.bot2 + door.inside_dir2N*door.thickness);
			ret.push_back(door.bot2 + door.diryN*door.height + door.inside_dir2N*door.thickness);
			ret.push_back(door.bot1 + door.diryN*door.height + door.inside_dir1N*door.thickness);

			ret.push_back(door.inside_door_bot1());
			ret.push_back(door.inside_door_bot2());
			ret.push_back(door.inside_door_bot2() + door.diryN*door.door_height);
			ret.push_back(door.inside_door_bot1() + door.diryN*door.door_height);

			return ret;
		}

		uint v(ArrayBuffer<glm::vec3>& vertices,
			   const dooredWall& door)
		{
			auto ret = v(door);

			for(const auto& e : ret)
			{
				vertices.add_value(e);
			}

			return ret.size();
		}

		uint topsided(ArrayBuffer<glm::vec3>& vertices,
					  ElementBuffer& indices,
					  const dooredWall& door,
					  bool flip)
		{
			uint i   = vertices.size();
			uint ret = v(vertices, door);
		
			indices.add_rectangle(i+0, i+4, i+7, i+3, flip);
			indices.add_rectangle(i+7, i+6, i+2, i+3, flip);
			indices.add_rectangle(i+6, i+5, i+1, i+2, flip);

			indices.add_rectangle(i+8+0, i+8+4, i+8+7, i+8+3, !flip);
			indices.add_rectangle(i+8+7, i+8+6, i+8+2, i+8+3, !flip);
			indices.add_rectangle(i+8+6, i+8+5, i+8+1, i+8+2, !flip);

			indices.add_rectangle(i+4, i+8+4, i+8+7, i+7, flip);
			indices.add_rectangle(i+7, i+8+7, i+8+6, i+6, flip);
			indices.add_rectangle(i+6, i+8+6, i+8+5, i+5, flip);

			indices.add_rectangle(i+2, i+8+2, i+8+3, i+3, flip);

			indices.add_rectangle(i+1, i+8+1, i+8+2, i+2, flip);
			indices.add_rectangle(i+3, i+8+3, i+8+0, i+0, flip);

			return ret;
		}

		uint frontsided(ArrayBuffer<glm::vec3>& vertices,
					  ElementBuffer& indices,
					  const dooredWall& door,
					  bool flip)
		{
			uint i   = vertices.size();
			uint ret = v(vertices, door);
		
			indices.add_rectangle(i+0, i+4, i+7, i+3, flip);
			indices.add_rectangle(i+7, i+6, i+2, i+3, flip);
			indices.add_rectangle(i+6, i+5, i+1, i+2, flip);

			indices.add_rectangle(i+8+0, i+8+4, i+8+7, i+8+3, !flip);
			indices.add_rectangle(i+8+7, i+8+6, i+8+2, i+8+3, !flip);
			indices.add_rectangle(i+8+6, i+8+5, i+8+1, i+8+2, !flip);

			indices.add_rectangle(i+4, i+8+4, i+8+7, i+7, flip);
			indices.add_rectangle(i+7, i+8+7, i+8+6, i+6, flip);
			indices.add_rectangle(i+6, i+8+6, i+8+5, i+5, flip);

			return ret;
		}

		uint extfrontsided(ArrayBuffer<glm::vec3>& vertices,
					  ElementBuffer& indices,
					  const dooredWall& door,
					  bool flip)
		{
			uint i   = vertices.size();
			uint ret = v(vertices, door);
		
			indices.add_rectangle(i+0, i+4, i+7, i+3, flip);
			indices.add_rectangle(i+7, i+6, i+2, i+3, flip);
			indices.add_rectangle(i+6, i+5, i+1, i+2, flip);

			indices.add_rectangle(i+4, i+8+4, i+8+7, i+7, flip);
			indices.add_rectangle(i+7, i+8+7, i+8+6, i+6, flip);
			indices.add_rectangle(i+6, i+8+6, i+8+5, i+5, flip);

			return ret;
		}

		uint infrontsided(ArrayBuffer<glm::vec3>& vertices,
					  ElementBuffer& indices,
					  const dooredWall& door,
					  bool flip)
		{
			uint i   = vertices.size();
			uint ret = v(vertices, door);
		
			indices.add_rectangle(i+8+0, i+8+4, i+8+7, i+8+3, !flip);
			indices.add_rectangle(i+8+7, i+8+6, i+8+2, i+8+3, !flip);
			indices.add_rectangle(i+8+6, i+8+5, i+8+1, i+8+2, !flip);

			indices.add_rectangle(i+4, i+8+4, i+8+7, i+7, flip);
			indices.add_rectangle(i+7, i+8+7, i+8+6, i+6, flip);
			indices.add_rectangle(i+6, i+8+6, i+8+5, i+5, flip);

			return ret;
		}

		uint frontbridged(ArrayBuffer<glm::vec3>& vertices,
					  ElementBuffer& indices,
					  const dooredWall& door,
					  bool flip)
		{
			uint i   = vertices.size();
			uint ret = v(vertices, door);
		
			indices.add_rectangle(i+8+0, i+8+4, i+8+7, i+8+3, flip);
			indices.add_rectangle(i+8+7, i+8+6, i+8+2, i+8+3, flip);
			indices.add_rectangle(i+8+6, i+8+5, i+8+1, i+8+2, flip);

			indices.add_rectangle(i+4, i+8+4, i+8+7, i+7, !flip);
			indices.add_rectangle(i+7, i+8+7, i+8+6, i+6, !flip);
			indices.add_rectangle(i+6, i+8+6, i+8+5, i+5, !flip);

			indices.add_rectangle(i+0, i+8+0, i+8+3, i+3, flip);
			indices.add_rectangle(i+3, i+8+3, i+8+2, i+2, flip);
			indices.add_rectangle(i+2, i+8+2, i+8+1, i+1, flip);

			return ret;
		}
	};
};