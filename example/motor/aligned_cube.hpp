#ifndef _ALIGNED_CUBE_HPP_
#define _ALIGNED_CUBE_HPP_

#include "../../ssl/ssl.hpp"

using namespace ssl;

class AlignedCube : public ssl::ElementDrawableObject
{
public:
	AlignedCube(const glm::vec3& position,
				const glm::vec3& size);

	AlignedCube(const glm::vec3& position,
				float size);

	virtual const ArrayBuffer<glm::vec3>&          get_points()   const;
	virtual const std::vector<const BasicBuffer*>& get_buffers()  const;
	virtual const ElementBuffer&                   get_elements() const;

	virtual ~AlignedCube(){}

private:
	glm::vec3 _position;
	glm::vec3 _size;

protected:
	ArrayBuffer<glm::vec3>          _points;
	std::vector<const BasicBuffer*> _buffers;

	ElementBuffer _indices;

};

#endif //_ALIGNED_CUBE_HPP_
