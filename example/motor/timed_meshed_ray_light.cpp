#include "timed_meshed_ray_light.hpp"

TimedStaticMeshedRayLight::TimedStaticMeshedRayLight(const std::vector<glm::vec3>& vertices,
												     const glm::vec3& color,
						      						 const std::vector<uint>& indices,
							  						 float duration)
	: TimedStaticMeshedRayLight(
			vertices, 
			std::vector<glm::vec3>(vertices.size(), color),
			indices,
			duration)
{
	
}

TimedStaticMeshedRayLight::TimedStaticMeshedRayLight(const std::vector<glm::vec3>& vertices,
												     const std::vector<glm::vec3>& colors,
						      						 const std::vector<uint>& indices,
							  						 float duration)
	: StoredMeshedRayLight(),
	  _original_colors(colors),
	  _vertices(),
	  _elements(indices),
	  _time_left(duration)
{
	if(_original_colors.size() != vertices.size())
	{
		err("TimedStaticMeshedRayLight::TimedStaticMeshedRayLight(): colors.size() != vertices.size()");
	}

	for(uint i=0; i<vertices.size(); i++)
	{
		_vertices.push_back({vertices[i], _original_colors[i]});
	}
}

void TimedStaticMeshedRayLight::animate(float dt)
{
	_time_left -= dt;

	const auto& intensity = this->get_intensity_evolution();

	for(uint i=0; i<_vertices.size(); i++)
	{
		_vertices[i].color = _original_colors[i] * intensity.evaluate(intensity.total_animation_time()-_time_left);
	}
}

bool TimedStaticMeshedRayLight::has_expired() const
{
	return (_time_left < 0.0f);
}

const std::vector<BasicMeshedRayLight::rayVertex>& TimedStaticMeshedRayLight::get_vertices() const
{
	return _vertices;
}

const std::vector<uint>&      TimedStaticMeshedRayLight::get_elements() const
{
	return _elements;
}

