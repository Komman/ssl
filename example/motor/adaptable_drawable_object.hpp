#ifndef _ADAPTABLE_DRAWABLE_OBJECT_HPP_
#define _ADAPTABLE_DRAWABLE_OBJECT_HPP_

#include "../../ssl/ssl.hpp"

using namespace ssl;

class AdaptableDrawableObject : public DrawableObject
{
public:
	AdaptableDrawableObject(const std::vector<const BasicBuffer*>& buffers);
	template<typename ... Buffers>
	AdaptableDrawableObject(const Buffers*... buffers);

	void change_buffers(const std::vector<const BasicBuffer*>& buffers);

	const std::vector<const BasicBuffer*>& get_buffers() const;

private:
	std::vector<const BasicBuffer*> _buffers;

};

class AdaptableElementDrawableObject : public ElementDrawableObject
{
public:
	AdaptableElementDrawableObject(const ElementBuffer* elements,
								   const std::vector<const BasicBuffer*>& buffers);
	template<typename ... Buffers>
	AdaptableElementDrawableObject(const ElementBuffer* elements, const Buffers*... buffers);

	void change_buffers(const std::vector<const BasicBuffer*>& buffers);
	void change_elements(const ElementBuffer* elements);

	const std::vector<const BasicBuffer*>& get_buffers()  const;
	const ElementBuffer&                   get_elements() const;
	
private:
	const ElementBuffer* _elements;
	std::vector<const BasicBuffer*> _buffers;

};







template<typename... Buffers>
AdaptableDrawableObject::AdaptableDrawableObject(const Buffers*... buffers)
	: AdaptableDrawableObject(std::vector<const BasicBuffer*>(0))
{
	_buffers.clear();
	_buffers.reserve(sizeof...(buffers));
	((_buffers.push_back(buffers)), ...);
}

template<typename... Buffers>
AdaptableElementDrawableObject::AdaptableElementDrawableObject(const ElementBuffer* elements, const Buffers*... buffers)
	: AdaptableElementDrawableObject(elements, {})
{
	_buffers.clear();
	_buffers.reserve(sizeof...(buffers));
	((_buffers.push_back(buffers)), ...);
}

#endif //_ADAPTABLE_DRAWABLE_OBJECT_HPP_
