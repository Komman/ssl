#include "game_drawer.hpp"

GameDrawer::GameDrawer(PostProcessor& post)
	: _postproc(post),
	  _drawcalls(),
	  _recorder(),
	  _current_task_name(NO_TASK_NAME),
	  _recording(false)
{

}

void GameDrawer::clear_framebuffer(gameDrawTypes drawing_type, GLbitfield mask)
{
	_drawcalls[drawing_type].order.push_back({CLEAR, _drawcalls[drawing_type].clears.size()});
	_drawcalls[drawing_type].clears.push_back(mask);
}


void GameDrawer::typed_draw(gameDrawTypes type)
{
	for(auto& o : _drawcalls[type].order)
	{
		if(o.first == OBJ_DRAW)
		{
			auto& draw = _drawcalls[type].objects_draws[o.second];
			bool cond = _recording && (draw.name != NO_TASK_NAME);
			if(cond) _recorder.gl_start(draw.name);

			auto tmp_params = draw.drawer->get_draws_params();
			draw.drawer->set_draws_params(draw.params);
			draw.drawer->draw_object(this->get_typed_frame(type), draw.object);
			draw.drawer->set_draws_params(tmp_params);

			if(cond) _recorder.gl_stop(draw.name);
		}
		else if(o.first == ELEMENT_OBJ_DRAW)
		{
			auto& draw = _drawcalls[type].element_objects_draws[o.second];
			bool cond = _recording && (draw.name != NO_TASK_NAME);
			if(cond) _recorder.gl_start(draw.name);
			
			auto tmp_params = draw.drawer->get_draws_params();
			draw.drawer->set_draws_params(draw.params);
			draw.drawer->draw_object(this->get_typed_frame(type), draw.object);
			draw.drawer->set_draws_params(tmp_params);
			

			if(cond) _recorder.gl_stop(draw.name);
		}
		else if(o.first == VAO_OBJ_DRAW)
		{
			auto& draw = _drawcalls[type].vao_draws[o.second];
			bool cond = _recording && (draw.name != NO_TASK_NAME);
			if(cond) _recorder.gl_start(draw.name);
			
			auto tmp_params = draw.drawer->get_draws_params();
			draw.drawer->set_draws_params(draw.params);
			draw.drawer->draw(this->get_typed_frame(type), *(draw.object));
			draw.drawer->set_draws_params(tmp_params);
			

			if(cond) _recorder.gl_stop(draw.name);
		}
		else if(o.first == CLEAR)
		{
			this->get_typed_frame(type).clear(_drawcalls[type].clears[o.second]);
		}
	}

	this->abort_typed_draws(type);
}

void GameDrawer::global_draw()
{
	this->typed_draw(MS_DRAW);
	this->typed_draw(BLOOM_MS_DRAW);
	this->typed_draw(BLOOM_MS_ONLY_DRAW);

	this->draw_multisampled();	

	this->typed_draw(NORMAL_DRAW);
	this->typed_draw(BLOOM_DRAW);
	this->typed_draw(BLOOM_ONLY_DRAW);
	this->typed_draw(BLENDING_DRAW);
	this->typed_draw(BLOOMED_BLENDING_DRAW);

	this->post_processing(); // Calls to ADDITIF_DRAW
}

void GameDrawer::draw_multisampled()
{
	// if(_recording) _recorder.gl_start("DrawMS");
	_postproc.draw_multisampled();
	// if(_recording) _recorder.gl_stop("DrawMS");
}

void GameDrawer::post_processing()
{
	if(_recording) _recorder.gl_start("Post");

	this->apply_bloom();

	this->typed_draw(ADDITIF_DRAW);

	this->screen_pass();

	if(_recording) _recorder.gl_stop("Post");
}

void GameDrawer::apply_bloom()
{
	if(_recording) _recorder.gl_start("Post");
	
	_postproc.apply_bloom();
	
	if(_recording) _recorder.gl_stop("Post");
}

void GameDrawer::screen_pass()
{
	if(_recording) _recorder.gl_start("Post");
	
	_postproc.screen_pass();
	
	if(_recording) _recorder.gl_stop("Post");
}

void GameDrawer::draw_typed(gameDrawTypes type, Drawer& drawer, const VertexArrayObject& vao)
{
	vaoDraw d = {
		&drawer,
		drawer.get_draws_params(),
		_current_task_name,
		&vao
	};

	auto& used_drawcall = _drawcalls[type].vao_draws;

	_drawcalls[type].order.push_back({VAO_OBJ_DRAW, used_drawcall.size()});
	used_drawcall.push_back(std::move(d));
}

void GameDrawer::draw_MS(Drawer& drawer, const VertexArrayObject& vao)
{
	this->draw_typed(MS_DRAW, drawer, vao);
}
void GameDrawer::draw_bloomed_MS(Drawer& drawer, const VertexArrayObject& vao)
{
	this->draw_typed(BLOOM_MS_DRAW, drawer, vao);
}
void GameDrawer::draw_normal_unsafe(Drawer& drawer, const VertexArrayObject& vao)
{
	this->draw_typed(NORMAL_DRAW, drawer, vao);
}
void GameDrawer::draw_bloomed(Drawer& drawer, const VertexArrayObject& vao)
{
	this->draw_typed(BLOOM_DRAW, drawer, vao);
}
void GameDrawer::draw_blend(Drawer& drawer, const VertexArrayObject& vao)
{
	this->draw_typed(BLENDING_DRAW, drawer, vao);
}
void GameDrawer::draw_bloomed_blend(Drawer& drawer, const VertexArrayObject& vao)
{
	this->draw_typed(BLOOMED_BLENDING_DRAW, drawer, vao);
}
void GameDrawer::draw_bloomed_MS_only(Drawer& drawer, const VertexArrayObject& vao)
{
	this->draw_typed(BLOOM_MS_ONLY_DRAW, drawer, vao);
}
void GameDrawer::draw_bloomed_only(Drawer& drawer, const VertexArrayObject& vao)
{
	this->draw_typed(BLOOM_ONLY_DRAW, drawer, vao);
}
void GameDrawer::draw_additif(Drawer& drawer, const VertexArrayObject& vao)
{
	this->draw_typed(ADDITIF_DRAW, drawer, vao);
}


void GameDrawer::abort_draws()
{
	this->abort_typed_draws(MS_DRAW);
	this->abort_typed_draws(BLOOM_MS_DRAW);
	this->abort_typed_draws(BLOOM_MS_ONLY_DRAW);
	this->abort_typed_draws(NORMAL_DRAW);
	this->abort_typed_draws(BLOOM_DRAW);
	this->abort_typed_draws(BLOOM_ONLY_DRAW);
	this->abort_typed_draws(BLENDING_DRAW);
	this->abort_typed_draws(BLOOMED_BLENDING_DRAW);
	this->abort_typed_draws(ADDITIF_DRAW);
}

void GameDrawer::abort_typed_draws(gameDrawTypes type)
{
	_drawcalls[type].order.clear();
	_drawcalls[type].objects_draws.clear();
	_drawcalls[type].element_objects_draws.clear();
	_drawcalls[type].clears.clear();
}


PostProcessor& GameDrawer::get_post_processor() const
{
	return _postproc;
}

BasicFrameBuffer& GameDrawer::get_typed_frame(gameDrawTypes type)
{
	if(type == MS_DRAW)
		return _postproc.frameMS();

	if(type == BLOOM_MS_DRAW)
		return _postproc.frame_and_bloomMS();

	if(type == NORMAL_DRAW)
		return _postproc.frame();

	if(type == BLOOM_DRAW)
		return _postproc.frame_and_bloom();

	if(type == BLENDING_DRAW)
		return _postproc.blending_frame();

	if(type == BLOOMED_BLENDING_DRAW)
		return _postproc.bloomed_blending_frame();

	if(type == BLOOM_MS_ONLY_DRAW)
		return _postproc.bloom_frameMS();

	if(type == BLOOM_ONLY_DRAW)
		return _postproc.bloom_frame();

	if(type == ADDITIF_DRAW)
		return _postproc.bloom_frame();

	err("GameDrawer::get_typed_frame(): unvalid type value");
	return _postproc.frameMS();
}

void GameDrawer::clear_frames()
{
	if(_recording) _recorder.gl_start("Clearing");
	_postproc.clear();
	if(_recording) _recorder.gl_stop("Clearing");
}

void GameDrawer::record_start(const std::string& task)
{
	_current_task_name = task;
}

void GameDrawer::record_stop()
{
	_current_task_name = NO_TASK_NAME;
}

void GameDrawer::record_enable()
{
	_recording = true;
}

void GameDrawer::record_disable()
{
	_recording = false;
}

void GameDrawer::record_print()
{
	_recorder.print();
	_recorder.reset_durations();
}

LabeledRecorder& GameDrawer::recorder()
{
	return _recorder;
}

bool GameDrawer::recording() const
{
	return _recording;
}
