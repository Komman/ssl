#ifndef _HBX_TRANSFORMATION_HPP_
#define _HBX_TRANSFORMATION_HPP_

#include <string>
#include <glm/glm.hpp>
#include "../utils/geom.hpp"

struct hbxTransformation
{
	glm::vec3 translation = glm::vec3(0);
	glm::mat3 rotation    = glm::mat3(1);
	float     scale       = 1.0f;

	

	hbxTransformation operator+(const hbxTransformation& transformation) const;
	bool operator!=(const hbxTransformation& transformation) const;
	bool operator==(const hbxTransformation& transformation) const;
	
	glm::vec3 transform(const glm::vec3& point)    const;
	spaceBox  transform(const spaceBox& space_box) const;

	std::string to_string() const;
};


#endif //_HBX_TRANSFORMATION_HPP_
