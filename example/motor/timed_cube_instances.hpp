#ifndef _TIMED_CUBE_INSTANCES_HPP_
#define _TIMED_CUBE_INSTANCES_HPP_

#include "../motor/aligned_cube.hpp"
#include "../motor/stored_vao_object.hpp"

struct timedCubeInstance
{
	glm::vec3 position;;
	glm::vec3 color;
	float size;
	float duration;
};

class TimedCubeInstances : public AlignedCube
{
public:
	TimedCubeInstances(const std::vector<timedCubeInstance>& instances,
			   		   const std::string& name);

	uint instances_amount() const;

	void animate(float dt);
	void add_cube(const timedCubeInstance& cube);

	virtual VertexArrayObject compute_vao() const override;

protected:
	static glm::vec3 merge_size_time_duration(float size, float start_time, float duration);

private:
	void check_sizes() const;
	bool has_expired(uint index) const;
	void swapop(uint index);

private:
	ArrayBuffer<glm::vec3> _positions;
	ArrayBuffer<glm::vec3> _colors;
	ArrayBuffer<glm::vec3> _size_U_start_time_U_duration;

	Uniform<float> _time;
};

#endif //_TIMED_CUBE_INSTANCES_HPP_
