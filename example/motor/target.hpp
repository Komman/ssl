#ifndef _TARGET_HPP_
#define _TARGET_HPP_

#include <glm/glm.hpp>

class BasicTarget
{
public:
	virtual glm::vec3 position() const =0;
	virtual glm::vec3 speed()    const =0;
};


class PointerTarget : public BasicTarget
{
public:
	PointerTarget(const glm::vec3* position_ptr, const glm::vec3* speed_ptr);

	void set_position_ptr(const glm::vec3* position_ptr);
	void set_speed_ptr(const glm::vec3* speed_ptr);

	glm::vec3 position() const override;
	glm::vec3 speed()    const override;

private:
	const glm::vec3* _position;
	const glm::vec3* _speed;
};

#endif //_TARGET_HPP_
