#ifndef _REGULAR_SIGNALER_HPP_
#define _REGULAR_SIGNALER_HPP_

using uint = unsigned int;

class RegularSignaler
{
public:
	RegularSignaler(float frequency);

	void  change_frequency(float frequency);
	float get_frequency();
	
	// Returns the number of signals given during dt
	uint animate(float dt);

private:
	float _frequency;
	float _modular_time;
};

#endif //_REGULAR_SIGNALER_HPP_
