#include "planar_building.hpp"

#include "../utils/mutils.hpp"

circleInfo tranform_relative_2D(const circleInfo& circle, const planarPlace& relative)
{
	circleInfo ret;

	ret.center = relative.center + geom::rotate_point2D(circle.center*relative.size, geom::orthogonal(relative.dirN));
	ret.radius = circle.radius*relative.size.x;

	return ret;
}

polygon2DInfo tranform_relative_2D(const polygon2DInfo& polygon, const planarPlace& relative)
{
	polygon2DInfo ret;

	ret.inside = relative.center + geom::rotate_point2D(polygon.inside * relative.size, geom::orthogonal(relative.dirN));
	
	for(const auto& p : polygon.points)
	{
		ret.points.push_back(relative.center + geom::rotate_point2D(p * relative.size, geom::orthogonal(relative.dirN)));
	}

	return ret;
}


PlanarBuilding::PlanarBuilding(const std::vector<valuedCircleInfo>& circles, const std::vector<valuedPolygon2DInfo>& polygons)
	: _circles(circles),
	  _polygons(polygons)
{

}

void PlanarBuilding::add_circles(const std::vector<valuedCircleInfo>& circles)
{
	utils::push_vector(_circles, circles);
}

void PlanarBuilding::add_polygon(const std::vector<valuedPolygon2DInfo>& polygons)
{
	utils::push_vector(_polygons, polygons);
}

bool PlanarBuilding::is_placable(SpaceGrid& spacegrid, const planarPlace& p)
{
	for(const auto& c : _circles)
	{
		auto newcircle = tranform_relative_2D(c.circle, p);

		if(spacegrid.collision_circle(newcircle.center, newcircle.radius))
		{
			return false;
		}
	}

	for(const auto& poly : _polygons)
	{
		auto newpoly = tranform_relative_2D(poly.polygon, p);
		
		if(spacegrid.collision_filled_polygon(newpoly.points, newpoly.inside, SpaceGrid::RESERVED_VALUE))
		{
			return false;
		}
	}

	return true;
}

void PlanarBuilding::place(SpaceGrid& spacegrid, const planarPlace& p)
{
	for(const auto& c : _circles)
	{
		auto newcircle = tranform_relative_2D(c.circle, p);

		spacegrid.set_circle(newcircle.center, newcircle.radius, c.value);
	}

	for(const auto& poly : _polygons)
	{
		auto newpoly = tranform_relative_2D(poly.polygon, p);

		spacegrid.set_filled_polygon(newpoly.points, newpoly.inside, poly.value);
	}
}

