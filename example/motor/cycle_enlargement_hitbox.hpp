#ifndef _CYCLE_ENLARGEMENT_HITBOX_HPP_
#define _CYCLE_ENLARGEMENT_HITBOX_HPP_

#include "shaped_hitbox_fusion.hpp"
#include "bevel_hitbox.hpp"

struct cycleEnlargement
{
	// cycle MUST be on a y-plan
	std::vector<glm::vec3> cycle;
	glm::vec3 translation;	
	float enlargement;
};

class CycleEnlargementHitbox : public ShapedHitboxFusion<cycleEnlargement>
{
public:
	CycleEnlargementHitbox(const cycleEnlargement& shape);

private:
	void build();
};

#endif //_CYCLE_ENLARGEMENT_HITBOX_HPP_
