#include "joystick2D.hpp"


#include <iostream>

Joystick2D::Joystick2D(const BasicMousekeyTarget* mousekey, int keys[JOYSTICK_NUMBER_CONTROLS], const ssl::View* view)
	: BasicJoystick(mousekey),
	  _view(view),
	  _control_keys()
{
	for(uint i=0; i<JOYSTICK_NUMBER_CONTROLS; i++)
	{
		_control_keys.push_back(keys[i]);
	}
}

glm::vec3 Joystick2D::compute_directionN() const
{
	std::vector<joystickKeysIndices> pressed_keys_indices;

	for(unsigned int i=0; i<_control_keys.size(); i++)
	{
		if(this->key_pressed(_control_keys[i]))
		{
			pressed_keys_indices.push_back((joystickKeysIndices)i);
		}
	}

	return this->directionN_from_pressed(pressed_keys_indices);
}

jumpSneakAction Joystick2D::get_jump_sneak() const
{
	return {
		this->key_pressed(_control_keys[JOYSTICK_CONTROL_UP]),
		this->key_pressed(_control_keys[JOYSTICK_CONTROL_DOWN])
	};
}


glm::vec3 Joystick2D::directionN_from_pressed(const std::vector<joystickKeysIndices>& pressed) const
{
	glm::vec3 direction = glm::vec3(0.0, 0.0, 0.0);

	//TOOPT (0) (with function pointer)
	for(joystickKeysIndices k : pressed)
	{
		if(k == JOYSTICK_CONTROL_RIGHT)
		{
			direction+= _view->get_right_vector();
		}
		if(k == JOYSTICK_CONTROL_LEFT)
		{
			direction-= _view->get_right_vector();
		}
		if(k == JOYSTICK_CONTROL_FRONT)
		{
			direction+= glm::cross(_view->get_up_head(), _view->get_right_vector());
		}
		if(k == JOYSTICK_CONTROL_BACK)
		{
			direction-= glm::cross(_view->get_up_head(), _view->get_right_vector());
		}
	}

	float dirl = glm::length(direction);

	if(dirl > 0.0001f)
	{
		return direction/dirl;
	}
	else
	{
		return glm::vec3(0);
	}
}
