#include "crown_hitbox.hpp"
#include "../utils/geom.hpp"

CrownHitbox::CrownHitbox(const crownInfo& shape, uint side_faces)
	: ShapedHitboxFusion(shape),
	  _side_faces(side_faces)
{
	this->build();
}

void CrownHitbox::build()
{
	const crownInfo& crown = this->get_shape();

	glm::vec3 rot_axisN = glm::cross(crown.base_orientationN, crown.to_top_center);
	rot_axisN = glm::normalize(glm::cross(crown.base_orientationN, rot_axisN));

	std::vector<glm::vec3> base_points_in  = geom::circle_points(
		crown.bottom_center,
		crown.base_orientationN,
		rot_axisN,
		crown.radius_start,
		_side_faces
	);
	std::vector<glm::vec3> base_points_out = geom::circle_points(
		crown.bottom_center,
		crown.base_orientationN,
		rot_axisN,
		crown.radius_end,
		_side_faces
	);

	for(uint i=0; i<_side_faces; i++)
	{
		glm::vec3 dri1N = glm::normalize(base_points_out[i] - base_points_in[i]);
		glm::vec3 dri2N = glm::normalize(base_points_out[(i+1)%_side_faces] - base_points_in[(i+1)%_side_faces]);
		glm::vec3 dirN  = glm::normalize(dri1N + dri2N);

		this->fusion_hitbox<PaverHitbox>(paverInfo{
			.bottom  = base_points_out[i],
			.sized_x = -dirN*(crown.radius_end - crown.radius_start)*float(cos(float(M_PI)/float(_side_faces))),
			.sized_y = crown.to_top_center,
			.sized_z = base_points_out[(i+1)%_side_faces] - base_points_out[i]
		});
	}
}
