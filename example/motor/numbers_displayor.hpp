#ifndef _NUMBERS_DISPLAYOR_HPP_
#define _NUMBERS_DISPLAYOR_HPP_

#include <memory>

#include "../../ssl/ssl.hpp"

using namespace ssl;

class FloatNumbersDisplayor
{
public:
	/*
	     /!\ NOT THREAD SAFE  /!\  
	*/
	FloatNumbersDisplayor(float value, const glm::vec2& location, const glm::vec2& size);

	void  set_value(float value);
	float get_value();

	// Location and size are between (0,0) and (1,1)
	const glm::vec2& location() const;
	const glm::vec2& size()     const;
	const glm::vec3& color()    const;

	void change_location(const glm::vec2& location);
	void change_size(const glm::vec2& size);
	void change_color(const glm::vec3& color);
	void change_digit_amount(const glm::ivec2& digit_amount);

	// Or NULL for the main framebuffer
	void draw(FrameBuffer<glm::vec4>* framebuffer);

public:
	static constexpr int MAX_DIGIT = 5;

protected:
	// -1: nothing, and else the numbers between 0 and 9
	static std::unique_ptr<UniformArray<int>>  _uniform_digits;
	static std::unique_ptr<Uniform<glm::vec2>> _uniform_location;
	static std::unique_ptr<Uniform<glm::vec2>> _uniform_size;
	static std::unique_ptr<Uniform<glm::vec3>> _uniform_color;
	
	static std::unique_ptr<Uniform<glm::ivec2>> _uniform_digit_amount; // pre dot and post dot

	static std::unique_ptr<ShaderGenerator> _shader_gen;
	static std::unique_ptr<Drawer> _drawer;

	static void init_statics();

private:
	void check_digit_amount();
	void compute_digits();

private:
	float _value;

	glm::vec2  _location;
	glm::vec2  _size;
	glm::vec3  _color;
	glm::ivec2 _digit_amount;;
};

#endif //_NUMBERS_DISPLAYOR_HPP_
