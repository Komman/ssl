#ifndef _SQUARED_BIBOLARS_HPP_
#define _SQUARED_BIBOLARS_HPP_

#include "timed_instances.hpp"


struct squareBibolar
{
	glm::vec3 center;
	glm::vec2 size;
	glm::vec3 color;
	float duration;
};

namespace squareBibolarsBuffers
{
	enum buffersOrder {RELATIVE_POS=0, CENTER, COLOR, SIZE_U_STIME_U_DURATION};
};

/*
	vec2: relative positions
	vec3: center
	vec3: color
	vec4: .xy : size
		  .z  : start  time
		  .w  : duration 
*/
class SquaredBibolars : public TimedInstances<squareBibolar,
											  squareBibolarsBuffers::CENTER,
											  glm::vec2,
											  glm::vec3,
											  glm::vec3,
											  glm::vec4>
{
public:
	SquaredBibolars(const std::string& name);
	SquaredBibolars(const std::string& name, const std::vector<squareBibolar>& initial_instances);
	virtual ~SquaredBibolars() {}

	void print() const;

protected:
	void add_retarded_instance(const squareBibolar& bib, float retard);
	void add_instance_in_buffers(const squareBibolar& bib) override;
	float get_instance_start_time(uint instance_index) const override;
	float get_instance_duration(uint instance_index)   const override;

};

#endif //_SQUARED_BIBOLARS_HPP_
