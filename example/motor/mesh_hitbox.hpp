#ifndef _MESH_HITBOX_HPP_
#define _MESH_HITBOX_HPP_

#include "shaped_hitbox_fusion.hpp"
#include "../../ssl/src/gl_buffer.hpp"

struct meshSurface
{
	const std::vector<glm::vec3>* mesh_points;
	const std::vector<uint>* elements;
	// first and the one after the last index of elements
	// (for a full part, it is {0, size})
	std::pair<uint, uint> part;
	float width;
};

class MeshHitobox : public ShapedHitboxFusion<meshSurface>
{
public:
	// every triangle with a side of less than
	// min_radius is ignored (no hitbx on it)
	MeshHitobox(const meshSurface& shape, float min_radius);

private:
	void build();

private:
	float _min_radius;
};

#endif //_MESH_HITBOX_HPP_
