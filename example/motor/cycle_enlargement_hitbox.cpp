#include "cycle_enlargement_hitbox.hpp"


#include "../../ssl/src/utils.hpp"
#include "../utils/mutils.hpp"

using namespace glm;

CycleEnlargementHitbox::CycleEnlargementHitbox(const cycleEnlargement& shape)
	: ShapedHitboxFusion(shape)
{
	this->build();
}

void CycleEnlargementHitbox::build()
{
	const auto& geocylinder = this->get_shape();
	const auto& cycle = geocylinder.cycle;

	const auto& cycle2 = geom::conspace_enlarge_cycle(
		geocylinder.cycle,
		vec3(0),
		geocylinder.enlargement,
		abs(geocylinder.enlargement)
	);

	if(cycle.size() < 3 || cycle2.size() < 3)
	{
		mot::err("CycleEnlargementHitbox::build(): cycle of size");
	}

	std::vector<glm::vec3> points = cycle;
	points.insert(points.end(), cycle2.begin(), cycle2.end());

	std::vector<triIndex> triangles;

	if(geocylinder.enlargement > 0.0f)
	{
		triangles = geom::rubban_triangulation(
			points,
			utils::uirange(0, cycle.size()-1),
			utils::uirange(cycle.size(), points.size()-1)
		);
	}
	else
	{
		triangles = geom::rubban_triangulation(
			points,
			utils::uirange(cycle.size(), points.size()-1),
			utils::uirange(0, cycle.size()-1)
		);
	}

	for(uint i=0; i<triangles.size(); i++)
	{
		this->fusion_hitbox<BevelHitbox>(bevelShape{
			.triangle_base   = {
				points[triangles[i].i1],
				points[triangles[i].i2],
				points[triangles[i].i3]
		   	},
		   	.sized_direction = geocylinder.translation
		});
	}
}

