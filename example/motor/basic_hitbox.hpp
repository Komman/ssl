#ifndef _BASIC_HITBOX_HPP_
#define _BASIC_HITBOX_HPP_

#include <memory>
#include <glm/glm.hpp>

#include "../utils/geom.hpp"
#include "../utils/updatable_struct.hpp"

#include "motor_debug.hpp"
#include "hbx_transformation.hpp"

// #define HITBOX_DEBUG

namespace hitbox_primary
{
	enum Shape {SPHERE=0, PRISM, NO_PRIMARY};
};

template<hitbox_primary::Shape SHAPE_TYPE>
class PrimaryHitbox;

using SphereHitbox = PrimaryHitbox<hitbox_primary::SPHERE>;
using PrismHitbox  = PrimaryHitbox<hitbox_primary::PRISM>;

/*
	EVERY CLASS THAT INHERIT OF BasicHitbox MUST RESPECT
	THE CONDITION OF CALL TO relative_spacebox_invalidate()
	
	(more info in description of protected method)
*/

class BasicHitbox
{
public:
	using storedHitbox = std::unique_ptr<BasicHitbox>;

public:
	BasicHitbox(const glm::vec3& translation);
	BasicHitbox(const hbxTransformation& transformation);
	virtual ~BasicHitbox() {}

	void transform_relative(const hbxTransformation& transformation);
	void transform_absolute(const hbxTransformation& transformation);

	storedHitbox build_symmetrical(const infinitePlan& sym_plan) const;

	bool in_collision(const BasicHitbox& hbx)  const;
	bool in_collision(const SphereHitbox& hbx) const;
	bool in_collision(const PrismHitbox& hbx)  const;
	bool in_collision(const std::vector<storedHitbox>& hbxs)  const;

	// Using the speed as direction
	collisionImpact impact(const BasicHitbox& hitbox, float dt) const;
	
	collisionImpact impact(const BasicHitbox& hitbox , const glm::vec3& self_directionN, float distance) const;
	collisionImpact impact(const SphereHitbox& hitbox, const glm::vec3& self_directionN, float distance) const;
	collisionImpact impact(const PrismHitbox& hitbox , const glm::vec3& self_directionN, float distance) const;

	// Theses 3 methods are changing relative positions
	void move(const glm::vec3& dp);
	void tp(const glm::vec3& new_pos);
	void homothety(float coeff);
	void rotate(const glm::mat3& new_base);

	void set_speed(const glm::vec3& speed);
	const glm::vec3& get_speed() const;

	// Returns the space box with abslute coords
	const spaceBox& absolute_space_box() const;
	// Returns the space box relatively to the parent hbx
	spaceBox extern_space_box() const;
	// Returns the space box relatively to the parent hbx but without its transformation
	const spaceBox& relative_space_box() const;
	// Returns the transformation relatively to the parent hbx
	const hbxTransformation& transformation()     const;
	// Returns the absolute transformation
	const hbxTransformation& abs_transformation() const;

	/*
		Position by reference, it will be the point that will be tp,
		and the remaining of the hitbox will be moved relatively to this point
		this is a parent-relative position
	*/
	const glm::vec3& get_ref_pos() const;
	const glm::mat3& get_rotation() const;
	float get_homothety() const;
	std::string get_str() const;

	// Returns absolute_space_box()
	const spaceBox& get_minmax_coord() const ;

public:
	/*
		ONLY FOR DEBUG
	*/
	virtual const std::vector<storedHitbox>& subhitboxes() const;
	virtual hitbox_primary::Shape get_primary_type() const;

public:
	/*
		Thoses methods will likely be only used by
		classes that inherit of this one, and then
		delte tem
	*/

	// Notify that absolute coordinates/shape is no longer valid
	void invalidate_absolute() const;
	/*
		The entity that uses this methd has the responsability
		to call invalidate_absolute() on the son's hitbox when
		the parent hitbox changes.
	*/
	void attach_parent(const BasicHitbox* parent); 

public:
	virtual storedHitbox copy() const =0;	

protected:
	virtual spaceBox compute_shape_relative_space_box() const =0;
	
	virtual bool in_shape_collision(const BasicHitbox&  hbx) const =0;
	virtual bool in_shape_collision(const SphereHitbox& hbx) const =0;
	virtual bool in_shape_collision(const PrismHitbox&  hbx) const =0;

	virtual collisionImpact shape_impact(const BasicHitbox& hitbox , const glm::vec3& self_directionN, float distance) const =0;
	virtual collisionImpact shape_impact(const SphereHitbox& hitbox, const glm::vec3& self_directionN, float distance) const =0;
	virtual collisionImpact shape_impact(const PrismHitbox& hitbox , const glm::vec3& self_directionN, float distance) const =0;

	// Called when the absolute coordinates/shape is no longer valid
	// For example if the relative transformation has changed
	virtual void shape_invalidate_absolute() const =0;

	/*
		FOR EVERY CLASS THAT INHERIT OF THIS ONE:
		- This method MUST be called whenever 
		the hitbox modifies something that 
		changes the relative hitbox (for 
		example, a shape redefinition, or
		a subhitbox adding).
	*/
	void relative_spacebox_invalidate() const;


private:
	spaceBox          compute_absolute_space_box() const;
	hbxTransformation compute_absolute_transform() const;

	bool absolute_spacebox_overlap(const BasicHitbox& hbx) const;
	bool absolute_spacebox_impact(const BasicHitbox& hbx, const glm::vec3& self_directionN, float distance) const;

	void update_dt(float dt) const;

private:
	BasicHitbox const* _parent;

	// Absolute
	glm::vec3 _speed;
	
	// Relative
	hbxTransformation  _transformation;
	mutable datedStruct<spaceBox> _relative_space_box;
	mutable float _old_dt;

	// Absolute
	mutable datedStruct<spaceBox>          _abs_space_box;
	mutable datedStruct<hbxTransformation> _abs_transformation;
};

#endif //_BASIC_HITBOX_HPP_
