#include "timed_cube_instances.hpp"

#include "gutils.hpp"

using namespace glm;
using namespace std;


TimedCubeInstances::TimedCubeInstances(const std::vector<timedCubeInstance>& instances,
			          		 const std::string& name)
	: AlignedCube(vec3(0.0f), vec3(2.0f)),
	  _positions(STATIC_DRAW),
	  _colors(STATIC_DRAW),
	  _size_U_start_time_U_duration(STATIC_DRAW),
	  _time(name + "_time", 0.0f)

{
	for(const auto& ins : instances)
	{
		this->add_cube(ins);
	}

	_buffers.push_back(&_positions);
	_buffers.push_back(&_colors);
	_buffers.push_back(&_size_U_start_time_U_duration);
}

VertexArrayObject TimedCubeInstances::compute_vao() const
{
	return VertexArrayObject({
		{&_points, 0},
		{&_positions, 1},
		{&_colors, 1},
		{&_size_U_start_time_U_duration, 1}
	}, &(this->get_elements()));
}


glm::vec3 TimedCubeInstances::merge_size_time_duration(float size, float start_time, float duration)
{
	return glm::vec3(size, start_time, duration);
}


void TimedCubeInstances::add_cube(const timedCubeInstance& cube)
{
	_positions.add_value(cube.position);
	_colors.add_value(cube.color);
	_size_U_start_time_U_duration.add_value(merge_size_time_duration(cube.size, _time.get_value(), cube.duration));
}

void TimedCubeInstances::swapop(uint index)
{
	#ifdef SSL_DEBUG
	if(this->instances_amount() == 0)
	{
		ssl::err("TimedCubeInstances::swapop(): no instances to pop");
	}
	#endif

	utils::swapop(_positions, index);
	utils::swapop(_colors, index);
	utils::swapop(_size_U_start_time_U_duration, index);
}

bool TimedCubeInstances::has_expired(uint index) const
{
	#ifdef SSL_DEBUG
	if(index >= this->instances_amount())
	{
		ssl::err("TimedCubeInstances::has_expired(): index out of bound : "
			+ to_string(index) + "/" + to_string(this->instances_amount()));
	}
	#endif

	float start_time = _size_U_start_time_U_duration[index].y;
	float duration   = _size_U_start_time_U_duration[index].z;

	return (_time.get_value() - start_time >= duration);
}

void TimedCubeInstances::animate(float dt)
{
	_time += dt;

	for(uint i=0; i<this->instances_amount(); i++)
	{
		if(this->has_expired(i))
		{
			this->swapop(i);

			i--;
		}
	}

	if(this->instances_amount() == 0)
	{
		// To prevent time to be not precise enough if it is too big
		_time = 0.0f;
	}
}

void TimedCubeInstances::check_sizes() const
{
	if(    _positions.size() != _size_U_start_time_U_duration.size()
		|| _positions.size() != _colors.size())
	{
		ssl::err("TimedCubeInstances::check_sizes(): bad sizes");
	}
}


uint TimedCubeInstances::instances_amount() const
{
	#ifdef SSL_DEBUG
	this->check_sizes();
	#endif

	return _positions.size();
}


