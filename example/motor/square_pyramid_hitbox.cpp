#include "square_pyramid_hitbox.hpp"

SquarePyramidHitbox::SquarePyramidHitbox(const squarePyramidShape& shape)
	: HitboxFusion()
{
	prismInfo p1 = {.points = {
		shape.base[0],
		shape.base[1],
		shape.base[2],
		shape.top
	}};
	prismInfo p2 = {.points = {
		shape.base[0],
		shape.base[2],
		shape.base[3],
		shape.top
	}};

	this->fusion_hitbox<PrismHitbox>(p1);
	this->fusion_hitbox<PrismHitbox>(p2);
}

SquarePyramidHitbox::SquarePyramidHitbox(const alignedSquarePyramidShape& shape)
	: SquarePyramidHitbox(convert(shape))
{

}

squarePyramidShape SquarePyramidHitbox::convert(const alignedSquarePyramidShape& shape)
{
	squarePyramidShape ret;

	ret.base[0] = shape.base_center + ( + shape.dir_base_XN*shape.base_size_XY.x + shape.dir_base_YN*shape.base_size_XY.y)/2.0f;
	ret.base[1] = shape.base_center + ( + shape.dir_base_XN*shape.base_size_XY.x - shape.dir_base_YN*shape.base_size_XY.y)/2.0f;
	ret.base[2] = shape.base_center + ( - shape.dir_base_XN*shape.base_size_XY.x - shape.dir_base_YN*shape.base_size_XY.y)/2.0f;
	ret.base[3] = shape.base_center + ( - shape.dir_base_XN*shape.base_size_XY.x + shape.dir_base_YN*shape.base_size_XY.y)/2.0f;

	ret.top = shape.top;

	return ret;
}

