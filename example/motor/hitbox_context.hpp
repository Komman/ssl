#ifndef _HITBOX_CONTEXT_HPP_
#define _HITBOX_CONTEXT_HPP_

#include <vector>

#include "basic_hitbox.hpp"

struct contextImpact
{
	const BasicHitbox* hbx;
	collisionImpact    impact;
};

struct staticHitboxDisc
{
	spaceBox range;
	uint array_size;
};

class HitboxContext
{
public:
	HitboxContext(const staticHitboxDisc& static_range);

	bool in_collision(const BasicHitbox& hbx) const;
	contextImpact impact(const BasicHitbox& hbx, float dt) const;
	contextImpact impact(const BasicHitbox& hbx, const glm::vec3& self_directionN, float distance) const;

	// The hitbox ptr MUST be allocated while the HitboxContext is alive
	void add_hitbox(const BasicHitbox* hitbox_ptr);
	void add_hitboxes(const std::vector<BasicHitbox::storedHitbox>& hitboxes);
	
	// hitboxes that never moves
	void add_static_hitbox(const BasicHitbox* hitbox_ptr);
	void add_static_hitboxes(const std::vector<BasicHitbox::storedHitbox>& hitboxes);

	std::vector<const BasicHitbox*> close_static_hitboxes(const BasicHitbox& hbx) const;
	std::vector<const BasicHitbox*> close_static_hitboxes(const BasicHitbox& hbx, float dt) const;
	std::vector<const BasicHitbox*> close_static_hitboxes(const BasicHitbox& hbx, const glm::vec3& self_directionN, float distance) const;

private:
	glm::uvec2 map_coord(const glm::vec3& pos) const;
	bool inmap(const glm::vec3& pos) const;

	std::vector<glm::uvec2> submap(const spaceBox& box) const;
	std::vector<const BasicHitbox*> subhbxs(const std::vector<glm::uvec2>& sub) const;

private:
	std::vector<const BasicHitbox*>  _hitboxes;
	std::vector<const BasicHitbox*>  _static_hitboxes;

	staticHitboxDisc _static_range;
	std::vector<std::vector<std::vector<const BasicHitbox*>>> _hbxs_array;
};

#endif //_HITBOX_CONTEXT_HPP_
