#ifndef _SPECIAL_SHAPES_HPP_
#define _SPECIAL_SHAPES_HPP_

#include "../utils/shapes.hpp"

struct dooredWall
{
	glm::vec3 bot1;
	glm::vec3 bot2;
	glm::vec3 diryN;
	glm::vec3 inside_dir1N;
	glm::vec3 inside_dir2N;

	float thickness;
	float height;
	float door_xpos; //0 to 1, from bot1 to bot2
	float door_size;
	float door_height;

	glm::vec3 inside_door_bot1() const;
	glm::vec3 inside_door_bot2() const;
	glm::vec3 inside_dirN() const;
	glm::vec3 door_bot1() const;
	glm::vec3 door_bot2() const;
	glm::vec3 sized_x() const;
	glm::vec3 dirxN() const;
	bool buildable() const;
};

#endif //_SPECIAL_SHAPES_HPP_
