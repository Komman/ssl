#include "cage_delim.hpp"


using namespace glm;

CageDelim::CageDelim(float thickness, const glm::vec3& color)
	: StoredInstancesBuffers(STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, DYNAMIC_DRAW, DYNAMIC_DRAW),
	  _thickness(thickness),
	  _color(color)
{
	this->build_base();
}

void CageDelim::add_cage(const spaceBox& box)
{
	this->get_array_buffer<MINCORNER>().add_value(box.min);
	this->get_array_buffer<SIZE>().add_value(box.max - box.min);
}

void CageDelim::clear()
{
	this->get_array_buffer<MINCORNER>().clear();
	this->get_array_buffer<SIZE>().clear();
}


uint CageDelim::place_vertex(AngularSymmetry<glm::vec3>& asym, const glm::vec3& pos, const glm::vec3& color)
{
	uint s = asym.place_symmetrical(pos, color);
	asym.place_symmetrical(vec3(pos.x, 1.0f-pos.y, pos.z), color);

	return s;
}

uint CageDelim::upsym(uint index)
{
	return index + 4;
}

/*
    C-----Cr
   /      /|
  /      / |
 D-----Dr  Br
 |      | /
 |      |/ 
 A-----Ar
*/

void CageDelim::build_base()
{
	auto& relpos = this->get_array_buffer<RELATIVE_POS>();
	auto& colors = this->get_array_buffer<COLORS>();

	AngularSymmetry<glm::vec3> sym(
		{.axe_point = vec3(0.5), .axe_directionN = vvv::y(), .divisor = 4},
		&relpos,
		&(this->get_elements()),
		&colors
	);

	float extcoeff = 0.7f;

	uint A  = this->place_vertex(sym, vec3(0), _color);
	uint B  = this->place_vertex(sym, vec3(0, 0, _thickness), _color*extcoeff);
	uint C  = this->place_vertex(sym, vec3(0, _thickness, _thickness), _color*extcoeff);
	uint D  = this->place_vertex(sym, vec3(0, _thickness, 0), _color);
	uint Br = this->place_vertex(sym, relpos[B] + vvv::x()*_thickness, _color*extcoeff);
	uint Cr = this->place_vertex(sym, relpos[C] + vvv::x()*_thickness, _color*0.5f);
	uint Dr = this->place_vertex(sym, relpos[D] + vvv::x()*_thickness, _color*extcoeff);

	sym.link_rectangle(D, sym.symmetrical(1, D), sym.symmetrical(1, A), A, true);
	sym.link_rectangle(upsym(D), sym.symmetrical(1, upsym(D)), sym.symmetrical(1, upsym(A)), upsym(A));
	sym.link_rectangle(A, sym.symmetrical(1, A), sym.symmetrical(1, Br), Br, true);
	sym.link_rectangle(upsym(A), sym.symmetrical(1, upsym(A)), sym.symmetrical(1, upsym(Br)), upsym(Br), false);
	sym.link_rectangle(D, sym.symmetrical(1, D), sym.symmetrical(1, Cr), Cr, false);
	sym.link_rectangle(upsym(D), sym.symmetrical(1, upsym(D)), sym.symmetrical(1, upsym(Cr)), upsym(Cr), true);
	sym.link_rectangle(Br, sym.symmetrical(1, Br), sym.symmetrical(1, Cr), Cr, true);
	sym.link_rectangle(upsym(Br), sym.symmetrical(1, upsym(Br)), sym.symmetrical(1, upsym(Cr)), upsym(Cr), false);

	sym.link_rectangle(Dr, upsym(Dr), upsym(Cr), Cr);
	sym.link_rectangle(D, upsym(D), upsym(Dr), Dr);
	sym.link_rectangle(D, upsym(D), upsym(C), C, true);
	sym.link_rectangle(Cr, upsym(Cr), upsym(C), C);
}

