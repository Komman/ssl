#ifndef _DEBUG_DRAW_HPP_
#define _DEBUG_DRAW_HPP_

#include "../utils/plan_graph.hpp"

#include "game_drawer.hpp"
#include "collisions.hpp"
#include "motor_debug.hpp"
#include "primary_hitbox.hpp"
#include "hitbox_fusion.hpp"
#include "reliefed_plan_dual_graph.hpp"

namespace debug_draw
{
	void init();
	void free();

	Drawer& texture_drawer();
	void texture(const UniformTexture2D<glm::vec3>& texture);
	void texture(const UniformTexture2D<glm::vec4>& texture);

	void space_box(const spaceBox& space_box);
	void space_box(const BasicHitbox& hitbox);
	void fusion_hbx(const HitboxFusion& fusion);
	void hitbox(const BasicHitbox& hbx);

	void sphere(const sphereInfo& sphere);
	void sphere(const SphereHitbox& sphere_hbx);
	void prism(const prismInfo& sphere);
	void prism(const PrismHitbox& sphere_hbx);
	void line(const triLine& line);
	void graph(const PlanGraph& g, float thickness, const glm::vec3& dep = glm::vec3(0), float size_coeff = 1.0f);
	void rgraph(const PlanGraph& g, const ReliefedPlanDualGraph& dual, float thickness, const glm::vec3& dep = glm::vec3(0), float size_coeff = 1.0f);
	
	void draw(GameDrawer& game_drawer);
};

#endif //_DEBUG_DRAW_HPP_
