#ifndef _HEIGHT_GRID_HPP_
#define _HEIGHT_GRID_HPP_

#include <map>
#include <unordered_map>

#include "../utils/space_grid.hpp"

class HeightGrid : public SpaceGrid
{
public:
	static constexpr float ROOF = std::numeric_limits<float>::min();

public:
	HeightGrid(const glm::vec2& bottom, const glm::vec2& top, uint line_cases_amout);

	void set_point(const glm::vec2& coord, float h);
	void set_circle(const glm::vec2& center, float radius, float h);
	void set_thin_line(const glm::vec2& start, const glm::vec2& stop, float h);
	void set_empty_polygon(const std::vector<glm::vec2>& points, float h);
	void set_filled_polygon(const std::vector<glm::vec2>& points, const glm::vec2& fill_start, float h);
	void set_convex_polygon(const std::vector<glm::vec2>& points, float h);
	void fill(const glm::vec2& start, float h, bool (*is_border)(float));
	void fill(const glm::vec2& start, float h, const std::vector<float>& border_values);
	void fill(const glm::vec2& start, float h);
	void fill_force(const glm::vec2& start, float h);

	bool collision_area(const std::vector<glm::vec2>& area) const;
	bool collision_circle(const glm::vec2& center, float radius) const;
	bool collision_thin_line(const glm::vec2& start, const glm::vec2& stop) const;
	bool collision_empty_polygon(const std::vector<glm::vec2>& points) const;
	bool collision_filling(const glm::vec2& start, float h);
	bool collision_filled_polygon(const std::vector<glm::vec2>& points, const glm::vec2& fill_start, float h);
	bool collision_convex_polygon(const std::vector<glm::vec2>& points, float h);

private:
	static constexpr int ISTART = 3;
	// Returns the int value
	int place_height(float h);

private:
	std::vector<float> _heights;
	std::unordered_map<float, int> _indices;
};

#endif //_HEIGHT_GRID_HPP_
