#include "facechitecture.hpp"

#include <iostream>

#include "../utils/geom.hpp"
#include "../motor/motor_debug.hpp"

using namespace std;
using namespace glm;

Facechitecture::Facechitecture(PlanGraph* g, ReliefedPlanDualGraph* dual, uint outer_face)
	: _graph(g),
	  _dual(dual),
	  _vertices(),
	  _vertices_ref(),
	  _faces_center(),
	  _face_sharing_edges(),
	  _onedge_face_adjency(),
	  _occupied_edges()
{
	std::vector<uint> faces = _dual->all_vertices();
	for(uint f : faces)
	{
		std::vector<uint> vertices = vertices_cycle(_dual->value(f).edges);
		
		for(uint i=0; i<vertices.size(); i++)
		{
			_index_from_face_orivertex[{f, vertices[i]}] = _vertices.size();
			_vertices.push_back(this->original_vertex_position(f, vertices[i]));
			_vertices_ref.push_back({f, i});
		}
	}

	std::vector<uint> fs = _dual->all_vertices();
	for(uint f : fs)
	{
		vec2 pos2D = _graph->center(vertices_cycle(_dual->value(f).edges));
		_faces_center.set(f, geom::unprojplan(pos2D, _dual->face_height(f)));
	}

	_face_sharing_edges = _dual->face_sharing_edges(outer_face);

	for(uint f : faces)
	{
		_onedge_face_adjency.set(f, {});
	}
	for(const dualEdge& de : _face_sharing_edges)
	{
		_onedge_face_adjency[de.face_edge.first ][Edge{this->vertex(de.face_edge.first , de.original_edge.first ), this->vertex(de.face_edge.first , de.original_edge.second)}] = de.face_edge.second;
		_onedge_face_adjency[de.face_edge.second][Edge{this->vertex(de.face_edge.second, de.original_edge.second), this->vertex(de.face_edge.second, de.original_edge.first )}] = de.face_edge.first;
	}
}

std::vector<uint> Facechitecture::faces() const
{
	return _dual->all_vertices();
}

std::vector<uint> Facechitecture::original_vertices() const
{
	return _graph->all_vertices();
}

glm::vec3 Facechitecture::original_vertex_position(uint face, uint vertex) const
{
	vec2 pos2D = _graph->value(vertex);
	return vec3(pos2D.x, _dual->face_height(face), pos2D.y);
}

glm::vec3 Facechitecture::face_position(uint face) const
{
	return _faces_center[face];
}

float Facechitecture::face_radius(uint face)
{
	std::vector<vec2> fpos = geom::projplan(this->vertices_pos(face));
	vec2 center = geom::projplan(this->face_position(face));

	if(fpos.size() < 1)
	{
		mot::err("Facechitecture::face_radius(): the face has no vertices");
	}

	return geom::cycle_mindistance(fpos, center);
}


const std::vector<glm::vec3>& Facechitecture::vertices() const
{
	return _vertices;
}

std::vector<uint> Facechitecture::vertices(uint face) const
{
	return vertices_cycle(this->edges(face));
}

std::vector<Graph::Edge> Facechitecture::edges(uint face) const
{
	std::vector<Edge> original_edges = _dual->value(face).edges;

	std::vector<Edge> ret;
	for(const auto& e : original_edges)
	{
		ret.push_back({
			this->vertex(face, e.first),
			this->vertex(face, e.second)
		});
	}

	return ret;
}


void Facechitecture::check_vertex(uint vertex) const
{
	if(vertex >= _vertices.size())
	{
		mot::err("Facechitecture::check_vertex(uint vertex): vertex out of bound");
	}
}

glm::vec3 Facechitecture::position(uint vertex) const
{
	this->check_vertex(vertex);
	return _vertices[vertex];
}

glm::vec3 Facechitecture::operator[](uint vertex) const
{
	return this->position(vertex);
}

uint Facechitecture::face(uint vertex) const
{
	return _vertices_ref[vertex].first;
}

uint Facechitecture::original_vertex(uint vertex) const
{
	return ((_dual->value(_vertices_ref[vertex].first)).edges)[_vertices_ref[vertex].first].first;
}

uint Facechitecture::vertex(uint face, uint original_vertex) const
{
	auto it = _index_from_face_orivertex.find({face, original_vertex});

	if(it == _index_from_face_orivertex.end())
	{
		mot::err("Facechitecture::vertex(): wrong couple face & original_vertex");
	}

	return it->second;
}


std::vector<Facechitecture::verticalFace> Facechitecture::horizontal_faces(uint ext_face) const
{
	std::vector<verticalFace> ret;

	for(const dualEdge& d : _face_sharing_edges)
	{
		uint v1 = this->vertex(d.face_edge.first, d.original_edge.first);
		uint v2 = this->vertex(d.face_edge.first, d.original_edge.second);
		uint v3 = this->vertex(d.face_edge.second, d.original_edge.first);
		uint v4 = this->vertex(d.face_edge.second, d.original_edge.second);

		ret.push_back(verticalFace{v3, v4, v2, v1});
	}

	return ret;
}

std::vector<uint> Facechitecture::yaligned_vertices(uint original_vertex) const
{
	std::vector<uint> ret;
	std::vector<uint> faces = this->faces();

	for(auto f : faces)
	{
		auto it = _index_from_face_orivertex.find({f, original_vertex});

		if(it != _index_from_face_orivertex.end())
		{
			ret.push_back(it->second);
		}
	}

	return ret;
}

std::vector<glm::vec3> Facechitecture::vertices_pos(uint face) const
{
	return this->position(this->vertices(face));
}

float Facechitecture::height(uint face)
{
	return _faces_center[face].y;
}

void Facechitecture::set_height(uint face, float h)
{
	vec3 tmp = _faces_center[face];
	tmp.y = h;
	_faces_center.set(face,  tmp);
}

void Facechitecture::up_face(uint face, float h)
{
	this->set_height(face, _faces_center[face].y + h);
}

std::vector<glm::vec3> Facechitecture::position(const std::vector<uint>& vertices) const
{
	std::vector<glm::vec3> ret;

	for(uint v : vertices)
	{
		ret.push_back(this->position(v));
	}

	return ret;
}

bool Facechitecture::adjacent(uint face1, uint face2) const
{
	return _dual->connected(face1, face2);
}

void Facechitecture::up_vertices(const std::vector<uint>& vertices, float h)
{
	for(uint v : vertices)
	{
		_vertices[v].y += h;
	}
}

glm::vec3 Facechitecture::edge_extdirN(const Edge& e)
{
	vec3 p1 = this->position(e.first);
	vec3 p2 = this->position(e.second);

	vec3 norma = geom::unprojplan(geom::trigo_normal(geom::projplan(p2 - p1)));

	if(glm::length(norma) < 0.0001f)
	{
		mot::err("Facechitecture::edge_extdirN(): points of edeg are at the same place");
	}

	return -glm::normalize(norma);
}


const std::unordered_map<Facechitecture::Edge, uint, Facechitecture::hashEdge>& Facechitecture::face_adjency(uint face) const
{
	return _onedge_face_adjency[face];
}

std::vector<glm::vec3> Facechitecture::operator[](const std::vector<uint>& vertices) const
{
	return this->position(vertices);
}

std::vector<uint> Facechitecture::adjacent_faces(uint face) const
{
	return _dual->neighbours(face);
}

PlanGraph& Facechitecture::vertex_graph()
{
	return *_graph;
}

ReliefedPlanDualGraph& Facechitecture::face_graph()
{
	return *_dual;
}

const std::unordered_set<Graph::Edge, Graph::hashEdge>& Facechitecture::occupied_edges() const
{
	return _occupied_edges;
}

void Facechitecture::occupy_edge(const Edge& e)
{
	_occupied_edges.insert(e);
}

void Facechitecture::occupy_face(uint face, int idoccupying)
{
	_occupied_faces[face]=idoccupying;
}

bool Facechitecture::is_occupied(const Edge& e) const
{
	return (_occupied_edges.find(e) != _occupied_edges.end());
}

int Facechitecture::face_build_id(uint face) const
{
	auto it = _occupied_faces.find(face);

	if (it == _occupied_faces.end())
	{
		mot::err("Facechitecture::face_build_id(): call to a non occupied face ("
				 + std::to_string(face) + ")");
	}

	return it->second;
}

const std::unordered_map<uint, int>& Facechitecture::occupied_faces() const
{
	return _occupied_faces;
}
