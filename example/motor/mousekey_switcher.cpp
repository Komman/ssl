#include "mousekey_switcher.hpp"

#include <cstddef>

MousekeySwitcher::MousekeySwitcher()
	: _target(NULL)
{

}

void MousekeySwitcher::animate(float dt)
{
	_target->animate(dt);
}

void MousekeySwitcher::target_switch(BasicMousekeyTarget* target)
{
	if(_target != NULL)
	{
		_target->exit();
	}

	_target = target;

	if(_target != NULL)
	{
		_target->enter();
	}
}

const BasicMousekeyTarget* MousekeySwitcher::get_target() const
{
	return _target;
}

BasicMousekeyTarget* MousekeySwitcher::get_target()
{
	return _target;
}

const BasicMousekeyTarget* const* MousekeySwitcher::get_switch_ptr() const
{
	return (const BasicMousekeyTarget* const*)(&_target);
}


