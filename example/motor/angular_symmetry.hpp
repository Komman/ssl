
#ifndef _ANGULAR_SYMMETRY_HPP_
#define _ANGULAR_SYMMETRY_HPP_

#include "../../ssl/ssl.hpp"
#include <glm/gtx/rotate_vector.hpp>

#include <tuple>
#include <unordered_map>

using namespace ssl;

struct cylindricDivisor
{
	glm::vec3 axe_point;
	glm::vec3 axe_directionN;
	uint      divisor;
};

// The ArrayBuffers must not be pop back while using AngularSymmetry 
template<typename... Types>
class AngularSymmetry
{
public:
	AngularSymmetry(const cylindricDivisor& axe,
		  			ArrayBuffer<glm::vec3>* coord_buffer,
				    ElementBuffer*          indices_buffer,
				    ArrayBuffer<Types>*...  symmetricals_buffers);

	uint place_symmetrical( const glm::vec3& position, const Types& ... other_values);
	uint place_asymmetrical(const glm::vec3& position, const Types& ... other_values);
	uint place_symmetrical( const glm::vec3& position, const std::tuple<const Types&...>& t);
	uint place_asymmetrical(const glm::vec3& position, const std::tuple<const Types&...>& t);

	uint      symmetrical(uint division, uint index);
	glm::vec3 symmetrical(uint division, const glm::vec3& pos);

	void link_triangle( uint i1, uint i2, uint i3, float flip = false);
	void link_rectangle(uint i1, uint i2, uint i3, uint i4, float flip = false);

	void print() const;
	const cylindricDivisor& infos() const;

	const ArrayBuffer<glm::vec3>& coord_buffer();
	const ElementBuffer&          indices();

private:
	template<std::size_t... Index>
	void add_other_value(const std::tuple<const Types&...>& t, std::index_sequence<Index...>);
	void add_all_other_values(const std::tuple<const Types&...>& t);
	template<std::size_t... Index>	
	void check_buffer_sizes(std::index_sequence<Index...>) const;
	void check_all_buffer_sizes() const;
	uint get_original_index(uint index) const;

	enum symIndexInfo {ASYMMETRICAL, SYMMETRICAL};

	static std::string index_info_str(symIndexInfo s);

private:
	const cylindricDivisor  _axe;
	ArrayBuffer<glm::vec3>* _coord_buffer;
	ElementBuffer*          _indices;

	std::tuple<ArrayBuffer<Types>* ...>    _symbuffers;
	std::unordered_map<uint, symIndexInfo> _index_infos;
	std::unordered_set<uint>               _originals;
};




template<typename... Types>
AngularSymmetry<Types...>::AngularSymmetry(const cylindricDivisor& axe,
		  								   ArrayBuffer<glm::vec3>* coord_buffer,
				    					   ElementBuffer*          indices_buffer,
				    					   ArrayBuffer<Types>*...  symmetricals_buffers)
	: _axe(axe),
	  _coord_buffer(coord_buffer),
	  _indices(indices_buffer),
	  _symbuffers(symmetricals_buffers...)
{
	if(_axe.divisor <= 1)
	{
		ssl::err("AngularSymmetry::AngularSymmetry(): axe's divisor must be greater than 1");
	}
}

template<typename... Types>
template<std::size_t... Index>
void AngularSymmetry<Types...>::add_other_value(const std::tuple<const Types&...>& t, std::index_sequence<Index...>)
{
	((std::get<Index>(_symbuffers)->add_value(std::get<Index>(t))), ...);
}

template<typename... Types>
void AngularSymmetry<Types...>::add_all_other_values(const std::tuple<const Types&...>& t)
{
	this->add_other_value(t, std::make_index_sequence<sizeof...(Types)>{});
}

template<typename... Types>
template<std::size_t... Index>
void AngularSymmetry<Types...>::check_buffer_sizes(std::index_sequence<Index...>) const
{
	bool all_same = true;

	((all_same = all_same && (std::get<Index>(_symbuffers)->size() == _coord_buffer->size())), ...);

	if(!all_same)
	{
		ssl::err("AngularSymmetry::check_buffer_sizes(): all symm buffers must havn't the same size");
	}
}

template<typename... Types>
void AngularSymmetry<Types...>::check_all_buffer_sizes() const
{
	this->check_buffer_sizes(std::make_index_sequence<sizeof...(Types)>{});
}

template<typename... Types>
uint AngularSymmetry<Types...>::get_original_index(uint index) const
{
	auto info = _index_infos.find(index);

	if(info == _index_infos.end())
	{
		ssl::err("AngularSymmetry::get_original_index(): index not placed with the symmetry");
	}
	if(info->second == ASYMMETRICAL)
	{
		ssl::err("AngularSymmetry::get_original_index(): try to get the original of an ASYMMETRICAL index");
	}
	

	uint i = index;
	
	while(_originals.find(i) == _originals.end())
	{
		if(i == 0)
		{
			ssl::err("AngularSymmetry::get_original_index(): impossible happened here:" + SSL_FILE_AND_LINE
					 + "\n(Unless index given has not be placed with the symmetry)");
		}
		if(index - i >= _axe.divisor)
		{
			ssl::err("AngularSymmetry::get_original_index(): can't find the original index (impossible unless index given has not be placed with the symmetry)");
		}

		i--;
	}

	return i;
}

template<typename... Types>
uint AngularSymmetry<Types...>::place_symmetrical(const glm::vec3& position, const std::tuple<const Types&...>& t)
{
	this->check_all_buffer_sizes();
	uint ret = _coord_buffer->size();

	for(uint i=0; i<_axe.divisor; i++)
	{
		_coord_buffer->add_value(this->symmetrical(i, position));
		this->add_all_other_values(t);
		_index_infos[ret + i] = SYMMETRICAL;
	}

	_originals.emplace(ret);

	return ret;
}

template<typename... Types>
uint AngularSymmetry<Types...>::place_symmetrical(const glm::vec3& position, const Types& ... other_values)
{
	return this->place_symmetrical(position, std::make_tuple<const Types&...>(other_values...));
}

template<typename... Types>
uint AngularSymmetry<Types...>::place_asymmetrical(const glm::vec3& position, const std::tuple<const Types&...>& t)
{
	this->check_all_buffer_sizes();

	uint ret = _coord_buffer->size();

	_coord_buffer->add_value(position);
	this->add_all_other_values(t);

	_index_infos[ret] = ASYMMETRICAL;

	return ret;
}

template<typename... Types>
uint AngularSymmetry<Types...>::place_asymmetrical(const glm::vec3& position, const Types& ... other_values)
{
	return this->place_asymmetrical(position, std::make_tuple<const Types&...>(other_values...));
}


template<typename... Types>
uint AngularSymmetry<Types...>::symmetrical(uint division, uint index)
{
	auto info = _index_infos.find(index);

	if(info == _index_infos.end())
	{
		ssl::err("AngularSymmetry::symmetrical(): index not placed with the symmetry");
	}

	if(info->second == ASYMMETRICAL)
	{
		return index;
	}

	uint ori    = this->get_original_index(index);
	uint div    = index - ori;
	uint newdiv = (div + division) % (_axe.divisor);

	return ori + newdiv;
}

template<typename... Types>
glm::vec3 AngularSymmetry<Types...>::symmetrical(uint division, const glm::vec3& pos)
{
	glm::vec3 point_to_pos = pos - _axe.axe_point;
	glm::vec3 axe_proj     = _axe.axe_point + _axe.axe_directionN*glm::dot(_axe.axe_directionN, point_to_pos);
	glm::vec3 to_rotate    = (pos - axe_proj);

	float divangle = 2.0f * float(M_PI) / _axe.divisor;

	return axe_proj + glm::rotate(to_rotate, float(division) * divangle, _axe.axe_directionN);
}

template<typename... Types>
void AngularSymmetry<Types...>::link_triangle(uint i1, uint i2, uint i3, float flip)
{
	for(uint i=0; i<_axe.divisor; i++)
	{
		_indices->add_triangle(
			this->symmetrical(i, i1),
			this->symmetrical(i, i2),
			this->symmetrical(i, i3), flip);
	}
}

template<typename... Types>
void AngularSymmetry<Types...>::link_rectangle(uint i1, uint i2, uint i3, uint i4, float flip)
{
	for(uint i=0; i<_axe.divisor; i++)
	{
		_indices->add_rectangle(
			this->symmetrical(i, i1),
			this->symmetrical(i, i2),
			this->symmetrical(i, i3),
			this->symmetrical(i, i4), flip);
	}
}

template<typename... Types>
std::string AngularSymmetry<Types...>::index_info_str(symIndexInfo s)
{
	return (s == SYMMETRICAL) ? "SYMMETRICAL" : "ASYMMETRICAL";
}

template<typename... Types>
void AngularSymmetry<Types...>::print() const
{
	std::cout<<"AngularSymmetry : {";
	for(const auto& i : _index_infos)
	{
		std::cout<<"(i:"<<i.first<<": "<<index_info_str(i.second)<<"), ";
	}
	std::cout<<std::endl;
}

template<typename... Types>
const cylindricDivisor& AngularSymmetry<Types...>::infos() const
{
	return _axe;
}

template<typename... Types>
const ArrayBuffer<glm::vec3>& AngularSymmetry<Types...>::coord_buffer()
{
	return *_coord_buffer;
}

template<typename... Types>
const ElementBuffer&          AngularSymmetry<Types...>::indices()
{
	return *_indices;
}


#endif //_ANGULAR_SYMMETRY_HPP_
