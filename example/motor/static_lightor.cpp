#include "static_lightor.hpp"

#include "gutils.hpp"
#include "motor_debug.hpp"

using namespace std;
using namespace glm;
using namespace utils;

StaticLigthor::StaticLigthor(const std::string& name,
				             const glm::vec2&   bottom, 
				             const glm::vec2&   top, 
				             const glm::uvec2&  size,
				  			 unsigned int max_ligths,
				  			 unsigned int max_ligths_references)
	: _name(name),
	  _bottom(name + bottom_addname, bottom), 
	  _top(name + top_addname, top), 
	  _size(size),
	  _max_ligths(max_ligths),
	  _max_ligths_references(max_ligths_references),
	  _ligths(0),
	  _ligths_map(size.x, vector<vector<int>>(size.y, vector<int>(0))),
	  _global_max_ligth(name + max_ligths_addname, (int)(max_ligths)),
	  _ligths_activation(name + activation_addname, 1.0f),
	  _ligths_positions_range(name + ligths_positions_range_addname, max_ligths, GL_NEAREST),
	  _ligths_colors(name + ligths_colors_addname, max_ligths, GL_NEAREST),
	  _ligths_intensity_saturation(name + ligths_intensity_saturation_addname, max_ligths, GL_NEAREST),
	  _ligths_references(name + ligths_references_addname, max_ligths_references, std::vector<int>(max_ligths_references, -1), GL_NEAREST),
	  _indices_map(name + texture_addname, size, std::vector<int>(size.x*size.y, -1), GL_NEAREST)
{
	if(bottom.x > top.x || bottom.y > top.y)
	{
		mot::err("StaticLigthor::StaticLigthor(): we must have : bottom < top for each coordinate");
	}
}

void StaticLigthor::set_ligths_activation(float coeff)
{	
	_ligths_activation = coeff;
}	


void StaticLigthor::add_ligth(const glm::vec3& position, const ligthingInfo& ligth)
{
	unsigned int index = _ligths.size();
	if(_ligths.size() >= _max_ligths)
	{
		mot::err("StaticLigthor: max ligth count reached : " + to_string(_max_ligths));
	}
	_ligths.push_back({position, ligth});

	vec2 block_size = (_top.get_value() - _bottom.get_value()) / vec2(_size);
	vec2 pos = vec2(position.x, position.z);
	float secure_distance = glm::length(block_size)/2.0f;

	ivec2 map_pos = ivec2((pos - _bottom.get_value()) / block_size);
	ivec2 map_range = ivec2(vec2(ligth.range)/block_size) + ivec2(1,1);

	ivec2 range_start = glm::clamp(map_pos - map_range, ivec2(0), ivec2(_size) - ivec2(1));
	ivec2 range_end   = glm::clamp(map_pos + map_range, ivec2(0), ivec2(_size) - ivec2(1));

	for(int x = range_start.x; x<=range_end.x; x++)
	{
		for(int y = range_start.y; y<=range_end.y; y++)
		{
			ivec2 put_pos = ivec2(x, y);
			if(glm::distance(block_size*(vec2(put_pos) + vec2(0.5)) + _bottom.get_value(), pos) < ligth.range + secure_distance)
			{
				this->put_ligth_index(put_pos, index);
			}
		}
	}
}

bool StaticLigthor::put_ligth_index(const glm::ivec2& pos, unsigned int ligth_index)
{
	if(ligth_index >= _ligths.size())
	{
		mot::err("StaticLigthor::put_ligth_index(): try to put an non existing ligth : " + to_string(ligth_index) + "/" + to_string(_ligths.size()));
	}

	if(pos.x >= (int)_size.x || pos.y >= (int)_size.y || pos.x < 0 || pos.y < 0)
	{
		return false;
	}

	_ligths_map[pos.x][pos.y].push_back(ligth_index);

	return true;
}

int StaticLigthor::add_references(const std::vector<int>& ligth_ref, std::vector<int>& vector_ligths_references)
{
	if(ligth_ref.size() == 0)
	{
		mot::err("Try to add an empty reference set here: " + SSL_FILE_AND_LINE);
	}
	if(vector_ligths_references.size() + ligth_ref.size() + 1 >= _max_ligths_references)
	{
		mot::err("StaticLigthor::add_references(): max ligth refrences count reached: " + to_string(_max_ligths_references));
	}

	int ret_index = vector_ligths_references.size();
	for(auto ref : ligth_ref)
	{
		vector_ligths_references.push_back(ref);
	}
	vector_ligths_references[vector_ligths_references.size()-1]*=-1;

	return ret_index;
}

template<typename T>
static int set_sumsize(const vector<vector<T>>& s)
{
	int sum = 0;
	for(const auto& v : s)
	{
		sum += v.size();
	}
	return sum;
}

void StaticLigthor::fill_references_textures(const indexedSets& sets)
{
	std::vector<int> vector_ligths_references;
	std::vector<int> new_sets_references;

	new_sets_references.reserve(sets.sets.size());
	vector_ligths_references.reserve(set_sumsize(sets.sets));

	for(int i=0; i<(int)sets.sets.size(); i++)
	{
		new_sets_references.push_back(vector_ligths_references.size());
	
		this->add_references(sets.sets[i], vector_ligths_references);
	}

	vector<int> ref_map1D;
	ref_map1D.reserve(set_sumsize(sets.indices_map));

	for(int y=0; y<(int)sets.indices_map[0].size(); y++)
	{
		for(int x=0; x<(int)sets.indices_map.size(); x++)
		{
			if(sets.indices_map[x][y] == -1)
			{
				ref_map1D.push_back(-1);
			}
			else
			{
				if(sets.indices_map[x][y] < 0 || sets.indices_map[x][y] >= (int)vector_ligths_references.size())
				{
					mot::err("Wrong value in sets.indices_map here:" + SSL_FILE_AND_LINE);
				}

				ref_map1D.push_back(new_sets_references[sets.indices_map[x][y]]);
			}
		}
	}

	// sets.print();
	// cout<<"Number ligths: "<<_ligths.size()<<endl;
	// cout<<"Number of distinct sets: "<<sets.sets.size()<<endl;
	// cout<<"Size of ligth references : "<<vector_ligths_references.size()<<endl;
	// cout<<"References: "; utils::print_vector(vector_ligths_references);cout<<endl;
	// cout<<"Map1D GPU: ";print_vecvector_niceint_map1D(ref_map1D, sets.indices_map.size());

	_ligths_references.change_data(vector_ligths_references);
	_indices_map.change_data(uvec3(sets.indices_map.size(), sets.indices_map[0].size(), 1),
							 ref_map1D.data());

	vector<glm::vec4> gpu_ligths_positions_range;
	vector<glm::vec3> gpu_ligths_colors;
	vector<glm::vec2> gpu_ligths_intensity_saturation;

	for(const auto& l : _ligths)
	{
		gpu_ligths_positions_range.push_back(vec4(l.position, l.ligth.range));
		gpu_ligths_colors.push_back(l.ligth.color);
		gpu_ligths_intensity_saturation.push_back(vec2(l.ligth.intensity, l.ligth.saturation));
	}

	_ligths_positions_range.change_data(gpu_ligths_positions_range);
	_ligths_colors.change_data(gpu_ligths_colors);
	_ligths_intensity_saturation.change_data(gpu_ligths_intensity_saturation);
}

StaticLigthor::indexedSets StaticLigthor::build_indexed_sets_inclusion()
{
	if(_ligths_map.size() == 0)
	{
		mot::err("StaticLigthor::build_indexed_sets_equality(): map cannot have size=0");
	}

	indexedSets ret;
	ret.indices_map = vector<vector<int>>(_ligths_map.size(), vector<int>(_ligths_map[0].size(), -1));

	for(int x=0; x<(int)_ligths_map.size(); x++)
	{
		for(int y=0; y<(int)_ligths_map[x].size(); y++)
		{
			if(_ligths_map[x][y].size() == 0)
			{
				ret.indices_map[x][y] = -1;
			}
			else
			{
				bool set_included = false;
				for(int i=0; i<(int)ret.sets.size(); i++)
				{
					if(vector_unordered_inclusion(ret.sets[i], _ligths_map[x][y]))
					{
						ret.sets[i] = _ligths_map[x][y];
						set_included = true;
						ret.indices_map[x][y] = i;
						break;
					}	
				}

				if(!set_included)
				{
					int index = vector_unordered_included_in_set(_ligths_map[x][y], ret.sets);
					if(index<0)
					{
						index = ret.sets.size();
						ret.sets.push_back(_ligths_map[x][y]);
					}

					ret.indices_map[x][y] = index;
				}
			}
		}
	}

	return ret;
}

StaticLigthor::indexedSets StaticLigthor::build_indexed_sets_equality()
{
	if(_ligths_map.size() == 0)
	{
		mot::err("StaticLigthor::build_indexed_sets_equality(): map cannot have size=0");
	}

	indexedSets ret;
	ret.indices_map = vector<vector<int>>(_ligths_map.size(), vector<int>(_ligths_map[0].size(), -1));

	for(int x=0; x<(int)_ligths_map.size(); x++)
	{
		for(int y=0; y<(int)_ligths_map[x].size(); y++)
		{
			if(_ligths_map[x][y].size() == 0)
			{
				ret.indices_map[x][y] = -1;
			}
			else
			{
				int index = vector_unordered_in_set(_ligths_map[x][y], ret.sets);
				if(index<0)
				{
					index = ret.sets.size();
					ret.sets.push_back(_ligths_map[x][y]);
				}

				ret.indices_map[x][y] = index;
			}
		}
	}

	return ret;
}

void StaticLigthor::compute_static_ligth_map()
{
	indexedSets isets = this->build_indexed_sets_inclusion();

	this->fill_references_textures(isets);
}

uint StaticLigthor::lights_amount() const
{
	return _ligths.size();
}

uint StaticLigthor::lights_left() const
{
	return _ligths.size() - _max_ligths;
}

void StaticLigthor::print() const
{
	cout<<"{"<<endl;
	for(auto& vx : _ligths_map)
	{
		cout<<" {";
		for(auto& vy : vx)
		{
			int s = vy.size();
			if(s == 0)
			{
				cout<<TERMB::BLACK;
			}
			if(s == 1)
			{
				cout<<TERMB::ORANGE;
			}
			if(s == 2)
			{
				cout<<TERMB::GREEN;
			}
			if(s == 3)
			{
				cout<<TERMB::BLUE;
			}
			if(s == 4)
			{
				cout<<TERMB::CYAN;
			}
			if(s>=100)
			{
				cout<<TERMB::RED;
				cout<<"++";
			}
			else if(s>=10)
			{
				cout<<TERMB::RED;
				cout<<s;
			}
			else
			{
				cout<<s<<" ";
			}
			cout<<TERMB::NOCOL;
		}
		cout<<"},"<<endl;
	}
	cout<<"}"<<endl;
}

void StaticLigthor::indexedSets::print(bool map) const
{
	cout<<"indexedSets : {"<<endl;;

	int i = 0;
	for(const auto& v : sets)
	{
		cout<<" "<<i<<": {";
		for(auto ind : v)
		{
			cout<<ind<<", ";
		}
		cout<<"},"<<endl;
		
		i++;
	}
	cout<<"}"<<endl;

	cout<<"map: {"<<endl;
	for(auto& vx : indices_map)
	{
		cout<<" {";
		for(auto& ind : vx)
		{
			if(ind<0)
			{
				cout<<"- ";
			}
			else if(ind>=10 && ind<100)
			{
				cout<<ind;
			}
			else if(ind<10 )
			{
				cout<<ind<<" ";
			}
			else
			{
				cout<<"+ ";
			}
		}
		cout<<"},"<<endl;
	}
	cout<<"}"<<endl;
}


