#ifndef _MOUSE_JOYSTICK_HPP_
#define _MOUSE_JOYSTICK_HPP_

#include "mouse_joystick_mktarget.hpp"

class MouseJoystick
{
public:
	MouseJoystick(const MouseJoystickMKTarget* mouse, float sensibility);

	const MouseJoystickMKTarget* get_mouse_control() const;
	
	// using the frame mouse move
	glm::vec2 compute_angles();

private:
	const MouseJoystickMKTarget* _mouse;

	float _sensibility;
	bool  _control;

	glm::dvec2 _set_mouse_pixel;
};

#endif //_MOUSE_JOYSTICK_HPP_
