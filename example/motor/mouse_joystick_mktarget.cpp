#include "mouse_joystick_mktarget.hpp"

#include "../../ssl/src/window.hpp"

MouseJoystickMKTarget::MouseJoystickMKTarget(const BasicMousekeyTarget* const* switcher)
	: BasicMousekeyTarget(switcher),
	 _set_mouse_pixel(0),
	 _frame_move(0)
{

}

void MouseJoystickMKTarget::enter()
{
	ssl::mouse::set_visibility(false);
	ssl::mouse::set_pixel(glm::dvec2(double(ssl::window::size().x), double(ssl::window::size().y))/2.0);
	_set_mouse_pixel = ssl::mouse::position_pixel();
}

void MouseJoystickMKTarget::exit()
{
	ssl::mouse::set_visibility(true);
}

void MouseJoystickMKTarget::animate(float dt)
{
	glm::vec2 current_mouse_pos = ssl::mouse::position_pixel();
	ssl::mouse::set_pixel(_set_mouse_pixel);
	_frame_move = current_mouse_pos - glm::vec2(_set_mouse_pixel);
}

const glm::dvec2& MouseJoystickMKTarget::frame_move() const
{
	return _frame_move;
}
