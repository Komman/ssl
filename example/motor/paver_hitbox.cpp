#include "paver_hitbox.hpp"

PaverHitbox::PaverHitbox(const paverInfo& paver)
	: HitboxFusion()
{
	bevelShape b1;
	bevelShape b2;

	b1.triangle_base[0] = paver.bottom;
	b1.triangle_base[1] = paver.bottom + paver.sized_x;
	b1.triangle_base[2] = paver.bottom + paver.sized_y;
	b1.sized_direction  = paver.sized_z;

	glm::vec3 topbot = paver.bottom + paver.sized_x + paver.sized_y + paver.sized_z;

	b2.triangle_base[0] = topbot;
	b2.triangle_base[1] = topbot - paver.sized_x;
	b2.triangle_base[2] = topbot - paver.sized_y;
	b2.sized_direction  = -paver.sized_z;

	this->fusion_hitbox<BevelHitbox>(b1);
	this->fusion_hitbox<BevelHitbox>(b2);
}
PaverHitbox::PaverHitbox(const ycentredPaverInfo& paver)
	: PaverHitbox(geom::to_paver_info(paver))
{
	
}

PaverHitbox::PaverHitbox(const transPaverInfo& paver)
	: PaverHitbox(geom::to_exact_paver_info(paver))
{

}

PaverHitbox::PaverHitbox(const exactPaverInfo& paver)
{
	complexBevelShape b1;
	complexBevelShape b2;

	b1.triangle_base_down[0] = paver.square_base_down[0];
	b1.triangle_base_down[1] = paver.square_base_down[1];
	b1.triangle_base_down[2] = paver.square_base_down[2];
	b1.triangle_base_up[0]   = paver.square_base_up[0];
	b1.triangle_base_up[1]   = paver.square_base_up[1];
	b1.triangle_base_up[2]   = paver.square_base_up[2];

	b2.triangle_base_down[0] = paver.square_base_down[0];
	b2.triangle_base_down[1] = paver.square_base_down[2];
	b2.triangle_base_down[2] = paver.square_base_down[3];
	b2.triangle_base_up[0]   = paver.square_base_up[0];
	b2.triangle_base_up[1]   = paver.square_base_up[2];
	b2.triangle_base_up[2]   = paver.square_base_up[3];

	this->fusion_hitbox<BevelHitbox>(b1);
	this->fusion_hitbox<BevelHitbox>(b2);
}

