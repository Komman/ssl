#include "numbers_displayor.hpp"

using namespace std;
using namespace glm;

std::unique_ptr<UniformArray<int>>   FloatNumbersDisplayor::_uniform_digits = NULL;
std::unique_ptr<Uniform<glm::vec2>>  FloatNumbersDisplayor::_uniform_location = NULL;
std::unique_ptr<Uniform<glm::vec2>>  FloatNumbersDisplayor::_uniform_size = NULL;
std::unique_ptr<Uniform<glm::vec3>>  FloatNumbersDisplayor::_uniform_color = NULL;
std::unique_ptr<Uniform<glm::ivec2>> FloatNumbersDisplayor::_uniform_digit_amount = NULL;

std::unique_ptr<ShaderGenerator> FloatNumbersDisplayor::_shader_gen = NULL;;
std::unique_ptr<Drawer> FloatNumbersDisplayor::_drawer = NULL;;

FloatNumbersDisplayor::FloatNumbersDisplayor(float value, const glm::vec2& location, const glm::vec2& size)
	: _value(value),
	  _location(location),
	  _size(size),
	  _color(0.0f),
	  _digit_amount(3,3)
{
	this->check_digit_amount();

	FloatNumbersDisplayor::init_statics();
}

void FloatNumbersDisplayor::init_statics()
{
	if(_uniform_digits == NULL)
	{
		int digits_max = MAX_DIGIT;
		vector<int> digits;
		digits.reserve(2*digits_max);
		for(int i=0; i<2*digits_max; i++)
		{
			digits.push_back(0);
		}

		_uniform_digits = std::make_unique<UniformArray<int>>("FloatNumbersDisplayor_digits", digits.size(), digits);
		_uniform_location = std::make_unique<Uniform<glm::vec2>>("FloatNumbersDisplayor_location", vec2(0.0f));
		_uniform_size = std::make_unique<Uniform<glm::vec2>>("FloatNumbersDisplayor_size", vec2(0.0f));
		_uniform_color = std::make_unique<Uniform<glm::vec3>>("FloatNumbersDisplayor_color", vec3(0.0f));
		_uniform_digit_amount = std::make_unique<Uniform<glm::ivec2>>("FloatNumbersDisplayor_uniform_digit_amount", ivec2(2,2));
	
		_shader_gen = std::make_unique<ShaderGenerator>("motor/shaders/number_displayor.glsl");
		_drawer     = std::make_unique<Drawer>(*_shader_gen, "number_displayor_vertex", "float_displayor_fragment");
	}
}

void FloatNumbersDisplayor::draw(FrameBuffer<glm::vec4>* framebuffer)
{
	_uniform_location->change_value(_location);
	_uniform_size->change_value(_size);
	_uniform_color->change_value(_color);
	_uniform_digit_amount->change_value(_digit_amount);

	this->compute_digits();

	if(framebuffer != NULL)
	{
		_drawer->draw_in_framebuffer(*framebuffer, buffers::rectangle());
	}
	else
	{
		_drawer->draw(buffers::rectangle());
	}
}

void FloatNumbersDisplayor::check_digit_amount()
{
	if(_digit_amount.x > MAX_DIGIT || _digit_amount.y > MAX_DIGIT)
	{
		err("FloatNumbersDisplayor::check_digit_amount(): too many digits");
	}
}

void FloatNumbersDisplayor::compute_digits()
{
	string valuestr = std::to_string(_value);

	string integer = "";
	string decimal = "";

	unsigned int i = 0;
	for(auto c : valuestr)
	{
		if(c == '.') {break;}

		i++;
		integer += c;
	}
	decimal = valuestr.substr(i+1);

	for(int i=0; i<_digit_amount.x; i++)
	{
		if(i - _digit_amount.x + (int)(integer.size()) < 0)
		{
			_uniform_digits->change_subvalue(i, -1);
		}
		else
		{
			_uniform_digits->change_subvalue(i, integer[i - _digit_amount.x + (int)(integer.size())] - '0');
		}
		// cout<<_uniform_digits->operator[](i)<<endl;
	}
	for(int i=0; i<_digit_amount.y; i++)
	{
		if(i >= (int)(decimal.size()))
		{
			_uniform_digits->change_subvalue(i, -1);
		}
		else
		{
			_uniform_digits->change_subvalue(i + MAX_DIGIT, decimal[i] - '0');
		}
		// cout<<_uniform_digits->operator[](i+MAX_DIGIT)<<endl;
	}

	// for(int i=0; i<(int)(_uniform_digits->max_size()); i++)
	// {
	// 	cout<<_uniform_digits->operator[](i)<<endl;
	// }
}


void  FloatNumbersDisplayor::set_value(float value)
{
	_value = value;
}

float FloatNumbersDisplayor::get_value()
{
	return _value;
}

const glm::vec2& FloatNumbersDisplayor::location() const
{
	return _location;
}

const glm::vec2& FloatNumbersDisplayor::size()     const
{
	return _size; 
}

const glm::vec3& FloatNumbersDisplayor::color()     const
{
	return _color; 
}

void FloatNumbersDisplayor::change_location(const glm::vec2& location)
{
	_location = location;
}

void FloatNumbersDisplayor::change_size(const glm::vec2& size)
{
	_size = size;
}

void FloatNumbersDisplayor::change_color(const glm::vec3& color)
{
	_color = color;
}

void FloatNumbersDisplayor::change_digit_amount(const glm::ivec2& digit_amount)
{
	_digit_amount = digit_amount;

	this->check_digit_amount();
}


