#ifndef _MOTOR_DEBUG_HPP_
#define _MOTOR_DEBUG_HPP_

#include <string>

#define MOTOR_DEBUG
#define GRAPH_DEBUG


#define MOT_FILE_AND_LINE ("(" + std::string(__FILE__) + std::string(", line ") + std::to_string(__LINE__) + ")")

using uint = unsigned int;

namespace mot
{
	class mot_error : public std::exception
	{
	public:
		const char * what() const noexcept override;
	};

	void bouh(const std::string& msg = "");
	
	void err(const std::string& msg);
};

#endif //_MOTOR_DEBUG_HPP_
