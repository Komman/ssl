#ifndef _STORED_VAO_OBJECT_HPP_
#define _STORED_VAO_OBJECT_HPP_

#include "../../ssl/src/vertex_array_object.hpp"
#include "../../ssl/src/drawable_object.hpp"

template<typename ObjectType>
class StoredVAObject : public ObjectType
{
public:
	template<typename... Types>
	StoredVAObject(Types&&... args);

	const ssl::VertexArrayObject& get_vao() const;

private:
	ssl::VertexArrayObject _vao;
};

template<typename ObjectType>
template<typename... Types>
StoredVAObject<ObjectType>::StoredVAObject(Types&&... args)
	: ObjectType(args...),
	  _vao(this->compute_vao())
{

}

template<typename ObjectType>
const ssl::VertexArrayObject& StoredVAObject<ObjectType>::get_vao() const
{
	return _vao;
}


#endif //_STORED_VAO_OBJECT_HPP_
