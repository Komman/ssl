#include "mutils.hpp"

#include <glm/gtx/string_cast.hpp>
#include "../../ssl/src/utils.hpp"

using namespace std;
using namespace glm;

namespace testint
{
	bool is_zero(int x)
	{
		return x==0;
	}

	bool non_zero(int x)
	{
		return x!=0;
	}

	bool is_max(int x)
	{
		return x == std::numeric_limits<int>::max();
	}
};

namespace utils
{
	std::vector<uint> map_vector(const std::vector<uint>& v, const std::vector<uint>& mapping_table)
	{
		std::vector<uint> ret;

		for(uint e : v)
		{
			if(e >= mapping_table.size())
			{
				utils::err("utils::map_vector(): mapping incomplete (too small)");
			}

			ret.push_back(mapping_table[e]);
		}

		return ret;
	}

	bool is_float(const std::string& str)
	{
	    std::istringstream iss(str);
	    float f;
	    iss >> noskipws >> f; // noskipws considers leading whitespace invalid
	    // Check the entire string was consumed and if either failbit or badbit is set
	    return iss.eof() && !iss.fail(); 
	}
	bool is_int(const std::string& str)
	{
	    std::istringstream iss(str);
	    int i;
	    iss >> noskipws >> i; // noskipws considers leading whitespace invalid
	    // Check the entire string was consumed and if either failbit or badbit is set
	    return iss.eof() && !iss.fail(); 
	}

	uint ulog(float x)
	{
		if(x < 0)
		{
			utils::err("utils::ullog(): x negative");
		}

		uint i = 0;

		while((1<<i) < x)
		{
			i++;
		}

		return i;
	}

	bool is_vec3(const std::string& str)
	{
		if(str.size()<7)
		{
			return false;
		}
		string s = str.substr(1, str.size()-2);
		vector<string> floats = utils::split_novoid(s, ",");
		if(floats.size() != 3)
		{
			return false;
		}
		for(uint i=0; i<floats.size(); i++)
		{
			if(!is_float(floats[i]))
			{
				return false;
			}
		}

		return true;
	}


	glm::vec3 tovec3(const std::string& str)
	{
		if(str.size()<7)
		{
			// "(x,y,z)" with at minimum one char for x,y and z coords
			utils::err("utils::tovec3(): Not enough characters");
		}

		string s = str.substr(1, str.size()-2);
		vector<string> floats = utils::split_novoid(s, ",");
		if(floats.size() != 3)
		{
			utils::err("utils::tovec3(): needs 3 floats");
		}
		return vec3(std::stof(floats[0]), std::stof(floats[1]), std::stof(floats[2]));
	}

	void print_vector(const std::vector<glm::vec3>& v)
	{
		cout<<"{";
		for(const auto& e : v)
		{
			cout<<glm::to_string(e)<<", ";
		}
		cout<<"}"<<endl;
	}

	void print_vector(const std::vector<glm::vec2>& v)
	{
		cout<<"{";
		for(const auto& e : v)
		{
			cout<<glm::to_string(e)<<", ";
		}
		cout<<"}"<<endl;
	}

	std::vector<uint> occurences_vector(const std::vector<uint>& v)
	{
		uint m = utils::max(v).first;
		std::vector<uint> ret(m+1, 0);

		for(uint i=0; i<v.size(); i++)
		{
			ret[v[i]] += 1;
		}

		return ret;
	}

	bool unorderd_vector_equals(const std::vector<uint>& v1, const std::vector<uint>& v2)
	{
		auto o1 = occurences_vector(v1);
		auto o2 = occurences_vector(v2);
	
		return (o1 == o2);
	}

	std::vector<uint> uirange(uint first, uint last) 
	{
		return vector_range<uint>(first, last-first+1, 1);
	}

	std::vector<int>  irange(int first, int last)    
	{
		return vector_range<int> (first, last-first+1, 1);
	}

	std::vector<uint> uirange(const std::pair<uint, uint>& range)
	{
		return uirange(range.first, range.second);
	}


	void print_vectorvec3(const std::vector<glm::vec3>& v)
	{
		std::cout<<"{";
		for(auto& e : v)
		{
			std::cout<<" "<<glm::to_string(e)<<","<<std::endl;
		}
		std::cout<<"}";
	}


	void print_vecvector_niceint(const std::vector<std::vector<int>>& v)
	{
		cout<<"{"<<endl;
		for(auto& vx : v)
		{
			cout<<" {";
			for(auto& e : vx)
			{
				if(e<0)
				{
					cout<<"-";
				}
				else if(e>=10)
				{
					cout<<"+";
				}
				else
				{
					cout<<e;
				}
				cout<<" ";
			}
			cout<<"},"<<endl;
		}
		cout<<"}"<<endl;
	}

	void print_vecvector_niceint_map1D(const std::vector<int>& v, unsigned int sizex)
	{
		cout<<"{"<<endl;
		unsigned int colcount = 0;
		for(auto& e : v)
		{
			if(colcount == 0)
			{
				cout<<" {";
			}

			if(e<0)
			{
				cout<<"-";
			}
			else if(e>=10)
			{
				cout<<"+";
			}
			else
			{
				cout<<e;
			}
			cout<<" ";

			colcount++;

			if(colcount >= sizex)
			{
				cout<<"},"<<endl;
				colcount=0;
			}
		}
		cout<<"}"<<endl;
	}

	std::vector<glm::uvec2> get_neighbours(const glm::uvec2& p, const glm::uvec2& max_size)
	{
		std::vector<glm::ivec2> iret = {
			glm::ivec2(p) + glm::ivec2( 1,  0),
			glm::ivec2(p) + glm::ivec2( 0,  1),
			glm::ivec2(p) + glm::ivec2( 0, -1),
			glm::ivec2(p) + glm::ivec2(-1,  0)
		};
		
		std::vector<glm::uvec2> ret;
		ret.reserve(4);

		for(uint i=0; i<iret.size(); i++)
		{
			if(iret[i].x < (int)max_size.x
			&& iret[i].x >= 0
			&& iret[i].y < (int)max_size.y
			&& iret[i].y >= 0)
			{
				ret.push_back(iret[i]);
			}
		}

		return ret;
	}

};