#include "coligrid.hpp"

#include <iostream>
#include <glm/gtx/string_cast.hpp>

#include "../../ssl/src/color.hpp"
#include "../../ssl/src/utils.hpp"

#include "utils_debug.hpp"
#include "mutils.hpp"

using namespace ssl;
using namespace std;


Coligrid::Coligrid(const glm::uvec2& dimension)
	: _grid(dimension.x, vector<int>(dimension.y, 0)),
	  _dimensions(dimension)
{
	if(dimension.x * dimension.y == 0)
	{
		utils::err("Coligrid::Coligrid(): dimensions must be > 0");
	}
}

const glm::uvec2& Coligrid::get_dimensions() const
{
	return _dimensions;
}


void Coligrid::set_point(const glm::uvec2& coord, int value)
{
	if(this->in_bounds(coord))
	{
		_grid[coord.x][coord.y] = value;
	}
}
void Coligrid::safe_set_point(const glm::uvec2& coord, int value)
{
	this->check(coord);

	_grid[coord.x][coord.y] = value;
}

std::vector<glm::uvec2> Coligrid::circle_points(const glm::uvec2& dimension, const glm::vec2& center, float radius)
{
	radius += sqrt(2)/2.0f;

	glm::ivec2 vradius = glm::ivec2(radius + 1.0);
	glm::ivec2 icenter = center;
	glm::ivec2 minc = glm::max(glm::ivec2(0)        , icenter - vradius);
	glm::ivec2 maxc = glm::min(glm::ivec2(dimension), icenter + vradius);

	std::vector<glm::uvec2> ret;

	for(int x = minc.x; x<maxc.x; x++)
	{
		for(int y = minc.y; y<maxc.y; y++)
		{
			glm::vec2 p(float(x)+0.5, float(y)+0.5);

			if(glm::distance(p, center) <= radius)
			{
				if(x >= 0 && y >= 0)
				{
					ret.push_back({x,y});
				}
			}
		}
	}

	return ret;
}

std::vector<glm::uvec2> Coligrid::thin_line_points(const glm::uvec2& dimension, const glm::vec2& start, const glm::vec2& stop)
{
	std::vector<glm::uvec2> ret;
	glm::vec2 path = stop - start;

	uint steps = 1 + (int)(glm::length(path))*2;
	glm::vec2 cur = start;
	glm::vec2 dep = path/(float)(steps);

	for(uint i=0; i<=steps; i++)
	{
		glm::uvec2 ucur = glm::uvec2(cur);
		if(ret.size() == 0 || ucur != ret[ret.size()-1])
		{
			ret.push_back(ucur);
		}
		cur = cur + dep;
	}

	return ret;
}

std::vector<glm::uvec2> Coligrid::polygonial_lines(const glm::uvec2& dimension, const std::vector<glm::vec2>& points)
{
	std::vector<glm::uvec2> ret;
	
	for(uint i=0; i<points.size(); i++)
	{
		glm::vec2 cur  = points[i];
		glm::vec2 next = points[(i+1)%points.size()];
	
		auto pts = thin_line_points(dimension, cur, next);

		for(uint j=0; j<pts.size(); j++)
		{
			if(ret.size()==0 || ret[ret.size()-1]!=pts[j])
			{
				ret.push_back(pts[j]);
			}
		}
	}

	return ret;
}

static bool returns_false(int x)
{
	return false;
}

void Coligrid::fill(const glm::vec2& start, int value, bool (*is_border)(int))
{
	this->custom_fill(start, value, is_border, {});
}
void Coligrid::fill(const glm::vec2& start, int value, const std::vector<int>& border_values)
{
	this->custom_fill(start, value, NULL, border_values);
}
void Coligrid::fill_force(const glm::vec2& start, int value)
{
	this->custom_fill(start, value, returns_false, {});
}
void Coligrid::fill(const glm::vec2& start, int value)
{
	this->custom_fill(start, value, testint::non_zero, {});
}
void Coligrid::fill_discrete(const glm::uvec2& start, int value)
{
	this->custom_fill(start, value, testint::non_zero, {});
}
void Coligrid::fill_discrete(const glm::uvec2& start, int value, int border_value)
{
	this->custom_fill(start, value, NULL, {border_value});
}

bool Coligrid::custom_fill(const glm::vec2& start, int value, bool (*is_border)(int), const std::vector<int>& border_values, const std::vector<int> free_area)
{
	bool collision  = (is_border == NULL) && (border_values.size() == 0);
	bool vectoruse  = (is_border == NULL) && (border_values.size() >  0);
	bool startbound = this->in_bounds(start);

	int freevalue = RESERVED_VALUE;

	if(!startbound)
	{
		return true;
	}
	else
	{
		if(collision)
		{
			freevalue = this->get_point(start);
			if(!utils::in_vector(free_area, freevalue))
			{
				return true;
			}
		}
	}
	if(startbound && _grid[start.x][start.y] == value)
	{
		return collision;
	}

	std::vector<glm::uvec2> tofill = {start};

	while(tofill.size() > 0)
	{
		glm::uvec2 p = tofill[tofill.size()-1];
		tofill.pop_back();

		if(!(this->in_bounds(p)))
			continue;

		if(collision)
		{
			bool freepoint = (freevalue == _grid[p.x][p.y]);

			if(!freepoint && _grid[p.x][p.y] != value && _grid[p.x][p.y] != RESERVED_VALUE)
			{
				this->replace_value(RESERVED_VALUE, freevalue);
				return true;
			}

			if(freepoint)
			{
				_grid[p.x][p.y] = RESERVED_VALUE;
			}
		}
		else
		{
			_grid[p.x][p.y] = value;
		}
		
		auto ngs = utils::get_neighbours(p, _dimensions);
		for(const auto n : ngs)
		{
			if(!(this->in_bounds(n)))
				continue;

			int v = this->get_point(n);
			if(v != value && v != RESERVED_VALUE)
			{
				if(collision)
				{
					tofill.push_back(n);
				}
				else
				{
					if(vectoruse)
					{
						if(!(utils::in_vector(border_values, v)))
						{
							tofill.push_back(n);
						}
					}
					else
					{
						if(!(is_border(v)))
						{
							tofill.push_back(n);
						}
					}
				}
			}
		}
	}

	if(collision)
	{
		this->replace_value(RESERVED_VALUE, freevalue);
	}

	return false;
}



void Coligrid::replace_value(int oldv, int newv)
{
	for(uint x=0; x<_dimensions.x; x++)
	{
		for(uint y=0; y<_dimensions.y; y++)
		{
			if(_grid[x][y] == oldv)
			{
				_grid[x][y] = newv;
			}
		}
	}
}


void Coligrid::set_circle(const glm::vec2& center, float radius, int value)
{
	auto circle = circle_points(_dimensions, center, radius);

	for(const auto p : circle)
	{
		this->set_point(p, value);
	}
}

void Coligrid::set_thin_line(const glm::vec2& start, const glm::vec2& stop, int value)
{
	auto points = thin_line_points(_dimensions, start, stop);

	for(const auto p : points)
	{
		this->set_point(p, value);
	}
}

void Coligrid::set_empty_polygon(const std::vector<glm::vec2>& points, int value)
{
	auto pts = polygonial_lines(_dimensions, points);

	for(const auto p : pts)
	{
		this->set_point(p, value);
	}
}

void Coligrid::set_filled_polygon(const std::vector<glm::vec2>& points, const glm::vec2& fill_start, int value)
{
	auto pts = polygonial_lines(_dimensions, points);

	for(const auto p : pts)
	{
		this->set_point(p, RESERVED_VALUE);
	}

	this->fill_force(fill_start, RESERVED_VALUE);

	this->replace_value(RESERVED_VALUE, value);
}

void Coligrid::set_convex_polygon(const std::vector<glm::vec2>& points, int value)
{
	this->set_filled_polygon(points, utils::average(points), value);
}


bool Coligrid::collision_area(const std::vector<glm::uvec2>& area, const std::vector<int>& free_area) const
{
	if(free_area.size() == 0)
	{
		return (area.size() > 0);
	}

	int freevalue = RESERVED_VALUE;

	for(const auto p : area)
	{
		if(this->in_bounds(p))
		{
			if(freevalue == RESERVED_VALUE)
			{
				freevalue = this->get_point(p);
				if(!utils::in_vector(free_area, freevalue))
				{
					return true;
				}
			}

			if(this->get_point(p) != freevalue)
			{
				return true;
			}
		}
		else
		{
			return true;
		}
	}
	return false;
}


bool Coligrid::collision_circle(const glm::vec2& center, float radius, const std::vector<int>& free_area) const
{
	if(!this->in_bounds(center) || this->get_point(center) != 0)
	{
		return true;
	}

	int freevalue = this->get_point(center);
	if(!utils::in_vector(free_area, freevalue))
	{
		return true;
	}

	return this->collision_area(circle_points(_dimensions, center, radius), {freevalue});
}

bool Coligrid::collision_thin_line(const glm::vec2& start, const glm::vec2& stop, const std::vector<int>& free_area) const
{
	return this->collision_area(thin_line_points(_dimensions, start, stop), free_area);
}

bool Coligrid::collision_empty_polygon(const std::vector<glm::vec2>& points, const std::vector<int>& free_area) const
{
	return this->collision_area(polygonial_lines(_dimensions, points), free_area);
}

bool Coligrid::collision_filling(const glm::vec2& start, int value, const std::vector<int>& free_area)
{
	return custom_fill(start, value, NULL, {}, free_area);
}

bool Coligrid::collision_filled_polygon(const std::vector<glm::vec2>& points, const glm::vec2& fill_start, int value, const std::vector<int>& free_area)
{
	auto borders = polygonial_lines(_dimensions, points);

	int freevalue = RESERVED_VALUE;

	for(const auto& p : borders)
	{
		if(this->in_bounds(p))
		{
			freevalue = this->get_point(p);
			break;
		}
	}

	if(freevalue == RESERVED_VALUE || !utils::in_vector(free_area, freevalue))
	{
		return true;
	}

	if(this->collision_area(borders, {freevalue}))
	{
		return true;
	}

	for(const auto p : borders)
	{
		this->set_point(p, RESERVED_VALUE);
	}

	bool ret = this->collision_filling(fill_start, RESERVED_VALUE, {freevalue});
	this->replace_value(RESERVED_VALUE, freevalue);

	return ret;
}

bool Coligrid::collision_convex_polygon(const std::vector<glm::vec2>& points, int value, const std::vector<int>& free_area)
{
	return this->collision_filled_polygon(points, utils::average(points),value, free_area);
}


void Coligrid::check(const glm::uvec2& coord) const
{
	check_coord(coord, _dimensions);
}

void Coligrid::check(const glm::vec2& coord) const
{
	check_coord(coord, _dimensions);
}

void Coligrid::check_coord(const glm::uvec2& coord, const glm::uvec2& dimension)
{
	if(coord.x >= dimension.x || coord.y >= dimension.y)
	{
		utils::err("static Coligrid::check_coord(): coord out of bound : "
				  + TERM::WHITE + glm::to_string(coord) + "/" + glm::to_string(dimension) + TERM::NOCOL);
	}
}

void Coligrid::check_coord(const glm::vec2& coord, const glm::uvec2& dimension)
{
	if(coord.x < 0 || coord.y < 0)
	{
		utils::err("Coligrid::check_coord(): coord with negative values : "
			 	 + TERM::WHITE + glm::to_string(coord) + TERM::NOCOL);
	}

	check_coord(glm::uvec2(coord), dimension);
}


int Coligrid::get_point(const glm::uvec2& coord) const
{
	this->check(coord);

	return _grid[coord.x][coord.y];
}

int Coligrid::operator()(uint x, uint y) const
{
	return this->get_point(glm::uvec2(x, y));
}

bool Coligrid::in_bounds(const glm::ivec2& p) const
{
	return (p.x >= 0 && p.y >= 0) && (p.x < (int)_dimensions.x && p.y < (int)_dimensions.y);
}

static std::string values_colors[8] = {
	TERMB::BLACK,
	TERMB::WHITE,
	TERMB::RED,
	TERMB::GREEN,
	TERMB::BLUE,
	TERMB::ORANGE,
	TERMB::PURPLE,
	TERMB::CYAN
};

static std::vector<std::string> values_colors_ppm = {
	"0 0 0",
	"255 255 255",
	"255 0 0",
	"0 255 0",
	"0 0 255",
	"255 255 0",
	"255 0 255",
	"0 255 255"
};

#include <fstream>

void Coligrid::print_ppm(const std::string& path) const
{
	this->print_ppm(path, values_colors_ppm);
}

void Coligrid::print_ppm(const std::string& path, const std::vector<std::string>& colors) const
{
	ofstream file(path);

	if(!file.is_open())
	{
		utils::err("Coligrid::print_ppm(): failed to open: " + TERM::colored(path, TERM::BLUE));
	}

	file<<"P3"<<endl;
	file<<_dimensions.x<<" "<<_dimensions.y<<endl;
	file<<"255"<<endl;
	file<<endl;

	for(uint y=0; y<_dimensions.y; y++)
	{
		for(uint x=0; x<_dimensions.x; x++)
		{
			file<<colors[_grid[x][_dimensions.y-1-y]%colors.size()]<<endl;
		}
	}

	file.close();
}

void Coligrid::print() const
{
	for(uint y=0; y<_dimensions.y;y++)
	{
		for(uint x=0; x<_dimensions.x; x++)
		{
			if(_grid[x][y] < 7)
			{
				cout<<values_colors[_grid[x][y]];
			}
			else
			{
				cout<<values_colors[1+_grid[x][y]%6];
			}

			if(_grid[x][y] >= 100)
			{
				cout<<'X';
			}
			else
			{
				if(_grid[x][y] >= 10)
				{
					cout<<_grid[x][y];
				}
				else
				{
					cout<<_grid[x][y]<<' ';
				}
			}
		}
		cout<<endl;
	}
	cout<<TERMB::NOCOL;
}
