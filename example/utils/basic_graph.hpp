#ifndef _BASIC_GRAPH_HPP_
#define _BASIC_GRAPH_HPP_

#include <vector>
#include <string>

class BasicGraph
{
public:
	struct Edge
	{
		uint first;
		uint second;

		Edge opposite();
		bool isomorphic(const Edge& e) const;
		bool operator==(const Edge& e) const;
		std::string to_string() const;
	};

	struct hashEdge
	{
		size_t operator()(const Edge& e) const
		{
			return e.first ^ e.second;
		}
	};

public:
	BasicGraph() {}

	// O(1)
	static Edge mirror(const Edge& e);

};	


std::vector<BasicGraph::Edge> edges_cycle(const std::vector<uint>& vertices);
std::vector<uint> vertices_cycle(const std::vector<BasicGraph::Edge>& edges);



#endif //_BASIC_GRAPH_HPP_
