#include "geom.hpp"
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/projection.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/string_cast.hpp>

#include <unordered_map>
#include <memory>

#include "utils_debug.hpp"
#include "mutils.hpp"
#include "plan_graph.hpp"
#include "space_grid.hpp"

using namespace glm;


std::string triIndex::to_string() const
{
	return "{" + std::to_string(i1) + ", " + std::to_string(i2) + ", " + std::to_string(i3) + "}";
}

std::string collisionImpact::to_string() const
{
	return "{I:" + std::to_string(int(impact))
	+ ", d:" + std::to_string(distance)
	+ ", p:" + glm::to_string(point)
	+ ", N:" + glm::to_string(normalN)
	+ "}";
}

bool circleInfo::inside(const glm::vec2& point) const
{
	return (glm::distance(point, this->center) < this->radius);
}

std::string circleInfo::to_string() const
{
	return "{c= " + glm::to_string(center) + ", r= " + std::to_string(radius) + "}";
}


namespace geom
{
	glm::vec3 safe_normalize(const glm::vec3& v)
	{
		return (glm::length(v)>geom::EPSILON) ? glm::normalize(v) : v;
	}	

	glm::vec2 safe_normalize(const glm::vec2& v)
	{
		return (glm::length(v)>geom::EPSILON) ? glm::normalize(v) : v;
	}	

	glm::vec3 safe_proj(const glm::vec3& v, const glm::vec3& normalN)
	{
		return normalN*glm::dot(v, normalN);
	}

	glm::vec4 safe_normalize_lenght(const glm::vec3& v)
	{
		float l = glm::length(v);
		if(l <  geom::EPSILON)
		{
			return vec4(0);
		}
		else
		{
			return vec4(v/l, l);
		}
	}	


	float relative_convexity(const std::vector<glm::vec2>& cycle, const glm::vec2& relative)
	{
		if(cycle.size() ==0)
		{
			utils::err("geom::relative_convexity(): cycle is empty");
		}
		float d = glm::distance(cycle[0], relative);

		for(uint i=0; i<cycle.size(); i++)
		{
			uint j = (i+1)%cycle.size();

			if(cross2D(cycle[j] - cycle[i], relative - cycle[j]) <= 0)
			{
				return 0.0f;
			}
			else
			{
				d = glm::min(d, distance_point_segment(cycle[i], cycle[j], relative));
			}
		}

		return d;
	}

	float relative_convexity(const std::vector<glm::vec3> cycle, const glm::vec3& relative)
	{
		return relative_convexity(projplan(cycle), projplan(relative));
	}

	float baryconvexity(const std::vector<glm::vec2>& cycle)
	{
		return relative_convexity(cycle, mean(cycle));
	}

	float baryconvexity(const std::vector<glm::vec3> cycle)
	{
		return relative_convexity(cycle, mean(cycle));
	}

	uint longest_edge(const std::vector<glm::vec3>& cycle)
	{
		uint ret = 0;
		float m = 0;

		for(uint i=0; i<cycle.size(); i++)
		{
			uint j=(i+1)%cycle.size();

			float d = glm::distance(cycle[i], cycle[j]);
			if(d > m)
			{
				m=d;
				ret = i;
			}
		}

		return ret;
	}


	glm::vec2 trigo_normal(const glm::vec2& v)
	{
		return vec2(-v.y, v.x);
	}
	
	glm::vec2 bissectriceN(const glm::vec2& v1N, const glm::vec2& v2N)
	{
		vec2 ret = v1N + v2N;

		if(glm::length(ret) < geom::EPSILON)
		{
			return trigo_normal(v1N);
		}
		else
		{
			return glm::normalize(ret);
		}
	}

	glm::vec2 trigo_bissectriceN(const glm::vec2& v1N, const glm::vec2& v2N)
	{
		vec2 ret = trigo_normal(v1N) - trigo_normal(v2N);

		if(glm::length(ret) < geom::EPSILON)
		{
			return trigo_normal(v1N);
		}
		else
		{
			return glm::normalize(ret);
		}
	}

	glm::vec2 trigo_bissectriceN(const glm::vec2& p1, const glm::vec2& p2, const glm::vec2& p3)
	{
		vec2 v1N = p1-p2;
		vec2 v2N = p3-p2;

		float l1 = glm::length(v1N);
		float l2 = glm::length(v2N);

		if(l1 < geom::EPSILON || l2 < geom::EPSILON)
		{
			utils::err("geom::trigo_bissectriceN(): 2 points are at the same place");
		}

		v1N = v1N/l1;
		v2N = v2N/l2;		

		return geom::trigo_bissectriceN(v1N, v2N);
	}

	glm::vec3 trigo_bissectriceN(const glm::vec3& p1, const glm::vec3& p2, const glm::vec3& p3, const glm::vec3& plan_normal)
	{
		using namespace std;
		vec3 v1N = glm::normalize(p2-p1);
		vec3 v2N = glm::normalize(p2-p3);
		vec3 ret = (v1N + v2N);

		vec3 trigo = glm::cross(plan_normal, v1N);

		float l = glm::length(ret);

		if(l < geom::EPSILON)
		{
			ret = trigo;
			l = glm::length(ret);

			if(glm::length(ret) < geom::EPSILON)
			{
				utils::err("trigo_bissectriceN(): normal vector is not normal");
			}
		}

		if(glm::dot(ret, trigo) < 0)
		{
			ret = -ret;
		}

		return ret/l;
	}

	std::vector<glm::vec2> bissectrice_enlarge_cycle(const std::vector<glm::vec2>& cycle, float signed_distance)
	{
		std::vector<glm::vec2> ret;

		for(uint i=0; i<cycle.size(); i++)
		{
			uint prev = (i + cycle.size() - 1) % cycle.size();
			uint next = (i + 1) % cycle.size();

			glm::vec2 v1 = cycle[prev] - cycle[i];
			glm::vec2 v2 = cycle[next] - cycle[i];

			if(glm::length(v2) <= geom::EPSILON || glm::length(v1) <= geom::EPSILON)
			{
				utils::err("gem::bissectrice_enlarge_cycle(): one of the vector in the cycle is quite null");
			}

			ret.push_back(cycle[i] + trigo_bissectriceN(glm::normalize(v1), glm::normalize(v2))*signed_distance);
		}

		#ifdef UTILS_DEBUG
		if(!is_cycle(ret))
		{
			std::string erf = "bissectrice_enlarge_cycle_error.ppm";
			SpaceGrid::print_polygon(erf, ret, 500);
			utils::err("bissectrice_enlarge_cycle(): enlargement of " 
				+ std::to_string(signed_distance) + " has broken the cycle\n (see " 
				+ erf + "");
		}
		if(!trigo_way(ret))
		{
			std::string erf = "bissectrice_enlarge_cycle_error.ppm";
			SpaceGrid::print_polygon(erf, ret, 500);
			utils::err("bissectrice_enlarge_cycle(): enlargement of " 
				+ std::to_string(signed_distance) + " has broken the trigonometric cycle\n (see " 
				+ erf + "");
		}
		#endif

		return ret;
	}

	std::vector<glm::vec3> bissectrice_enlarge_cycle(const std::vector<glm::vec3>& cycle, const glm::vec3& translation, float signed_distance)
	{
		std::vector<glm::vec3> ret3D;
		std::vector<glm::vec2> points2D;

		for(const glm::vec3& v : cycle)
		{
			points2D.push_back(projplan(v));
		}

		std::vector<glm::vec2> ret2D = bissectrice_enlarge_cycle(points2D, signed_distance);

		if(ret2D.size() != cycle.size())
		{
			utils::err("geomm::bissectrice_enlarge_cycle(vec3): bissectrice_enlarge_cycle(vec2) returned a wrong vector size");
		}

		for(uint i=0; i<ret2D.size(); i++)
		{
			ret3D.push_back(unprojplan(ret2D[i], cycle[i].y) + translation);
		}

		return ret3D;
	}

	glm::vec2 directionN(const Segment2D& s)
	{
		float l = glm::length(s.second - s.first);

		if(l < geom::EPSILON)
		{
			utils::err("geom::directionN(): segment is a point");
		}

		return (s.second - s.first)/l;
	}

	bool is_cycle(const std::vector<glm::vec2>& points)
	{
		uint N = points.size();

		for(uint i=0; i<N; i++)
		{
			uint h=(i+N-1)%N;
			uint j=(i+1)%N;

			for(uint k=0; k<N; k++)
			{
				Segment2D s1 = {points[i], points[j]};

				if(k!=i && k!=h && k!=j)
				{
					Segment2D s2 = {points[k], points[(k+1)%N]};
					lineIntersection inter = strong_segment_segment_intersection(s1, s2);

					if(inter.intersection)
					{
						// std::cout<<glm::to_string(s1.first)<<"-"<<glm::to_string(s1.second)<<" inter with "<<glm::to_string(s2.first)<<"-"<<glm::to_string(s2.second)<<std::endl;
						return false;
					}
				}
			}
		}

		return true;
	}

	bool is_cycle(const std::vector<glm::vec3>& points)
	{
		return is_cycle(proj_cycle_on_its_plan(points));
	}

	uint nearest_segment_index(const std::vector<glm::vec2>& cycle, const glm::vec2& point)
	{
		if(cycle.size() <= 1)
		{
			utils::err("geom::nearest_segment_index(): cycle has only one vertex");
		}

		float dmin = glm::distance(cycle[0], cycle[1]);
		uint ret = 0;

		for(uint i=1; i<cycle.size(); i++)
		{
			uint j = (i+1)%cycle.size();

			Segment2D s = {cycle[i], cycle[j]};

			float d = distance_point_segment(s, point);

			if(d < dmin)
			{
				dmin = d;
				ret = i;
			}
		}

		return ret;
	}

	// static uint shortest_segment_cut(const std::vector<glm::vec2>& point_sequence, uint point_index)
	// {
	// 	uint N = point_sequence.size();
	// 	uint ret = (point_index+1)%N;

	// 	float d = glm::distance(point_sequence[point_index], point_sequence[(point_index+1)%N]);
	// 	uint  i = (ret+1)%N;

	// 	while(i!=(point_index+N-1)%N)
	// 	{
	// 		lineIntersection inter = segment_segment_intersection(
	// 			point_sequence[point_index],
	// 			point_sequence[(point_index+1)%N],
	// 			point_sequence[i],
	// 			point_sequence[(i+1)%N]
	// 		);

	// 		if(inter.intersection && d < inter.distance_from_start)
	// 		{
	// 			d = inter.distance_from_start;
	// 			ret = i;
	// 		}

	// 		i=(i+1)%N;
	// 	}

	// 	return ret;
	// }

	std::unordered_map<glm::vec2, uint, hashVec2> extract_indexing(const std::vector<generatedPoint>& cycle, bool perfect)
	{
		std::unordered_map<glm::vec2, uint, hashVec2> ret;

		for(const generatedPoint& p : cycle)
		{
			if(ret.find(p.point) != ret.end() && perfect)
			{
				std::cout<<"cycle: {";
				for(const auto& a : cycle)
				{
					std::cout<<glm::to_string(a.point)<<", ";
				}
				std::cout<<"}"<<std::endl;
				utils::err("geom::extract_indexing(): cycle has a same point twice: "
					+ glm::to_string(p.point));
			}

			ret[p.point] = p.original_index;
		}

		return ret;
	}

	std::vector<glm::vec2> extract_cycle(const std::vector<generatedPoint>& cycle)
	{
		std::vector<glm::vec2> ret;

		for(const generatedPoint& p : cycle)
		{
			ret.push_back(p.point);
		}

		return ret;
	}

	std::vector<generatedPoint> recreate_generated_cycle(const std::vector<glm::vec2>& cycle, const std::unordered_map<glm::vec2, uint, hashVec2>& indexing)
	{
		#ifdef UTILS_DEBUG
		if(!is_cycle(cycle))
		{
			std::string erf = "recreate_generated_cycle_error.ppm";
			SpaceGrid::print_polygon(erf, cycle, 500);
			utils::err("geom::recreate_generated_cycle(): cycles is not a cycle (see " + erf + ")");
		}
		#endif

		std::vector<generatedPoint> ret;

		for(const glm::vec2& p : cycle)
		{
			auto it = indexing.find(p);

			if(it == indexing.end())
			{
				utils::err("geom::recreate_generated_cycle(): indexing is not complete, " +
					glm::to_string(p) + " no found");
			}

			ret.push_back({p, it->second});
		}

		return ret;
	}

	std::vector<glm::vec2> smooth_cycle(const std::vector<glm::vec2>& cycle, float min_distance)
	{
		std::vector<glm::vec2> ret = cycle;
		uint N = ret.size();

		for(uint i=0; i<N; i++)
		{
			uint h = (i+N-1)%N;
			uint j = (i+1)%N;

			if(distance_point_segment({ret[h], ret[i]}, ret[j]) <= min_distance
			|| distance_point_segment({ret[j], ret[i]}, ret[h]) <= min_distance)
			{
				ret[i] = (ret[h]+ret[j])/2.0f;
				// std::cout<<to_string(ret[h])<<to_string(ret[i])<<to_string(ret[j])<<std::endl;
			}
		}

		return ret;
	}

	float distance(const Segment2D& s)
	{
		return glm::distance(s.first, s.second);
	}

	bool unsmooth_croos(const std::vector<glm::vec2>& cycle, float min_distance)
	{
		uint N = cycle.size();

		for(uint i=0; i<N; i++)
		{
			uint h = (i+N-1)%N;
			uint j = (i+1)%N;

			if(distance_point_segment({cycle[h], cycle[i]}, cycle[j]) <= min_distance
			|| distance_point_segment({cycle[j], cycle[i]}, cycle[h]) <= min_distance)
			{
				return true;
			}
		}

		return false;
	}

	// static void gdb_here_conspace_debug()
	// {
	// 	std::cout<<"next"<<std::endl;
	// }
	// static int xx  = 0;
	// static int xxg = 57;
	// static std::unique_ptr<SpaceGrid> sp = NULL;

	std::vector<glm::vec2> enlargement_connection_curved(const glm::vec2& p1, const glm::vec2& p2, const glm::vec2& p3, float signed_distance, float curveprecision_distance)
	{	
		Segment2D s1 = {p1, p2};
		Segment2D s2 = {p2, p3};

		vec2 dir1N = directionN(s1);
		vec2 dir2N = directionN(s2);

		vec2 normal1N = -trigo_normal(dir1N);
		vec2 normal2N = -trigo_normal(dir2N);

		s1 = {s1.first + normal1N*signed_distance, s1.second + normal1N*signed_distance};
		s2 = {s2.first + normal2N*signed_distance, s2.second + normal2N*signed_distance};

		std::vector<glm::vec2> curve = {s1.second, s2.first};
		float approxd = glm::distance(curve[0], curve[1]);

		uint precision;
		if(curveprecision_distance < approxd/4.0f)
		{
		 	precision = 4;
		}
		else
		{
		 	precision = utils::ulog(approxd/curveprecision_distance);
		}

		if(precision == 0 && distance_point_segment({curve[0], curve[1]}, p2) < abs(signed_distance)*0.7f)
		{
			precision = 1;
		}

		while(precision > 0)
		{
			std::vector<glm::vec2> newcurve = {curve[0]};

			for(uint j=0; j<curve.size()-1; j++)
			{
				vec2 dirext = safe_normalize(middle(curve[j], curve[j+1])-p2);

				newcurve.push_back(p2 + dirext*abs(signed_distance));
				newcurve.push_back(curve[j+1]);
			}

			curve = newcurve;
			precision--;
		}

		std::vector<glm::vec2> ret;

		uint lastic = 0;
		for(uint ic=0; ic<curve.size()-1; ic++)
		{
			uint jc = ic+1;

			if(ic==0
			|| distance_point_segment({curve[ic], curve[jc]}, p2) < abs(signed_distance)/2.0f
			|| glm::distance(curve[jc], curve[lastic]) > curveprecision_distance/2.0f)
			{
				ret.push_back(curve[ic]);
				lastic = ic;
			}
		}
		if(glm::distance(ret.back(), curve.back()) > EPSILON)
		{
			ret.push_back(curve.back());
		}

		return ret;
	}

	std::vector<glm::vec2> enlargement_connection_no_curve(const glm::vec2& p1, const glm::vec2& p2, const glm::vec2& p3, float signed_distance)
	{
		Segment2D s1 = {p1, p2};
		Segment2D s2 = {p2, p3};

		vec2 dir1N = directionN(s1);
		vec2 dir2N = directionN(s2);

		vec2 normal1N = -trigo_normal(dir1N);
		vec2 normal2N = -trigo_normal(dir2N);

		s1 = {s1.first + normal1N*signed_distance, s1.second + normal1N*signed_distance};
		s2 = {s2.first + normal2N*signed_distance, s2.second + normal2N*signed_distance};

		lineIntersection inter = segment_segment_intersection(s1, s2);
		
		if(inter.intersection)
		{
			glm::vec2 interp = intersection_point(s1, inter);
			return {interp};
		}
		else
		{
			vec2 start;
			vec2 impactp;
			vec2 dirN;
			float d;
			float safecoeff = 2.0f;

			if(glm::distance(p2, p1) < glm::distance(p2, p3))
			{
				start   = s2.second + dir2N*abs(signed_distance)*safecoeff;
				impactp = p1;
				dirN    = -dir2N;
				d       = glm::distance(p2, p3) + abs(signed_distance)*safecoeff*2.0f;
			}
			else
			{
				start   = s1.first - dir1N*abs(signed_distance)*safecoeff;
				impactp = p3;
				dirN    = dir1N;
				d       = glm::distance(p2, p1) + abs(signed_distance)*safecoeff*2.0f;
			}

			sphereInfo sphim = {
				unprojplan(start),
				abs(signed_distance)
			};
			Segment3D segim = {
				unprojplan(p2),
				unprojplan(impactp)
			};

			collisionImpact impact = sphere_segment_impact(
				sphim,
				segim,
				unprojplan(dirN)*d
			);

			if(!impact.impact || point_on_segment({p1, p3}, p2))
			{
				
				return {s1.second};
			}
			else
			{
				lineIntersection interl = line_line_intersection(s1, {p2, p3});
				
				std::vector<glm::vec2> ret;

				if(interl.intersection)
				{
					if(interl.distance_from_start < geom::distance(s1) + glm::distance(p2, p3))
					{
						// bouh("1");
						ret.push_back(intersection_point(s1, interl));
					}
					else
					{
						// bouh("2");
						ret.push_back(s2.second);
					}
					ret.push_back(p2);
				}
				ret.push_back(start+dirN*impact.distance);

				return ret;
			}
		}
	}
	
	static std::vector<generatedPoint> real_maycross_conspace_enlarge_cycle(const std::vector<glm::vec2>& cycle, float signed_distance, float curveprecision_distance)
	{
		std::vector<generatedPoint> ret;

		uint N = cycle.size();

		uint curvn = std::max(4u, (uint)(abs(1.5f*signed_distance/curveprecision_distance)));
		float signd = glm::sign(signed_distance);

		// sp = std::make_unique<SpaceGrid>(cycle, 600, 5.0f);
		for(uint i=0; i<N; i++)
		{
			uint j=(i+1)%N;

			Segment2D s = {cycle[i], cycle[j]};

			vec2 dirN = directionN(s);

			vec2 normalN = -trigo_normal(dirN);

			s = {s.first + normalN*signed_distance, s.second + normalN*signed_distance};

			for(uint a=curvn; a>=1; a--)
			{
				float anglec = -signd*M_PI/2.0f*((float)(a))/((float)(curvn));
				ret.push_back({
					circle_point(cycle[i], normalN*signd, anglec, abs(signed_distance)),
					i
				});
			}

			ret.push_back({s.first , i});
			ret.push_back({s.second, i});

			for(uint a=1; a<=curvn; a++)
			{
				float anglec = signd*M_PI/2.0f*((float)(a))/((float)(curvn));
				ret.push_back({
					circle_point(cycle[j], normalN*signd, anglec, abs(signed_distance)),
					i
				});
			}

			// bouh(signd);
			// SpaceGrid sp(extract_cycle(std::vector<generatedPoint>(ret.begin()+(ret.size()-(2*curvn+2)), ret.end())), 500, 10.0f);
			// sp.set_thin_line({cycle[i], cycle[j]}, 4);
			// sp.print_ppm("test.ppm");	

		}

		bouh(ret.size());
		return ret;
	}


	// Precision sample for curves is upper bounded to 32
	static std::vector<generatedPoint> maycross_conspace_enlarge_cycle(const std::vector<glm::vec2>& cycle, float signed_distance, float curveprecision_distance)
	{
		// xx++; bool cond = true	;//= (signed_distance < 0) && (cycle.size() == 13);

		if(curveprecision_distance < 0)
		{
			utils::err("geom::maycross_conspace_enlarge_cycle(): curveprecision_distance < 0");
		}
		std::vector<generatedPoint> ret;

		if(cycle.size() <= 2)
		{
			utils::err("geom::conspace_enlarge_cycle(): cycle.size() <= 2");
		}

		uint N = cycle.size();
		vec2 lastconsidered = cycle[0];

		// sp = std::make_unique<SpaceGrid>(cycle, 600, 5.0f);
		for(uint i=0; i<N; i++)
		{
			uint h=(i+N-1)%N;
			uint j=(i+1)%N;

			Segment2D s1 = {cycle[h], cycle[i]};
			Segment2D s2 = {cycle[i], cycle[j]};

			vec2 dir1N = directionN(s1);
			vec2 dir2N = directionN(s2);

			vec2 normal1N = -trigo_normal(dir1N);
			vec2 normal2N = -trigo_normal(dir2N);

			s1 = {s1.first + normal1N*signed_distance, s1.second + normal1N*signed_distance};
			s2 = {s2.first + normal2N*signed_distance, s2.second + normal2N*signed_distance};
	
			bool openangle = (glm::dot(dir1N, normal2N*sign(signed_distance)) > -geom::EPSILON);

			// if(xx == xxg && cond)
			// {
			// 	sp->set_thin_line(s1, 2);
			// 	sp->set_thin_line(s2, 3);
			// }

			std::vector<vec2> pts;

			if(!openangle)
			{
				pts = enlargement_connection_no_curve(
					cycle[h], cycle[i], cycle[j], signed_distance
				);
				// if(xx == xxg && cond) bouh("uncurved enlargement");
			}
			else
			{
				pts = enlargement_connection_curved(
					cycle[h], cycle[i], cycle[j],
					signed_distance, curveprecision_distance
				);
				// if(xx == xxg && cond) bouh("curved enlargement");
			}

			lastconsidered = cycle[i];

			for(const auto& p : pts)
			{
				ret.push_back({p, i});
			}

			// if(xx == xxg && cond)
			// {
			// 	for(uint i=1; i<ret.size(); i++)
			// 	{
			// 		sp->set_thin_line({ret[i-1].point, ret[i].point},5);
			// 	}
			// 	bouh(pts.size(), "points");
			// 	sp->print_ppm("ola.ppm");
			// 	gdb_here_conspace_debug();
			// }
		}

		// bouh(xx);

		return ret;
	}

	void order_trigo(std::vector<glm::vec2>& cycle)
	{
		#ifdef UTILS_DEBUG
		if(!is_cycle(cycle))
		{
			std::string erf = "order_trigo_error.ppm";
			SpaceGrid::print_polygon(erf, cycle, 500);
			utils::err("geom::order_trigo(): cycle is not a cycle\n(see"
				+ erf + ")");
		}
		#endif

		if(!geom::trigo_way(cycle))
		{
			utils::invert(cycle);
		}
	}

	directedLine2D projplan(const directedLine3D& l)
	{
		return {
			.point = projplan(l.point),
			.dirN = projplan(l.dirN)
		};
	}

	
	// Remove consecutive vertices at distance < EPSILON 
	static std::vector<glm::vec2> remove_tooclose(const std::vector<glm::vec2>& cycle)
	{
		if(cycle.size() < 3)
		{
			utils::err("geom::remove_tooclose(): not cycle (size<3)");
		}

		std::vector<glm::vec2> ret;

		for(uint i=0; i<cycle.size(); i++)
		{
			if(i==0 || glm::distance(ret.back(), cycle[i]) > EPSILON)
			{
				ret.push_back(cycle[i]);
			}
		}

		return ret;
	}
	static std::vector<generatedPoint> remove_tooclose(const std::vector<generatedPoint>& fullenlarged_indexed)
	{
		if(fullenlarged_indexed.size() < 2)
		{
			utils::err("geom::remove_tooclose(): not cycle (size<3)");
		}

		std::vector<generatedPoint> ret;
		vec2 last = fullenlarged_indexed.back().point;

		for(uint i=0; i<fullenlarged_indexed.size(); i++)
		{
			if(i==0 || glm::distance(last, fullenlarged_indexed[i].point) > EPSILON)
			{
				ret.push_back(fullenlarged_indexed[i]);
				last = fullenlarged_indexed[i].point;
			}
		}

		return ret;
	}

	std::vector<generatedPoint> conspace_enlarge_cycle_fullinfos(const std::vector<glm::vec2>& cycle, float signed_distance, float curveprecision_distance)
	{
		#ifdef UTILS_DEBUG
		if(!trigo_way(cycle))
		{
			std::string erf = "conspace_enlarge_cycle_fullinfos_error.ppm";
			SpaceGrid::print_polygon(erf, cycle, 500);
			utils::err("geom::conspace_enlarge_cycle_fullinfos(): cycle is not in trigonometric order\n(see"
				+ erf + ")");
		}
		if(!is_cycle(cycle))
		{
			std::string erf = "conspace_enlarge_cycle_fullinfos_error.ppm";
			SpaceGrid::print_polygon(erf, cycle, 500);
			utils::err("geom::conspace_enlarge_cycle_fullinfos(): cycle is not in a cycle\n(see"
				+ erf + ")");
		}
		if(utils::has_duplicate(cycle))
		{
			utils::err("geom::conspace_enlarge_cycle_fullinfos(): duplicates in cycle");
		}
		#endif

		circleInfo goodcircle = max_inscribed_cycle(cycle);

		#ifdef UTILS_DEBUG
		if(signed_distance <= -goodcircle.radius + EPSILON)
		{
			std::string erf = "conspace_enlarge_cycle_fullinfos_error.ppm";
			SpaceGrid sp(cycle, 500, 1.0f);
			sp.set_circle(goodcircle.center, goodcircle.radius, 3);
			sp.print_ppm(erf);
			utils::err("geom::conspace_enlarge_cycle_fullinfos(): delargement signed_distance is not possible:\ndistance <= -goodcircle.radius + EPSILON:\n"
				+ std::to_string(signed_distance) + " < " + std::to_string(-goodcircle.radius)
				+ " + " + std::to_string(EPSILON)
				+ "\n(see " + erf + ")");
		}
		#endif

		std::vector<generatedPoint> fullenlarged_indexed = maycross_conspace_enlarge_cycle(cycle, signed_distance, curveprecision_distance);
		fullenlarged_indexed = remove_tooclose(fullenlarged_indexed);

		auto indexing = extract_indexing(fullenlarged_indexed, false);

		// if(xx == xxg)
		// {
		// 	SpaceGrid::print_polygon("geomo.ppm", extract_cycle(fullenlarged_indexed), 500);
		// }

		PlanGraph p;
		// <vertex index in graph, original cycle index> 
		std::vector<std::pair<uint, uint>> cyclei;

		// bouh("START", xx);
		// bouh("added  vertexed");
		for(uint i=0; i<fullenlarged_indexed.size(); i++)
		{
			auto v = fullenlarged_indexed[i];
			auto added = p.build_planar_vertex(v.point, EPSILON);
			// bouh(to_string(v.point));
			if(i==0 || added.second != cyclei.back().first)
			{
				cyclei.push_back({added.second, v.original_index});
			}
		}

		// bouh("added  edged");
		for(uint i=0; i<cyclei.size(); i++)
		{		
			// if(xx==xxg)
			// {
			// 	bouh("kk");
			// }
			std::vector<uint> verts = p.build_planar_edge(cyclei[i].first, cyclei[(i+1)%cyclei.size()].first);
			
			// if(xx == xxg)
			// {
			// 	auto draw = p.compute_drawing(500);
			// 	draw.set_thin_line(p[cyclei[i].first], p[cyclei[(i+1)%cyclei.size()].first], 5);
			// 	draw.set_circle(p[cyclei[i].first], 0.3f, 4);
			// 	draw.set_circle(p[cyclei[(i+1)%cyclei.size()].first],  0.3f, 4);
			// 	draw.print_ppm("edgebuilt.ppm");
			// }

			for(uint v : verts)
			{
				// bouh(to_string(p[v]));
				indexing[p[v]] = cyclei[i].second;

				// if(xx==xxg)
				// {
				// 	bouh(to_string(p[v]));
				// }
			}
		}

		// if(xx == xxg)
		// 	p.print_ppm("geom.ppm", 500);

		if(signed_distance >= 0.0f)
		{
			std::vector<glm::vec2> ret = p.value(p.outer_face());

			if(!geom::trigo_way(ret))
			{
				utils::invert(ret);
			}
			approximate_cycle(ret, curveprecision_distance, &(goodcircle.center));

			// if(xx == xxg)
			// 	SpaceGrid::print_polygon("ret.ppm", ret, 500);
			return recreate_generated_cycle(ret, indexing);
		}
		else
		{
			std::vector<std::vector<uint>> faces = p.all_faces();

			if(faces.size() == 0)
			{
				utils::err("geom::conspace_enlarge_cycle(): no face");
			}

			// uint dsqds=0;
			for(const std::vector<uint>& f : faces)
			{
				std::vector<glm::vec2> facev = p.value(f);

				// std::cout<<"GOOD CIRCLE: "<<goodcircle.to_string()<<std::endl;
				// SpaceGrid sp(500, facev, 1.0f);
				// sp.set_circle(goodcircle.center, goodcircle.radius, 3);
				// sp.print_ppm("geomt" + std::to_string(dsqds) + ".ppm");
				// dsqds++;

				if(!geom::trigo_way(facev) || !geom::is_cycle(facev))
				{
					continue;
				}
				approximate_cycle(facev, curveprecision_distance, &(goodcircle.center));
				
				if(geom::point_in_polygon(goodcircle.center, facev))
				{
					// if(xx == xxg)
					// 	SpaceGrid::print_polygon("ret.ppm", facev, 500);
					return recreate_generated_cycle(facev, indexing);
				}
			}
			
			std::string erf = "geom_conspace_enlarge_cycle_fullinfos_error.ppm";
			p.print_ppm(erf, 500);
			utils::err("geom::conspace_enlarge_cycle_fullinfos(): no valid face\n(see"
				+ erf +")");
			return {};
		}
	}

	std::vector<glm::vec2> conspace_enlarge_cycle(const std::vector<glm::vec2>& cycle, float signed_distance, float curveprecision_distance)
	{
		auto ret = extract_cycle(conspace_enlarge_cycle_fullinfos(cycle, signed_distance, curveprecision_distance));
		
		// if(xx == xxg)
		// {
		// 	SpaceGrid s(cycle, 500, 2.0f);
		// 	s.set_empty_polygon_doted(ret, 3,4);
		// 	s.print_ppm("conspace_enlarge_cycle_vec2_vec2.ppm");
		// }

		return ret;
	}

	lineIntersection halfline_segment_intersection(const directedLine2D& halfline, const Segment2D& s)
	{
		float l = distance(s);
		if(l < EPSILON)
		{
			return {false, 0.0f};
		}
		lineIntersection inter = halfline_line_intersection(halfline, {s.first, (s.second - s.first)/l});
	
		if(!inter.intersection)
		{
			return {false, 1.0f};
		}
		else
		{
			glm::vec2 inter2 = intersection_point(halfline, inter);

			if(online_point_on_segment(s, inter2, EPSILON))
			{
				return inter;
			}
			else
			{
				return {false, 2.0f};
			}
		}
	}

	float distance_point_segment(const Segment2D& s, const glm::vec2& point)
	{
		return distance_point_segment(s.first, s.second, point);
	}
	
	float cycle_conspace_maxdelargement(const std::vector<glm::vec2>& points)
	{
		#ifdef UTILS_DEBUG
		if(!trigo_way(points))
		{
			utils::err("geom::cycle_conspace_maxdelargement(): cycle is not in trigonometric order");
		}
		#endif

		return max_inscribed_cycle(points).radius;
	}

	// float cycle_conspace_maxdelargement(const std::vector<glm::vec2>& points)
	// {
	// 	uint N = points.size();
	// 	float maxd = 0;

	// 	for(uint i=0; i<N; i++)
	// 	{
	// 		vec2 p = points[i];
			
	// 		uint h=(i+N-1)%N;
	// 		// uint j=(i+1)%N;

	// 		for(uint k=0; k<N; k++)
	// 		{
	// 			if(k!=i && k!=h)
	// 			{
	// 				Segment2D s = {points[k], points[(k+1)%N]};

	// 				float d = distance_point_segment(s, p)/2.0f;
	// 				vec2 dir1 = p-s.first;
	// 				vec2 dir2 = p-s.second;
	// 				vec2 inside = trigo_normal(points[(k+1)%N]-points[k]);

	// 				if(d > maxd && (glm::dot(inside, dir1)>0.0f || glm::dot(inside, dir2)>0.0f))
	// 				{
	// 					maxd = d;
	// 				}
	// 			}
	// 		}
	// 	}

	// 	return maxd;
	// }

	float cycle_conspace_maxdelargement(const std::vector<glm::vec3>& points)
	{
		return cycle_conspace_maxdelargement(proj_cycle_on_its_plan(points));
	}

	partialCycle<glm::vec2> partial_cycle(const std::vector<glm::vec2>& cycle, uint cut_edge)
	{
		if(cut_edge >= cycle.size())
		{
			utils::err("geom::partial_cycle(): cut_edge out of bounds : "
				+ std::to_string(cut_edge) + "/" + std::to_string(cycle.size()));
		}

		partialCycle<glm::vec2> ret(cycle, {cut_edge});

		ret.check();
		return ret;
	}

	partialCycle<glm::vec3> partial_cycle(const std::vector<glm::vec3>& cycle, uint cut_edge)
	{
		if(cut_edge >= cycle.size())
		{
			utils::err("geom::partial_cycle(): cut_edge out of bounds : "
				+ std::to_string(cut_edge) + "/" + std::to_string(cycle.size()));
		}

		partialCycle<glm::vec3> ret(cycle, {cut_edge});

		ret.check();
		return ret;
	}

	partialCycle<glm::vec2> partial_cycle(const partialCycle<glm::vec2>& cycle, uint cut_edge)
	{
		cycle.check();

		if(cut_edge >= cycle.cycle.size())
		{
			utils::err("geom::partial_cycle(): cut_edge out of bounds : "
				+ std::to_string(cut_edge) + "/" + std::to_string(cycle.cycle.size()));
		}

		partialCycle<glm::vec2> ret = cycle;
		ret.edge[cut_edge] = false;

		return ret;
	}

	partialCycle<glm::vec3> partial_cycle(const partialCycle<glm::vec3>& cycle, uint cut_edge)
	{
		cycle.check();

		if(cut_edge >= cycle.cycle.size())
		{
			utils::err("geom::partial_cycle(): cut_edge out of bounds : "
				+ std::to_string(cut_edge) + "/" + std::to_string(cycle.cycle.size()));
		}

		partialCycle<glm::vec3> ret = cycle;
		ret.edge[cut_edge] = false;

		return ret;
	}

	partialEnlargementInfos<glm::vec3> conspace_enlarge_cycle(const partialCycle<glm::vec3>& cycle, float signed_distance, float curveprecision_distance)
	{
		base2D plan = find_trigo_plan(cycle.cycle);

		partialEnlargementInfos<glm::vec2> ret2D = conspace_enlarge_cycle(projplan(cycle, plan), signed_distance, curveprecision_distance);

		// if(xx==21)
		// {
		// 	SpaceGrid s(projplan(cycle.cycle), 500, 3.0f);
		// 	s.set_empty_polygon(projplan(unprojplan(ret2D.compute_outline(), plan)), 3);
		// 	s.print_ppm("au_secour.ppm");
		// }

		return unprojplan(ret2D, plan);
	}


	partialEnlargementInfos<glm::vec2> conspace_enlarge_cycle(const partialCycle<glm::vec2>& cycle, float signed_distance, float curveprecision_distance)
	{
		#ifdef UTILS_DEBUG
		if(!trigo_way(cycle.cycle))
		{
			std::string erf = "conspace_enlarge_cycle_error.ppm";
			SpaceGrid::print_polygon(erf, cycle.cycle, 500);
			utils::err("geom::conspace_enlarge_cycle(): cycle is not in trigonometric order\n(see"
				+ erf + ")");
		}
		#endif

		cycle.check();

		std::vector<generatedPoint> outer = conspace_enlarge_cycle_fullinfos(cycle.cycle, signed_distance, curveprecision_distance);
		// SpaceGrid s(extract_cycle(outer), 500, std::max(0.0f, signed_distance));
		// SpaceGrid::print_polygon("geomb.ppm", extract_cycle(outer), 500);

		// Build ret begin
		partialEnlargementInfos<glm::vec2> ret;
		ret.points = cycle.cycle;
		for(const generatedPoint& p : outer)
		{
			ret.points.push_back(p.point);
		}

		uint N = cycle.cycle.size();
		uint S = ret.points.size();
		uint T = outer.size();
		ret.base     = partialCycle<uint>(utils::uirange(0, N-1), utils::neg(cycle.edge)      );
		ret.enlarged = partialCycle<uint>(utils::uirange(N, S-1), std::vector<bool>(T, true));

		partialCycle<uint>&             outline = ret.enlargement.outline;
		std::vector<std::vector<uint>>& faces   = ret.enlargement.faces;

		uint first   = 0;
		uint firstin = 0;

		// Build outline
		int lastcut = -1;
		if(!cycle.edge[outer.back().original_index])
		{
			lastcut = outer.back().original_index;
		}

		for(uint i=0; i<T; i++)
		{
			uint cycle_index = outer[i].original_index;

			if(cycle.edge[cycle_index])
			{	
				if(lastcut != -1)
				{
					firstin = (lastcut+1)%N;
					first = i;
				}
				outline.cycle.push_back(cycle.cycle.size() + i);
				outline.edge.push_back(true);
				lastcut = -1;

				ret.outer_added.push_back(outline.cycle.back());
			}
			else
			{
				ret.enlarged.edge[i] = false;

				if(lastcut != (int)cycle_index)
				{
					if(lastcut == -1)
					{
						outline.cycle.push_back(cycle.cycle.size() + i);
						outline.edge.push_back(true);
						ret.outer_added.push_back(outline.cycle.back());

						outline.cycle.push_back(cycle_index);
						outline.edge.push_back(false);
					}
					else
					{
						#ifdef UTILS_DEBUG
						if(outline.edge.size() == 0)
						{
							utils::err("geom::conspace_enlarge_cycle(): algorithm impossible happened");
						}
						#endif

						outline.edge.back() = false;
					}
					
					outline.cycle.push_back((cycle_index+1)%N);
					outline.edge.push_back(true);

					lastcut = cycle_index;
				}
			}
		}

		outline.check();
		ret.base.check();
		ret.enlarged.check();

		#ifdef UTILS_DEBUG
		if(!is_cycle(ret.compute_outline()))
		{
			std::string erf1 = "conspace_enlarge_cycle_error1.ppm";
			std::string erf2 = "conspace_enlarge_cycle_error2.ppm";
			std::string erf3 = "conspace_enlarge_cycle_error3.ppm";
			SpaceGrid::print_polygon(erf3, ret.compute_outline(), 500);
			SpaceGrid::print_polygon(erf1, cycle.cycle, 500);
			SpaceGrid::print_polygon(erf2, extract_cycle(outer), 500);
			utils::err("geom::conspace_enlarge_cycle(): outlie computed is not a cycle (see " 
				+ erf1 + ", " + erf2 + ", " + erf3 + ")");
		}
		#endif

		// Faces computation
		std::vector<uint> face = {(uint)(cycle.cycle.size()) + first};
		lastcut = -1;

		for(uint i=(first+1)%T; i!=first; i=(i+1)%T)
		{
			uint cycle_index = outer[i].original_index;

			if(cycle.edge[cycle_index])
			{
				if(lastcut != -1)
				{
					firstin = (lastcut+1)%N;
				}
				face.push_back(cycle.cycle.size() + i);
				lastcut = -1;
			}
			else
			{
				if(lastcut != (int)cycle_index)
				{
					if(lastcut == -1)
					{

						face.push_back(cycle.cycle.size() + i);
						face.push_back(cycle_index);

						uint j = (cycle_index+N-1)%N;
						while(j != firstin)
						{
							face.push_back(j);
							j = (j+N-1)%N;
						}
						face.push_back(firstin);

						#ifdef UTILS_DEBUG
						if(face.size() < 3)
						{
							utils::err("conspace_enlarge_cycle(): try to add face of size <3");
						}
						#endif

						faces.push_back(face);
						face.clear();
					}
					
					lastcut = cycle_index;
				}
			}
		}
		
		// bouh(ret.enlargement.to_string());

		// s.set_empty_polygon(ret.compute_outline(), 2);
		// s.print_ppm("geoma.ppm");
		// SpaceGrid::print_polygon("geoma.ppm", ret.compute_outline(), 500);
		return ret;
	}

	glm::vec2 get_inside_point(const std::vector<glm::vec2>& polygon)
	{
		triIndex i = find_cycle_corner(polygon);
		return (polygon[i.i1] + polygon[i.i2] + polygon[i.i3])/3.0f;
	}

	std::vector<glm::vec3> conspace_enlarge_cycle(const std::vector<glm::vec3>& cycle, const glm::vec3& translation, float signed_distance, float curveprecision_distance)
	{
		std::vector<glm::vec3> ret3D;
		base2D base = find_trigo_plan(cycle);

		std::vector<glm::vec2> points2D = projplan(cycle, base);
		std::vector<glm::vec2> ret2D = conspace_enlarge_cycle(points2D, signed_distance, curveprecision_distance);

		return translate(unprojplan(ret2D, base), translation);
	}

	glm::vec3 highest(const std::vector<glm::vec3>& points)
	{
		if(points.size() == 0)
		{
			utils::err("geom::highest(): points.size() = 0");
		}	

		glm::vec3 ret = points[0];

		for(const auto& v : points)
		{
			if(v.y > ret.y)
			{
				ret = v;
			}
		}

		return ret;
	}

	uint highest(const std::vector<glm::vec2>& points)
	{
		if(points.size() == 0)
		{
			utils::err("geom::highest(): points.size() = 0");
		}	

		glm::vec2 hest = points[0];
		uint ret = 0;

		for(uint i=1; i<points.size(); i++)
		{
			if(points[i].y > hest.y)
			{
				hest = points[i];
				ret = i;
			}
		}

		return ret;
	}
	glm::vec3 lowest(const std::vector<glm::vec3>& points)
	{
		if(points.size() == 0)
		{
			utils::err("geom::highest(): points.size() = 0");
		}	

		glm::vec3 ret = points[0];

		for(const auto& v : points)
		{
			if(v.y < ret.y)
			{
				ret = v;
			}
		}

		return ret;
	}

	float angle(const glm::vec2& v1, const glm::vec2& v2)
	{
		return mod(atan2(v1.x*v2.y - v1.y*v2.x, glm::dot(v1,v2)), 2.0f*M_PI);
	}

	glm::vec2 projplan(const glm::vec3& v)
	{
		return vec2(v.x, v.z);
	}

	glm::vec2 projplan(const glm::vec3& v, const base2D& plan)
	{
		return vec2(glm::dot(v - plan.origin, plan.dirxN), glm::dot(v - plan.origin, plan.diryN));
	}

	partialCycle<glm::vec2> projplan(const partialCycle<glm::vec3>& cycle, const base2D& plan)
	{
		partialCycle<glm::vec2> ret;

		ret.cycle = projplan(cycle.cycle, plan);
		ret.edge  = cycle.edge;

		return ret;
	}

	partialEnlargementInfos<glm::vec2> projplan(const partialEnlargementInfos<glm::vec3>& cycle, const base2D& plan)
	{
		partialEnlargementInfos<glm::vec2> ret;

		ret.points  = projplan(cycle.points, plan);
		ret.outer_added = cycle.outer_added;
		ret.enlargement = cycle.enlargement;
		ret.base        = cycle.base;
		ret.enlarged    = cycle.enlarged;

		return ret;
	}

	partialCycle<glm::vec3> unprojplan(const partialCycle<glm::vec2>& cycle, const base2D& plan)
	{
		partialCycle<glm::vec3> ret;

		ret.cycle = unprojplan(cycle.cycle, plan);
		ret.edge  = cycle.edge;

		return ret;
	}

	partialEnlargementInfos<glm::vec3> unprojplan(const partialEnlargementInfos<glm::vec2>& cycle, const base2D& plan)
	{
		partialEnlargementInfos<glm::vec3> ret;

		ret.points  = unprojplan(cycle.points, plan);
		ret.outer_added = cycle.outer_added;
		ret.enlargement = cycle.enlargement;
		ret.base        = cycle.base;
		ret.enlarged    = cycle.enlarged;

		return ret;
	}
	
	glm::vec2 orthogonal(const glm::vec2& v)
	{
		return vec2(-v.y, v.x);
	}

	glm::vec2 rotate_point2D(const glm::vec2& point, const glm::vec2& dirN)
	{
		vec2 ndirX = dirN;
		vec2 ndirY = vec2(dirN.y, -dirN.x);

		return ndirX*point.x + ndirY*point.y;
	}

	glm::vec3 planar_rotate(const glm::vec3& point,  const glm::vec3& center, const glm::vec2& old_dirN, const glm::vec2& new_dirN)
	{
		vec3 oldN = geom::unprojplan(old_dirN);
		vec3 newN = geom::unprojplan(new_dirN);

		vec3 oldorthoN = glm::cross(oldN, vec3(0,1,0));
		vec3 neworthoN = glm::cross(newN, vec3(0,1,0));

		vec3 topoint = point - center;
		
		float oldL      = glm::dot(topoint, oldN);
		float oldorthoL = glm::dot(topoint, oldorthoN);

		return vec3(center.x, point.y, center.z) + newN*oldL + neworthoN*oldorthoL;
	}

	glm::vec3 reflect(const glm::vec3& v, const glm::vec3& normalN)
	{
		return v - normalN*2.0f*glm::dot(v, normalN);
	}

	glm::mat3 symmetrical(const glm::mat3& rotation_matrix, const glm::vec3& normalN)
	{
		return glm::mat3(reflect(rotation_matrix[0], normalN),
						 reflect(rotation_matrix[1], normalN),
						 reflect(rotation_matrix[2], normalN));
	}

	glm::vec3 symmetrical(const glm::vec3& point, const infinitePlan& sym_plan)
	{
		return point + glm::proj(sym_plan.point - point, sym_plan.normalN)*2.0f;
	}

	paverInfo symmetrical(const paverInfo& paver, const infinitePlan& sym_plan)
	{
		return paverInfo{
			.bottom  = symmetrical(paver.bottom, sym_plan),
			.sized_x = reflect(paver.sized_x, sym_plan.normalN),
			.sized_y = reflect(paver.sized_y, sym_plan.normalN),
			.sized_z = reflect(paver.sized_z, sym_plan.normalN)
		};
	}


	glm::vec3 circle_point_plan(const glm::vec3& circle_center,
						   const glm::vec3& xaxisN,
						   const glm::vec3& yaxisN,
						   float angle,
						   float radius)
	{
		return circle_center + glm::rotate(xaxisN, angle, glm::cross(xaxisN, yaxisN))*radius;
	}

	glm::vec3 unprojplan(const glm::vec2& v, float y)
	{
		return glm::vec3(v.x, y, v.y);
	}

	glm::vec3 unprojplan(const glm::vec2& v, const base2D& original_plan)
	{
		return original_plan.origin + (original_plan.dirxN*v.x + original_plan.diryN*v.y);
	}

	std::vector<glm::vec3> unprojplan(const std::vector<glm::vec2>& v, const base2D& original_plan)
	{
		std::vector<glm::vec3> ret;

		for(const auto& p : v)
		{
			ret.push_back(unprojplan(p, original_plan));
		}

		return ret;
	}

	glm::vec3 circle_point(const glm::vec3& circle_center,
						   const glm::vec3& xaxisN,
						   const glm::vec3& normalN,
						   float angle,
						   float radius)
	{
		return circle_center + glm::rotate(xaxisN, angle, normalN)*radius;
	}

	glm::vec2 circle_point(const glm::vec2& circle_center,
						   const glm::vec2& xaxisN,
						   float angle,
						   float radius)
	{
		return projplan(circle_point(unprojplan(circle_center), unprojplan(xaxisN), vec3(0,1,0), -angle, radius));
	}

	glm::vec2 circle_point(const glm::vec2& circle_center,
						   float angle,
						   float radius)
	{
		return circle_point(circle_center, vec2(1,0), angle, radius);
	}


	std::vector<glm::vec3> circle_points(const glm::vec3& circle_center,
										 const glm::vec3& xaxisN,
										 const glm::vec3& normalN,
										 float radius,
										 uint points_amount)
	{
		std::vector<glm::vec3> ret;

		float dangle = float(2.0f*M_PI)/float(points_amount);
		for(uint i=0; i<points_amount; i++)
		{
			ret.push_back(geom::circle_point(
				circle_center,
				xaxisN,
				normalN,
				dangle*float(i),
				radius
			));
		}

		return ret;
	}

	glm::ivec2 unit_direction(const glm::vec2& start, const glm::vec2& dst)
	{
		glm::ivec2 dir(0);

		if(start.x < dst.x)
		{
			dir.x = 1;
		}
		if(start.y < dst.y)
		{
			dir.y = 1;
		}
		if(start.x > dst.x)
		{
			dir.x = -1;
		}
		if(start.y > dst.y)
		{
			dir.y = -1;
		}

		return dir;
	}

	glm::vec3 rotate_redirect(const glm::vec3& v,
						      const glm::vec3& original_dirN,
						      const glm::vec3& new_dirN)
	{
		glm::vec3 c = glm::cross(original_dirN, new_dirN);

		if(glm::length(c) <= geom::EPSILON)
		{
			return v;
		}

		c = glm::normalize(c);
		glm::vec3 t = glm::normalize(cross(c, original_dirN));
		glm::vec3 q = glm::normalize(cross(c, new_dirN));

		return   glm::dot(v, original_dirN)*new_dirN
			   + glm::dot(v, c)*c
			   + glm::dot(v, t)*q;
	}

	glm::vec3 get_orthogonalyx_N(const glm::vec3& v)
	{
		vec3 ortho = glm::cross(v, vec3(0,1,0));

		if(glm::length(ortho) <= geom::EPSILON)
		{
			return glm::cross(v, vec3(1,0,0));
		}
		else
		{
			return glm::normalize(ortho);
		}
	}

	glm::vec3 plan_projection(const glm::vec3& point,
							  const glm::vec3& plan_point,
							  const glm::vec3& plan_normalN)
	{
		vec3 localpoint = point - plan_point;

		vec3 planproj = glm::proj(localpoint, plan_normalN);

		return point - planproj;
	}

	glm::vec3 plan_projection(const glm::vec3& point,
							  const glm::vec3& plan_point1,
							  const glm::vec3& plan_point2,
							  const glm::vec3& plan_point3)
	{
		vec3 v1 = plan_point2 - plan_point1;
		vec3 v2 = plan_point3 - plan_point1;

		vec3 normalN = glm::normalize(glm::cross(v1, v2));

		return plan_projection(point, plan_point1, normalN);
	}

	bool point_in_triangle(const glm::vec3& point,
						   const glm::vec3& triangle_point1,
						   const glm::vec3& triangle_point2,
						   const glm::vec3& triangle_point3)
	{
		vec3 tocorners[3] = {
			triangle_point1 - point,
			triangle_point2 - point,
			triangle_point3 - point
		};

		for(uint i=0; i<3; i++)
		{
			uint i0 = i;
			uint i1 = (i+1)%3;
			uint i2 = (i+2)%3;

			vec3 proj1 = glm::proj(tocorners[i1], tocorners[i0]);
			vec3 proj2 = glm::proj(tocorners[i2], tocorners[i0]);

			if(glm::dot(proj1, tocorners[i0]) < 0 && glm::dot(proj2, tocorners[i0]) < 0)
			{
				if(glm::dot(glm::cross(tocorners[i1], proj1), glm::cross(tocorners[i2], proj2)) < 0)
				{
					return true;
				}
			}
		}

		return false;
	}

	bool point_in_triangle(const glm::vec3& point,
						   const glm::vec3 plan3points[3])
	{
		return point_in_triangle(point, plan3points[0], plan3points[1], plan3points[2]);
	}


	bool point_in_triangle(const glm::vec2& point,
						   const glm::vec2& triangle_point1,
						   const glm::vec2& triangle_point2,
						   const glm::vec2& triangle_point3)
	{
		return point_in_triangle(unprojplan(point),
								 unprojplan(triangle_point1),
								 unprojplan(triangle_point2),
								 unprojplan(triangle_point3));
	}

	bool point_in_triangle(const glm::vec2& point,
						   const glm::vec2 plan3points[3])
	{
		glm::vec3 points[3] = {
			unprojplan(plan3points[1]),
			unprojplan(plan3points[2]),
			unprojplan(plan3points[3])
		};

		return point_in_triangle(unprojplan(point), points);
	}

	float angle_plan(const glm::vec2& v1, const glm::vec2& v2)
	{
		float a = glm::dot(v1, v2)/(glm::length(v1)*glm::length(v2));
		return acos(glm::clamp(a, -1.0f, 1.0f))*glm::sign(v1.x*v2.y-v1.y*v2.x);
	}

	bool online_point_on_segment(const Segment2D& seg, const glm::vec2& point, float precision)
	{
		float L = glm::distance(seg.first, seg.second);
		return (glm::distance(seg.first, point) < L-precision) && (glm::distance(seg.second, point) < L-precision);
	}
	
	bool point_on_segment(const glm::vec2& line_point1, const glm::vec2& line_point2, const glm::vec2& point)
	{
		return geom::point_on_segment({line_point1, line_point2}, point);
	}

	bool point_on_segment(const Segment2D& seg, const glm::vec2& point)
	{	
		if(abs(cross2D(safe_normalize(seg.second - seg.first), safe_normalize(point - seg.first))) > EPSILON)
		{
			return false;
		}

		return online_point_on_segment(seg, point);
	}

	bool large_point_on_segment(const glm::vec2& line_point1, const glm::vec2& line_point2, const glm::vec2& point, float radius)
	{
		float L = glm::distance(line_point1, line_point2);
		return (glm::distance(line_point1, point) < L - radius) && (glm::distance(line_point2, point) < L - radius);
	}

	bool point_in_polygon(const glm::vec2& point, const std::vector<glm::vec2>& polygon)
	{
		if(polygon.size() < 3)
		{
			utils::err("geom::point_in_polygon(): not a polygon (size < 3)");
		}

		float a = 0;

		for(int i=0; i<(int)polygon.size(); i++)
		{
			vec2 v1 = polygon[i] - point;
			vec2 v2 = polygon[(i+1)%polygon.size()] - point;

			if(glm::distance(v1, v2) <= geom::EPSILON)
			{
				std::string erf = "point_in_polygon_error.ppm";
				SpaceGrid s(polygon, 500, 2.0f);
				s.set_circle(polygon[i], 0.3f, 3);
				s.set_circle(polygon[(i+1)%polygon.size()], 0.3f, 4);
				s.print_ppm(erf);
				utils::err("geom::point_in_polygon(): two vertices at the same place: "
					+ to_string(polygon[i]) + " and " + to_string(polygon[(i+1)%polygon.size()])
					+ "\n(see " + erf + ")");
			}
				
			if(glm::length(v1)*glm::length(v2) <= geom::EPSILON)
			{
				return true;
			}
			if(glm::distance(safe_normalize(v1), safe_normalize(v2)) <= geom::EPSILON*(glm::length(v1)+glm::length(v2)))
			{
				continue;
			}

			a += angle_plan(v1, v2);
		}

		return (abs(a) >= M_PI/2.0f);
	}

	bool point_in_polygon3D(const glm::vec3& point, const std::vector<glm::vec3>& polygon)
	{
		base2D plan = find_plan(polygon);
		return point_in_polygon(projplan(point, plan), projplan(polygon, plan));
	}
	
	bool point_in_prism(const glm::vec3& point, const prismInfo& prism)
	{
		for(uint i=0; i<4; i++)
		{
			vec3 v1 = prism.points[PRISM_FACES[i][1]] - prism.points[PRISM_FACES[i][0]];
			vec3 v2 = prism.points[PRISM_FACES[i][2]] - prism.points[PRISM_FACES[i][0]];

			vec3 normalface = glm::cross(v1, v2);
			vec3 to_last = prism.points[i] - point;

			if(glm::dot(to_last, normalface)*glm::dot(normalface, prism.points[PRISM_FACES[i][0]] - point) > 0)
			{
				return false;
			}
		}

		return true;
	}

	bool space_box_collision(const spaceBox& s1, const spaceBox& s2)
	{
		return glm::all(glm::greaterThan(s1.max, s2.min) && glm::lessThan(s1.min, s2.max));
	}

	bool segment_sphere_collision(const glm::vec3& seg_start, const glm::vec3& seg_end, const sphereInfo& sphere)
	{
		vec3 middle    = (seg_start + seg_end)/2.0f;
		vec3 to_middle = middle - sphere.center;
		vec3 seg_dirN  = glm::normalize(seg_end - seg_start);
		vec3 seg_dep   = glm::proj(to_middle, seg_dirN);
		
		float depf = glm::length(seg_dep);
		float seglength = glm::distance(seg_start, seg_end);

		if(depf< geom::EPSILON)
		{
			return (glm::length(to_middle) < sphere.radius);
		}

		float clampeddep = glm::clamp(depf, -seglength/2.0f, +seglength/2.0f);
		
		vec3 dep_dir      = glm::normalize(seg_dep);
		vec3 closestpoint = middle - dep_dir*clampeddep;

		return (glm::distance(sphere.center, closestpoint) <= sphere.radius);
	}
	
	lineIntersection line_plan_intersection(const glm::vec3& line_point, const glm::vec3& line_dirN, const glm::vec3& plan_point, const glm::vec3& plan_normalN)
	{
		lineIntersection ret;

		float t = glm::dot(plan_normalN, plan_point - line_point);
		float denom = glm::dot(plan_normalN, line_dirN);

		if(abs(denom) < geom::EPSILON)
		{
			ret.intersection = false;
			return ret;
		}

		ret.intersection = true;
		ret.distance_from_start = t/denom;

		return ret;
	}


	lineIntersection line_plan_intersection(const glm::vec3& line_point1, const glm::vec3& line_point2, const glm::vec3 plan3points[3])
	{
		return line_plan_intersection(
			line_point1,
			glm::normalize(line_point2 - line_point1),
			plan3points[0],
			glm::normalize(glm::cross(plan3points[1] - plan3points[0], plan3points[2] - plan3points[0]))
		);
	}

	lineIntersection segment_plane_intersection(const glm::vec3& seg_start, const glm::vec3& seg_end, const glm::vec3& plan_point, const glm::vec3& plan_normalN)
	{
		vec3 seg_dep = seg_end - seg_start;
		lineIntersection ret = line_plan_intersection(seg_start, glm::normalize(seg_dep), plan_point, plan_normalN);
	
		if(ret.intersection == false
		|| ret.distance_from_start < 0
		|| ret.distance_from_start > glm::length(seg_dep))
		{
			ret.intersection = false;
			return ret;
		}

		return ret;
	}

	lineIntersection segment_plane_intersection(const glm::vec3& seg_start, const glm::vec3& seg_end, const glm::vec3 plan3points[3])
	{
		return segment_plane_intersection(
			seg_start,
			seg_end,
			plan3points[0],
			plan_normal(plan3points)
		);
	}

	glm::vec3 plan_normal(const glm::vec3 plan3points[3])
	{
		return glm::normalize(glm::cross(plan3points[1] - plan3points[0], plan3points[2] - plan3points[0]));
	}

	// inside_point is a point on the inside of the plan (needed for normal)
	glm::vec3 plan_normal(const glm::vec3 plan3points[3], const glm::vec3& inside_point)
	{
		vec3 ret = plan_normal(plan3points);

		if(glm::dot(plan3points[0]-inside_point, ret) < 0)
		{
			ret = -ret;
		}

		return ret;
	}

	glm::vec3 plan_normal_ext(const glm::vec3 plan3points[3], const glm::vec3& ext_point)
	{
		vec3 ret = plan_normal(plan3points);

		if(glm::dot(plan3points[0]-ext_point, ret) > 0)
		{
			ret = -ret;
		}

		return ret;
	}

	collisionImpact impact_from_line_inter(const glm::vec3& seg_start, const glm::vec3& seg_end, const lineIntersection& inter, const glm::vec3& impact_normalN)
	{
		collisionImpact ret;

		if(!(inter.intersection))
		{
			ret.impact = false;
		}
		else
		{
			vec3 dirsegN = glm::normalize(seg_end - seg_start);

			ret.impact   = true,
			ret.distance = inter.distance_from_start,
			ret.point    = seg_start + dirsegN*inter.distance_from_start,
			ret.normalN  = impact_normalN;
		}

		return ret;
	}

	collisionImpact point_plane_impact(const glm::vec3& point, const glm::vec3& dirN, float d, const infinitePlan& plan)
	{
		lineIntersection inter = line_plan_intersection(point, dirN, plan.point, plan.normalN);
	
		if(inter.intersection == false
		|| inter.distance_from_start < 0
		|| inter.distance_from_start > d)
		{
			return NO_IMPACT;
		}

		return collisionImpact{
			.impact   = true,
			.distance = inter.distance_from_start,
			.point    = point + dirN*inter.distance_from_start,
			.normalN  = plan.normalN
		};
	}


	collisionImpact segment_plane_impact(const glm::vec3& seg_start, const glm::vec3& seg_end, const glm::vec3 plan3points[3], const glm::vec3& inside_point)
	{
		lineIntersection inter = segment_plane_intersection(seg_start, seg_end, plan3points);
		
		return impact_from_line_inter(seg_start, seg_end, inter, plan_normal(plan3points, inside_point)); 
	}

	collisionImpact segment_triangle_impact(const glm::vec3& seg_start, const glm::vec3& seg_end, const glm::vec3 plan3points[3], const glm::vec3& inside_point)
	{
		auto ret = segment_plane_impact(seg_start, seg_end, plan3points, inside_point);

		ret.impact = ret.impact && segment_triangle_collision(seg_start, seg_end, plan3points);
		
		return ret;
	}

	collisionImpact sphere_plan_impact(const sphereInfo& sphere, const infinitePlan& plan, const glm::vec3& dirN, float d)
	{
		vec3 backsphere = sphere.center - plan.normalN*sphere.radius;

		return point_plane_impact(backsphere, dirN, d, plan);
	}

	bool circle_segment_intersection(const circleInfo& circle, const Pair<glm::vec2>& segment)
	{
		bool insides[2] = {
			circle.inside(segment[0]),
			circle.inside(segment[1])
		};

		if(insides[0] == insides[1])
		{
			if(insides[0])
			{
				return false;
			}
			else
			{
				if(circle.inside(proj_point_on_segment(segment[0], segment[1], circle.center)))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		else
		{
			return true;
		}
	}	

	glm::vec3 prism_face_normal(const prismInfo& prism, uint face)
	{
		vec3 points[3] = {
			prism.points[PRISM_FACES[face][0]],
			prism.points[PRISM_FACES[face][1]],
			prism.points[PRISM_FACES[face][2]]
		};

		return plan_normal(points, prism.points[face]);
	}

	collisionImpact first_impact(const collisionImpact& i1, const collisionImpact& i2)
	{
		if(!(i1.impact))
		{
			return i2;
		}
		if(!(i2.impact))
		{
			return i1;
		}

		if(i1.distance <= i2.distance)
		{
			return i1;
		}
		else
		{
			return i2;
		}
	}

	collisionImpact first_impact_epsilonkeep(const collisionImpact& i1, const collisionImpact& i2, float espilon)
	{
		if(!(i1.impact))
		{
			// std::cout<<"Return i2 woth distance "<<i2.distance<<std::endl;
			return i2;
		}
		if(!(i2.impact))
		{
			// std::cout<<"Return i1 woth distance "<<i1.distance<<std::endl;
			return i1;
		}

		if(i1.distance <= i2.distance + espilon)
		{
			return i1;
		}
		else
		{
			return i2;
		}
	}


	collisionImpact opposite_impact(const collisionImpact& impact)
	{
		return collisionImpact{
			.impact   = impact.impact,
			.distance = impact.distance,
			.point    = impact.point + impact.normalN*impact.distance,
			.normalN  = -impact.normalN
		};
	}

	// bool:collision, and the closest to segstart and the closest to segend
	std::tuple<bool, glm::vec3, glm::vec3> line_sphere_intersection(const sphereInfo& sphere, const glm::vec3& segstart, const glm::vec3& segend)
	{
		vec3 online = proj_point_on_line(segstart, segend, sphere.center);
		vec3 dirN = safe_normalize(segend - segstart);
		
		float toline = glm::distance(sphere.center, online);
		float d = sphere.radius*sphere.radius - toline*toline;
		
		if(d < 0)
		{
			return {false, vec3(), vec3()};
		}
		else
		{
			d = sqrt(d);

			return {
				true,
				online - dirN*d,
				online + dirN*d
			};
		}
	}	

	collisionImpact sphere_segment_impact(const sphereInfo& sphere, const Segment3D& seg, const glm::vec3& move)
	{
		collisionImpact ret = NO_IMPACT;

		vec4 sm = safe_normalize_lenght(move);

		ret = first_impact(ret, sphere_point_impact(sphere, seg.first , move));
		ret = first_impact(ret, sphere_point_impact(sphere, seg.second, move));
		ret = first_impact(ret, sphere_segment_impact_noextrm(sphere, seg.first, seg.second, vec3(sm.x, sm.y, sm.z), sm.w));

		return ret;
	}

	collisionImpact sphere_point_impact(const sphereInfo& sphere, const glm::vec3& point, const glm::vec3& move)
	{
		using namespace std;

		if(distance_point_segment(sphere.center, sphere.center + move, point) > sphere.radius - EPSILON)
		{
			return NO_IMPACT;
		}
		else
		{
			auto intersect = line_sphere_intersection(sphere, point, point-move);

			// possible due to floating point precision
			if(std::get<0>(intersect) == false)
			{
				return NO_IMPACT;
			}

			vec3 dirN = safe_normalize(move);
			float d = glm::distance(std::get<1>(intersect), point);

			return collisionImpact{
				.impact   = true,
				.distance = d,
				.point    = point,
				.normalN  = safe_normalize((sphere.center + dirN*d)-point)
			};
		}
	}


	glm::vec3 symmetrical(const glm::vec3& v, const glm::vec3& axeN)
	{
		return v - axeN*glm::dot(v, axeN)*2.0f;
	}

	bool segment_triangle_collision(const glm::vec3& seg_start, const glm::vec3& seg_end, const glm::vec3 triangle[3])
	{
		lineIntersection inter = segment_plane_intersection(seg_start, seg_end, triangle); 

		if(!(inter.intersection))
		{
			return false;
		}

		vec3 seg_dirN = glm::normalize(seg_end - seg_start);
		vec3 inter_point = seg_start + seg_dirN*inter.distance_from_start;
	
		return point_in_triangle(inter_point, triangle[0], triangle[1], triangle[2]);
	}

	spaceBox moving_spacebox(const spaceBox& space_box, const glm::vec3& move)
	{
		spaceBox ret = space_box;

		ret.min += glm::min(move, vec3(0));
		ret.max += glm::max(move, vec3(0));
	
		return ret;
	}

	std::vector<glm::vec2> projplan(const std::vector<glm::vec3>& v)
	{
		std::vector<glm::vec2> v2;

		for(const auto& p : v)
		{
			v2.push_back(projplan(p));
		}

		return v2;
	}

	std::vector<glm::vec2> projplan(const std::vector<glm::vec3>& v, const base2D& plan)
	{
		std::vector<glm::vec2> v2;

		for(const auto& p : v)
		{
			v2.push_back(projplan(p, plan));
		}

		return v2;
	}

	std::vector<glm::vec3> unprojplan(const std::vector<glm::vec2>& v, float y)
	{
		std::vector<glm::vec3> v3;

		for(const auto& p : v)
		{
			v3.push_back(unprojplan(p, y));
		}

		return v3;
	}


	std::vector<triIndex> cycle_triangulation(const std::vector<glm::vec2>& cycle)
	{
		#ifdef UTILS_DEBUG
		if(!trigo_way(cycle))
		{
			std::cout<<"Little debug before crash:"<<std::endl;
			utils::print_vector(cycle);
			std::string erf = "cycle_triangulation_error.ppm";
			SpaceGrid::print_polygon(erf, cycle, 500);
			utils::err("geom::cycle_triangulation(): cycle is not in trigonometric order (see " + erf + ")");
		}
		#endif

		#ifdef UTILS_DEBUG
		if(!is_cycle(cycle))
		{
			std::string erf = "cycle_triangulation_error.ppm";
			SpaceGrid::print_polygon(erf, cycle, 500);
			utils::err("geom::cycle_triangulation(): cycles is not a cycle (see " + erf + ")");
		}
		#endif

		std::vector<triIndex> ret;
		std::vector<glm::vec2> cy = cycle;
		// cy -> cycle
		std::vector<uint> indexing = utils::uirange(0, cycle.size()-1);

		for(uint i=0; i<cycle.size()-3; i++)
		{
			triIndex corner = find_cycle_corner(cy);

			ret.push_back({
				indexing[corner.i1],
				indexing[corner.i2],
				indexing[corner.i3]
			});


			for(uint j=corner.i2; j<cy.size()-1; j++)
			{
				cy[j] = cy[j+1];
				indexing[j] = indexing[j+1];
			}
			cy.pop_back();
		}

		if(cy.size() != 3)
		{
			utils::err("cycle_triangulation(): incorrect algorithm =P");
		}

		ret.push_back({
			indexing[0],
			indexing[1],
			indexing[2]
		});

		return ret;
	}

	#warning NOT ACCURATE (is is possible not to find one with this algorithm)
	glm::uvec2 rubban_diameter_segment(const std::vector<glm::vec2>& points, const std::vector<uint>& outer_cycle, const std::vector<uint>& inner_cycle)
	{
		uvec2 conection = uvec2(0,0);
		bool collision = true;

		for(uint i1=0; i1<inner_cycle.size(); i1++)
		{
			conection.x = i1;

			for(uint i=0; i<inner_cycle.size(); i++)
			{
				collision = false;

				for(uint j=0;  j<inner_cycle.size(); j++)
				{
					uint jn = (j+1)%inner_cycle.size();

					if(j==i || jn==i)
					{
						continue;
					}

					if(geom::segment_segment_intersection(points[outer_cycle[conection.x]], points[inner_cycle[i]], points[inner_cycle[j]], points[inner_cycle[jn]]).intersection)
					{
						// cout<<"0-"<<i<<": collision with "<<j<<"-"<<jn<<endl;
						// cout<<glm::to_string(points[outer_cycle[conection.x]])<<"-"<<glm::to_string(points[inner_cycle[i]])<<": collision with "<<glm::to_string(points[inner_cycle[j]])<<"-"<<glm::to_string(points[inner_cycle[jn]])<<endl;
						collision = true;
						break;
					}
				}
				for(uint j=0;  j<outer_cycle.size(); j++)
				{
					uint jn = (j+1)%outer_cycle.size();

					if(j==conection.x || jn==conection.x)
					{
						continue;
					}

					if(geom::segment_segment_intersection(points[outer_cycle[conection.x]], points[inner_cycle[i]], points[outer_cycle[j]], points[outer_cycle[jn]]).intersection)
					{
						collision = true;
						break;
					}
				}	

				if(!collision)
				{
					conection.y = i;
					break;
				}		
			}

			if(!collision)
				break;
		}

		if(collision)
		{
			std::string erf = "rubban_diameter_segment_error.ppm";
			SpaceGrid s(utils::select_indexed(points, outer_cycle), 500, 2.0f);
			s.set_empty_polygon_doted(utils::select_indexed(points, inner_cycle), 3, 4);
			s.print_ppm(erf);
			utils::err("geom::rubban_diameter_segment(): Impossible: did not find any linking segment between the two faces\n(see "
				+ erf + ")");
		}
		
		return conection;
	}

	glm::vec3 trigo_bissectriceN(const std::vector<glm::vec3>& v, uint index, const glm::vec3& plan_normal)
	{
		if(index >= v.size())
		{
			utils::err("geom::trigo_bissectriceN(): index out oof range");
		}
		uint N = v.size();
		uint h = (index + N - 1)%N;
		uint j = (index + 1)%N;
		return trigo_bissectriceN(v[h], v[index], v[j], plan_normal);
	}

	bool intersection(const std::vector<glm::vec2>& cycle1, const std::vector<glm::vec2>& cycle2)
	{
		for(uint i=0; i<cycle1.size(); i++)
		{
			for(uint j=0; j<cycle2.size(); j++)
			{
				if(segment_segment_intersection(
						{cycle1[i], cycle1[(i+1)%(cycle1.size())]},
						{cycle2[j], cycle2[(j+1)%(cycle2.size())]}
					).intersection)
				{
					return true;
				}
			}
		}
		return false;
	}

	bool strong_intersection(const std::vector<glm::vec2>& cycle1, const std::vector<glm::vec2>& cycle2)
	{
		for(uint i=0; i<cycle1.size(); i++)
		{
			for(uint j=0; j<cycle2.size(); j++)
			{
				Segment2D s1 = {cycle1[i], cycle1[(i+1)%(cycle1.size())]};
				Segment2D s2 = {cycle2[j], cycle2[(j+1)%(cycle2.size())]};

				if(strong_segment_segment_intersection(s1, s2).intersection)
				{
					return true;
				}
			}
		}
		return false;
	}

	std::vector<triIndex> rubban_triangulation(const std::vector<glm::vec2>& points, std::vector<uint> outer_cycle, std::vector<uint> inner_cycle)
	{
		if(outer_cycle.size() < 3 || inner_cycle.size() < 3)
		{
			utils::err("geom::rubban_triangulation(): one of the cycle is not a cycle");
		}

		// PUTTING IN RIGHT WAY THE CYCLES
		{
			#ifdef UTILS_DEBUG
			std::vector<vec2> couter = utils::select_indexed(points, outer_cycle);
			std::vector<vec2> cinner = utils::select_indexed(points, inner_cycle);

			if(!trigo_way(couter)) {utils::err("geom::rubban_triangulation(): outer_cycle not in trigo order");}
			if(!trigo_way(cinner)) {utils::err("geom::rubban_triangulation(): inner_cycle not in trigo order");}

			if(strong_intersection(couter, cinner))
			{
				spaceBox2D box = {
					couter[0],
					couter[0]
				};
				box.cover(couter);
				box.cover(cinner);

				SpaceGrid s(box.min, box.max, 500);

				s.set_empty_polygon(couter, 1);
				s.set_empty_polygon(cinner, 2);

				std::string erf = "rubban_triangulation_error.ppm";
				s.print_ppm(erf);
				utils::err("geom::rubban_triangulation_error(): cycles are intersecting (see " + erf + ")");
			}
			if(point_in_polygon(couter[0], cinner))
			{
				utils::err("geom::rubban_triangulation(): outer polygon is inside inner polygon");
			}
			#endif
		}

		// vec2(Outer, Inner)
		ivec2 conection = geom::rubban_diameter_segment(points, outer_cycle, inner_cycle); // TOFIND REAL VALUE

		std::vector<glm::vec2> cycle;
		// cycle mapping => points index 
		std::unordered_map<int, int> indices_mapping;

		for(int i=0; i<=conection.x; i++)
		{
			indices_mapping[cycle.size()] = outer_cycle[i];
			cycle.push_back(points[outer_cycle[i]]);
		}

		int j = conection.y;
		while(j != conection.y || (int)cycle.size() == conection.x+1)
		{
			indices_mapping[cycle.size()] = inner_cycle[j];
			cycle.push_back(points[inner_cycle[j]]);

			j = (j+inner_cycle.size()-1)%(inner_cycle.size());
		}
		indices_mapping[cycle.size()] = inner_cycle[conection.y];
		cycle.push_back(points[inner_cycle[conection.y]]);
		
		for(int i=conection.x; i<(int)outer_cycle.size(); i++)
		{
			indices_mapping[cycle.size()] = outer_cycle[i];
			cycle.push_back(points[outer_cycle[i]]);
		}

		std::vector<triIndex> unmapped = cycle_triangulation(cycle);
		std::vector<triIndex> ret;

		for(const triIndex& tr : unmapped)
		{
			ret.push_back({
				(uint)indices_mapping[tr.i1],
				(uint)indices_mapping[tr.i2],
				(uint)indices_mapping[tr.i3]
			});
		}

		return ret;
	}

	// partialDelargement partial_conspace_delarge(const std::vector<glm::vec2>& cycle, const std::vector<bool>& delarged_segment, float delargement, float curveprecision_distance)
	// {
	// 	if(cycle.size() < 3)
	// 	{
	// 		utils::err("geom::partial_conspace_delarge(): cycle.size() < 3");
	// 	}

	// 	partialDelargement ret;

	// 	// FILLING RET BEGIN
	// 	{
	// 		auto delarged = conspace_enlarge_cycle(cycle, -delargement, curveprecision_distance);
			
	// 		for(const auto& v : cycle)    {ret.vertices.push_back(v);}
	// 		for(const auto& v : delarged) {ret.vertices.push_back(v);}
	// 		ret.outer = utils::uirange(0, cycle.size()-1);
	// 		ret.inner = utils::uirange(cycle.size(), ret.vertices.size()-1);
	// 	}

		


	// 	return ret;
	// }

	triIndex find_cycle_corner(const std::vector<glm::vec2>& cycle)
	{
		if(cycle.size() < 3)
		{
			utils::err("geom::find_cycle_corner(): cycle.size() < 3");
		}

		#ifdef UTILS_DEBUG
		if(!trigo_way(cycle))
		{
			std::string erf = "find_cycle_corner_error.ppm";
			SpaceGrid::print_polygon(erf, cycle, 500);
			utils::err("geom::find_cycle_corner(): cycle is not in trigonometric order, (see " + erf + ")");
		}
		#endif

		// if(!trigo_way(cycle))
		// {
		// 	std::cout<<"AAAAH"<<std::endl;
		// }
		// else
		// {
		// 	std::cout<<"OUF"<<std::endl;
		// }
		
		for(uint v1=0; v1<cycle.size(); v1++)
		{
			uint v2 = (v1 + 1) % cycle.size();
			uint v3 = (v1 + 2) % cycle.size();

			vec2 to1  = cycle[v2] - cycle[v1];
			vec2 to2  = cycle[v3] - cycle[v2];

			if(geom::angle(to1, to2) < float(M_PI))
			{
				bool incycle = true;

				for(uint i=0; i<cycle.size(); i++)
				{
					if((i != v1) && (i != v2) && (i != v3))
					{
						if(point_in_triangle(cycle[i], cycle[v1], cycle[v2], cycle[v3]))
						{
							incycle = false;
							break;
						}
					}
				}

				if(incycle)
				{
					return {v1, v2, v3};
				}
			}
		}

		std::string svec = "{";
		for(const glm::vec2& v : cycle)
		{
			svec += glm::to_string(v) + ", ";
		}
		svec += "}";

		std::string erf = "geom_nocycle_error.ppm";
		SpaceGrid::print_polygon(erf, cycle, 500);
		utils::err("geom::find_cycle_corner(): cycle of size " + std::to_string(cycle.size())
			+  " is not a cycle :\n" + svec + "\n(see " + erf + ")");

		return {};
	}

	base2D find_trigo_plan(const std::vector<glm::vec3>& cycle)
	{
		base2D ret = find_plan(cycle);
		std::vector<vec2> projected = projplan(cycle, ret);

		if(!trigo_way(projected))
		{
			ret.diryN *= -1.0f;
		}

		return ret;
	}

	base2D find_plan(const std::vector<glm::vec3>& cycle, const glm::vec3& zpositive)
	{
		if(cycle.size() < 3)
		{
			utils::err("geom::find_plan(): cycle of length < 3: " + std::to_string(cycle.size()));
		}

		glm::vec3 p1 = cycle[0];

		uint imax = 1;
		float dmax = 0;
		for(uint i=1; i<cycle.size();i++)
		{
			float d = glm::distance(p1, cycle[i]);
			if(d > dmax)
			{
				dmax = d;
				imax = i;
			}
		}

		vec3 p2 = cycle[imax];

		dmax = 0;
		for(uint i=0; i<cycle.size();i++)
		{
			float d1 = glm::distance(p1, cycle[i]);
			float d2 = glm::distance(p2, cycle[i]);
			float d  = std::min(d1, d2);

			if(d > dmax)
			{
				dmax = d;
				imax = i;
			}
		}

		vec3 p3 = cycle[imax];

		if(p1 == p2 || p1 == p3 || p3 == p2 || glm::length(glm::cross(p2-p1, p3-p1)) < EPSILON)
		{
			utils::err("geom::find_plan(): cycle is not on a plan");
		}

		base2D ret;
		ret.dirxN = glm::normalize(p2-p1);
		ret.diryN = p3-p2;
		ret.diryN = glm::normalize(ret.diryN - ret.dirxN*glm::dot(ret.diryN, ret.dirxN));
		ret.origin = p1;

		if(glm::dot(zpositive, glm::cross(ret.dirxN, ret.diryN)) < 0)
		{
			ret.diryN = -ret.diryN;
		}

		if(glm::length(ret.diryN) < geom::EPSILON)
		{
			utils::err("geom::find_plan(): impossible in the algorithm happened");
		}

		// if(abs(ret.dirxN.y) < geom::EPSILON && abs(ret.diryN.y) < geom::EPSILON)
		// {
		// 	return {
		// 		vec3(1,0,0),
		// 		vec3(0,0,1)
		// 	};
		// }

		// using namespace std;
		// cout<<"x: "<<to_string(ret.dirxN)<<", y: "<<to_string(ret.diryN)<<", dot="<<glm::dot(ret.dirxN, ret.diryN)<<endl;

		// vec3 dirzN = glm::cross(ret.dirxN, ret.diryN);
		// float l = glm::length(dirzN);
		// if(l < 1.0f - geom::EPSILON*10.0f || l > 1.0f + geom::EPSILON*10.0f)
		// {
		// 	utils::err("geom::find_plan():  plan not orthogonal");
		// }

		// for(uint j=1; j<cycle.size(); j++)
		// {
		// 	if(abs(glm::dot(cycle[j]-p1, dirzN)) > geom::EPSILON)
		// 	{
		// 		utils::err("geom::find_plan(): a point is not on the plan");
		// 	}
		// }

		return ret;
	}

	std::vector<glm::vec2> proj_cycle_on_its_plan(const std::vector<glm::vec3>& cycle)
	{
		base2D plan = geom::find_trigo_plan(cycle);
		return projplan(cycle, plan);
	}

	triIndex find_cycle_corner(const std::vector<glm::vec3>& cycle)
	{
		return find_cycle_corner(geom::proj_cycle_on_its_plan(cycle));
	}

	std::vector<triIndex> cycle_triangulation(const std::vector<glm::vec3>& cycle)
	{
		return cycle_triangulation(geom::proj_cycle_on_its_plan(cycle));
	}

	std::vector<triIndex> rubban_triangulation(const std::vector<glm::vec3>& points, const std::vector<uint>& outer_cycle, const std::vector<uint>& inner_cycle)
	{
		if(outer_cycle.size() < 3 || inner_cycle.size() < 3)
		{
			utils::err("geom::rubban_triangulation(): outer_cycle or inner_cycle has a size < 3");
		}

		base2D b = find_trigo_plan(utils::select_indexed(points, outer_cycle));
		#ifdef UTILS_DEBUG
		if(!trigo_way(projplan(utils::select_indexed(points, inner_cycle), b)))
		{
			std::string erf = "rubban_triangulation.ppm";
			SpaceGrid sp(geom::projplan(utils::select_indexed(points, outer_cycle), b), 500, 1.0f);
			sp.set_empty_polygon_doted(geom::projplan(utils::select_indexed(points, inner_cycle), b), 3,4);
			sp.set_circle(geom::projplan(points[outer_cycle[0]], b), 2.0, 6);
			sp.set_circle(geom::projplan(points[inner_cycle[0]], b), 2.0, 6);
			sp.set_circle(geom::projplan(points[outer_cycle[1]], b), 1.0, 7);
			sp.set_circle(geom::projplan(points[inner_cycle[1]], b), 1.0, 7);
			sp.set_thin_line({geom::projplan(points[outer_cycle[0]], b), geom::projplan(points[outer_cycle[1]], b)}, 5);
			sp.set_thin_line({geom::projplan(points[inner_cycle[0]], b), geom::projplan(points[inner_cycle[1]], b)}, 5);
			sp.print_ppm(erf);
			utils::err("geom::rubban_triangulation: outer cycle and inner cycle don't have the same trigo order on their common plan\n(see "
				+ erf + ")");
		}
		#endif
		return rubban_triangulation(geom::projplan(points, b), outer_cycle, inner_cycle);
	}

	bool trigo_turn(const glm::vec2& AB, const glm::vec2& BC)
	{
		return (glm::dot(trigo_normal(AB), BC) >= 0);
	}

	bool trigo_way(const std::vector<glm::vec2>& cycle)
	{
		uint hi = highest(cycle);

		uint prev = hi;
		uint next = hi;

		do
		{
			prev = (prev + cycle.size() - 1)%cycle.size();
		}
		while(prev != hi && glm::distance(cycle[hi], cycle[prev]) < EPSILON);
		
		do
		{
			next = (next + 1)%cycle.size();
		}
		while(next != hi && glm::distance(cycle[hi], cycle[next]) < EPSILON);


		if(prev == hi || next == hi)
		{
			utils::err("geom::trigo_way(): cycle too smal (contained in EPSILON)");
		}

		return trigo_turn(cycle[hi] - cycle[prev], cycle[next] - cycle[hi]);
	}

	float cycle_mindistance(const std::vector<glm::vec3>& points, const glm::vec3& source)
	{
		std::vector<glm::vec2> pts = projplan(points);

		return cycle_mindistance(pts, projplan(source));
	}
	
	float cycle_mindistance(const std::vector<glm::vec2>& points, const glm::vec2& source)
	{
		if(points.size() == 0)
		{
			utils::err("geom::cycle_mindistance(): no points");
		}

		float dmin = glm::distance(source, points[0]);

		for(uint i=0; i<points.size(); i++)
		{
			uint j = (i+1)%points.size();
			float d = distance_point_segment(points[i], points[j], source);
			
			if(d < dmin)
			{
				dmin = d;
			}
		}

		return dmin;
	}

	float diameter(const std::vector<glm::vec2>& points)
	{
		if(points.size() == 0)
		{
			utils::err("geom::diagonal(): vector is empty");
		}

		float d = 0;

		for(const glm::vec2& p : points)
		{
			d = std::max(d, maxdistance(p, points));
		}

		return d;
	}

	float diameter(const std::vector<glm::vec3>& points)
	{
		if(points.size() == 0)
		{
			utils::err("geom::diagonal(): vector is empty");
		}

		float d = 0;

		for(const glm::vec3& p : points)
		{
			d = std::max(d, maxdistance(p, points));
		}

		return d;
	}

	float maxdistance(const glm::vec2& source, const std::vector<glm::vec2>& points)
	{
		if(points.size() == 0)
		{
			utils::err("geom::maxdistance(): vector is empty");
		}

		float d = 0;

		for(const glm::vec2& p : points)
		{
			d = std::max(d, glm::distance(source, p));
		}

		return d;
	}	

	float maxdistance(const glm::vec3& source, const std::vector<glm::vec3>& points)
	{
		if(points.size() == 0)
		{
			utils::err("geom::maxdistance(): vector is empty");
		}

		float d = 0;

		for(const glm::vec3& p : points)
		{
			d = std::max(d, glm::distance(source, p));
		}

		return d;
	}	

	float distance(const glm::vec3& source, const std::vector<glm::vec3>& points)
	{
		if(points.size() == 0)
		{
			utils::err("geom::distance(): vector is empty");
		}

		float d = glm::distance(source, points[0]);

		for(const glm::vec3& p : points)
		{
			d = std::min(d, glm::distance(source, p));
		}

		return d;
	}

	float distance(const glm::vec2& source, const std::vector<glm::vec2>& points)
	{
		if(points.size() == 0)
		{
			utils::err("geom::distance(): vector is empty");
		}

		float d = glm::distance(source, points[0]);

		for(const glm::vec2& p : points)
		{
			d = std::min(d, glm::distance(source, p));
		}

		return d;
	}

	float cycle_maxdistance(const std::vector<glm::vec2>& points, const glm::vec2& source)
	{
		if(points.size() == 0)
		{
			utils::err("geom::cycle_maxdistance(): no points");
		}

		float dmax = glm::distance(source, points[0]);

		for(uint i=0; i<points.size(); i++)
		{
			uint j = (i+1)%points.size();
			float d = distance_point_segment(points[i], points[j], source);
			
			if(d > dmax)
			{
				dmax = d;
			}
		}

		return dmax;
	}

	glm::vec2 proj_point_on_line(const glm::vec2& line_point1, const glm::vec2& line_point2, const glm::vec2& point)
	{
		vec3 ret = proj_point_on_line(vec3(line_point1, 0), vec3(line_point2, 0), vec3(point, 0));
		return vec2(ret.x, ret.y);
	}

	glm::vec3 proj_point_on_line(const glm::vec3& line_point1, const glm::vec3& line_point2, const glm::vec3& point)
	{
		vec3 dirN = safe_normalize(line_point2 - line_point1);

		float d = glm::dot(dirN, point-line_point1);

		return line_point1 + dirN*d;
	}

	glm::vec2 proj_point_on_segment(const glm::vec2& seg_start, const glm::vec2& seg_end, const glm::vec2& point)
	{
		return proj_point_on_segment(vec3(seg_start, 0), vec3(seg_end, 0), vec3(point, 0));
	}

	glm::vec3 proj_point_on_segment(const glm::vec3& seg_start, const glm::vec3& seg_end, const glm::vec3& point)
	{
		float segd = glm::distance(seg_start, seg_end);
		vec3 p = proj_point_on_line(seg_start, seg_end, point);

		if(glm::distance(seg_start, p) < segd && glm::distance(seg_end, p) < segd)
		{
			return p;
		}
		else
		{
			if(glm::distance(seg_start, point) < glm::distance(seg_end, point))
			{
				return seg_start;
			}
			else
			{
				return seg_end;
			}
		}
	}

	glm::vec3 trigo_crossing_problem(const glm::vec3& l1p1, const glm::vec3& l1p2, const glm::vec3& l2p1, const glm::vec3& l2p2, const glm::vec3& point)
	{
		return geom::unprojplan(geom::trigo_crossing_problem(geom::projplan(l1p1), geom::projplan(l1p2), geom::projplan(l2p1), geom::projplan(l2p2), geom::projplan(point)), point.y);
	}
	
	glm::vec2 trigo_crossing_problem(const glm::vec2& l1p1, const glm::vec2& l1p2, const glm::vec2& l2p1, const glm::vec2& l2p2, const glm::vec2& point)
	{
		vec2 l1dir = l1p2 - l1p1;
		vec2 l2dir = l2p2 - l2p1;

		float l1length = glm::length(l1dir);
		float l2length = glm::length(l2dir);
		if(l1length <= geom::EPSILON)
		{
			utils::err("trigo_crossing_problem() l1 is a point");
		}
		if(l2length <= geom::EPSILON)
		{
			utils::err("trigo_crossing_problem() l2 is a point");
		}

		vec2 normalN = geom::trigo_normal(l1dir/l1length);
		// vec2 dirproj = l2dir/l2length;

		lineIntersection inter = geom::line_line_intersection(point, point+normalN*1.0f, l2p1, l2p2);

		if(!inter.intersection)
		{
			utils::err("geom::trigo_crossing_problem(): no intersection");
		}

		return point + normalN*inter.distance_from_start;
	}

	std::pair<glm::vec3, glm::vec3> closest_point(const directedLine3D& l1, const directedLine3D& l2)
	{
		float A1 = glm::dot(l1.dirN, (l2.point - l1.point));
		float A2 = glm::dot(l2.dirN, (l2.point - l1.point));
		float D  = glm::dot(l1.dirN, l2.dirN);

		if(abs(D*D-1.0f) <= geom::EPSILON)
		{
			vec3 normal  = glm::cross((l2.point - l1.point), l1.dirN);
			vec3 toline2 = safe_normalize(glm::cross(l1.dirN, normal));
			return {l1.point, l1.point + safe_proj(l2.point - l1.point, toline2)};
		}

		float t1 = (A1 - D*A2)/(1.0f - D*D);
		float t2 = (D*A1 -   A2)/(1.0f - D*D);

		return {l1.point + l1.dirN*t1, l2.point + l2.dirN*t2};
	}

	collisionImpact sphere_line_impact(const sphereInfo& sphere, const directedLine3D& line, const glm::vec3& dirN)
	{
		const auto& ds = dirN;
		const auto& dl = line.dirN;
		const auto& Ps = sphere.center;
		const auto& Pl = line.point;

		vec3  V  = Ps - Pl;
		double V2 = glm::dot(V, V);
		double D  = glm::dot(ds, dl);
		double Js = glm::dot(V, ds);
		double Jl = glm::dot(V, dl);
		double R  = sphere.radius;

		double X = 1.0 - D*D;
		double Y = 2.0*(Js-Jl*D);
		double Z = V2-R*R-Jl*Jl;

		double delta = Y*Y-4.0*X*Z;

		if(delta < 0 || abs(X) < 0.0001)
		{
			return NO_IMPACT;
		}

		float ts = (-Y-sqrt(delta))/(2.0*X);
		float tl = Jl + D*ts;

		// std::cout<<"ts: "<<ts<<", X:"<<X<<", delta:"<<delta<<", Y:"<<Y<<std::endl;

		collisionImpact ret;
		ret.impact   = true;
		ret.distance = ts;
		ret.point    = Pl + dl*tl;
		ret.normalN  = safe_normalize((Ps + ds*ts) - (Pl + dl*tl));
		
		// effects::debug_point(ret.point , 0.1, vec4(0,0,1,1));
		// effects::debug_point(closests.second, 0.1, vec4(0,1,0,1));

		return ret;
	}

	collisionImpact line_line_impact(const directedLine3D& l1, const directedLine3D& l2, const glm::vec3& dirN)
	{
		vec3 plan_normalN = glm::cross(l1.dirN, dirN);
		vec3 normalN = glm::cross(l1.dirN, l2.dirN);

		if(glm::length(normalN) < EPSILON)
		{
			return NO_IMPACT;
		}
		if(glm::length(plan_normalN) < EPSILON)
		{
			auto almostinter = closest_point(l1, l2);

			if(glm::distance(almostinter.first, almostinter.second) < EPSILON)
			{
				return collisionImpact{
					.impact   = true,
					.distance = 0,
					.point    = almostinter.second,
					.normalN  = -dirN
				};
			}
			else
			{
				return NO_IMPACT;
			}
		}

		plan_normalN = glm::normalize(plan_normalN);
		normalN      = glm::normalize(normalN);

		if(glm::dot(normalN, dirN) > 0)
		{
			normalN = -normalN;
		}
		
		lineIntersection inter = line_plan_intersection(l2.point, l2.dirN, l1.point, plan_normalN);
		
		if(!inter.intersection)
		{
			return NO_IMPACT;
		}

		collisionImpact ret;
		ret.impact  = true;
		ret.point   = (l2.point + l2.dirN*inter.distance_from_start);
		ret.normalN = normalN;

		vec3 tointer = ret.point - l1.point;
		vec3 projvec = safe_normalize(glm::cross(glm::cross(l1.dirN, dirN), l1.dirN));

		ret.distance = glm::dot(tointer, projvec);

		return ret;
	}

	// Not accurate: do not consider points extremities
	collisionImpact sphere_segment_impact_noextrm(const sphereInfo& sphere, const glm::vec3& segstart, const glm::vec3& segend, const glm::vec3& dirN, float d)
	{
		collisionImpact ret = sphere_line_impact(sphere, {segstart, safe_normalize(segend - segstart)}, dirN);
		// std::cout<<"computed: "<<ret.distance<<std::endl;

		if(ret.impact == false || ret.distance < 0 || ret.distance > d)
		{
			// std::cout<<"no_impact"<<std::endl;
			return NO_IMPACT;
		}

		if(!online_point_on_segment({segstart, segend}, ret.point))
		{
			// std::cout<<"no_impact"<<std::endl;
			// std::cout<<"!point_on_segment :"
			// <<to_string(ret.point)<<" not on ("
			// <<to_string(segstart)<<"<->"<<to_string(segend)
			// <<")"<<std::endl;
			return NO_IMPACT;
		}
		else
		{
			// std::cout<<"ON IT !: "<<ret.to_string()<<std::endl;
			return ret;
		}
	}

	float cross2D(const glm::vec2& v1, const glm::vec2& v2)
	{
		return glm::dot(trigo_normal(v1), v2);
	}
	
	void approximate_cycle(std::vector<glm::vec2>& cycle, float min_distance, const glm::vec2* center_ptr)
	{
		if(cycle.size() <= 3)
		{
			return;
		}

		vec2 center;
		bool trigow = trigo_way(cycle);

		if(center_ptr == NULL)
		{
			center = max_inscribed_cycle(cycle).center;
		}
		else
		{
			center = *center_ptr;
		}
		// Can be optimized
		bool end = false;
		while(!end && cycle.size()>3)
		{
			end = true;

			for(uint i=0; i<cycle.size(); i++)
			{
				uint j = (i+1)%cycle.size();
				uint k = (i+2)%cycle.size();
				
				if(glm::distance(cycle[i], cycle[j]) + glm::distance(cycle[j], cycle[k]) < min_distance)
				{
					if(!point_in_triangle(center, cycle[i], cycle[j], cycle[k]))
					{
						cycle.erase(cycle.begin()+j);
						end = false;
						break;
					}
				}
			}	
		}

		cycle = remove_tooclose(cycle);

		if(trigo_way(cycle) != trigow)
		{
			utils::invert(cycle);
		}
	}

	circleInfo circumscribed_circle(const glm::vec2 points[3])
	{
		vec2 v1 = points[1] - points[0];
		vec2 v2 = points[2] - points[0];

		vec2 m1 = geom::middle(points[0], points[1]);
		vec2 m2 = geom::middle(points[0], points[2]);
		vec2 c1 = m1+geom::trigo_normal(v1);
		vec2 c2 = m2+geom::trigo_normal(v2);

		auto inter = geom::line_line_intersection(m1, c1,
												  m2, c2);
	
		if(!inter.intersection)
		{
			return {vec2(0), -1.0f};
		}
		else
		{
			circleInfo ret;

			ret.center = m1 + inter.distance_from_start*glm::normalize(c1-m1);
			ret.radius = glm::distance(ret.center, points[0]);

			return ret;
		}
	}

	circleInfoPlanY circumscribed_circle(const glm::vec3 points[3])
	{
		vec2 points2D[] = {geom::projplan(points[0]),
						   geom::projplan(points[1]),
						   geom::projplan(points[2])};

		auto center2D = circumscribed_circle(points2D);
	
		circleInfoPlanY ret;

		ret.center = geom::unprojplan(center2D.center, points[0].y); 
		ret.radius = center2D.radius;

		return ret;
	}

	lineIntersection line_line_intersection(const Segment2D& l1, const Segment2D& l2)
	{
		return line_line_intersection(l1.first, l1.second, l2.first, l2.second);
	}

	lineIntersection line_line_intersection(const glm::vec2& line1_p1, const glm::vec2& line1_p2, const glm::vec2& line2_p1, const glm::vec2& line2_p2)
	{
		vec2 d1N = (line1_p2 - line1_p1);
		vec2 d2N = (line2_p2 - line2_p1);

		if(length(d1N) < geom::EPSILON || length(d2N) < geom::EPSILON)
		{
			return {false, 0.0f};
			// utils::err("geom::line_line_intersection(): segment has no length");
		}

		d1N = glm::normalize(d1N);
		d2N = glm::normalize(d2N);

		return geom::line_line_intersection(directedLine2D{line1_p1, d1N}, directedLine2D{line2_p1, d2N});
	}

	lineIntersection line_line_intersection(const directedLine2D& l1, const directedLine2D& l2)
	{
		float denum = cross2D(l2.dirN, l1.dirN);

		if(abs(denum) < geom::EPSILON)
		{
			return {false, 0};
		}

		return {true, cross2D(l1.point - l2.point, l2.dirN)/denum};
	}


	lineIntersection line_segment_intersection(const directedLine2D& l, const Segment2D& segment)
	{
		directedLine2D l2 = {
			segment.first,
			safe_normalize(segment.second-segment.first)
		};

		lineIntersection ret = line_line_intersection(l, l2);
	
		if(!ret.intersection)
		{
			return ret;
		}

		vec2 p = l.point + l.dirN*ret.distance_from_start;

		if(!geom::online_point_on_segment(segment, p))
		{
			return {false, 0};
		}

		return ret;
	}

	lineIntersection strong_segment_segment_intersection(const Segment2D& s1, const Segment2D& s2)
	{
		if(s1.first  != s2.first
		&& s1.first  != s2.second
		&& s1.second != s2.first
		&& s1.second != s2.second
		&& !(point_on_segment(s2, s1.first ) && point_on_segment(s2, s1.second))
		&& !(point_on_segment(s1, s2.first ) && point_on_segment(s1, s2.second)))
		{
			return segment_segment_intersection(s1, s2);
		}

		return {false, 0};
	}

	lineIntersection segment_segment_intersection(const glm::vec2& seg1_start, const glm::vec2& seg1_end, const glm::vec2& seg2_start, const glm::vec2& seg2_end)
	{
		lineIntersection lineinter = line_line_intersection(seg1_start, seg1_end, seg2_start, seg2_end);
		
		if(!lineinter.intersection)
		{
			return {false, 0};
		}
		else
		{
			vec2 interpoint = seg1_start + glm::normalize(seg1_end - seg1_start)*lineinter.distance_from_start;
			float seg2l = glm::distance(seg2_end, seg2_start);

			return {(
						   lineinter.distance_from_start>=0 
						&& lineinter.distance_from_start <= glm::distance(seg1_start, seg1_end)
						&& glm::distance(seg2_start, interpoint) <= seg2l
						&& glm::distance(seg2_end, interpoint) <= seg2l
					),
					lineinter.distance_from_start};
		}
	}
	
	lineIntersection segment_segment_intersection(const Segment2D& s1, const Segment2D& s2)
	{
		return segment_segment_intersection(s1.first, s1.second, s2.first, s2.second);
	}

	#warning NOT ACCURATE FOR NOW (but working in most cases)
	float cycle_bissectrice_maxdelargement(const std::vector<glm::vec2>& points)
	{
		if(points.size() < 3)
		{
			utils::err("geom::cycle_bissectrice_maxdelargement(): points.size() < 3");
		}

		float diameter = 2.1f*cycle_maxdistance(points, points[0]);
		float mind = diameter;
		uint  intercount = 0;

		for(uint i=0; i<points.size(); i++)
		{
			uint prev = (i + points.size()-1)%points.size();
			uint next = (i + 1)%points.size();

			vec2 dir1N = points[prev] - points[i];
			vec2 dir2N = points[next] - points[i];

			if(glm::length(dir1N) < geom::EPSILON || glm::length(dir2N) < geom::EPSILON)
			{
				utils::err("geom::cycle_bissectrice_maxdelargement(): two points are at the same place");
			}
			dir1N = glm::normalize(dir1N);
			dir2N = glm::normalize(dir2N);

			vec2 bisN = trigo_bissectriceN(dir2N, dir1N);

			for(uint estart=0; estart<points.size(); estart++)
			{
				uint eend = (estart + 1)%points.size();

				if(estart == i || eend == i)
				{
					continue;
				}
				
				vec2  inter = proj_point_on_segment(points[estart], points[eend], points[i]);

				if(glm::dot(inter - points[i], bisN) >= 0)
				{
					intercount ++;
					mind = std::min(mind, glm::length(inter - points[i]));
				}
			}
		}

		if(intercount < points.size())
		{
			utils::err("geom::cycle_bissectrice_maxdelargement() too low number of intersection, sould be at least one by vertex but here it is : " + std::to_string(intercount));
		}

		return mind;
	}

	spaceBase orthogonal_spacebase(const glm::vec3& vx, const glm::vec3& vy)
	{
		return spaceBase{
			vx,
			vy, 
			glm::cross(vx, vy)
		};
	}

	float cycle_bissectrice_maxdelargement(const std::vector<glm::vec3>& points)
	{
		std::vector<vec2> proj2D = projplan(points);

		return cycle_bissectrice_maxdelargement(proj2D);
	}


	float distance_point_segment(const glm::vec2& seg_start, const glm::vec2& seg_end, const glm::vec2& point)
	{
		return glm::distance(proj_point_on_segment(seg_start, seg_end, point), point);
	}

	float distance_point_segment(const glm::vec3& seg_start, const glm::vec3& seg_end, const glm::vec3& point)
	{
		return glm::distance(proj_point_on_segment(seg_start, seg_end, point), point);
	}

	float distance_segment_segment(const Segment2D& seg1, const Segment2D& seg2)
	{
		return std::min(
			std::min(distance_point_segment(seg1.first, seg1.second, seg2.first), 
					 distance_point_segment(seg1.first, seg1.second, seg2.second)),
			std::min(distance_point_segment(seg2.first, seg2.second, seg1.first),
					 distance_point_segment(seg2.first, seg2.second, seg1.second))
		);
	}

	float distance_point_polygon(const glm::vec2& point, const std::vector<glm::vec2>& polygon)
	{
		if(polygon.size() == 0)
		{
			utils::err("geom::distance_point_polygon(): polygon is empty");
		}

		float dmin = glm::distance(point, polygon[0]);

		for(uint i=0; i<polygon.size(); i++)
		{
			Segment2D s = {polygon[i], polygon[(i+1)%polygon.size()]};

			float d = distance_point_segment(s, point);
			if(d < dmin)
			{
				dmin = d;
			}
		}

		return dmin;
	}

	Parallelogram infinite_strip_intersection(const Segment2D& strip1, const Segment2D& strip2)
	{
		Parallelogram ret = {vec2(0),vec2(0),vec2(0),vec2(0)};

		vec2 normal1N   = directionN(strip1);
		vec2 normal2N   = directionN(strip2);

		vec2 dir1N = trigo_normal(normal1N);
		vec2 dir2N = trigo_normal(normal2N);

		Segment2D lines[2][2] = {
			{	
				{strip1.first , strip1.first  + dir1N},
				{strip1.second, strip1.second + dir1N}
			},
			{
				{strip2.first , strip2.first  + dir2N},
				{strip2.second, strip2.second + dir2N}
			}
		};

		lineIntersection inters[2][2];

		for(uint i=0; i<4; i++)
		{
			uint l1 = i/2;
			uint l2 = i%2;

			inters[l1][l2] = line_line_intersection(lines[0][l1], lines[1][l2]);
		
			if(!(inters[l1][l2].intersection))
			{
				return ret;
			}

			ret[i] = lines[0][l1].first + dir1N*inters[l1][l2].distance_from_start;
		}

		utils::permute_values(&(ret[2]), &(ret[3]));

		return ret;
	}

	glm::vec2 proj_point_on_line(const Segment2D& line, const glm::vec2& point)
	{
		return proj_point_on_line(line.first, line.second, point);
	}

	bool point_on_line(const Segment2D& line, const glm::vec2& point)
	{
		return (glm::distance(proj_point_on_line(line, point), point) < geom::EPSILON);
	}

	bool point_on_line(const directedLine2D& line, const glm::vec2& point)
	{
		return point_on_line(Segment2D{line.point, line.point+line.dirN}, point);
	}

	static const Segment2D NO_SEGINTER = Segment2D({vec2(0), vec2(0)});
	
	Segment2D parallelogram_line_intersection(const Parallelogram& p, const directedLine2D& l)
	{
		std::vector<glm::vec2> v;

		for(uint i=0; i<4; i++)
		{
			lineIntersection inter = line_segment_intersection({l.point, l.dirN}, {p[i], p[(i+1)%4]});
		
			if(inter.intersection)
			{
				v.push_back(l.point + l.dirN*inter.distance_from_start);
			}
			else
			{
				if(point_on_line(l, p[i]))
				{
					v.push_back(p[i]);
				}
			}
		}

		if(v.size() < 2)
		{
			return NO_SEGINTER;
		}

		for(uint i=1;  i<v.size(); i++)
		{
			if(glm::distance(v[0], v[i]) > geom::EPSILON)
			{
				return {v[0], v[i]};
			}
		}

		return NO_SEGINTER;
	}

	Segment2D sphere_tagent_equidistant_segment(const Segment2D& s1, const Segment2D& s2)
	{
		lineIntersection inter = line_line_intersection(s1.first, s1.second, s2.first, s2.second);

		if(!inter.intersection)
			return NO_SEGINTER;

		Parallelogram p = infinite_strip_intersection(s1, s2);
		
		vec2 dir1N   = directionN(s1);
		vec2 dir2N   = directionN(s2);

		vec2 bisbot = s1.first + dir1N*inter.distance_from_start;
		
		vec2 bidirN1 = bissectriceN(dir1N, dir2N);
		vec2 bidirN2 = bissectriceN(dir1N, -dir2N);

		// SpaceGrid sp(500, *xxv, 10.0f);
		// sp.set_empty_polygon({p[0], p[1], p[2], p[3]}, 5);
		// sp.set_thin_line(s1, 2);
		// sp.set_thin_line(s2, 2);
		// sp.set_circle(bisbot, 2.0f, 3);
		// sp.set_thin_line({bisbot, bisbot+bidirN1*30.0f}, 4);
		// sp.set_thin_line({bisbot, bisbot+bidirN2*30.0f}, 4);
		// sp.print_ppm("dsfljfsidfsdi.ppm");


		Segment2D ret = parallelogram_line_intersection(p, {bisbot, bidirN1});

		if(ret == NO_SEGINTER)
			ret = parallelogram_line_intersection(p, {bisbot, bidirN2});

		

		return ret;
	}

	glm::vec2 intersection_point(const directedLine2D& line, const lineIntersection& inter)
	{
		if(!(inter.intersection))
		{
			utils::err("geom::intersection_point(): no intersection");
		}

		return line.point + line.dirN*inter.distance_from_start;
	}

	glm::vec2 intersection_point(const Segment2D& s, const lineIntersection& inter)
	{
		if(!(inter.intersection))
		{
			utils::err("geom::intersection_point(): no intersection");
		}

		return intersection_point(s, inter.distance_from_start);
	}
	
	glm::vec2 intersection_point(const Segment2D& s, float distance_from_start)
	{
		float l = glm::distance(s.first, s.second);

		if(l < EPSILON)
		{
			return s.first;
		}

		return s.first + (s.second-s.first)/l*distance_from_start;
	}

	#warning NOT ACCURATE (always valid inscibed circle but it may ot be the maximum)
	circleInfo max_inscribed_cycle(const std::vector<glm::vec2>& polygon)
	{
		#ifdef UTILS_DEBUG
		if(!trigo_way(polygon))
		{
			std::string erf = "max_inscribed_cycle_error.ppm";
			SpaceGrid::print_polygon(erf, polygon, 500);
			utils::err("geom::max_inscribed_cycle(): cycle is not in trigonometric order\n(see"
				+ erf + ")");
		}
		#endif

		uint N = polygon.size();
		std::vector<Segment2D> areas;
		std::vector<vec2> centers;

		for(uint i=0; i<N; i++)
		{
			for(uint j=i+1; j<N; j++)
			{
				Segment2D s1 = {polygon[i], polygon[(i+1)%N]};
				Segment2D s2 = {polygon[j], polygon[(j+1)%N]};

				Segment2D available = sphere_tagent_equidistant_segment(s1, s2);

				// SpaceGrid s(500, polygon, 20.0f);
				// s.set_thin_line(s1.first, s1.second, 2);
				// s.set_thin_line(s2.first, s2.second, 4);
				// s.set_thin_line(available.first, available.second, 3);
				// s.print_ppm("maxinsc.ppm");

				if(!(available.first == vec2(0,0) && available.second != vec2(0,0)))
				{
					areas.push_back(available);
				}
			}
		}

		for(uint i=0; i<areas.size(); i++)
		{
			for(uint j=i+1; j<areas.size(); j++)
			{
				lineIntersection inter = segment_segment_intersection(areas[i], areas[j]);

				if(inter.intersection)
				{
					vec2 c = intersection_point(areas[i], inter);
					
					if(point_in_polygon(c, polygon))
					{
						centers.push_back(c);
					}
				}
			}
		}

		// using namespace std;

		triIndex tr = find_cycle_corner(polygon); 

		vec2  best = (polygon[tr.i1]+polygon[tr.i2]+polygon[tr.i3])/3.0f;
		float dmax = distance_point_polygon(best, polygon);

		// ADD MORE PRECSION IN CASE OF NON WORKING EXACT METHOD
		{
			vec2 barycenter = mean(polygon);
			if(point_in_polygon(barycenter, polygon))
			{
				centers.push_back(barycenter);
			}

			std::vector<triIndex> trs = cycle_triangulation(polygon);
			for(triIndex& t : trs)
			{
				centers.push_back((polygon[t.i1]+polygon[t.i2]+polygon[t.i3])/3.0f);
			}
		}

		// bool fuck = true;

		for(uint i=0; i<centers.size(); i++)
		{
			float d = distance_point_polygon(centers[i], polygon);

			if(d > dmax)
			{
				dmax = d;
				best = centers[i];
				// fuck = false;
			}
		}

	
		// SpaceGrid s(1000, polygon, 20.0f);
		// s.set_circle(best, dmax, 2);
		// for(auto& a : areas)
		// {
		// 	s.set_thin_line(a.first, a.second, 3);
		// }
		// s.print_ppm("maxinsc_" + std::to_string(N) + ".ppm");

		return {best, dmax};	
	}

	lineIntersection halfline_line_intersection(const directedLine2D& halfline, const directedLine2D& line)
	{
		auto ret = line_line_intersection(halfline, line);

		if(ret.intersection && ret.distance_from_start < 0)
		{
			ret.intersection = false;
			ret.distance_from_start = 0.0f;
		}

		return ret;
	}

	glm::vec2 min_equidistant_point(const glm::vec2& p1, const glm::vec2& p2, const glm::vec2& p1normalN, const glm::vec2& p2normalN, const glm::vec2& extdirN)
	{
		vec2 m = middle(p1, p2);

		auto inter1 = halfline_line_intersection({m, extdirN}, {p1, p1normalN});
		auto inter2 = halfline_line_intersection({m, extdirN}, {p2, p2normalN});
	
		if(!inter1.intersection && !inter2.intersection)
		{
			utils::err("geom::min_equidistant_point(): no equidistant point");
		}
		if(inter1.intersection && !inter2.intersection)
		{
			return m + extdirN*inter1.distance_from_start;
		}
		if(!inter1.intersection && inter2.intersection)
		{
			return m + extdirN*inter2.distance_from_start;
		}
		if(inter1.intersection && inter2.intersection)
		{
			if(inter1.distance_from_start > inter2.distance_from_start)
			{
				return m + extdirN*inter1.distance_from_start;
			}
			else
			{
				return m + extdirN*inter2.distance_from_start;
			}
		}

		utils::err("geom::min_equidistant_point(): IMPOSSIBLE REACHED");
		return m;
	}

	std::pair<directedLine2D, directedLine2D> equidistant_halflines(const Segment2D& s1, const Segment2D& s2)
	{
		std::pair<directedLine2D, directedLine2D> ret;

		rectriangleEntries entries = rectriangle_entries(s1, s2);
		uint N = entries.rectriangle.size();

		#warning TOCHECK NORMALIZE
		ret.first.dirN  = -glm::normalize(trigo_normal(entries.rectriangle[entries.entry1.second] - entries.rectriangle[entries.entry1.first]));
		ret.second.dirN = -glm::normalize(trigo_normal(entries.rectriangle[entries.entry2.second]  - entries.rectriangle[entries.entry2.first]));

		vec2 s1p1normalN = -trigo_normal(entries.rectriangle[entries.entry1.first] - entries.rectriangle[(entries.entry1.first+N-1)%N]);
		vec2 s1p2normalN = -trigo_normal(entries.rectriangle[entries.entry1.second]  - entries.rectriangle[(entries.entry1.second+1)%N]);
		vec2 s2p1normalN = -trigo_normal(entries.rectriangle[entries.entry2.first] - entries.rectriangle[(entries.entry2.first+N-1)%N]);
		vec2 s2p2normalN = -trigo_normal(entries.rectriangle[entries.entry2.second]  - entries.rectriangle[(entries.entry2.second+1)%N]);
		
		if(glm::length(s1p1normalN)<geom::EPSILON || glm::length(s1p2normalN)<geom::EPSILON
		|| glm::length(s2p1normalN)<geom::EPSILON || glm::length(s2p2normalN)<geom::EPSILON)
		{
			utils::err("geom::equidistant_halflines(): segment is point");
		}

		s1p1normalN = glm::normalize(s1p1normalN);
		s1p2normalN = glm::normalize(s1p2normalN);
		s2p1normalN = glm::normalize(s2p1normalN);
		s2p2normalN = glm::normalize(s2p2normalN);

		ret.first.point  = min_equidistant_point(entries.rectriangle[entries.entry1.first], entries.rectriangle[entries.entry1.second], s1p1normalN, s1p2normalN, ret.first.dirN );
		ret.second.point = min_equidistant_point(entries.rectriangle[entries.entry2.first], entries.rectriangle[entries.entry2.second], s2p1normalN, s2p2normalN, ret.second.dirN);

		return ret;
	}

	Segment2D flip(const Segment2D& s)
	{
		return {s.second, s.first};
	}

	int planar_K4(const glm::vec2 points[4])
	{
		if(point_in_triangle(points[0], points[1], points[2], points[3]))
			return 0;
		if(point_in_triangle(points[1], points[0], points[2], points[3]))
			return 1;
		if(point_in_triangle(points[2], points[0], points[1], points[3]))
			return 2;
		if(point_in_triangle(points[3], points[0], points[1], points[2]))
			return 3;

		return -1;
	}

	rectriangleEntries rectriangle_entries(const Segment2D& s1, const Segment2D& s2)
	{
		vec2 vs[4] = {s1.first, s1.second, s2.first, s2.second};
		rectriangleEntries ret;

		int pli = planar_K4(vs);

		if(pli == -1)
		{
			ret.rectriangle = {s1.first, s1.second, s2.first, s2.second};
			ret.entry1 = {1,2};
			ret.entry2 = {3,0};

			if(!is_cycle(ret.rectriangle))
			{
				ret.rectriangle = {s1.second, s1.first, s2.first, s2.second};
			}

			if(!trigo_way(ret.rectriangle))
			{
				ret.rectriangle = {ret.rectriangle[1], ret.rectriangle[0], ret.rectriangle[3], ret.rectriangle[2]};
			}

			return ret;	
		}
		else
		{
			if(pli<=1)
			{
				ret.rectriangle = {vs[2], vs[3], vs[1-pli]};
			}
			else
			{
				int other = 2+(1-(pli-2));
				ret.rectriangle = {vs[0], vs[1], vs[other]};
			}

			if(!trigo_way(ret.rectriangle))
			{
				ret.rectriangle = {ret.rectriangle[1], ret.rectriangle[0], ret.rectriangle[2]};
			}

			ret.entry1 = {1, 2};
			ret.entry2 = {2, 0};

			return ret;
		}
	}

	glm::vec3 yfixed(const glm::vec3& v, float y)
	{
		return vec3(v.x, y, v.z);
	}

	paverInfo to_paver_info(const ycentredPaverInfo& paver)
	{
		return paverInfo{
			.bottom  = paver.bottomy - (paver.sized_x + paver.sized_z)/2.0f,
			.sized_x = paver.sized_x,
			.sized_y = paver.sized_y,
			.sized_z = paver.sized_z,
		};
	}	

	exactPaverInfo to_exact_paver_info(const transPaverInfo& paver)
	{
		return exactPaverInfo{
			.square_base_up = {
				paver.square_base[0],
				paver.square_base[1],
				paver.square_base[2],
				paver.square_base[3]
			},
			.square_base_down = {
				paver.square_base[0] + paver.sized_direction,
				paver.square_base[1] + paver.sized_direction,
				paver.square_base[2] + paver.sized_direction,
				paver.square_base[3] + paver.sized_direction
			}
		};
	}

	exactPaverInfo to_exact_paver_info(const paverInfo& paver)
	{
		return exactPaverInfo{
			.square_base_up = {
				paver.bottom,
				paver.bottom + paver.sized_x,
				paver.bottom + paver.sized_x + paver.sized_z,
				paver.bottom + paver.sized_z
			},
			.square_base_down = {
				paver.bottom + paver.sized_y,
				paver.bottom + paver.sized_y + paver.sized_x,
				paver.bottom + paver.sized_y + paver.sized_x + paver.sized_z,
				paver.bottom + paver.sized_y + paver.sized_z
			}
		};
	}

	glm::vec3 trigo_normal(const glm::vec3& v)
	{
		return unprojplan(trigo_normal(projplan(v)));
	}

};


namespace glm
{
	float normalize(float x)
	{
		return (x >= 0.0) ? 1.0f : -1.0f;
	}
};



