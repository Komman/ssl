#ifndef _STORED_GRAPH_HPP_
#define _STORED_GRAPH_HPP_

#include <utility>

#include "graph.hpp"
#include "utils_debug.hpp"

template<typename VertexType>
class StoredGraph : public Graph
{
public:
	StoredGraph();
	StoredGraph(const StoredGraph& graph);

	VertexType value(uint vertex) const;
	std::pair<VertexType, VertexType> value(const Edge& e) const;
	std::vector<VertexType> value(const std::vector<uint>& vertices) const;

	uint add_vertex(const VertexType& value);
	void remove_vertex(uint vertex);

	VertexType operator[](uint vertex) const;
	std::vector<VertexType> operator[](const std::vector<uint>& vertices) const;

private:
	std::vector<VertexType> _data;
};






template<typename VertexType>
std::pair<VertexType, VertexType> StoredGraph<VertexType>::value(const Edge& e) const
{
	return {this->value(e.first), this->value(e.second)};
}


template<typename VertexType>
StoredGraph<VertexType>::StoredGraph()
	: Graph(),
	 _data()
{

}
template<typename VertexType>
StoredGraph<VertexType>::StoredGraph(const StoredGraph& graph)
	: Graph(graph),
	 _data(graph._data)
{

}

template<typename VertexType>
uint StoredGraph<VertexType>::add_vertex(const VertexType& value)
{
	uint i = this->Graph::add_vertex();

	if(i < _data.size())
	{
		_data[i] = value;
	}
	else
	{
		for(uint j=_data.size(); j<i; j++)
		{
			_data.push_back(VertexType());
		}
		_data.push_back(value);
	}

	return i;
}

template<typename VertexType>
void StoredGraph<VertexType>::remove_vertex(uint vertex)
{
	this->Graph::remove_vertex(vertex);

	while(_data.size() > this->cpu_adjency_size())
	{
		_data.pop_back();
	}
}

template<typename VertexType>
VertexType StoredGraph<VertexType>::value(uint vertex) const
{
	if(!this->check_vertex(vertex))
	{
		utils::err("StoredGraph::value(" + std::to_string(vertex)
				 + ") vertex not existing");
	}

	return _data[vertex];
}

template<typename VertexType>
VertexType StoredGraph<VertexType>::operator[](uint vertex) const
{
	return this->value(vertex);
}

template<typename VertexType>
std::vector<VertexType> StoredGraph<VertexType>::value(const std::vector<uint>& vertices) const
{
	std::vector<VertexType> ret;

	for(uint i : vertices)
	{
		ret.push_back(this->value(i));
	}
	
	return ret;
}

template<typename VertexType>
std::vector<VertexType> StoredGraph<VertexType>::operator[](const std::vector<uint>& vertices) const
{
	return this->value(vertices);
}



#endif //_STORED_GRAPH_HPP_
