#include "utils_debug.hpp"

#include <iostream>

using namespace std;

namespace TERM
{
	const std::string BLACK  = "\033[1;30m";
	const std::string RED    = "\033[1;31m";
	const std::string GREEN  = "\033[1;32m";
	const std::string ORANGE = "\033[1;33m";
	const std::string BLUE   = "\033[1;34m";
	const std::string PURPLE = "\033[1;35m";
	const std::string CYAN   = "\033[1;36m";
	const std::string WHITE  = "\033[1;37m";
	const std::string NOCOL  = "\033[0m";
};

namespace TERMB
{
	const std::string BLACK  = "\033[7;30m";
	const std::string RED    = "\033[7;31m";
	const std::string GREEN  = "\033[7;32m";
	const std::string ORANGE = "\033[7;33m";
	const std::string BLUE   = "\033[7;34m";
	const std::string PURPLE = "\033[7;35m";
	const std::string CYAN   = "\033[7;36m";
	const std::string WHITE  = "\033[7;37m";
	const std::string NOCOL  = "\033[0m";
};

namespace utils
{
	const char* utils_error::what() const noexcept
	{
	    return "UTILS ERROR";
	}

	void err(const std::string& msg)
	{
		cout<< endl<< TERM::RED<< "UTILS ERROR: " + TERM::BLUE + msg + TERM::NOCOL<< endl<< endl;
		throw utils_error();
	}
};
