#ifndef _UPDATABLE_STRUCT_HPP_
#define _UPDATABLE_STRUCT_HPP_

template<typename StructName>
struct datedStruct : public StructName
{
	mutable bool updated;
	

	template<typename... ArgsType>
	datedStruct(ArgsType&&... args) : StructName(args...), updated(false) {}

	const datedStruct& operator=(const StructName& s) {StructName::operator=(s); return *this;}
	const datedStruct& operator+(const StructName& s) {StructName::operator+(s); return *this;}
	const datedStruct& operator-(const StructName& s) {StructName::operator-(s); return *this;}
	const datedStruct& operator*(const StructName& s) {StructName::operator*(s); return *this;}
	const datedStruct& operator/(const StructName& s) {StructName::operator/(s); return *this;}
};

#endif //_UPDATABLE_STRUCT_HPP_
