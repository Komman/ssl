#include "graph.hpp"

#include <iostream>

#include "utils_debug.hpp"

using namespace std;

Graph::Graph()
	: BasicGraph(),
	  _adjency(),
	  _deads()
{

}

Graph::Graph(const Graph& g)
	: _adjency(g._adjency),
	  _deads(g._deads)
{

}


uint Graph::degree(uint vertex) const
{
	return this->neighbours(vertex).size();
}


unsigned int Graph::size() const
{
	#ifdef GRAPH_DEBUG
	if(_deads.size() > _adjency.size())
	{
		utils::err("Graph::size() wrong behavior: _deads.size() > _adjency.size");
	}
	#endif

	return _adjency.size() - _deads.size();
}

uint Graph::cpu_adjency_size() const
{
	return _adjency.size();
}


void Graph::print() const
{
	for(uint y=0; y<_adjency.size(); y++)
	{
		if(_deads.find(y) != _deads.end())
			continue;

		for(uint x=0; x<_adjency.size(); x++)
		{
			if(_deads.find(x) != _deads.end())
				continue;

			std::cout<<(_adjency[x][y] ? '1' : '0');
		}
		cout<<endl;
	}
}

bool Graph::connected(const Edge& e1, const Edge& e2) const 
{
	return (e1.first == e2.first)
		|| (e1.first == e2.second)
		|| (e1.second == e2.first)
		|| (e1.second == e2.second);
}

uint Graph::add_vertex()
{
	auto it = _deads.begin();

	if(it != _deads.end())
	{
		uint ret = *it;
		_deads.erase(it);
		return ret;
	}
	else
	{
		uint ret = _adjency.size();

		#ifdef GRAPH_DEBUG
		if(_deads.size() != 0)
		{
			utils::err("Graph::add_vertex(): wrong behavior: _deads should be empty");
		}
		#endif

		_adjency.push_back(std::vector<bool>(_adjency.size(), false));

		for(auto& c : _adjency)
		{
			c.push_back(false);
		}

		return ret;
	}
}

bool Graph::check_vertex(uint index) const
{
	return (index < _adjency.size()) && (_deads.find(index) == _deads.end());
}

bool Graph::check_edge(const Edge& e) const
{
	return this->check_vertex(e.first) && this->check_vertex(e.second);
}

std::vector<uint> Graph::all_vertices() const
{
	std::vector<uint> ret;

	for(uint i=0; i<_adjency.size(); i++)
	{
		if(this->check_vertex(i))
		{
			ret.push_back(i);
		}
	}

	return ret;
}

const std::vector<Graph::Edge>& Graph::all_edges() const
{
	if(_edgecache.updated)
	{
		return _edgecache;
	}

	std::vector<Graph::Edge> ret;

	for(uint x=0; x<_adjency.size(); x++)
	{
		for(uint y=0; y<=x; y++)
		{
			if(this->check_vertex(x) && this->check_vertex(y) && _adjency[x][y])
			{
				ret.push_back({y,x});
			}
		}
	}

	_edgecache = ret;
	_edgecache.updated = true;
	return _edgecache;
}

void Graph::unoriented_fusion_vertices_no_selfloop(uint tokeep, uint toremove)
{
	if(!this->check_vertex(tokeep) || !this->check_vertex(toremove))
	{
		utils::err("Graph::fusion_vertices_no_selfloop(): try to fusion non existing vertex");
	}

	auto neis = this->neighbours(toremove);

	for(uint n : neis)
	{
		if(n != tokeep)
		{
			this->add_edge({n, tokeep});
		}
	}

	this->remove_vertex(toremove);
}

void Graph::remove_oriented_edge(const Edge& e)
{
	_edgecache.updated = false;
	
	if(this->check_edge(e))
	{
		_adjency[e.first][e.second] = false;
	}
}

void Graph::remove_edge(const Edge& e)
{
	this->remove_oriented_edge(e);
	this->remove_oriented_edge(mirror(e));
}

// Remove every edge, O(size*size)
void Graph::stable()
{
	for(uint x=0; x<_adjency.size(); x++)
	{
		for(uint y=0; y<_adjency[x].size(); y++)
		{
			if(this->check_vertex(x) && this->check_vertex(y))
			{
				this->remove_oriented_edge({x,y});
			}
		}
	}
}

bool Graph::connected(uint u, uint v) const
{
	return this->check_edge({u,v}) && _adjency[u][v];
}


void Graph::add_oriented_edge(const Edge& e)
{
	#ifdef GRAPH_DEBUG
	if(!this->check_edge(e))
	{
		utils::err("Graph::add_oriented_edge(): edge out of bound : {"
			     + std::to_string(e.first) + ", " + std::to_string(e.second)
			     + "} while size = " + std::to_string(this->size()));
	}
	#endif

	if(_edgecache.updated && e.first <= e.second && !(_adjency[e.first][e.second]))
	{
		_edgecache.push_back(e);
	}

	_adjency[e.first][e.second] = true;
}

void Graph::add_edge(const Edge& e)
{
	this->add_oriented_edge(e);
	this->add_oriented_edge(mirror(e));
}

void Graph::remove_vertex(uint vertex)
{
	_deads.insert(vertex);
	_edgecache.updated = false;
	
	while(_deads.find(_adjency.size()-1) != _deads.end())
	{
		_deads.erase(_adjency.size()-1);

		_adjency.pop_back();
		for(auto& c : _adjency)
		{
			c.pop_back();
		}
	}	
}

std::vector<uint> Graph::neighbours(uint vertex) const
{
	if(!this->check_vertex(vertex))
	{
		utils::err("Graph::neighbours(uint vertex): vertex out of bound: " + std::to_string(vertex));
	}

	std::vector<uint> ret;

	for(uint i=0; i<_adjency.size(); i++)
	{
		if(this->check_vertex(i) && _adjency[vertex][i])
		{
			ret.push_back(i);
		}
	}

	return ret;
}

void Graph::add_path(const std::vector<uint>& border)
{
	for(uint i=0; i<border.size(); i++)
	{
		uint j = (i+1)%border.size();
		
		this->add_edge({border[i], border[j]});
	}
}

std::unordered_set<uint> Graph::DFS(uint start)
{
	std::unordered_set<uint> ret  = {start};
	std::vector<uint> todo = {start};

	while(todo.size() > 0)
	{
		uint v = todo[todo.size() - 1];
		todo.pop_back();

		std::vector<uint> neis = this->neighbours(v);
		for(uint n : neis)
		{
			if(ret.find(n) == ret.end())
			{
				todo.push_back(n);
				ret.insert(n);
			}
		}
	}

	return ret;
}

