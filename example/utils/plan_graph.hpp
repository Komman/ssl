
#ifndef _PLAN_GRAPH_HPP_
#define _PLAN_GRAPH_HPP_

#include <string>
#include <unordered_set>
#include <glm/glm.hpp>

#include "geom.hpp"
#include "stored_graph.hpp"
#include "plan_dual_graph.hpp"
#include "space_grid.hpp"

class PlanGraph : public StoredGraph<glm::vec2>
{
public:
	static constexpr uint SKIP = std::numeric_limits<uint>::max();

public:
	PlanGraph();
	PlanGraph(const PlanGraph& graph);

	/*
		Add a segment bewteen v1 and v2, if there is
		intersection with other edges, creates a vertex
		at every intersection.
		At the end, v1 and v2 may not be adjacent but 
		there will be a path in straight line from v1
		to v2.
		Returns the vertices added
	*/
	std::vector<uint> build_planar_edge(uint v1, uint v2);
	std::vector<uint> build_planar_edge(const Edge& e);
	// If the position has distance <mind of another vertex,
	// just merge the two vertices.
	// Return if a vertex has been added,
	// and the id of the placed vertex, that can be the 
	// merged vertex if no vertex has been added
	std::pair<bool, uint> build_planar_vertex(const glm::vec2& position, float mind);

	// On the plan O(size), in vertices, MAX means "skip"
	uint nearest(uint v, const std::vector<uint>& vertices);
	uint nearest(const glm::vec2& point, const std::vector<uint>& vertices);
	
	float angle(const Edge& e1, const Edge& e2) const;
	// On the vertex v, coming from n, returns the angle-closest vertex
	// NOT WORKING WHEN LOOP EDGES YET
	uint  angulext_neighbour(uint v, uint n, bool trigo) const;

	// is_crossing_edges(e,e) = false
	bool is_crossing_edges(const Edge& e1, const Edge& e2) const;
	bool is_crossing_edge(const Edge& e) const;
	// Returns if is planar, and two non crossing edges if it is not
	std::pair<bool, std::pair<Edge, Edge>> is_planar() const;

	// That is not one of the two edge vertex
	float nearest_vertex_distance(const Edge& e);

	spaceBox2D space_box() const;

	// On the plan
	float distance(uint u, uint v);
	float distance(const glm::vec2& point, uint v);
	glm::vec2 center(const std::vector<uint>& vertices) const;

	// Returns edges that intersects the circle
	std::vector<Edge> circle_intersection(const circleInfo& circle);

	void delaunay_triangulation();
	// Remove every edge, and then compute a chicken triangulation
	void set_chicken_triangulation();
	void remove_small_degree(uint max_degree, const std::unordered_set<uint>& dontremove = {});
	void remove_big_degree(uint min_degree, const std::unordered_set<uint>& dontremove = {});
	void remove_close_vertices(float distance, const std::unordered_set<uint>& dontremove = {});
	// Forces the border path to be the delimiter with the outerface 
	void remove_border(const std::vector<uint>& border);
	void remove_self_loops();

	// O(size^3), Can be optimized
	// In trigo order
	std::vector<std::vector<uint>> all_faces() const;
	// O(size^2) = O(size of output) = Optimal
	std::vector<std::vector<uint>> faces(uint v) const;
	// O(size) = O(size of output) = Optimal
	std::vector<uint> face(uint v, uint neibour, bool trigo) const;

	PlanDualGraph compute_dual() const;

	// Supppose it is connected
	std::vector<uint> outer_face() const;
	bool is_outer_face(const std::vector<uint>& face) const;

	void print_ppm(const std::string& path, uint xdimension) const;
	SpaceGrid compute_drawing(uint xdimension) const;
	std::string string_value(const Edge& e) const;

private:
	void set_full_chicken_triangulation();
};


#endif //_PLAN_GRAPH_HPP_
