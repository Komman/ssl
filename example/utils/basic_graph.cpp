#include "basic_graph.hpp"

#include "utils_debug.hpp"

using namespace std;

std::string BasicGraph::Edge::to_string() const
{
	return "{" + std::to_string(first) + ", " + std::to_string(second) + "}";
}

bool BasicGraph::Edge::operator==(const Edge& e) const
{
	return first == e.first && second == e.second;
}

bool BasicGraph::Edge::isomorphic(const Edge& e) const
{
	return ((*this) == e) || ((*this) == BasicGraph::mirror(e));
}

BasicGraph::Edge BasicGraph::Edge::opposite()
{
	return {second, first};
}


std::vector<BasicGraph::Edge> edges_cycle(const std::vector<uint>& vertices)
{
	std::vector<BasicGraph::Edge> ret;

	for(uint i=0; i<vertices.size(); i++)
	{
		uint next = (i+1)%vertices.size();

		ret.push_back({vertices[i], vertices[next]});
	}

	return ret;
}

std::vector<uint> vertices_cycle(const std::vector<BasicGraph::Edge>& edges)
{
	std::vector<uint> ret;

	for(uint i=0; i<edges.size(); i++)
	{
		uint next = (i+1)%edges.size();

		#ifdef GRAPH_DEBUG
		if(edges[i].second != edges[next].first)
		{
			utils::err("vertices_cycle(): edges doesn't form a cycle");
		}
		#endif

		ret.push_back(edges[i].first);
	}

	return ret;
}


BasicGraph::Edge BasicGraph::mirror(const Edge& e)
{
	return {e.second, e.first};
}
