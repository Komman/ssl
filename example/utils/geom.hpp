#ifndef _GEOM_HPP_
#define _GEOM_HPP_

#include <glm/glm.hpp>
#include <vector>
#include <string>
#include <array>
#include <unordered_map>
#include <glm/gtx/string_cast.hpp>

#include "shapes.hpp"

struct triIndex
{
	uint i1;
	uint i2;
	uint i3;

	std::string to_string() const;
};

struct generatedPoint
{
	glm::vec2 point;
	uint original_index;
};

struct hashVec2 {
    std::size_t operator()(const glm::vec2& v) const {
        std::size_t h1 = std::hash<float>()(v.x);
        std::size_t h2 = std::hash<float>()(v.y);
        return h1 ^ h2;
    }
};

namespace geom
{
	constexpr collisionImpact NO_IMPACT = {
		.impact   = false,
		.distance = 0,
		.point    = glm::vec3(0),
		.normalN  = glm::vec3(0)
	};

	constexpr float EPSILON = 0.00001f;

	Segment2D flip(const Segment2D& s);

	glm::vec2 directionN(const Segment2D& s);
	// Returns the normalized vector or v if v is too close to vec3(0)
	glm::vec3 safe_normalize(const glm::vec3& v);
	glm::vec2 safe_normalize(const glm::vec2& v);
	// Returns the normalized vector or v if v is too close to vec3(0), and its norm as .w component
	glm::vec4 safe_normalize_lenght(const glm::vec3& v);
	glm::vec3 safe_proj(const glm::vec3& v, const glm::vec3& normalN);
	glm::vec3 reflect(const glm::vec3& v, const glm::vec3& normalN);
	glm::vec3 symmetrical(const glm::vec3& point, const infinitePlan& sym_plan);
	glm::mat3 symmetrical(const glm::mat3& rotation_matrix, const glm::vec3& normalN);
	paverInfo symmetrical(const paverInfo& paver, const infinitePlan& sym_plan);
	glm::vec2 trigo_normal(const glm::vec2& v);
	// Same methods as the previous method, but the y-component is set to 0
	glm::vec3 trigo_normal(const glm::vec3& v);
	// Force he cycle to by in trigonometruc order
	void order_trigo(std::vector<glm::vec2>& cycle);
	// Coord x and z
	glm::vec2 projplan(const glm::vec3& v);
	glm::vec2 projplan(const glm::vec3& v, const base2D& plan);
	std::vector<glm::vec2> projplan(const std::vector<glm::vec3>& v);
	std::vector<glm::vec2> projplan(const std::vector<glm::vec3>& v, const base2D& plan);
	partialCycle<glm::vec2> projplan(const partialCycle<glm::vec3>& cycle, const base2D& plan);
	partialEnlargementInfos<glm::vec2> projplan(const partialEnlargementInfos<glm::vec3>& cycle, const base2D& plan);
	directedLine2D projplan(const directedLine3D& l);
	glm::vec3 unprojplan(const glm::vec2& v, float y = 0.0f);
	glm::vec3 unprojplan(const glm::vec2& v, const base2D& original_plan);
	std::vector<glm::vec3> unprojplan(const std::vector<glm::vec2>& v, float y);
	std::vector<glm::vec3> unprojplan(const std::vector<glm::vec2>& v, const base2D& original_plan);
	partialCycle<glm::vec3> unprojplan(const partialCycle<glm::vec2>& cycle, const base2D& plan);
	partialEnlargementInfos<glm::vec3> unprojplan(const partialEnlargementInfos<glm::vec2>& cycle, const base2D& plan);
	glm::vec2 orthogonal(const glm::vec2& v);
	spaceBase orthogonal_spacebase(const glm::vec3& vx, const glm::vec3& vy);
	glm::vec3 highest(const std::vector<glm::vec3>& points);
	uint highest(const std::vector<glm::vec2>& points);
	glm::vec3 lowest(const std::vector<glm::vec3>& points);
	/*
		  -| v2N
		  /
		 /
		/_____\ trigo_bissectriceN(v1N, v2N)
		\     /
		 \
		 _\| v1N
	*/
	// returns the trigonometric normal if v1N == -v2N
	glm::vec2 trigo_bissectriceN(const glm::vec2& v1N, const glm::vec2& v2N);
	glm::vec2 trigo_bissectriceN(const glm::vec2& p1, const glm::vec2& p2, const glm::vec2& p3);
	// if poits are aligned, supposes that the plan normal is plan_normal
	glm::vec3 trigo_bissectriceN(const glm::vec3& p1, const glm::vec3& p2, const glm::vec3& p3, const glm::vec3& plan_normal = glm::vec3(0,1,0));
	glm::vec3 trigo_bissectriceN(const std::vector<glm::vec3>& v, uint index, const glm::vec3& plan_normal = glm::vec3(0,1,0));
	// Poiting in the same direction ans v1N and v2N
	glm::vec2 bissectriceN(const glm::vec2& v1N, const glm::vec2& v2N);
	// Returns dot(trigo_normal(v1), v2)
	float cross2D(const glm::vec2& v1, const glm::vec2& v2);
	bool trigo_way(const std::vector<glm::vec2>& cycle);
	bool trigo_turn(const glm::vec2& AB, const glm::vec2& BC);
	// Returns the max distance between the source and one of the points
	float maxdistance(const glm::vec2& source, const std::vector<glm::vec2>& points);
	float maxdistance(const glm::vec3& source, const std::vector<glm::vec3>& points);
	// Returns the min distance between the source and any of the points
	float distance(const Segment2D& s);
	float distance(const glm::vec3& source, const std::vector<glm::vec3>& points);
	float distance(const glm::vec2& source, const std::vector<glm::vec2>& points);
	// Cycle must be in trigonometric order
	circleInfo max_inscribed_cycle(const std::vector<glm::vec2>& polygon);
	// Cycle must be in trigonometric order
	glm::vec2 get_inside_point(const std::vector<glm::vec2>& polygon);
	// Returns the maximum distance between two points
	float diameter(const std::vector<glm::vec2>& points);
	float diameter(const std::vector<glm::vec3>& points);
	float cycle_maxdistance(const std::vector<glm::vec2>& points, const glm::vec2& source);
	float cycle_mindistance(const std::vector<glm::vec2>& points, const glm::vec2& source);
	// Same methods as the previous method, but the y-component is ignored 
	float cycle_mindistance(const std::vector<glm::vec3>& points, const glm::vec3& source);
	// Returns the maximum distance d that is possible that allows bissectrice_enlarge_cycle(points, -d) to be a cycle
	// points must be in trigonometric order
	// NOT ACCURATE FOR NOW (but working)
	float cycle_bissectrice_maxdelargement(const std::vector<glm::vec2>& points);
	// Same methods as the previous method, but the y-component is ignored 
	float cycle_bissectrice_maxdelargement(const std::vector<glm::vec3>& points);
	// Must be in trigonometric order
	float cycle_conspace_maxdelargement(const std::vector<glm::vec2>& points);
	float cycle_conspace_maxdelargement(const std::vector<glm::vec3>& points);
	float distance_point_segment(const glm::vec3& seg_start, const glm::vec3& seg_end, const glm::vec3& point);
	float distance_point_segment(const glm::vec2& seg_start, const glm::vec2& seg_end, const glm::vec2& point);
	float distance_point_segment(const Segment2D& s, const glm::vec2& point);
	float distance_segment_segment(const Segment2D& seg1, const Segment2D& seg2);
	float distance_point_polygon(const glm::vec2& point, const std::vector<glm::vec2>& polygon);
	// Returns the nearest point from the point to the segment
	glm::vec3 proj_point_on_segment(const glm::vec3& line_point1, const glm::vec3& line_point2, const glm::vec3& point);
	glm::vec2 proj_point_on_segment(const glm::vec2& line_point1, const glm::vec2& line_point2, const glm::vec2& point);
	glm::vec3 proj_point_on_line(const glm::vec3& line_point1, const glm::vec3& line_point2, const glm::vec3& point);
	glm::vec2 proj_point_on_line(const glm::vec2& line_point1, const glm::vec2& line_point2, const glm::vec2& point);
	glm::vec2 proj_point_on_line(const Segment2D& line, const glm::vec2& point);
	// point on the segment l1p1 -> l1p2
	glm::vec2 trigo_crossing_problem(const glm::vec2& l1p1, const glm::vec2& l1p2, const glm::vec2& l2p1, const glm::vec2& l2p2, const glm::vec2& point);
	// Supposing all points are on a same y-fixed plan
	glm::vec3 trigo_crossing_problem(const glm::vec3& l1p1, const glm::vec3& l1p2, const glm::vec3& l2p1, const glm::vec3& l2p2, const glm::vec3& point);
	// closest on l1
	std::pair<glm::vec3, glm::vec3> closest_point(const directedLine3D& l1, const directedLine3D& l2);
	// index i means segment (i)-((i+1)%cycle.size())
	uint nearest_segment_index(const std::vector<glm::vec2>& cycle, const glm::vec2& point);
	bool point_on_line(const Segment2D& line, const glm::vec2& point);
	// Assuming the point is on the line
	bool online_point_on_segment(const Segment2D& seg, const glm::vec2& point, float precision = 0.0f);
	// Decide if it inside the segment or not
	bool point_on_segment(const glm::vec2& line_point1, const glm::vec2& line_point2, const glm::vec2& point);
	bool point_on_segment(const Segment2D& seg, const glm::vec2& point);
	bool large_point_on_segment(const glm::vec2& line_point1, const glm::vec2& line_point2, const glm::vec2& point, float radius);
	glm::vec3 yfixed(const glm::vec3& v, float y);

	// If every point of the cycle is reachable by a line from the "relative" without
	// crossing the cycle then returns the minimal distance from "relative" to a cycle 
	// point (or edge), else returns 0
	// "cycle" must be in trigo sense
	float relative_convexity(const std::vector<glm::vec2>& cycle, const glm::vec2& relative);
	float baryconvexity(const std::vector<glm::vec2>& cycle);
	// Same methods as the previous method, but the y-component is ignored 
	float relative_convexity(const std::vector<glm::vec3> cycle, const glm::vec3& relative);
	float baryconvexity(const std::vector<glm::vec3> cycle);
	// The longest edge is the edge (r)-((r+1)%cycle.size()), with r the returned value
	uint longest_edge(const std::vector<glm::vec3>& cycle);

	// returns a corner that forms a triangle IN the cycle with its neighbourgs
	// The cycle must be in trigonometric order
	triIndex find_cycle_corner(const std::vector<glm::vec2>& cycle);
	// The cycle must be in trigonometric order
	std::vector<triIndex> cycle_triangulation(const std::vector<glm::vec2>& cycle);
	// Inner cycle must be in the outer cycle and never cross edges with it
	// Boths cycles must be in trigonometric order
	std::vector<triIndex> rubban_triangulation(const std::vector<glm::vec2>& points, std::vector<uint> outer_cycle, std::vector<uint> inner_cycle);
	// Returns a segment (outer_cycle index, inner_cycle index) that doesn't
	// cross any segment of the rubban
	glm::uvec2 rubban_diameter_segment(const std::vector<glm::vec2>& points, const std::vector<uint>& outer_cycle, const std::vector<uint>& inner_cycle);
	bool is_cycle(const std::vector<glm::vec2>& points);
	bool is_cycle(const std::vector<glm::vec3>& points);


	// Takes a cycle that must be on a plan
	// zpositive is a vector that forces the base
	// to have: dot(zpositive, cross(base2D.dirxN, base2D.diryN)) > 0
	base2D find_plan(const std::vector<glm::vec3>& cycle, const glm::vec3& zpositive = glm::vec3(0,1,0));
	// Returns a plan on which the cycle is in trigonometric order
	base2D find_trigo_plan(const std::vector<glm::vec3>& cycle);
	// The returned cycle is in trigonometri order
	std::vector<glm::vec2> proj_cycle_on_its_plan(const std::vector<glm::vec3>& cycle);

	// Same methods as the 3 previous ones, but all points must be on a same plan 
	triIndex find_cycle_corner(const std::vector<glm::vec3>& cycle);
	std::vector<triIndex> cycle_triangulation(const std::vector<glm::vec3>& cycle);
	// y-fixed plan
	std::vector<triIndex> rubban_triangulation(const std::vector<glm::vec3>& points, const std::vector<uint>& outer_cycle, const std::vector<uint>& inner_cycle);

	// Cycle must be in trigonometric order
	std::vector<glm::vec2> bissectrice_enlarge_cycle(const std::vector<glm::vec2>& cycle, float distance);
	// same method as 2D, but the y dimension is ignored, and a translation is possible
	// Can be optimized
	std::vector<glm::vec3> bissectrice_enlarge_cycle(const std::vector<glm::vec3>& cycle, const glm::vec3& translation, float distance);
	
	std::vector<glm::vec2> extract_cycle(const std::vector<generatedPoint>& cycle);
	// perfect: throw error if a point appears twice
	std::unordered_map<glm::vec2, uint, hashVec2> extract_indexing(const std::vector<generatedPoint>& cycle, bool perfect);
	std::vector<generatedPoint> recreate_generated_cycle(const std::vector<glm::vec2>& cycle, const std::unordered_map<glm::vec2, uint, hashVec2>& indexing);


	partialCycle<glm::vec2> partial_cycle(const std::vector<glm::vec2>& cycle, uint cut_edge);
	partialCycle<glm::vec2> partial_cycle(const partialCycle<glm::vec2>& cycle, uint cut_edge);
	// Can be optimized
	// Assuming all vertices of the cycle is on a same 2D plan
	partialCycle<glm::vec3> partial_cycle(const std::vector<glm::vec3>& cycle, uint cut_edge);
	partialCycle<glm::vec3> partial_cycle(const partialCycle<glm::vec3>& cycle, uint cut_edge);
	// std::vector
	// Cycle must be in trigonometric order (for all conspace_enlarge_cycle functions)
	// Constant space between out and in polygon
	// curveprecision_distance is the disance precision, i.e. the 
	// distance discretization of the curves
	// Precision sample for curves is upper bounded to 32 (even if curveprecision_distance is very small)
	std::vector<glm::vec2> conspace_enlarge_cycle(const std::vector<glm::vec2>& cycle, float distance, float curveprecision_distance);
	partialEnlargementInfos<glm::vec2> conspace_enlarge_cycle(const partialCycle<glm::vec2>& cycle, float distance, float curveprecision_distance);
	partialEnlargementInfos<glm::vec3> conspace_enlarge_cycle(const partialCycle<glm::vec3>& cycle, float distance, float curveprecision_distance);
	std::vector<glm::vec3> conspace_enlarge_cycle(const std::vector<glm::vec3>& cycle, const glm::vec3& translation, float distance, float curveprecision_distance);
	// Returns the enlarged cycle and the index of cycle from
	// which the point comes from
	std::vector<generatedPoint> conspace_enlarge_cycle_fullinfos(const std::vector<glm::vec2>& cycle, float distance, float curveprecision_distance);
	std::vector<glm::vec2> enlargement_connection_curved(const glm::vec2& p1, const glm::vec2& p2, const glm::vec2& p3, float distance, float curveprecision_distance);
	std::vector<glm::vec2> enlargement_connection_no_curve(const glm::vec2& p1, const glm::vec2& p2, const glm::vec2& p3, float distance);

	// Cycle must be in trigonometric sense
	// Remove points that are on a segment with a length less than min_distance
	// Guaranty that the cycle will keep its maximum inscribed circle center into the cicle 
	// center_ptr is for avoiding recalutation is already known, it is renseigned, it is a 
	// point in the cycle that is guarantee to be in the returned approximated cycle
	void approximate_cycle(std::vector<glm::vec2>& cycle, float min_distance, const glm::vec2* center_ptr = NULL);
	// Removes all corners that have a face-inside aof less than min_distance
	std::vector<glm::vec2> smooth_cycle(const std::vector<glm::vec2>& cycle, float min_distance);
	bool unsmooth_croos(const std::vector<glm::vec2>& cycle, float min_distance);

	template<typename T>
	T homothety(const T& v, const T& old_center, float coeff);
	template<typename T>
	std::vector<T> homothety(const std::vector<T>& v, const T& old_center, const T& new_center, float coeff);
	template<typename T>
	std::vector<T> absolute_homothety(const std::vector<T>& v, const T& old_center, const T& new_center, float d);
	template<typename T>
	std::vector<T> barymetric_homothety(const std::vector<T>& v, const T& new_center, float coeff);
	template<typename T>
	std::vector<T> barycentred_homothety(const std::vector<T>& v, float coeff);
	template<typename T>
	std::vector<T> translate(const std::vector<T>& v, const T& traslation);

	template<typename T>
	T mean(const std::vector<T>& v);
	template<typename T>
	std::vector<T> interpolate(const T& v1, const T& v2, uint amount);

	// vector must not be of non zero length
	// returns a positve [0, 2pi] angle following the trigonometic order
	float angle(const glm::vec2& v1, const glm::vec2& v2);

	// vector must not be of non zero length
	// returns the angle in the plan in [-pi, pi]
	float angle_plan(const glm::vec2& v1, const glm::vec2& v2);

	template<typename T>
	T middle(const T& v1, const T& v2) {return (v1 + v2)/2.0f;}

	glm::vec3 circle_point_plan(const glm::vec3& circle_center,
						  		const glm::vec3& xaxisN,
						  		const glm::vec3& yaxisN,
						  		float angle,
						  		float radius);

	glm::vec3 circle_point(const glm::vec3& circle_center,
						   const glm::vec3& xaxisN,
						   const glm::vec3& normalN,
						   float angle,
						   float radius);

	glm::vec2 circle_point(const glm::vec2& circle_center,
						   const glm::vec2& xaxisN,
						   float angle,
						   float radius);

	glm::vec2 circle_point(const glm::vec2& circle_center,
						   float angle,
						   float radius);

	std::vector<glm::vec3> circle_points(const glm::vec3& circle_center,
										 const glm::vec3& xaxisN,
										 const glm::vec3& normalN,
										 float radius,
										 uint points_amount);

	glm::ivec2 unit_direction(const glm::vec2& start, const glm::vec2& dst);

	glm::vec3 get_orthogonalyx_N(const glm::vec3& v);

	// No rotation if dirN = vec2(1,0)
	glm::vec2 rotate_point2D(const glm::vec2& point, const glm::vec2& dirN);
	glm::vec3 planar_rotate(const glm::vec3& point,  const glm::vec3& center, const glm::vec2& old_dirN, const glm::vec2& new_dirN);

	glm::vec3 rotate_redirect(const glm::vec3& v,
						      const glm::vec3& original_dirN,
						      const glm::vec3& new_dirN);

	glm::vec3 plan_projection(const glm::vec3& point,
							  const glm::vec3& plan_point,
							  const glm::vec3& plan_normalN);
	
	glm::vec3 plan_projection(const glm::vec3& point,
							  const glm::vec3& plan_point1,
							  const glm::vec3& plan_point2,
							  const glm::vec3& plan_point3);

	// Assuming the point is in the triangle's plan
	bool point_in_triangle(const glm::vec3& point,
						   const glm::vec3& triangle_point1,
						   const glm::vec3& triangle_point2,
						   const glm::vec3& triangle_point3);
	bool point_in_triangle(const glm::vec3& point,
						   const glm::vec3 plan3points[3]);
	bool point_in_triangle(const glm::vec2& point,
						   const glm::vec2& triangle_point1,
						   const glm::vec2& triangle_point2,
						   const glm::vec2& triangle_point3);
	bool point_in_triangle(const glm::vec2& point,
						   const glm::vec2 plan3points[3]);

	// if the point is on the polygon lines, returns true
	bool point_in_polygon(const glm::vec2& point, const std::vector<glm::vec2>& polygon);
	// Assuming all points are on a same plan
	bool point_in_polygon3D(const glm::vec3& point, const std::vector<glm::vec3>& polygon);

	// The PRISM_FACES[i] does not contains the i index (and contains all the others)
	constexpr inline glm::uvec3 PRISM_FACES[4] = {glm::uvec3(1,2,3),
									       		  glm::uvec3(0,2,3),
									       		  glm::uvec3(0,1,3),
									       		  glm::uvec3(0,1,2)};

	constexpr inline glm::uvec2 PRISM_EDGES[6] = {glm::uvec2(0,1),
									       		  glm::uvec2(0,2),
									       		  glm::uvec2(0,3),
									       		  glm::uvec2(1,2),
									       		  glm::uvec2(1,3),
									       		  glm::uvec2(2,3)};

	// A prism face is defined by PRISM_FACES
	glm::vec3 prism_face_normal(const prismInfo& prism, uint face);
	collisionImpact first_impact(const collisionImpact& i1, const collisionImpact& i2);
	collisionImpact first_impact_epsilonkeep(const collisionImpact& i1, const collisionImpact& i2, float espilon);
	collisionImpact opposite_impact(const collisionImpact& impact);

	bool point_in_prism(const glm::vec3& point, const prismInfo& prism);
	bool space_box_collision(const spaceBox& s1, const spaceBox& s2);
	bool segment_sphere_collision(const glm::vec3& seg_start, const glm::vec3& seg_end, const sphereInfo& sphere);
	bool segment_triangle_collision(const glm::vec3& seg_start, const glm::vec3& seg_end, const glm::vec3 triangle[3]);

	// Returns -1 as radius if points are aligned
	circleInfo circumscribed_circle(const glm::vec2 points[3]);
	// Same as previous method but y commponent is ignored, the returned value has the y of points[0]
	circleInfoPlanY circumscribed_circle(const glm::vec3 points[3]);

	glm::vec3 plan_normal(const glm::vec3 plan3points[3]);
	// inside_point is a point on the inside of the plan (needed for normal)
	glm::vec3 plan_normal(const glm::vec3 plan3points[3], const glm::vec3& inside_point);
	// inside_point is a point on the inside of the plan (needed for normal)
	glm::vec3 plan_normal_ext(const glm::vec3 plan3points[3], const glm::vec3& ext_point);

	// An infinite strip is represented by an orthogonal cut
	// Return {0,0,0,0} is no intersection
	Parallelogram infinite_strip_intersection(const Segment2D& strip1, const Segment2D& strip2);
	// Return (0,0) if no intersection
	Segment2D parallelogram_line_intersection(const Parallelogram& p, const directedLine2D& l);
	// Return (0,0) if no intersection
	Segment2D sphere_tagent_equidistant_segment(const Segment2D& s1, const Segment2D& s2);
	bool circle_segment_intersection(const circleInfo& circle, const Pair<glm::vec2>& segment);
	std::pair<directedLine2D, directedLine2D> equidistant_halflines(const Segment2D& s1, const Segment2D& s2);
	/*
		ret.rectriangle is a cycle in trigonometric sense
		ret.entry1/2 are indices of rectriangle that refers to the
		two entries segment in the rectriangle, and they are directed
		by trigonometric sense
	*/
	rectriangleEntries rectriangle_entries(const Segment2D& s1, const Segment2D& s2);
	// Return the index of the inside point if it is planar, and -1 else 
	int planar_K4(const glm::vec2 points[4]);

	glm::vec2 intersection_point(const directedLine2D& line, const lineIntersection& inter);
	glm::vec2 intersection_point(const Segment2D& s, const lineIntersection& inter);
	glm::vec2 intersection_point(const Segment2D& s, float distance_from_start);
	// If lines are colinear, there is no intersection
	// The distance from start is the distance from line1_p1 following the vector (line1_p2 - line1_p1)
	lineIntersection line_line_intersection(const glm::vec2& line1_p1, const glm::vec2& line1_p2, const glm::vec2& line2_p1, const glm::vec2& line2_p2);
	lineIntersection line_line_intersection(const directedLine2D& l1, const directedLine2D& l2);
	lineIntersection line_line_intersection(const Segment2D& l1, const Segment2D& l2);
	lineIntersection line_segment_intersection(const directedLine2D& l, const Segment2D& segment);
	lineIntersection line_plan_intersection(const glm::vec3& line_point1, const glm::vec3& line_point2, const glm::vec3 plan3points[3]);
	lineIntersection line_plan_intersection(const glm::vec3& line_point, const glm::vec3& line_dirN, const glm::vec3& plan_point, const glm::vec3& plan_normalN);
	lineIntersection halfline_line_intersection(const directedLine2D& halfline, const directedLine2D& line);
	lineIntersection halfline_segment_intersection(const directedLine2D& halfline, const Segment2D& s);
	// In 3D, if the segment is in the plane of the triangle, there is no collision 
	lineIntersection segment_plane_intersection(const glm::vec3& seg_start, const glm::vec3& seg_end, const glm::vec3 plan3points[3]);
	// The distance from start is the distance from seg1_start, following the vector (seg1_end - seg1_start)
	lineIntersection segment_segment_intersection(const glm::vec2& seg1_start, const glm::vec2& seg1_end, const glm::vec2& seg2_start, const glm::vec2& seg2_end);
	lineIntersection segment_segment_intersection(const Segment2D& s1, const Segment2D& s2);
	// inside segmment intersection, returns false if one of them is in the other
	lineIntersection strong_segment_segment_intersection(const Segment2D& s1, const Segment2D& s2);

	collisionImpact sphere_point_impact(const sphereInfo& sphere, const glm::vec3& point, const glm::vec3& move);
	collisionImpact sphere_segment_impact(const sphereInfo& sphere, const Segment3D& seg, const glm::vec3& move);
	collisionImpact segment_plane_impact(const glm::vec3& seg_start, const glm::vec3& seg_end, const glm::vec3 plan3points[3], const glm::vec3& inside_point);
	collisionImpact point_plane_impact(const glm::vec3& point, const glm::vec3& dirN, float d, const infinitePlan& plan);
	collisionImpact segment_triangle_impact(const glm::vec3& seg_start, const glm::vec3& seg_end, const glm::vec3 plan3points[3], const glm::vec3& inside_point);
	collisionImpact sphere_plan_impact(const sphereInfo& sphere, const infinitePlan& plan, const glm::vec3& dirN, float d);
	collisionImpact sphere_line_impact(const sphereInfo& sphere, const directedLine3D& line, const glm::vec3& dirN);
	// Only consider cases where the sphere impacts ON the segment (not it intersections)
	collisionImpact sphere_segment_impact_noextrm(const sphereInfo& sphere, const glm::vec3& segstart, const glm::vec3& segend, const glm::vec3& dirN, float d);
	collisionImpact line_line_impact(const directedLine3D& l1, const directedLine3D& l2, const glm::vec3& dirN);
	bool intersection(const std::vector<glm::vec2>& cycle1, const std::vector<glm::vec2>& cycle2);
	bool strong_intersection(const std::vector<glm::vec2>& cycle1, const std::vector<glm::vec2>& cycle2);

	glm::vec3 symmetrical(const glm::vec3& v, const glm::vec3& axeN);

	spaceBox moving_spacebox(const spaceBox& space_box, const glm::vec3& move);
	paverInfo to_paver_info(const ycentredPaverInfo& paver);
	exactPaverInfo to_exact_paver_info(const transPaverInfo& paver);
	exactPaverInfo to_exact_paver_info(const paverInfo& paver);

};

namespace glm
{
	float normalize(float x);
};

namespace geom
{

	template<typename T>
	T homothety(const T& v, const T& old_center, float coeff)
	{
		return old_center + (v-old_center)*coeff;
	}

	template<typename T>
	std::vector<T> homothety(const std::vector<T>& v, const T& old_center, const T& new_center, float coeff)
	{
		std::vector<T> ret;

		for(const auto& e : v)
		{
			ret.push_back(new_center + (e-old_center)*coeff);
		}

		return ret;
	}
	template<typename T>
	std::vector<T> absolute_homothety(const std::vector<T>& v, const T& old_center, const T& new_center, float d)
	{
		std::vector<T> ret;

		for(const auto& e : v)
		{
			T dir = e-old_center;

			if(length(dir) < 0.0001f)
			{
				ret.push_back(new_center + dir);
			}
			else
			{
				ret.push_back(new_center + (dir+glm::normalize(dir)*d));
			}
		}

		return ret;
	}
	template<typename T>
	std::vector<T> barymetric_homothety(const std::vector<T>& v, const T& new_center, float coeff)
	{
		return homothety(v, mean(v), new_center, coeff);
	}

	template<typename T>
	std::vector<T> barycentred_homothety(const std::vector<T>& v, float coeff)
	{
		T m = mean(v);
		return homothety(v, m, m, coeff);
	}

	template<typename T>
	T mean(const std::vector<T>& v)
	{
		T s = T(0);

		for(const auto& e : v)
		{
			s = s + e;
		}

		return s / T(v.size());
	}

	template<typename T>
	std::vector<T> interpolate(const T& v1, const T& v2, uint amount)
	{
		std::vector<T> ret;

		if(amount == 0)
		{
			return {};
		}

		if(amount == 1)
		{
			return {(v1 + v2)/T(2)};
		}

		for(uint i=0; i<amount; i++)
		{
			T coeff = T(i)/T(amount-1);

			ret.push_back(v1 + (v2 - v1)*coeff);
		}

		return ret;
	}

	template<typename T>
	std::vector<T> translate(const std::vector<T>& v, const T& traslation)
	{
		std::vector<T> ret;

		for(const auto& e : v)
		{
			ret.push_back(e + traslation);
		}

		return ret;
	}
};

#endif //_GEOM_HPP_
