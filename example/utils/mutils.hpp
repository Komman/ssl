#ifndef _MUTILS_HPP_
#define _MUTILS_HPP_

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <utility>
#include <list>
#include <unordered_set>

#include <glm/glm.hpp>

#include "utils_debug.hpp"

#define GETTER_BUILDER(type, var) const type& get_ ## var () const {return _ ## var;}
#define GETTER_BUILDER_COPY(type, var)  type  get_ ## var () const {return _ ## var;}
#define SETTER_BUILDER(type, var) void set_ ## var (const type& value)  {_ ## var = value;}
#define SETTER_BUILDER_COPY(type, var) void set_ ## var (type value)  {_ ## var = value;}

template<typename... ArgsType>
void bouh(const ArgsType&... args)
{
	((std::cout<<args<<" "), ...);
	std::cout<<std::endl;
}

namespace utils
{
	// min and max
	template<typename T>
	void sort(T* e1, T* e2);
	template<typename T>
	void permute_values(T* e1, T* e2);

	// first power of two that is greater than x
	uint ulog(float x);

	int circulator(int x, int imax, int imin=0);
	bool k_chance_on_n(unsigned int k, unsigned int n);
	bool one_chance_on(unsigned int n);	

	bool is_float(const std::string& str);
	bool is_int(const std::string& str);
	bool is_vec3(const std::string& str);
	// "(x,y,z)"
	glm::vec3 tovec3(const std::string& str);

	void print_vector(const std::vector<glm::vec3>& v);
	void print_vector(const std::vector<glm::vec2>& v);

	// CAN BE OPTIMISED
	template<typename T>
	bool has_duplicate(const std::vector<T>& v);
	template<typename T>
	uint occurences(const std::vector<T>& v, const T& e);
	// Returns if segment appears as a contigous subsequence of sequence
	template<typename T>
	bool subsegment(const std::vector<T>& sequence, const std::vector<T>& segment);

	template<typename T>
	std::pair<T, uint> max(const std::vector<T>& v);
	template<typename T>
	std::pair<T, uint> min(const std::vector<T>& v);
	template<typename T>
	T foldmax(const std::vector<T>& v);
	template<typename T>
	T foldmin(const std::vector<T>& v);

	std::vector<uint> occurences_vector(const std::vector<uint>& v1);
	// O(max(v1.size() v2.size(), max_element(v1), max_element(v2)))
	bool unorderd_vector_equals(const std::vector<uint>& v1, const std::vector<uint>& v2);

	template<typename T>
	std::unordered_set<T> vectounset(const std::vector<T>& v);

	template<typename T>
	T square(T x);

	template<typename T>
	T linear_animator(const T& x, const T& init, const T& final, float start, float stop, float time);
	template<typename T>
	void push_after_it(std::list<T>* l, const typename std::list<T>::iterator& it, const T& e);

	template<typename T>
	void push_vector(std::vector<T>& v, const std::vector<T>& toadd);

	template<typename T>
	std::vector<T> vector_range(T first, uint count, T step);
	std::vector<uint> uirange(uint first, uint last);
	std::vector<uint> uirange(const std::pair<uint, uint>& range);
	std::vector<int>  irange(int first, int last);
	std::vector<uint> map_vector(const std::vector<uint>& v, const std::vector<uint>& mapping_table); 

	template<typename T>
	void invert(std::vector<T>& v);
	template<typename T>
	void swapop(std::vector<T>& v, unsigned int index);
	template<typename T>
	T average(const std::vector<T>& v);
	template<typename T>
	std::vector<T> concatenate(const std::vector<T>& v1, const std::vector<T>& v2);
	template<typename T>
	std::vector<T> neg(const std::vector<T>& v);


	// Returns the amout of filled cases
	// The map must be rectangular
	template<typename T>
	uint fill_bordered_map(std::vector<std::vector<T>>& map, T filling_value, T border_value, const glm::uvec2& start);

	// Neisbours ^<v> (cross)
	std::vector<glm::uvec2> get_neighbours(const glm::uvec2& p, const glm::uvec2& max_size);

	void print_vectorvec3(const std::vector<glm::vec3>& v);

	template<typename T>
	void print_vecvector(const std::vector<std::vector<T>>& v);
	template<typename T>
	void print_list(const std::list<T>& l);
	
	void print_vecvector_niceint(const std::vector<std::vector<int>>& v);
	void print_vecvector_niceint_map1D(const std::vector<int>& v, unsigned int sizex);

	// Returns if v1 == v2
	template<typename T>
	bool vector_unordered_inclusion(const std::vector<T>& v1, const std::vector<T>& v2);

	// Returns if v1 C v2
	template<typename T>
	bool vector_unordered_equality(const std::vector<T>& v1, const std::vector<T>& v2);

	// Returns the index of the set, and -1 else
	template<typename T>
	int vector_unordered_in_set(const std::vector<T>& v, const std::vector<std::vector<T>>& set);

	// Returns the index of the set, and -1 else
	template<typename T>
	int vector_unordered_included_in_set(const std::vector<T>& v, const std::vector<std::vector<T>>& set);

	// Pop n random elements from v and returns them.
	// Returns an empty vector if it is not possible to choose n elements
	template<typename T>
	std::vector<T> extract_random_elements(std::vector<T>& v, unsigned int n);
	template<typename T>
	std::vector<T> select_indexed(const std::vector<T>& v, const std::vector<uint>& indices);
};

namespace testint
{
	bool is_zero(int x);
	bool non_zero(int x);
	bool is_max(int x);

	template<int min, int max>
	inline bool in_range(int x)
	{
		return (x >= min) && (x <= max);
	}
	template<int value>
	inline bool equal(int x)
	{
		return (x == value);
	}
	template<int value>
	inline bool diff(int x)
	{
		return (x != value);
	}
	template<int value>
	inline bool ge(int x)
	{
		return (x >= value);
	}
	template<int value>
	inline bool le(int x)
	{
		return (x <= value);
	}
};






namespace utils
{
	template<typename T>
	std::vector<T> neg(const std::vector<T>& v)
	{
		std::vector<T> ret;

		for(const auto& e : v)
		{
			ret.push_back(!e);
		}

		return ret;
	}

	template<typename T>
	std::vector<T> concatenate(const std::vector<T>& v1, const std::vector<T>& v2)
	{
		std::vector<T> ret = v1;
		ret.insert(ret.end(), v2.begin(), v2.end());
		return ret;
	}

	template<typename T>
	void permute_values(T* e1, T* e2)
	{
		T tmp = *e1;
		*e1 = *e2;
		*e2 = tmp;
	}

	template<typename T>
	void sort(T* e1, T* e2)
	{
		if(*e1 > *e2)
		{
			permute_values<T>(e1, e2);
		}
	}

	template<typename T>
	std::pair<T, uint> max(const std::vector<T>& v)
	{
		if(v.size() == 0)
		{
			utils::err("T max(const std::vector<T>& v): v is empty");
		}

		std::pair<T, uint> m = {v[0], 0};

		for(uint i=1; i<v.size(); i++)
		{
			if(v[i] > m.first)
			{
				m.second = i;
				m.first  = v[i];
			}
		}

		return m;
	}

	template<typename T>
	std::pair<T, uint> min(const std::vector<T>& v)
	{
		if(v.size() == 0)
		{
			utils::err("T min(const std::vector<T>& v): v is empty");
		}

		std::pair<T, uint> m = {v[0], 0};

		for(uint i=1; i<v.size(); i++)
		{
			if(v[i] < m.first)
			{
				m.second = i;
				m.first  = v[i];
			}
		}

		return m;
	}


	template<typename T>
	T foldmax(const std::vector<T>& v)
	{
		if(v.size() == 0)
		{
			utils::err("utils::foldmax(): vector is empty");
		}
		T ret = v[0];

		for(const T& e : v)
		{
			ret = glm::max(ret, e);
		}

		return ret;
	}

	template<typename T>
	T foldmin(const std::vector<T>& v)
	{
		if(v.size() == 0)
		{
			utils::err("utils::foldmin(): vector is empty");
		}
		T ret = v[0];

		for(const T& e : v)
		{
			ret = glm::min(ret, e);
		}

		return ret;
	}

	template<typename T>
	bool has_duplicate(const std::vector<T>& v)
	{
		for(uint i=0; i<v.size(); i++)
		{
			for(uint j=i+1; j<v.size(); j++)
			{
				if(v[i] == v[j])
				{
					return true;
				}
			}
		}

		return false;
	}

	template<typename T>
	std::unordered_set<T> vectounset(const std::vector<T>& v)
	{
		std::unordered_set<T> ret;

		for(const auto& e : v)
		{
			ret.insert(e);
		}

		return ret;
	}

	template<typename T>
	inline T square(T x)
	{
		return x*x;
	}

	inline int circulator(int x, int imax, int imin)
	{
		if(x<0)
			return imax - abs(x);
		return x % imax;
	}

	template<typename T>
	inline T linear_animator(const T& x, const T& init, const T& final, float start, float stop, float time)
	{
		if(time < start || time > stop)
		{
			return x;
		}
		float coeff = (time-start)/(stop - start);
		return init*(1.0-coeff) + final*coeff;
	}

	template<typename T>
	inline void push_after_it(std::list<T>* l, const typename std::list<T>::iterator& it, const T& e)
	{
		auto it2 = it;
		it2++;
		l->insert(it2, e);
	}

	inline bool k_chance_on_n(unsigned int k, unsigned int n)
	{
		return (rand()%n) < k;
	}

	inline bool one_chance_on(unsigned int n)
	{
		return k_chance_on_n(1, n);
	}

	template<typename T>
	void push_vector(std::vector<T>& v, const std::vector<T>& toadd)
	{
		for(const auto e : toadd)
		{
			v.push_back(e);
		}
	}

	template<typename T>
	void swapop(std::vector<T>& v, unsigned int index)
	{
		#ifdef SSL_DEBUG
		if(index >= v.size())
		{
			utils::err("swapop: try to pop index " + std::to_string(index) + " while size is " + std::to_string(v.size()));
		}
		#endif

		if(index < v.size()-1)
		{
			v[index] = std::move(v[v.size()-1]);
		}

		v.pop_back();
	}


	// Returns if v1 C v2
	template<typename T>
	inline bool vector_unordered_inclusion(const std::vector<T>& v1, const std::vector<T>& v2)
	{
		for(const auto& e : v1)
		{
			if(std::find(v2.begin(), v2.end(), e) == v2.end())
			{
				return false;
			}
		}

		return true;
	}

	// Returns if v1 = v2
	template<typename T>
	inline bool vector_unordered_equality(const std::vector<T>& v1, const std::vector<T>& v2)
	{
		if(v2.size() != v1.size())
		{
			return false;
		}

		return vector_unordered_inclusion(v1, v2);
	}

	// Returns the index of the set, and -1 else
	template<typename T>
	inline int vector_unordered_in_set(const std::vector<T>& v, const std::vector<std::vector<T>>& set)
	{
		for(int i=0; i<(int)set.size(); i++)
		{
			if(vector_unordered_equality(v, set[i]))
			{
				return i;
			}
		}
		return -1;
	}

	// Returns the index of the set, and -1 else
	template<typename T>
	inline int vector_unordered_included_in_set(const std::vector<T>& v, const std::vector<std::vector<T>>& set)
	{
		for(int i=0; i<(int)set.size(); i++)
		{
			if(vector_unordered_inclusion(v, set[i]))
			{
				return i;
			}
		}
		return -1;
	}

	template<typename T>
	void print_vecvector(const std::vector<std::vector<T>>& v)
	{
		std::cout<<"{"<<std::endl;
		for(auto& vx : v)
		{
			std::cout<<" {";
			for(auto& e : vx)
			{
				std::cout<<e<<", ";
			}
			std::cout<<"},"<<std::endl;
		}
		std::cout<<"}"<<std::endl;
	}

	template<typename T>
	void print_list(const std::list<T>& l)
	{
		std::cout<<"{";
		for(const auto& e : l)
		{
			std::cout<<e<<", ";
		}
		std::cout<<"}"<<std::endl;
	}


	template<typename T>
	std::vector<T> extract_random_elements(std::vector<T>& v, unsigned int n)
	{
		if(v.size() < n)
		{
			return {};
		}

		std::vector<T> ret;

		for(unsigned int i=0; i<n; i++)
		{
			auto itr = v.begin() + (rand() % v.size());
			ret.push_back(*itr);
			v.erase(itr);
		}

		return ret;
	}


	template<typename T>
	T average(const std::vector<T>& v)
	{
		T s = (T)(0);

		for(auto& e : v)
		{
			s = s + e;
		}

		return s / (T)(v.size());
	}


	template<typename T>
	uint fill_bordered_map(std::vector<std::vector<T>>& map, T filling_value, T border_value, const glm::uvec2& start)
	{
		uint ret = 0;

		if(start.x >= map.size() || start.y >= map[start.x].size())
		{
			utils::err("fill_bordered_map(): start out of map");
		}
		glm::uvec2 map_size = glm::uvec2(map.size(), map[start.x].size());

		if(map[start.x][start.y] == border_value)
		{
			return ret;
		}
		ret++;

		std::vector<glm::uvec2> to_visit = {start};

		while(to_visit.size() > 0)
		{
			auto pos = to_visit[to_visit.size()-1];
			to_visit.pop_back();

			if(map[pos.x][pos.y] != border_value && map[pos.x][pos.y] != filling_value)
			{
				ret++;
				map[pos.x][pos.y] = filling_value;

				auto neigh = get_neighbours(pos, map_size);
				for(auto& n : neigh)
				{
					if(map[n.x][n.y] != border_value && map[n.x][n.y] != filling_value)
					{
						to_visit.push_back(n);
					}
				}
			}
		}

		return ret;
	}

	template<typename T>
	std::vector<T> vector_range(T first, uint count, T step)
	{
		std::vector<T> ret;
		T e = first;

		for(uint i=0;  i<count; i++)
		{
			ret.push_back(e);
			e = e + step;
		} 
		
		return ret;
	}

	template<typename T>
	void invert(std::vector<T>& v)
	{
		for(uint i=0; i<v.size()/2; i++)
		{
			uint opposite = v.size() - 1 - i;
			T opp = v[opposite];
			v[opposite] = v[i];
			v[i] = opp;
		}
	}


	template<typename T>
	uint occurences(const std::vector<T>& v, const T& e)
	{
		uint s = 0;
		for(const auto& ev : v)
		{
			if(ev == e)
			{
				s++;
			}
		}

		return s;
	}

	template<typename T>
	bool subsegment(const std::vector<T>& sequence, const std::vector<T>& segment)
	{
		if(segment.size() == 0)
		{
			return true;
		}

		for(uint i=0; i<(sequence.size()-segment.size()+1); i++)
		{
			uint j;
			for(j=0; j<segment.size(); j++)
			{
				if(sequence[i+j] != segment[j])
				{
					break;
				}
			}

			if(j == segment.size())
			{
				return true;
			}
		}

		return false;
	}


	template<typename T>
	std::vector<T> select_indexed(const std::vector<T>& v, const std::vector<uint>& indices)
	{
		std::vector<T> ret;

		for(auto i : indices)
		{
			ret.push_back(v.at(i));
		}

		return ret;
	}
};

#endif //_MUTILS_HPP_
