#ifndef _COLIGRID_HPP_
#define _COLIGRID_HPP_

#include <glm/glm.hpp>
#include <vector>
#include <string>

class Coligrid
{
public:
	static constexpr int RESERVED_VALUE = std::numeric_limits<int>::max();

public:
	// Filled with 0, and the RESERVED_VALUE is reserved 
	Coligrid(const glm::uvec2& dimension);

	void set_point(const glm::uvec2& coord, int value);
	// Error when coord s not in bounds
	void safe_set_point(const glm::uvec2& coord, int value);
	void set_circle(const glm::vec2& center, float radius, int value);
	void set_thin_line(const glm::vec2& start, const glm::vec2& stop, int value);
	void set_empty_polygon(const std::vector<glm::vec2>& points, int value);
	void set_filled_polygon(const std::vector<glm::vec2>& points, const glm::vec2& fill_start, int value);
	// Works when the barycenter of the polygon is in the polygon
	void set_convex_polygon(const std::vector<glm::vec2>& points, int value);
	void fill(const glm::vec2& start, int value, bool (*is_border)(int));
	void fill(const glm::vec2& start, int value, const std::vector<int>& border_values);
	void fill(const glm::vec2& start, int value);
	void fill_discrete(const glm::uvec2& start, int value, int border_value);
	void fill_discrete(const glm::uvec2& start, int value);
	void fill_force(const glm::vec2& start, int value);
	void replace_value(int oldv, int newv);

	/*
		The shapes must have collision with an unique
		value in free_area.
	*/
	bool collision_area(const std::vector<glm::uvec2>& area, const std::vector<int>& free_area = {0}) const;
	bool collision_circle(const glm::vec2& center, float radius, const std::vector<int>& free_area = {0}) const;
	bool collision_thin_line(const glm::vec2& start, const glm::vec2& stop, const std::vector<int>& free_area = {0}) const;
	bool collision_empty_polygon(const std::vector<glm::vec2>& points, const std::vector<int>& free_area = {0}) const;
	bool collision_filling(const glm::vec2& start, int value, const std::vector<int>& free_area = {0});
	bool collision_filled_polygon(const std::vector<glm::vec2>& points, const glm::vec2& fill_start, int value, const std::vector<int>& free_area = {0});
	bool collision_convex_polygon(const std::vector<glm::vec2>& points, int value, const std::vector<int>& free_area = {0});

	int  get_point(const glm::uvec2& coord) const;
	void print() const;

	int operator()(uint x, uint y) const;

	const glm::uvec2& get_dimensions() const;
	void print_ppm(const std::string& file) const;
	// value v is of color colors[v%colors.size()]
	void print_ppm(const std::string& file, const std::vector<std::string>& colors) const;

public:
	static std::vector<glm::uvec2> circle_points(const glm::uvec2& dimension, const glm::vec2& center, float radius);
	static std::vector<glm::uvec2> thin_line_points(const glm::uvec2& dimension, const glm::vec2& start, const glm::vec2& stop);
	static std::vector<glm::uvec2> polygonial_lines(const glm::uvec2& dimension, const std::vector<glm::vec2>& points);

protected:
	bool in_bounds(const glm::ivec2& p) const;

	void check(const glm::uvec2& coord) const;
	void check(const glm::vec2& coord) const;
	static void check_coord(const glm::uvec2& coord, const glm::uvec2& dimension);
	static void check_coord(const glm::vec2& coord, const glm::uvec2& dimension);

	/*
		is_border != NULL : Performs a fill and borders are
							indentified with the "is_border"
							function.
							Returns false

		is_border == NULL and border_values.size() > 0:
							Performs a fill and borders values
							are the values in "border_values".
							Returns false

		is_border == NULL and border_values.size()==0:
							Don't modify the grid, and
							returns true if and only
							if there is collision while
							filling the area.
	*/
	bool custom_fill(const glm::vec2& start, int value, bool (*is_border)(int), const std::vector<int>& border_values, const std::vector<int> free_area = {0});

private:
	std::vector<std::vector<int>> _grid;

	glm::uvec2 _dimensions;
};

#endif //_COLIGRID_HPP_
