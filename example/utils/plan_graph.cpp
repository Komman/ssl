#include "plan_graph.hpp"

#include <iostream>
#include <tuple>

#include <glm/gtx/string_cast.hpp>

#include "coligrid.hpp"
#include "geom.hpp"
#include "mutils.hpp"
#include "utils_debug.hpp"
#include "space_grid.hpp"

using namespace std;
using namespace glm;

PlanGraph::PlanGraph()
	: StoredGraph()
{

}

PlanGraph::PlanGraph(const PlanGraph& graph)
	: StoredGraph(graph)
{

}

std::pair<bool, uint> PlanGraph::build_planar_vertex(const glm::vec2& position, float mind)
{
	vector<uint> verts = this->all_vertices();

	for(uint i=0; i<verts.size(); i++)
	{
		glm::vec2 p = this->value(verts[i]);

		if(glm::distance(p, position) < mind)
		{
			return {false, verts[i]};
		}
	}

	return {true, this->add_vertex(position)};
}

std::vector<uint> PlanGraph::build_planar_edge(const Edge& e)
{
	return this->build_planar_edge(e.first, e.second);
}

std::vector<uint> PlanGraph::build_planar_edge(uint v1, uint v2)
{
	if(v1 == v2)
	{
		utils::err("PlanGraph::build_planar_edge(): self loop impossible for a planar graph");
	}

	std::vector<uint> ret;

	Segment2D s = {this->value(v1), this->value(v2)};
	
	vector<uint> verts = this->all_vertices();
	for(uint v : verts)
	{
		if(v != v1 && v != v2)
		{
			glm::vec2 p = this->value(v);

			if(geom::point_on_segment(s, p))
			{
				// bouh("subdivided call on", v1, v, v2);
				ret = this->build_planar_edge(v1, v);

				vector<uint> ret2 = this->build_planar_edge(v, v2);
				for(uint r : ret2) {ret.push_back(r);}

				return ret;
			}
		}
	}

	vector<Edge> edges = this->all_edges();

	for(const auto& e : edges)
	{
		if(e.first == v1 || e.second == v1 || e.first == v2 || e.second == v2)
		{
			continue;
		}

		Segment2D sp = {this->value(e.first), this->value(e.second)};
		lineIntersection inter = geom::segment_segment_intersection(s, sp);

		if(inter.intersection)
		{
			vec2 interp = geom::intersection_point(s, inter);

			vector<uint> verts = this->all_vertices();
			while(verts.size() > 0)
			{
				if(glm::distance(this->value(verts.back()), interp) < geom::EPSILON)
					break;
				verts.pop_back();
			}

			uint m;
			
			if(verts.size() == 0)
			{
				m = this->add_vertex(geom::intersection_point(s, inter));
				// bouh("wtf:", to_string(this->value(m)));
				ret.push_back(m);
			}
			else
			{
				m = verts.back();
			}

			if(e.first != m && e.second != m)
			{
				this->remove_edge(e);

				auto r1 = this->build_planar_edge({e.first, m});
				auto r2 = this->build_planar_edge({m, e.second});

				ret.insert(ret.end(), r1.begin(), r1.end());
				ret.insert(ret.end(), r2.begin(), r2.end());
			}

			if(v1 != m && v2 != m)
			{
				auto r3 = this->build_planar_edge({v1, m});
				auto r4 = this->build_planar_edge({m, v2});

				ret.insert(ret.end(), r3.begin(), r3.end());
				ret.insert(ret.end(), r4.begin(), r4.end());
			}
			else
			{
				auto r5 = this->build_planar_edge({v1, v2});
				ret.insert(ret.end(), r5.begin(), r5.end());
			}

			return ret;
		}
	}

	this->add_edge({v1, v2});
	return ret;
}


glm::vec2 PlanGraph::center(const std::vector<uint>& vertices) const
{
	vec2 ret = vec2(0);

	for(uint v : vertices)
	{
		ret += this->value(v);
	}

	return ret / float(vertices.size());	
}


float PlanGraph::distance(uint u, uint v)
{
	return this->distance(this->value(u),  v);
}

float PlanGraph::distance(const glm::vec2& point, uint v)
{
	return glm::distance(point,  this->value(v));
}

bool PlanGraph::is_crossing_edges(const Edge& e1, const Edge& e2) const
{
	if(e1.first  == e2.first || e1.first  == e2.second
	|| e1.second == e2.first || e1.second == e2.second)
	{
		return false;
	}

	if(!(e2 == e1) && !(e2 == mirror(e1)))
	{
		vec2 e1s  = this->value(e1.first);
		vec2 e1e  = this->value(e1.second);
		vec2 e2s = this->value(e2.first);
		vec2 e2e = this->value(e2.second);

		lineIntersection inter = geom::segment_segment_intersection(e1s, e1e, e2s, e2e);
		
		if(inter.intersection)
		{
			return true;
		}
	}

	return false;
}

float PlanGraph::nearest_vertex_distance(const Edge& e)
{
	if(!this->check_edge(e))
	{
		utils::err("PlanGraph::nearest_vertex(): edge " + e.to_string() + " does not exist");
	}

	auto vertices = this->all_vertices();

	float dmin = std::numeric_limits<float>::max();
	// uint vertex = e.first;

	for(auto u : vertices)
	{
		if(u == e.first || u == e.second)
			continue;
	
		float d = geom::distance_point_segment(this->value(e.first), this->value(e.second), this->value(u));
	
		if(d < dmin)
		{
			dmin = d;
			// vertex = u;
		}
	}

	return dmin;
}


bool PlanGraph::is_crossing_edge(const Edge& e) const
{
	auto edges = this->all_edges();

	for(auto ep : edges)
	{
		if(this->is_crossing_edges(e, ep))
		{
			return true;
		}
	}

	return false;
}

std::pair<bool, std::pair<PlanGraph::Edge, PlanGraph::Edge>> PlanGraph::is_planar() const
{
	vector<Edge> edges = this->all_edges();

	for(const Edge& e1 : edges)
	{
		for(const Edge& e2 : edges)
		{
			if(!this->connected(e1, e2))
			{
				if(this->is_crossing_edges(e1, e2))
				{
					return {false, {e1, e2}};
				}
			}
		}
	}

	return {true, {Edge{0,0}, Edge{0,0}}};
}

uint PlanGraph::nearest(const glm::vec2& point, const std::vector<uint>& vertices)
{
	if(vertices.size() == 0)
	{
		utils::err("PlanGraph::nearest(): wrong vertices passed as argument");
	}

	uint near = 0;
	float dmin = std::numeric_limits<float>::max();

	for(uint u : vertices)
	{
		if(u == SKIP || !this->check_vertex(u))
			continue;

		float d = this->distance(point, u);

		if(d < dmin)
		{
			dmin = d;
			near = u;
		}
	}

	if(dmin == std::numeric_limits<float>::max())
	{
		utils::err("PlanGraph::nearest(): no good vertix passed as argument");
	}

	return near;
}


uint PlanGraph::nearest(uint v, const std::vector<uint>& vertices)
{
	return this->nearest(this->value(v), vertices);
}

float PlanGraph::angle(const Edge& e1, const Edge& e2) const
{
	return geom::angle(this->value(e1.second) - this->value(e1.first),
					   this->value(e2.second) - this->value(e2.first));
}


uint PlanGraph::angulext_neighbour(uint v, uint n, bool trigo) const
{
	auto alln = this->neighbours(v);

	float trico = (trigo ? 1.0 : -1.0);
	float mina = 8.0f;
	uint  next = n;
	bool first = true;

	for(auto nei : alln)
	{
		if(nei != n && nei != v)
		{
			float angle = geom::angle(this->value(nei) - this->value(v),
									  this->value(n)   - this->value(v));

			if(first)
			{
				mina = angle;
				next = nei;
				first = false;
			}

			if(angle*trico < mina*trico)
			{
				mina = angle;
				next = nei;
			}
		}		
	}

	return next;
}

void PlanGraph::remove_close_vertices(float distance, const std::unordered_set<uint>& dontremove)
{
	auto vertices = this->all_vertices();
	
	for(uint u : vertices)
	{
		for(uint v : vertices)
		{
			if(!this->check_vertex(v) || !this->check_vertex(u))
				continue;
			
			if(u != v && this->distance(u,v) <= distance && dontremove.find(v) == dontremove.end())
			{
				this->remove_vertex(v);
			}
		}
	}
}

std::string PlanGraph::string_value(const Edge& e) const
{
	return "{" + glm::to_string(this->value(e.first )) + ", "
			   + glm::to_string(this->value(e.second)) + "}";
}

#include "../../ssl/src/utils.hpp"
#include "../../ssl/src/autoprint_recorder.hpp"


std::vector<std::vector<uint>> PlanGraph::all_faces() const
{
	std::vector<std::vector<uint>> ret;

	std::vector<uint> vertices = this->all_vertices();

	#ifdef UTILS_DEBUG
	auto planarty = this->is_planar();
	if(!planarty.first)
	{
		std::string erf = "graph_non_planar.ppm";

		auto drawing = this->compute_drawing(3000);

		drawing.set_thin_line(this->value(planarty.second.first.first ), this->value(planarty.second.first.second ), 4);
		drawing.set_thin_line(this->value(planarty.second.second.first), this->value(planarty.second.second.second), 4);
		drawing.set_circle(this->value(planarty.second.first .first ), 0.08f, 5);
		drawing.set_circle(this->value(planarty.second.first .second), 0.08f, 5);
		drawing.set_circle(this->value(planarty.second.second.first ), 0.08f, 5);
		drawing.set_circle(this->value(planarty.second.second.second), 0.08f, 5);

		drawing.print_ppm(erf);

		utils::err("PlanGraph::all_faces(): graph is not planar because of edges "
		+ planarty.second.first.to_string() + " and " + planarty.second.second.to_string()
		+ " of coordinates " + this->string_value(planarty.second.first) 
		+ " -> " + this->string_value(planarty.second.second) 
		+ "\n(see " + erf + ")");
	}
	#endif

	for(uint v : vertices)
	{
		auto fs = this->faces(v);

		for(const auto& f : fs)
		{
			bool newface = true;
			
			for(const auto& fr : ret)
			{
				if(utils::unorderd_vector_equals(fr, f))
				{
					newface = false;
					break;
				}
			}

			if(newface)
			{
				ret.push_back(f);
			}
		}
	}

	#warning Not accurate if there is severals connected components	
	if(ret.size() == 1)
	{
		auto v = ret[0];
		utils::invert(v);
		ret.push_back(v);
	}
	
	return ret;
}


std::vector<std::vector<uint>> PlanGraph::faces(uint v) const
{
	std::vector<std::vector<uint>> ret;
	vector<uint> nei = this->neighbours(v);

	for(uint i=0; i<nei.size(); i++)
	{
		uint n = nei[i];
		auto f = this->face(v, n, true);

		ret.push_back(f);
	}

	return ret;
}

bool PlanGraph::is_outer_face(const std::vector<uint>& face) const
{
	vector<uint> outf = this->outer_face();

	if(utils::unorderd_vector_equals(outf, face))
	{
		return true;
	}
	return false;
}


std::vector<uint> PlanGraph::face(uint v, uint neibour, bool trigo) const
{
	#ifdef UTILS_DEBUG
	std::unordered_set<glm::vec2, hashVec2> coords;
	auto alvert = this->all_vertices();
	for(auto& v : alvert)
	{
		glm::vec2 c = this->value(v);
		if(coords.find(c) != coords.end())
		{
			utils::err(" PlanGraph::face(): risk of not termining because two vertices have the same coordinates: "
				+ glm::to_string(c));
		}
		coords.insert(c);
	}
	#endif

	if(v == neibour)
	{
		utils::err("Try do compute face from a vertex directed to himself");
	}
	if(!utils::in_vector(this->neighbours(v), neibour))
	{
		utils::err("PlanGraph::face(): n is not a neighbour");
	}

	std::vector<uint> ret = {neibour};
	uint prev = v;
	uint next = this->angulext_neighbour(v, neibour, trigo);

	#ifdef UTILS_EXPENSIVE_DEBUG
	if(!(this->is_planar().first))
	{
		std::string erf = "graph_non_planar.ppm";
		this->print_ppm(erf, 500);
		utils::err("PlanGraph::face(): graph is not planar, (see " + erf + ")");
	}
	#endif

	while(!(next == v && prev == neibour))
	{
		// bouh("next:", next);
		// bouh("prev:", prev);
		// bouh("neibour:", neibour);
		ret.push_back(prev);

		uint nnext = this->angulext_neighbour(next, prev, trigo);

		prev = next;
		next = nnext;
	}

	return ret;
}

std::vector<PlanGraph::Edge> PlanGraph::circle_intersection(const circleInfo& circle)
{
	std::vector<Edge> ret;
	std::vector<Edge> edges = this->all_edges();

	for(const auto& e : edges)
	{
		if(geom::circle_segment_intersection(circle, {this->value(e.first), this->value(e.second)}))
		{
			ret.push_back(e);
		}
	}

	return ret;
}


void PlanGraph::delaunay_triangulation()
{
	this->stable();

	auto vertices = this->all_vertices();
	
	for(uint u : vertices)
	{
		for(uint v : vertices)
		{
			if(v == u) continue;

			for(uint w : vertices)
			{	
				if(w == u || w == v) continue;

				vec2 points[] = {this->value(u),
							     this->value(v),
							     this->value(w)};
				circleInfo circircle = geom::circumscribed_circle(points);
			
				if(circircle.radius < 0)
				{
					continue;
				}
				else
				{
					uint i = 0;
					for(i=0; i<vertices.size(); i++)
					{
						if(vertices[i] != u && vertices[i] != v && vertices[i] != w)
						{
							if(glm::distance(circircle.center, this->value(vertices[i])) < circircle.radius)
							{
								break;
							}
						}
					}

					if(i == vertices.size())
					{
						this->add_edge({u,v});
						this->add_edge({v,w});
						this->add_edge({w,u});
					}
				}
			}
		}
	}
}

void PlanGraph::remove_self_loops()
{
	auto vertices = this->all_vertices();

	for(auto v : vertices)
	{
		this->remove_edge({v,v});
	}
}

std::vector<uint> PlanGraph::outer_face() const
{
	std::vector<uint> vertices = this->all_vertices();
	
	if(vertices.size() == 0)
	{
		utils::err("PlanGraph::outer_face(): no vertices");
	}
	uint wester=vertices[0];

	for(uint id : vertices)
	{
		if(this->value(id).x < this->value(wester).x)
		{
			wester = id;
		}
	}
	std::vector<uint> neis = this->neighbours(wester);

	if(neis.size() == 0)
	{
		utils::err("PlanGraph::outer_face(): graph disconnected");
	}
	uint next=neis[0];

	for(uint id : neis)
	{
		if(this->value(id).x < this->value(next).x)
		{
			next = id;
		}
	}

	bool trigo = (this->value(wester).y > this->value(next).y);

	return this->face(next, wester, !trigo);
}

void PlanGraph::set_chicken_triangulation()
{
	this->stable();
	this->set_full_chicken_triangulation();
}

void PlanGraph::set_full_chicken_triangulation()
{
	auto vertices = this->all_vertices();

	for(uint u : vertices)
	{
		for(uint v : vertices)
		{
			if(u < v)
			{
				vec2 middle = (this->value(u) + this->value(v))/2.0f;

				uint near = this->nearest(middle, vertices);

				if((near == u || near == v))
				{
					this->add_edge({u,v});
				}
			}
		}
	}
}

void PlanGraph::remove_big_degree(uint min_degree, const std::unordered_set<uint>& dontremove)
{
	bool end = false;

	while(!end)
	{
		auto vertices = this->all_vertices();
		end = true;

		for(uint u : vertices)
		{
			if(this->neighbours(u).size() >= min_degree && dontremove.find(u) == dontremove.end())
			{
				this->remove_vertex(u);
				end = false;
				break;
			}
		}
	}
}


void PlanGraph::remove_small_degree(uint max_degree, const std::unordered_set<uint>& dontremove)
{
	bool end = false;

	while(!end)
	{
		auto vertices = this->all_vertices();
		end = true;

		for(uint u : vertices)
		{
			if(this->neighbours(u).size() <= max_degree && dontremove.find(u) == dontremove.end())
			{
				this->remove_vertex(u);
				end = false;
				break;
			}
		}
	}
}


void PlanGraph::remove_border(const std::vector<uint>& border)
{
	bool trigo = geom::trigo_way(this->value(border));

	for(uint i=0; i<border.size(); i++)
	{
		uint j  = (i+1)%border.size();
		uint jn = (i+2)%border.size();
		uint next = this->angulext_neighbour(border[j], border[i], !trigo);
					
		while(next != border[jn])
		{		
			this->remove_edge({border[j], next});
			next = this->angulext_neighbour(border[j], border[i], !trigo);

			if(next == border[i] || this->neighbours(border[j]).size() <= 1)
			{
				utils::err("PlanGraph::remove_border(): border is not a cycle");
			}
		}
	}
}


PlanDualGraph PlanGraph::compute_dual() const
{
	PlanDualGraph g;

	vector<vector<uint>> faces = this->all_faces();

	for(const vector<uint>& f : faces)
	{
		vector<glm::vec2> cycle  = this->value(f);
		vector<uint>      cyclei = f;

		if(!geom::trigo_way(cycle))
		{
			utils::invert(cyclei);
		}

		std::vector<Edge> edges = edges_cycle(cyclei);

		g.add_vertex(planDualFace{
			.position = this->center(cyclei),
			.edges    = edges});
	}

	vector<Edge> edges     = this->all_edges(); 
	vector<uint> all_faces = g.all_vertices();

	for(const auto& e : edges)
	{
		vector<uint> adj_faces;

		for(uint f : all_faces)
		{
			vector<Edge> fvertices = g[f].edges;

			if(utils::in_vector(fvertices, e) || utils::in_vector(fvertices, mirror(e)))
			{
				adj_faces.push_back(f);
			}
		}

		if(adj_faces.size() > 2)
		{
			utils::err("PlanGraph::compute_dual(): edge on more than 2 faces");
		}

		if(adj_faces.size() == 2)
		{
			g.add_edge({adj_faces[0], adj_faces[1]});
		}
	}

	return g;
}

spaceBox2D PlanGraph::space_box() const
{
	std::vector<uint> vertices = this->all_vertices();

	if(vertices.size() == 0)
	{
		utils::err("PlanGraph::space_box(): no vertices");
	}

	spaceBox2D ret = {
		.min = this->value(vertices[0]),
		.max = this->value(vertices[0])
	};

	for(uint i=1; i<vertices.size(); i++)
	{
		ret.cover(this->value(vertices[i]));
	}

	return ret;
}

SpaceGrid PlanGraph::compute_drawing(uint xdimension) const
{
	spaceBox2D box = this->space_box();
	glm::vec2 center = (box.min + box.max)/2.0f;
	box.min = center + (box.min - center)*1.05f;
	box.max = center + (box.max - center)*1.05f;
	float diam = glm::distance(box.min, box.max);

	SpaceGrid drawing(box.min, box.max, xdimension);

	std::vector<Edge> edges = this->all_edges();

	for(const auto& e : edges)
	{
		glm::vec2 segment[2] = {
			this->value(e.first),
			this->value(e.second)
		};

		drawing.set_thin_line(segment[0], segment[1], 3);
		drawing.set_circle(segment[0], diam*0.003f, 1);
		drawing.set_circle(segment[1], diam*0.003f, 1);
	}

	return drawing;
}

void PlanGraph::print_ppm(const std::string& path, uint xdimension) const
{
	SpaceGrid draw = this->compute_drawing(xdimension);
	draw.print_ppm(path);
}


