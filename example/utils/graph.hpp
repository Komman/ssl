#ifndef _GRAPH_HPP_
#define _GRAPH_HPP_

#include <vector>
#include <unordered_set>

#include "basic_graph.hpp"
#include "updatable_struct.hpp"


class Graph : public BasicGraph
{
public:
	Graph();
	Graph(const Graph& g);

	// O(1)
	uint size() const;
	// O(1)
	uint cpu_adjency_size() const;
	void print() const;

	// O(1)
	bool connected(uint u, uint v) const;
	// O(1), returns if e1 and e2 have at eat one common vertex
	bool connected(const Edge& e1, const Edge& e2) const;

	// O(size)
	std::vector<uint> all_vertices() const;
	// O(size*size), non-oriented edges: (lower, greater)
	const std::vector<Edge>& all_edges() const;

	// O(size), the new vertex keeps the id of "tokeep", and the vertex "toremove" is removed
	// If the graph is oriented, may not work properly
	void unoriented_fusion_vertices_no_selfloop(uint tokeep, uint toremove);
	
	// O(size)   ----> Can be optimized
	std::vector<uint> neighbours(uint vertex) const;
	// O(size)   ----> Can be optimized
	uint degree(uint vertex) const;
	// O(size*size)
	std::unordered_set<uint> DFS(uint start);

	// Returns the id of the vertex
	uint add_vertex();

	// O(1)
	void add_edge(const Edge& e);
	// O(1)
	void add_oriented_edge(const Edge& e);

	// O(1)
	void remove_vertex(uint vertex);
	void remove_oriented_edge(const Edge& e);
	void remove_edge(const Edge& e);
	// Remove every edge, O(size*size)
	void stable();

	void add_path(const std::vector<uint>& border);

	bool check_vertex(uint index) const;
	bool check_edge(const Edge& e) const;

private:
	std::vector<std::vector<bool>> _adjency;
	std::unordered_set<uint> _deads;

	// NON ORIENTED
	mutable datedStruct<std::vector<Edge>> _edgecache;
};

#endif //_GRAPH_HPP_
