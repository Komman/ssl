#ifndef _SHAPES_HPP_
#define _SHAPES_HPP_

#include <glm/glm.hpp>
#include <vector>
#include <string>
#include <array>
#include <glm/gtx/string_cast.hpp>

#include "utils_debug.hpp"
#include "mutils.hpp"

using uint = unsigned int;

template<typename Type>
using Pair = std::array<Type,2>;

struct triLine
{
	glm::vec3 p1;
	glm::vec3 p2;
	float radius;
};

struct circleInfo
{
	glm::vec2 center;
	float     radius;

	std::string to_string() const;
	bool inside(const glm::vec2& point) const;
};

struct circleInfoPlanY
{
	glm::vec3 center;
	float     radius;
};

struct orthHomothety
{
	glm::vec3 center;
	float scale;
};

struct polygon2DInfo
{
	glm::vec2 inside;
	std::vector<glm::vec2> points;
};

struct sphereInfo
{
	glm::vec3 center;
	float     radius;
};

struct prismInfo
{
	glm::vec3 points[4];
};

template<typename Type>
struct typedSpaceBox
{
	Type min;
	Type max;

	void cover_translation(const Type& translation);
	void cover(const Type& point);
	void cover(const std::vector<Type>& points);
	typedSpaceBox operator|(const typedSpaceBox& s) const;
	bool operator<=(const typedSpaceBox& s) const;
	bool operator<(const typedSpaceBox& s) const;
	std::string to_string() const;
};

using spaceBox3D = typedSpaceBox<glm::vec3>;
using spaceBox2D = typedSpaceBox<glm::vec2>;
using spaceBox = spaceBox3D;

struct paverInfo
{
	glm::vec3 bottom;
	glm::vec3 sized_x;
	glm::vec3 sized_y;
	glm::vec3 sized_z;
};

struct ycentredPaverInfo
{
	glm::vec3 bottomy;
	glm::vec3 sized_x;
	glm::vec3 sized_y;
	glm::vec3 sized_z;
};

struct spaceBase
{
	glm::vec3 dirx;
	glm::vec3 diry;
	glm::vec3 dirz;
};

struct exactPaverInfo
{
	glm::vec3 square_base_up[4];
	glm::vec3 square_base_down[4];
};

using Segment3D = std::pair<glm::vec3, glm::vec3>; 
using Segment2D = std::pair<glm::vec2, glm::vec2>; 
using Parallelogram = std::array<glm::vec2, 4>; 

struct transPaverInfo
{
	glm::vec3 square_base[4];
	glm::vec3 sized_direction;
};

struct centredPaverInfo
{
	glm::vec3 center;
	glm::vec3 sized_x;
	glm::vec3 sized_y;
	glm::vec3 sized_z;
};

struct paverInfoN
{
	glm::vec3 bottom;
	glm::vec3 dir_xN;
	glm::vec3 dir_yN;
	glm::vec3 dir_zN;
	glm::vec3 size;
};

struct lineIntersection
{
	bool  intersection;
	float distance_from_start;
};

struct pointIntersection
{
	bool  intersection;
	glm::vec3 point;
};

struct base2D
{
	glm::vec3 dirxN;
	glm::vec3 diryN;
	glm::vec3 origin;
};

struct infinitePlan
{
	glm::vec3 point;
	glm::vec3 normalN;
};

struct orientedPlan
{
	glm::vec3 point;
	glm::vec3 dirxN;
	glm::vec3 diryN;
};

struct collisionImpact
{
	bool      impact;
	float     distance; // Distance to reach to the impact
	glm::vec3 point; // Point of the impact
	glm::vec3 normalN;

	std::string to_string() const;
};

struct directedLine3D
{
	glm::vec3 point;
	glm::vec3 dirN;
};

struct directedLine2D
{
	glm::vec2 point;
	glm::vec2 dirN;
};

struct rectriangleEntries
{
	std::vector<glm::vec2> rectriangle;
	std::pair<uint, uint> entry1;
	std::pair<uint, uint> entry2;
};

// edge[i] refers to edge i->(i+1)%cycle.size()
template<typename Type>
struct partialCycle
{
	partialCycle();
	partialCycle(const partialCycle& pcycle);
	partialCycle(const std::vector<Type>& cycle);
	partialCycle(const std::vector<Type>& cycle, const std::initializer_list<uint>& cutedges);
	partialCycle(const std::vector<Type>& cycle, const std::vector<uint>& cutedges);
	partialCycle(const std::vector<Type>& cycle, const std::vector<bool>& edgecut);

	std::vector<Type> cycle;
	std::vector<bool> edge;

	std::vector<std::vector<uint>> paths(bool outer) const;
	std::vector<std::vector<uint>> outer_paths() const;
	std::vector<std::vector<uint>> inner_paths() const;
	uint size() const;
	bool aligned() const;
	bool partial() const;
	bool is_cycle() const;
	void check() const;
	bool same_origin(const partialCycle<Type>& cycle) const;
	std::string to_string() const;
};

template<typename Type>
struct partialEnlargement
{
	partialCycle<Type> outline;
	// Trigo order
	std::vector<std::vector<Type>> faces;

	std::string to_string() const;
};


template<typename Type>
struct partialEnlargementInfos
{
	std::vector<Type> points;
	
	// base.edge represent if the edge is taken in the enlargement
	partialCycle<uint> base;
	// enlarged.edge represent if the edge is taken in the enlargement
	partialCycle<uint> enlarged;
	std::vector<uint> outer_added;
	partialEnlargement<uint> enlargement;

	std::vector<uint> added_indices() const;
	std::vector<Type> added_points() const;
	std::vector<Type> compute_outline() const;
	std::vector<Type> compute_base() const;
	std::vector<Type> compute_enlarged() const;
	std::vector<std::vector<Type>> compute_faces() const;
};



template<typename Type>
std::vector<Type> partialEnlargementInfos<Type>::compute_outline() const
{
	return utils::select_indexed(points, enlargement.outline.cycle);
}
template<typename Type>
std::vector<Type> partialEnlargementInfos<Type>::compute_base() const
{
	return utils::select_indexed(points, base.cycle);
}
template<typename Type>
std::vector<Type> partialEnlargementInfos<Type>::compute_enlarged() const
{
	return utils::select_indexed(points, enlarged.cycle);
}
template<typename Type>
std::vector<std::vector<Type>> partialEnlargementInfos<Type>::compute_faces() const
{
	std::vector<std::vector<Type>> ret;

	for(uint f=0; f<enlargement.faces.size(); f++)
	{
		ret.push_back(utils::select_indexed(points, enlargement.faces[f]));
	}

	return ret;
}
template<typename Type>
std::string partialEnlargement<Type>::to_string() const
{
	std::string ret = "{outline: {";

	ret += outline.to_string();
	ret += ", faces: {";

	for(uint f=0; f<faces.size(); f++)
	{
		ret += "{";
		for(uint i=0; i<faces[f].size(); i++)
		{
			ret += std::to_string(faces[f][i]);

			if(i+1<faces[f].size())
			{
				ret+=", ";
			}
		}
		ret.push_back('}');

		if(f+1 < faces.size())
		{
			ret+=", ";
		}
	}

	ret.push_back('}');
	ret.push_back('}');
	return ret; 
}

template<typename Type>
partialCycle<Type>::partialCycle()
{

}

template<typename Type>
partialCycle<Type>::partialCycle(const partialCycle& pcycle)
	: cycle(pcycle.cycle),
	  edge(pcycle.edge)
{

}

template<typename Type>
partialCycle<Type>::partialCycle(const std::vector<Type>& cycle, const std::vector<bool>& edgecut)
	: cycle(cycle),
	  edge(edgecut)
{

}


template<typename Type>
partialCycle<Type>::partialCycle(const std::vector<Type>& cycle)
	: cycle(cycle),
	  edge(cycle.size(), true)
{

}

template<typename Type>
partialCycle<Type>::partialCycle(const std::vector<Type>& cycle, const std::initializer_list<uint>& cutedges)
	: partialCycle(cycle, std::vector<uint>(cutedges.begin(), cutedges.end()))
{
	
}

template<typename Type>
partialCycle<Type>::partialCycle(const std::vector<Type>& cycle, const std::vector<uint>& cutedges)
	: partialCycle(cycle)
{
	for(uint c : cutedges)
	{
		if(c >= edge.size())
		{
			utils::err("partialCycle<Type>::partialCycle(): cutedge out of bounds");
		}

		edge[c] = false;
	}
}

template<typename Type>
bool partialCycle<Type>::aligned() const
{
	return cycle.size() == edge.size();
}

template<typename Type>
uint partialCycle<Type>::size() const
{
	if(!this->aligned())
	{
		utils::err("partialCycle<Type>::size(): not aligned");
	}
	return cycle.size();
}

template<typename Type>
bool partialCycle<Type>::partial() const
{
	for(bool b : edge)
	{
		if(!b)
			return true;
	}

	return false;
}

template<typename Type>
bool partialCycle<Type>::is_cycle() const
{
	return (cycle.size() >= 3);
}

template<typename Type>
std::string partialCycle<Type>::to_string() const
{
	if(!this->aligned())
	{
		utils::err("partialCycle<Type>::to_string(): not aligned");
	}

	std::string ret = "{";

	for(uint i=0; i<cycle.size(); i++)
	{
		ret.push_back('{');
		ret += std::to_string(cycle[i]);
		ret.push_back(',');
		ret.push_back(' ');
		ret += std::to_string(edge[i]);
		ret.push_back('}');

		if(i+1<cycle.size())
		{
			ret+=", ";
		}
	}
	ret.push_back('}');

	return ret;
}

template<typename Type>
std::vector<std::vector<uint>> partialCycle<Type>::paths(bool outer) const
{
	std::vector<std::vector<uint>> ret;
	this->check();

	std::vector<uint> path;
	uint N = cycle.size();

	for(uint i=0; i<N; i++)
	{
		if(edge[i] ^ (!outer))
		{
			path.push_back(i);

			if((!edge[(i+1)%N]) ^ (!outer))
			{
				ret.push_back(path);
				path.clear();
			}
		}
	}

	if(path.size() > 0)
	{
		if(ret.size() == 0)
		{
			ret.push_back(path);
		}
		else
		{
			ret[0].insert(ret[0].begin(), path.begin(), path.end());
		}
	}

	return ret;
}

template<typename Type>
std::vector<std::vector<uint>> partialCycle<Type>::outer_paths() const
{
	return this->paths(true);
}

template<typename Type>
std::vector<std::vector<uint>> partialCycle<Type>::inner_paths() const
{
	return this->paths(false);
}

template<typename Type>
void partialCycle<Type>::check() const
{
	if(!(this->aligned()))
	{
		utils::err("partialCycle::check(): no aligned: cycle.size() ("
			+ std::to_string(cycle.size()) + ") != edge.size() (" + std::to_string(edge.size()) + ")");
	}

	if(!(this->is_cycle()))
	{
		utils::err("partialCycle::check(): not a cycle (less than 3 edges)");
	}
}

template<typename Type>
bool partialCycle<Type>::same_origin(const partialCycle<Type>& cycle) const
{
	if(this->size() != cycle.size())
		return false;

	for(uint i=0; i<this->size(); i++)
	{
		if(edge[i] != cycle.edge[i])
		{
			return false;
		}
	}

	return true;
}

template<typename Type>
void typedSpaceBox<Type>::cover(const Type& point)
{
	this->min = glm::min(this->min, point);
	this->max = glm::max(this->max, point);
}

template<typename Type>
void typedSpaceBox<Type>::cover(const std::vector<Type>& points)
{
	for(const Type& p : points)
	{
		this->cover(p);
	}
}

template<typename Type>
void typedSpaceBox<Type>::cover_translation(const Type& translation)
{
	this->cover(this->min + translation);
	this->cover(this->max + translation);
}

template<typename Type>
typedSpaceBox<Type> typedSpaceBox<Type>::operator|(const typedSpaceBox<Type>& s) const
{
	typedSpaceBox<Type> ret = s;

	ret.cover(this->min);
	ret.cover(this->max);

	return ret;
}

template<typename Type>
bool typedSpaceBox<Type>::operator<=(const typedSpaceBox<Type>& s) const
{
	return glm::all(glm::greaterThanEqual(s.max, this->max) && glm::lessThanEqual(s.min, this->min));
}

template<typename Type>
bool typedSpaceBox<Type>::operator<(const typedSpaceBox<Type>& s) const
{
	return glm::all(glm::greaterThan(s.max, this->max) && glm::lessThan(s.min, this->min));
}

template<typename Type>
std::string typedSpaceBox<Type>::to_string() const
{
	return "{min:"+glm::to_string(this->min)+", max:"+glm::to_string(this->max)+"}";
}

#endif //_SHAPES_HPP_
