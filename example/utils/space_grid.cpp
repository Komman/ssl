#include "space_grid.hpp"

#include <iostream>
#include <glm/gtx/string_cast.hpp>

#include "mutils.hpp"
#include "geom.hpp"

SpaceGrid::SpaceGrid(const glm::vec2& bottom, const glm::vec2& top, uint line_cases_amout)
	: Coligrid(glm::uvec2((top - bottom)/(top.x - bottom.x)*float(line_cases_amout))),
	  _dimx(line_cases_amout),
	  _xyratio((top.y - bottom.y)/(top.x - bottom.x)),
	  _ratio(float(line_cases_amout)/(top.x - bottom.x)),
	  _bottom(bottom),
	  _top(top)
{

}

SpaceGrid::SpaceGrid(const std::vector<glm::vec2>& polygon, uint line_cases_amout, float extension)
	: SpaceGrid(utils::foldmin(polygon) - glm::vec2(extension), utils::foldmax(polygon) + glm::vec2(extension), line_cases_amout)
{
	this->set_empty_polygon_doted(polygon, 1, 2);
}

void SpaceGrid::set_empty_polygon_doted(const std::vector<glm::vec2>& points, int valuepol, int valuecir)
{
	float d = glm::distance(_bottom, _top);
	
	this->set_circles(points, d*0.005f, valuecir);
	this->set_empty_polygon(points, valuepol);
}

void SpaceGrid::set_doted_line(const Segment2D& s, float radius, int value)
{
	this->set_thin_line(s, value);
	this->set_circle(s.first, radius, value);
	this->set_circle(s.second, radius, value);
}


void SpaceGrid::print_polygon(const std::string& path, const std::vector<glm::vec2>& points, uint line_cases_amout)
{
	SpaceGrid s(points, line_cases_amout, 0.0f);

	float d = glm::distance(s.get_bottom(), s.get_top());

	s.set_circles(points, d*0.005f, 2);
	s.set_empty_polygon(points, 1);
	s.print_ppm(path);
}

void SpaceGrid::print_segments(const std::string& path, const std::vector<Segment2D>& segments, uint line_cases_amout)
{
	if(segments.size() == 0)
	{
		utils::err("SpaceGrid::print_segments(): no segments");
	}

	glm::vec2 minv = segments[0].first;
	glm::vec2 maxv = segments[0].first;

	for(const auto& seg : segments)
	{
		minv = glm::min(minv, seg.first);
		minv = glm::min(minv, seg.second);
		maxv = glm::max(maxv, seg.first);
		maxv = glm::max(maxv, seg.second);
	}
	float d = glm::distance(minv, maxv);

	SpaceGrid s(minv - glm::vec2(d*0.005f), maxv + glm::vec2(d*0.005f), 500);

	for(const auto& seg : segments)
	{
		s.set_thin_line(seg, 1);
		s.set_circles({seg.first, seg.second}, d*0.005f, 2);
	}

	s.print_ppm(path);
}

void SpaceGrid::set_point(const glm::vec2& coord, int value)
{
	glm::ivec2 trans = this->transform(coord);

	this->Coligrid::set_point(glm::uvec2(trans), value);
}

void SpaceGrid::set_circle(const glm::vec2& center, float radius, int value)
{
	glm::ivec2 trans = this->transform(center);

	this->Coligrid::set_circle(trans, radius*_ratio, value);
}

void SpaceGrid::set_thin_line(const glm::vec2& start, const glm::vec2& stop, int value)
{
	glm::ivec2 p1 = this->transform(start);
	glm::ivec2 p2 = this->transform(stop);

	this->Coligrid::set_thin_line(p1, p2, value);
}

void SpaceGrid::set_thin_line(const Segment2D& s, int value)
{
	this->set_thin_line(s.first, s.second, value);
}

void SpaceGrid::set_circles(const std::vector<glm::vec2>& centers, float radius, int value)
{
	for(const auto& p : centers)
	{
		this->set_circle(p, radius, value);
	}
}


void SpaceGrid::set_empty_polygon(const std::vector<glm::vec2>& points, int value)
{
	std::vector<glm::vec2> newpts;

	for(const auto& p : points)
	{
		newpts.push_back(this->transform(p));
	}

	this->Coligrid::set_empty_polygon(newpts, value);
}

void SpaceGrid::set_filled_polygon(const std::vector<glm::vec2>& points, int value)
{
	std::vector<glm::vec2> pp = points;

	if(!geom::trigo_way(points))
	{
		utils::invert(pp);
	}

	this->set_filled_polygon(points, geom::get_inside_point(pp), value);
}

void SpaceGrid::set_filled_polygon(const std::vector<glm::vec2>& points, const glm::vec2& fill_start, int value)
{
	std::vector<glm::vec2> newpts;

	for(const auto& p : points)
	{
		newpts.push_back(this->transform(p));
	}

	auto tmp = this->transform(fill_start);

	this->Coligrid::set_filled_polygon(newpts, tmp, value);
}

void SpaceGrid::set_convex_polygon(const std::vector<glm::vec2>& points, int value)
{
	std::vector<glm::vec2> newpts;

	for(const auto& p : points)
	{
		newpts.push_back(this->transform(p));
	}

	this->Coligrid::set_convex_polygon(newpts, value);
}

void SpaceGrid::fill(const glm::vec2& start, int value, bool (*is_border)(int))
{
	glm::ivec2 trans = this->transform(start);

	this->Coligrid::fill(trans, value, is_border);
}

void SpaceGrid::fill(const glm::vec2& start, int value, const std::vector<int>& border_values)
{
	glm::ivec2 trans = this->transform(start);

	this->Coligrid::fill(trans, value, border_values);
}

void SpaceGrid::fill(const glm::vec2& start, int value)
{
	glm::ivec2 trans = this->transform(start);

	this->Coligrid::fill(trans, value);
}

void SpaceGrid::fill_force(const glm::vec2& start, int value)
{
	glm::ivec2 trans = this->transform(start);

	this->Coligrid::fill_force(trans, value);
}


bool SpaceGrid::collision_area(const std::vector<glm::vec2>& area) const
{
	std::vector<glm::uvec2> newpts;

	for(const auto& p : area)
	{
		newpts.push_back(this->transform(p));
	}

	return this->Coligrid::collision_area(newpts);
}

bool SpaceGrid::collision_circle(const glm::vec2& center, float radius) const
{
	return this->Coligrid::collision_circle(this->transform(center), radius*_ratio);
}

bool SpaceGrid::collision_thin_line(const glm::vec2& start, const glm::vec2& stop) const
{
	return this->Coligrid::collision_thin_line(this->transform(start), this->transform(stop));
}

bool SpaceGrid::collision_empty_polygon(const std::vector<glm::vec2>& points) const
{
	std::vector<glm::vec2> newpts;

	for(const auto& p : points)
	{
		newpts.push_back(this->transform(p));
	}

	return this->Coligrid::collision_empty_polygon(newpts);
}

bool SpaceGrid::collision_filling(const glm::vec2& start, int value)
{
	return this->Coligrid::collision_filling(this->transform(start), value);
}

bool SpaceGrid::collision_filled_polygon(const std::vector<glm::vec2>& points, const glm::vec2& fill_start, int value)
{
	std::vector<glm::vec2> newpts;

	for(const auto& p : points)
	{
		newpts.push_back(this->transform(p));
	}

	return this->Coligrid::collision_filled_polygon(newpts, this->transform(fill_start), value);
}

bool SpaceGrid::collision_convex_polygon(const std::vector<glm::vec2>& points, int value)
{
	std::vector<glm::vec2> newpts;

	for(const auto& p : points)
	{
		newpts.push_back(this->transform(p));
	}

	return this->Coligrid::collision_convex_polygon(newpts, value);
}

bool SpaceGrid::in_grid(const glm::vec2& p) const
{
	return this->in_bounds(this->transform(p));
}


glm::ivec2 SpaceGrid::transform(const glm::vec2& point) const
{
	glm::vec2 tmp = (point - _bottom)/(_top.x - _bottom.x);

	return glm::ivec2(tmp*float(_dimx));
}
