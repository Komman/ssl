#ifndef _UTILS_DEBUG_HPP_
#define _UTILS_DEBUG_HPP_	

#include <string>

#define UTILS_DEBUG
// #define UTILS_EXPENSIVE_DEBUG

namespace utils
{
	class utils_error : public std::exception
	{
	public:
		const char * what() const noexcept override;
	};

	
	void err(const std::string& msg);
};


#endif //_UTILS_DEBUG_HPP_
