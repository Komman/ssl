#ifndef _PLAN_DUAL_GRAPH_HPP_
#define _PLAN_DUAL_GRAPH_HPP_

#include <glm/glm.hpp>

#include "stored_graph.hpp"


struct planDualFace
{
	glm::vec2 position;
	std::vector<Graph::Edge> edges;
};
/*
	A PlanGraph G can give its PlanDualGraph Gd  
*/
class PlanDualGraph : public StoredGraph<planDualFace>
{
public:
	struct dualEdge
	{
		// The two faces (vertices of graph Gd) that are connected
		// The first one owns the original edge on the
		// trigonometric way
		// If the dual edge is in the outer face, the outer face 
		// will be the face_edge.second
		Edge face_edge; 
		// The edge of the graph G that links the two faces
		Edge original_edge;
	};

public:
	/*
		 /!\  CONDITIONNAL METHOD : face_sharing_edges()  /!\ 
		
		Faces stored in each dual's vertex must have been created
		with a dual computation where edges of a face must be in
		cycle order in a trigonometric way.
		
		Else, the face_sharing_edges() won't work
	*/ 
	PlanDualGraph();

	uint find_face(const std::vector<uint>& vertices);

	std::unordered_set<Edge, hashEdge> original_edges() const;
	// For this method:
	// - Faces must own edges in cycle with the trigonometric order 
	std::vector<dualEdge> face_sharing_edges(uint ext_face) const;
};

#endif //_PLAN_DUAL_GRAPH_HPP_
