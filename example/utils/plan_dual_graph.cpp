#include "plan_dual_graph.hpp"

#include "mutils.hpp"
#include "utils_debug.hpp"

using namespace std;

PlanDualGraph::PlanDualGraph()
	: StoredGraph()
{

}

std::unordered_set<PlanDualGraph::Edge, PlanDualGraph::hashEdge> PlanDualGraph::original_edges() const
{
	std::unordered_set<PlanDualGraph::Edge, PlanDualGraph::hashEdge> ret;
	auto faces = this->all_vertices();

	for(auto f : faces)
	{
		auto face_edges = this->value(f).edges;

		for(const auto& e : face_edges)
		{
			if(ret.find(e) == ret.end() && ret.find(mirror(e)) == ret.end())
			{
				ret.insert(e);
			}
		}
	}

	return ret;
}

uint PlanDualGraph::find_face(const std::vector<uint>& vertices)
{
	auto faces = this->all_vertices();

	for(uint v : faces)
	{
		auto vs = vertices_cycle(this->value(v).edges);
		
		if(utils::unorderd_vector_equals(vs, vertices))
		{
			return v;
		}
	}

	utils::err("PlanDualGraph::find_face(): face doesn't exist");
	return 0;
}

using namespace std;

std::vector<PlanDualGraph::dualEdge> PlanDualGraph::face_sharing_edges(uint ext_face) const
{
	std::vector<PlanDualGraph::dualEdge> ret;

	auto edges = this->original_edges();
	auto faces = this->all_vertices();
	
	for(Edge e : edges)
	{
		std::vector<int> adj; 
		
		for(uint f : faces)
		{
			vector<Edge> face_edges = this->value(f).edges;

			for(const auto& ef : face_edges)
			{
				if(ef == e)
				{
					adj.push_back(1+f);
					break;
				}
				if(ef == mirror(e))
				{
					adj.push_back(-1-(int)(f));
					break;
				}
			}
		}

		if(adj.size() > 2)
		{
			utils::err("PlanDualGraph::face_sharing_edges(): edge {"
					 + std::to_string(e.first) + "-" + std::to_string(e.second) + "} on "
				     + std::to_string(adj.size()) + ">2 faces : "
				     + std::to_string(abs(adj[0])-1) + ", "
				     + std::to_string(abs(adj[1])-1) + ", "
				     + std::to_string(abs(adj[2])-1) + "..."
				     );
		}

		if(adj.size() == 2)
		{
			uint f1 = abs(adj[0])-1;
			uint f2 = abs(adj[1])-1;

			if(adj[0]<0 && adj[1]<0)
			{
				utils::err("PlanDualGraph::face_sharing_edges(): edges of faces not in trigonometric order: face{"
						 + std::to_string(f1) + ", " + std::to_string(f2) + "}");
			}

			if(adj[0] > 0 && adj[1] < 0)
			{
				ret.push_back({
					.face_edge = {f1, f2},
					.original_edge = e,
				});
			}
			if(adj[0] < 0 && adj[1] > 0)
			{
				ret.push_back({
					.face_edge = {f2, f1},
					.original_edge = e,
				});
			}


			// Case of the outer face
			if(adj[0] > 0 && adj[1] > 0)
			{
				if(f1 == ext_face)
				{
					ret.push_back({
						.face_edge = {f2, f1},
						.original_edge = e,
					});
				}
				else
				{
					if(f2 == ext_face)
					{
						ret.push_back({
							.face_edge = {f1, f2},
							.original_edge = e,
						});
					}
					else
					{
						utils::err("PlanDualGraph::face_sharing_edges(uint ext_face): edge on trigonometric sense for two faces that are not the outer face");
					}
				}
			}
		}
	}
	
	return ret;
}
