#ifndef _VECTORED_MAP_HPP_
#define _VECTORED_MAP_HPP_

#include <vector>

#include "utils_debug.hpp"

template<typename T>
class VectoredMap
{
public:
	VectoredMap();

	void set(uint index, const T& v);
	void set(uint index, T&& v);
	const T& value(uint index) const;
	const T& operator[](uint index) const;
	T& operator[](uint index);
	bool find(uint index) const;

private:
	void update_size(uint index) const;

private:
	mutable std::vector<std::pair<T, bool>> _data;
};







template<typename T>
VectoredMap<T>::VectoredMap()
	: _data()
{

}

template<typename T>
void VectoredMap<T>::update_size(uint index) const
{
	while(_data.size() <= index)
	{
		_data.push_back({T(), false});
	}
}

template<typename T>
const T& VectoredMap<T>::value(uint index) const
{
	this->update_size(index);
	if(!(_data[index].second))
	{
		utils::err("VectoredMap<T>::value(index): index not defined");
	}

	return _data[index].first;
}

template<typename T>
bool VectoredMap<T>::find(uint index) const
{
	return ((index < _data.size()) && _data[index].second);
}

template<typename T>
T& VectoredMap<T>::operator[](uint index) 
{
	this->update_size(index);

	return _data[index].first;
}

template<typename T>
const T& VectoredMap<T>::operator[](uint index) const
{
	return this->value(index);
}

template<typename T>
void VectoredMap<T>::set(uint index, const T& v)
{
	this->update_size(index);

	_data[index].first  = v;
	_data[index].second = true;
}


template<typename T>
void VectoredMap<T>::set(uint index, T&& v)
{
	this->update_size(index);

	_data[index].first  = std::move(v);
	_data[index].second = true;
}


#endif //_VECTORED_MAP_HPP_
