#ifndef _SPACE_GRID_HPP_
#define _SPACE_GRID_HPP_
	
#include "coligrid.hpp"
#include "mutils.hpp"
#include "shapes.hpp"

class SpaceGrid : public Coligrid
{
public:
	// We must have top > bottom
	SpaceGrid(const glm::vec2& bottom, const glm::vec2& top, uint line_cases_amout);
	SpaceGrid(const std::vector<glm::vec2>& polygon, uint line_cases_amout, float extension);

	void set_point(const glm::vec2& coord, int value);
	void set_circle(const glm::vec2& center, float radius, int value);
	void set_circles(const std::vector<glm::vec2>& centers, float radius, int value);
	void set_thin_line(const glm::vec2& start, const glm::vec2& stop, int value);
	void set_thin_line(const Segment2D& s, int value);
	void set_doted_line(const Segment2D& s, float radius, int value);
	void set_empty_polygon(const std::vector<glm::vec2>& points, int value);
	void set_empty_polygon_doted(const std::vector<glm::vec2>& points, int valuepol, int valuecir);
	void set_filled_polygon(const std::vector<glm::vec2>& points, const glm::vec2& fill_start, int value);
	void set_filled_polygon(const std::vector<glm::vec2>& points, int value);
	void set_convex_polygon(const std::vector<glm::vec2>& points, int value);
	void fill(const glm::vec2& start, int value, bool (*is_border)(int));
	void fill(const glm::vec2& start, int value, const std::vector<int>& border_values);
	void fill(const glm::vec2& start, int value);
	void fill_force(const glm::vec2& start, int value);

	bool collision_area(const std::vector<glm::vec2>& area) const;
	bool collision_circle(const glm::vec2& center, float radius) const;
	bool collision_thin_line(const glm::vec2& start, const glm::vec2& stop) const;
	bool collision_empty_polygon(const std::vector<glm::vec2>& points) const;
	bool collision_filling(const glm::vec2& start, int value);
	bool collision_filled_polygon(const std::vector<glm::vec2>& points, const glm::vec2& fill_start, int value);
	bool collision_convex_polygon(const std::vector<glm::vec2>& points, int value);

	GETTER_BUILDER(glm::vec2, top)
	GETTER_BUILDER(glm::vec2, bottom)

	bool in_grid(const glm::vec2& p) const;

	static void print_polygon(const std::string& path, const std::vector<glm::vec2>& points, uint line_cases_amout);
	static void print_segments(const std::string& path, const std::vector<Segment2D>& segments, uint line_cases_amout);

protected:
	glm::ivec2 transform(const glm::vec2& point) const;

private:
	uint  _dimx;
	float _xyratio;
	float _ratio;

	glm::vec2 _bottom;
	glm::vec2 _top;
};

#endif //_SPACE_GRID_HPP_
