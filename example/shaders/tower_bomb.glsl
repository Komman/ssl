#include "shaders.glsl"

void tower_bomb_vertex(in vec3 rel_pos, in vec3 color, in vec3 vbloom_color, in::instance vec4 pos_U_sizeY, in::instance vec4 color_sizeXZ, in::instance vec3 directionN,
					   out vec3 fpos, out vec3 fcolor, out vec3 fbloom_color)
{
	vec3 unor_pos = vec3(0,rel_pos.y,0)*pos_U_sizeY.w + vec3(rel_pos.x,0,rel_pos.z)*color_sizeXZ.w;
	
	unor_pos = local::rotate_redirect2D(unor_pos, vec3(0.0,1.0,0.0), directionN);

	// gl_Position = local::camera_transform(pos_U_sizeY.xyz + unor_pos);
	local::vertex_only(pos_U_sizeY.xyz + unor_pos, fpos);

	fcolor = color;
	fbloom_color = vbloom_color;
}

void tower_bomb_fragment(in vec3 fpos, in vec3 fcolor, in vec3 fbloom_color, out vec4 final_color, out vec4 bloom_color)
{
	final_color = vec4(fcolor, 1.0);
	bloom_color = vec4(fbloom_color, 1.0);
}
