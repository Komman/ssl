#include "shaders.glsl"
#include "voronoi.glsl"
/* 
	vec4            : relative_pos (and on a normalized disc) and extrm info
	vec3 (instance) : sphere center
	vec4 (instance) : .x : radius
		 			  .y : thickness
		 			  .z : start time
		 			  .w : last time
	vec4 (instance) : color and speed
 */
void explodisc_vertex(in  vec4 rpos_and_extrm , in::instance vec3 center , in::instance vec4 infos, in::instance vec4 color_speed,
				      out vec3 fpos, out vec4 rpos_and_hole_coeff, out vec4 color_and_time_coeff)
{
	float time_spend = (uniform::holed_disc_time - infos.z);
	float time_coeff = clamp(1.0f - time_spend/infos.w, 0.0f, 1.0f);

	float incr_coeff = 1.0f + time_spend*color_speed.w;
	float new_radius = incr_coeff * infos.x;

	float rel_thickness = infos.y/new_radius*time_coeff*time_coeff;
	vec3 relative = rpos_and_extrm.xyz*max(0.0, (1.0 - (1.0-rpos_and_extrm.w)*rel_thickness));

	local::vertex_only(center + relative * new_radius, fpos);

	rpos_and_hole_coeff  = vec4(relative, 1.0-rel_thickness);
	color_and_time_coeff = vec4(color_speed.xyz, time_coeff);
}

void explodisc_fragment(in vec3 fpos, in vec4 rpos_and_hole_coeff, in vec4 color_and_time_coeff,
					    out vec4 color, out vec4 bloom_color)
{
	vec3  rpos       = rpos_and_hole_coeff.xyz;
	float hole_coeff = rpos_and_hole_coeff.w;
	float time_coeff = color_and_time_coeff.w;
	vec3  fcolor     = color_and_time_coeff.xyz;

	float radiusN =(length(rpos.xyz) - hole_coeff)/(1.0f - hole_coeff);

	if(radiusN < 0.0 || radiusN > 1.0f)
	{
		discard;
	}

	float coscarre = local::modulinear(rpos.x*rpos.z*15.0f , 1.0f);

	if(coscarre >= time_coeff*time_coeff*2.8f)
	{
		discard;
	}

	float antidegrad = 0.2f;
	float radiusN2   = mix(antidegrad, 1.0, radiusN)*2.0f;
	radiusN2*=radiusN2;
	radiusN2*=radiusN2;

	color = vec4(mix(fcolor + vec3(0.4), fcolor, radiusN), 1.0);
	bloom_color = vec4(fcolor,1.0)* radiusN2*2.0f;// * sqrt(local::camera_distance(fpos));
	// bloom_color *= time_coeff;
	// bloom_color *= 0.0f;
}
