#include "../motor/shaders/functions.glsl"

#include "voronoi.glsl"
#include "fog.glsl"

vec2 first_sunlight_coord_texture(in vec3 position)
{
    return local::isometric_texture_coordinate(position, uniform::isoshadow_source, uniform::isoshadow_furthest, uniform::isoshadow_plansize);
}

vec3 first_sunlight_impact_fp(in vec2 texcoord)
{
    return local::isoshadower_firstpos(uniform::isoshadow_texture, texcoord, uniform::isoshadow_source, uniform::isoshadow_furthest);
}

vec3 first_sunlight_impact(in vec3 position)
{
    return local::first_sunlight_impact_fp(local::first_sunlight_coord_texture(position));
}

float distance_first_sunlight_impact(in vec3 position, in vec3 firstlight)
{
    return local::isoshadower_distance_general(position, firstlight, uniform::isoshadow_source, uniform::isoshadow_furthest);
}

/* 
        vec2 first_texco = local::first_sunlight_coord_texture(position);
    vec3 firstimpact = local::first_sunlight_impact_fp(first_texco);

    vec2 inpixel = fract(first_texco*vec2(textureSize(uniform::isoshadow_texture, 0)));
    ivec2 pixdec = ivec2(greaterThan(inpixel, vec2(0.5)))*2 - ivec2(1);

    vec3 nei1 = textureOffset(uniform::isoshadow_texture, first_texco, pixdec*ivec2(1,0)).xyz;
    vec3 nei2 = textureOffset(uniform::isoshadow_texture, first_texco, pixdec*ivec2(0,1)).xyz;
    
    if(nei1 == uniform::isoshadow_furthest && nei2 == uniform::isoshadow_furthest)
    {
        firstimpact = uniform::isoshadow_furthest;
    }   

    float dshadow = local::distance_first_sunlight_impact(position, firstimpact);
    float scoeff = (dshadow >    0.5f) ? 1.0f/(1.0f + 0.3*dshadow) : 1.0f;


 */

vec3 sun_light_color(in vec3 position, in vec3 normalN, float normal_coeff)
{
    // return vec3(0);
    
    float normalc = local::normal_multiplior(position, normalN, vec3(500, 200, 1000.0f));
     
    vec3 fistimpact =local::first_sunlight_impact(position);

    float dshadow = local::distance_first_sunlight_impact(position, fistimpact);
    float scoeff = (dshadow > 0.0f) ? 1.0f/(1.0f + 0.25*dshadow) : 1.0f;
    scoeff = (fistimpact == uniform::isoshadow_furthest) ? 1 : scoeff;

    return (uniform::moon1_color.xyz + vec3(0.0,0.15,0.15))*normalc*mix(scoeff, 1.0f, 0.1)*0.7;
}

void sky_vertex(in vec2 screen_pos, out vec2 out_screen_pos)
{
    gl_Position = vec4(screen_pos.x, screen_pos.y, 1.0, 1.0);
    out_screen_pos = screen_pos;
}

// void sky_fragment(in vec2 uv, out vec4 color, out vec4 bloom_color)
// {
//     bloom_color = vec4(0.0f);

//     vec3 xdir = normalize(cross(uniform::camera_direction, vec3(0.0,1.0,0.0)));
//     vec3 ydir = normalize(cross(xdir, uniform::camera_direction));
//     float tanviewfield = tan(uniform::view_field/2.0f);
//     vec3 dep  = xdir*uv.x*tanviewfield*uniform::view_aspect
//               + ydir*uv.y*tanviewfield;
//     vec3 new_dirN = normalize(uniform::camera_direction + dep);
    
//     vec3 plan_start   = vec3(1,0,0);
//     vec3 plan_normalN = vec3(1,0,0);
//     vec3 plan_dirN    = cross(plan_normalN, vec3(0,1,0));
//     float piler_size  = 0.2;

//     vec2 interplan = local::line_plan_intersection(uniform::camera_position, new_dirN, plan_start, plan_normalN);

//     if(interplan.x == 0 || interplan.y <0)
//         discard;

//     vec3 intersection = uniform::camera_position + new_dirN*interplan.y;
//     float distplan = dot(plan_dirN, intersection);


//     if(int(distplan/10.0f)%4 == 0)
//     { 
//         color = vec4(0.5) / (interplan.y+10.0f) * 200.0f;

//         gl_FragDepth = local::depth_from_proj(local::camera_transform(intersection));
//     }
//     else
//     {
//         discard;
//     }
// }

void sky_fragment(in vec2 uv, out vec4 color, out vec4 bloom_color)
{
    bloom_color = vec4(0.0f);

    color = uniform::fog;
    // return;

    vec3 xdir = normalize(cross(uniform::camera_direction, vec3(0.0,1.0,0.0)));
    vec3 ydir = normalize(cross(xdir, uniform::camera_direction));
    float tanviewfield = tan(uniform::view_field/2.0f);
    vec3 dep  = xdir*uv.x*tanviewfield*uniform::view_aspect
              + ydir*uv.y*tanviewfield;
    vec3 new_dir = normalize(uniform::camera_direction + dep);
    
    float cam_angle_y = 3.141592/2.0 - acos(dot(vec3(0.0,1.0,0.0), new_dir));
    float cam_angle_x = local::total_angle2D(normalize(new_dir.xz));

    if(abs(cam_angle_y) < local::epsilon)
    {
        color = local::equator_color(cam_angle_x, bloom_color);
    }
    else
    {
        float sky_distance = 1.0/tan(cam_angle_y);
        vec2  sky_pos = vec2(sky_distance*cos(cam_angle_x), sky_distance*sin(cam_angle_x));
        
        if(cam_angle_y>0.0)
        {   
            color = local::sky_color(sky_pos, new_dir, bloom_color);
        }
        else
        {
            color = local::antisky_color(sky_pos, new_dir, bloom_color);
        }
    }

    if(bloom_color.w == 0.0f)
    {
        bloom_color = uniform::sky_back_color/5.0f;
    }
}



void place_moon(in     vec3 looking_directionN ,
                in     vec3 directionnal_coordN, in    float moon_radius, in vec4 color,
                inout  vec4 final_color        , inout vec4  moon_bloom_color)
{
    float radius = length(looking_directionN - directionnal_coordN);
    if(radius < moon_radius)
    {
        moon_bloom_color += color*2.5f;
        vec3 sphere_coord = local::sphere_proj(vec3(0.0), looking_directionN, directionnal_coordN, moon_radius);

        float vor = local::voronoi3D_distance(sphere_coord+vec3(cos(uniform::sky_moon_turn), sin(uniform::sky_moon_turn), 1.0), uniform::moon_voronoi_distance);
        final_color = color*clamp(vor, 0.2, 1.0);
    }
}

vec4 sky_color(in vec2 sky_pos, in vec3 directionN, inout vec4 bloom_color)
{
	float dir_radius = length(directionN.xz);
	float angle = -local::total_angle2D(normalize(directionN.xz));
	angle+=uniform::sky_stars_achievement*2.0f;
	vec3 star_directionN = normalize(vec3(dir_radius*cos(angle), directionN.y, dir_radius*sin(angle)));

	float vd = clamp(0.015f/local::voronoi3D_distance(star_directionN*20.0f, 2.0f-local::modulinear(uniform::sky_stars_achievement, 1.0)/4.0f), 0.0, 1.0);
    float star_coeff = vd;
    vd = vd*vd;
    vd = vd*vd;
    vd = vd*vd;
    // vd = vd*vd;
    float final_coeff = vd;

    vec4 scolor = vec4(final_coeff, final_coeff, final_coeff, 1.0);
    vec4 bcolor = vec4(0.1, 0.1, final_coeff, 1.0)*50.0f;
    vec4 final_color = mix(scolor,
			    		   uniform::sky_back_color,
			    		   1.0-final_coeff);

    final_color *= max(1, (1.0+directionN.y)*0.3f);

    if(final_coeff > 0.1f)
    {
    	bloom_color = bcolor;
    }
    else
    {

    }

    if(directionN.y > 0.1f)
    {
        float constant_d      = 4.0f;
        float aurore_strength = uniform::aurore_intens;
        float aurore_repeat   = 2.0f;
        float aurore_h_repeat = 0.3f;
        float d_coeff         = constant_d/max(length(sky_pos), constant_d);
        d_coeff=d_coeff*d_coeff;
        d_coeff=d_coeff*d_coeff;
        vec3 aurore_color = vec3(1.0f+cos(sky_pos.x*aurore_repeat), 1.0f+sin(sky_pos.y*aurore_repeat), 1.0f+sin(sky_pos.x*aurore_repeat)*cos(sky_pos.y*aurore_repeat));	
        // vec2 modsky_pos = fract(sky_pos*aurore_h_repeat);
        // modsky_pos = min(modsky_pos, vec2(1.0f)) - max(vec2(0.0), vec2(1.0f)-modsky_pos);
        // float holes_coeff=sqrt(length(modsky_pos));

        float holes_coeff = local::voronoi3D_distance(vec3(sky_pos.x, sky_pos.y, (sky_pos.x+sky_pos.y)/4.0f + uniform::aurore_dep), 3.0f);
        aurore_color*=d_coeff*aurore_strength*holes_coeff;

        final_color.xyz += aurore_color;
        // bloom_color.xyz += aurore_color;
    } 


	local::place_moon(directionN,
                      normalize(uniform::moon1_direction), uniform::moon1_radius, uniform::moon1_color,
                      final_color, bloom_color);

    local::place_moon(directionN,
                      normalize(uniform::moon2_direction), uniform::moon2_radius, uniform::moon2_color,
                      final_color, bloom_color);

    local::place_moon(directionN,
                      normalize(uniform::moon3_direction), uniform::moon3_radius, uniform::moon3_color,
                      final_color, bloom_color);

	return final_color;
}

vec4 antisky_color(in vec2 sky_pos, in vec3 directionN, inout vec4 bloom_color)
{
	return local::sky_color(sky_pos, directionN, bloom_color);
}

vec4 equator_color(in float angle, inout vec4 bloom_color)
{
	// discard;
	return uniform::sky_back_color;
}












// vec3 sun_light_color(in vec3 position, in vec3 normalN, float normal_coeff)
// {

    // float normalc = local::normal_multiplior(position, normalN, vec3(500, 200, 1000.0f));
    // vec2 texsize = textureSize(uniform::isoshadow_texture,0);
    
    // vec3 dirlight  = uniform::isoshadow_furthest - uniform::isoshadow_source;
    // vec3 dirlightN = normalize(dirlight);

    // vec3 dirlightXN = normalize(cross(dirlightN, vec3(0,1,0)));
    // vec3 dirlightYN = cross(dirlightXN, dirlightN);

    // vec3 dirXN = local::safe_normalize(local::proj_on_plan(dirlightXN, normalN), vec3(0));
    // vec3 dirYN = local::safe_normalize(local::proj_on_plan(dirlightYN, normalN), vec3(0));

    // vec2 cosa = vec2(
    //     dot(dirXN, dirlightXN),
    //     dot(dirYN, dirlightYN)
    // );
    // vec2 d = vec2(
    //     (uniform::isoshadow_plansize.x/float(texsize.x)),
    //     (uniform::isoshadow_plansize.y/float(texsize.y))
    // );


    // vec2 H = abs(vec2(
    //     d.x/cosa.x,
    //     d.y/cosa.y
    // ));

    // H=H/2.0;

    // vec3 positions[4] = {
    //     position - dirXN*H.x - dirYN*H.y,
    //     position + dirXN*H.x - dirYN*H.y,
    //     position + dirXN*H.x + dirYN*H.y,
    //     position - dirXN*H.x + dirYN*H.y,
    // };
    // vec3 fistimpacts[4];
    // for(int i=0; i<4; i++)
    // {
    //     fistimpacts[i] = local::first_sunlight_impact(positions[i]);
    // }

    // float whites[4];
    // for(int i=0; i<4; i++)
    // {
    //     float dshadow = local::distance_first_sunlight_impact(positions[i], fistimpacts[i]);
    //     whites[i] = (dshadow > 0.0f) ? 0 : 1.0f;
    // }

    // vec2 inpix1 = local::inpixel_coord(texsize, local::first_sunlight_coord_texture(positions[0]));
    // vec2 inpix = (inpix1);

    // float scoeff = local::bilinear_interpolate1(whites[0], whites[1], whites[3], whites[2], inpix.y, inpix.x);

    // return vec3(1,0.4,0.4)*normalc*mix(scoeff, 1.0f, 0.1)*1.0;
// }