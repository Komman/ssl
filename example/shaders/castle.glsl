#include "shaders.glsl"
#include "lanterns.glsl"
#include "castle_ext.glsl"
#include "graphics.glsl"
#include "casern.glsl"
#include "noise.glsl"

float custom_vdbrick(in vec3 fpos, in float variance, in float size, in float threshold, in float darkness, out float nonclamped_vd)
{
    float brickmod = local::normalizedcos((fpos.x+fpos.y+fpos.z)*variance);
    return local::custom_close_brick_coeff(fpos, (brickmod+2.0f)*size, threshold, darkness, nonclamped_vd);
}

void vdbrick(inout vec3 color, in vec3 fpos)
{
    float darkness = 0.97;
    float nonclamped_vd;
    float vdb = local::custom_vdbrick(fpos, 0.055, 0.15, 0.2, darkness, nonclamped_vd);

    color = color - vec3(0.2f,0.3f,0.5f)*2.9f*(1.0f-vdb);
}

void vdbrick_inside(inout vec3 color, in vec3 fpos)
{
    float darkness = 0.97;
    float nonclamped_vd;
    float vdb = local::custom_vdbrick(fpos, 0.125, 0.10, 0.15, darkness, nonclamped_vd);

    color = color - vec3(0.2f,0.3f,0.5f)*2.9f*(1.0f-vdb);
}

// vec3 indexednormals[6] = {
//     vec3(0.299000, -0.922800, 0.313800),
//     vec3(0.501200, -0.137600, -0.388800),
//     vec3(-0.010000, -0.221400, -0.680800),
//     vec3(0.430000, 0.009800, -0.374600),
//     vec3(-0.736000, -0.637600, -0.053200),
//     vec3(-0.621800, -0.073600, 0.266400)
// };

// void vdbrick_normal(inout vec3 color, inout vec3 normalN, in vec3 fpos)
// {
//     float darkness = 0.97;
//     float variance = 0.055;
//     float size = 0.15;
//     float threshold = 0.2;
//     float modval = fpos.x+fpos.y+fpos.z;

//     float nonclamped_vd;
//     int   cellindex;

//     float brickmod = local::normalizedcos(modval*variance);
//     float noncampled_vd = local::voronoi3D_2neighbourgs_indexed(fpos, size, cellindex);
//     float vdb = ((noncampled_vd>threshold) ? darkness : 1.0);

//     // color = color*vdb;
    
//     normalN = (vdb==1.0f)
//               ?
//               normalN
//               :
//               normalize(normalN + local::indexednormals[(cellindex)%local::voronoi_size]*0.2f);
// }

void base_vertex(in vec3 pos, in vec3 color, out vec3 fpos, out vec3 fcolor)
{
    local::simple_vertex3(pos, color, fpos, fcolor);
}

void base_fragment(in vec3 fpos, in vec3 fcolor, out vec4 final_color)
{
    final_color = vec4(clamp(fcolor, vec3(0.0) ,vec3(1.0)), 1.0);
    float vd  = local::voronoi3D_distance(fpos, 30.0f);

    final_color = final_color*(0.8f + vd*0.7f);
    local::vdbrick(final_color.xyz, fpos);
    final_color = vec4(final_color.xyz, 1.0f);

    local::vertical_shadow_modifier(final_color, fpos);
    local::castle_floor_dark(final_color, fpos);
}

vec3 castle_inside_ligth_sum(in vec3 position, in vec3 normalN, in float normal_coeff)
{
    vec3 lights_result = vec3(0.0);
    lights_result += local::camera_light_color(position, normalN, normal_coeff);
    // lights_result += local::static_ligth_color(position, normalN, normal_coeff);
    // lights_result += local::castle_ligth(position, normalN, normal_coeff);
    lights_result += local::dynamic_lights_color(position, normalN, normal_coeff);
    // lights_result += local::sun_light_color(position, normalN, normal_coeff);

    return lights_result;
}

void castle_inside_mesh_normal(inout vec4 color, in vec3 position)
{
    vec3 normalN = normalize(local::native_normal(position));

    vec3 lights_result = local::castle_inside_ligth_sum(position, normalN, local::light_normal_coeff);

    color.xyz = local::merge_lights_colors(position, color.xyz, lights_result); 
    local::centralized_modifier(color, position);
}

void inside_fragment(in vec3 fpos, in vec3 fcolor, out vec4 final_color)
{
    final_color = vec4(clamp(fcolor, vec3(0.0) ,vec3(1.0)), 1.0);
    float vd  = local::voronoi3D_distance(fpos, 30.0f);

    final_color = final_color*(0.8f + vd*0.7f);
    local::vdbrick_inside(final_color.xyz, fpos);
    final_color = vec4(final_color.xyz, 1.0f);

    local::castle_floor_dark(final_color, fpos);

    local::castle_inside_mesh_normal(final_color, fpos);
}

void castle_floor_dark(inout vec4 final_color, in vec3 pos)
{
    float coeff = min(1, 0.6 + abs(pos.y - uniform::catsle_floor_absolute_height)*1.0f);
    final_color *= coeff;
}

void ground_fragment(in  vec3 frag_position, in vec3 frag_color,
                     out vec4 final_color) 
{
    final_color = vec4(frag_color, 1.0f);
    local::vdbrick(final_color.xyz, frag_position);
    
    // local::many_vertices(frag_position, frag_color, final_color);
}

void arc_bridge_vertex(in vec3 rel_pos,
                       in vec3 color,
                       in::instance vec4 start_height,
                       in::instance vec4 orientation2DN_sizeXZ,
                       out vec3 pos,
                       out vec4 fcolor_maxsize)
{
    vec3 new_relpos = vec3(rel_pos.x*orientation2DN_sizeXZ.z, rel_pos.y*start_height.w, rel_pos.z*orientation2DN_sizeXZ.w);

    new_relpos = local::rotate_redirect2D(new_relpos, vec3(0,0,1), vec3(orientation2DN_sizeXZ.x, 0, orientation2DN_sizeXZ.y));
    pos        = start_height.xyz + new_relpos;

    gl_Position = local::camera_transform(pos.xyz);

    fcolor_maxsize = vec4(color, max(start_height.w, max(orientation2DN_sizeXZ.z, orientation2DN_sizeXZ.w)));
}

void tower_cannons_vertex(in vec3 rel_pos,
                          in vec3 color,
                          in::instance vec4 dep_light,
                          in::instance vec4 orientation_length,
                          out vec4 pos_light,
                          out vec4 fcolor_length)
{
    pos_light.xyz = vec3(rel_pos.x, rel_pos.y*orientation_length.w, rel_pos.z);
    fcolor_length = vec4(color, rel_pos.y);

    pos_light.xyz = dep_light.xyz + local::rotate_redirect2D(pos_light.xyz, vec3(0,1,0), orientation_length.xyz);
    pos_light.w   = dep_light.w;

    gl_Position = local::camera_transform(pos_light.xyz);
}

void tower_cannons_fragment(in vec4 pos_light,
                            in vec4 fcolor_length,
                            out vec4 final_color,
                            out vec4 bloom_color)
{
    float endlight   = 0.8f;
    float calight    = abs(pos_light.w);
    float finalight  = calight*clamp((fcolor_length.w-endlight)/(1.0f-endlight), 0,1);
    float finalightb = calight*clamp((fcolor_length.w-endlight*0.6f)/(1.0f-endlight*0.6f), 0,1);
    vec3  lightcolor = (pos_light.w < 0) ? vec3(1.0,0.2,0.1)*local::normalizedcos(uniform::time*14.0f) : vec3(1,0.7,0.4);
  
    bloom_color= vec4(lightcolor*finalightb*local::camera_distance(pos_light.xyz)*0.03f, 1.0f);

    final_color = vec4(fcolor_length.xyz, 1.0f);
    final_color.xyz *= vec3(1.0f) + bloom_color.xyz;
    
    local::all_lights_mesh_normal(final_color, pos_light.xyz);
}

void arc_bridge_fragment(in vec3 pos,
                         in vec4 fcolor_maxsize,
                         out vec4 final_color)
{
    final_color = vec4(clamp(fcolor_maxsize.xyz, vec3(0.0) ,vec3(1.0)), 1.0);
    local::vdbrick(final_color.xyz, pos);

    local::castle_floor_dark(final_color, pos);
}

void arc_door_vertex(in vec3 rel_pos,
                     in vec4 color_door,
                     in::instance vec4 start_height,
                     in::instance vec4 orientation2DN_sizeXZ,
                     in::instance vec3 inst_color,
                     out vec3 pos,
                     out vec4 frel_pos_maxisize,
                     out vec4 fcolor_door)
{
    vec3 new_relpos = vec3(rel_pos.x*orientation2DN_sizeXZ.z, rel_pos.y*start_height.w, rel_pos.z*orientation2DN_sizeXZ.w);
    float depth = new_relpos.z;

    new_relpos = local::rotate_redirect2D(new_relpos, vec3(0,0,1), vec3(orientation2DN_sizeXZ.x, 0, orientation2DN_sizeXZ.y));
    new_relpos.y = abs(new_relpos.y);
    pos = start_height.xyz + new_relpos;

    gl_Position = local::camera_transform(pos);

    frel_pos_maxisize = vec4(rel_pos, max(start_height.w, max(orientation2DN_sizeXZ.z, orientation2DN_sizeXZ.w)));
    fcolor_door       = (color_door.w >= 0.0f) ? vec4(inst_color, 1.0f) : color_door;
}


void arc_door_fragment(in vec3 pos,
                       in vec4 frel_pos_maxisize,
                       in vec4 fcolor_door,
                       out vec4 final_color)
{
    final_color = vec4(clamp(fcolor_door.xyz, vec3(0.0) ,vec3(1.0)), 1.0);
    float vd = local::voronoi3D_distance(pos.xyz, frel_pos_maxisize.w*0.7f);
    vd = (1.0f+vd*0.5f);


    float t = uniform::time;

    vec2 moon_center = vec2(frel_pos_maxisize.x, (frel_pos_maxisize.y-0.5)*1.2f);
    float d = local::sd_moon(moon_center,
                             0.07 + cos(t)*0.005,
                             0.10 + cos(0.7f*t)*0.06,
                             0.10 + abs(cos(0.8f*t))*0.04);
    d = (0.8+fract(d*10.0)*0.2) / clamp(length(moon_center)*3, 1, 3);
    d *= min(frel_pos_maxisize.y*5 + 0.7, 1);

    bool door = (fcolor_door.w < 0);

    local::castle_floor_dark(final_color, pos);
    final_color.xyz *= door ? d : vd;
}




void mtower_vertex(in vec3 rel_pos,
                   in vec4 color_room,
                   in::instance vec4  center_height,
                   in::instance float size,
                   out vec3 pos,
                   out vec4 frelpos_room,
                   out vec4 fcolor_size)
{
    pos =   center_height.xyz
          + vec3(rel_pos.x, 0, rel_pos.z)*size
          + vec3(0, min(rel_pos.y, 1.0),0)*center_height.w
          + ((rel_pos.y >= 1.0f) ? vec3(0, rel_pos.y-1.0, 0)*size : vec3(0));

    gl_Position = local::camera_transform(pos);

    frelpos_room = vec4(rel_pos, color_room.w);
    fcolor_size  = vec4(color_room.xyz, size);
}

void mtower_fragment(in vec3 pos, in vec4 frelpos_room, in vec4 fcolor_size, out vec4 final_color, out vec4 bloom_color)
{
    final_color = vec4(clamp(fcolor_size.xyz, vec3(0.0) ,vec3(1.0)), 1.0);
    bool roof = frelpos_room.w > 1.0f;

    float vd = local::voronoi3D_distance(pos, (roof ? 0.7f : 4.0f)*fcolor_size.w);

    final_color = final_color*(1.0f+vd*0.7f);
    local::vdbrick(final_color.xyz, pos);
    bloom_color = vec4(0);
    vec2 winsize_coeff = vec2(0.8, 0.5 - 0.04); 

    bool window = (frelpos_room.w >= 0.5f - winsize_coeff.y/2.0f
                && frelpos_room.w <= 0.5f + winsize_coeff.y/2.0f
                && length(frelpos_room.xz) < 0.92f);
    bool shutters = window || frelpos_room.w < -0.5;

    local::all_light_custom_normal(final_color, pos, normalize(frelpos_room.xyz - vec3(0,frelpos_room.y,0)), 0.75);

    local::castle_floor_dark(final_color, pos);
    final_color = window   ? vec4(0.4, 1, 0.3, 1) : final_color;
    bloom_color = shutters ? vec4(0.4, 1, 0.3, 1)*2.0f : bloom_color;
}


void counterfort_vertex(in vec3 rel_pos,
                        in vec3 color,
                        in::instance vec4 start_height,
                        in::instance vec4 orientation2DN_sizeXZ,
                        out vec3 pos,
                        out vec4 fcolor_maxsize)
{
    vec3 new_relpos = vec3(rel_pos.x*orientation2DN_sizeXZ.z, rel_pos.y*orientation2DN_sizeXZ.z, rel_pos.z*orientation2DN_sizeXZ.w);

    new_relpos = local::rotate_redirect2D(new_relpos, vec3(0,0,1), vec3(orientation2DN_sizeXZ.x, 0, orientation2DN_sizeXZ.y));
    pos        = start_height.xyz + new_relpos;

    pos.y =  (rel_pos.y > -1.0f) ? pos.y : start_height.y - start_height.w;

    gl_Position = local::camera_transform(pos.xyz);

    fcolor_maxsize = vec4(color, max(start_height.w, max(orientation2DN_sizeXZ.z, orientation2DN_sizeXZ.w)));
}


void counterfort_fragment(in vec3 pos,
                          in vec4 fcolor_maxsize,
                          out vec4 final_color)
{
    final_color = vec4(clamp(fcolor_maxsize.xyz, vec3(0.0) ,vec3(1.0)), 1.0);
    local::vdbrick(final_color.xyz, pos);

    local::castle_floor_dark(final_color, pos);
}