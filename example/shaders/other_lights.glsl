#include "voronoi.glsl"

float shading_range = 80.0f;

float castle_light_height_coeff(in vec3 position)
{
	const float hmin = uniform::castle_minminax_tower_height.x;
	const float hmax = uniform::castle_minminax_tower_height.y;

	float d = local::voronoi3D_distance(position, 50.0f);

	return clamp((position.y-(hmin-5.0f))/50.0f, 0.1, 1.0);
}

vec3 castle_ligth(in vec3 position, in vec3 normalN, in float normal_coeff)
{
	return vec3(0.0);

	// float d   = uniform::castle_light_radius;
	// float rd  = length(position.xz - uniform::castle_center.xz);
	// vec3 color;

	// if(rd < d)
	// {
	// 	color = uniform::castle_ambiant_color*uniform::castle_ligths_activation;
	// }
	// else
	// {
	// 	float c = 1.0+(rd - d)/local::shading_range;
	// 	color = uniform::castle_ambiant_color/(c*c)*uniform::castle_ligths_activation; 
	// }

	// return color*local::castle_light_height_coeff(position);
}
