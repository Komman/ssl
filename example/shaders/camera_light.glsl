// Camera light
float camera_light_power              = 10.0;
float camera_light_max_divisor        = 999999999.0;
float camera_light_min_divisor        = 0.9;
vec3  camera_light_source_color       = vec3(1.0,1.0,1.0); 

float camera_distance(in vec3 position)
{
	return length(position-uniform::camera_position);
}

float camera_light_distance(in vec3 position)	
{
	float distance_coeff = clamp(local::camera_distance(position)/local::camera_light_power,
									    local::camera_light_min_divisor,
									    local::camera_light_max_divisor);

	float distance_coeff2 = clamp(local::camera_distance(position)/2.0f,
									    local::camera_light_min_divisor,
									    local::camera_light_max_divisor);

	return 
		1.0/(distance_coeff*distance_coeff)  ;
		// +1.0/(distance_coeff2*distance_coeff2);
}

vec3 camera_light_color(in vec3 position, in vec3 normalN, float normal_coeff)
{
	float coeff_mult = local::camera_light_distance(position);
	coeff_mult      *= mix(1.0, local::normal_multiplior(position, normalN, uniform::camera_position), normal_coeff);

	// float player_radius = 0.2f;
	// float player_height = 0.2f;
	// float own_shadow_distance = length(position.xz - uniform::camera_position.xz);
	// float own_shadow_coeff    = own_shadow_distance/player_radius;
	// coeff_mult *= (own_shadow_distance < player_radius && uniform::camera_position.y>position.y+player_height) ? mix(own_shadow_coeff, 1.0f, 0.7f) : 1.0f;

	return local::camera_light_source_color*coeff_mult;
}
