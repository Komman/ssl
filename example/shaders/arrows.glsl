#include "../motor/shaders/functions.glsl"

#include "shaders.glsl"
#include "lights.glsl"
#include "voronoi.glsl"

void simple_arrow_vertex(in vec3 position,
						 in vec3 colormask,
						 in::instance vec3 dep,
						 in::instance vec3 directionN,
						 in::instance vec3 shaft_color,
						 in::instance vec3 head_color,
						 in::instance vec3 geom_params,
						 out vec3 frag_pos,
						 out vec3 frag_color,
						 out vec4 static_pos)
{
	float alength     = geom_params.x;
	float size_coeff  = geom_params.y/2.0f;
	float head_coeff  = geom_params.z;
	bool shaft   =  gl_VertexID < uniform::simple_arrow_pack_first_head_index;
	bool feather = (shaft) && (gl_VertexID >= uniform::simple_arrow_pack_first_feather_index);

	float shaft_coeff  = (1.0f-head_coeff);
	float length_coeff = shaft ? shaft_coeff : head_coeff;
	
	vec3 newdir = vec3(0.0f, position.y*size_coeff, position.z*size_coeff);
	newdir.x = position.x*alength*length_coeff;
	newdir.x += shaft ? 0.0f : alength*shaft_coeff;
	
	newdir = local::rotate_redirect2D(newdir, vec3(1.0,0.0,0.0), directionN);

	local::vertex_only(newdir+dep, frag_pos);
	frag_color     = (shaft   ? shaft_color : head_color)*colormask;
	frag_color     = (feather ? colormask   : frag_color);
	static_pos.xyz = position;
	static_pos.w   = shaft ? 0.0f : 1.0f;
	static_pos.w   = feather ? -1.0f : static_pos.w;
}

bool is_feather(float t)
{
	return (t < -0.5);
}
bool is_shaft(float t)
{
	return (t > -0.5) && (t < 0.5);
}
bool is_head(float t)
{
	return (t > 0.5);
}

void simple_arrow_fragment(in vec3 pos, in vec3 color, in vec4 static_pos, out vec4 final_color)
{
	final_color = vec4(color, 1.0);

	if(local::is_feather(static_pos.w))
	{
		float amp  = 1.0;
		float lin  = 1.0;
		float freq = 60.0;

		float coscoeff = (1.0+cos(color.x*freq))/2.0;
		final_color.xyz = vec3(1.0)*(amp + coscoeff)/(1.0 + amp)/4.0f;
		final_color.xyz *= (1.0+color.x*lin)/(1.0+lin);

		if(coscoeff*color.y*color.y>0.7)
		{
			discard;
		}
	}

	final_color = clamp(final_color, 0.0,1.0);

	//*(1.0f + 1.0*(abs(cos(static_pos.x*10.0f)*sin(static_pos.z*10.0f))/8.0f));
	// bloom_color = vec4(0.1,0.3,0.8,1.0)*length(pos - uniform::camera_position);
}
