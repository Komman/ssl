#include "../motor/shaders/functions.glsl"
#include "shaders.glsl"

void houses_vertex(in vec3 pos, in vec2 in_infos, in::instance vec3 dep,
				   out vec3 frag_pos, out vec2 infos, out vec3 frag_dep)
{
	local::vertex_only(pos + dep, frag_pos);
	frag_pos = pos;
	frag_dep = dep;
	infos    = in_infos;
}

void houses_fragment(in vec3 frag_pos, in vec2 infos, in vec3 frag_dep, out vec4 color, out vec4 bloom_color)
{
	bloom_color = vec4(0.0);
	
	float shadow = infos.x;
	float type   = infos.y;

	vec3 col      = vec3(0.4,0.15,0.10);
	vec3 col_door = vec3(0.4,0.18,0.05);
	vec3 col_extr = col_door/2.0f;

	if(type < -0.5f)
	{
		col = col_extr;
	}
	if(type < -1.5f)
	{
		col = col_door;
		col *= (fract(frag_pos.y*1.4f) > 0.4 && abs(frag_pos.x) < 0.2f) ? 0.7 : 1.0;
		bloom_color = vec4(col*2.0f, 1.0);
	}

	float bot_coeff = clamp(frag_pos.y + 0.7f, 0.0, 1.0);

	color = vec4(col* bot_coeff * shadow, 1.0f) ;

	local::all_lights_mesh_normal(color, frag_pos + frag_dep);
}

