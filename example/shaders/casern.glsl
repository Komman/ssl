// void fatower_bridge_vertex(in vec3 rel_pos,
//                            in vec3 color,
//                            in::instance vec4 start_height,
//                            in::instance vec4 orientation2DN_sizeXZ,
//                            out vec3 pos,
//                            out vec4 fcolor_maxsize,
//                            out vec3 frelpos)
// {
//     vec3 new_relpos = vec3(rel_pos.x*orientation2DN_sizeXZ.z, rel_pos.y*start_height.w, rel_pos.z*orientation2DN_sizeXZ.w);
//     float depth = new_relpos.z;

//     new_relpos = local::rotate_redirect2D(new_relpos, vec3(0,0,1), vec3(orientation2DN_sizeXZ.x, 0, orientation2DN_sizeXZ.y));
//     pos        = start_height.xyz + new_relpos;

//     gl_Position = local::camera_transform(pos.xyz);

//     fcolor_maxsize = vec4(color, max(start_height.w, max(orientation2DN_sizeXZ.z, orientation2DN_sizeXZ.w)));
//     frelpos = rel_pos;
// }

// void fatower_bridge_fragment(in vec3 pos,
//                              in vec4 fcolor_maxsize,
//                              in vec3 frelpos,
//                              out vec4 final_color)
// {
//     final_color = vec4(clamp(fcolor_maxsize.xyz, vec3(0.0) ,vec3(1.0)), 1.0);
//     float vd = local::voronoi3D_distance(pos.xyz, fcolor_maxsize.w*0.3f);

//     float dloophole = abs(frelpos.y-0.5);
//     bool loophole   = length(frelpos.xz) < 0.416f
//                     && dloophole > 0.1
//                     && dloophole < 0.3; 

//     final_color = loophole ? vec4(0,0,0,1) : (final_color*(1.0f+vd*0.5f));
// }
