#include "../motor/shaders/functions.glsl"

vec3 static_ligth_color(in vec3 position, in vec3 normalN, in float normal_coeff)
{
	vec3 acc_color = vec3(0.0);

	if(    position.x >= uniform::lightor_bottom.x && position.z >= uniform::lightor_bottom.y
		&& position.x <= uniform::lightor_top.x    && position.z <= uniform::lightor_top.y)
	{
		vec2 relative_in_map = (position.xz - uniform::lightor_bottom)/(uniform::lightor_top - uniform::lightor_bottom);
		
		int index = texture(uniform::lightor_ligthmap, relative_in_map).x;
		int count = -1;
		int MAX_LIGHT_ONE_FRAG = min(100, uniform::lightor_max_ligths);

		if(index != -1)
		{
			for(count=0; count<MAX_LIGHT_ONE_FRAG; count++)
			{
				int sligth_id = texelFetch(uniform::lightor_ligths_references, index, 0).x;
				int ligth_id = abs(sligth_id);
				count++;
				index++;

				vec4 texposrange = texelFetch(uniform::lightor_ligths_positions_range, ligth_id, 0);

				vec3  l_pos   = texposrange.xyz;
				float l_range = texposrange.w;
				
				float dligth = length(position - l_pos);

				if(dligth < l_range)
				{
					vec2 intens_satu = texelFetch(uniform::lightor_ligths_intensity_saturation, ligth_id, 0).xy;

					vec3  l_color = texelFetch(uniform::lightor_ligths_colors , ligth_id, 0).xyz;
					float l_inten = intens_satu.x;
					float l_satur = intens_satu.y;

					float pixel_normal_coeff = local::normal_multiplior(position, normalN, l_pos);
					pixel_normal_coeff       = mix(1.0f, pixel_normal_coeff, normal_coeff);

					float coeff  = clamp(l_inten/(dligth*dligth), 0.0f, l_satur);
					float dimco = 0.8f;

					if(dligth >= l_range*dimco)
					{
						coeff *= clamp((1.0f - dligth/l_range)/(1.0f - dimco), 0.0f, 1.0f);
					}

					// coeff = (1.0f-dligth/l_range)*l_inten/100.0f;

					acc_color += l_color*coeff*pixel_normal_coeff;
				}
				// acc_color += vec3(0.1);

				if(sligth_id<0)
				{
					break;
				}
			}
		}
	}
	
	return acc_color*uniform::lightor_activation;
}


