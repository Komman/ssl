#include "shaders.glsl"

void halo_vertex(in vec3 coord,
				 in::instance vec3 dep,
				 in::instance vec4 sphere_color,
				 out vec3 sphere_coord,
				 out vec3 center_coord,
				 out vec4 color)
{
	local::vertex_only(coord + dep, sphere_coord);
	color = sphere_color;
	center_coord = dep;
}

void halo_fragment(in vec3 pixel_coord, in vec3 sphere_center, in vec4 center_color, out vec4 color)
{
	float d = length(normalize(pixel_coord - uniform::camera_position) - normalize(sphere_center - uniform::camera_position));
	
	float coeff = d;

	color = vec4(center_color.rgb*coeff, center_color.a);
}

