#include "../motor/shaders/textures.glsl"
	
void vertical_projection(in vec3 pos)
{
	vec2 mappos = clamp(vec2(pos.x, pos.z)/uniform::shadowmap_proj_size, vec2(-1.0f), vec2(1.0));
	gl_Position = vec4(mappos.x, mappos.y, 0.0, 1.0);
}

void vertical_projection_instance(in vec3 pos, in::instance vec3 dep)
{
	local::vertical_projection(pos*uniform::shadowmap_enlargement+dep);
}
void circle_projection(in vec3 pos, out vec3 fpos)
{
	local::vertical_projection(pos);
	fpos = pos;
}

void down_prof_fragment(out float final_coeff)
{
	final_coeff = 1.0f;
}

void down_prof_circle_fragment(in vec3 fpos, out float final_coeff)
{
	if(distance(fpos.xz, uniform::vertical_shadower_circle_center) > uniform::vertical_shadower_circle_radius)
	{
		discard;
	}

	final_coeff = 1.0f;
}

const int blur_coeffs = 9;

void shadow_blur_float(in vec2 frag_uv, out float shadowcoeff)
{
	shadowcoeff = 0.0f;
	{
	    float v[local::blur_coeffs] = {
	        textureOffset(uniform::shadowmap_preparing_map, local::get_tex_coord(frag_uv), ivec2(-1,-1)).x,
	        textureOffset(uniform::shadowmap_preparing_map, local::get_tex_coord(frag_uv), ivec2( 0,-1)).x,
	        textureOffset(uniform::shadowmap_preparing_map, local::get_tex_coord(frag_uv), ivec2( 1,-1)).x,
	        textureOffset(uniform::shadowmap_preparing_map, local::get_tex_coord(frag_uv), ivec2(-1, 0)).x,
	        textureOffset(uniform::shadowmap_preparing_map, local::get_tex_coord(frag_uv), ivec2( 0, 0)).x,
	        textureOffset(uniform::shadowmap_preparing_map, local::get_tex_coord(frag_uv), ivec2( 1, 0)).x,
	        textureOffset(uniform::shadowmap_preparing_map, local::get_tex_coord(frag_uv), ivec2(-1, 1)).x,
	        textureOffset(uniform::shadowmap_preparing_map, local::get_tex_coord(frag_uv), ivec2( 0, 1)).x,
	        textureOffset(uniform::shadowmap_preparing_map, local::get_tex_coord(frag_uv), ivec2( 1, 1)).x
	    };

	    float coeffs[local::blur_coeffs] = {
	    	1.0, 2.0, 1.0,
	    	2.0,10.0, 2.0,
	    	1.0, 2.0, 1.0
	    };

	    float sum=0.0;
	    for(int i=0; i<local::blur_coeffs; i++)
	    {
	    	sum+=coeffs[i];
	    }
	    for(int i=0; i<local::blur_coeffs; i++)
	    {
	    	shadowcoeff+=v[i]*coeffs[i]/sum;
	    }
	}

}


void vertical_shadow_modifier(inout vec4 color, in vec3 pos)
{
	vec2 mappos = vec2(pos.x, pos.z)/uniform::shadowmap_proj_size/2.0f + vec2(0.5f);
	color*=(1.0f-texture(uniform::vertical_shadowmap, mappos).x/4.0f);
}