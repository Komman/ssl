#include "../motor/shaders/functions.glsl"
#include "shaders.glsl"
#include "voronoi.glsl"
#include "explodisc.glsl"
#include "explosphere.glsl"
#include "explobibo.glsl"

void meshed_light_vertex(in  vec3 position, in vec3 in_color,
                         out vec3 fcolor)
{
    gl_Position = local::camera_transform(position);
    fcolor = in_color;
}  

void meshed_light_fragment(in  vec3 fcolor,
                           out vec4 final_color) 
{
    final_color = vec4(fcolor, 0.0f);
}

float size_decrease_coeff(float time_coeff)
{
    // float tc = cos(time_coeff * 3.141592f);
    // tc = pow(tc, 256);

    float di = 0.01;
    float df = 0.2;

    if(time_coeff  < di)
    {
        return time_coeff/di;
    }
    if(time_coeff  > 1.0-df)
    {
        return (1.0-time_coeff)/df;
    }

    return 1.0f;
}

// LBAC = Light on Bloom Affecion Coeff
void packed_clouds_effects_vertex(in vec3 in_pos,
                                  in::instance vec4 dep_LBAC,
                                  in::instance vec4 cloud_color,
                                  in::instance vec3 start_speed,
                                  in::instance vec3 start_accelerations,
                                  in::instance vec4 infos,
                                  out vec3 out_pos,
                                  out vec4 dep_frag_lightonbloom_coeff,
                                  out vec4 fcolor,
                                  out vec4 frag_info)
{

    float start_time = infos.x;
    float last_time  = infos.y;
    float friction   = infos.z;
    float frag_time  = (uniform::packed_could_dispatchor_time - start_time)/last_time;
    float old_size   = infos.w * sqrt(3.0f) / 2.0f;
    float size       = old_size * local::size_decrease_coeff(frag_time);

    vec3 computed_pos = in_pos*size + dep_LBAC.xyz;
    float t = (uniform::packed_could_dispatchor_time - start_time);
    vec3 alpha = start_speed - start_accelerations/friction;
    vec3 new_path = alpha/friction*(1.0f-exp(-friction*t)) + start_accelerations/friction*t;
    computed_pos +=  new_path;
    dep_frag_lightonbloom_coeff = vec4(dep_LBAC.xyz + new_path, dep_LBAC.w);

    local::vertex_only(computed_pos, out_pos);

    frag_info.x = frag_time;
    frag_info.y = size;
    frag_info.z = start_speed.x + start_speed.y + start_speed.z;
    frag_info.w = old_size;
    fcolor      = cloud_color; 
}

void coulds_effects_vertex(in vec3 in_pos,
                           in::instance vec4 dep_LBAC,
                           in::instance vec4 cloud_color,
                           in::instance vec2 infos,
                           out vec3 out_pos,
                           out vec4 dep_frag_lightonbloom_coeff,
                           out vec4 fcolor,
                           out vec4 frag_info)
{
    float vert_time = infos.x;
    float frag_time = 1.0f-vert_time;
    float old_size  = infos.y * sqrt(3.0f) / 2.0f;
    float size      = old_size * local::size_decrease_coeff(frag_time);

    local::vertex_only(in_pos*size + dep_LBAC.xyz, out_pos);  

    frag_info.x = frag_time;
    frag_info.y = size;
    frag_info.z = old_size*10.0f;
    frag_info.w = old_size;

    dep_frag_lightonbloom_coeff = dep_LBAC;
    fcolor   = cloud_color; 
}

void coulds_effects_fragment(in vec3 pos, in vec4 dep_frag_lightonbloom_coeff, in vec4 fcolor, in vec4 frag_info, out vec4 color, out vec4 bloom_color)
{
    float bloom_coeff = (fcolor.w >= 0.0) ? (fcolor.w) : (-fcolor.w * local::camera_distance(pos));
    bloom_color = vec4(fcolor.xyz, 1.0) * bloom_coeff;

    float frag_time = frag_info.x;
    float size      = frag_info.y;
    float diffid    = frag_info.z;
    float old_size  = frag_info.w;

    if(frag_time>1.0)
    {
        discard;
    }

    vec3 cam_to_frag  = pos      - uniform::camera_position;
    vec3 cam_to_fragN = normalize(cam_to_frag);
    vec3 sphere_pos = local::sphere_proj(uniform::camera_position, cam_to_fragN, dep_frag_lightonbloom_coeff.xyz, size/2.0f);

    if(sphere_pos == vec3(0.0f))
    {
        discard;
    }

    /* 
        CLOUDS WELL DRAWN
    */
        // vec3 diffvector = vec3(diffid, diffid*diffid, 1.0f/(diffid+1.0f));
        // float vd = local::voronoi3D_distance(sphere_pos-dep_frag + diffvector, old_size);

        // color = vec4(fcolor.xyz*vec3(1.0f/(vd*3.0f + 1.0f)), 1.0f);


    /* 
        SIMPLIFIED
    */

    vec4 oldbloom = bloom_color;

    color = vec4(fcolor.xyz, 1.0f);
    local::all_lights_smooth_bloomed(color, bloom_color, pos);

    bloom_color = mix(oldbloom, bloom_color, dep_frag_lightonbloom_coeff.w);
}



void simple_trail_vertex(in vec3 in_pos, in float vert_time, out vec3 out_pos, out float ftime)
{
    // vec3 npos = in_pos;
    // if(vert_time > 0.0f)
    // {
    //     npos.y -= (uniform::time - vert_time) * length(local::normalizedcos3(in_pos*5.0f) + 1.0f);
    // }

    local::vertex_only(in_pos, out_pos);    
    ftime = vert_time < 0.0f ? (uniform::time - uniform::trail_last_time) : vert_time;
}

void simple_trail_fragment(in vec3 pos, in float frag_time, out vec4 color)
{
    float trail_last = uniform::trail_last_time;
    float time_coeff = clamp((frag_time + trail_last - uniform::time)/(trail_last), 0.0, 1.0);

    float smoke_coeff = local::voronoi3D_distance(pos, 0.2f);

    if(time_coeff*smoke_coeff < 0.15)
    {
        discard;
    }
    vec3 trail_color = vec3(1.0)*0.8f;
    float vcoeff = min(time_coeff*smoke_coeff, 1.0);

    color = vec4(trail_color*mix(vcoeff, 1.0, 0.4), vcoeff);
}


