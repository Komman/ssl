#include "../motor/shaders/basic_shaders.glsl"

#include "fog.glsl"
#include "camera_light.glsl"
#include "lights.glsl"

void storm_fragment(in vec3 pos, out vec4 color, out vec4 bloom_color)
{
    local::white_fragment(pos, color);
    bloom_color = uniform::storm_powered_color*local::absolute_bloom_coeff(pos);
}

