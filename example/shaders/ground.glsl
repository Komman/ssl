#include "../motor/shaders/post_processing.glsl"
#include "../motor/shaders/functions.glsl"

#include "shaders.glsl"
#include "voronoi.glsl"
#include "upshadow.glsl"
#include "static_ligths.glsl"
#include "camera_light.glsl"

//TODEL
#include "houses.glsl"

void tree_vertex(in  vec3 in_position , in vec4 in_color, in::instance vec3 dep,
                 out vec3 out_position, out vec4 out_color, out vec3 tex_coord)	
{
	vec3 newpos = in_position;
	float dmax = 5.0f;
	float antispeed = 10.0f;
	float vd1 = local::voronoi3D_distance(in_position*(uniform::time+0.0*dmax)/antispeed, dmax);
	float vd2 = local::voronoi3D_distance(in_position*(uniform::time+0.3*dmax)/antispeed, dmax);
	float vd3 = local::voronoi3D_distance(in_position*(uniform::time+0.6*dmax)/antispeed, dmax);
	newpos += vec3(vd1, vd2, vd3)*0.2;

    local::instance_vertex(newpos, in_color, dep, out_position, out_color);
    tex_coord = in_position + vec3(dep.x, 0, dep.z);
} 

void tree_fragment(in  vec3 frag_position, in vec4 frag_color, in vec3 frag_text_coord,
                   out vec4 final_color) 
{
    local::many_vertices(frag_position, frag_color, final_color);

    float leave_green = 0.2f;
    float voronoi_ext = (frag_color.g >= leave_green) ? 0.1f : 1.0f;

    float vd = local::voronoi3D_2neighbourgs(frag_text_coord, voronoi_ext);
	final_color*=mix(0.9,1.0,vd);
 	final_color*=min(1.0f, frag_text_coord.y/3+0.5);
}

void ground_vertex(in  vec3 in_position_s, in vec4 in_color_s, in vec4 normal_angles,
                   out vec3 out_position_s, out vec4 out_color_s, out vec4 fnormal_angles)
{
	// vec3 mod_pos = in_position_s;
	// mod_pos.y *= clamp(uniform::time/15.0f, 0.0,1.0);

    local::vertex_only(in_position_s, out_position_s);
    out_color_s    = in_color_s;
    fnormal_angles = normal_angles;
}

float doted_light_reflect(in vec3 light_to_fragN, in vec3 cam_to_fragN, in vec3 frag_normalN)
{
	return dot(-cam_to_fragN, reflect(light_to_fragN, frag_normalN));
}

void ground_fragment(in  vec3 frag_position, in vec4 frag_color, in vec4 fnormal_angles,
                     out vec4 final_color) 
{
	final_color = vec4(frag_color.xyz, 1.0f);
	float att = 32.0;
  
    float grass_coeff = local::close_brick_coeff(frag_position);
    float grass_dist_carac = clamp((local::camera_distance(frag_position)-local::camera_light_power)/5.0, 0.0, 1.0);
	
    float case_l = 1000.0f/40;

	// if(local::modulinear(frag_position.x, case_l/2.0f) < 0.1f)
	// {
	// 	final_color *= 0.5f;
	// }
	// if(local::modulinear(frag_position.z, case_l/2.0f) < 0.1f)
	// {
	// 	final_color *= 0.5f;
	// }

	final_color *= mix(grass_coeff, 1.0, grass_dist_carac)*1.4f;

	vec3 normalN = normalize(vec3(fnormal_angles.xy, 0) + vec3(0, fnormal_angles.wz));

	local::vertical_shadow_modifier(final_color, frag_position);
	local::all_light_custom_normal(final_color, frag_position, normalN, 0.85f);
}	

