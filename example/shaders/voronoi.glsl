vec3 voronoi_positions[]={
    vec3(0.1, 0.5, 0.1),
    vec3(0.2, 0.1, 0.7),
    vec3(0.4, 0.9, 0.6),
    vec3(0.3, 0.8, 0.3),
    vec3(0.2, 0.8, 0.7),
    vec3(0.9, 0.2, 0.9)
};

int voronoi_size = 6;

vec3 vec3one = vec3(1.0,1.0,1.0);

float voronoi3D_distance(in vec3 pos, in float cell_size)
{
    float mind = 3.0;
    vec3  mod_pos = fract(pos/cell_size/2.0f);
    mod_pos *= 2.0f;
    mod_pos = min(mod_pos, local::vec3one) - max(vec3(0.0), mod_pos - local::vec3one);
    for(int i=0; i<local::voronoi_size; i++)
    {
        float d = distance(mod_pos, local::voronoi_positions[i]);
        mind = min(mind, d);
    }

    return mind;
}

float voronoi3D_2neighbourgs_indexed(in vec3 pos, in float cell_size, out int cellindex)
{
    vec3  mod_pos = fract(pos/cell_size/2.0f);
    mod_pos *= 2.0f;
    mod_pos = min(mod_pos, local::vec3one) - max(vec3(0.0), mod_pos - local::vec3one);
    
    float mind1 = 3.0;
    float mind2 = 3.0;
    cellindex = 0;
    
    for(int i=0; i<local::voronoi_size; i++)
    {
        float d = distance(mod_pos, local::voronoi_positions[i]);
        if(d < mind1)
        {
            mind2 = mind1;
            mind1 = d;
            cellindex = i;
        }
        else
        {
            mind2 = min(mind2, d);
        }
    }

    return 2.0f*abs(mind1-mind2)/(mind1+mind2);
}

float voronoi3D_2neighbourgs(in vec3 pos, in float cell_size)
{
    int i;
    return local::voronoi3D_2neighbourgs_indexed(pos, cell_size, i);
}

vec3 voronoi3D_vec3distance(in vec3 pos, in float cell_size)
{
    vec3 mind = vec3(3.0);
    vec3  mod_pos = fract(pos/cell_size/2.0f);
    mod_pos *= 2.0f;
    mod_pos = min(mod_pos, local::vec3one) - max(vec3(0.0), mod_pos - local::vec3one);
    for(int i=0; i<local::voronoi_size; i++)
    {
        mind = min(mind, local::voronoi_positions[i] - mod_pos);
    }

    return mind;
}

float custom_close_brick_coeff(in vec3 pos, in float size, in float threshold, in float darkness, out float noncampled_vd)
{
    noncampled_vd = local::voronoi3D_2neighbourgs(pos, size);
    return ((noncampled_vd>threshold) ? darkness : 1.0);
}

float custom_close_brick_coeffvd(in float vd, in float threshold, in float darkness)
{
    return ((vd>threshold) ? darkness : 1.0);
}

float close_brick_coeff(in vec3 pos)
{
    float x;
    return local::custom_close_brick_coeff(pos, 1.6f, 0.25f, 0.985f, x);
}

float close_brick_coeffvd(in float vd)
{
    return ((vd>0.25f) ? 0.985 : 1.0);
}

