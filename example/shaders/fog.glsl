void instance_fog(in vec3 in_pos  , in::instance vec3 center,
				  out vec3 out_pos, out vec3 frag_center)
{
	local::vertex_only(center+in_pos, out_pos);
	frag_center = center;
}

void global_fog(inout vec4 color, in vec3 pos)
{
	float tolerance_view = 30.0f;
	float d = max(0.0, local::camdist(pos)-tolerance_view);
	float coeff =  1.0/(d*uniform::fog_coeff+1.0);
	color = mix(uniform::fog_color, color, coeff);
}

void fog(in vec3 frag_pos, in vec3 fog_center,
		 out vec4 color)
{
	float radius = 15.0f;
	vec3 d = fog_center - uniform::camera_position;
	float alpha  = acos(dot(normalize(d),
					        normalize(frag_pos - uniform::camera_position)));
	float R = length(d)*sin(alpha);
	if(R>radius)
	{
		discard;
	}
	float coeff = R/radius;
	coeff = 1.0 - coeff*coeff;
	coeff = sqrt(coeff);
	coeff = sqrt(coeff);

	color = vec4(0.3, 0.3, 0.4, 0.4) * coeff;

	// gl_FragDepth = -1.0;
}

float oriented_fog(in vec3 pos)
{
	vec3 vd = pos - uniform::camera_position;
	float d = length(vd);

	// return 1.0 - 1.0f/(d/uniform::fog.w + 1.0f);
	return 1.0f-exp(-d*0.02f);
}