#include "../motor/shaders/functions.glsl"

vec3 noisin(in vec3 v)
{
	vec3 ret = sin(v);
	ret+=sin(v*1.5f+1.0f);
	ret+=sin(v*1.3f+3.0f);
	ret+=sin(v*1.2f+2.0f);
	ret+=sin(v*0.3f+1.5f);
	return ret;
}
	
float noisinf(in float v)
{
	float ret = sin(v);
	ret+=sin(v*1.5f+1.0f);
	ret+=sin(v*1.3f+3.0f);
	ret+=sin(v*1.2f+2.0f);
	ret+=sin(v*0.3f+1.5f);
	return ret;
}
	