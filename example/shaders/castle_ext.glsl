void tower_vertex(in vec3 rel_pos,
                  in vec3 color,
                  in::instance vec4 center_height,
                  in::instance vec4 orientationN_size,
                  out vec3 pos,
                  out vec3 fcolor)
{
    vec3 new_relpos = local::rotate_redirect2D(rel_pos, vec3(1,0,0), orientationN_size.xyz);
    pos =   center_height.xyz
          + vec3(new_relpos.x, abs(new_relpos.y) ,new_relpos.z)*orientationN_size.w/2.0f
          + vec3(0, (new_relpos.y < -0.01f) ? abs(center_height.w) : 0,0);

    gl_Position = local::camera_transform(pos);

    fcolor = color;
}

void tower_fragment(in vec3 pos, in vec3 fcolor, out vec4 final_color)
{
    final_color = vec4(clamp(fcolor.xyz, vec3(0.0) ,vec3(1.0)), 1.0);
    float vd = local::voronoi3D_distance(pos, 5.0f);

    final_color = final_color*(1.0f+vd/0.8f);
    local::vdbrick(final_color.xyz, pos);
}

void wall_vertex(in vec3 rel_pos,
                 in vec3 color,
                 in::instance vec4 start_height,
                 in::instance vec4 orientationN_length,
                 out vec4 pos_relhight,
                 out vec4 fcolor_reldepth)
{
    vec3 new_relpos = vec3(rel_pos.x, rel_pos.y*start_height.w, rel_pos.z*orientationN_length.w);

    new_relpos       = local::rotate_redirect2D(new_relpos, vec3(0,0,1), orientationN_length.xyz);
    pos_relhight.xyz = start_height.xyz + new_relpos;

    gl_Position = local::camera_transform(pos_relhight.xyz);

    pos_relhight.w      = rel_pos.y;
    fcolor_reldepth.w   = rel_pos.z;
    fcolor_reldepth.xyz = color;
}

void wall_fragment(in vec4 pos_relhight,
                   in vec4 fcolor_reldepth ,
                   out vec4 final_color,
                   out vec4 bloom_color)
{

    final_color = vec4(clamp(fcolor_reldepth.xyz, vec3(0.0) ,vec3(1.0)), 1.0);
    float vd = local::voronoi3D_distance(pos_relhight.xyz, 15.0f / (1.0f + max(0, pos_relhight.y*pos_relhight.y/500.0f - 3.0f)));

    final_color = final_color*(1.0f+vd/2.5f);
    local::vdbrick(final_color.xyz, pos_relhight.xyz);
    bloom_color = vec4(0);

    // int wein = int(fcolor_reldepth.w*50.0f);
    // if(pos_relhight.w>0.6 && pos_relhight.w<0.75 && wein%5==0)
    // {
    //     final_color = vec4(0.4,0.4,1,0);
    //     bloom_color = final_color*2.0f;
    // }

    int loophole_deg = int(fcolor_reldepth.w * 500.0f);

    bool loophole =  pos_relhight.w > 0.5
                  && pos_relhight.w < 0.7
                  && loophole_deg % 100 == 0;

    local::castle_floor_dark(final_color, pos_relhight.xyz);
    final_color *= loophole ? 0.1 : 1.0;


    local::all_lights_mesh_normal(final_color, pos_relhight.xyz);
}