#include "shaders.glsl"
#include "camera_light.glsl"

void lantern_vertex(in vec3 in_pos,
					in vec3 in_color,
					in::instance vec4 dep_size,
					in::instance vec3 lightcolor,
					in::instance vec3 directionN,
					out vec3 out_position,
					out vec4 out_color)
{
	vec3 rotated_coord = local::rotate_redirect2D(in_pos*dep_size.w, vec3(0,0,1), directionN);
	
	local::vertex_only(dep_size.xyz + rotated_coord, out_position);

	out_color = (in_color == vec3(0)) ? vec4(lightcolor, 1.0f) : vec4(in_color, 0.0f);
}

void lantern_fragment(in vec3  frag_pos,
					  in vec4  frag_color,
					  out vec4 final_color,
					  out vec4 bloom_color)
{
	final_color = vec4(frag_color.xyz * (1.0f + 2.0f*frag_color.w), 1.0);
	bloom_color = vec4(frag_color.xyz, 1.0)*clamp(local::camera_distance(frag_pos)*0.6, 8.0f, 60.0f);
	// bloom_color = frag_color*sqrt(max(local::camera_distance(frag_pos), 4.0f))*1.5f;

	if(frag_color.w < 0.5f)
	{
		local::all_lights_mesh_normal(final_color, frag_pos);
	}
}