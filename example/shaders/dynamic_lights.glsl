#include "../motor/shaders/functions.glsl"

vec3 dynamic_lights_color(in vec3 position, in vec3 normalN, in float normal_coeff)
{
	vec3 ret = vec3(0.0f);

	for(int i=0; i < uniform::DynamicLightor_dynamic_lightor_positions_saturation_size; i++)
	{
		vec4 pos_satur    = texelFetch(uniform::DynamicLightor_dynamic_lightor_positions_saturation, i, 0);
		vec4 color_intens = texelFetch(uniform::DynamicLightor_dynamic_lightor_colors_intens       , i, 0);
		
		vec3  pos       = pos_satur.xyz;
		vec3  color     = color_intens.xyz;
		float intensity = color_intens.w;
		float satur     = pos_satur.w;

		if(intensity < 0.0f) continue;

		float d = distance(position, pos);

		float pixel_normal_coeff = local::normal_multiplior(position, normalN, pos);
		pixel_normal_coeff       = mix(1.0f, pixel_normal_coeff, normal_coeff);

		ret += min(color*intensity/(d*d)*pixel_normal_coeff, satur);
	}

	return ret;
}