#include "../motor/shaders/functions.glsl"
#include "../motor/shaders/isometric_shadower.glsl"

#include "static_ligths.glsl"
#include "camera_light.glsl"
#include "other_lights.glsl"
#include "dynamic_lights.glsl"
#include "sky.glsl"
#include "fog.glsl"

float light_normal_coeff  = 0.90;
float smooth_normal_coeff = 0.08;
float ambiant_ligth       = 0.13;

vec3 merge_lights_colors(in vec3 pos, in vec3 initial_color, in vec3 lights_colors)
{
	vec3 ret = mix(initial_color*lights_colors, initial_color, local::ambiant_ligth);

	return ret;
	// return mix(ret, uniform::fog.xyz, local::oriented_fog(pos));
}

void centralized_modifier(inout vec4 color, in vec3 position)
{
	
}

vec3 all_ligth_sum(in vec3 position, in vec3 normalN, in float normal_coeff)
{
	vec3 lights_result = vec3(0.0);
	lights_result += local::camera_light_color(position, normalN, normal_coeff);
	lights_result += local::static_ligth_color(position, normalN, normal_coeff);
	lights_result += local::castle_ligth(position, normalN, normal_coeff);
	lights_result += local::dynamic_lights_color(position, normalN, normal_coeff);
	lights_result += local::sun_light_color(position, normalN, normal_coeff);

	return lights_result;
}

void all_lights_mesh_normal(inout vec4 color, in vec3 position)
{
	vec3 normalN = normalize(local::native_normal(position));

	vec3 lights_result = local::all_ligth_sum(position, normalN, local::light_normal_coeff);

	color.xyz = local::merge_lights_colors(position, color.xyz, lights_result);	
	local::centralized_modifier(color, position);
}

void all_lights_smooth(inout vec4 color, in vec3 position)
{
	vec3 normal_uselessN = vec3(1.0,0.0,0.0);

	vec3 lights_result = local::all_ligth_sum(position,  normal_uselessN, local::smooth_normal_coeff);

	color.xyz = local::merge_lights_colors(position, color.xyz, lights_result);
	local::centralized_modifier(color, position);
}

void all_lights_smooth_bloomed(inout vec4 color, inout vec4 bloom_color, in vec3 position)
{
	vec3 normal_uselessN = vec3(1.0,0.0,0.0);

	vec3 lights_result = local::all_ligth_sum(position,  normal_uselessN, local::smooth_normal_coeff);

	color.xyz = local::merge_lights_colors(position, color.xyz, lights_result);
	bloom_color.xyz = local::merge_lights_colors(position, bloom_color.xyz, lights_result);
	local::centralized_modifier(color, position);
}

void all_light_custom_normal(inout vec4 color, in vec3 position, in vec3 normalN, in float normal_coeff)
{
	vec3 lights_result = local::all_ligth_sum(position,  normalN, normal_coeff);

	color.xyz = local::merge_lights_colors(position, color.xyz, lights_result);
	local::centralized_modifier(color, position);
}
		