void explobibo_vertex(in vec2 rel_pos, in::instance vec3 center, in::instance vec3 bibcolor, in::instance vec4 size_U_stime_U_duration,
					  out vec2 bibocoord, out vec3 fcolor)
{
	vec2  new_size   = size_U_stime_U_duration.xy;
	bool is_timed = (size_U_stime_U_duration.w >= 0.0);

	if(is_timed)
	{
		float time_coeff = (uniform::explobibo_time - size_U_stime_U_duration.z)/size_U_stime_U_duration.w;
		new_size *= clamp((0.5-abs(time_coeff-0.5))*3.0f, 0.0f, 1.0f);
	}
	float zoverride = is_timed ? 0.1f : 0.0f;

	vec3 new_pos = local::bibolar_proj(center, rel_pos*new_size, uniform::camera_direction);
	gl_Position  = local::camera_transform(new_pos);
	gl_Position.z = max(-gl_Position.w, gl_Position.z - zoverride);

	bibocoord = rel_pos;
	fcolor    = bibcolor;
}

void explobibo_fragment(in vec2 bibocoord, in vec3 fcolor, out vec4 bloom_color)
{	
	vec2 extcoeff = (vec2(1.0f) - abs(bibocoord));

	bloom_color = vec4(fcolor*extcoeff.y*extcoeff.x*extcoeff.y, 0.0f);
}