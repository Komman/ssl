#include "shaders.glsl"

void explosphere_vertex(in vec3 relpos,
                        in::instance vec3 center,
                        in::instance vec3 color,
                        in::instance vec3 size_U_start_time_U_duration,
                        out vec3 fpos,
                        out vec3 fcenter,
                        out vec3 fcolor,
                        out vec3 size_U_timecoeff_U_maxsize)
{
	float time_coeff = (uniform::explospheres_time - size_U_start_time_U_duration.y)/size_U_start_time_U_duration.z;
	float new_size   = size_U_start_time_U_duration.x*sin(time_coeff*local::PI);
	vec3 new_pos = center + relpos * new_size;

	local::vertex_only(new_pos, fpos);

	fcenter = center;
	fcolor = color;
	size_U_timecoeff_U_maxsize.x = new_size;
	size_U_timecoeff_U_maxsize.y = time_coeff;
	size_U_timecoeff_U_maxsize.z = size_U_start_time_U_duration.x;
}

void explosphere_fragment(in vec3 fpos,
						  in vec3 fcenter,
						  in vec3 fcolor,
						  in vec3 size_U_timecoeff_U_maxsize,
                          out vec4 color,
                          out vec4 bloom_color)
{
	vec3 cam_to_fragN = normalize(fpos - uniform::camera_position);
    vec3 sphere_pos = local::sphere_proj(uniform::camera_position, cam_to_fragN, fcenter, size_U_timecoeff_U_maxsize.x);

    if(sphere_pos == vec3(0.0))
    {
		discard;
    }

    if(distance(uniform::camera_position, fcenter) < size_U_timecoeff_U_maxsize.x)
	{
		color = vec4(fcolor, 1)*100.0f;
		bloom_color = vec4(fcolor, 1)*100.0f;
	}
	else
	{
		gl_FragDepth = local::depth_from_proj(local::camera_transform(sphere_pos));

   		vec3  biboproj    = local::proj_on_plan(sphere_pos - fcenter, cam_to_fragN);
	    float bibo_radius = length(biboproj) / size_U_timecoeff_U_maxsize.x;
	    float d = local::voronoi3D_distance(sphere_pos + vec3(0.0,-uniform::time*10.0f,0.0), size_U_timecoeff_U_maxsize.z*0.4f);

		color = vec4(mix(vec3(1.0), fcolor, bibo_radius), 1.0f);
		bloom_color = vec4((fcolor)*4.0f, 1.0f);

	 	bloom_color *= (bibo_radius >= 0.8) ? 2.0f : 1.0f;

	 	float overbeginlight = 2.0f;
	 	float timebegincoeff = 1.0 + max((0.3-size_U_timecoeff_U_maxsize.y)/0.3, 0.0)*overbeginlight;

	 	color       *= timebegincoeff;
	 	bloom_color *= timebegincoeff;

		color *= (d+0.5)*1.2f;
	}

    
}
