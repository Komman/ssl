#include "shaders.glsl"
#include "tower_bomb.glsl"
#include "castle.glsl"
#include "upshadow.glsl"

void mesh_viewer_vert(in vec3 pos, out vec3 posf)
{
	posf = pos;
	gl_Position = local::camera_transform(pos);
}

void mesh_viewer_frag(in vec3 posf, out vec4 color)
{
	local::base_fragment(posf, vec3(0.5f,0.5f,0.5f), color);
	local::all_lights_mesh_normal(color, posf);
}
