#include <iostream>
#include <unistd.h>

#include <glm/gtx/string_cast.hpp>

#include "../ssl/ssl.hpp"

#include "utils/plan_graph.hpp"
#include "utils/coligrid.hpp"
#include "utils/space_grid.hpp"

#include "motor/premade_buffers.hpp"
#include "motor/debug_draw.hpp"
#include "motor/crown_hitbox.hpp"
#include "motor/smooth_setter.hpp"
#include "motor/mousekey_switcher.hpp"
#include "motor/mouse_joystick_mktarget.hpp"
#include "motor/text_box.hpp"
#include "motor/entity_context.hpp"
#include "motor/building.hpp"

#include "game/archer.hpp"
#include "game/castle_world.hpp"
#include "game/projectiles.hpp"
#include "game/absolute_objects.hpp"
#include "game/effects.hpp"
#include "game/shareders.hpp"
#include "game/shinfos.hpp"
#include "game/sword.hpp"
#include "game/shareders.hpp"
#include "game/game_console.hpp"
	
using namespace std;	
using namespace ssl;
using namespace glm;

static time_t RANDOM_SEED = 0;
static void print_seed(time_t t)
{
	static const std::string seedfilepath = "last_seeds.txt";
	fstream f(seedfilepath, fstream::app);
	if(f.is_open())
		f<<t<<endl;
	else
		ssl::war("failed to open " + seedfilepath);
}

int main()
{
	{
		RANDOM_SEED = (RANDOM_SEED == 0) ? time(NULL) : RANDOM_SEED;
		cout<<TERMB::GREEN<<"SEED: "<< RANDOM_SEED <<TERM::NOCOL<<endl; print_seed(RANDOM_SEED);
		srand(RANDOM_SEED);
		
		// srand(1730946803); // TESTER ATOWERS

		// srand(1737231797); // CACAAAAA ACHILLE
		// srand(1737914855); // OH POPETTE
		
		// srand(1715532171); // VOIR WARNING DANS castle_placer_utils.cpp (~l:1450)
		// srand(1715551991); // VOIR WARNING DANS castle_placer_utils.cpp (~l:1450)
		// srand(1717879585); // PEUT ETRE WARNING MAIS PEUT ETRE OULALA
		// srand(1719684777); // PEUT ETRE WARNING MAIS PEUT ETRE OULALA
		// srand(1723217291); // PEUT ETRE WARNING MAIS PEUT ETRE OULALA

		// srand(1729992858); // C'EST LA FAUTE DU MAXDELARGEMENT QUI EST PAS ENCORE BON
		// srand(1730246049); // OULALA
		// srand(1730725043); // OULALA
		// srand(1730728229); // OULALA
		// srand(1735418686); // OULALA
		// srand(1735553137); // OULALA

		// srand(1729773061); // MAX CYCLE DELARGEMENT TEST
		// srand(1729773101); // MAX CYCLE DELARGEMENT TEST
		// srand(1729772581); // MAX CYCLE DELARGEMENT TEST
		// srand(1729772602); // MAX CYCLE DELARGEMENT TEST
		// srand(1729772641); // MAX CYCLE DELARGEMENT TEST

		// srand(1735644475) // VOIR WARNING DANS castle_cathedral__structure.cpp (~l:119)
	}

	ssl::init("bouh");

	cout<<"Window Size: "<<window::size().x<<"x"<<window::size().y<<endl;

	// Initialisations
	premade_buffers::init();
	shareders::init();
	debug_draw::init();
	ingame::init();
	effects::init();

	TextBox textbox("text/characters.ppm", 300, "textbox");

	Uniform<float> test("test", 0.0);
	Uniform<vec4> fog("fog", vec4(vec3(2,2,2.5)*0.05f, 10.0f));

	SharedRessources shared_ressources(vec4(0.0), bloomParameter({5, 8, 0.35}));
	auto& game_drawer = shared_ressources.tools().game_drawer;
	
	projectiles::init(shared_ressources);


	Archer cam(vec3(0.0f), 6.5f, shared_ressources);

	EntityContext::global->set_player(&cam);
	ingame::set_camera_position_ptr(&(cam.get_view().get_position()));

	AbsoluteObjects abs_objs = {
		.archer                = &cam,
		.all_arrows            = &projectiles::arrows(),
		.clouds                = &effects::clouds(),
		.packed_clouds         = &effects::packed_clouds(),
		.dynamic_lightor       = &effects::dynamic_lightor(),
		.timed_dynamic_lightor = &effects::timed_dynamic_lightor(),
		.explodiscs            = &effects::explodisc(),
		.explospheres          = &effects::explospheres(),
		.explobibos            = &effects::explobibos(),
		.exploraylights        = &effects::meshedlight_luminor()
	};
	shinfos::init(abs_objs);


	CastleWorld* castle_world = new CastleWorld(abs_objs, shared_ressources);
	
	const auto& floor = castle_world->get_ground();
	cam.set_context(&(castle_world->get_hbx_context()));
	cam.tp(floor.get_xyz(-100.0, 0.0)+vvv::y()*100.0f - vvv::x()*80.0f + vvv::z()*70.0f);
	cam.look_at(vec3(0.0, 50.0, 0.0));

	// ssl::enable_prints(SSL_PRINTS_DRAW);
	// ssl::enable_prints(SSL_PRINTS_MEMORY_FLOW);
	// ssl::enable_prints(SSL_PRINTS_MEMORY);
	// ssl::enable_prints(SSL_PRINTS_LAZY_OP);
	// ssl::enable_prints(SSL_PRINTS_PARAMS_SETTING);

	float time_div = 1.0f;
	float sleept   = 10.0f;
	float recordgl = 1.0f;

	// Global console
	ConsoleObjects cobjs = {
		.archer = &cam,
		.shared_ressources = &shared_ressources,
		.castle = &castle_world->get_castle()
	};
	GameConsole console(cobjs, &(ingame::mk_switcher()), SSL_KEY(ENTER), 3.0f);
	CONSOLE::set(&console);

	CONSOLE_CONTROL(time_div);
	CONSOLE_CONTROL(recordgl);
	CONSOLE_CONTROL(sleept);

	shareders::try_to_initialize();
	window::reset_frame_time();

	// TODO
	console.todo("kill objects out of the map");
	console.send_msg("/gamemode 1");

	CONSOLE::say("SIZE OF THE CASTLE: ");
	CONSOLE::say(" -Outside: " + to_string(castle_world->get_castle().real_base().get_mesh().size()) + " vertices and " + to_string(castle_world->get_castle().real_base().get_mesh().get_elements().size()/3) + " triangles");
	CONSOLE::say(" -Inside:  " + to_string(castle_world->get_castle().real_base().get_inside_mesh().size()) + " vertices and " + to_string(castle_world->get_castle().real_base().get_inside_mesh().get_elements().size()/3) + " triangles");

	while(!window::should_close() && !keyboard::key_pressed(SSL_KEY(F4)))
	{
		if(recordgl && !game_drawer.recording()) AutoPrintRecorder::GL_START();
		if(ingame::game_key_pressed('P')) {test += window::delta_time();}
		if(ingame::game_key_pressed('M')) {test -= window::delta_time();}
		if(ingame::game_key_pressed(',')) {game_drawer.record_disable();}
		if(ingame::game_key_pressed(';')) {game_drawer.record_enable();}
		if(ingame::game_key_pressed('O')) {time_div = 1.0f;}
		if(ingame::game_key_pressed('L')) {time_div = 20.0f;}

		float dt = window::delta_time();

		castle_world->animate(abs_objs, dt / time_div);
		CONSOLE::get().animate(dt);

		ingame::mk_switcher().animate(dt);

		castle_world->draw(abs_objs);

		textbox.draw_console(CONSOLE::get(), shareders::text_renderer(), vec2(0.01, 0.01), 0.015f);
		if(ingame::game_key_pressed(SSL_KEY(F3)))
			{textbox.draw_text("XYZ: " + glm::to_string(ivec3(cam.get_position())), shareders::text_renderer(), vec2(0,0.96), 0.02);}

		if(recordgl)
		{
			if(!game_drawer.recording())
			{
				AutoPrintRecorder::GL_STOP();
			}
			else
			{
				game_drawer.record_print();
			}
		}

		ingame::interframe(dt / time_div);
		usleep(sleept*1000);
	}

	ssl::disable_prints(SSL_PRINTS_MEMORY);
	ssl::disable_prints(SSL_PRINTS_MEMORY_FLOW);

	delete castle_world;

	//  Order of frees is important 
	shinfos::free();
	projectiles::free();
	effects::free();
	ingame::free();
	debug_draw::free();
	shareders::free();
	premade_buffers::free();

	ssl::close();

	cout<<TERMB::GREEN<<"SEED: "<< RANDOM_SEED <<TERM::NOCOL<<endl;
	return 0;
}
