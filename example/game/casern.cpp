#include "casern.hpp"


using namespace glm;

Casern::Casern(VerticalShadower* vshadower, CastleSupport* support, SimpleTower* tower, ArcDoor* door, FatTower* fat_tower)
	: CastleBuildingPlacer(
		{
			{},
			{
				valuedPolygon2DInfo{
					vec2(0),
					{
						vec2(-1,-1)/2.0f,
						vec2(-1,+1)/2.0f,
						vec2(+1,+1)/2.0f,
						vec2(+1,-1)/2.0f
					},
					1
				}
			}
	  }),
	  _vshadower(vshadower),
	  _support(support),
	  _tower(tower),
	  _door(door),
	  _fat_tower(fat_tower),
	  _doors_walls_color(vec3(0.7,0.5,0.3))
{

}

void Casern::place_building_only(const castleSupport& building, float h)
{
	auto b = building;
	b.center.y += h;

	this->add_casern(b);
}


castleSupport Casern::generate_building(const genParam& g)
{
	return {
		.center = vec3(g.position.x, 0.0f, g.position.y),
		.dirN2D = g.dirN,
		.size   = vec3(g.maxsize*utils::random_float(0.2f, 1.0f), g.maxsize*utils::random_float(0.2, 0.5), g.maxsize)
	};
}


planarPlace Casern::get_place(const castleSupport& g)
{
	return {
		.center = geom::projplan(g.center),
		.size   = geom::projplan(g.size),
		.dirN   = g.dirN2D
	};
}


void Casern::add_casern(const castleSupport& casern)
{
	_support->add_support(casern);

	vec3 dirN3D = vec3(casern.dirN2D.x, 0, casern.dirN2D.y);
	vec3 dirxN  = normalize(glm::cross(dirN3D, vvv::y()));

	float thickness = _support->get_thickness();
	float untilmaxY = (1.0f - thickness);
	float untilmaxXZ = untilmaxY/2.0f;
	float tsize = casern.size.x*thickness*1.2f;
	float dsize = casern.size.x*4.0f*thickness;
	float minsizeXZ = min(casern.size.x, casern.size.z);

	_tower->add_tower({
		.position =   casern.center
		            + vvv::y()*casern.size.y*untilmaxY
		            - dirN3D*(casern.size.z*untilmaxXZ - tsize)
		            + dirxN*(casern.size.x*untilmaxXZ - tsize),
		.orientationN = dirN3D,
		.height = casern.size.y*0.4f,
		.size   = tsize
	});

	_tower->add_tower({
		.position =   casern.center
		            + vvv::y()*casern.size.y*untilmaxY
		            - dirN3D*(casern.size.z*untilmaxXZ - tsize)
		            - dirxN*(casern.size.x*untilmaxXZ - tsize),
		.orientationN = dirN3D,
		.height = casern.size.y*0.4f,
		.size   = tsize
	});

	_fat_tower->add_tower({
		.center = casern.center
		          + vvv::y()*casern.size.y*untilmaxY
		          + dirN3D*abs(casern.size.z - thickness - minsizeXZ)/3.0f,
		.dirN2D = casern.dirN2D,
		.size   = vec3(1,0,1)*(minsizeXZ - thickness)*0.8f + vvv::y()*casern.size.y*1.7f
	});

	_door->add_door({
		.pos   = casern.center - dirN3D*(casern.size.z*untilmaxXZ),
		.dir2D = -casern.dirN2D,
		.size  = vec3(dsize, dsize, dsize/2.0),
		.wall_color = _doors_walls_color
	});

	_door->add_door({
		.pos   = casern.center + dirN3D*(casern.size.z*untilmaxXZ),
		.dir2D = casern.dirN2D,
		.size  = vec3(dsize, dsize, dsize/2.0),
		.wall_color = _doors_walls_color
	});

	_door->add_door({
		.pos   = casern.center - dirxN*(casern.size.x*untilmaxXZ),
		.dir2D = -vec2(dirxN.x, dirxN.z),
		.size  = vec3(dsize, dsize, dsize/2.0),
		.wall_color = _doors_walls_color
	});

	_door->add_door({
		.pos   = casern.center + dirxN*(casern.size.x*untilmaxXZ),
		.dir2D = vec2(dirxN.x, dirxN.z),
		.size  = vec3(dsize, dsize, dsize/2.0),
		.wall_color = _doors_walls_color
	});
}
