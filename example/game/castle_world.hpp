#ifndef _CASTLE_WORLD_HPP_
#define _CASTLE_WORLD_HPP_

#include "../motor/controlled_drawer.hpp"
#include "../motor/shared_ressources.hpp"
#include "../motor/vertical_shadower.hpp"
#include "../motor/static_lightor.hpp"
#include "../motor/hitbox_context.hpp"
#include "../motor/isometric_shadower.hpp"

#include "world.hpp"
#include "sky.hpp"
#include "procedural_tree.hpp"
#include "ground.hpp"
#include "castle_animator.hpp"

class CastleWorld : public ControlledDrawsWorld
{
public:
	CastleWorld(AbsoluteObjects& objs, SharedRessources& shared_ressources);
	virtual ~CastleWorld() {}

	BasicWorld* animate(AbsoluteObjects& objs, float dt) override;

	const BasicGround&   get_ground() const;
	const HitboxContext& get_hbx_context() const;

	void add_gate(ElementBuffer& mesh_indices, ArrayBuffer<glm::vec3>& mesh);

	VerticalShadower& get_vertical_shadower();

	CASTLE::CastleAnimator& get_castle();

protected:

	void far_gate_draw(const ElementBuffer& indices,
					   const ArrayBuffer<glm::vec3>& vertices);
	void close_gate_draw(GLuint mask_value);

	void draw_world(AbsoluteObjects& objs) override;

	void draw_gates();

private:
	struct gateDraw
	{
		ElementBuffer*          mesh_indices;
		ArrayBuffer<glm::vec3>* mesh;
	};

	CastleWorld(const CastleWorld& w);
	CastleWorld& operator=(const CastleWorld& w);

	void project_static_shadows(VerticalShadower& vshadower);
	
	static void build_forest(std::unique_ptr<ProceduralTree>& trees, const Ground& ground);

// private:
// 	struct VAOs
// 	{
// 		VertexArrayObject 
// 		VertexArrayObject explodiscs;
// 		VertexArrayObject explospheres;
// 		VertexArrayObject explobibos;
// 		VertexArrayObject exploraylights;
// 		VertexArrayObject ground;
// 		VertexArrayObject simple_towers;
// 		VertexArrayObject arc_bridges;
// 		VertexArrayObject supports;
// 		VertexArrayObject fat_towers;
// 		VertexArrayObject squares_towers;
// 		VertexArrayObject churchs;
// 		VertexArrayObject face_arcs;
// 		VertexArrayObject counter_forts;
// 		VertexArrayObject arc_doors;
// 		VertexArrayObject cannons;
// 		VertexArrayObject simple_walls;
// 		VertexArrayObject magic_towers;
// 		VertexArrayObject lanterns;
// 		VertexArrayObject trees;
// 	};

private:
	SharedRessources& _shared_ressources;
	GameDrawer& _game_drawer;

	std::vector<gateDraw>  _gates;
	ArrayBuffer<glm::vec4> _far_colors;

	Ground           _ground;
	VerticalShadower _vshadower;
	StaticLigthor    _sligthor;
	HitboxContext    _hbx_context;
	IsometricShadower _shadows;
	Sky               _nigth;
	
	CASTLE::CastleAnimator _castle;

	bool _catsle_building;
	bool _towers_toshadow;
	bool _walls_toshadow;
	
	// Drawers
	ShaderGenerator  _ground_gen;
	ControlledDrawer _tree_pencil;
	ControlledDrawer _ground_pencil;

	// To draw
	std::unique_ptr<ProceduralTree> _trees;
};

#endif //_CASTLE_WORLD_HPP_
