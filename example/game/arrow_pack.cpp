#include "arrow_pack.hpp"

static std::unique_ptr<ShaderGenerator> arrow_shader_gen = NULL;

ShaderGenerator* get_arrow_shader_generator()
{
	if(arrow_shader_gen == NULL)
	{
		arrow_shader_gen = std::make_unique<ShaderGenerator>("shaders/arrows.glsl");
	}

	return arrow_shader_gen.get();
}
