#ifndef _BASIC_GROUND_HPP_
#define _BASIC_GROUND_HPP_

#include "../../ssl/ssl.hpp"
#include "../motor/basic_hitbox.hpp"

class BasicGround : public ssl::ElementDrawableObject
{
public:

	virtual float get_y(float x, float y) const=0;
	virtual float size() const =0;


	float get_y(const glm::vec3& position) const; // projected on the ground
	float get_y(const glm::vec2& position) const; // on the plan of the ground

	glm::vec3 get_xyz(float x, float z) const;
	glm::vec3 get_xyz(const glm::vec2& position) const; // on the plan of the ground
	glm::vec3 get_xyz(const glm::vec3& position) const; // projected on the ground

	// get max y positions from src to dst
	virtual glm::vec3 get_max_y_position(const glm::vec3& src,
										 const glm::vec3& dest,
										 int precision) const;

	virtual void dark_color(const glm::vec3& position, float coeff) {}

	virtual const std::vector<BasicHitbox::storedHitbox>& get_hitboxes() const =0;

};

#endif //_BASIC_GROUND_HPP_
