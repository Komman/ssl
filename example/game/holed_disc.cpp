#include "holed_disc.hpp"

#include "../utils/geom.hpp"
#include "../motor/gutils.hpp"

#include "ingame.hpp"

using namespace std;
using namespace glm;

HoledDisc::HoledDisc(ssl::draw_type drtype,
			  		 unsigned int precision,
			  		 const std::string& name)

	: StoredDrawableObject(ssl::STATIC_DRAW, drtype, drtype, drtype, drtype),
	  _normalized(0),
	  _time(name + "_time", 0.0f),
	  _vao({
	  	{&(this->get_array_buffer<RELATIVE_POS>()), 0},
	  	{&(this->get_array_buffer<CENTER>()), 1},
	  	{&(this->get_array_buffer<INFOS>()), 1},
	  	{&(this->get_array_buffer<COLOR_SPEED>()), 1},
	  }, &(this->get_elements()))
{
	float angle = 0.0f;
	float dtheta = 2.0f*M_PI / float(precision);

	for(uint i=0; i<precision; i++)
	{
		_normalized.push_back(geom::circle_point(
				vec3(0.0),
				vvv::x(),
				vvv::y(),
				angle,
				1.0f
			));

		angle += dtheta;

		this->get_array_buffer<RELATIVE_POS>().add_value(glm::vec4(0.0f));
		this->get_array_buffer<RELATIVE_POS>().add_value(glm::vec4(0.0f));

		int ipos  = (2*i  )%(2*precision);
		int ileft = (2*i+2)%(2*precision);
		int iup   = (2*i+1)%(2*precision);
		int iopp  = (2*i+3)%(2*precision);

		_elements.add_rectangle(ipos, ileft, iopp, iup);
	}


	float sqrt2 = 1.0f/(cos(M_PI/float(precision)));

	for(int i=0; i<(int)_normalized.size(); i++)
	{
		this->get_array_buffer<RELATIVE_POS>().change_subvalue(2*i + 0, vec4(_normalized[i],       0.0f));
		this->get_array_buffer<RELATIVE_POS>().change_subvalue(2*i + 1, vec4(_normalized[i]*sqrt2, 1.0f));
	}
}

const VertexArrayObject& HoledDisc::get_vao() const
{
	return _vao;
}

void HoledDisc::add(const discParams& params)
{
	glm::vec3 center     = params.center;
	glm::vec3 color      = params.color;
	float     radius     = params.radius;
	float     thickness  = params.thickness;
	float     duration   = params.duration;
	float     speed      = params.speed;

	if(this->get_array_buffer<RELATIVE_POS>().size() != _normalized.size()*2)
	{
		ssl::err("HoledDisc::recreate(): bad size:" +TERM::WHITE +  " _normalized.size() = " + std::to_string(_normalized.size()) 
				+ ERR_COLOR + " but" + TERM::WHITE + " this->size() = " +std::to_string(this->size()));
	}

	this->get_array_buffer<CENTER>().add_value(center);
	this->get_array_buffer<INFOS>().add_value(vec4(radius, thickness, _time.get_value(), duration));
	this->get_array_buffer<COLOR_SPEED>().add_value(vec4(color, speed));
}

void HoledDisc::check_sizes() const
{
	if(    this->get_array_buffer<INFOS>().size()       != this->get_array_buffer<INFOS>().size()
		|| this->get_array_buffer<CENTER>().size()      != this->get_array_buffer<INFOS>().size()
		|| this->get_array_buffer<COLOR_SPEED>().size() != this->get_array_buffer<INFOS>().size())
	{
		ssl::err("HoledDisc::check_sizes(): wrong sizes");
	}
}

uint HoledDisc::instances_amount() const
{
	#ifdef SSL_DEBUG
	this->check_sizes();
	#endif

	return this->get_array_buffer<INFOS>().size();
}

void HoledDisc::swapop(uint instance_index)
{
	if(instance_index >= this->instances_amount())
	{
		ssl::err("HoledDisc::swapop(uint instance_index): index out of bound");
	}

	utils::swapop(this->get_array_buffer<CENTER>()     , instance_index);
	utils::swapop(this->get_array_buffer<INFOS>()      , instance_index);
	utils::swapop(this->get_array_buffer<COLOR_SPEED>(), instance_index);
}

bool HoledDisc::has_expired(uint instance_index) const
{
	#ifdef SSL_DEBUG
	if(instance_index >= this->instances_amount())
	{
		ssl::err("HoledDisc::has_expired(uint instance_index): index out of bound : "
			+ to_string(instance_index) + "/" + to_string(this->instances_amount()));
	}
	#endif

	vec4 infos = this->get_array_buffer<INFOS>()[instance_index];

	float start_time = infos.z;
	float duration = infos.w;

	return (_time.get_value() - start_time >= duration);
}

void HoledDisc::animate(float dt)
{
	_time += dt;

	for(uint i=0; i<this->instances_amount(); i++)
	{
		if(this->has_expired(i))
		{
			this->swapop(i);

			i--;
		}
	}

	if(this->instances_amount() == 0)
	{
		// To prevent time to be not precise enough if it is too big
		_time = 0.0f;
	}
}
