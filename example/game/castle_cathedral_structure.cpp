#include "castle_cathedral_structure.hpp"

#include "../../ssl/src/vvv.hpp"
#include "../utils/mutils.hpp"

#include "../motor/building.hpp"
#include "../motor/meshbuild.hpp"
#include "../motor/wall_hitbox.hpp"
#include "../motor/paver_hitbox.hpp"
#include "../motor/bevel_hitbox.hpp"
#include "../motor/cycle_enlargement_hitbox.hpp"
#include "../motor/mesh_hitbox.hpp"

#include "castle_colors.hpp"

namespace CASTLE
{

	using namespace glm;
	using namespace std;

	CathedralStructure::CathedralStructure(const baseBuiling& base, uint face)
		: FaceStructure(base, face)
	{

	}

	void CathedralStructure::build_wall_deco(const std::vector<doorWall>& walls_to_build)
	{
		uint N = walls_to_build.size();

		for(uint i=0; i<N; i++)
		{
			const doorWall& w = walls_to_build[i];

			if(w.dh < 0)
				continue;

			if(w.dh > this->get_over_height() + this->safe_height())
			{
				this->build_topdeco(w);
			}
		}
	}

	/*
		     un3___
		   un2_/
		     /      y3
		   _/
		  |_        y2 e2 (height of the bluff) ov2 (overborder)
		    |
		 ___|
		|_  ^       y1 e1 (height of the bluff) ov1 (overborder)
		  | |
		  | |
		    un1
	*/

	static float hcoeff  = 50.0f;
	static float y1coeff = 0.6f;
	static float y2coeff = 0.8f;
	static float y3coeff = 0.9f;

	float CathedralStructure::get_over_height() const
	{
		return (1.0f - y1coeff)*hcoeff*this->ND();
	}

	void CathedralStructure::build_face(const std::vector<doorWall>& walls_to_build)
	{

		ArrayBuffer<glm::vec3>& meshpos    = this->mesh_vertices();
		// ArrayBuffer<glm::vec3>& inmeshpos  = this->inmesh_vertices();
		ElementBuffer& elements   = this->get_elements();
		// ElementBuffer& inelements = this->get_inelements();
		float ND = this->ND();
		float h = hcoeff*ND;

		#warning this->wall_radius()*0.7f: bissectrice delargement inside causes potential overlapping
		float y1 = y1coeff*h; float e1 = 0.03*h; float ov1 = std::min(0.03f*h, this->wall_radius()*0.7f);
		float y2 = y2coeff*h; float e2 = 0.03*h; float ov2 = 0.03*h;
		float y3 = y3coeff*h;
		float ytop = h;
		vec3 base_color = this->walls_color();
		vec3 upcolor = this->walls_color()*0.7f;
		vec3 roofcolor = castle_colors::CATHEDRAL_ROOF();

		this->fill_walls(walls_to_build);

		std::vector<uint> vertices = this->mesh_vertices_indices();
		std::vector<indexedWall> iwalls = this->index_wall(walls_to_build, vertices);
		vertices = this->extract_indices(iwalls);

		vector<vec3> pos = meshpos.subset(vertices);

		float maxdelarge = geom::cycle_conspace_maxdelargement(geom::projplan(pos));
		float delacoeff = 0.3f;
		float un1 = std::min(0.2f*maxdelarge*delacoeff, this->wall_radius()*0.9f);
		float un2 = 0.3*maxdelarge*delacoeff;
		float un3 = 0.9*maxdelarge*delacoeff;
		// float 	bx = 0.05*ND;
		float winwallwidth = this->wall_radius() - un1;
		float roofwidth = -maxdelarge*0.01f;

		if(winwallwidth < 0.0f)
		{
			mot::err("CathedralStructure::build_face(): it's not lethal but a delargement will be necessary");
		}

		pair<uint, uint> indices;
		float curveprec = ov1+un1+un3;

		pair<uint, uint> indices2 = this->conspace_enlarge_cycle(vertices, +ov1, curveprec, vec3(0), base_color, true, true);
		pair<uint, uint> indices3 = this->conspace_enlarge_cycle(utils::uirange(indices2), 0.0f, curveprec, vvv::y()*e1, base_color, true, true);
		indices = this->conspace_enlarge_cycle(utils::uirange(indices3), -(un1+ov1), curveprec, vec3(0), base_color*0.6f, true, true);
	
		// FIXING IF ADJACENT TO A WALL
		// #warning TODO: (with partial cycle)
		// for(uint i=0; i<iwalls.size(); i++)
		// {
		// 	if(iwalls[i].wall.dh > -(y2-y1))
		// 	{
		// 		uint j = (i+1)%iwalls.size();
		// 		meshpos.change_subvalue(indices2.first + i,
		// 			geom::yfixed(pos[i], meshpos[indices2.first + i].y)
		// 		);
		// 		meshpos.change_subvalue(indices3.first + i,
		// 			geom::yfixed(pos[i], meshpos[indices3.first + i].y)
		// 		);
		// 		meshpos.change_subvalue(indices2.first + j,
		// 			geom::yfixed(pos[j], meshpos[indices2.first + j].y)
		// 		);
		// 		meshpos.change_subvalue(indices3.first + j,
		// 			geom::yfixed(pos[j], meshpos[indices3.first + j].y)
		// 		);
		// 	}
		// }

		wallInfo wall = {
			.excycle = meshpos.subset(indices3),
			.incycle = meshpos.subset(indices),
			.sized_direction = -vvv::y()*e1
		};
		this->add_hitbox(std::make_unique<WallHitbox>(wall));
		
	
		// WINDOW FACADE
		pair<uint, uint> indices_fbot = meshbuild::copytranslate(meshpos, indices, vec3(0));
		this->complete_color_buffer(upcolor);
		pair<uint, uint> indices_ftop = this->extrude_cycle(indices_fbot, vvv::y()*(y2-y1), upcolor, true);
		
		// SECOND BLUFF
		this->place_conspace_wall_hitbox(indices_ftop, -(y2-y1), -winwallwidth);
		indices = meshbuild::copytranslate(meshpos, indices_ftop, vec3(0));
		this->complete_color_buffer(base_color*0.6f);

		indices = this->conspace_enlarge_cycle(utils::uirange(indices), + this->wall_radius(), curveprec, vec3(0), base_color, true, true);
		indices = this->conspace_enlarge_cycle(utils::uirange(indices), 0, curveprec, vvv::y()*e2, base_color, true, true);
		indices = this->conspace_enlarge_cycle(utils::uirange(indices), -ov2, curveprec, vec3(0), base_color, true, true);
		
		this->place_conspace_wall_hitbox(indices, -e2, ov2);

		// PLACE ROOF
		uint roofei_start = elements.size();
		indices = meshbuild::copytranslate(meshpos, indices, vec3(0));
		this->complete_color_buffer(roofcolor);
		pair<uint, uint> indicesr = this->conspace_enlarge_cycle(utils::uirange(indices), -un2, curveprec, vvv::y()*(y3-y2), roofcolor, true, true);
		indices = this->conspace_enlarge_cycle(utils::uirange(indicesr), -un3, curveprec, vvv::y()*(ytop-y3), roofcolor, true, true);

		meshbuild::fill_cycle(meshpos, elements, indices, true);

		meshSurface surface = {
			.mesh_points = &(meshpos.get_value()),
			.elements = &(elements.get_value()),
			.part = {roofei_start, elements.size()},
			.width = roofwidth
		};
		this->add_hitbox(std::make_unique<MeshHitobox>(surface, this->player_size()));


		// PLACE WINDOWS
		if((indices_fbot.second - indices_fbot.first) != (indices_ftop.second - indices_ftop.first))
		{
			mot::err("CathedralStructure::build_face(): extruding lamentably failed");
		}
		for(uint i=0; i<(indices_fbot.second+1 - indices_fbot.first); i++)
		{
			uint j = (i+1)%(indices_fbot.second+1 - indices_fbot.first);
			
			float d = glm::distance(
				meshpos[indices_fbot.first + i],
				meshpos[indices_fbot.first + j]
			);
			
			vec3 v1b = meshpos[indices_fbot.first + i]; 
			vec3 v2b = meshpos[indices_fbot.first + j]; 

			vec3 normalN = -glm::normalize(geom::trigo_normal(v2b-v1b));
			vec3 mid = geom::middle(v1b, v2b) - normalN*2.0f*ND;

			uint corwall = this->wall_intersection(walls_to_build, {mid, normalN});

			if(corwall != std::numeric_limits<uint>::max() && d > (y2-y1)*0.8f && walls_to_build[corwall].dh < -(y3-y1))
			{

				vec3 v1t = meshpos[indices_ftop.first + i]; 
				vec3 v2t = meshpos[indices_ftop.first + j]; 


				this->place_window((v1b + v2b + v1t + v2t)/4.0f, normalN,
					vec3(
						glm::distance(v1b, v2b)*0.7f,
						(v1t.y - v1b.y)*0.7f,
						2.0f*ND
					)
				);
			}
		}
	}
};	
