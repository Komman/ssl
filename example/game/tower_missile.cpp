#include "tower_missile.hpp"

#include "../motor/entity_context.hpp"

#include "ingame.hpp"
#include "effects.hpp"

using namespace std;
using namespace glm;

TowerMissile::TowerMissile(const towerMissileStart& start)
	: HitboxedArrow({start.position, start.directionN, start.length, start.length*0.4f}),
	  _light_intensity(100.0f),
	  _duration(3.5f),
	  _time(0.0f),
	  _smoke_emission(30.0f)
{
	
}

void TowerMissile::animate(const PhysicalContext& context, float dt)
{
	this->HitboxedArrow::animate(context, dt);
	_time+=dt;

	uint smoke_amount = _smoke_emission.animate(dt); 

	if(_time > 0.15f)
	{
		for(uint i=0; i<smoke_amount; i++)
		{
			Particle p(this->get_position() - this->get_directionN()*this->get_length()*0.9f,
					   vec4(1.0,0.6,0.3, -0.008)*utils::random_float(2.0, 4.0),
					   this->get_size()*0.75f,	
					   1.0f,
					   1.0f);

			p.set_speed(-this->get_directionN()*this->get_size()*6.0f + utils::random_vec3(vec3(-1.0), vec3(1.0))*this->get_size());

			effects::clouds().add_particle(p);
		}
	}
}


void TowerMissile::planting_reaction(const glm::vec3& position, const glm::vec3& normal, const glm::vec3& impact_speed)
{
	float intensity = _light_intensity * this->get_size() / 2.0f;

	vec3 plant_impact = position + vvv::y()*3.0f*intensity/_light_intensity;

	effects::add_homogenous_explosion(plant_impact,
								      intensity,
								      utils::random_float(_duration/20.0f, _duration*2.0f),
								      utils::random_vec3()*0.8f,
								      utils::random_vec3(),
								      utils::random_vec3(),
								      utils::random_vec3());

	EntityContext::global->energy_explosion(position, intensity*0.6f, intensity*0.15f);
}

bool TowerMissile::to_destroy()
{	
	return this->is_planted();
}

