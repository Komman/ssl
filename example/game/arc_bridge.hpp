#ifndef _ARC_BRIDGE_HPP_
#define _ARC_BRIDGE_HPP_

#include "../motor/physical_instances.hpp"
#include "../motor/planar_symmetry.hpp"
#include "../motor/stored_vao_object.hpp"
#include "../motor/hitbox_context.hpp"

#include "arc_door.hpp"

struct arcBridge
{
	glm::vec3 start;
	glm::vec2 direction2DN;
	glm::vec3 size;
};

class ArcBridge : public StoredVAObject<PhysicalInstances<2,
							glm::vec3,
							glm::vec3,
							glm::vec4,
							glm::vec4>>
{
public:
	ArcBridge(HitboxContext& context);

	void add_bridge(const arcBridge& bridge);
	void add_doored_bridge(const arcBridge& bridge, ArcDoor& doors);
	// Door on the start
	void add_oriented_doored_bridge(const arcBridge& bridge, ArcDoor& doors);

private:
	enum buffersRoles {RELATIVE_POS, COLORS, START_HIGHT, ORIENTATION2D_N_SIZEXY};
	using SymmetryType = PlanarSymmetry<glm::vec3>;

	void build_base();
	uint place_symmetrical(SymmetryType& sym, const glm::vec2& sideYZ, const glm::vec3& color);
	void create_hitbox(const arcBridge& bridge);

private:
	float _barrier_coeff;
	float _margin;
	float _underbridge;
	uint  _arc_discr;

	glm::vec3 _color;
};

#endif //_ARC_BRIDGE_HPP_
