#ifndef _CASTLE_ROLE_HPP_
#define _CASTLE_ROLE_HPP_

#include <memory>
#include <vector>

namespace CASTLE
{
	enum faceRole {FREE=0, CATHEDRAL, ATOWER, ROOF, ROOFUP, CIRCLEBUMP, FLOOR, WALLSTRUCT,
				   FACE_BUILD_ID_AMOUNT};

	enum doorType {RECTANGULAR=0, 
				   DOOR_TYPE_AMOUNT};
				   
	class BasicRole
	{
	public:
		BasicRole();

		std::vector<doorType> get_compatible_doors_type(const BasicRole& structure) const;

	public:
		virtual faceRole get_role() const=0;
		virtual bool is_roof_open() const=0;
		// Additionnal height to the building 
		virtual float additionnal_height() const;
		/*
			/!\ Any pair of castle structure must 
			have at least one door type in common.
		*/
		virtual bool door_type_available(doorType type) const;
	};



	class FreeRole : public BasicRole
	{
	public:
		FreeRole();

		faceRole get_role() const override;
		bool is_roof_open() const override;
	};

	class CathedralRole : public BasicRole
	{
	public:
		CathedralRole();

		faceRole get_role() const override;
		bool is_roof_open() const override;
		float additionnal_height() const override;
	};

	class AtowerRole : public BasicRole
	{
	public:
		AtowerRole();

		faceRole get_role() const override;
		bool is_roof_open() const override;
	};
	class RoofRole : public BasicRole
	{
	public:
		RoofRole();

		faceRole get_role() const override;
		bool is_roof_open() const override;
	};

	class CirclebumpRole : public BasicRole
	{
	public:
		CirclebumpRole();

		faceRole get_role() const override;
		bool is_roof_open() const override;
	};

	class RoofupRole : public BasicRole
	{
	public:
		RoofupRole();

		faceRole get_role() const override;
		bool is_roof_open() const override;
	};

	class FloorRole : public BasicRole
	{
	public:
		FloorRole();

		faceRole get_role() const override;
		bool is_roof_open() const override;
	};
};



#endif //_CASTLE_ROLE_HPP_
