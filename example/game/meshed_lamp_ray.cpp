#include "meshed_lamp_ray.hpp"

#include "../utils/geom.hpp"
#include "ingame.hpp"
#include "effects.hpp"

using namespace glm;
using namespace std;

static std::vector<uint> triple = {0,1,2};

MeshedLampRay::MeshedLampRay(const lampRay& ray)
	: MeshedRayLight(effects::meshedlight_luminor()),
	  _ray(ray),
	  _vertices(3)
{
	this->compute_vertices();
}

void MeshedLampRay::set_ray(const lampRay& ray)
{
	_ray = ray;
	this->compute_vertices();
}

void MeshedLampRay::compute_vertices()
{	
	_vertices[0] = {_ray.center, _ray.color};

	vec3 yaxisN = glm::cross(_ray.dirN, _ray.center - ingame::camera_position());
	yaxisN = geom::safe_normalize(yaxisN);

	_vertices[1] = {_ray.center + (_ray.dirN + yaxisN*_ray.opening)*_ray.range, vec3(0)};
	_vertices[2] = {_ray.center + (_ray.dirN - yaxisN*_ray.opening)*_ray.range, vec3(0)};
}

const std::vector<MeshedLampRay::rayVertex>& MeshedLampRay::get_vertices() const
{	
	return _vertices;
}

const std::vector<uint>& MeshedLampRay::get_elements() const
{
	return triple;
}

void MeshedLampRay::animate(float dt)
{

}


