#include "simple_wall.hpp"

#include "../utils/geom.hpp"
#include "castle_colors.hpp"

using namespace glm;


SimpleWall::SimpleWall(HitboxContext& hbx_context, VerticalShadower& vshadower)
	: StoredVAObject(hbx_context, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW),
	  _vshadower(vshadower),
	  _wall_color(castle_colors::SIMPLE_WALL()
),
	  _size(1.5f),
	  _niche_size(0.08),
	  _niche_count(30)
{
	this->build_relative_base();
}

void SimpleWall::add_wall(const simpleWall& wall)
{
	vec3 traj         = wall.dst - wall.start;
	vec3 trajN        = glm::normalize(traj);
	vec3 xdirN        = glm::cross(trajN, vvv::y());
	float traj_length = glm::length(traj);
	vec3 niche_length = traj / (2.0f*float(_niche_count));

	this->get_array_buffer<CENTERS_HEIGHT>().add_value(vec4(wall.start, wall.height));
	this->get_array_buffer<ORIENTATION_N_LENGTH>().add_value(vec4(trajN, traj_length));

	_vshadower.add_rectangle(wall.start, wall.dst, vec2(_size*3.0f, 0));
	
	auto base_hbx = std::make_unique<PaverHitbox>(paverInfo({
		.bottom  = wall.start - xdirN*_size/2.0f,
		.sized_x = xdirN*_size,
		.sized_y = vvv::y()*wall.height*(1.0f - 2.0f*_niche_size),
		.sized_z = traj
	}));
	auto mid_hbx = std::make_unique<PaverHitbox>(paverInfo({
		.bottom  = wall.start + vvv::y()*wall.height*(1.0f - 2.0f*_niche_size),
		.sized_x = xdirN*_size/2.0f,
		.sized_y = vvv::y()*wall.height*_niche_size,
		.sized_z = traj
	}));
	auto niches_hbx = std::make_unique<HitboxFusion>();
	for(uint i=0; i<_niche_count; i++)
	{
		niches_hbx->fusion_hitbox<PaverHitbox>(paverInfo({
			.bottom  = wall.start + vvv::y()*wall.height*(1.0f - 1.0f*_niche_size) + niche_length*float(2*i),
			.sized_x = xdirN*_size/2.0f,
			.sized_y = vvv::y()*wall.height*_niche_size,
			.sized_z = niche_length
		}));
	}

	auto whole_hbx = std::make_unique<HitboxFusion>();
	whole_hbx->fusion_hitbox(std::move(base_hbx));
	whole_hbx->fusion_hitbox(std::move(mid_hbx));
	whole_hbx->fusion_hitbox(std::move(niches_hbx));

	_hitboxes.push_back(std::move(whole_hbx));

	_hbx_context->add_static_hitbox(_hitboxes[_hitboxes.size()-1].get());
}

uint SimpleWall::add_base_vertex(const glm::vec3& pos, const glm::vec3& color)
{
	auto& relpos = this->get_array_buffer<RELATIVE_POS>();
	auto& colors = this->get_array_buffer<COLORS>();

	relpos.add_value(pos);
	colors.add_value(color);

	return relpos.size() - 1;
}

void SimpleWall::build_niches(const glm::vec3 starts[4], const glm::vec3 dsts[4], uint corner_left, uint middle)
{
	auto& relpos = this->get_array_buffer<RELATIVE_POS>();
	auto& elements = this->get_elements();

	uint  nichei     = relpos.size();

	float color_coeff[4] = {1, 0.7, 1, 1};

	for(uint i=0; i<2*_niche_count; i++)
	{
		float coeff = float(i)/float(2*_niche_count);

		for(uint c=0; c<4; c++)
		{
			this->add_base_vertex(glm::mix(starts[c], dsts[c], coeff), _wall_color*color_coeff[c]);
		}
	}

	for(uint i=0; i<_niche_count; i++)
	{
		elements.add_rectangle(
			nichei + 8*i + 0,
			nichei + 8*i + 1,
			nichei + 8*i + 2,
			nichei + 8*i + 3,
			true
		);

		elements.add_rectangle(
			nichei + 8*i + 4,
			nichei + 8*i + 5,
			nichei + 8*i + 6,
			nichei + 8*i + 7
		);

		elements.add_rectangle(
			nichei + 8*i + 1,
			nichei + 8*i + 5,
			nichei + 8*i + 6,
			nichei + 8*i + 2,
			true
		);

		elements.add_rectangle(
			nichei + 8*i + 0,
			nichei + 8*i + 3,
			nichei + 8*i + 7,
			nichei + 8*i + 4,
			true
		);

		elements.add_rectangle(
			nichei + 8*i + 2,
			nichei + 8*i + 3,
			nichei + 8*i + 7,
			nichei + 8*i + 6
		);

		if(i < _niche_count - 1)
		{
			elements.add_rectangle(
				nichei + 8*i + 4,
				nichei + 8*i + 5,
				nichei + 8*i + 9,
				nichei + 8*i + 8,
				true
			);
		}
	}

	elements.add_rectangle(
		nichei + 8*(_niche_count-1) + 4,
		nichei + 8*(_niche_count-1) + 5,
		middle,
		corner_left,
		true
	);
}


void SimpleWall::build_relative_base()
{
	auto& relpos = this->get_array_buffer<RELATIVE_POS>();
	auto& elements = this->get_elements();

	std::vector<uint> bot;
	std::vector<uint> top_L;
	std::vector<uint> top_m;
	
	bot.push_back(this->add_base_vertex(vec3(-_size/2.0f, 0, 0), _wall_color*0.6f));
	bot.push_back(this->add_base_vertex(vec3(-_size/2.0f, 0, 1), _wall_color*0.6f));
	bot.push_back(this->add_base_vertex(vec3(+_size/2.0f, 0, 1), _wall_color*0.6f));
	bot.push_back(this->add_base_vertex(vec3(+_size/2.0f, 0, 0), _wall_color*0.6f));

	uint size = relpos.size();

	for(uint i=0; i<size; i++)
	{
		top_L.push_back(this->add_base_vertex(relpos[i] + vvv::y()*(1.0f - _niche_size), _wall_color));
	}
	top_m.push_back(top_L[2]);
	top_m.push_back(top_L[3]);
	top_m.push_back(this->add_base_vertex(geom::middle(relpos[top_L[0]], relpos[top_L[3]]), _wall_color*0.7f));
	top_m.push_back(this->add_base_vertex(geom::middle(relpos[top_L[1]], relpos[top_L[2]]), _wall_color*0.7f));

	elements.add_rectangle(bot[0], bot[1], top_L[1], top_L[0]);
	elements.add_rectangle(bot[3], top_L[3], top_L[2], bot[2]);

	vec3 start_niche[4] = {
		vec3(-_size/2.0f, 1.0f - _niche_size, 0),
		vec3(0          , 1.0f - _niche_size, 0),
		vec3(0          , 1.0f             , 0),
		vec3(-_size/2.0f, 1.0f             , 0)
	};

	vec3 dst_niche[4];
	for(uint i=0; i<4; i++)
	{
		dst_niche[i] = start_niche[i];
		dst_niche[i].z = 1;
	}

	std::vector<uint> top_m2 = top_m;
	
	relpos.change_subvalue(top_L[2], relpos[top_L[2]] - vvv::y()*_niche_size);
	relpos.change_subvalue(top_L[3], relpos[top_L[3]] - vvv::y()*_niche_size);
	top_m2[2] = this->add_base_vertex(relpos[top_m[2]] - vvv::y()*_niche_size, _wall_color);
	top_m2[3] = this->add_base_vertex(relpos[top_m[3]] - vvv::y()*_niche_size, _wall_color);

	elements.add_rectangle(top_m2);
	elements.add_rectangle(top_m2[2], top_m2[3], top_m[3], top_m[2], true);

	this->build_niches(start_niche, dst_niche, top_L[1], top_m[3]);
}

