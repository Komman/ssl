#ifndef _SKY_HPP_
#define _SKY_HPP_

#include "../../ssl/ssl.hpp"

#include "../motor/post_processor.hpp"
#include "../motor/controlled_drawer.hpp"
#include "../motor/shared_ressources.hpp"

using namespace ssl;

class Sky
{
public:
	Sky(SharedRessources& shared_ressources,
		DrawerPackController& drawer_controller);

	// NOT NORMALIZED
	glm::vec3 get_sunlight_direction() const;

	void animate(float dt);
	void draw();

	void  set_aurore_intensity(float new_intensity);
	float get_aurore_intensity();

private:
	SharedRessources& _shared_ressources;

	Uniform<glm::vec4> _sky_back_color;
	Uniform<float>     _sky_stars_achievement;
	Uniform<float>     _sky_moon_turn;
	Uniform<glm::vec3> _moon1_direction;
	Uniform<glm::vec3> _moon2_direction;
	Uniform<glm::vec3> _moon3_direction;
	Uniform<float>     _radius1;
	Uniform<float>     _radius2;
	Uniform<float>     _radius3;
	Uniform<glm::vec4> _color1;
	Uniform<glm::vec4> _color2;
	Uniform<glm::vec4> _color3;
	Uniform<float>     _voronoi_distance;
	Uniform<float>     _aurore_dep;
	Uniform<float>     _aurore_intens;

	ShaderGenerator  _shader_gen;
	ControlledDrawer _drawer;

	float _self_time;
};


#endif //_SKY_HPP_
