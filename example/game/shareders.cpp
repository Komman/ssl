#include "shareders.hpp"

namespace shareders
{
	template<typename Type>
	void delnull(Type*& ptr)
	{
		if(ptr == NULL)
		{
			ssl::err("Try to call shareders::free() while it is already freed or not init");
		}

		delete ptr;
		ptr = NULL;
	}


	static ShaderGenerator* _gen   = NULL;
	static Drawer* _mesh_viewer    = NULL;
	static Drawer* _tower_bomb     = NULL;
	static Drawer* _brut_color     = NULL;
	static Drawer* _brut_ground    = NULL;
	static Drawer* _tower_drawer   = NULL;
	static Drawer* _mtower_drawer  = NULL;
	static Drawer* _wall_drawer    = NULL;
	static Drawer* _planar_builds  = NULL;
	static Drawer* _arc_doors      = NULL;
	static Drawer* _lantern_drawer = NULL;
	static Drawer* _castle_base    = NULL;
	static Drawer* _castle_inside  = NULL;
	static Drawer* _tower_cannons  = NULL;
	static Drawer* _text_renderer  = NULL;
	static Drawer* _counterforts   = NULL;
	// static Drawer* _ftower_drawer  = NULL;


	void init()
	{
		if(_gen != NULL)
		{
			ssl::err("Try to call shareders::init() while it is already initialized");
		}

		_gen = new ShaderGenerator("shaders/shareders.glsl");
		
		_mesh_viewer = new Drawer(*_gen, "mesh_viewer_vert", "mesh_viewer_frag");
		_mesh_viewer->depth(true);
		
		_tower_bomb = new Drawer(*_gen, "tower_bomb_vertex", "tower_bomb_fragment");
		_tower_bomb->depth(true);

		_brut_color = new Drawer(*_gen, "simple_vertex3", "simple_fragment");
		_brut_color->depth(true);

		_brut_ground = new Drawer(*_gen, "simple_vertex3", "ground_fragment", {"vertical_shadow_modifier", "all_lights_mesh_normal"});
		_brut_ground->depth(true);

		_tower_drawer = new Drawer(*_gen, "tower_vertex", "tower_fragment", {"all_lights_mesh_normal"});
		_tower_drawer->depth(true);

		_mtower_drawer = new Drawer(*_gen, "mtower_vertex", "mtower_fragment");
		_mtower_drawer->depth(true);

		_wall_drawer = new Drawer(*_gen, "wall_vertex", "wall_fragment");
		_wall_drawer->depth(true);

		_planar_builds = new Drawer(*_gen, "arc_bridge_vertex", "arc_bridge_fragment", {"all_lights_mesh_normal"});
		_planar_builds->depth(true);

		_counterforts = new Drawer(*_gen, "counterfort_vertex", "counterfort_fragment", {"all_lights_mesh_normal"});
		_counterforts->depth(true);

		_arc_doors = new Drawer(*_gen, "arc_door_vertex", "arc_door_fragment", {"all_lights_mesh_normal"});
		_arc_doors->depth(true);

		_lantern_drawer = new Drawer(*_gen, "lantern_vertex", "lantern_fragment");
		_lantern_drawer->depth(true);

		_castle_base = new Drawer(*_gen, "base_vertex", "base_fragment", {"vertical_shadow_modifier", "all_lights_mesh_normal"});
		_castle_base->depth(true);

		_castle_inside = new Drawer(*_gen, "base_vertex", "inside_fragment");
		_castle_inside->depth(true);

		_tower_cannons = new Drawer(*_gen, "tower_cannons_vertex", "tower_cannons_fragment");
		_tower_cannons->depth(true);

		_text_renderer = new Drawer(*_gen, "text_vertex", "text_fragment");
		// _text_renderer->blending(true);
   		// _text_renderer->set_blend_function(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
   		// _text_renderer->set_blend_equation(GL_FUNC_ADD);
   		
		// _ftower_drawer = new Drawer(*_gen, "fatower_bridge_vertex", "fatower_bridge_fragment", {"all_lights_mesh_normal"});
		// _ftower_drawer->depth(true);
	}

	void free()
	{
		delnull(_gen);
		delnull(_mesh_viewer);
		delnull(_tower_bomb);
		delnull(_brut_color);
		delnull(_brut_ground);
		delnull(_tower_drawer);
		delnull(_mtower_drawer);
		delnull(_wall_drawer);
		delnull(_counterforts);
		delnull(_planar_builds);
		delnull(_arc_doors);
		delnull(_lantern_drawer);
		delnull(_castle_base);
		delnull(_castle_inside);
		delnull(_tower_cannons);
		delnull(_text_renderer);
		// delnull(_ftower_drawer);
	}

	void try_to_initialize()
	{
		_mesh_viewer->try_to_initialize();
		_tower_bomb->try_to_initialize();
		_brut_color->try_to_initialize();
		_brut_ground->try_to_initialize();
		_tower_drawer->try_to_initialize();
		_mtower_drawer->try_to_initialize();
		_wall_drawer->try_to_initialize();
		_counterforts->try_to_initialize();
		_planar_builds->try_to_initialize();
		_arc_doors->try_to_initialize();
		_lantern_drawer->try_to_initialize();
		_castle_base->try_to_initialize();
		_castle_inside->try_to_initialize();
		_tower_cannons->try_to_initialize();
		_text_renderer->try_to_initialize();
	}

	Drawer& mesh_viewer()
	{
		return *_mesh_viewer;
	}

	Drawer& tower_bomb()
	{
		return *_tower_bomb;
	}

	Drawer& brut_color()
	{
		return *_brut_color;
	}

	Drawer& brut_ground()
	{
		return *_brut_ground;
	}

	Drawer& tower_drawer()
	{
		return *_tower_drawer;
	}

	Drawer& mtower_drawer()
	{
		return *_mtower_drawer;
	}

	Drawer& wall_drawer()
	{
		return *_wall_drawer;
	}

	Drawer& counterforts()
	{
		return *_counterforts;
	}

	Drawer& planar_builds()
	{
		return *_planar_builds;
	}

	Drawer& arc_doors()
	{
		return *_arc_doors;
	}

	Drawer& lantern_drawer()
	{
		return *_lantern_drawer;
	}

	Drawer& castle_base()
	{
		return *_castle_base;
	}

	Drawer& castle_inside()
	{
		return *_castle_inside;
	}

	Drawer& tower_cannons()
	{
		return *_tower_cannons;
	}

	Drawer& text_renderer()
	{
		return *_text_renderer;
	}

	// Drawer& fat_tower_drawer()
	// {
	// 	return *_ftower_drawer;
	// }

};



