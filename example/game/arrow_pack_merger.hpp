#ifndef _ARROW_PACK_MERGER_HPP_
#define _ARROW_PACK_MERGER_HPP_

#include "simple_arrow_trail_pack.hpp"
#include "tower_bombs.hpp"
#include "physical_context.hpp"

class ArrowPackMerger
{
public:
	ArrowPackMerger();

	void animate(const PhysicalContext& context, float dt);
	void draw(GameDrawer& game_drawer);

	void launch_simple_trail(std::unique_ptr<Arrow>&& start, const glm::vec3& directionN, float speed);
	void launch_tower_bomb(std::unique_ptr<Arrow>&& start, const glm::vec3& directionN, float speed);
	

	const SimpleArrowTrailPack& get_simple_trails() const;

private:
	SimpleArrowTrailPack _simple_trails;
	TowerBombs _tower_bombs;
};

#endif //_ARROW_PACK_MERGER_HPP_
