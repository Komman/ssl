#ifndef _LANTERNS_HPP_
#define _LANTERNS_HPP_

#include "../motor/stored_instances_buffers.hpp"
#include "../motor/stored_vao_object.hpp"
#include "../motor/static_lightor.hpp"

struct lanternDisposition
{
	glm::vec3 position;
	glm::vec3 directionN;
	glm::vec3 color;
	float     size;
};

class Lanterns : public  StoredVAObject<StoredInstancesBuffers<2,
							glm::vec3,
							glm::vec3,
							glm::vec4,
							glm::vec3,
							glm::vec3>>
{
public:
	Lanterns(StaticLigthor& slightor);

	void add_lantern(const lanternDisposition& lantern);
	void add_custom_lantern(const lanternDisposition& lantern, const ligthingInfo& light);

	std::vector<glm::vec3> lanterns_positions() const;

private:
	enum bufferRoles {RELATIVE_POS, COLORS, DEP_SIZE, LIGHT_COLOR, DIRECTION_N};

	// Returns the index
	uint add_point(const glm::vec3& coord, const glm::vec3 color);

	void build_lantern();
	void build_support(float support_coeff, float support_depth_coeff);
	float color_random();

private:
	StaticLigthor& _slightor;

	glm::vec3 _size;
	glm::vec3 _light_color;
	glm::vec3 _light_rel_pos;
};

#endif //_LANTERNS_HPP_
