#include "simple_arrow_pack.hpp"

#include "../utils/geom.hpp"
#include "ingame.hpp"

using namespace std;
using namespace glm;

/*
	SIMPLE ARROW PACK
*/

std::unique_ptr<Uniform<int>>  SimpleArrowPack::global_first_head_index    = NULL;
std::unique_ptr<Uniform<int>>  SimpleArrowPack::global_first_feather_index = NULL;
std::unique_ptr<Drawer>        SimpleArrowPack::drawer                     = NULL;

SimpleArrowPack::SimpleArrowPack()
	: ArrowPack<simpleArrowGeometry>(),
	  _indices(STATIC_DRAW),
	  _vertexs(STATIC_DRAW),
	  _colormasks(STATIC_DRAW),
	  _backpos(DYNAMIC_DRAW),
	  _directionsN(DYNAMIC_DRAW),
	  _color_shaft(DYNAMIC_DRAW),
	  _color_head(DYNAMIC_DRAW),
	  _geom_params(DYNAMIC_DRAW),
	  _vao({
	  	{&_vertexs, 0},
	  	{&_colormasks, 0},
	  	{&_backpos, 1},
	  	{&_directionsN, 1},
	  	{&_color_shaft, 1},
	  	{&_color_head, 1},
	  	{&_geom_params, 1},
	  }, &_indices),
	  _default_trail_config({{},{}}),
	  _trails_configs(),
	  _first_head_index(0),
	  _feather_inter(0.04,0.22)
{
	this->init_static_members();

	this->build_vertexs();
}

void SimpleArrowPack::init_static_members()
{
	if(global_first_head_index == NULL)
	{
		global_first_head_index = std::make_unique<Uniform<int>>("simple_arrow_pack_first_head_index", 0);
	}
	if(global_first_feather_index == NULL)
	{
		global_first_feather_index = std::make_unique<Uniform<int>>("simple_arrow_pack_first_feather_index", 0);
	}
	if(drawer == NULL)
	{
		drawer = std::make_unique<Drawer>(*get_arrow_shader_generator(), string("simple_arrow_vertex"), string("simple_arrow_fragment"), std::vector<string>({"all_lights_smooth"}));
		drawer->depth(true);
	}
}

void SimpleArrowPack::build_vertexs()
{
	this->build_shaft();
	
	_first_feather_index = _vertexs.size();
	
	this->build_feathers();

	_first_head_index = _vertexs.size();
	
	this->build_head();
}

const trailConfig& SimpleArrowPack::get_fether_config(unsigned int index) const
{
	if(index >= _trails_configs.size())
	{
		err("SimpleArrowPack::get_fether_config(): index out of bound: " + to_string(index) + "/"
			+ to_string(_trails_configs.size()));
	}

	return _trails_configs[index];
}

void SimpleArrowPack::build_shaft()
{
	int circle_points_count = 6;

	for(int i=0; i<circle_points_count; i++)
	{
		unsigned int fi = _vertexs.size();
		_vertexs.add_value(geom::circle_point_plan(
				vvv::x()*0.0f,
				vvv::y(),
				vvv::z(),
				2.0f*M_PI*(float(i)/float(circle_points_count)),
				1.0f
			));
		_colormasks.add_value(vec3(1.0f)*utils::random_float(0.6f, 1.0f));

		unsigned int bi = _vertexs.size();
		_vertexs.add_value(geom::circle_point_plan(
				vvv::x()*1.0f,
				vvv::y(),
				vvv::z(),
				2.0f*M_PI*(float(i)/float(circle_points_count)),
				1.0f
			));
		_colormasks.add_value(vec3(1.0f)*utils::random_float(0.6f, 1.0f));

		if(i>=1)
		{
			_indices.add_rectangle(fi-2, fi, bi, bi-2);
		}
	}
	unsigned int fi = _vertexs.size()-2;
	unsigned int bi = _vertexs.size()-1;
	_indices.add_rectangle(fi, 0, 1, bi);

	for(int i=2; i<circle_points_count; i++)
	{
		_indices.add_triangle(2*i-2, 0, 2*i);
		_indices.add_triangle(1, 2*i-1, 2*i+1);
	}
}

void SimpleArrowPack::build_feathers()
{
	unsigned int feather_count = 3;
	float feather_radius = 2.8f;
	float feather_dec = _feather_inter.x/1.5f;

	vec3 feather_color_back_down  = vec3(0.0, 0.0, 1.0);
	vec3 feather_color_back_up    = vec3(0.0, 1.0, 1.0);
	vec3 feather_color_front_down = vec3(1.0, 0.0, 1.0);
	vec3 feather_color_front_up   = vec3(1.0, 1.0, 1.0);

	unsigned int b1 = _vertexs.size();
	unsigned int b2 = b1 + 1;
	_vertexs.add_value(vvv::x()*_feather_inter.x);
	_colormasks.add_value(feather_color_back_down);
	_vertexs.add_value(vvv::x()*_feather_inter.y);
	_colormasks.add_value(feather_color_front_down);
	_default_trail_config.base.push_back(_vertexs[b1]);

	for(int i=0; i<(int)feather_count; i++)
	{
		unsigned int fi = _vertexs.size();
		_vertexs.add_value(geom::circle_point_plan(
				vvv::x()*_feather_inter.x - feather_dec,
				vvv::y(),
				vvv::z(),
				2.0f*M_PI*(float(i)/float(feather_count)),
				feather_radius
			));
		_colormasks.add_value(feather_color_back_up);
		_default_trail_config.base.push_back(_vertexs[fi]);

		unsigned int bi = _vertexs.size();
		_vertexs.add_value(geom::circle_point_plan(
				vvv::x()*_feather_inter.y - feather_dec,
				vvv::y(),
				vvv::z(),
				2.0f*M_PI*(float(i)/float(feather_count)),
				feather_radius/1.2f
			));
		_colormasks.add_value(feather_color_front_up);


		_indices.add_rectangle(fi, bi, b2, b1);
		_indices.add_rectangle(fi, b1, b2, bi);
		_default_trail_config.plan.push_back({0,i+1});
	}
}

void SimpleArrowPack::build_head()
{
	unsigned int first = _vertexs.size();
	/*
		1 -- \
		|\	  ------\
		3/4          0    
		|/    ------/
		2 -- / 

	*/
	_vertexs.add_value(vvv::x());
	_colormasks.add_value(vec3(1.0f));

	_vertexs.add_value( vvv::z()*2.6f);
	_colormasks.add_value(vec3(1.0f)/1.5f);

	_vertexs.add_value(-vvv::z()*2.6f);
	_colormasks.add_value(vec3(1.0f)/1.5f);

	_vertexs.add_value(vvv::y()*1.2f);
	_colormasks.add_value(vec3(1.0f));

	_vertexs.add_value(-vvv::y()*1.2f);
	_colormasks.add_value(vec3(1.0f));

	_indices.add_triangle(first+0, first+1, first+4);
	_indices.add_triangle(first+0, first+4, first+2);
	_indices.add_triangle(first+0, first+3, first+1);
	_indices.add_triangle(first+0, first+2, first+3);

	_indices.add_rectangle(first+1, first+3, first+2, first+4);
}

trailConfig SimpleArrowPack::get_arrow_trail_config(const Arrow& arrow) const
{
	auto new_trail_config = _default_trail_config;
	for(unsigned int i=1; i<new_trail_config.base.size(); i++)
	{
		vec2 lateral(new_trail_config.base[i].y, new_trail_config.base[i].z);
		lateral *= arrow.get_size()/2.0f;
		float front = new_trail_config.base[i].x*arrow.get_length();

		new_trail_config.base[i] = vec3(front, lateral.x, lateral.y);
	}

	return new_trail_config;
}

void SimpleArrowPack::draw(GameDrawer& game_drawer)
{
	// if(this->size() > 0)
	// {
		(*global_first_head_index)    = _first_head_index;	
		(*global_first_feather_index) = _first_feather_index;	
		
		// game_drawer.draw_bloomed_MS(*drawer, _indices, _vertexs, _colormasks, _backpos, _directionsN, _color_shaft, _color_head, _geom_params);
		game_drawer.draw_MS(*drawer, _vao);
		// game_drawer.draw_blend(*drawer, _indices, _vertexs, _colormasks, _backpos, _directionsN, _color_shaft, _color_head, _geom_params);
	// }
}

void SimpleArrowPack::pre_add_arrow(const Arrow& arrow,
							   	    const simpleArrowGeometry& look)
{
	this->check_sizes();

	vec3 default_value = vvv::x();

	_backpos.add_value(default_value);
	_directionsN.add_value(default_value);
	_color_shaft.add_value(default_value);
	_color_head.add_value(default_value);
	_geom_params.add_value(default_value);
	_trails_configs.push_back({{}, {}});

	this->update_arrow_GPU(this->size(), arrow, look);
}

void SimpleArrowPack::update_arrow_GPU(unsigned int index,
									   const Arrow& arrow,
							  		   const simpleArrowGeometry& look)
{
	if(index >= _backpos.size())
	{	
		err("SimpleArrowPack::post_set_arrow(): index out of bound: " + to_string(index) + "/" + to_string(this->size()));
	}

	if(_backpos[index] != arrow.get_position())
	{
		_backpos.change_subvalue(index, arrow.get_position() - arrow.get_directionN()*arrow.get_length());
	}

	if(_directionsN[index] != arrow.get_directionN())
	{
		_directionsN.change_subvalue(index, arrow.get_directionN());
	}

	if(_color_shaft[index] != look.shaft_color)
	{
		_color_shaft.change_subvalue(index, look.shaft_color);
	}

	if(_color_head[index] != look.head_color)
	{
		_color_head.change_subvalue(index, look.head_color);
	}

	if(    _geom_params[index].x != arrow.get_length()
		|| _geom_params[index].y != arrow.get_size()
		|| _geom_params[index].z != look.head_coeff)
	{
		_geom_params.change_subvalue(index, {arrow.get_length(), arrow.get_size(), look.head_coeff});
	}

	_trails_configs[index] = this->get_arrow_trail_config(arrow);
}


void SimpleArrowPack::post_set_arrow(unsigned int index,
									 const Arrow& arrow,
							 		 const simpleArrowGeometry& look)
{
	this->update_arrow_GPU(index, arrow, look);
}



void SimpleArrowPack::pre_pop_last_arrow()
{
	#ifdef SSL_DEBUG
	if(this->size() == 0)
	{
		err("SimpleArrowPack::pre_pop_last_arrow(): try to pop while no there are no arrows");
	}
	#endif

	this->check_sizes();

	_backpos.pop_value();
	_directionsN.pop_value();
	_color_shaft.pop_value();
	_color_head.pop_value();
	_geom_params.pop_value();
	_trails_configs.pop_back();
}

void SimpleArrowPack::pre_clear()
{
	_backpos.change_value({});
	_directionsN.change_value({});
	_color_shaft.change_value({});
	_color_head.change_value({});
	_geom_params.change_value({});

	_backpos.instant_update_GPU();
	_directionsN.instant_update_GPU();
	_color_shaft.instant_update_GPU();
	_color_head.instant_update_GPU();
	_geom_params.instant_update_GPU();

	_trails_configs.clear();
}

void SimpleArrowPack::check_sizes() const
{
	#ifdef SSL_DEBUG
	if(    this->size() != _backpos.size()
		|| this->size() != _directionsN.size()
		|| this->size() != _color_shaft.size()
		|| this->size() != _color_head.size()
		|| this->size() != _geom_params.size()
		|| this->size() != _trails_configs.size())
	{
		err("SimpleArrowPack::check_sizes(): wrong size");
	}
	#endif
}



