#include "castle_inside_face.hpp"

#include "castle_base.hpp"
#include "castle_colors.hpp"

#include "../motor/meshbuild.hpp"
#include "../motor/building.hpp"
#include "../motor/holed_wall_hitbox.hpp"
#include "../motor/geo_cylinder_hitbox.hpp"
#include "../motor/wall_hitbox.hpp"
#include "../motor/fan_hitbox.hpp"
#include "../motor/hanoi_hitbox.hpp"

using namespace ssl;
using namespace glm;
using namespace std;

namespace CASTLE
{
	InsideFace::InsideFace(const std::vector<insideDoor>& doors_to_set, FaceStructure* outside)
		: _outside(outside),
		  _doors_to_set(doors_to_set)
	{
		
	}	

	void InsideFace::build()
	{	
		this->build(_doors_to_set);
	}

	Base* InsideFace::get_base() const
	{
		return _outside->get_base();
	}

	ssl::ArrayBuffer<glm::vec3>& InsideFace::mesh()
	{
		return this->get_base()->get_inside_mesh().get_array_buffer<0>();
	}
	ssl::ArrayBuffer<glm::vec3>& InsideFace::colors()
	{
		return this->get_base()->get_inside_mesh().get_array_buffer<1>();
	}
	ssl::ElementBuffer& InsideFace::elements()
	{
		return this->get_base()->get_inside_mesh().get_elements();
	}

	const glm::vec3& InsideFace::inside_color() const
	{
		return _outside->inside_color();
	}

	void InsideFace::complete_color_buffer(const glm::vec3& color, float randomness)
	{
		auto& inside = this->get_base()->get_inside_mesh();
		while(inside.get_array_buffer<1>().size() < inside.get_array_buffer<0>().size())
		{
			inside.get_array_buffer<1>().add_value(color*utils::random_float(1.0f-randomness, 1.0f));
		}
	}

	std::vector<indexedInWall> InsideFace::wall_inside_base_vertices(const std::vector<insideDoor>& doors_to_set, float height, float wall_size)
	{
		ssl::ArrayBuffer<glm::vec3>& pos = this->mesh();
		std::vector<indexedInWall> ret;
		uint starti = pos.size();

		for(uint i=0; i<doors_to_set.size(); i++)
		{
			const auto& w = doors_to_set[i];

			uint u = (i+doors_to_set.size()-1)%doors_to_set.size();

			glm::vec3 v1 = geom::yfixed(_outside->down_position(doors_to_set[u].pos.edge)[0], height);
			glm::vec3 v2 = geom::yfixed(_outside->down_position(w.pos.edge)[0], height);
			glm::vec3 v3 = geom::yfixed(_outside->down_position(w.pos.edge)[1], height);

			pos.add_value(geom::yfixed(v2, height)-geom::trigo_bissectriceN(v1, v2, v3, vvv::y())*wall_size);
			ret.push_back({
				.door  = w,
				.edgei = {starti+i, uint(starti+((i+1)%doors_to_set.size()))}
			});
		}
		this->complete_color_buffer(this->inside_color());

		return ret;
	}

	FaceStructure* InsideFace::outside()
	{
		return _outside;
	}

	doorFullInfo InsideFace::compute_infos(const insideDoor& door)
	{
		if(!door.door)
		{
			mot::err("InsideFace::compute_infos(): not a doored door");
		}

		float dh = door.pos.altitude - this->floor_h();

		glm::vec3 v1 = geom::yfixed(_outside->down_position(door.pos.edge)[0], this->floor_h());
		glm::vec3 v2 = geom::yfixed(_outside->down_position(door.pos.edge)[1], this->floor_h());

		glm::vec3 dirlatN = geom::safe_normalize(v2- v1);

		glm::vec3 mid = geom::middle(v1, v2) + vvv::y()*dh;
		glm::vec3 midtop = mid + vvv::y()*door.pos.height;

		return doorFullInfo{
			.door = door.pos,
			.edge = {v1, v2},
			.bottom = {mid - dirlatN*door.pos.width/2.0f, mid + dirlatN*door.pos.width/2.0f},
			.top = {midtop - dirlatN*door.pos.width/2.0f, midtop + dirlatN*door.pos.width/2.0f},
			.dirdoorN = geom::trigo_normal(dirlatN),
			.centerbot = mid
		};
	}

	float InsideFace::height() const
	{
		return _outside->height();
	}

	float InsideFace::wall_radius() const
	{
		return _outside->wall_radius();
	}

	float InsideFace::floor_h() const
	{
		return _outside->floor_h();
	}

	void InsideFace::add_hitbox(std::unique_ptr<BasicHitbox>&& hbx)
	{
		_outside->add_hitbox(std::move(hbx));
	}

	void InsideFace::check_buffer_sizes()
	{
		if(this->mesh().size() != this->colors().size())
		{
			mot::err("InsideFace::check_buffer_sizes(): buffers not aligned");
		}
	}
	
	indexedInWall InsideFace::build_inside_wall_facade(const indexedInWall& wall, float height, const glm::vec3& color)
	{
		ArrayBuffer<glm::vec3>& inpos  = this->mesh();
		
		vec3 down1 = geom::yfixed(_outside->down_position(wall.door.pos.edge)[0], inpos[wall.edgei.first ].y);
		vec3 down2 = geom::yfixed(_outside->down_position(wall.door.pos.edge)[1], inpos[wall.edgei.second].y);

		uint i1 = inpos.add_value(inpos[wall.edgei.first] + vvv::y()*height);
		uint i2 = inpos.add_value(inpos[wall.edgei.second] + vvv::y()*height);
		this->complete_color_buffer(color);

		this->elements().add_rectangle(wall.edgei.first, wall.edgei.second, i2, i1, false);

		transPaverInfo paver = {
			.square_base = {
				inpos[wall.edgei.first],
				inpos[wall.edgei.second],
				down2,
				down1
			},
			.sized_direction = vvv::y()*height
		};
		this->add_hitbox(std::make_unique<PaverHitbox>(paver));


		return indexedInWall{.door = wall.door, .edgei = edgeIndex{i1, i2}};
	}

	indexedInWall InsideFace::build_inside_cut_wall_facade(const indexedInWall& wall, float cut_center_coeff, float cut_width, float height, const glm::vec3& color, bool fill_cutfloor)
	{
		ArrayBuffer<glm::vec3>& inpos  = this->mesh();
		ElementBuffer& elmts     = this->elements();

		vec3 down1 = geom::yfixed(_outside->down_position(wall.door.pos.edge)[0], inpos[wall.edgei.first ].y);
		vec3 down2 = geom::yfixed(_outside->down_position(wall.door.pos.edge)[1], inpos[wall.edgei.second].y);

		vec3  dirN      = down2 - down1;
		float wallength = glm::length(dirN);
		dirN = dirN/wallength;

		
		/*
			           OUTSIDE
				j1 --- j2    j3 --- j4
			           |     |
			   i1 ---- i2    i3 ------------ i4
			            INSIDE
		*/

		uint j1 = inpos.add_value(down1 + dirN*(wallength*0.0f             + cut_width*0.0f));
		uint j2 = inpos.add_value(down1 + dirN*(wallength*cut_center_coeff - cut_width*0.5f));
		uint j3 = inpos.add_value(down1 + dirN*(wallength*cut_center_coeff + cut_width*0.5f));
		uint j4 = inpos.add_value(down1 + dirN*(wallength*1.0f             + cut_width*0.0f));
		
		// uint j1up = inpos.add_value(inpos[j1] + vvv::y()*height);
		uint j2up = inpos.add_value(inpos[j2] + vvv::y()*height);
		uint j3up = inpos.add_value(inpos[j3] + vvv::y()*height);
		// uint j4up = inpos.add_value(inpos[j4] + vvv::y()*height);
		
		uint i1 = wall.edgei.first;
		uint i4 = wall.edgei.second;
		uint i2 = inpos.add_value(geom::trigo_crossing_problem(inpos[j1], inpos[j4], inpos[i1], inpos[i4], inpos[j2]));
		uint i3 = inpos.add_value(geom::trigo_crossing_problem(inpos[j1], inpos[j4], inpos[i1], inpos[i4], inpos[j3]));

		uint i1up = inpos.add_value(inpos[i1] + vvv::y()*height);
		uint i2up = inpos.add_value(inpos[i2] + vvv::y()*height);
		uint i3up = inpos.add_value(inpos[i3] + vvv::y()*height);
		uint i4up = inpos.add_value(inpos[i4] + vvv::y()*height);
		
		this->complete_color_buffer(color);

		elmts.add_rectangle(i1, i2, i2up, i1up, false);
		elmts.add_rectangle(i4, i3, i3up, i4up, true);

		elmts.add_rectangle(j2, i2, i2up, j2up, true);
		elmts.add_rectangle(j3, i3, i3up, j3up, false);

		elmts.add_rectangle(j2up, j3up, i3up, i2up, false);
		
		if(fill_cutfloor)
		{
			elmts.add_rectangle(j2, j3, i3, i2, true);
		}
	
		transPaverInfo paver1 = {
			.square_base = {
				inpos[j1],
				inpos[j2],
				inpos[i2],
				inpos[i1]
			},
			.sized_direction = vvv::y()*height
		};
		transPaverInfo paver2 = {
			.square_base = {
				inpos[j3],
				inpos[j4],
				inpos[i4],
				inpos[i3]
			},
			.sized_direction = vvv::y()*height
		};
		this->add_hitbox(std::make_unique<PaverHitbox>(paver1));
		this->add_hitbox(std::make_unique<PaverHitbox>(paver2));

		return indexedInWall{.door = wall.door, .edgei = edgeIndex{i1up, i4up}};
	}

	std::vector<indexedInWall> InsideFace::build_inside_walls(const std::vector<indexedInWall>& walls, float height, const glm::vec3& color)
	{
		std::vector<indexedInWall> ret;
		this->check_buffer_sizes();

		for(uint i=0; i<walls.size(); i++)
		{
			indexedInWall w = walls[i];

			if(w.door.door)
			{
				float dh = w.door.pos.altitude - this->floor_h();
				if(dh <= 0.0001f)
				{
					w = this->build_inside_cut_wall_facade(w, w.door.pos.position, w.door.pos.width, w.door.pos.height, color, false);
					w = this->build_inside_wall_facade(w, height-w.door.pos.height, color);
				}
				else
				{
					if(w.door.pos.altitude + w.door.pos.height < this->height())
					{
						// _outside->put_floor(dh, castle_colors::FLOOR());
						// _outside->put_roof(dh, castle_colors::FLOOR()*0.4f);

						w = this->build_inside_wall_facade(w, dh, color);
						w = this->build_inside_cut_wall_facade(w, w.door.pos.position, w.door.pos.width, w.door.pos.height, color, true);
						w = this->build_inside_wall_facade(w, height-dh-w.door.pos.height, color);
					}
					else
					{
						w = this->build_inside_wall_facade(w, height, color);
					}
				}	
			}
			else
			{
				w = this->build_inside_wall_facade(w, height, color);
			}
			ret.push_back(w);
		}

		_outside->put_floor(0.0f, castle_colors::FLOOR());

		return ret;
	}

	walledInside InsideFace::build_walls(const std::vector<insideDoor>& doors_to_set, float h)
	{
		walledInside ret;

		std::vector<indexedInWall> insidewalls = this->wall_inside_base_vertices(doors_to_set, this->floor_h(), this->wall_radius());
		for(auto& w : insidewalls)
		{
			ret.bot.push_back(w.edgei.first);
		}

		insidewalls = this->build_inside_walls(insidewalls, h, this->inside_color());
		for(auto& w : insidewalls)
		{
			ret.top.push_back(w.edgei.first);
		}

		return ret;
	}

	void InsideFace::build(const std::vector<insideDoor>& doors_to_set)
	{	
		this->build_walls(doors_to_set, this->height()-this->floor_h()-_outside->safe_height());
	}
};
