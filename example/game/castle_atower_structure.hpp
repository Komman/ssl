#ifndef _CASTLE_ATOWER_STRUCTURE_HPP_
#define _CASTLE_ATOWER_STRUCTURE_HPP_

#include "castle_face_structure.hpp"

namespace CASTLE
{
	class AtowerStructure : public FaceStructure
	{
	public:
		AtowerStructure(const baseBuiling& base, uint face);
		
	protected:
		void build_wall_deco(const std::vector<doorWall>& walls_to_build) override;
		void build_face(const std::vector<doorWall>& walls_to_build) override;
	
	private:
		/*
		            highex------highin
		                  |      |
		                  |      |
		                  |      |
		            highside----hightop
		                         |
		                         |
		    ATOWER OUSIDE        |   ATOWER INSIDE
		                        ...
		                         |
		                         |
		                         |
		                         |
		                 top----back
		                  |
		                  |
		    input ------ bot    backbot
		*/
		struct subATowerIndices
		{
			std::vector<uint> nextop;
		};

		/*
			So total delargement is (floordelargement + barrier_sizes.x*barrier_sizes.z)
			The following conditions are required:
			- (height < barrier_sizes.y)
			- (floordelargement < barrier_sizes.x)

			barrier_sizes.x :width
			barrier_sizes.y :heigh
			barrier_sizes.z :coefficient barrier_sizes.z on the draft
		*/
		subATowerIndices build_subatower(const subATowerIndices& prevtop, float floordelargement, float height);
		std::vector<glm::vec2> computes_delargement_and_heights(float final_delargement);
		void put_subatower_door(const partialCycle<uint>& bot, const partialCycle<uint>& top);
		std::vector<uint> select_doors_edge(const std::vector<glm::vec3>& cycle, float atowerheight);
		void put_subatower_pilers(const partialCycle<uint>& bot, const partialCycle<uint>& top);

	private:
		glm::vec2 _barriersize;
		glm::vec2 _rebortsize;
		glm::vec2 _firstbarsize;

		float _groundthick;
		float _wallssize;
		float _minishad;
		float _doorw;
		float _doorh;
		float _undetoproof_h;
		float _pilers_size;
		float _roofheight;
	};
};


#endif //_CASTLE_ATOWER_STRUCTURE_HPP_
