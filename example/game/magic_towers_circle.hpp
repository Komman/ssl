#ifndef _MAGIC_TOWERS_CIRCLE_HPP_
#define _MAGIC_TOWERS_CIRCLE_HPP_

#include "magic_tower.hpp"
#include "arc_bridge.hpp"
#include "arc_door.hpp"

struct magicTowerCircle
{
	glm::vec3 position;
	uint      tower_count;
	float     radius;
	float     towers_size;
	glm::vec2 minmax_height;
};

class MagicTowersCircle : public CastleBuildingPlacer<magicTowerCircle>
{
public:
	MagicTowersCircle(MagicTower* towers,
					  ArcBridge*  bridges,
					  ArcDoor*    doors);

	void add_tower_circle(const magicTowerCircle& mcircle, bool shadow = false);
	void add_tower_circle(const glm::vec3& position,
		 				  uint  tower_count,
		 				  float radius,
		 				  float towers_size,
		 				  const glm::vec2& minmax_height,
		 				  bool shadow = false);

protected:
	void place_building_only(const magicTowerCircle& builing, float h) override;
	magicTowerCircle generate_building(const genParam& g) override;
	planarPlace  get_place(const magicTowerCircle& g) override;

private:
	glm::vec3 _doors_walls_color;

	MagicTower* _towers;
	ArcBridge*  _bridges;
	ArcDoor*    _doors;
};

#endif //_MAGIC_TOWERS_CIRCLE_HPP_
