#ifndef _ARCHER_HPP_
#define _ARCHER_HPP_

#include "../motor/shared_ressources.hpp"
#include "../motor/primary_hitbox.hpp"
#include "../motor/player.hpp"

class Archer : public Player
{
public:
	Archer(const glm::vec3&   start,
		   float speed,
		   SharedRessources& shared_ressources);

	void draw(GameDrawer& game_drawer);
	void frame_update(float dt);

	BasicHitbox& get_hitbox();

	// For procedural generation parameter
	const float MAX_SIZE;
	static constexpr float MIN_JUMP_HEIGHT = 1.0f;

protected:
	void lanch_simple_arrow(const glm::vec3& direction, float speed);
	void lanch_bomb(const glm::vec3& direction, float speed);

private:
	SharedRessources& _shared_ressources;
	SphereHitbox _hitbox;

	bool _loading;
};

#endif //_ARCHER_HPP_
