#ifndef _HOUSES_HPP_
#define _HOUSES_HPP_

#include "../../ssl/ssl.hpp"

using namespace ssl;

class Houses : public ssl::ElementDrawableObject
{
public:
	struct doorGeom
	{
		float height; 
		float width;
		float extract;
	};
	struct roofGeom
	{
		float height; 
		float overoof;
		float width;
	};
	struct geomParams
	{
		glm::vec3 size;
		roofGeom  roof;
		doorGeom  door;
	};
public:
	Houses(const geomParams& params, const std::vector<glm::vec3>& dep);

	const std::vector<const BasicBuffer*>& get_buffers()  const;
	const ElementBuffer&                   get_elements() const;

protected:

	unsigned int add_vertex(const glm::vec3& position, const glm::vec2& infos);
	void build_base();
	void build_roof();
	void build_door();

private:
	geomParams _geom;

	ArrayBuffer<glm::vec3> _pos;
	// x: shadower, y: identifior
	// (0: base, 1: roof, -1: door_extr, -2: door)
	ArrayBuffer<glm::vec2> _infos; 
	ArrayBuffer<glm::vec3> _deps;

	ElementBuffer                   _elements;
	std::vector<const BasicBuffer*> _buffers;
};

#endif //_HOUSES_HPP_
