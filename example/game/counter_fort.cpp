#include "counter_fort.hpp"

#include "../motor/building.hpp"
#include "../motor/meshbuild.hpp"
#include "../motor/square_pyramid_hitbox.hpp"
#include "../motor/paver_hitbox.hpp"
#include "../motor/planar_symmetry.hpp"

using namespace std;
using namespace glm;

static float bluff = 0.8;
static float unpil = 0.8;
static float untop = 0.6;

CounterFort::CounterFort(HitboxContext& contex, VerticalShadower* vshadower, const counterFort& geometry, const glm::vec3& color)
	: StoredVAObject(contex, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW),
	_geometry(geometry),
	_color(color),
	_normal_hitbox(),
	_vshadower(vshadower)
{
	this->build_base();
}

const counterFort& CounterFort::get_geometry() const
{
	return _geometry;
}

void CounterFort::add_instance_AO(const planarYPlace& place)
{
	vec3 topiler = place.center + geom::unprojplan(place.dirN)*place.size.z*glm::length(geom::projplan(_geometry.bot_center - _geometry.endgirder));

	_vshadower->add_circle(topiler, 0.85f*std::max(place.size.x*_geometry.size, place.size.z*_geometry.size));
}

std::unique_ptr<BasicHitbox> CounterFort::build_hitbox(const planarYPlace& place) const
{
	std::unique_ptr<HitboxFusion> fhbx = std::make_unique<HitboxFusion>();

	if(place.size.x != place.size.z)
	{
		mot::err("CounterFort::build_hitbox(): can only homeotate hitbox with deux coeffs for xz dimensions");
	}

	vec3 pr = this->underbluff_pos();

	auto hbx = _normal_hitbox.copy();
	fhbx->fusion_hitbox(std::move(hbx));
	
	fhbx->fusion_hitbox<PaverHitbox>(ycentredPaverInfo{
		.bottomy = pr,
		.sized_x = vvv::x()*_geometry.size,
		.sized_y = -vvv::y()*(place.size.y/place.size.x+_geometry.height_startgirder-_geometry.size/2.0f),
		.sized_z = vvv::z()*_geometry.size
	});

	fhbx->homothety(place.size.x);
	fhbx->tp(place.center);
	vec3 dirN3D = geom::unprojplan(place.dirN);
	fhbx->rotate(glm::mat3(glm::cross(vvv::y(), dirN3D), vvv::y(), dirN3D));


	return fhbx;
}

glm::vec3 CounterFort::underbluff_pos() const
{
	return geom::yfixed(_geometry.bot_center, _geometry.height_startgirder - _geometry.size/2.0f);
}

void CounterFort::build_base()
{
	glm::vec3 bot_center         = _geometry.bot_center;
	glm::vec3 endgirder          = _geometry.endgirder;
	glm::vec3 color              = _color;
	float     size               = _geometry.size;
	float     height_startgirder = _geometry.height_startgirder;
	
	ArrayBuffer<glm::vec3>& basebuffer  = this->get_array_buffer<RELATIVE_POS>();
	ArrayBuffer<glm::vec3>& colorbuffer = this->get_array_buffer<COLOR>();
	ElementBuffer&          elements    = this->get_elements();

	if(glm::length(geom::projplan(endgirder - bot_center)) < 0.001)
	{
		mot::err("CounterFort::build_base(): bot_center and endgirder are at the same planproj");
	}
	float longh = height_startgirder - bot_center.y; 
	vec2 sizeslope(size*unpil, size*2.0f);
	std::vector<vec2> coslopiler_bluffs = {
		vec2(size*1.0f , 0),
		vec2(size*1.0f , longh - size/2.0f),
		vec2(size*bluff, longh - size/2.0f),
		vec2(size*bluff, longh - size/4.0f),
		vec2(size*1.0f , longh - size/4.0f),
		vec2(size*1.0f , longh),
		vec2(sizeslope.x, longh),
		vec2(sizeslope.x, longh + sizeslope.y),
		vec2(size*untop, longh + sizeslope.y),
		vec2(size*untop, longh + size*4.0f)
	};
	float ytoppic = coslopiler_bluffs[coslopiler_bluffs.size()-1].y + size;

	vec3 dirgirderN = endgirder - bot_center;
	vec3 dirzN = geom::unprojplan(glm::normalize(geom::projplan(dirgirderN)));
	dirgirderN = glm::normalize(dirgirderN);
	vec3 dirxN = glm::cross(vvv::y(), dirzN);
	
	// PLACING PILER
	uint first = basebuffer.size();
	building::hanoi::squared::sided(basebuffer, elements, coslopiler_bluffs, bot_center, spaceBase{
		.dirx = dirxN,
		.diry = vvv::y(),
		.dirz = dirzN
	});
	uint last = basebuffer.size() - 1;
	meshbuild::fan_cycle(basebuffer, elements, pair<uint, uint>{last-3, last}, bot_center + vvv::y()*ytoppic, true);

	for(uint i=0; i<coslopiler_bluffs.size()-1; ++i)
	{
		if(i != 0)
		{
			_normal_hitbox.fusion_hitbox<PaverHitbox>(ycentredPaverInfo{
				.bottomy = bot_center + vvv::y()*coslopiler_bluffs[i].y,
				.sized_x = dirxN*coslopiler_bluffs[i].x,
				.sized_y = vvv::y()*(coslopiler_bluffs[i+1].y - coslopiler_bluffs[i].y),
				.sized_z = dirzN*coslopiler_bluffs[i].x
			});
		}
	}
	_normal_hitbox.fusion_hitbox<SquarePyramidHitbox>(alignedSquarePyramidShape{
		.base_center = bot_center + vvv::y()*coslopiler_bluffs[coslopiler_bluffs.size()-1].y,
		.dir_base_XN = dirxN,
		.dir_base_YN = dirzN,
		.base_size_XY = vec2(coslopiler_bluffs[coslopiler_bluffs.size()-1].x),
		.top = bot_center + vvv::y()*ytoppic
	});

	while(colorbuffer.size() < basebuffer.size())
	{
		colorbuffer.add_value(color);
	}
	float bluffcolordark1 = 0.7f;
	float bluffcolordark2 = 0.8f;
	for (int i=0; i<4; ++i)
	{
		colorbuffer.change_subvalue(first+4*2+i, color*bluffcolordark1);
		colorbuffer.change_subvalue(first+4*3+i, color*bluffcolordark2);
		colorbuffer.change_subvalue(first+4*6+i, color*bluffcolordark2);
		colorbuffer.change_subvalue(first+4*8+i, color*bluffcolordark2);
	}
	
	/*
  
      xbluff
      <--->
	6------ S  ytop
	|       |
	5-4   S-S   yuntop
	  |   |   
	2-3   S-S   ysquare
	|       |
	1       S   ypic
	 \     /
	  \   /
	   \ /
	    0
	*/

	// PLACING SLOPE
	vec3 slopebotcenter = geom::yfixed(bot_center, height_startgirder) + dirzN*sizeslope.x/2.0f;
	float xbluff  = 0.3f*sizeslope.x/2.0f;
	float ytop    = 0.7f*sizeslope.y;
	float yuntop  = 0.6f*sizeslope.y;
	float ysquare = 0.5f*sizeslope.y;
	float ypic    = 0.2f*sizeslope.y;
	float branchsize = 0.5f*size/2.0f;
	vec3 slopedirN = (endgirder - vvv::y()*sizeslope.y) - slopebotcenter;
	float slopel = glm::length(slopedirN);
	slopedirN = glm::normalize(slopedirN);

	PlanarSymmetry<glm::vec3> sym(
		infinitePlan{
			.point = slopebotcenter,
			.normalN = dirxN
		},
		&basebuffer,
		&elements,
		&colorbuffer
	);

	uint symi[7];
	symi[0]=sym.place_asymmetrical(slopebotcenter, color*0.6f);
	symi[1]=sym.place_symmetrical(slopebotcenter + vvv::y()*ypic    - dirxN*sizeslope.x/2.0f, color*0.8f);
	symi[2]=sym.place_symmetrical(slopebotcenter + vvv::y()*ysquare - dirxN*sizeslope.x/2.0f, color);
	symi[3]=sym.place_symmetrical(slopebotcenter + vvv::y()*ysquare - dirxN*xbluff, color*0.7f);
	symi[4]=sym.place_symmetrical(slopebotcenter + vvv::y()*yuntop  - dirxN*xbluff, color*0.7f);
	symi[5]=sym.place_symmetrical(slopebotcenter + vvv::y()*yuntop  - dirxN*sizeslope.x/2.0f, color*0.9f);
	symi[6]=sym.place_symmetrical(slopebotcenter + vvv::y()*ytop    - dirxN*sizeslope.x/2.0f, color);

	building::extruding::convex::sided(basebuffer, elements, {
		symi[0],
		symi[1],
		symi[2],
		symi[3],
		symi[4],
		symi[5],
		symi[6],
		sym.symmetrical(symi[6]),
		sym.symmetrical(symi[5]),
		sym.symmetrical(symi[4]),
		sym.symmetrical(symi[3]),
		sym.symmetrical(symi[2]),
		sym.symmetrical(symi[1]),
	}, slopedirN*slopel);

	_normal_hitbox.fusion_hitbox<PaverHitbox>(paverInfo{
		.bottom = basebuffer[symi[6]],
		.sized_x = basebuffer[symi[1]] - basebuffer[symi[6]],
		.sized_y = basebuffer[sym.symmetrical(symi[6])] - basebuffer[symi[6]],
		.sized_z = slopedirN*slopel
	});
	_normal_hitbox.fusion_hitbox<BevelHitbox>(bevelShape{
		.triangle_base = {
			basebuffer[symi[1]],
			basebuffer[symi[0]],
			basebuffer[sym.symmetrical(symi[1])]
		},
		.sized_direction = slopedirN*slopel
	});
	
	uint startc = colorbuffer.size();
	uint endc   = basebuffer.size();
	for(uint i=startc; i<endc; ++i)
	{
		colorbuffer.add_value(colorbuffer[i-endc+startc]);
	}

	// PLACING BRANCH
	first = basebuffer.size();
	paverInfo branch_info = paverInfo{
		.bottom  = endgirder + dirxN*(sizeslope.x/2.0f+branchsize) - vvv::y()*branchsize,
		.sized_x = -dirxN*(sizeslope.x+branchsize*2.0f),
		.sized_y = -dirzN*branchsize,
		.sized_z = -vvv::y()*sizeslope.y
	};
	building::paver::topsided(
		basebuffer,
		elements,
		branch_info
	);

	_normal_hitbox.fusion_hitbox<PaverHitbox>(branch_info);

	while(colorbuffer.size() < basebuffer.size())
	{
		colorbuffer.add_value(color);
	}
	colorbuffer.change_subvalue(first+0, color*0.7f);
	colorbuffer.change_subvalue(first+1, color*0.7f);
	colorbuffer.change_subvalue(first+2, color*0.7f);
	colorbuffer.change_subvalue(first+3, color*0.7f);
}
