#include "shinfos.hpp"

namespace shinfos
{
	static Archer* _player = NULL;

	void init(AbsoluteObjects& objs)
	{
		_player = objs.archer;
	}
	void free()
	{

	}

	Archer* player()
	{
		if(_player == NULL)
		{
			ssl::err("Try to call shinfos::player() while shinfos is not initialized");
		}

		return _player;
	}
};