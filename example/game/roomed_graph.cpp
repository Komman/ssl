#include "roomed_graph.hpp"

#include "../utils/geom.hpp"
#include "../utils/mutils.hpp"
#include "../../ssl/src/utils.hpp"

#include "../motor/motor_debug.hpp"

using namespace glm;
using namespace std;


RoomedGraph::RoomedGraph(const BorderedGraph& planar_graph)
	: PreRoomedGraph(planar_graph)
{

}

PreRoomedGraph::PreRoomedGraph(const BorderedGraph& planar_graph)
	: BorderedGraph(planar_graph)
{
	auto& border     = this->get_border(); 
	auto  dontremove = utils::vectounset(border);

	/*
		SET BORDER
	*/
	{
		this->apply_border();
	}

	/*
		REMOVE EVERY SELF LOOP EDGES
	*/
	{
		this->remove_self_loops();
	}


	/*
		REMOVE EVERY EDGES THAT
		ARE ON ONE UNIQUE FACE
	*/
	{
		this->remove_small_degree(1, dontremove);
		bool end   = false;

		while(!end)
		{
			auto edges = this->all_edges();
			end = true;

			for(const auto& e : edges)
			{
				std::vector<uint> face = this->face(e.first, e.second, true);

				if(utils::subsegment(face, {e.first, e.second}) && utils::subsegment(face, {e.second, e.first}))
				{
					// utils::print_vector(face);cout<<endl;

					#ifdef MOTOR_DEBUG
					if(utils::in_vector(border, e.first) || utils::in_vector(border, e.second))
					{	
						mot::err("Impossible: border on the same face");
					}	
					#endif
					
					this->remove_edge(e);
					end = false;
					break;
				}
			}
		}
	}

	/*
		REMOVE ALL THE GRAPH CONNECTED
		COMPONENTS THAT ARE NOT THE BORDER'S ONE
	*/
	{
		this->remove_small_degree(1, dontremove);

		if(border.size() == 0)
		{
			mot::err("no border");
		}

		std::unordered_set<uint> connected = this->DFS(border[0]);
		
		std::vector<uint> vertices = this->all_vertices();
		for(uint v : vertices)
		{
			if(connected.find(v) == connected.end())
			{
				this->remove_vertex(v);
			}
		}
	}
}
