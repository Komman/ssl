#include "castle_colors.hpp"
#include <vector>

#include "../motor/gutils.hpp"

namespace castle_colors
{
	static glm::vec3 _BASE       = glm::vec3(0.7, 0.5, 0.3)*1.3f;
	static glm::vec3 _FLOOR      = glm::vec3(0.7, 0.4, 0.2);
	static glm::vec3 _FAT_TOWERS = _BASE;
	static glm::vec3 _FACE_ARCS  = glm::vec3(0.7,0.5,0.35)*1.3f;
	static glm::vec3 _ELEVATORS  = glm::vec3(0.7,0.5,0.35)*0.7f;
	static glm::vec3 _ROOFS = glm::vec3(0.3, 0.3, 0.5);

	static glm::vec3 _ATOWER_HEAD = glm::vec3(0.6, 0.5, 0.4)*1.3f;
	static glm::vec3 _ATOWER_ROOF = glm::vec3(0.3,0.4,0.8);

	static glm::vec3 _CATHEDRAL_ROOF = glm::vec3(0.2,0.8,0.4)*0.8f;
	static glm::vec3 _CATHEDRAL_WINDOW = glm::vec3(0.6, 0.4, 0.3)*1.3f;
	static glm::vec3 _CATHEDRAL_WINDOW_GLASS = glm::vec3(0.3, 0.3, 0.6)*0.7f;
	static glm::vec3 _CATHEDRAL_WINDOW_PILERS = glm::vec3(0.5, 0.4, 0.4)*0.7f;
	static glm::vec3 _CATHEDRAL_WINDOW_ROOF = glm::vec3(0.4, 0.24, 0.06)*0.7f;

	static glm::vec3 _CIRCL_BUMP_PROBABLE = glm::vec3(0.3, 0.3, 0.5);
	static glm::vec3 _CIRCL_BUMP_UNPROBABLE = glm::vec3(0.15,0.15,0.3);

	static glm::vec3 _WALLCON_PAVUP = glm::vec3(0.8,0.5,0.4);

	static glm::vec3 _SIMPLE_TOWER = glm::vec3(0.7,0.5,0.3);
	static glm::vec3 _SIMPLE_TOWER_ROOF = glm::vec3(0.20,0.10,0.25);
	static glm::vec3 _SIMPLE_TOWER_WIN = glm::vec3(0.1,0.05,0.05);
	static glm::vec3 _SIMPLE_TOWER_CORNER = glm::vec3(0.4,0.28,0.24);

	static glm::vec3 _SIMPLE_WALL = glm::vec3(0.6,0.5,0.5);
	static glm::vec3 _BRIDGE = glm::vec3(0.6,0.4,0.3)*1.3f;

	static glm::vec3 _ARC_DOORS_EXT = glm::vec3(0.7,0.5,0.3);
	static glm::vec3 _ARC_DOORS_INSIDE = glm::vec3(0.4,0.2,0.1);

	static glm::vec3 _SQUARE_TOWER = glm::vec3(0.6,0.5,0.5)*1.25f;

	static glm::vec3 _MAGIC_TOWER_BASE = glm::vec3(0.7,0.5,0.3)*1.3f;
	static glm::vec3 _MAGIC_TOWER_ROOM = _MAGIC_TOWER_BASE;
	static glm::vec3 _MAGIC_TOWER_ROOF = glm::vec3(0.4,0.1,0.1);
	static glm::vec3 _MAGIC_TOWER_WIN = glm::vec3(0.4,0.3,0.2);

	static glm::vec3 _INSIDE_COLOR = glm::vec3(0.5,0.5,0.5);

	static std::vector<glm::vec3*> all_colors = {
		&_BASE,
		&_FLOOR,
		&_FAT_TOWERS,
		&_FACE_ARCS,
		&_ELEVATORS,
		&_ROOFS,
		&_ATOWER_HEAD,
		&_ATOWER_ROOF,
		&_CATHEDRAL_ROOF,
		&_CATHEDRAL_WINDOW,
		&_CATHEDRAL_WINDOW_GLASS,
		&_CATHEDRAL_WINDOW_PILERS,
		&_CATHEDRAL_WINDOW_ROOF,
		&_CIRCL_BUMP_PROBABLE,
		&_CIRCL_BUMP_UNPROBABLE,
		&_WALLCON_PAVUP,
		&_SIMPLE_TOWER,
		&_SIMPLE_TOWER_ROOF,
		&_SIMPLE_TOWER_WIN,
		&_SIMPLE_TOWER_CORNER,
		&_SIMPLE_WALL,
		&_BRIDGE,
		&_ARC_DOORS_EXT,
		&_ARC_DOORS_INSIDE,
		&_SQUARE_TOWER,
		&_MAGIC_TOWER_BASE,
		&_MAGIC_TOWER_ROOM,
		&_MAGIC_TOWER_ROOF,
		&_MAGIC_TOWER_WIN,
		&_INSIDE_COLOR,
	};

	void generate_color_set()
	{
		using namespace glm;

		for(uint i=0; i<all_colors.size(); i++)
		{
			*(all_colors[i]) = glm::normalize(utils::random_vec3(vec3(0), vec3(1)))*utils::random_float(0.6f, 1.4f);
		}
	}

	const glm::vec3& BASE()
	{
		return _BASE;
	}

	const glm::vec3& FLOOR()
	{
		return _FLOOR;
	}

	const glm::vec3& FAT_TOWERS()
	{
		return _FAT_TOWERS;
	}

	const glm::vec3& FACE_ARCS()
	{
		return _FACE_ARCS;
	}

	const glm::vec3& ELEVATORS()
	{
		return _ELEVATORS;
	}

	const glm::vec3& ROOFS()
	{
		return _ROOFS;
	}

	const glm::vec3& ATOWER_HEAD()
	{
		return _ATOWER_HEAD;
	}

	const glm::vec3& ATOWER_ROOF()
	{
		return _ATOWER_ROOF;
	}

	const glm::vec3& CATHEDRAL_ROOF()
	{
		return _CATHEDRAL_ROOF;
	}

	const glm::vec3& CATHEDRAL_WINDOW()
	{
		return _CATHEDRAL_WINDOW;
	}

	const glm::vec3& CATHEDRAL_WINDOW_GLASS()
	{
		return _CATHEDRAL_WINDOW_GLASS;
	}

	const glm::vec3& CATHEDRAL_WINDOW_PILERS()
	{
		return _CATHEDRAL_WINDOW_PILERS;
	}

	const glm::vec3& CATHEDRAL_WINDOW_ROOF()
	{
		return _CATHEDRAL_WINDOW_ROOF;
	}


	const glm::vec3& CIRCL_BUMP_PROBABLE()
	{
		return _CIRCL_BUMP_PROBABLE;
	}

	const glm::vec3& CIRCL_BUMP_UNPROBABLE()
	{
		return _CIRCL_BUMP_UNPROBABLE;
	}


	const glm::vec3& WALLCON_PAVUP()
	{
		return _WALLCON_PAVUP;
	}


	const glm::vec3& SIMPLE_TOWER()
	{
		return _SIMPLE_TOWER;
	}

	const glm::vec3& SIMPLE_TOWER_ROOF()
	{
		return _SIMPLE_TOWER_ROOF;
	}

	const glm::vec3& SIMPLE_TOWER_WIN()
	{
		return _SIMPLE_TOWER_WIN;
	}

	const glm::vec3& SIMPLE_TOWER_CORNER()
	{
		return _SIMPLE_TOWER_CORNER;
	}


	const glm::vec3& SIMPLE_WALL()
	{
		return _SIMPLE_WALL;
	}

	const glm::vec3& BRIDGE()
	{
		return _BRIDGE;
	}


	const glm::vec3& ARC_DOORS_EXT()
	{
		return _ARC_DOORS_EXT;
	}

	const glm::vec3& ARC_DOORS_INSIDE()
	{
		return _ARC_DOORS_INSIDE;
	}


	const glm::vec3& SQUARE_TOWER()
	{
		return _SQUARE_TOWER;
	}


	const glm::vec3& MAGIC_TOWER_BASE()
	{
		return _MAGIC_TOWER_BASE;
	}

	const glm::vec3& MAGIC_TOWER_ROOM()
	{
		return _MAGIC_TOWER_ROOM;
	}

	const glm::vec3& MAGIC_TOWER_ROOF()
	{
		return _MAGIC_TOWER_ROOF;
	}

	const glm::vec3& MAGIC_TOWER_WIN()
	{
		return _MAGIC_TOWER_WIN;
	}

	const glm::vec3& INSIDE_COLOR()
	{
		return _INSIDE_COLOR;
	}
};
