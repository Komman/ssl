#ifndef _LASTING_ARROW_HPP_
#define _LASTING_ARROW_HPP_

#include "arrow.hpp"

template<typename ArrowClass>
class LastingArrow : public ArrowClass
{
public:
	template<typename... ConstructorArgs>
	LastingArrow(float last_time, ConstructorArgs&&... args);
	virtual ~LastingArrow() {}

	void animate(const PhysicalContext& context, float dt) override;
	bool to_destroy() override;

protected:
	virtual void disapearing_action() {}

private:
	float _last_time;
	float _impact_time;
	float _time;
};




template<typename ArrowClass>
template<typename... ConstructorArgs>
LastingArrow<ArrowClass>::LastingArrow(float last_time, ConstructorArgs&&... args)
	: ArrowClass(args...),
	  _last_time(last_time),
	  _impact_time(-1),
	  _time(0)
{

}

template<typename ArrowClass>
void LastingArrow<ArrowClass>::animate(const PhysicalContext& context, float dt)
{
	this->ArrowClass::animate(context, dt);

	if(this->is_planted())
	{
		_time += dt;
	}
}

template<typename ArrowClass>
bool LastingArrow<ArrowClass>::to_destroy()
{
	if(this->is_planted())
	{
		return (_time  >= _last_time);
	}
	return false;
}


#endif //_LASTING_ARROW_HPP_
