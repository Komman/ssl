#ifndef _CASTLE_ANIMATOR_HPP_
#define _CASTLE_ANIMATOR_HPP_

#include "castle_placer.hpp"

namespace CASTLE
{
	class CastleAnimator : public CastlePlacer
	{
	public:
		CastleAnimator(const castleTools& tools, const glm::vec3& position);

		void animate(float dt);

	private:

	};
}

#endif //_CASTLE_ANIMATOR_HPP_
