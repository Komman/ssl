#ifndef _CASTLE_COLORS_HPP_
#define _CASTLE_COLORS_HPP_

#include <glm/glm.hpp>

namespace castle_colors
{
	void generate_color_set();

	const glm::vec3& BASE();
	const glm::vec3& FLOOR();
	const glm::vec3& FAT_TOWERS();
	const glm::vec3& FACE_ARCS();
	const glm::vec3& ELEVATORS();
	const glm::vec3& ROOFS();

	const glm::vec3& ATOWER_HEAD();
	const glm::vec3& ATOWER_ROOF();

	const glm::vec3& CATHEDRAL_ROOF();
	const glm::vec3& CATHEDRAL_WINDOW();
	const glm::vec3& CATHEDRAL_WINDOW_GLASS();
	const glm::vec3& CATHEDRAL_WINDOW_PILERS();
	const glm::vec3& CATHEDRAL_WINDOW_ROOF();

	const glm::vec3& CIRCL_BUMP_PROBABLE();
	const glm::vec3& CIRCL_BUMP_UNPROBABLE();

	const glm::vec3& WALLCON_PAVUP();

	const glm::vec3& SIMPLE_TOWER();
	const glm::vec3& SIMPLE_TOWER_ROOF();
	const glm::vec3& SIMPLE_TOWER_WIN();
	const glm::vec3& SIMPLE_TOWER_CORNER();

	const glm::vec3& SIMPLE_WALL();
	const glm::vec3& BRIDGE();

	const glm::vec3& ARC_DOORS_EXT();
	const glm::vec3& ARC_DOORS_INSIDE();

	const glm::vec3& SQUARE_TOWER();

	const glm::vec3& MAGIC_TOWER_BASE();
	const glm::vec3& MAGIC_TOWER_ROOM();
	const glm::vec3& MAGIC_TOWER_ROOF();
	const glm::vec3& MAGIC_TOWER_WIN();

	const glm::vec3& INSIDE_COLOR();
};

#endif //_CASTLE_COLORS_HPP_
