#include "hitboxed_arrow.hpp"

#include "../utils/geom.hpp"

HitboxedArrow::HitboxedArrow(const arrowStart& arrow)
	: Arrow(arrow),
	  _hbx(prism_from_arrow(arrow))
{
	this->update_hitbox();
}

const BasicHitbox& HitboxedArrow::get_hitbox() const
{
	return _hbx;
}

prismInfo HitboxedArrow::prism_from_arrow(const arrowStart& arrow)
{
	glm::vec3 dirxN = geom::get_orthogonalyx_N(arrow.directionN);
	glm::vec3 diryN = glm::cross(arrow.directionN, dirxN);

	float cospi3 = 0.5;
	float sinpi3 = sqrt(3.0f)/2.0f;

	float sm = arrow.size/2.0f;

	prismInfo prism = {.points = {
		arrow.position - arrow.directionN*arrow.length,
		arrow.position + diryN*sm,
		arrow.position - (diryN*cospi3 + dirxN*sinpi3)*sm,
		arrow.position - (diryN*cospi3 - dirxN*sinpi3)*sm
	}};

	return prism;
}


void HitboxedArrow::update_hitbox()
{
	_hbx.reset_shape(prism_from_arrow({
		this->get_position(),
		this->get_directionN(),
		this->get_length(),
		this->get_size()
	}));

	_hbx.set_speed(this->get_speed());
}
