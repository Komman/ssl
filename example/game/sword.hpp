#ifndef _SWORD_HPP_
#define _SWORD_HPP_

#include "../motor/stored_drawable_object.hpp"

class Sword : public StoredDrawableObject<glm::vec3, // Positions
										  glm::vec3  // Colors
										  >
{
public:
	Sword(const glm::vec3& position);

private:
	enum buffersRoles {POSITIONS, COLORS};

	void build_sword();
	void center_on_pos(const glm::vec3& position);

};

#endif //_SWORD_HPP_
