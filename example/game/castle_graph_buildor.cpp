#include "castle_graph_buildor.hpp"

#include "../utils/geom.hpp"
#include "../utils/mutils.hpp"

#include "roomed_graph.hpp"

using namespace glm;
using namespace std;

namespace CASTLE
{
	static bool print_adv = true;

	GraphBuildor::GraphBuildor(const graphParameters& params)
		: CastlePositioner(params.context, params.position),
		  _params(params)
	{

	}

	TheoricalArchitecture GraphBuildor::compute_theorical_architecture(const theoricalParameters& parameters)
	{
		/*
			Compute Roomed graph
		*/
		RoomedGraph roomed = this->compute_roomed_graph(parameters.safe_distance);
	
	
		/*
			And from it, generates its theorical architecture
		*/		
		if(print_adv) cout<<"Computing Theorical Architecture..."<<endl;
		return TheoricalArchitecture(roomed, parameters.heights);
	}


	RoomedGraph GraphBuildor::compute_roomed_graph(float safe_distance)
	{
		/*
			1. Bordered points
		*/
		BorderedGraph bgraph = this->create_points();

		/*
			2. Build the planar thick graph
		*/
		ThickGraph thickplanar = this->build_thick_planar_graph(bgraph, safe_distance);

		/*
			3. Force it to ba a thick roomed graph
		*/
		if(print_adv) cout<<"Computing graph room compatible..."<<endl;
		return RoomedGraph(thickplanar);
	}

	DualMultiGraph GraphBuildor::build_dual(const BorderedGraph& graph)
	{
		return DualMultiGraph(graph);
	}


	BorderedGraph GraphBuildor::create_points()
	{
		PlanGraph graph;

		vec2 center = geom::projplan(geom::mean(this->enclosure_towers()));

		graph.add_vertex(center);
		uint pointbyline = 15;
		std::vector<uint> border;

		for(uint i=0; i<this->enclosure_towers().size(); i++)
		{
			uint inext = (i+1)%this->enclosure_towers().size();
			vec2 pos1 = geom::projplan(this->enclosure_towers()[i    ]);
			vec2 pos2 = geom::projplan(this->enclosure_towers()[inext]);
			auto interpos1 = geom::interpolate(pos1, center, pointbyline);
			auto interpos2 = geom::interpolate(pos2, center, pointbyline);
			
			uint n = std::min(interpos1.size(), interpos2.size());
			uint first = 2;

			border.push_back(graph.add_vertex(interpos1[first]));

			for(uint j=first+1; j<n-2; j++)
			{
				graph.add_vertex(glm::mix(interpos1[j],
										  interpos2[j],
										  utils::random_float(0,1)));
			}
		}

		return BorderedGraph(graph, border);
	}

	ThickGraph GraphBuildor::build_thick_planar_graph(const BorderedGraph& points, float safe_distance)
	{
		BorderedGraph graph(points);
		this->check_border_safe(graph, safe_distance);

		auto dontremove = utils::vectounset(points.get_border());

		if(print_adv) cout<<"Correcting castle graph points..."<<endl;
		graph.remove_close_vertices(glm::max(safe_distance, 15.0f), dontremove);
		
		if(print_adv) cout<<"Triangulating castle graph..."<<endl;
		// graph.delaunay_triangulation();
		graph.set_chicken_triangulation();

		// std::vector<Graph::Edge> oncircle = graph.circle_intersection({geom::projplan(_params.position), this->get_enclosure_radius()/4.0f});
		// for(uint i=2; i<oncircle.size(); i++)
		// {
		// 	graph.remove_edge(oncircle[i]);
		// }

		if(print_adv) cout<<"Adapting castle graph edges..."<<endl;
		graph.apply_border();

		if(print_adv) cout<<"Adapting castle graph vertices..."<<endl;
		graph.remove_big_degree(6, dontremove);
		graph.remove_small_degree(1, dontremove);

		if(print_adv) cout<<"Creating Thick Graph..."<<endl;
		return ThickGraph(graph, safe_distance);
	}

	void GraphBuildor::check_border_safe(const BorderedGraph& points, float safe_distance)
	{
		auto& border = points.get_border();

		for(uint i=0; i<border.size(); i++)
		{
			uint j = (i+1)%(border.size());

			float d = glm::distance(points[border[i]], points[border[j]]);

			if(d < safe_distance)
			{
				mot::err("GraphBuildor::check_border_safe(): border not safe: two vertices from border are at distance"
						 + to_string(d) + " while safe_distance = " + to_string(safe_distance));
			}
		}
	}
};
