#include "castle_role_graph.hpp"

#include "../../ssl/src/utils.hpp"
#include "../utils/mutils.hpp"
#include "../utils/geom.hpp"

#include "../motor/motor_debug.hpp"

using namespace std;
using namespace glm;

namespace CASTLE
{
	RoleGraph::RoleGraph(const BorderedGraph& g, const graphHeightParam& params)
		: StoredFaceGraph(g),
		  _params(params),
		  _heights_variance(1.0f)
	{
		_heights_variance = utils::random_float(0.8f, 1.3f);
		_heights_variance = _heights_variance*_heights_variance;
		_heights_variance = _heights_variance*_heights_variance;

		this->give_heights();
		this->give_roles();
		this->adapt_height_to_roles();
	}


	std::pair<float, float> RoleGraph::inborder_face_adjacent_minmaxhights(uint face) const
	{
		vector<uint> adj = this->inborder_adjacent_faces(face);

		if(adj.size() == 0)
		{
			if(this->adjacent(face, this->get_border_face()))
			{
				float h = this->get_height(face);
				return {h, h};
			}
			else
			{
				mot::err("RoleGraph::inborder_face_adjacent_minmaxhights(): face "
					+ std::to_string(face) + " has no adjacent faces");
			}
		}

		std::pair<float, float> minmax =  {this->get_height(adj[0]), this->get_height(adj[0])};

		for(uint f : adj)
		{
			float h = this->get_height(f);
			
			if(h < minmax.first)
			{
				minmax.first = h;
			}
			if(h > minmax.second)
			{
				minmax.second = h;
			}
		}

		return minmax;
	}

	void RoleGraph::give_heights()
	{
		float ND = _params.ND;
		float baseh = _params.floor_h + _params.min_roof_h;

		std::vector<uint> faces = this->all_faces();
		for(uint f : faces)
		{
			float dcenter = glm::distance(this->position(f), this->get_center());
			this->set_height(f, baseh + ND*200.0f*(0.4f + utils::random_float(0,_heights_variance))/(2.2f + 0.001f*dcenter*dcenter));
		}

		vector<pair<uint, float>> minaround;
		for(uint f : faces)
		{
			if(f != this->get_border_face() && this->adjacent(f, this->get_border_face()))
			{
				minaround.push_back({f, this->inborder_face_adjacent_minmaxhights(f).first - baseh});
			}
		}
		for(pair<uint, float> fh : minaround)
		{
			this->set_height(fh.first, baseh + fh.second/2.0f);
		}

		this->set_height(this->get_border_face(), _params.floor_h);
	}

	void RoleGraph::give_roles()
	{
		float ND    = _params.ND;
		auto& graph = this->graph();

		std::vector<uint> faces = this->all_faces();
		uint borderface = this->get_border_face();

		for(uint f : faces)
		{
			this->set_structure(f, NULL);

			if(f != borderface)
			{
				// this->set_structure(f, std::make_unique<CASTLE::AtowerRole>());
				// continue;

				vector<uint> vertices = this->face_vertices(f);
				float convexity = geom::cycle_conspace_maxdelargement(graph[vertices]);

				#warning An ATOWER needs to be convex, it is not checked here yet
				if(convexity >= 13.0f*ND && rand()%4==0 && !this->adjacent(f, this->get_border_face()))
				{
					this->set_structure(f, std::make_unique<CASTLE::AtowerRole>());
					// this->place_atower(graph, f, utils::random_float(40,70)*ND, convexity);
					continue;
				}
		 
				if(vertices.size() >= 8 && rand()%2==0)
				{
					this->set_structure(f, std::make_unique<CASTLE::CathedralRole>());
					// this->place_cathedral(graph, f, 50.0f*ND);
					continue;
				}
			}
		}

		for(uint f : faces)
		{
			if(this->get_structure(f) == NULL && f != borderface)
			{
				uint type = rand()%3;

				if(type==0)
				{
					this->set_structure(f, std::make_unique<CASTLE::RoofupRole>());
					// this->bluffup(graph, f, 2.0f*ND);
					// this->place_roof(graph, f, 2.0f*ND, 5.0f*ND);
				}
				else if(type==1)
				{
					this->set_structure(f, std::make_unique<CASTLE::FreeRole>());
					// this->openext_face(graph, f, 2.0f*ND);
				}
				else if(type==2)
				{
					vector<uint> vertices = this->face_vertices(f);

					float radius = geom::cycle_mindistance(graph[vertices], this->position(f));
					float thick  = ND;

					if(!geom::point_in_polygon(this->position(f), graph[vertices]))
					{
						radius = 0;
					}

					if(radius > 2.0f*thick)
					{
						this->set_structure(f, std::make_unique<CASTLE::CirclebumpRole>());
						// this->circle_bump(graph, f, 5.0f*ND, radius*0.9f, thick, 5+rand()%5);	
					}
					else
					{
						this->set_structure(f, std::make_unique<CASTLE::RoofRole>());
						// this->place_roof(graph, f, 0.0f, 5.0f*ND);
					}
				}
			}

			if(this->get_structure(f) == NULL)
			{
				this->set_structure(f, std::make_unique<CASTLE::FreeRole>());
			}
		}

		this->set_structure(borderface, std::make_unique<CASTLE::FloorRole>());
	}


	void RoleGraph::adapt_height_to_roles()
	{
		float ND    = _params.ND;
		auto& graph = this->graph();

		std::vector<uint> faces = this->all_faces();

		for(uint f : faces)
		{
			auto structure = this->get_structure(f);

			if(structure == NULL)
			{
				mot::err("RoleGraph::adapt_height_to_roles(): a face has no role");
			}

			this->set_height(f, this->get_height(f) + structure->additionnal_height()*_params.ND);
		}
	}

	float RoleGraph::get_min_door_h() const
	{
		return _params.door_height;
	}

	bool RoleGraph::place_for_ext_door(uint face1, uint face2) const
	{
		return (std::abs(this->get_height(face1) - this->get_height(face2))) > _params.door_height;
	}

	bool RoleGraph::ascending_face_edge(const Edge& face_edge) const
	{
		Edge orientation = this->faces_orientation(face_edge);

		return (orientation.first == face_edge.first);
	}

	bool RoleGraph::descending_edge(const Edge& e, uint face) const
	{
		uint opface = this->opposite_face(e, face);
		float hs = this->get_height(face);
		float ho = this->get_height(opface);
			
		if(hs < ho)
		{
			return true;
		}
		if(hs > ho)
		{
			return false;
		}

		return face < opface;
	}

	float RoleGraph::ascending_height(const Edge& e, uint face) const
	{
		return this->get_height(this->opposite_face(e, face)) - this->get_height(face);
	}

	std::vector<RoleGraph::Edge> RoleGraph::descending_adjacent_edges(uint face) const
	{
		using  namespace std;
		std::vector<Edge> all = this->face_edges(face);
		std::vector<Edge> ret;

		for(const Edge& e : all)
		{
			uint f = this->opposite_face(e, face);

			if(this->ascending_face_edge({f, face}))
			{
				ret.push_back(e);
			}
		}

		return ret;
	}

	std::vector<RoleGraph::Edge> RoleGraph::ascending_adjacent_edges(uint face) const
	{
		using  namespace std;
		std::vector<Edge> all = this->face_edges(face);
		std::vector<Edge> ret;

		for(const Edge& e : all)
		{
			uint f = this->opposite_face(e, face);

			if(!(this->ascending_face_edge({f, face})))
			{
				ret.push_back(e);
			}
		}

		return ret;
	}	

	RoleGraph::Edge RoleGraph::faces_orientation(const Edge& face_edge) const
	{
		return this->faces_orientation(face_edge.first, face_edge.second);
	}


	RoleGraph::Edge RoleGraph::faces_orientation(uint face1, uint face2) const
	{
		if(this->get_height(face1) == this->get_height(face2))
		{
			if(face1 < face2)
			{
				return {face1, face2};
			}
			else
			{
				return {face2, face1};
			}
		}

		if(this->get_height(face1) < this->get_height(face2))
		{
			return {face1, face2};
		}
		else
		{
			return {face2, face1};
		}
	}

	void RoleGraph::set_height(uint face, float h)
	{
		this->value(face).height = h;
	}

	void RoleGraph::set_structure(uint face, Structure&& structure)
	{
		this->value(face).structure = std::move(structure);
	}

	float RoleGraph::get_height(uint face) const
	{
		return this->get_storage(face).height;
	}

	const glm::vec2& RoleGraph::get_center() const
	{
		return _params.center;
	}

	CASTLE::BasicRole* RoleGraph::get_structure(uint face) const
	{
		return this->get_storage(face).structure.get();
	}

	const graphHeightParam& RoleGraph::geom_params() const
	{
		return _params;
	}
};

