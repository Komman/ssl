#ifndef _TEST_ARROW_HPP_
#define _TEST_ARROW_HPP_

#include "arrow.hpp"
#include "physical_context.hpp"

#include "../motor/linear_valuator.hpp"
#include "../motor/dynamic_light.hpp"
#include "../motor/light_intensity_valuator.hpp"
#include "../motor/regular_signaler.hpp"

class TestArrow : public Arrow
{
public:
	TestArrow(const arrowStart& start);
	virtual ~TestArrow() {}

	void animate(const PhysicalContext& context, float dt);

protected:
	virtual void planting_reaction(const glm::vec3& position,
								   const glm::vec3& normal,
								   const glm::vec3& impact_speed);
	virtual bool to_destroy() override;


private:
	void init();
	
private:
	float _light_intensity;
	float _planted_time;
	float _duration;

	RegularSignaler _smoke_emission;
	DynamicLight _light;
};

#endif //_TEST_ARROW_HPP_
