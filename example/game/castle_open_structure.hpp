#ifndef _CASTLE_OPEN_STRUCTURE_HPP_
#define _CASTLE_OPEN_STRUCTURE_HPP_

#include "castle_face_structure.hpp"

namespace CASTLE
{
	class OpenStructure : public FaceStructure
	{
	public:
		OpenStructure(const baseBuiling& base, uint face);

	protected:
		void build_face(const std::vector<doorWall>& walls_to_build) override;
		void build_wall_deco(const std::vector<doorWall>& walls_to_build) override;
		
	private:
		void build_wall_deco_exposed(const std::vector<doorWall>& walls_to_build);
		void build_wall_deco_cuve(const std::vector<doorWall>& walls_to_build);
	
	private:
		float _barrierw;
		float _underbluf;
		bool  _bluff;
	};
};

#endif //_CASTLE_OPEN_STRUCTURE_HPP_
