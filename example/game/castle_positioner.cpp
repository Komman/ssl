#include "castle_positioner.hpp"

#include <vector>

#include "../utils/geom.hpp"
#include "../motor/primary_hitbox.hpp"
#include "castle_colors.hpp"

using namespace glm;


CastlePositioner::CastlePositioner(const castlePhysicalContext& context, const glm::vec3& position)
	: _context(context),
	  _position(position),
	  _enclosure_radius(150.0f),
	  _ext_towers_count(15),
	  _floor_height(1),
	  _floor_absolute_height("catsle_floor_absolute_height", _position.y + _floor_height),
	  _floor_color("castle_floor_color", castle_colors::FLOOR()),
	  _enclosure_towers(0)
{
	this->place_enclosure(_enclosure_radius, _ext_towers_count);
}

void CastlePositioner::place_enclosure(float radius, uint tower_count)
{
	float radius_random_coeff = 0.3f;
	float angle_random_coeff  = 0.3f;

	float dangle = float(M_PI)*2.0f/float(tower_count);

	std::vector<vec2> v;
	for(uint i=0; i<tower_count; i++)
	{
		vec3 point = geom::circle_point(
				_position,
				vec3(1,0,0),
				vec3(0,1,0),
				dangle*(float(i) + utils::random_float(0, angle_random_coeff)),
				radius*utils::random_float(1.0f - radius_random_coeff, 1.0f + radius_random_coeff));

		_enclosure_towers.push_back(point);

		v.push_back(vec2(point.x, point.z));
	
		if(i==0)
		{
			_minmaxbox.min = point;
			_minmaxbox.max = point;
		}
		else
		{
			_minmaxbox.cover(point);
		}
	}

	for(uint i=0; i<_enclosure_towers.size(); i++)
	{
		_floor_hbx.fusion_hitbox<PrismHitbox>(prismInfo{.points={
			vvv::y()*_floor_height + _enclosure_towers[i],
			vvv::y()*_floor_height + _enclosure_towers[(i+1)%_enclosure_towers.size()],
			vvv::y()*_floor_height + _position,
			_position
		}});	
	}
	_context.context->add_static_hitbox(&_floor_hbx);

	_context.ground->hole_circular(v, vec3(_position.x, _position.y, _position.z));
	_context.ground->generate_hitboxes();
}

const spaceBox& CastlePositioner::minmax_coord() const
{
	return _minmaxbox;
}

const std::vector<glm::vec3>& CastlePositioner::enclosure_towers() const
{
	return _enclosure_towers;
}

glm::vec3 CastlePositioner::get_floor_potision() const
{
	return _position + vvv::y()*_floor_height;
}

const HitboxFusion& CastlePositioner::get_floor_hbx() const
{
	return _floor_hbx;
}

