#include "archer.hpp"

#include "../utils/geom.hpp"

#include "projectiles.hpp"
#include "test_arrow.hpp"
#include "lasting_arrow.hpp"
#include "hitboxed_arrow.hpp"
#include "tower_missile.hpp"

using namespace glm;

Archer::Archer(const glm::vec3&   start,
		       float speed,
		       SharedRessources& shared_ressources)
	: Player(ingame::game_mouse_key(), start, speed, 1.0f),
	  MAX_SIZE(1.0f),	 
	  _shared_ressources(shared_ressources),
	  _hitbox(vec3(0), sphereInfo{vec3(0,-0.3,0), MAX_SIZE*0.3f}),
	  _loading(false)
{

}

BasicHitbox& Archer::get_hitbox()
{
	return _hitbox;
}


void Archer::frame_update(float dt)
{
	Player::frame_update(dt);

	bool kp = false;

	if(this->key_pressed('E'))
	{
		if(!_loading)
		{
			lanch_simple_arrow(this->get_direction(), 20.0f);
		}
		_loading = true;
		kp = true;
	}
	if(this->key_pressed('B'))
	{
		if(!_loading)
		{
			lanch_bomb(this->get_direction(), 100.0f);
		}
		_loading = true;
		kp = true;
	}

	if(!kp)
	{
		_loading = false;
	}
}

void Archer::lanch_simple_arrow(const glm::vec3& direction, float speed)
{
	float arr_size = 1.0f;
	
	auto arr = std::make_unique<LastingArrow<HitboxedArrow>>(2, arrowStart({this->get_position(), this->get_direction(), arr_size, arr_size/30.0f}));

	projectiles::arrows().launch_simple_trail(std::move(arr), direction, speed);
}	

void Archer::lanch_bomb(const glm::vec3& direction, float speed)
{
	float arr_size = utils::random_float(2.0, 6.0f);
	
	auto arr = std::make_unique<TowerMissile>(towerMissileStart{this->get_position(), this->get_direction(), arr_size});

	projectiles::arrows().launch_tower_bomb(std::move(arr), direction, speed);
}	

void Archer::draw(GameDrawer& game_drawer)
{
	
}
