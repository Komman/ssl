#include "castle_atower_structure.hpp"

#include "../../ssl/src/vvv.hpp"
#include "../utils/mutils.hpp"

#include "../motor/building.hpp"
#include "../motor/meshbuild.hpp"
#include "../motor/wall_hitbox.hpp"
#include "../motor/partial_wall_hitbox.hpp"
#include "../motor/paver_hitbox.hpp"
#include "../motor/bevel_hitbox.hpp"
#include "../motor/geo_cylinder_hitbox.hpp"

#include "castle_colors.hpp"

using namespace glm;
using namespace std;

namespace CASTLE
{
	static float baseshadcoeff = 0.7f;

	AtowerStructure::AtowerStructure(const baseBuiling& base, uint face)
		: FaceStructure(base, face),
		  _barriersize(this->ND()*1.0f, this->ND()*1.0f),
		  _rebortsize( this->ND()*1.0f, this->ND()*1.0f),
		  _firstbarsize(this->ND()*0.8f, this->ND()*1.0f),
		  _groundthick(this->ND()*0.2f),
		  _wallssize(this->ND()*0.2f),
		  _minishad(this->ND()*0.8f),
		  _doorw(this->door_minh()),
		  _doorh(_doorw*1.5f),
		  _undetoproof_h(_doorh*5.0f),
		  _pilers_size(1.5f),
		  _roofheight(this->ND()*1.0f)
	{

	}

	void AtowerStructure::build_wall_deco(const std::vector<doorWall>& walls_to_build)
	{
		uint N = walls_to_build.size();

		for(uint i=0; i<N; i++)
		{
			doorWall w = walls_to_build[i];

			if(w.dh > 0 && rand()%2==0)
			{
				this->build_topdeco(w);
			}
		}
	}

	std::vector<glm::vec2> AtowerStructure::computes_delargement_and_heights(float final_delargement)
	{
		float ND = this->ND();
		float mindelarge  = (this->minwidth() + std::max(_rebortsize.x, _pilers_size));
		float totaldelarg = 0.0f;
		float dynmindelarge = mindelarge*1.5f;
		// int   maxlvls = final_delargement/mindelarge;

		std::vector<vec2> ret;
		while(totaldelarg + dynmindelarge < final_delargement)
		{
			float mdel = std::min(utils::random_float(dynmindelarge, dynmindelarge*3.0f), final_delargement - totaldelarg);
			float hsqrt = utils::random_float(3.0f, 8.0f);

			ret.push_back({mdel, _doorh + hsqrt*hsqrt*ND});
			totaldelarg += mdel;
		}

		return ret;
	}

	void AtowerStructure::build_face(const std::vector<doorWall>& walls_to_build)
	{
		ArrayBuffer<glm::vec3>& vertices = this->mesh_vertices();
		// ArrayBuffer<glm::vec3>& colors = this->mesh_colors();
		ElementBuffer& elements   = this->get_elements();

		// FILL WALL OUTSIDE
		this->fill_walls(walls_to_build);

		// BUILD OUTSIDE
		std::vector<uint> startindices = this->build_border(
			this->index_wall(walls_to_build, this->mesh_vertices_indices(walls_to_build)),
			_firstbarsize.y,
			_firstbarsize.x,
			0.0f,
			4.0f*_barriersize.x
		);		

		float maxrad = geom::cycle_conspace_maxdelargement(vertices.subset(startindices));
		float finalh = 0.0f; 

		std::vector<glm::vec2> delheights = this->computes_delargement_and_heights(
			(maxrad	-_pilers_size*3.0f)*0.9f//utils::random_float(0.75f, 0.95f)
		);
		subATowerIndices indices = {startindices};

		for(const auto& delh : delheights)
		{
			indices = this->build_subatower(indices, delh.x, delh.y);
		}


		// HEAD ROOF
		meshbuild::fill_cycle(vertices, elements, indices.nextop, true);

		geoCylinderInfo cyl = {
			.cycle = vertices.subset(indices.nextop),
			.sized_direction = -vvv::y()*_groundthick
		};
		this->add_hitbox(std::make_unique<GeoCylinderHitbox>(cyl));


		// PLACE ROOF
		std::vector<vec3> frompos = vertices.subset(indices.nextop);
		std::vector<vec3> pos = geom::conspace_enlarge_cycle(frompos, vec3(0), -_pilers_size, _pilers_size*3.0f);
		std::vector<vec3> xyshadow = {
			vec3(_pilers_size*1.0f, _undetoproof_h*0.0f, 0.7f),
			vec3(_pilers_size*1.0f, _undetoproof_h*0.2f, 1.0f),
			vec3(_pilers_size*0.8f, _undetoproof_h*0.2f, 0.8f),
			vec3(_pilers_size*0.8f, _undetoproof_h*1.0f, 1.0f)
		};	

		std::vector<glm::vec3> pilpoints;
		for(uint i=0; i<pos.size(); i++)
		{
			vec3 dirN = geom::trigo_bissectriceN(pos, i, vvv::y());
			
			if(pilpoints.size() > 0 && geom::distance(pos[i], pilpoints) < _pilers_size*1.3f)
			{
				continue;
			}

			pilpoints.push_back(pos[i]);
			this->place_fullsided_piler(pos[i], dirN, xyshadow, castle_colors::ATOWER_HEAD());
		}

		this->place_hatroof(
			geom::conspace_enlarge_cycle(pos, vvv::y()*_undetoproof_h, _pilers_size, _pilers_size*3.0f),
			_pilers_size*0.7f,
			_pilers_size,
			(7.0f+std::min(finalh*0.2f, 30.0f))*this->ND(),
			castle_colors::ATOWER_HEAD(),
			castle_colors::ATOWER_ROOF()
		);
	}

	void AtowerStructure::put_subatower_pilers(const partialCycle<uint>& backbot, const partialCycle<uint>& top)
	{
		ArrayBuffer<glm::vec3>& vertices = this->mesh_vertices();
		// ArrayBuffer<glm::vec3>& colors   = this->mesh_colors();
		// ElementBuffer& elements   = this->get_elements();

		#ifdef MOTOR_DEBUG
		if(!backbot.same_origin(top))
		{
			mot::err("AtowerStructure::put_subatower_pilers(): building allgorithm failed should be impossible");
		}
		#endif

		uint N = backbot.size();

		for(uint i=0; i<backbot.size(); i++)
		{
			uint h = (i+N-1)%N;
			uint j = (i+1)%N;

			glm::vec3 middle = geom::middle(vertices[backbot.cycle[i]], vertices[backbot.cycle[j]]);
			glm::vec3 dirmN  = glm::normalize(geom::trigo_normal(vertices[backbot.cycle[j]] - vertices[backbot.cycle[i]]));
			glm::vec3 botv = vertices[backbot.cycle[i]];
			glm::vec3 dirN = -geom::trigo_bissectriceN(vertices[backbot.cycle[h]], vertices[backbot.cycle[i]], vertices[backbot.cycle[j]]);
			float H = (vertices[top.cycle[i]].y - botv.y)*0.999f;
			float W = _pilers_size;

			if(glm::distance(middle, botv) < 2.0f*W)
				continue;

			std::vector<vec3> xyshadow = {
				vec3(W*1.0f, H*0.0f, 0.7f),
				vec3(W*1.0f, H*0.1f, 1.0f),
				vec3(W*0.8f, H*0.1f, 0.8f),
				vec3(W*0.8f, H*1.0f, 1.0f),
				vec3(W*0.1f, H*1.0f, 1.0f)
			};
			this->place_fullsided_wallpiler(botv, dirN, xyshadow, 1.0f, 0.0f, this->walls_color());
		
			if(backbot.edge[i])
			{
				std::vector<vec3> xyshadow = {
					vec3(W*1.0f, H*0.0f, 0.7f),
					vec3(W*1.0f, H*0.75f, 1.0f)
				};
				this->place_fullsided_wallpiler(middle, dirmN, xyshadow, 0.8f, H*0.09f, this->walls_color());
			}
		}
	}

	void AtowerStructure::put_subatower_door(const partialCycle<uint>& backbot, const partialCycle<uint>& top)
	{
		ArrayBuffer<glm::vec3>& vertices = this->mesh_vertices();
		ArrayBuffer<glm::vec3>& colors   = this->mesh_colors();
		ElementBuffer& elements   = this->get_elements();

		#ifdef MOTOR_DEBUG
		if(!backbot.same_origin(top))
		{
			mot::err("AtowerStructure::put_subatower_door(): building allgorithm failed should be impossible");
		}
		#endif

		uint N = backbot.size();

		for(uint i=0; i<backbot.size(); i++)
		{
			if(backbot.edge[i] == false)
			{
				uint j = (i+1)%N;

				float ytop = vertices[top.cycle[i]].y; 
				float ybot = vertices[backbot.cycle[i]].y; 

				auto indices = meshbuild::doored_fill(
					vertices,
					elements,
					{backbot.cycle[i], backbot.cycle[j], top.cycle[j], top.cycle[i]},
					0.5f, 
					_doorh,
					_doorw,
					true
				);
				this->complete_color_buffer(this->walls_color()*baseshadcoeff);
				colors.change_subvalue(indices[2], this->walls_color()*glm::mix(baseshadcoeff, 1.0f, (_doorh)/(ytop - ybot)));
				colors.change_subvalue(indices[3], this->walls_color()*glm::mix(baseshadcoeff, 1.0f, (_doorh)/(ytop - ybot)));

				geoCylinderInfo geo = {
					vertices.subset(vector<uint>(indices.begin(), indices.end())),
					geom::unprojplan(geom::trigo_normal(geom::projplan(geom::safe_normalize(vertices[backbot.cycle[j]] - vertices[backbot.cycle[i]]))))*_wallssize
				};
				this->add_hitbox<GeoCylinderHitbox>(geo);
			}
		}
	}

	// returns uint max if not possible
	std::vector<uint> AtowerStructure::select_doors_edge(const std::vector<glm::vec3>& cycle, float atowerheight)
	{
		if(_doorh*1.5f > atowerheight)
		{
			return {};
		}

		uint N = cycle.size();

		float maxd = 0.0f;
		uint  maxi = 0;

		for(uint i=0; i<N; i++)
		{
			if(rand()%3 >= 1)
				continue;

			float d = glm::distance(cycle[i], cycle[(i+1)%N]);
			if(d > maxd)
			{
				maxd = d;
				maxi = i;
			}
		}

		std::vector<uint> ret = {maxi};

		std::vector<uint> realret;
		for(uint e : ret)
		{
			if(glm::distance(cycle[e], cycle[(e+1)%N]) > _doorw)
			{
				realret.push_back(e);
			}
		}
		return realret;
	}

	AtowerStructure::subATowerIndices AtowerStructure::build_subatower(const subATowerIndices& prevtop, float floordelargement, float height)
	{
		ArrayBuffer<glm::vec3>& vertices = this->mesh_vertices();
		// ArrayBuffer<glm::vec3>& colors = this->mesh_colors();
		ElementBuffer& elements   = this->get_elements();

		#ifdef MOTOR_DEBUG
		float maxrad = geom::cycle_conspace_maxdelargement(vertices.subset(prevtop.nextop));
		if(maxrad < floordelargement + _barriersize.x)
		{
			mot::err("AtowerStructure::build_subatower(): too big delargement requied :\nmaxrad < floordelargement + _barriersize.x :\n"
				+ std::to_string(floordelargement) + " > "
				+ std::to_string(floordelargement)
				+ std::to_string(_barriersize.x));
		}
		#endif

		subATowerIndices ret;

		std::vector<uint> backbot = utils::uirange(meshbuild::conspace_enlarge_cycle(
			vertices,
			elements,
			prevtop.nextop,
			-floordelargement,
			vvv::y()*0.0f,
			true,
			_barriersize.x*10.0f,
			false
		));
		this->complete_color_buffer(this->walls_color()*baseshadcoeff);

		// SpaceGrid s(geom::projplan(vertices.subset(prevtop.nextop)), 500, 3.0f);
		// s.set_empty_polygon(geom::projplan(vertices.subset(backbot)), 3);
		// s.print_ppm("wtf.ppm");

		std::vector<uint> doors_pos = this->select_doors_edge(vertices.subset(backbot), height);

		partialCycle<uint> pbackbot = partialCycle(backbot,  doors_pos);
		partialEnlargement<uint> bot = meshbuild::place_conspace_enlargement(
			vertices,
			pbackbot,
			_rebortsize.x,
			_rebortsize.x
		);
		partialEnlargement<uint> top = meshbuild::extrude_cycle(vertices, elements, bot, vvv::y()*_rebortsize.y, true);

		if(top.faces.size() > 0)
		{
			meshbuild::fill_cycles(vertices, elements, top, true);
			for(const auto& f : top.faces)
			{
				geoCylinderInfo cyl = {
					.cycle = vertices.subset(f),
					.sized_direction = -vvv::y()*_rebortsize.y
				};
				this->add_hitbox<GeoCylinderHitbox>(cyl);
			}
		}
		else
		{
			vector<uint> botup = utils::uirange(meshbuild::copytranslate(vertices, backbot, vvv::y()*_barriersize.y));
			meshbuild::fill_rubban(vertices, elements, top.outline.cycle, botup, true);
			
			wallInfo wf = {
				vertices.subset(top.outline.cycle),
				vertices.subset(botup),
				-vvv::y()*_rebortsize.y
			};
			this->add_hitbox<WallHitbox>(wf);
		}
		
		wallInfo wall = {
			vertices.subset(prevtop.nextop),
			vertices.subset(bot.outline.cycle),
			-vvv::y()*_groundthick
		};	
		this->add_hitbox(std::make_unique<WallHitbox>(wall));


		// SpaceGrid s(geom::projplan(vertices.subset(prevtop.nextop)), 500, 3.0f);
		// s.set_empty_polygon(geom::projplan(vertices.subset(backbot)), 3);
		// s.set_empty_polygon(geom::projplan(vertices.subset(outline.cycle)), 2);
		// s.print_ppm("outline.ppm");


		// utils::print_vector(vertices.subset(outline.cycle));		

		meshbuild::fill_rubban(vertices, elements, prevtop.nextop, bot.outline.cycle, true);

		partialCycle<uint> hightop  = meshbuild::extrude_cycle(vertices, elements, pbackbot, vvv::y()*(height), true);
		this->complete_color_buffer(this->walls_color());

		std::vector<uint> highside = utils::uirange(meshbuild::conspace_enlarge_cycle(vertices, elements, hightop.cycle, _barriersize.x, vec3(0), true, _barriersize.x, true));
		this->complete_color_buffer(this->walls_color()*0.89f);
		
		std::vector<uint> highex = utils::uirange(meshbuild::extrude_cycle(vertices, elements, highside, vvv::y()*_barriersize.y, true));
		this->complete_color_buffer(this->walls_color());
		
		std::vector<uint> highin =  utils::uirange(meshbuild::extrude_cycle(vertices, elements, hightop.cycle, vvv::y()*_barriersize.y, false));
		this->complete_color_buffer(this->walls_color());
		meshbuild::fill_rubban(vertices, elements, highex, highin, true);

		wall = {
			vertices.subset(highex),
			vertices.subset(highin),
			-vvv::y()*_barriersize.y
		};	
		this->add_hitbox(std::make_unique<WallHitbox>(wall));

		partialWallInfo pwall = {
			geom::conspace_enlarge_cycle(
				meshbuild::partial_cycle(vertices, hightop),
				-_wallssize,
				_wallssize
				),
			-vvv::y()*(height+_barriersize.y)
		};	
		this->add_hitbox(std::make_unique<PartialWallHitbox>(pwall));
		
		this->put_subatower_door(pbackbot, hightop);
		this->put_subatower_pilers(pbackbot, hightop);

		ret.nextop = hightop.cycle;
		return ret;
	}
};
