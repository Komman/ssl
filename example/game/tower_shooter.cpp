#include "tower_shooter.hpp"

#include "../motor/physic.hpp"

#include "ingame.hpp"
#include "tower_missile.hpp"
#include "projectiles.hpp"
#include "effects.hpp"
#include "lasting_arrow.hpp"
#include "hitboxed_arrow.hpp"

using namespace std;
using namespace glm;

TowerShooter::TowerShooter(const glm::vec3& position, float range)
	: _position(position),
	  _target(NULL),
	  _signaler(3.0f),
	  _range(range),
	  _reload(0)
{

}

void TowerShooter::set_target(const BasicTarget* target)
{
	_target = target;
}

void TowerShooter::set_range(float new_range)
{
	_range = new_range;
}

void TowerShooter::reload()
{
	_reload = 0.0f;
}

void TowerShooter::animate(float dt)
{
	_reload = std::max(0.0f, _reload - dt);
}

bool TowerShooter::reloadable_shoot(float reload_time)
{
	if(_reload == 0.0f)
	{
		this->shoot();
		_reload = reload_time;

		return true;
	}

	return false;
}

void TowerShooter::shoot()
{
	if(_target == NULL)
	{
		return;
	}

	vec3 totargetflat = _target->position() - _position;
	totargetflat.y = 0;
	totargetflat = geom::safe_normalize(totargetflat);

	vec3 shootpos = _position;

	auto dir = physic::compute_shot_direction(shootpos,
									      	  _target->position(), 
									      	  _range,
									      	  ingame::gravity());
	vec3 shootdirN;

	if(dir.possible)
	{
		// auto arr = std::make_unique<LastingArrow<HitboxedArrow>>(2, arrowStart({shootpos, dir.direct_directionN, 1.0, 0.05f}));
		// projectiles::arrows().launch_simple_trail(std::move(arr), dir.direct_directionN, _range);
		shootdirN = dir.direct_directionN;
	}
	else
	{
		shootdirN = _target->position() - shootpos;
		shootdirN.y = abs(shootdirN.x) + abs(shootdirN.z);
		shootdirN = geom::safe_normalize(shootdirN);
	}

	this->shoot_animation(shootpos, shootdirN);

	float arr_size = utils::random_float(2.0, 6.0f);
	auto arr = std::make_unique<TowerMissile>(towerMissileStart{shootpos, shootdirN, arr_size});

	projectiles::arrows().launch_tower_bomb(std::move(arr), shootdirN, _range);
}

void TowerShooter::shoot_animation(const glm::vec3& shootpos, const glm::vec3& shootdir)
{
	float cspeed = 10.0f;
	effects::add_explosion_light(shootpos, vec3(1.0,0.3,0.2), 20.0f, 0.25f);
	effects::particles_explosion(shootpos, 0.19f, cspeed, vec3(0.9,0.6,0.3), -0.6f, -0.35f, 2.0, 50, 6, shootdir, 0.0f);
	effects::particles_explosion(shootpos, 0.25f, cspeed, vec3(0.1f), 2.0f, 2.0f, 2.0, 10, 6, shootdir, 1.0f);
	effects::particles_explosion(shootpos, 0.3f, cspeed, vec3(0.4f), 2.0f, 2.0f, 2.0, 20, 6, shootdir, 1.0f);
}
