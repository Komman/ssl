#ifndef _CASTLE_CATHEDRAL_STRUCTURE_HPP_
#define _CASTLE_CATHEDRAL_STRUCTURE_HPP_

#include "castle_face_structure.hpp"

namespace CASTLE
{
	class CathedralStructure : public FaceStructure
	{
	public:
		CathedralStructure(const baseBuiling& base, uint face);
	
	protected:
		void build_face(const std::vector<doorWall>& walls_to_build) override;
		void build_wall_deco(const std::vector<doorWall>& walls_to_build) override;

	private:
		float get_over_height() const; 
	};
};


#endif //_CASTLE_CATHEDRAL_STRUCTURE_HPP_
