#include "castle_theorical_architecture.hpp"

#include "../utils/geom.hpp"

#include "../motor/motor_debug.hpp"

#include <iostream>

using namespace std;

namespace CASTLE
{
	TheoricalArchitecture::TheoricalArchitecture(const BorderedGraph& g, const graphHeightParam& params)
		: LabyrinthGraph(g, params),
		  _doors({})
	{
		this->set_doors();
	}

	void TheoricalArchitecture::set_doors()
	{
		auto face_edges = this->all_face_edges();

		for(auto fe : face_edges)
		{
			if(!(this->is_there_door(fe.edge)))
			{
				continue;
			}

			Edge fedge = this->faces_orientation(fe.face_edge.first, fe.face_edge.second);

			uint f1 = fedge.first;
			uint f2 = fedge.second;

			float h1 = this->get_height(f1);
			float h2 = this->get_height(f2);

			if(h2 - h1 < this->geom_params().door_height)
			{
				_doors[fe.edge] = this->random_door(f1, f2, INSIDE);
			}
			else
			{
				if(this->get_structure(f1)->is_roof_open())
				{
					_doors[fe.edge] = this->random_door(f1, f2, OUTSIDE);
				}
				else
				{
					_doors[fe.edge] = this->random_door(f1, f2, INSIDE);
				}
			}
		}
	}

	doorInfo TheoricalArchitecture::get_door_infos(const Edge& e) const
	{
		auto ret = _doors.find(e);

		if(ret == _doors.end())
		{
			ret = _doors.find(Graph::mirror(e));
		}

		if(ret == _doors.end())
		{
			mot::err("TheoricalArchitecture::get_door_infos(): asked for a non-existing edge: " + 
				e.to_string());
		}

		return ret->second;
	}

	doorPlace TheoricalArchitecture::get_door_place(const Edge& e) const
	{
		return this->get_door_infos(e).place;
	}

	doorInfo TheoricalArchitecture::random_door(uint f1, uint f2, doorPlace door_place)
	{
		std::vector<doorType> types = this->get_structure(f1)->get_compatible_doors_type(*(this->get_structure(f2)));
	
		if(types.size() == 0)
		{
			mot::err("TheoricalArchitecture::random_door(): faces "
				+ to_string(f1) + " and " + to_string(f2)
				+ " have no available door type in common");
		}

		return {
			.place  = door_place,
			.type   = types[rand()%types.size()]
		};
	}
};