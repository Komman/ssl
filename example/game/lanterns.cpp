#include "lanterns.hpp"

#include "../utils/geom.hpp"

using namespace glm;

Lanterns::Lanterns(StaticLigthor& slightor)
	: StoredVAObject(STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW),
	  _slightor(slightor),
	  _size(1, 1, 2),
	  _light_color(0),
	  _light_rel_pos(0)
{
	this->build_lantern();
}

uint Lanterns::add_point(const glm::vec3& coord, const glm::vec3 color)
{
	auto& relpos = this->get_array_buffer<RELATIVE_POS>();
	auto& colors = this->get_array_buffer<COLORS>();

	int ret = relpos.size();

	relpos.add_value(coord);
	colors.add_value(color);

	return ret;
}

std::vector<glm::vec3> Lanterns::lanterns_positions() const
{
	const auto& deps = this->get_array_buffer<DEP_SIZE>();

	std::vector<glm::vec3> ret;

	for(uint i=0; i<deps.size(); i++)
	{
		vec4 v = deps[i];
		ret.push_back(vec3(v.x, v.y, v.z));
	}

	return ret;
}


void Lanterns::add_lantern(const lanternDisposition& lantern)
{
	this->add_custom_lantern(lantern,
			      {
			         .color      = lantern.color,
			         .range      = 15.0f * lantern.size,
			         .intensity  = 70.0f * lantern.size,
			      	  .saturation = 2.5f
			      });
}

void Lanterns::add_custom_lantern(const lanternDisposition& lantern, const ligthingInfo& light)
{
	this->get_array_buffer<DEP_SIZE>().add_value(vec4(lantern.position, lantern.size));
	this->get_array_buffer<LIGHT_COLOR>().add_value(lantern.color);
	this->get_array_buffer<DIRECTION_N>().add_value(lantern.directionN);

	_slightor.add_ligth(lantern.position + geom::rotate_redirect(_light_rel_pos*lantern.size/2.0f, vec3(0,0,1), lantern.directionN), light);
}



/*
10     ^13 ^     9

       6   5

11     7   4    8
                
         12
*/

void Lanterns::build_lantern()
{
	auto& indices = this->get_elements();
	auto& relpos  = this->get_array_buffer<RELATIVE_POS>();

	float support_size_coeff  = 0.3f;
	float support_depth_coeff = 0.6f;
	this->build_support(support_size_coeff, support_depth_coeff);
	
	vec3 torch_color = vec3(0.2,0.1,0.05)*1.0f;
	float start_torch_z = (support_depth_coeff-0.5f) * _size.z;
	float mindirection  = std::min(_size.x, _size.y);
	float support_depth = _size.z*support_depth_coeff - _size.z/2.0f;
	float support_size  = mindirection*support_size_coeff/2.0f;

	vec3 top_triangle[3]={
		vec3(- _size.x/2.0f, + _size.y/2.0f, support_depth),
		vec3(+ _size.x/2.0f, + _size.y/2.0f, support_depth),
		vec3(0.0,  _size.y/2.0f, _size.z/2.0f)
	};

	this->add_point(vec3(- _size.x/2.0f, - support_size, support_depth), torch_color*color_random());
	this->add_point(top_triangle[0], torch_color*color_random());
	this->add_point(top_triangle[1], torch_color*color_random());
	this->add_point(vec3(+ _size.x/2.0f, - support_size, support_depth), torch_color*color_random());
	
	this->add_point(vec3(0.0, -_size.y/2.0f, start_torch_z), torch_color*color_random());
	this->add_point(top_triangle[2], torch_color*color_random());

	indices.add_triangle(8,11,12);
	indices.add_rectangle(8,9,10,11);
	indices.add_triangle(10,9,13);

	indices.add_triangle(11, 10, 13);
	indices.add_triangle(9 , 8 , 13);
	indices.add_triangle(12, 11, 13);
	indices.add_triangle(8 , 12, 13);

	float ligth_proportion  = 0.5;
	float ligth_heigth      = _size.z/5.0f;

	vec3 top_triangle_center = (top_triangle[0] + top_triangle[1] + top_triangle[2])/3.0f;

	vec3 ligth_triangle[3];
	for(int i=0; i<3; i++)
	{
		ligth_triangle[i] = glm::mix(top_triangle[i], top_triangle_center, ligth_proportion);
	}

	int start_light = relpos.size();
	for(int i=0; i<3; i++)
	{
		this->add_point(ligth_triangle[i], _light_color);
		_light_rel_pos += relpos[relpos.size()-1];
	}
	for(int i=0; i<3; i++)
	{
		this->add_point(ligth_triangle[i]+vvv::y()*ligth_heigth, _light_color);
		_light_rel_pos += relpos[relpos.size()-1];
	}
	_light_rel_pos /= 6.0f;
	_light_rel_pos += vvv::z()*_size.z*0.8f;

	for(int i=0; i<3; i++)
	{
		indices.add_rectangle(start_light + i,
							   start_light + i       + 3,
							   start_light + (i+1)%3 + 3,
							   start_light + (i+1)%3);
	}
	indices.add_triangle(start_light+3+0,
						  start_light+3+2,
						  start_light+3+1);
}

void Lanterns::build_support(float support_coeff, float support_depth_coeff)
{
	auto& indices = this->get_elements();
	
	float mindirection  = std::min(_size.x, _size.y);
	float support_size  = mindirection*support_coeff/2.0f;
	float support_depth = _size.z*support_depth_coeff - _size.z/2.0f;
	vec3  support_color = vec3(1.0)*0.2f;

	this->add_point(vec3(- support_size, - support_size, 0.0f - _size.z/2.0f), support_color*color_random()/2.0f);
	this->add_point(vec3(- support_size, + support_size, 0.0f - _size.z/2.0f), support_color*color_random()/2.0f);
	this->add_point(vec3(+ support_size, + support_size, 0.0f - _size.z/2.0f), support_color*color_random()/2.0f);
	this->add_point(vec3(+ support_size, - support_size, 0.0f - _size.z/2.0f), support_color*color_random()/2.0f);

	this->add_point(vec3(- support_size, - support_size, support_depth ), support_color*color_random());
	this->add_point(vec3(- support_size, + support_size, support_depth ), support_color*color_random());
	this->add_point(vec3(+ support_size, + support_size, support_depth ), support_color*color_random());
	this->add_point(vec3(+ support_size, - support_size, support_depth ), support_color*color_random());

	indices.add_rectangle(0,1,2,3);
	// indices.add_rectangle(7,6,5,4);

	for(int i=0; i<4; i++)
	{
		indices.add_rectangle(4+i, 4+(5+i)%4, (i+1)%4, i);
	}
}

float Lanterns::color_random()
{
	return utils::random_float(0.5,1.5);
}
