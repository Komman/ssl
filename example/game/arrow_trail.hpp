#ifndef _ARROW_TRAIL_HPP_
#define _ARROW_TRAIL_HPP_

#include <vector>
#include <glm/glm.hpp>
#include "../../ssl/ssl.hpp"

#include "../motor/gutils.hpp"

#include "ingame.hpp"

using namespace ssl;

using trailPlan = std::pair<unsigned int, unsigned int>;

// Supposing the arrow is in vec3(0,0,0) and its directionN vec3(1,0,0),
// - base is the vector of the trail vertices.
// - plans are indices of base designing what plan to link between two pauses.

struct trailConfig
{
	std::vector<glm::vec3> base;
	std::vector<trailPlan> plan;
};

template<typename pointInfo>
class ArrowTrail
{
public:
	
	struct planInfos
	{
		std::vector<unsigned int> vertices_idx;
		glm::vec3 center;
		glm::vec3 directionN;
		float     distance;
		pointInfo infos;
	};

public:
	// Supposing the arrow is in vec3(0,0,0) and its directionN vec3(1,0,0),
	// - trail_base is the vector of the trail vertices.
	// - plans are indices of trail_base designing what plan to link between two pauses.
	ArrowTrail(const std::vector<glm::vec3>& trail_base,
			   const std::vector<trailPlan>& plans, 
			   float cosangle_sensibility);
	virtual ~ArrowTrail() {}

	// void clear();

	// Returns if a new trail point has been estalished
	bool set_trail(const glm::vec3& position, const glm::vec3& directionN, const pointInfo& infos);

	unsigned int size() const;

	const std::vector<planInfos>&    get_trail()       const;
	const ArrayBuffer<glm::vec3>&    get_vertices()    const;
	const ArrayBuffer<float>&        get_start_times() const;
	const ElementBuffer&             get_indices()     const;

	void clear();
	
	// Clear the part of the trail that is before "time"
	void shorten(float time);

private:
	ArrowTrail(const ArrowTrail&);
	ArrowTrail& operator=(const ArrowTrail&);

	void add_vertex(const glm::vec3& vertex, float time);
	void pop_vertex();
	void add_plan(const glm::vec3& position, const glm::vec3& directionN, const pointInfo& infos, float time);
	void pop_plan();
	std::vector<glm::vec3> get_plan(const glm::vec3& position, const glm::vec3& directionN);
	void cut_indices_begin(unsigned int first_kept_index);

private:

	std::vector<glm::vec3> _trail_base;
	std::vector<trailPlan> _plans;
	float _cosangle_sensibility;

	float _total_distance;

	ArrayBuffer<glm::vec3> _vertices;
	ArrayBuffer<float>     _start_time;
	ElementBuffer          _indices;

	std::vector<planInfos> _full_trail_info;
};




template<typename pointInfo>
ArrowTrail<pointInfo>::ArrowTrail(const std::vector<glm::vec3>& trail_base,
							      const std::vector<trailPlan>& plans,
							      float cosangle_sensibility)
	: _trail_base(trail_base),
	  _plans(plans),
	  _cosangle_sensibility(cosangle_sensibility),
	  _total_distance(0.0),
	  _vertices(DYNAMIC_DRAW),
	  _start_time(DYNAMIC_DRAW),
	  _indices(DYNAMIC_DRAW),
	  _full_trail_info(0)
{
	for(const auto& p : _plans)
	{
		if(std::max(p.first, p.second) >= trail_base.size())
		{
			err("ArrowTrail::ArrowTrail(): plans indices out of bound: " + std::to_string(std::max(p.first, p.second)) + "/"
				+ std::to_string(trail_base.size()));
		}
	}
}

template<typename pointInfo>
void ArrowTrail<pointInfo>::add_vertex(const glm::vec3& vertex, float time)
{
	_vertices.add_value(vertex);
	_start_time.add_value(time);
}


template<typename pointInfo>
void ArrowTrail<pointInfo>::pop_vertex()
{
	_vertices.pop_value();
	_start_time.pop_value();
}


template<typename pointInfo>
bool ArrowTrail<pointInfo>::set_trail(const glm::vec3& position, const glm::vec3& directionN, const pointInfo& infos)
{
	bool trail_to_update = true;

	if(_full_trail_info.size() > 1)
	{
		_total_distance+=glm::length(_full_trail_info[_full_trail_info.size()-1].center - position);
	
		const auto& last_dir = _full_trail_info[_full_trail_info.size()-2].directionN;
		if(glm::dot(last_dir, directionN) > _cosangle_sensibility)
		{
			trail_to_update = false;
		}
	}
	else
	{
		this->add_plan(position, directionN, infos, -1.0f);
	}

	if(trail_to_update)
	{
		this->add_plan(position, directionN, infos, ingame::gtime());
	}
	else
	{
		this->pop_plan();
		this->add_plan(position, directionN, infos, ingame::gtime());
	}

	// std::cout<<_indices.size()<<std::endl;
	// std::cout<<_vertices.size()<<std::endl;

	return trail_to_update;
}

template<typename pointInfo>
void ArrowTrail<pointInfo>::add_plan(const glm::vec3& position, const glm::vec3& directionN, const pointInfo& infos, float time)
{
	planInfos new_plan;
	new_plan.vertices_idx.resize(0);
	
	auto plan = this->get_plan(position, directionN);

	for(int i=0; i<(int)plan.size(); i++)
	{
		new_plan.vertices_idx.push_back(_vertices.size());
		this->add_vertex(plan[i], time);
	}

	if(_full_trail_info.size() > 0)
	{
		const auto& prev_indices = _full_trail_info[_full_trail_info.size()-1].vertices_idx;
		const auto& curr_indices = new_plan.vertices_idx;

		for(const auto& link : _plans)
		{
			_indices.add_rectangle(
				prev_indices[0] + link.first,
				prev_indices[0] + link.second,
				curr_indices[0] + link.second,
				curr_indices[0] + link.first
				);
		}
	}

	new_plan.center     = position;
	new_plan.directionN = directionN;
	new_plan.distance   = _total_distance;
	new_plan.infos      = infos;

	_full_trail_info.push_back(new_plan);
}

template<typename pointInfo>
void ArrowTrail<pointInfo>::pop_plan()
{
	for(int i=0; i<(int)_trail_base.size(); i++)
	{
		this->pop_vertex();
	}
	for(int i=0; i<(int)_plans.size(); i++)
	{
		_indices.pop_rectangle();
	}

	_full_trail_info.pop_back();
}

template<typename pointInfo>
void ArrowTrail<pointInfo>::clear()
{
	_vertices.clear();
	_start_time.clear();
	_indices.clear();

	_vertices.instant_update_GPU();
	_start_time.instant_update_GPU();
	_indices.instant_update_GPU();

	_full_trail_info.clear();
}

template<typename pointInfo>
void ArrowTrail<pointInfo>::shorten(float time)
{
	unsigned int indices_in_rectangle = 6;
	
	unsigned int index_to_cut=0;
	unsigned int j;

	for(unsigned int i=0; i<_indices.size(); i+=indices_in_rectangle)
	{
		index_to_cut = i;

		for(j=0; j<indices_in_rectangle; j++)
		{
			if(_start_time[_indices[i+j]] > time)
			{
				break;
			}
		}

		if(j != indices_in_rectangle)
		{
			break;
		}
	}

	if(index_to_cut == 0)
	{
		return;
	}

	// std::cout<<"cuting "<<index_to_cut<<" on size "<<_indices.size()<<std::endl;

	if(_indices.size() - index_to_cut >= 2*indices_in_rectangle)
	{
		this->cut_indices_begin(index_to_cut);
	}
	else
	{
		if(index_to_cut >= 2*indices_in_rectangle)
		{
			this->cut_indices_begin(index_to_cut-2*indices_in_rectangle);
		}
	}
}

template<typename pointInfo>
void ArrowTrail<pointInfo>::cut_indices_begin(unsigned int first_kept_index)
{
	if(first_kept_index == 0)
		return;

	auto first = _indices.get_value().cbegin() + first_kept_index;
	auto nlast = _indices.get_value().cend();



	_indices.change_value(std::vector<unsigned int>(first, nlast));
}


template<typename pointInfo>
std::vector<glm::vec3> ArrowTrail<pointInfo>::get_plan(const glm::vec3& position, const glm::vec3& directionN)
{
	std::vector<glm::vec3> ret;

	for(const auto& dep : _trail_base)
	{
		ret.push_back(position+utils::rotate_redirect(dep, glm::vec3(1.0,0.0,0.0), directionN));
	}

	return ret;
}

template<typename pointInfo>
const std::vector<typename ArrowTrail<pointInfo>::planInfos>& ArrowTrail<pointInfo>::get_trail() const
{
	return _full_trail_info;
}

template<typename pointInfo>
const ArrayBuffer<glm::vec3>& ArrowTrail<pointInfo>::get_vertices() const
{
	return _vertices;
}

template<typename pointInfo>
const ArrayBuffer<float>& ArrowTrail<pointInfo>::get_start_times() const
{
	return _start_time;
}

template<typename pointInfo>
const ElementBuffer& ArrowTrail<pointInfo>::get_indices() const
{
	return _indices;
}

template<typename pointInfo>
unsigned int ArrowTrail<pointInfo>::size() const
{
	return _full_trail_info.size();
}


#endif //_ARROW_TRAIL_HPP_
