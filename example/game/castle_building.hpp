#ifndef _CASTLE_BUILDING_HPP_
#define _CASTLE_BUILDING_HPP_

#include "../motor/physical_instances.hpp"
#include "../motor/planar_building.hpp"
#include "../motor/motor_debug.hpp"

struct castleGridPlacing
{
	std::vector<valuedCircleInfo>    circles;
	std::vector<valuedPolygon2DInfo> polygons;
};

class CastleBuilding
{
public:
	static constexpr int UP_AVAILABLE = 9;
	static constexpr int FIRST_UP     = 10;

	struct genParam
	{
		glm::vec2& position;
		glm::vec2& dirN;
		float      maxsize;
	};

public:
	CastleBuilding(const castleGridPlacing& grid_draw);

	virtual void generate(const genParam& g) =0;
	virtual planarPlace get_generated_place() const =0;

	virtual void place(SpaceGrid& spacegrid, float h) =0;
	virtual void place_building_only(float h) =0;

	void place_grid_only(SpaceGrid& spacegrid, const planarPlace& place);
	bool is_placable(SpaceGrid& spacegrid);

	PlanarBuilding& get_planar_building();
	bool expose_roof() const;

protected:
	bool is_placable(SpaceGrid& spacegrid, const planarPlace& place);

private:
	PlanarBuilding _planarb;

	bool _exposing_roof;
};


#endif //_CASTLE_BUILDING_HPP_
