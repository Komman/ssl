#ifndef _INGAME_HPP_
#define _INGAME_HPP_

#include "../motor/game_drawer.hpp"
#include "../motor/shared_ressources.hpp"
#include "../motor/mousekey_switcher.hpp"
#include "../motor/mouse_joystick_mktarget.hpp"

namespace ingame
{
	/*
		Only called by main
	*/
	void init();
	void free();
	void interframe(float dt);
	void set_camera_position_ptr(const glm::vec3* pos_ptr);

	MousekeySwitcher& mk_switcher(); 
	MouseJoystickMKTarget* game_mouse_key(); 

	bool game_key_pressed(int key);


	/*
		Global variables
	*/
	float gtime();
	float gravity();

	const glm::vec3& camera_position();
};

#endif //_INGAME_HPP_
