#ifndef _SIMPLE_ARROW_TRAIL_PACK_HPP_
#define _SIMPLE_ARROW_TRAIL_PACK_HPP_

#include "simple_arrow_pack.hpp"

class SimpleArrowTrailPack : public SimpleArrowPack
{
public:
	SimpleArrowTrailPack();
	virtual ~SimpleArrowTrailPack() {}

	void draw(GameDrawer& game_drawer) override;

	const std::vector<std::unique_ptr<ArrowTrail<float>>>& get_trails() const;

	static inline std::string TRAIL_LAST_TIME_UNIFORM_NAME = "trail_last_time";
	static void init_trail_last_time();

	GETTER_BUILDER(float, trail_last_time)

protected:
	/*
		Implement a vector
	*/  
	void pre_add_arrow(const Arrow& arrow,
					   const simpleArrowGeometry& look) override;
	void post_set_arrow(unsigned int index,
		  	   			const Arrow& arrow,
		  	   			const simpleArrowGeometry& look) override;
	void pre_pop_last_arrow() override;
	void pre_clear() override;

	void move_last_arrow(unsigned int dst_index);


	static std::unique_ptr<Uniform<float>> trail_last_time;

private:
	std::vector<std::unique_ptr<ArrowTrail<float>>> _trails;
	std::vector<glm::vec3> _last_backpos;

	float _trail_last_time;
};

#endif //_SIMPLE_ARROW_TRAIL_PACK_HPP_
