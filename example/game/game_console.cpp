#include "game_console.hpp"
#include "effects.hpp"

using namespace std;
using namespace glm;

GameConsole::GameConsole(ConsoleObjects& objs, MousekeySwitcher* switcher, int write_key, float lasting)
	: WindowConsole(switcher, write_key, lasting),
	  _objs(objs)
{

}

using checkTypeFunc = bool(*)(const std::string&);

static const checkTypeFunc CHECK_FUNCTIONS[GameConsole::GAME_TYPES_AMOUT] = {
	NULL,
	utils::is_float,
	utils::is_vec3,
	utils::is_int,
};

static inline const std::string TYPES_STRING[GameConsole::GAME_TYPES_AMOUT] = {
	"NOTYPE",
	"float",
	"vec3",
	"int",
};

void GameConsole::todo(const std::string& msg)
{
	this->send_msg(TERM::CYAN + "TODO: " + TERM::NOCOL + msg);
}

std::string GameConsole::check(const std::vector<std::string>& msg)
{
	for(uint i=0; i<GAME_COMMANDS_AMOUNT; i++)
	{
		if(msg[0] == GCOMMANDS[i].command)
		{
			if(GCOMMANDS[i].arg_number >= 0 && (int)msg.size() != GCOMMANDS[i].arg_number)
			{
				return error("Incorrect argument number for command " + TERM::colored(GCOMMANDS[i].command, TERM::ORANGE)
				       + ", expected " + to_string(GCOMMANDS[i].arg_number) + " and get " + to_string(msg.size()));
			}
			if(GCOMMANDS[i].type != NO_TYPE && GCOMMANDS[i].arg_number <2)
			{
				mot::err("GameConsole::response(): a command has a type and less than 2 arguments");
			}
			if(GCOMMANDS[i].type != NO_TYPE)
			{
				if(!(CHECK_FUNCTIONS[GCOMMANDS[i].type](msg[1])))
				{
					return error("Unrecognize type, expected " + TERM::colored(TYPES_STRING[GCOMMANDS[i].type], TERM::CYAN) +  " for command " + TERM::colored(GCOMMANDS[i].command, TERM::BLUE));
				}
			}
		}
	}

	return "";
}

std::string GameConsole::response(const std::vector<std::string>& msg)
{
	string ret = this->WindowConsole::response(msg);
	ret = (ret == "") ? (this->check(msg)) : ret;

	if(ret != "" || msg.size() == 0)
	{
		return ret;
	}

	if(msg[0] == GCOMMANDS[TP].command)
	{
		_objs.archer->tp(utils::tovec3(msg[1]));
	}

	if(msg[0] == GCOMMANDS[MOVE].command)
	{
		_objs.archer->tp(_objs.archer->get_position()+utils::tovec3(msg[1]));
	}

	if(msg[0] == GCOMMANDS[SPEED].command)
	{
		_objs.archer->set_speed(std::stof(msg[1]));
	}

	if(msg[0] == GCOMMANDS[JUMP].command)
	{
		_objs.archer->set_jump_impulse(std::stof(msg[1]));
	}

	if(msg[0] == GCOMMANDS[GAMEMODE].command)
	{
		_objs.archer->gamemode((Player::gameMode)std::stoi(msg[1]));
	}

	if(msg[0] == GCOMMANDS[BLOOM].command)
	{
		bool enable = (std::stoi(msg[1]) == 1);
		
		if(enable)
		{
			_objs.shared_ressources->tools().bloomer.bloom_enable();
		}
		else
		{
			_objs.shared_ressources->tools().bloomer.bloom_disable();
			_objs.shared_ressources->tools().bloomer.clean_textures();
		}
	}

	if(msg[0] == GCOMMANDS[TARGET].command)
	{
		_objs.archer->set_false_target(!(std::stoi(msg[1]) == 1));
	}

	if(msg[0] == GCOMMANDS[UP].command)
	{
		_objs.archer->tp(_objs.archer->get_position()+vvv::y()*std::stof(msg[1]));
	}

	if(msg[0] == GCOMMANDS[DIR].command)
	{
		this->show_vector(vvv::x(), vec4(1.0f, 0.5f, 0.5f, 3.0f));
		this->show_vector(vvv::y(), vec4(0.5f, 1.0f, 0.5f, 3.0f));
		this->show_vector(vvv::z(), vec4(0.5f, 0.5f, 1.0f, 3.0f));
	}

	if(msg[0] == GCOMMANDS[DIRX].command)
	{
		this->show_vector(vvv::x(), vec4(1.0f, 0.5f, 0.5f, 3.0f));
	}

	if(msg[0] == GCOMMANDS[DIRY].command)
	{
		this->show_vector(vvv::y(), vec4(0.5f, 1.0f, 0.5f, 3.0f));
	}

	if(msg[0] == GCOMMANDS[DIRZ].command)
	{
		this->show_vector(vvv::z(), vec4(0.5f, 0.5f, 1.0f, 3.0f));
	}

	if(msg[0] == GCOMMANDS[COLLISIONS].command)
	{
		bool enable = (std::stoi(msg[1]) == 1);

		if(enable)
			_objs.archer->enable_collision();
		else
			_objs.archer->disable_collision();
	}

	return ret;
}

void GameConsole::show_vector(const glm::vec3& dirN, const glm::vec4& particle_color)
{
	float s = 3.0f;
	for(uint i=0; i<150; i++)
	{
		effects::packed_clouds().add_particle(Particle{
			_objs.archer->get_position() + vvv::y()*2.0f, 
			dirN*utils::random_float(0.0f, 10.0f)*s + utils::random_vec3(vec3(-1), vec3(1))*s*0.05f,
			vec3(0),
			particle_color,
			0.5f,
			0.2f,
			4.0f
		});
	}
}

