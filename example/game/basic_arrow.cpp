#include "basic_arrow.hpp"

using namespace std;
using namespace glm;

BasicArrow::BasicArrow(const glm::vec3& position,
			           const glm::vec3& speed,
			           const glm::vec3& acceleration)
	: BasicPhysicalPoint(),
	  _position(position),
	  _directionN((glm::length(speed) > ssl::EPSILON) ? glm::normalize(speed) : vvv::x()),
	  _speed(speed),
	  _acceleration(acceleration),
	  _flying(true),
	  _has_moved(false)
{
	
}

void BasicArrow::animate(float dt)
{
	if(_flying)
	{
		_speed    += _acceleration * dt;
		this->move(_speed * dt);

		if(glm::length(_speed) > ssl::EPSILON)
		{
			this->set_directionN(glm::normalize(_speed));
		}
	}

	_has_moved = false;
}

void BasicArrow::animate(const PhysicalContext& context, float dt)
{
	auto& hbx_context = context.context();

	if(_flying)
	{
		auto iner = hbx_context.impact(this->get_hitbox(), dt);
		if(iner.impact.impact)
		{
			this->plant(iner.impact.point + this->get_directionN()*this->get_head_planted_length(), iner.impact.normalN, this->get_speed());
		}
	}

	this->animate(dt);
	this->update_hitbox();
}

void BasicArrow::plant(const glm::vec3& position, const glm::vec3& normal, const glm::vec3& impact_speed)
{
	this->tp(position);
	
	_flying = false;

	this->planting_reaction(position, normal, impact_speed);
}

void BasicArrow::unplant()
{
	_flying = true;
}

bool BasicArrow::is_planted() const
{
	return !_flying;
}

void BasicArrow::set_position(const glm::vec3& position)
{
	_position = position;
	_has_moved = true;	
}

void BasicArrow::set_acceleration(const glm::vec3& acceleration)
{
	_acceleration = acceleration;
	_has_moved = true;	
}

void BasicArrow::set_directionN(const glm::vec3& directionN)
{
	_directionN = directionN;
	_has_moved = true;	
}

void BasicArrow::set_speed(const glm::vec3& speed)
{
	// this->set_directionN(glm::normalize(speed));
	_speed = speed;
}

bool BasicArrow::has_moved_since_last_animation() const
{
	return _has_moved;
}


void BasicArrow::tp(const glm::vec3& position)
{
	this->set_position(position);
}

void BasicArrow::move(const glm::vec3& dposition)
{
	this->set_position(_position + dposition);
}

void BasicArrow::launch(const glm::vec3& position,
						const glm::vec3& start_speed)
{
	this->tp(position + geom::safe_normalize(start_speed)*this->get_length());
	this->unplant();
	this->set_speed(start_speed);

	this->launching_reaction(position, start_speed);
}


bool BasicArrow::position_difference(const BasicArrow& arrow)
{
	if(   (this->_position   != arrow._position)
	   || (this->_directionN != arrow._directionN))
	{
		return true;
	}
	return false;
}

void BasicArrow::planting_reaction(const glm::vec3& position, const glm::vec3& normal, const glm::vec3& impact_speed)
{
	// vec3 ref = glm::reflect(impact_speed, normal);
	// this->launch(position + geom::safe_normalize(ref)*this->get_length(), ref);
}

void BasicArrow::launching_reaction(const glm::vec3& position, const glm::vec3& speed)
{
	
}


bool BasicArrow::to_destroy()
{
	return this->is_planted();
	// return false;
}

