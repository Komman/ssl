#ifndef _TOWER_SHOOTER_HPP_
#define _TOWER_SHOOTER_HPP_

#include "../motor/regular_signaler.hpp"
#include "../motor/gutils.hpp"
#include "../motor/target.hpp"

class TowerShooter
{
public:
	TowerShooter(const glm::vec3& position, float range);

	GETTER_BUILDER(glm::vec3, position)
	SETTER_BUILDER(glm::vec3, position)

	void set_target(const BasicTarget* target);
	void set_range(float new_range);
	
	void animate(float dt);
	void reload();

	// Return if it has shot
	bool reloadable_shoot(float reload_time);
	void shoot();

private:
	void shoot_animation(const glm::vec3& shootpos, const glm::vec3& shootdir);

private:
	glm::vec3 _position;

	BasicTarget const* _target;
	RegularSignaler _signaler;

	float _range;
	float _reload;
};

#endif //_TOWER_SHOOTER_HPP_
