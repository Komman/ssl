#ifndef _CASTLE_BUILDING_INST_HPP_
#define _CASTLE_BUILDING_INST_HPP_

#include "../motor/stored_vao_object.hpp"

#include "castle_building_placer.hpp"

template<typename BuildingInfo, uint FIRST_INSTANCE_BUFFER, typename... Types>
class CastleBuildingInst : public StoredVAObject<PhysicalInstances<FIRST_INSTANCE_BUFFER, Types...>>, public CastleBuildingPlacer<BuildingInfo>
{
public:
	template<typename Type>
	struct storedDrawType
	{
		using type = ssl::draw_type;
	};

public:
	CastleBuildingInst(HitboxContext& hbx_context,
	   const castleGridPlacing& grid_draw,
	   ssl::draw_type element_draw_type,
	   typename storedDrawType<Types>::type ... buffers_draw_type);
};





template<typename BuildingInfo, uint FIRST_INSTANCE_BUFFER, typename... Types>
CastleBuildingInst<BuildingInfo, FIRST_INSTANCE_BUFFER, Types...>::CastleBuildingInst(HitboxContext& hbx_context,
	const castleGridPlacing& grid_draw,
	ssl::draw_type element_draw_type,
	typename storedDrawType<Types>::type ... buffers_draw_type)
	: StoredVAObject<PhysicalInstances<FIRST_INSTANCE_BUFFER, Types...>>(hbx_context, element_draw_type, buffers_draw_type...),
	  CastleBuildingPlacer<BuildingInfo>(grid_draw)
{

}


#endif //_CASTLE_BUILDING_INST_HPP_
