#include "ingame.hpp"

#include "../motor/motor_debug.hpp"

using namespace std;
using namespace glm;

namespace ingame
{

	static double _precise_time = 0.0;
	static float  _gravity      = -9.81;

	static glm::vec3 const* _camera_position_ptr = NULL;

	static std::unique_ptr<Uniform<float>> _time             = NULL;
	static std::unique_ptr<Uniform<float>> _view_aspect      = NULL;
	static std::unique_ptr<Uniform<float>> _view_field       = NULL;

	static std::unique_ptr<MousekeySwitcher>      _mkswitcher = NULL;
	static std::unique_ptr<MouseJoystickMKTarget> _gamemk     = NULL;
	
	void set_camera_position_ptr(const glm::vec3* pos_ptr)
	{
		_camera_position_ptr = pos_ptr;
	}

	void init()
	{
		_precise_time = 0.0;
		_time = std::make_unique<Uniform<float>>("time", 0.0);
		_view_aspect = std::make_unique<Uniform<float>>("view_aspect", (float)(window::size().x) / (float)(window::size().y));
		_view_field = std::make_unique<Uniform<float>>("view_field" , DEFAULT::VIEW_FIELD);
		
		_mkswitcher = std::make_unique<MousekeySwitcher>();
		_gamemk     = std::make_unique<MouseJoystickMKTarget>((_mkswitcher->get_switch_ptr()));
		_mkswitcher->target_switch(_gamemk.get());
	}

	void free()
	{
		_time.reset();
		_view_aspect.reset();
		_view_field.reset();
	}

	void interframe(float dt)
	{
		_precise_time += (double)dt;
		(*_time) = (float)(_precise_time);

		window::flip();
	}




	float gtime()
	{
		return _time->get_value();
	}

	float gravity()
	{
		return _gravity;
	}

	MouseJoystickMKTarget* game_mouse_key()
	{
		return _gamemk.get();
	} 

	MousekeySwitcher&      mk_switcher()
	{
		return *_mkswitcher;
	}

	bool game_key_pressed(int key)
	{
		return _gamemk->key_pressed(key);
	}

	const glm::vec3& camera_position()
	{
		if(_camera_position_ptr == NULL)
		{
			mot::err("ingame::camera_position(): camera position not renseigned");
		}

		return *_camera_position_ptr;
	}	
};