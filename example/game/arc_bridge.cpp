#include "arc_bridge.hpp"

#include "../utils/geom.hpp"
#include "../motor/hitbox_fusion.hpp"
#include "../motor/bevel_hitbox.hpp"
#include "../motor/paver_hitbox.hpp"

#include "castle_colors.hpp"

using namespace glm;


ArcBridge::ArcBridge(HitboxContext& context)
	: StoredVAObject(context, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW,STATIC_DRAW),
	 _barrier_coeff(0.02),
	 _margin(0.1),
	 _underbridge(0.1),
	 _arc_discr(8),
	 _color(castle_colors::BRIDGE()
)
{
	this->build_base();
}

void ArcBridge::add_bridge(const arcBridge& bridge)
{
	this->get_array_buffer<START_HIGHT>().add_value(vec4(bridge.start, bridge.size.y));
	this->get_array_buffer<ORIENTATION2D_N_SIZEXY>().add_value(vec4(bridge.direction2DN.x,
																	bridge.direction2DN.y,
																	bridge.size.x,
																	bridge.size.z));

	this->create_hitbox(bridge);
}

void ArcBridge::add_doored_bridge(const arcBridge& bridge, ArcDoor& doors)
{
	this->add_bridge(bridge);

	vec3 doorsizecoeffs = vec3(0.9,1.2f,0.4);

	doors.add_door({
		.pos          = bridge.start,
		.dir2D        = bridge.direction2DN,
		.size         = vec3(bridge.size.x)*doorsizecoeffs,
		.wall_color   = _color
	});

	doors.add_door({
		.pos          = bridge.start + geom::unprojplan(bridge.direction2DN)*bridge.size.z,
		.dir2D        = -bridge.direction2DN,
		.size         = vec3(bridge.size.x)*doorsizecoeffs,
		.wall_color   = _color
	});
}

void ArcBridge::add_oriented_doored_bridge(const arcBridge& bridge, ArcDoor& doors)
{
	this->add_bridge(bridge);

	vec3 doorsizecoeffs = vec3(0.9,1.2f,0.4);

	doors.add_door({
		.pos          = bridge.start,
		.dir2D        = bridge.direction2DN,
		.size         = vec3(bridge.size.x)*doorsizecoeffs,
		.wall_color   = _color
	});
}


void ArcBridge::create_hitbox(const arcBridge& bridge)
{
	uint hbx_disc = 4;
	vec3 dirzN = vec3(bridge.direction2DN.x, 0, bridge.direction2DN.y);
	vec3 dirxN = glm::cross(dirzN, vvv::y());
	vec3 H0 = bridge.start - dirxN*bridge.size.x/2.0f;
	vec3 H1 = bridge.start - dirxN*bridge.size.x/2.0f + dirzN*bridge.size.z;
	vec3 O0 = H0 - vvv::y()*bridge.size.y*(1.0f - _barrier_coeff);
	vec3 O1 = H1 - vvv::y()*bridge.size.y*(1.0f - _barrier_coeff);
	vec3 HM = bridge.start - dirxN*bridge.size.x/2.0f + dirzN*bridge.size.z/2.0f - vvv::y()*_underbridge*bridge.size.y;
	std::vector<vec3> U;
	float dangle = float(M_PI)/float(2*hbx_disc);
	for(uint i=0; i<=2*hbx_disc; i++)
	{
		vec3  center  = (O0+O1)/2.0f;
		float zradius = (0.5f - _margin)*bridge.size.z;
		U.push_back(geom::circle_point(
			center,
			-dirzN,
			-dirxN,
			float(i)*dangle,
			zradius
		));

		U[i].y = center.y + (U[i].y - center.y)/zradius*bridge.size.y*(1.0f - _barrier_coeff - _underbridge);
	}
	U[hbx_disc] = HM;
	auto total_hbx = std::make_unique<HitboxFusion>();

	total_hbx->fusion_hitbox<PaverHitbox>(paverInfo{
		.bottom  = H0,
		.sized_x = dirxN*bridge.size.x*_barrier_coeff*2.0f,
		.sized_y = vvv::y()*bridge.size.y*_barrier_coeff,
		.sized_z = dirzN*bridge.size.z
	});
	total_hbx->fusion_hitbox<PaverHitbox>(paverInfo{
		.bottom  = bridge.start + dirxN*bridge.size.x/2.0f,
		.sized_x = -dirxN*bridge.size.x*_barrier_coeff*2.0f,
		.sized_y = vvv::y()*bridge.size.y*_barrier_coeff,
		.sized_z = dirzN*bridge.size.z
	});
	total_hbx->fusion_hitbox<BevelHitbox>(bevelShape{
		.triangle_base   = {H0, H1, HM},
		.sized_direction = dirxN*bridge.size.x
	});

	auto underarc_hbx = std::make_unique<HitboxFusion>();
	underarc_hbx->fusion_hitbox<BevelHitbox>(bevelShape{
		.triangle_base   = {H0, O0, U[0]},
		.sized_direction = dirxN*bridge.size.x
	});
	underarc_hbx->fusion_hitbox<BevelHitbox>(bevelShape{
		.triangle_base   = {H1, O1, U[U.size()-1]},
		.sized_direction = dirxN*bridge.size.x
	});
	for(uint i=0; i<hbx_disc; i++)
	{
		underarc_hbx->fusion_hitbox<BevelHitbox>(bevelShape{
			.triangle_base   = {H0, U[i], U[i+1]},
			.sized_direction = dirxN*bridge.size.x
		});
		underarc_hbx->fusion_hitbox<BevelHitbox>(bevelShape{
			.triangle_base   = {H1, U[U.size()-1-i], U[U.size()-2-i]},
			.sized_direction = dirxN*bridge.size.x
		});
	}

	total_hbx->fusion_hitbox(std::move(underarc_hbx));
	_hitboxes.push_back(std::move(total_hbx));
	_hbx_context->add_hitbox(_hitboxes[_hitboxes.size()-1].get());
}

uint ArcBridge::place_symmetrical(SymmetryType& sym, const glm::vec2& sideYZ, const glm::vec3& color)
{
	return sym.place_symmetrical(vec3(0.5, sideYZ.x, sideYZ.y), color);
}


/*
          B[3] --------------------------------- B[2]
         /                                    /
        O[3] ----------------------------- O[2]
        |                                    | 
        |                                    |
        | B[0] --------------------------------- B[1]
        |/                                   |/ 
        H0----------------------------------H1      ^
        |                                    |      |  _underbridge
        |            _____HM_______    _   _ | _   _V
        |          --              --        |      
        |         /                  \       | 
  Y     |       /                     \      | 
  ^     |      |                       |     | 
  |     |     |                         |    | 
  |    O[0]   U[0]       U[2*_arc_discr+1]  O[1]
  |  X   <----> _margin
  | / 
   --------> Z


*/

void ArcBridge::build_base()
{
	auto& relpos = this->get_array_buffer<RELATIVE_POS>();
	auto& colors = this->get_array_buffer<COLORS>();
	auto& elements = this->get_elements();

	SymmetryType sym({vec3(0), vec3(1,0,0)},
		&relpos,
		&elements,
		&colors
	);

	float dark_down_barrier = 0.6;

	uvec4 O;
	O[0] = this->place_symmetrical(sym, vec2(-1.0f+_barrier_coeff, 0), _color*0.7f);
	O[1] = this->place_symmetrical(sym, vec2(-1.0f+_barrier_coeff, 1), _color*0.7f);
	O[2] = this->place_symmetrical(sym, vec2(_barrier_coeff, 1)      , _color);
	O[3] = this->place_symmetrical(sym, vec2(_barrier_coeff, 0)      , _color);

	uvec4 B;
	B[0] = sym.place_symmetrical(vec3(0.5 - _barrier_coeff*2.0f, 0, 0)             , _color*dark_down_barrier);
	B[1] = sym.place_symmetrical(vec3(0.5 - _barrier_coeff*2.0f, 0, 1)             , _color*dark_down_barrier);
	B[2] = sym.place_symmetrical(vec3(0.5 - _barrier_coeff*2.0f, _barrier_coeff, 1), _color);
	B[3] = sym.place_symmetrical(vec3(0.5 - _barrier_coeff*2.0f, _barrier_coeff, 0), _color);

	sym.link_rectangle(B[0], B[1], B[2], B[3]);
	sym.link_rectangle(B[1], B[0], sym.symmetrical(B[0]), sym.symmetrical(B[1]));
	sym.link_rectangle(B[3], B[2], O[2], O[3]);

	uint amount = 2*_arc_discr;
	float anglediv = float(M_PI)/float(amount);

	std::vector<uint> U;
	for(uint i=0; i<=amount; i++)
	{
		vec3 ucenter = vec3(0.5, -1 + _barrier_coeff, 0.5);
		vec3 pos = geom::circle_point(vec3(0), vec3(0,0,-1), vec3(1,0,0), float(i)*anglediv, 1);
		pos.y *= 1.0f - _underbridge - _barrier_coeff;
		pos.z *= 0.5f - _margin;
		pos += ucenter;

		U.push_back(sym.place_symmetrical(pos, _color));
	}

	for(uint i=0; i<U.size()-1; i++)
	{
		uint connector = (i < U.size()/2) ? O[3] : O[2];
		sym.link_triangle(connector, U[i], U[i+1], true);

		sym.link_rectangle(U[i], U[i+1], sym.symmetrical(U[i+1]), sym.symmetrical(U[i]));
	}

	sym.link_triangle(O[3], U[0], O[0]);
	sym.link_triangle(O[2], U[U.size()-1], O[1], true);
	sym.link_triangle(O[3], O[2], U[U.size()/2], false);
	
	sym.link_rectangle(O[0], U[0], sym.symmetrical(U[0]), sym.symmetrical(O[0]));
	sym.link_rectangle(O[1], U[U.size()-1], sym.symmetrical(U[U.size()-1]), sym.symmetrical(O[1]), true);
}
