#ifndef _WORLD_HPP_
#define _WORLD_HPP_

#include <memory>

#include "../motor/drawer_pack_controller.hpp"
#include "../motor/adaptable_drawable_object.hpp"

#include "absolute_objects.hpp"

class BasicWorld
{
public:
	/*
	     -  /!\  NOT THREAD SAFE /!\  -
	*/
	BasicWorld();

	// Returns a pointor to the world where to go.
	// Returns NULL otherwise.
	virtual BasicWorld* animate(AbsoluteObjects& objs, float dt) =0;
	
	void draw(AbsoluteObjects& objs);

	/*
		far_gate_draw() and close_gate_draw() draw something on
		framebuffers that are not multisampled.
		The bloom and other texture may be not empty when the
		draws are called.
	*/

	// Draw something on this coordinates
	virtual void far_gate_draw(const ElementBuffer& indices,
						 	   const ArrayBuffer<glm::vec3>& vertices)=0;
	// Draw the close gate where the stencil value is mask_value
	virtual void close_gate_draw(GLuint mask_value)=0;

	// Returns the amont of required gates for this world
	unsigned int total_gates_amount();
	// Returns the amount of gates that must be linked
	unsigned int remaining_gates();
	// Returns the amount of gates that are linked
	unsigned int linked_gates();
	// Link a gate to an other world
	void link_gate_out(BasicWorld* gate_world);

	// Link a gate of both worlds from the one to the other
	static void link_two_worlds(BasicWorld* w1, BasicWorld* w2);

protected:

	// Add a gate that must be linked to a wolrd
	void require_gate();
	// Get the wolrld on which the gate is linked
	BasicWorld* get_gate_linked_world(unsigned int gate_index);

	// Draw the world
	virtual void draw_world(AbsoluteObjects& objs) =0;


protected:
	static std::unique_ptr<Drawer> static_gate_identifior;
	static std::unique_ptr<Drawer> static_gate_holer;

private:
	unsigned int _required_gates;
	std::vector<BasicWorld*> _linked_gates;
};


class ControlledDrawsWorld : public BasicWorld
{
public:
	ControlledDrawsWorld();

	DrawerPackController& drawer_controller();

private:
	DrawerPackController _drawers_controller;
};

#endif //_WORLD_HPP_
