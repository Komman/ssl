#ifndef _SIMPLE_ARROW_PACK_HPP_
#define _SIMPLE_ARROW_PACK_HPP_

#include <vector>

#include "../../ssl/src/vertex_array_object.hpp"

#include "../motor/gutils.hpp"
#include "../motor/shared_ressources.hpp"

#include "arrow_pack.hpp"
#include "arrow.hpp"
#include "arrow_trail.hpp"


struct simpleArrowGeometry
{
	float head_coeff;
	glm::vec3 shaft_color;
	glm::vec3 head_color;

};	

class SimpleArrowPack : public ArrowPack<simpleArrowGeometry>
{
public:
	using arrowId = unsigned int;

public:
	SimpleArrowPack(); /* NOT THREAD SAFE 
	(but easy to make it thread safe with a extern initializer)*/
	
	virtual ~SimpleArrowPack() {}

	const VertexArrayObject& get_vao() const;
	
	/*
		NOT THREAD SAFE
	*/
	virtual void draw(GameDrawer& game_drawer); 

protected:
	static std::unique_ptr<Uniform<int>> global_first_head_index;
	static std::unique_ptr<Uniform<int>> global_first_feather_index;
	static std::unique_ptr<Drawer>       drawer;

protected:
	/*
		Implement a vector
	*/  
	virtual void pre_add_arrow(const Arrow& arrow,
							   const simpleArrowGeometry& look) override;
	virtual void post_set_arrow(unsigned int index,
		  						const Arrow& arrow,
		  						const simpleArrowGeometry& look) override;
	virtual void pre_pop_last_arrow() override;
	virtual void pre_clear() override;
	

	const trailConfig& get_fether_config(unsigned int index) const; 

private:
	void build_vertexs();
	void build_shaft();
	void build_feathers();
	void build_head();
	void init_static_members();
	void update_arrow_GPU(unsigned int index,
						  const Arrow& arrow,
						  const simpleArrowGeometry& look);
	trailConfig get_arrow_trail_config(const Arrow& arr) const;

	void check_sizes() const;

private:
	ElementBuffer _indices;
	ArrayBuffer<glm::vec3> _vertexs; // from (0,0,0) to (1,0,0)
	ArrayBuffer<glm::vec3> _colormasks;

	// Instances
	ArrayBuffer<glm::vec3> _backpos;
	ArrayBuffer<glm::vec3> _directionsN;
	ArrayBuffer<glm::vec3> _color_shaft;
	ArrayBuffer<glm::vec3> _color_head;
	ArrayBuffer<glm::vec3> _geom_params; //x: length, y: size, z: head_coeff

	VertexArrayObject _vao;

	trailConfig _default_trail_config;
	std::vector<trailConfig> _trails_configs;

	int _first_head_index;
	int _first_feather_index;

	glm::vec2 _feather_inter; //between 0 and 1
};

#endif //_SIMPLE_ARROW_PACK_HPP_
