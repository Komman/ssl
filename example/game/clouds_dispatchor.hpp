#ifndef _CLOUDS_DISPATCHOR_HPP_
#define _CLOUDS_DISPATCHOR_HPP_

#include "../motor/packed_particle_dispatchor.hpp"
#include "../motor/game_drawer.hpp"


using namespace ssl;

class CloudsDispatchor : public ParticleDispatchor
{
public:
	CloudsDispatchor(ShaderGenerator& generator);
	virtual ~CloudsDispatchor() {}

	void draw(GameDrawer& game_drawer, bool use_vao = false);
	const VertexArrayObject& get_vao() const;

private:
	Drawer _drawer;
	VertexArrayObject _vao;
};


class PackedCloudsDispatchor : public PackedParticleDispatchor
{
public:
	PackedCloudsDispatchor(ShaderGenerator& generator, const std::string& name);
	virtual ~PackedCloudsDispatchor() {}

	void draw(GameDrawer& game_drawer, bool use_vao = false);
	const VertexArrayObject& get_vao() const;

private:
	Drawer _drawer;
	VertexArrayObject _vao;
};

#endif //_CLOUDS_DISPATCHOR_HPP_
