#include "castle_base.hpp"

#include "../motor/meshbuild.hpp"

#include "castle_open_structure.hpp"
#include "castle_cathedral_structure.hpp"
#include "castle_atower_structure.hpp"
#include "castle_roof_structure.hpp"
#include "castle_circle_bump.hpp"
#include "castle_colors.hpp"

using namespace std;
using namespace glm;

namespace CASTLE
{
	static float fatyadapt()
	{
		float c = utils::random_float(0.0f, 2.0f);
		return 0.8f + c*c*c*0.6f;
	}

	Base::Base(const TheoricalArchitecture& architecture, const baseParams& params, const builtStructures& buildings)
		: _architecture(architecture),
		  _mesh(STATIC_DRAW, STATIC_DRAW, STATIC_DRAW),
		  _inside(STATIC_DRAW, STATIC_DRAW, STATIC_DRAW),
		  _faces(),
		  _params(params),
		  _built(buildings),
		  _soul({
		  	.fatower_ycoeff = fatyadapt(),
		  	.magicircl_amount = 1
		  })
	{
		this->place_base_vertices();
		this->fill_faces();
		this->set_doors_structures();
	}

	void Base::build()
	{
		std::vector<uint> faces = _architecture.all_faces();
	
		for(uint f : faces)
		{
			std::unique_ptr<InsideFace> inside = _faces[f]->build();

			if(inside != NULL)
			{
				inside->build();
			}
		}

		_params.context->add_static_hitboxes(_hbxs);

		cout<<"SIZE OF THE CATSLE: "<<endl;
		cout<<" -Outside: "<<_mesh.size()<<" vertices and "<<_mesh.get_elements().size()/3<<" triangles"<<endl;
		cout<<" -Inside:  "<<_inside.size()<<" vertices and "<<_inside.get_elements().size()/3<<" triangles"<<endl;
	}

	void Base::build_floor(const std::vector<glm::vec3>& enclosure, float y)
	{
		if(enclosure.size() == 0)
		{
			mot::err("Base::build_floor(): empty enclosure (should be litterally impossible)");
		}

		std::vector<uint> outcycle = utils::uirange(_mesh.size(), _mesh.size()+enclosure.size()-1);
		std::vector<uint> incastlecycle = this->face_vertices_indices(this->architecture().get_border_face());
		for(const auto& p : enclosure)
		{
			_mesh.add_vertex(geom::yfixed(p, y), castle_colors::FLOOR()/0.6f);
		}
		
		std::vector<uint> incycle = utils::uirange(_mesh.size(), _mesh.size()+incastlecycle.size()-1);
		for(uint i : incastlecycle)
		{
			_mesh.add_vertex(geom::yfixed(_mesh.get_array_buffer<0>()[i], y), castle_colors::FLOOR()/0.6f);
		}

		meshbuild::fill_rubban(_mesh.get_array_buffer<0>(), _mesh.get_elements(), outcycle, incycle, false);
	}

	void Base::consume_magic_circle()
	{
		if(_soul.magicircl_amount == 0)
		{
			mot::err("Base::consume_magic_circle(): no magic circle available");
		}
		_soul.magicircl_amount--;
	}

	castleTools& Base::tools()
	{
		return *_params.tools;
	}

	const globalArchi& Base::get_soul() const
	{
		return _soul;
	}

	std::vector<uint> Base::face_vertices_indices(uint face) const
	{
		std::vector<uint> ret;
		std::vector<uint> vertices = _architecture.face_vertices(face);
	
		for(uint i=0; i<vertices.size(); i++)
		{
			uint j = (i+1)%vertices.size();
			Edge e = {vertices[i], vertices[j]};

			edgeIndex indices = this->edge_vertices_indices(e, face);
			ret.push_back(indices.first);
		}

		return ret;
	}

	builtStructures& Base::build_structures()
	{
		return _built;
	}

	static const uint NO_VALUE = std::numeric_limits<uint>::max();
	static const reliefEdge EMPTY = reliefEdge{
		reliefVertex{NO_VALUE, NO_VALUE},
		reliefVertex{NO_VALUE, NO_VALUE}
	};

	// Useless (I think)
	reliefEdge Base::relief_edge_indices_registred(const Edge& e)
	{
		std::vector<Edge> edges = _architecture.graph().all_edges();
		reliefEdge ret = EMPTY;

		for(Edge ei : edges)
		{
			ei = this->order(ei);

			Edge adjf1 = _architecture.adjacent_faces(e);
			Edge adjf2 = _architecture.adjacent_faces(ei);
			if(!adjf1.isomorphic(adjf2))
			{
				continue;
			}

			auto it = _relief_edges.find(ei);
			if(it == _relief_edges.end())
			{
				mot::err("call to Base::relief_edge_indices_registred() while all edges have not been registered");
			}

			reliefEdge re = it->second;

			if(e.first == ei.first)
			{	
				ret.first.top = (re.first.top != NO_VALUE) ? re.first.top : ret.first.top;
				ret.first.bot = (re.first.bot != NO_VALUE) ? re.first.bot : ret.first.bot;
			}
			if(e.first == ei.second)
			{
				ret.first.top = (re.second.top != NO_VALUE) ? re.second.top : ret.first.top;
				ret.first.bot = (re.second.bot != NO_VALUE) ? re.second.bot : ret.first.bot;	
			}
			if(e.second == ei.first)
			{
				ret.second.top = (re.first.top != NO_VALUE) ? re.first.top : ret.second.top;
				ret.second.bot = (re.first.bot != NO_VALUE) ? re.first.bot : ret.second.bot;
			}
			if(e.second == ei.second)
			{
				ret.second.top = (re.second.top != NO_VALUE) ? re.second.top : ret.second.top;
				ret.second.bot = (re.second.bot != NO_VALUE) ? re.second.bot : ret.second.bot;
			}
		}

		return ret;
	}

	void Base::place_base_vertices()
	{

		std::vector<Edge> edges = _architecture.graph().all_edges();

		for(const Edge& une : edges)
		{
			Edge e = this->order(une);
			_relief_edges[e] = EMPTY;
		}

		
		std::vector<uint> faces = _architecture.all_faces();

		for(uint f : faces)
		{
			std::vector<Edge> face_edges = _architecture.face_edges(f);

			for(uint i=0; i<face_edges.size(); i++)
			{
				Edge h = face_edges[(i+face_edges.size()-1)%face_edges.size()];
				Edge e = face_edges[i];

				bool descendinge = _architecture.descending_edge(e, f);
				bool descendingh = _architecture.descending_edge(h, f);
				bool orderede = (this->order(e).first == e.first);
				bool orderedh = (this->order(h).first == h.first);

				uint index = _mesh.add_vertex(geom::unprojplan(_architecture.graph()[e.first], _architecture.get_height(f)), _params.color * (descendinge ? 0.8f : 1.0f));

				if(!descendinge)
				{
					if(orderede)
					{
						_relief_edges[this->order(e)].first.top  = index;
					}
					else
					{
						_relief_edges[this->order(e)].second.top  = index;
					}
				}
				else
				{
					if(orderede)
					{
						_relief_edges[this->order(e)].first.bot  = index;
					}
					else
					{
						_relief_edges[this->order(e)].second.bot  = index;
					}
				}

				if(!descendingh)
				{
					if(orderedh)
					{
						_relief_edges[this->order(h)].second.top = index;
					}
					else
					{
						_relief_edges[this->order(h)].first.top = index;
					}
				}
				else
				{
					if(orderedh)
					{
						_relief_edges[this->order(h)].second.bot = index;
					}
					else
					{
						_relief_edges[this->order(h)].first.bot = index;
					}
				}
			}
		}
		

		// Check if all edges have been filled
		edges = _architecture.graph().all_edges();
		for(const Edge& e : edges)
		{
			auto it = _relief_edges.find(this->order(e));

			if(it == _relief_edges.end())
			{
				mot::err("Base::place_base_vertices(): algorithm error: an edge is not filled");
			}

			if(it->second.first.bot == NO_VALUE
			|| it->second.first.top == NO_VALUE
			|| it->second.second.bot == NO_VALUE
			|| it->second.second.top == NO_VALUE)
			{
				mot::err("Base::place_base_vertices(): algorithm error: an edge is partially not filled: edge " + e.to_string()
						 + " : {FB:" + ((it->second.first.bot  == NO_VALUE) ? "NO_VALUE" : to_string(it->second.first.bot ))
						 + ", FT:"   + ((it->second.first.top  == NO_VALUE) ? "NO_VALUE" : to_string(it->second.first.top ))
						 + ", SB:"   + ((it->second.second.bot == NO_VALUE) ? "NO_VALUE" : to_string(it->second.second.bot))
						 + ", ST:"   + ((it->second.second.top == NO_VALUE) ? "NO_VALUE" : to_string(it->second.second.top)) + "}");
			}
		}
	}

	void Base::fill_faces()
	{
		std::vector<uint> faces = _architecture.all_faces();
	
		for(uint f : faces)
		{
			std::unique_ptr<FaceStructure> structure = this->generate_face_strcture(f);

			if(structure == NULL)
			{
				mot::err("Base::fill_faces(): a face " + std::to_string(f) + " has an ampty strcure");
			}

			_faces.set(f, std::move(structure));
		}
	}

	void Base::set_doors_structures()
	{
		if(!(this->ready_to_set_doors(_architecture.get_border_face())))
		{
			mot::err("Base::set_doors_structures(): Impossible: the border face is not ready to place doors");
		}

		bool finished = false;
		std::vector<uint> faces = _architecture.all_faces();

		/*
			Place doors when it is possible for the face.
			This algorithm terminates because the graph
			induced by faces with oriented edges from
			the lower face to the higher face (in terms
			of architecture altitude) is a DAG.
			Thus there is always a face that is ready to
			place its doors.
		*/

		while(!finished)
		{
			finished = true;

			for(uint f : faces)
			{
				if(this->ready_to_set_doors(f) && !(_faces[f]->are_door_placed()))
				{
					finished = false;

					this->set_face_doors_positions(f);
				}
			}
		}

		// Check if all faces has placed its doors
		for(uint f : faces)
		{
			if(!(_faces[f]->are_door_placed()))
			{
				mot::err("Base::set_doors_structures(): face "
					+ to_string(f)+ " has not placed its doors");
			}
		}
	}

	std::vector<edgeDoorPosition> Base::edges_infos(const std::vector<Edge>& edges) const
	{
		std::vector<edgeDoorPosition> ret;

		for(const Edge& e : edges)
		{
			auto einfo = _placed_doors.find(this->order(e));

			if(einfo == _placed_doors.end())
			{
				mot::err(" Base::edges_infos(): asked for the non-placed edge: "
					+ e.to_string());
			}

			ret.push_back(einfo->second);
		}

		return ret;
	}

	reliefEdge Base::edge_vertices_indices(const Edge& e) const
	{
		auto it = _relief_edges.find(this->order(e));

		if(it == _relief_edges.end())
		{
			mot::err("Base::edge_vertices_indices(): edge " + e.to_string() + " not refered (so not existing if the previous algorithms did work)");
		}

		reliefEdge ret = it->second;

		if(e.first != this->order(e).first)
		{
			auto tmp = ret.first;
			ret.first  = ret.second;
			ret.second = tmp;
		}

		return ret;
	}

	edgeIndex Base::edge_vertices_indices(const Edge& e, uint face) const
	{
		reliefEdge re = this->edge_vertices_indices(e);

		Edge faces = _architecture.adjacent_faces(e);
		faces = _architecture.faces_orientation(faces);

		if(faces.first == face)
		{
			return edgeIndex{
				.first  = re.first.bot,
				.second = re.second.bot
			};
		}
		else
		{
			return edgeIndex{
				.first  = re.first.top,
				.second = re.second.top
			};
		}
	}

	void Base::set_face_doors_positions(uint face)
	{
		std::vector<Edge> desedges = _architecture.doored_descending_adjacent_edges(face);
		std::vector<Edge> asedges  = _architecture.doored_ascending_adjacent_edges(face);
		std::vector<edgeDoorPosition> imposed = this->edges_infos(desedges);

		_faces[face]->set_doors_positions(imposed, asedges);
		std::vector<edgeDoorPosition> placed = _faces[face]->outgoing_door_positions();

		/*
			Checl if the returned placement corresponds
			to the good edges
		*/
		if(asedges.size() != placed.size())
		{
			mot::err("Base::set_face_doors_positions(): wrong number of doors to place");
		}
		for(uint ei=0; ei<placed.size(); ei++)
		{
			if(!(placed[ei].edge == asedges[ei]))
			{
				mot::err("Base::set_face_doors_positions(): door placed on the wrong edge");
			}
		}		

		for(edgeDoorPosition d : placed)
		{
			d.edge = this->order(d.edge);
			
			// It must be the only time the door is place
			auto nothere = _placed_doors.find(d.edge);
			if(nothere != _placed_doors.end())
			{
				mot::err("Base::set_face_doors_positions(): try to place an already placed door on edge "
					+ d.edge.to_string());
			}

			// cout<<"placed "<<d.edge.to_string()<<endl;
			_placed_doors[d.edge] = d;
		}
	}

	std::unique_ptr<FaceStructure> Base::generate_face_strcture(uint face)
	{
		faceRole role = _architecture.get_structure(face)->get_role();
		
		baseBuiling fullbase = {
			.base   = this,
			.mesh   = &_mesh,
			.inside = &_inside,
			.hbxs   = &_hbxs
		};

		if(role == FREE)
		{
			return std::make_unique<OpenStructure>(fullbase, face);
		}
		if(role == CATHEDRAL)
		{
			return std::make_unique<CathedralStructure>(fullbase, face);
		}
		if(role == ATOWER)
		{
			return std::make_unique<AtowerStructure>(fullbase, face);
		}
		if(role == ROOF)
		{
			return std::make_unique<RoofStructure>(fullbase, face);
		}
		if(role == ROOFUP)
		{
			return std::make_unique<RoofStructure>(fullbase, face);
		}
		if(role == CIRCLEBUMP)
		{
			return std::make_unique<CircleBump>(fullbase, face);
		}
		if(role == FLOOR)
		{
			return std::make_unique<FloorStructure>(fullbase, face);
		}

		return NULL;
	}

	bool Base::ready_to_set_doors(uint face) const
	{
		std::vector<uint> adjfaces = _architecture.adjacent_faces(face);

		for(uint f : adjfaces)
		{
			if(_architecture.ascending_face_edge({f, face}) && (_faces[f]->are_door_placed() == false))
			{
				return false;
			}
		}

		return true; 
	}

	Base::Edge Base::order(const Edge& e) const	
	{
		if(e.first > e.second)
		{
			return {e.second, e.first};
		}
		else
		{
			return e;
		}
	}

	std::vector<glm::vec3> Base::face_out_vertices(uint face, float h)
	{
		return this->face_shift_vertices(face, h, 0);
	}

	std::vector<glm::vec3> Base::face_in_vertices(uint face, float h)
	{
		return this->face_shift_vertices(face, h, -_params.wall_radius);
	}

	std::vector<glm::vec3> Base::face_shift_vertices(uint face, float h, float shift)
	{
		return geom::unprojplan(geom::bissectrice_enlarge_cycle(_architecture.face_vertices_positions(face), shift), h);
	}

	const StoredDrawableObject<glm::vec3, glm::vec3>& Base::get_mesh() const
	{
		return _mesh;
	}

	const StoredDrawableObject<glm::vec3, glm::vec3>& Base::get_inside_mesh() const
	{
		return _inside;
	}

	StoredDrawableObject<glm::vec3, glm::vec3>& Base::get_inside_mesh()
	{
		return _inside;
	}

	const TheoricalArchitecture& Base::architecture() const
	{
		return _architecture;
	}

	const baseParams& Base::get_params() const
	{
		return _params;
	}

	const std::vector<std::unique_ptr<BasicHitbox>>& Base::get_hitboxes() const
	{
		return _hbxs;
	}
};
