#ifndef _TOWER_CANNON_HPP_
#define _TOWER_CANNON_HPP_

#include "../motor/instances_manager.hpp"
#include "../motor/stored_vao_object.hpp"

struct towerCannon
{
	glm::vec3 position;
	glm::vec3 orientationN;
	float size;
	float light;
};

class TowerCannon : public StoredVAObject<InstancesManager<
	towerCannon,
	2,
	glm::vec3,
	glm::vec3,
	glm::vec4,
	glm::vec4>>
{
public:
	TowerCannon(float size);

	void change_cannon(uint instance_index, const towerCannon& cannon);

protected:
	void add_instance_in_buffers(const towerCannon& infos) override;
	
private:
	enum bufferRoles {RELATIVE_POS=0, COLORS, DEPS_LIGHT, ORIENTATION_N_SIZE};

	void build_mesh();

private:
	float _size;
};

#endif //_TOWER_CANNON_HPP_
