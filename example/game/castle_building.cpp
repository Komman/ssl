#include "castle_building.hpp"

CastleBuilding::CastleBuilding(const castleGridPlacing& grid_draw)
	: _planarb(grid_draw.circles, grid_draw.polygons),
	  _exposing_roof(false)
{
	for(const auto& c : grid_draw.circles)
	{
		if(c.value == UP_AVAILABLE)
		{
			_exposing_roof = true;
			break;
		}
	}
	if(!_exposing_roof)
	{
		for(const auto& p : grid_draw.polygons)
		{
			if(p.value == UP_AVAILABLE)
			{
				_exposing_roof = true;
				break;
			}
		}
	}
}

bool CastleBuilding::is_placable(SpaceGrid& spacegrid, const planarPlace& place)
{
	return _planarb.is_placable(spacegrid, place);
}

bool CastleBuilding::is_placable(SpaceGrid& spacegrid)
{
	return this->is_placable(spacegrid, this->get_generated_place());
}

bool CastleBuilding::expose_roof() const
{
	return _exposing_roof;
}

PlanarBuilding& CastleBuilding::get_planar_building()
{
	return _planarb;
}

void CastleBuilding::place_grid_only(SpaceGrid& spacegrid, const planarPlace& place)
{
	_planarb.place(spacegrid, place);
}