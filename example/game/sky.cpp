#include "sky.hpp"

#include "ingame.hpp"

#include <glm/gtx/rotate_vector.hpp>

using namespace std;
using namespace glm;

Sky::Sky(SharedRessources& shared_ressources,
		 DrawerPackController& drawer_controller)
	: _shared_ressources(shared_ressources),
	  _sky_back_color("sky_back_color", vec4(0.05,0.05,0.10,1.0)),
	  _sky_stars_achievement("sky_stars_achievement", 0.0),
	  _sky_moon_turn("sky_moon_turn", 0.0),
	  _moon1_direction("moon1_direction", vec3(1.0, 2.0, 3.0)),
	  _moon2_direction("moon2_direction", vec3(3.0, 2.0, 1.0)),
	  _moon3_direction("moon3_direction", vec3(-1.0, 2.0, -1.0)),
	  _radius1("moon1_radius", 0.07f),
	  _radius2("moon2_radius", 0.03f),
	  _radius3("moon3_radius", 0.02f),
	  _color1("moon1_color", vec4(1.0,0.2,0.1,1.0)),
	  _color2("moon2_color", vec4(0.2,0.8,0.1,1.0)),
	  _color3("moon3_color", vec4(0.2,0.2,1.0,1.0)),
	  _voronoi_distance("moon_voronoi_distance", 0.05f),
	  _aurore_dep("aurore_dep", 0.0f),
	  _aurore_intens("aurore_intens", 0.05f),
	  _shader_gen("shaders/sky.glsl"),
	  _drawer(_shader_gen, "sky_vertex", "sky_fragment", drawer_controller),
	  _self_time(0.0)
{
	_drawer.depth(true);
	_drawer.depth_mask(false);
	_drawer.depth_function(GL_LEQUAL);
	// If the draw is not multisampled:
	// _drawer.blending(true);
	// _drawer.set_blend_equation(GL_MAX);
}

glm::vec3 Sky::get_sunlight_direction() const
{
	return _moon1_direction.get_value();
}

void Sky::animate(float dt)
{
	_self_time+=dt;
	_sky_stars_achievement += dt/250.0;
	_sky_moon_turn += dt/20.0;
	_moon2_direction = glm::rotate(_moon2_direction.get_value(), dt/50.0f, vvv::y());
	_moon3_direction = glm::rotate(_moon3_direction.get_value(), dt/40.0f, vvv::y());
	_moon3_direction = vec3(_moon3_direction.get_value().x,
						    2.5f + 2.0f*cos(_self_time/20.0f),
						    _moon3_direction.get_value().z);
	_aurore_dep += dt/2.0f;
}

void Sky::draw()
{
	_shared_ressources.tools().game_drawer.draw_bloomed_MS(_drawer, buffers::screenfill());
}

void  Sky::set_aurore_intensity(float new_intensity)
{
	_aurore_intens = new_intensity;
}

float Sky::get_aurore_intensity()
{
	return _aurore_intens.get_value();
}
