#include "face_arc.hpp"

#include "../motor/building.hpp"

#include "arc_hitbox.hpp"

using namespace std;
using namespace glm;

FaceArc::FaceArc(HitboxContext& context, const glm::vec3& color, float angle, float thickness_coeff)
	: StoredVAObject(context, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW),
	  _color(color),
	  _angle(angle),
	  _thickness_coeff(thickness_coeff)
{
	this->build_base();
}

void FaceArc::build_base()
{
	auto& elements = this->get_elements();
	auto& relpos   = this->get_array_buffer<RELATIVE_POS>();
	auto& colors   = this->get_array_buffer<COLOR>();

	float extruding = 1.0f;
	float winextrud = 0.2f;
	int   arcdiscr  = 10;
	float occlusion_ext = 0.6f;
	float occlusion_int = 0.7f;
	float slopeh = 0.1f;

	uint count = building::arc::thickedowntoped(
		relpos,
		elements,
		vec3(-0.5, 0.5, 0),
		vvv::x(),
		vvv::y(),
		vvv::z()*extruding,
		_thickness_coeff,
		_angle,
		arcdiscr,
		vec2(1,0.5)
	);

	// WINDOW EXTRUDING
	for(int i=2*(arcdiscr-1)+1; i<=4*(arcdiscr-1)+1; i++)
	{
		relpos.change_subvalue(i, relpos[i] + vvv::z()*extruding*winextrud);
	}
	// COLOR FILLING
	for(uint i=0; i<count; i++)
	{
		colors.add_value(_color*((i<count/2) ? ((i<count/4) ? occlusion_ext : occlusion_int) : 1.0f));
	}

	// WINDOW FACE EXTRUDING
	uint midlight = relpos.size();
	relpos.add_value(vec3(0,0.75, extruding*winextrud));
	colors.add_value(_color);

	// WINDOW FILLING
	for(int i=2*(arcdiscr-1)+1; i<(arcdiscr-1)*4-2; i+=2)
	{
		elements.add_triangle(i+0, i+2, midlight, false);
		elements.add_triangle(i+3, i+1, midlight, false);
	}
	int lasta = (arcdiscr-1)*4-1;
	elements.add_triangle(lasta, lasta+2, lasta+1, false);
	elements.add_triangle(midlight, lasta+0, lasta+1, false);


	// PAVER ADDING
	uint pavstart = relpos.size();
	for(vec3 dep : {vec3(-0.5f,0,0), vec3(0.5f-_thickness_coeff/2.0f,0,0)})
	{
		uint pstart = relpos.size();
		building::paver::v(relpos, paverInfo{
			dep,
			vec3(1,0,0)*_thickness_coeff/2.0f,
			vec3(0,1,0)/2.0f,
			vec3(0,0,1)*extruding,
		});

		colors.add_value(_color*occlusion_ext);
		colors.add_value(_color*occlusion_ext);
		colors.add_value(_color);
		colors.add_value(_color);

		colors.add_value(_color*occlusion_ext);
		colors.add_value(_color*occlusion_ext);
		colors.add_value(_color);
		colors.add_value(_color);

		relpos.change_subvalue(pstart + 3, relpos[pstart + 3] + vvv::y()*slopeh);
		relpos.change_subvalue(pstart + 2, relpos[pstart + 2] + vvv::y()*slopeh);

		elements.add_rectangle(
			pstart+0,
			pstart+3,
			pstart+7,
			pstart+4,
			false
		);

		elements.add_rectangle(
			pstart+3,
			pstart+2,
			pstart+6,
			pstart+7,
			false
		);

		elements.add_rectangle(
			pstart+2,
			pstart+1,
			pstart+5,
			pstart+6,
			false
		);

		elements.add_rectangle(
			pstart+0,
			pstart+1,
			pstart+2,
			pstart+3,
			false
		);
	}	

	// DOWN WINDOW
	relpos.change_subvalue(pavstart+8 + 4, relpos[pavstart+8 + 4] + vvv::z()*extruding*winextrud);
	relpos.change_subvalue(pavstart+0 + 5, relpos[pavstart+0 + 5] + vvv::z()*extruding*winextrud);
	colors.change_subvalue(pavstart+8 + 0, _color*occlusion_int);
	colors.change_subvalue(pavstart+8 + 4, _color*occlusion_int);
	colors.change_subvalue(pavstart+0 + 1, _color*occlusion_int);
	colors.change_subvalue(pavstart+0 + 5, _color*occlusion_int);

	uint downface = relpos.size();
	relpos.add_value(relpos[pavstart+8 + 0] + vvv::z()*extruding*winextrud + vvv::y()*slopeh);
	relpos.add_value(relpos[pavstart+0 + 1] + vvv::z()*extruding*winextrud + vvv::y()*slopeh);
	colors.add_value(_color*glm::mix(occlusion_int, 1.0f, 0.6f));
	colors.add_value(_color*glm::mix(occlusion_int, 1.0f, 0.6f));

	uint closeface = relpos.size();
	relpos.add_value(relpos[downface+0] - vvv::z()*extruding*winextrud - vvv::y()*slopeh/3.0f);
	relpos.add_value(relpos[downface+1] - vvv::z()*extruding*winextrud - vvv::y()*slopeh/3.0f);
	colors.add_value(_color*glm::mix(occlusion_int, 1.0f, 0.3f));
	colors.add_value(_color*glm::mix(occlusion_int, 1.0f, 0.3f));

	elements.add_triangle(midlight, 2*(arcdiscr-1)+1, downface, true);
	elements.add_triangle(midlight, 2*(arcdiscr-1)+2, downface+1, false);
	elements.add_triangle(midlight, downface+1, downface, false);
	elements.add_rectangle(downface+0, downface+1, closeface+1, closeface+0, false);
}

std::unique_ptr<BasicHitbox> FaceArc::build_hitbox(const planarYPlace& place) const
{
	vec3 dirzN = geom::unprojplan(place.dirN);
	vec3 dirxN = glm::cross(vvv::y(), dirzN);

	std::unique_ptr<HitboxFusion> ret = std::make_unique<HitboxFusion>();

	ret->fusion_hitbox<ArcHitbox>(
		arcInfo{
			.bottom        = place.center - dirxN*place.size.x/2.0f + vvv::y()*place.size.y/2.0f,
			.sized_x       = dirxN*place.size.x,
			.sized_y       = vvv::y()*place.size.y/2.0f,
			.sized_z       = dirzN*place.size.z,
			.thickness     = _thickness_coeff,
			.angle         = _angle,
			.points_amount = 6
		}
	);

	ret->fusion_hitbox<PaverHitbox>(paverInfo{
		.bottom  = place.center - dirxN*place.size.x/2.0f,
		.sized_x = dirxN*place.size.x/2.0f*_thickness_coeff,
		.sized_y = vvv::y()*place.size.y/2.0f,
		.sized_z = dirzN*place.size.z
	});

	ret->fusion_hitbox<PaverHitbox>(paverInfo{
		.bottom  = place.center + dirxN*place.size.x/2.0f,
		.sized_x = -dirxN*place.size.x/2.0f*_thickness_coeff,
		.sized_y = vvv::y()*place.size.y/2.0f,
		.sized_z = dirzN*place.size.z
	});

	return ret;
}


void FaceArc::place_range(const planarYPlace& place, float arc_length, float interspace)
{
	vec3 dirzN = geom::unprojplan(place.dirN);
	vec3 dirxN = glm::cross(vvv::y(), dirzN);
	vec3 start = place.center - dirxN*place.size.x/2.0f;

	int   amount = (int)((place.size.x+interspace) / (arc_length+interspace));
	float space  = (place.size.x+interspace)/float(amount);

	auto hbx = std::make_unique<HitboxFusion>();

	for(uint i=0; i<(uint)amount; i++)
	{
		planarYPlace dplace = {
			.center = start + dirxN*(space*float(i)+arc_length/2.0f),
			.size   = vec3(arc_length, place.size.y, place.size.z),
			.dirN   = place.dirN
		};

		hbx->fusion_hitbox(std::move(this->build_hitbox(dplace)));
		this->add_mesh_only(dplace);
	}

	this->add_static_hitbox_only(std::move(hbx));
}


void FaceArc::place_range_no_contact(const planarYPlace& place, float arc_length, float interspace, const std::vector<std::unique_ptr<BasicHitbox>>&  hitboxes)
{
	vec3 dirzN = geom::unprojplan(place.dirN);
	vec3 dirxN = glm::cross(vvv::y(), dirzN);
	vec3 start = place.center - dirxN*place.size.x/2.0f;

	int   amount = (int)((place.size.x+interspace) / (arc_length+interspace));
	float space  = (place.size.x+interspace)/float(amount);

	auto hbx = std::make_unique<HitboxFusion>();

	for(uint i=0; i<(uint)amount; i++)
	{
		planarYPlace dplace = {
			.center = start + dirxN*(space*float(i)+arc_length/2.0f),
			.size   = vec3(arc_length, place.size.y, place.size.z),
			.dirN   = place.dirN
		};

		planarYPlace testplace = dplace;
		dplace.center += geom::unprojplan(dplace.dirN)*dplace.size.z*0.1f;
		auto testhbx = this->build_hitbox(testplace);

		if(testhbx->in_collision(hitboxes))
		{
			continue;
		}
		else
		{
			hbx->fusion_hitbox(std::move(this->build_hitbox(dplace)));
			this->add_mesh_only(dplace);
		}		
	}

	this->add_static_hitbox_only(std::move(hbx));
}

