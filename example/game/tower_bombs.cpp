#include "tower_bombs.hpp"

#include "../motor/angular_symmetry.hpp"
#include "../motor/premade_buffers.hpp"

#include "shareders.hpp"

using namespace glm;

TowerBombs::TowerBombs()
	: DrawableArrowPack(STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, DYNAMIC_DRAW, DYNAMIC_DRAW, DYNAMIC_DRAW)
{
	cylindricDivisor axe =  {
		.axe_point      = vec3(0.0),
		.axe_directionN = vec3(0.0,1.0,0.0),
		.divisor        = 6,
	};
	AngularSymmetry<vec3, vec3> symm(axe,
		&(this->array_buffer<RELATIVE_POS>()),
		&(this->element_buffer()),
		&(this->array_buffer<COLOR>()),
		&(this->array_buffer<BLOOM_COLOR>()));

	vec3 blue_bloom = vec3(0.5,0.5,3.5)*2.0f;
	vec3 fire_bloom = vec3(3.5,1.5,0.5)*3.0f;

	symm.place_asymmetrical(vec3(0.0,0.0,0.0), vec3(0.5), blue_bloom);

	float cyl_top  = -0.2;
	float cyl_bot  = -0.7;
	float cyl_size =  0.5;
	float large_top  = -0.9;
	float large_bot  = -1.0;
	float large_size = 0.7;

	float int_size  = 0.1;
	float int_depth = 0.1;

	uint ftop = symm.place_symmetrical(vec3(cyl_size  , cyl_top  , 0.0), vec3(0.8), blue_bloom);
	uint fmid = symm.place_symmetrical(vec3(cyl_size  , cyl_bot  , 0.0), vec3(0.5), vec3(0.0));
	uint fbot = symm.place_symmetrical(vec3(large_size, large_top, 0.0), vec3(0.8), fire_bloom);
	uint frea = symm.place_symmetrical(vec3(large_size, large_bot, 0.0), vec3(0.7), fire_bloom);
	
	uint fbot_int = symm.place_symmetrical(vec3(large_size - int_size, large_bot + int_depth, 0.0), vec3(0.6), vec3(0.0));
	uint frea_int = symm.place_symmetrical(vec3(large_size - int_size, large_bot            , 0.0), vec3(0.6), vec3(0.0));

	vec3 combu_color  = vec3(1.0, 0.5, 0.3);
	vec3 combu_bcolor = vec3(4.0, 1.5, 0.3)*1.3f;

	uint cumbu_center = symm.place_asymmetrical(vec3(0.0,large_bot + int_depth,0.0), combu_color*2.0f, combu_bcolor*2.0f);
	uint cumbu_int    = symm.place_symmetrical(vec3(large_size - int_size, large_bot + int_depth, 0.0), combu_color, combu_bcolor);

	symm.link_triangle(0, ftop, symm.symmetrical(1, ftop));
	
	symm.link_rectangle(ftop, fmid, symm.symmetrical(1, fmid), symm.symmetrical(1, ftop));
	symm.link_rectangle(fmid, fbot, symm.symmetrical(1, fbot), symm.symmetrical(1, fmid));
	symm.link_rectangle(fbot, frea, symm.symmetrical(1, frea), symm.symmetrical(1, fbot));
	symm.link_rectangle(frea, frea_int, symm.symmetrical(1, frea_int), symm.symmetrical(1, frea));
	symm.link_rectangle(frea_int, fbot_int, symm.symmetrical(1, fbot_int), symm.symmetrical(1, frea_int));

	symm.link_triangle(cumbu_center, symm.symmetrical(1, cumbu_int), cumbu_int);


	/*
	 c0
      \
       \\
      \   \
	    TH
	  <------>
      \       \
       \       \        __
        |       |      /\
        |       |        \ L
        |       |        _\/
        |       |        
        |       |
        |       |
        |       |
        |\      |\
        | \     | \
      a1|a2|  b1|b2|
        |  |    |  |
    */

	float TH = 0.40;
	float L  = 0.20; 

	vec3 rainucolor = vec3(0.3);

	uint c0 = symm.place_asymmetrical(vec3(0.0,0.1,0.0), rainucolor, vec3(0.0));

	uint a1 = symm.place_symmetrical(vec3(cyl_size, large_top + L/8.0, -L/2.0), rainucolor*0.2f, vec3(0.0));
	uint a2 = symm.place_symmetrical(vec3(cyl_size, large_top + L/8.0, +L/2.0), rainucolor*0.2f, vec3(0.0));
	
	uint b1 = symm.place_symmetrical(vec3(cyl_size + TH, large_top, -L/2.0), rainucolor, vec3(0.0));
	uint b2 = symm.place_symmetrical(vec3(cyl_size + TH, large_top, +L/2.0), rainucolor, vec3(0.0));

	
	symm.link_triangle(c0, b1, a1);
	symm.link_triangle(c0, a2, b2);
	symm.link_triangle(c0, b2, b1);
	symm.link_rectangle(b2, a2, a1, b1);
}


TowerBombs::InstancesTuple TowerBombs::buffer_filling(const Arrow& arrow, const towerBomb& look) const
{
	// static_assert(std::is_same_v<InstancesTuple, std::tuple<glm::vec4, glm::vec3>> == true);

	return {vec4(arrow.get_position(), arrow.get_length()), vec4(look.color, arrow.get_size()), arrow.get_directionN()};
}

GameDrawer::gameDrawTypes TowerBombs::get_typed_draw() const
{
	return GameDrawer::BLOOM_MS_DRAW;
}

Drawer& TowerBombs::get_drawer() const
{
	return shareders::tower_bomb();
}
