#include "castle_role.hpp"


namespace CASTLE
{
	BasicRole::BasicRole() {}

	bool BasicRole::door_type_available(doorType type) const
	{
		return true;
	}

	std::vector<doorType> BasicRole::get_compatible_doors_type(const BasicRole& structure) const
	{
		std::vector<doorType> ret;

		for(int i=0; i<DOOR_TYPE_AMOUNT; i++)
		{
			if(this->door_type_available((doorType)i) && structure.door_type_available((doorType)i))
			{
				ret.push_back((doorType)i);
			}
		}

		return ret;
	}
	
	float BasicRole::additionnal_height() const
	{
		return 0.0f;
	}

	/*
		==== STRUCTURES SPECIALIZED ==== 
	*/

	/*
		CONSTRUCTORS
	*/
	FreeRole::FreeRole() : BasicRole() {}
	AtowerRole::AtowerRole() : BasicRole() {}
	CathedralRole::CathedralRole() : BasicRole() {}
	RoofRole::RoofRole() : BasicRole() {}
	CirclebumpRole::CirclebumpRole() : BasicRole() {}
	RoofupRole::RoofupRole() : BasicRole() {}
	FloorRole::FloorRole() : BasicRole() {}


	/*
		FREE
	*/
	bool FreeRole::is_roof_open() const
	{
		return true;
	}
	faceRole FreeRole::get_role() const
	{
		return FREE;
	}

	/*
		ATOWER
	*/
	bool AtowerRole::is_roof_open() const
	{
		return true;
	}
	faceRole AtowerRole::get_role() const
	{
		return ATOWER;
	}
	
	/*
		CATHEDRAL
	*/
	bool CathedralRole::is_roof_open() const
	{
		return false;
	}
	faceRole CathedralRole::get_role() const
	{
		return CATHEDRAL;
	}
	float CathedralRole::additionnal_height() const
	{
		return 50.0f*0.6f;
	}

	/*
		ROOF
	*/
	bool RoofRole::is_roof_open() const
	{
		return false;
	}
	faceRole RoofRole::get_role() const
	{
		return ROOF;
	}

	/*
		CIRCLEBUMP
	*/
	bool CirclebumpRole::is_roof_open() const
	{
		return false;
	}
	faceRole CirclebumpRole::get_role() const
	{
		return CIRCLEBUMP;
	}

	/*
		ROOFUP
	*/
	bool RoofupRole::is_roof_open() const
	{
		return false;
	}
	faceRole RoofupRole::get_role() const
	{
		return ROOFUP;
	}

	/*
		FLOOR
	*/
	bool FloorRole::is_roof_open() const
	{
		return true;
	}
	faceRole FloorRole::get_role() const
	{
		return FLOOR;
	}
};
