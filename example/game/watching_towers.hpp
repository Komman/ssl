#ifndef _WATCHING_TOWERS_HPP_
#define _WATCHING_TOWERS_HPP_

#include "../motor/target.hpp"
#include "../motor/smooth_setter.hpp"
#include "simple_towers.hpp"
#include "tower_shooter.hpp"
#include "tower_cannon.hpp"
#include "meshed_lamp_ray.hpp"

class WatchingTowers : public SimpleTower
{
public:
	WatchingTowers(const BasicTarget* target, HitboxContext& hbx_context, Lanterns& lanterns, VerticalShadower& vshadower);

	void reset_target(const BasicTarget* target);
	void animate(float dt);

	const TowerCannon& cannons() const;

private:
	enum towerBehavior {SLEEPING=0, DRESSING, SPYING, TARGETING, SHOOTING};

	struct towerLife
	{
		float starth;
		float maxdh;
		float target_range;
		float light_range;
		float dress_range;
		float cannon_size;
		float shotping_coeff;

		glm::vec3 bissecdirN;
		towerBehavior behavior;

		SmoothSetter<float> height;
		SmoothSetter<float> deployment;
		SmoothSetter<glm::vec3> cannondirN;
		TowerShooter  shooter;
		MeshedLampRay lamp;
	};

private:
	void update_towers_life();
	void animate_tower(uint tower_id, float dt);
	glm::vec3 get_head_position(uint towerid) const;
	void set_tower_growth(uint tower_id, float dt, towerLife& tlife, float distarget);
	bool full_deployed(towerLife& tlife) const;
	bool tower_deployed(towerLife& tlife) const;
	void try_to_shot(towerLife& tlife);
	void position_cannon(uint tower_id, towerLife& tlife, float dt);
	glm::vec3 cannon_position(uint tower_id) const;

private:
	const BasicTarget* _target;

	std::vector<towerLife> _towers_life;
	TowerCannon _cannons;
};


#endif //_WATCHING_TOWERS_HPP_
