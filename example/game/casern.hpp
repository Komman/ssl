#ifndef _CASERN_HPP_
#define _CASERN_HPP_

#include "../motor/stored_instances_buffers.hpp"
#include "../motor/angular_symmetry.hpp"
#include "../motor/vertical_shadower.hpp"

#include "simple_towers.hpp"
#include "arc_door.hpp"
#include "fat_tower.hpp"
#include "castle_support.hpp"


class Casern : public CastleBuildingPlacer<castleSupport>
{
public:
	Casern(VerticalShadower* vshadower, CastleSupport* support, SimpleTower* tower, ArcDoor* door, FatTower* fat_tower);

	void add_casern(const castleSupport& casern);

protected:
	void place_building_only(const castleSupport& building, float h) override;
	castleSupport generate_building(const genParam& g) override;
	planarPlace  get_place(const castleSupport& g) override;


private:
	VerticalShadower* _vshadower;
	CastleSupport*    _support;
	SimpleTower*      _tower;
	ArcDoor*          _door;
	FatTower*         _fat_tower;


	glm::vec3 _doors_walls_color;

};

#endif //_CASERN_HPP_
