#include "fat_tower.hpp"

#include <glm/gtx/rotate_vector.hpp>

#include "../utils/geom.hpp"
#include "../motor/cylinder_hitbox.hpp"
#include "../motor/crown_hitbox.hpp"
#include "castle_colors.hpp"

using namespace glm;


FatTower::FatTower(HitboxContext& hbx_context, VerticalShadower* vshadower, ArcDoor* door, uint sides_amount, const glm::vec3& color)
	: CastleBuildingInst(hbx_context,
		{
			{valuedCircleInfo{vec2(0),
							  0.5f*sqrt(1.0f + utils::square(sin(float(M_PI)*2.0f/float(sides_amount)))),
							  1}},
			{}
		},
		STATIC_DRAW,
		STATIC_DRAW,
		STATIC_DRAW,
		STATIC_DRAW,
		STATIC_DRAW),
	  _vshadower(vshadower),
	  _door(door),
	  _circlediscr(sides_amount),
	  _base_color(color),
	  _thickness(0.1),
	  _yB(_thickness),
	  _xC(1.0f - _thickness),
	  _yD(1.0f - 0.8f*_thickness),
	  _yDm((_yB + _yD)/2.0f),
	  _yE(1.0f - _thickness/2.0f),
	  _yF(1.00),
	  _xG(_xC),
	  _yH(_yE),
	  _middle_thickness(_thickness/3.0f),
	  _loophole_thickness(0.015)
{
	this->build_base();
}

void FatTower::place_building_only(const fatTower& building, float h)
{
	auto b = building;
	b.center.y += h;

	this->add_tower(b);
}

fatTower FatTower::generate_building(const genParam& g)
{
	return {
		.center = vec3(g.position.x, 0.0f, g.position.y),
		.dirN2D = g.dirN,
		.size   = vec3(g.maxsize, g.maxsize*utils::random_float(1, 10), g.maxsize)
	};
}

planarPlace FatTower::get_place(const fatTower& g)
{
	return {
		.center = geom::projplan(g.center),
		.size   = geom::projplan(g.size),
		.dirN   = g.dirN2D
	};
}

void FatTower::add_tower(const fatTower& tower, bool shadows)
{
	this->get_array_buffer<POS_SIZEY>().add_value(vec4(tower.center, tower.size.y));
	this->get_array_buffer<DIR2D_SIZEXZ>().add_value(vec4(tower.dirN2D.x, tower.dirN2D.y, tower.size.x, tower.size.z));

	vec3 dirN3D = vec3(tower.dirN2D.x, 0, tower.dirN2D.y);
	auto total_hbx = std::make_unique<HitboxFusion>();
	vec3 dirN3D_or = glm::rotate(dirN3D, float(M_PI)/float(_circlediscr), vvv::y());

	float dize = tower.size.z * 0.2f;
	float face = tower.size.z/2.0f*cos((2.0f*M_PI)/float(_circlediscr)/2.0f);

	if(shadows)
	{
		if(_circlediscr != 4)
		{
			_vshadower->add_circle(tower.center, std::max(tower.size.x, tower.size.z)/2.0f * 1.1f);
		}
		else
		{
			_vshadower->add_square(tower.center, dirN3D, vec3(std::max(tower.size.x, tower.size.z)/sqrt(2.0f)*1.1f));
		}
	}

	_door->add_door({
		.pos = tower.center - dirN3D*face,
		.dir2D = -tower.dirN2D,
		.size  = vec3(dize, tower.size.y*_thickness*0.9f, dize/2.0f),
		.wall_color = vec3(0.7,0.5,0.3)
	});
	auto hbx = std::make_unique<CylinderHitbox>(cylinderInfo{
		.bottom = tower.center + vvv::y()*_yB*tower.size.y,
		.to_top = vvv::y()*tower.size.y*(_yH - _yB),
		.base_orientationN = dirN3D_or,
		.radius = tower.size.x/2.0f*(1.0f-_thickness)
	}, _circlediscr);

	auto hbx_base = std::make_unique<CylinderHitbox>(cylinderInfo{
		.bottom = tower.center,
		.to_top = vvv::y()*_yB*tower.size.y,
		.base_orientationN = dirN3D_or,
		.radius = tower.size.x/2.0f
	}, _circlediscr);

	auto hbx_midle = std::make_unique<CylinderHitbox>(cylinderInfo{
		.bottom = tower.center + vvv::y()*_yDm*tower.size.y,
		.to_top = vvv::y()*_middle_thickness*tower.size.y,
		.base_orientationN = dirN3D_or,
		.radius = tower.size.x/2.0f
	}, _circlediscr);


	auto hbx_crown = std::make_unique<CrownHitbox>(crownInfo{
		.bottom_center = tower.center + vvv::y()*_yH*tower.size.y,
		.to_top_center = vvv::y()*(1.0f-_yH)*tower.size.y,
		.base_orientationN = dirN3D_or,
		.radius_start = tower.size.x/2.0f*_xG,
		.radius_end = tower.size.x/2.0f
	}, _circlediscr);


	total_hbx->fusion_hitbox(std::move(hbx));
	total_hbx->fusion_hitbox(std::move(hbx_base));
	total_hbx->fusion_hitbox(std::move(hbx_midle));
	total_hbx->fusion_hitbox(std::move(hbx_crown));

	_hitboxes.push_back(std::move(total_hbx));
	_hbx_context->add_static_hitbox(_hitboxes[_hitboxes.size()-1].get());
}

void FatTower::link_growbase(SymType& sym, uint i1, uint i2)
{
	sym.link_rectangle(i1, sym.symmetrical(1, i1), sym.symmetrical(1, i2), i2);
}

/*
    F-G
    | |
    E H--T
     \
      D
      |
   D3-D4   
   |
   D2-D1   yDm
      |
    B-C
    |
	A
*/

void FatTower::build_base()
{
	auto& relpos = this->get_array_buffer<RELATIVE_POS>();
	auto& colors = this->get_array_buffer<COLOR>();
	auto& elements = this->get_elements();

	AngularSymmetry<glm::vec3> sym(
		{vec3(0), vvv::y(), _circlediscr},
		&relpos,
		&elements,
		&colors
	);

	float yB  = _yB ; 
	float xC  = _xC ; 
	float yD  = _yD ; 
	float yDm = _yDm; 
	float yE  = _yE ; 
	float yF  = _yF ; 
	float xG  = _xG ; 
	float yH  = _yH ; 


	float halfangle = M_PI/float(_circlediscr);
	vec3 align = vec3(0.5f, 1, 0.5f)*geom::circle_point(
		vec3(0,1,0),
		vec3(0,0,-1),
		vec3(0,1,0),
		halfangle,
		1
	);



	uint A = sym.place_symmetrical(align*vec3( 1,  0,  1), _base_color*0.6f);
	uint B = sym.place_symmetrical(align*vec3( 1, yB,  1), _base_color);
	uint C = sym.place_symmetrical(align*vec3(xC, yB, xC), _base_color*0.8f);
	uint D = sym.place_symmetrical(align*vec3(xC, yD, xC), _base_color*0.9f);
	uint D1 = sym.place_symmetrical(align*vec3(xC, yDm, xC), _base_color);
	uint D2 = sym.place_symmetrical(align*vec3(1, yDm, 1), _base_color*0.9f);
	uint D3 = sym.place_symmetrical(align*vec3(1, yDm+_middle_thickness, 1), _base_color);
	uint D4 = sym.place_symmetrical(align*vec3(xC, yDm+_middle_thickness, xC), _base_color);
	uint E = sym.place_symmetrical(align*vec3( 1, yE,  1), _base_color);
	uint F = sym.place_symmetrical(align*vec3( 1, yF,  1), _base_color);
	uint G = sym.place_symmetrical(align*vec3(xG, yF, xG), _base_color);
	uint H = sym.place_symmetrical(align*vec3(xG, yH, xG), _base_color*0.6f);
	uint T = sym.place_asymmetrical(align*vec3(0, yH, 0),  _base_color);

	this->link_growbase(sym, A, B);
	this->link_growbase(sym, B, C);
	this->link_growbase(sym, C, D1);
	this->link_growbase(sym, D1,D2);
	this->link_growbase(sym, D2,D3);
	this->link_growbase(sym, D3,D4);
	this->link_growbase(sym, D4,D);
	this->link_growbase(sym, D, E);
	this->link_growbase(sym, E, F);
	this->link_growbase(sym, F, G);
	this->link_growbase(sym, G, H);

	sym.link_triangle(
		sym.symmetrical(0, H),
		sym.symmetrical(1, H),
		T
	);

	this->build_loophole(sym, cos(halfangle)*(1.0f - _thickness)/2.0f, 0.6, 0.25);
	this->build_loophole(sym, cos(halfangle)*(1.0f - _thickness)/2.0f, 0.2, 0.2);
}

/*

      B[3]---B[2]   
       |      |
       |      |
       |      | 
      B[0]---B[1]   
   
    Y
    ^
	|
	 --> Z
*/

void FatTower::build_loophole(SymType& sym, float x, float y, float h)
{
	vec3 _loophole_color = vec3(0.1);

	uvec4 B;
	B.x = sym.place_symmetrical(vec3(-_loophole_thickness, y  , - x), _loophole_color);
	B.y = sym.place_symmetrical(vec3(+_loophole_thickness, y  , - x), _loophole_color);
	B.z = sym.place_symmetrical(vec3(+_loophole_thickness, y+h, - x), _loophole_color);
	B.w = sym.place_symmetrical(vec3(-_loophole_thickness, y+h, - x), _loophole_color);

	uvec4 C;
	C.x = sym.place_symmetrical(vec3(-_loophole_thickness, y  , - x - _loophole_thickness*0.7f), _loophole_color);
	C.y = sym.place_symmetrical(vec3(+_loophole_thickness, y  , - x - _loophole_thickness*0.7f), _loophole_color);
	C.z = sym.place_symmetrical(vec3(+_loophole_thickness, y+h, - x - _loophole_thickness*0.7f), _loophole_color);
	C.w = sym.place_symmetrical(vec3(-_loophole_thickness, y+h, - x - _loophole_thickness*0.7f), _loophole_color);

	sym.link_rectangle(B[0], B[1], C[1], C[0], true);
	sym.link_rectangle(B[2], B[3], C[3], C[2], true);
	sym.link_rectangle(B[0], B[3], C[3], C[0], false);
	sym.link_rectangle(B[1], B[2], C[2], C[1], true);
	sym.link_rectangle(C[1], C[2], C[3], C[0], true);
}
