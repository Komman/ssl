#include "castle_placer.hpp"

#include <glm/gtx/string_cast.hpp>

#include "../utils/geom.hpp"
#include "../motor/fan_hitbox.hpp"
#include "../motor/wall_hitbox.hpp"
#include "../motor/cycle_enlargement_hitbox.hpp"
#include "../motor/meshbuild.hpp"
#include "../motor/gutils.hpp"

#include "castle_colors.hpp"

using namespace glm;
using namespace std;

namespace CASTLE
{
	CastlePlacer::CastlePlacer(const castleTools& tools, const glm::vec3& position)
		: GraphBuildor((castle_colors::generate_color_set(), CASTLE::graphParameters{{tools.context, tools.ground}, position})),
		  _tools(tools),
		  _grindside(geom::projplan(this->minmax_coord().min - (this->minmax_coord().max - this->minmax_coord().min)/100.0f),
		  			 geom::projplan(this->minmax_coord().max + (this->minmax_coord().max - this->minmax_coord().min)/100.0f),
		  			 100),
		  _base_graph(),
		  _base(STATIC_DRAW, STATIC_DRAW, STATIC_DRAW),
		  _base_color(castle_colors::BASE()),
		  _verteices_spacing(15.0f),
		  _lanterns(*(tools.slightor)),
		  _simple_towers(&(tools.player->get_target()), *(tools.context), _lanterns, *(tools.vshadower)),
		  _magic_towers(*(tools.context), *(tools.slightor), *(tools.vshadower)),
		  _simple_walls(*(tools.context), *(tools.vshadower)),
		  _arc_bridges(*(tools.context)),
		  _arc_doors(*(tools.context)),
		  _fat_towers(*(tools.context), tools.vshadower, &_arc_doors, 8, castle_colors::FAT_TOWERS()),
		  _squares_towers(*(tools.context), tools.vshadower, &_arc_doors, 4, castle_colors::SQUARE_TOWER()
		  ),
		  _support(*(tools.context), _tools.vshadower),
		  _churchs(*(tools.context), tools.vshadower, &_arc_doors),
		  _face_arcs(*(tools.context), castle_colors::FACE_ARCS(), M_PI/3.0f, 0.2f),
		  _counter_forts(*(tools.context), tools.vshadower, {
		  		.bot_center = vec3(0,-2.0f, 0.1f),
		  		.size = 0.025f,
				.height_startgirder = -0.13f,
				.endgirder = vec3(0,0,0)
		  	}, _base_color),
		  _elevators(*(tools.context), castle_colors::ELEVATORS()),
		  _caserns(tools.vshadower, &_support, &_simple_towers, &_arc_doors, &_fat_towers),
		  _circles(&_magic_towers, &_arc_bridges, &_arc_doors),
		  _circles_placed(false)
	{
		
	}

	static bool showadv = true;


	void CastlePlacer::build()
	{
		if(showadv) cout<<TERM::ORANGE<<"=== BUILDING CASTLE ===="<<TERM::NOCOL<<endl;
		if(showadv) cout<<"Building castle border..."<<endl;
		this->place_ext();
		if(showadv) cout<<"Building castle inside..."<<endl;
		this->place_inside();
		if(showadv) cout<<TERM::ORANGE<<"========================"<<TERM::NOCOL<<endl;
	}

	void CastlePlacer::place_inside()
	{
		this->place_inside_base();
	}

	void CastlePlacer::place_ext()
	{
		float ND = this->get_enclosure_radius()/150.0f;
		
		const auto& tows = this->enclosure_towers();
		uint towcount = tows.size(); 
		
		_grindside.fill_discrete(uvec2(0), 2);

		for(uint i=0; i<towcount; i++)
		{
			uint prev = (i + towcount - 1) % towcount;
			uint next = (i + towcount + 1) % towcount;

			glm::vec3 baridirN = this->get_ext_direction(tows[prev], tows[i], tows[next]);

			_simple_towers.add_tower({
				.position = tows[i],
				.orientationN = baridirN,
				.height = 12*ND,
				.size   = 3.0f*ND
			});

			_simple_walls.add_wall({
				.start  = tows[i],
				.dst    = tows[next],
				.height = 10.0f*ND
			});

			if(!_grindside.in_grid(geom::projplan(tows[i])))
			{
				cout<<glm::to_string(geom::projplan(tows[i]))<<endl;
			}

			_grindside.set_thin_line(geom::projplan(tows[i]), geom::projplan(tows[next]), 1);
		}

		_grindside.fill_discrete(_grindside.get_dimensions()/2u, 0, 1);
		_grindside.replace_value(1, 2);
	}


	void CastlePlacer::place_inside_base()
	{
		using namespace CASTLE;

		if(showadv) cout<<"Building architecture graph..."<<endl;

		float door_maxsafe = _tools.player->MAX_SIZE*4.0f;
		float door_height = door_maxsafe*0.5f;

		_architecture = std::make_unique<TheoricalArchitecture>(this->compute_theorical_architecture(theoricalParameters{
			.safe_distance = _tools.player->MAX_SIZE*10.0f,
			graphHeightParam{
				.floor_h       = this->get_floor_potision().y,
				.min_roof_h    = door_maxsafe,
				.door_height   = door_maxsafe,
				.ND            = this->get_enclosure_radius()/150.0f,
				.center        = geom::projplan(this->get_floor_potision())
			}
		}));

		if(showadv) cout<<"Setting base..."<<endl;
		_real_base = std::make_unique<Base>(*_architecture,
			baseParams{
				.context       = _tools.context,
				.tools         = &_tools,
				.safe_distance = _tools.player->MAX_SIZE*10.0f,
				.wall_radius   = _tools.player->MAX_SIZE*2.0f,
				.door_height   = door_height,
				.safe_height   = door_maxsafe-door_height,
				.minwidth      = _tools.player->MAX_SIZE*2.0f,
				.color = _base_color,
				.incolor = castle_colors::INSIDE_COLOR()
			},
			builtStructures{
				.lanterns = &_lanterns,
				.simple_towers = &_simple_towers,
				.magic_towers = &_magic_towers,
				.simple_walls = &_simple_walls,
				.arc_bridges = &_arc_bridges,
				.arc_doors = &_arc_doors,
				.fat_towers = &_fat_towers,
				.squares_towers = &_squares_towers,
				.support = &_support,
				.churchs = &_churchs,
				.face_arcs = &_face_arcs,
				.counter_forts = &_counter_forts,
				.circles = &_circles,
				.elevators = &_elevators
			}
		);

		if(showadv) cout<<"Building base..."<<endl;
		_real_base->build();
		_real_base->build_floor(this->enclosure_towers(), this->get_floor_potision().y);	

		// _real_base->architecture().graph().print_ppm("castle_graph.ppm", 500);
	}

	glm::vec3 CastlePlacer::get_ext_direction(const glm::vec3& prev_tower,
											  const glm::vec3& cur_tower,
											  const glm::vec3& next_tower)
	{
		vec3 baridirN = glm::normalize((glm::normalize(cur_tower - prev_tower) + glm::normalize(cur_tower - next_tower))/2.0f);

		if(glm::dot(baridirN, cur_tower - this->get_position()) < 0.0f)
		{
			baridirN = -baridirN;
		}

		return baridirN;
	}

	const PlanGraph& CastlePlacer::base_graph() const
	{
		return _real_base->architecture().graph();
		// return _base_graph;
	}

	const CASTLE::Base& CastlePlacer::real_base() const
	{
		if(_real_base == NULL)
		{
			mot::err("call to CastlePlacer::real_base() while the castle has not been build");
		}

		return *_real_base;
	}
	const WatchingTowers& CastlePlacer::simple_towers() const
	{
		return _simple_towers;
	}

	WatchingTowers& CastlePlacer::watching_towers()
	{
		return _simple_towers;
	}

	const SimpleWall& CastlePlacer::simple_walls() const
	{
		return _simple_walls;
	}

	const Lanterns& CastlePlacer::lanterns() const
	{
		return _lanterns;
	}

	const MagicTower& CastlePlacer::magic_towers() const
	{
		return _magic_towers;
	}


	const ArcBridge& CastlePlacer::arc_bridges() const
	{
		return _arc_bridges;
	}

	const ArcDoor& CastlePlacer::arc_doors()  const
	{
		return _arc_doors;
	}

	const FatTower& CastlePlacer::fat_towers() const
	{
		return _fat_towers;
	}

	const FatTower& CastlePlacer::squares_towers() const
	{
		return _squares_towers;
	}

	const CastleSupport& CastlePlacer::supports() const
	{
		return _support;
	}

	const Church& CastlePlacer::churchs() const
	{
		return _churchs;
	}

	const FaceArc& CastlePlacer::face_arcs() const
	{
		return _face_arcs;
	}
	const SimpleElevator& CastlePlacer::elevators() const
	{
		return _elevators;
	}
	
	SimpleElevator& CastlePlacer::moving_elevators()
	{
		return _elevators;
	}

	const CounterFort& CastlePlacer::counter_forts()   const
	{
		return _counter_forts;
	}

	const std::vector<std::unique_ptr<BasicHitbox>>& CastlePlacer::base_hitboxes() const
	{
		return _base_hbx;
	}
};
