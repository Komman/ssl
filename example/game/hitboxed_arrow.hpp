#ifndef _HITBOXED_ARROW_HPP_
#define _HITBOXED_ARROW_HPP_

#include "../motor/primary_hitbox.hpp"

#include "arrow.hpp"

class HitboxedArrow : public Arrow
{
public:
	HitboxedArrow(const arrowStart& arrow);

	const BasicHitbox& get_hitbox() const override;

protected:
	void update_hitbox() override;

private:
	static prismInfo prism_from_arrow(const arrowStart& arrow);

private:
	PrismHitbox _hbx;
};

#endif //_HITBOXED_ARROW_HPP_
