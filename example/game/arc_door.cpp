#include "arc_door.hpp"

#include "../utils/geom.hpp"
#include "../motor/hitbox_fusion.hpp"
#include "../motor/paver_hitbox.hpp"
#include "castle_colors.hpp"

using namespace glm;


ArcDoor::ArcDoor(HitboxContext& context)
	: StoredVAObject(context, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW),
	  _arc_discr(4),
	  _hbx_arc_discr(2),
	  _wall_thickness(0.05),
	  _arc_begin(0.5),
	  _door_depth(0.8),
	  _wall_color(vec4(castle_colors::ARC_DOORS_EXT()
, 1)),
	  _door_color(vec4(castle_colors::ARC_DOORS_INSIDE()
, -1))
{
	_hbx_arc_discr = glm::min(_hbx_arc_discr, _arc_discr);
	this->build_base();
}

void ArcDoor::add_door(const arcDoor& door)
{
	this->get_array_buffer<DEP_SIZEY>().add_value(vec4(door.pos, door.size.y));
	this->get_array_buffer<DIR2D_SIZEXZ>().add_value(vec4(door.dir2D.x, door.dir2D.y, door.size.x, door.size.z));
	this->get_array_buffer<INST_COLOR>().add_value(door.wall_color);

	vec3 dirzN = vec3(door.dir2D.x, 0, door.dir2D.y); 
	vec3 dirxN = glm::cross(dirzN, vvv::y());
	vec3 B[2]  = {door.pos - dirxN*door.size.x/2.0f,
				  door.pos - dirxN*door.size.x/2.0f*(1.0f - _wall_thickness*2.0f)};

	auto total_hbx = std::make_unique<HitboxFusion>();

	total_hbx->fusion_hitbox<PaverHitbox>(paverInfo{
		.bottom  = B[1],
		.sized_x = B[0] - B[1],
		.sized_y = vvv::y()*_arc_begin*door.size.y,
		.sized_z = dirzN*door.size.z
	});

	for(uint i=0; i<_A.size()-1; i++)
	{
		vec3 Acur  = door.pos + dirxN*door.size.x*_A[i].x + vvv::y()*door.size.y*_A[i].y;
		vec3 Anext = door.pos + dirxN*door.size.x*_A[i+1].x + vvv::y()*door.size.y*_A[i+1].y;
		total_hbx->fusion_hitbox<PaverHitbox>(paverInfo{
			.bottom  = Acur,
			.sized_x = Anext - Acur,
			.sized_y = glm::normalize(door.pos - (Anext + Acur)/2.0f)*_wall_thickness*door.size.x,
			.sized_z = dirzN*door.size.z
		});
	}
	infinitePlan symmetry = {
		.point   = door.pos,
		.normalN = dirxN
	};

	// THE CLOSED DOOR TO BE IN COLLISION
	auto door_hbx = std::make_unique<HitboxFusion>();

	door_hbx->fusion_hitbox<PaverHitbox>(paverInfo{
		.bottom  = B[0],
		.sized_x = dirxN*door.size.x,
		.sized_y = vvv::y()*door.size.y*_arc_begin,
		.sized_z = dirzN*door.size.z*_door_depth
	});
	vec3 topdoorleft  = B[1] + vvv::y()*door.size.y*_arc_begin + dirzN*door.size.z*_door_depth;
	vec3 topdoorright = geom::symmetrical(topdoorleft, symmetry);
	door_hbx->fusion_hitbox<PrismHitbox>(prismInfo{.points={
		topdoorleft,
		topdoorright,
		(topdoorleft + topdoorright)/2.0f + vvv::y()*(1.0f - _arc_begin)*door.size.y,
		(topdoorleft + topdoorright)/2.0f - dirzN*door.size.z*_door_depth
	}});

	total_hbx->fusion_symmetric(symmetry);
	total_hbx->fusion_hitbox(std::move(door_hbx));

	_hitboxes.push_back(std::move(total_hbx));
	_hbx_context->add_static_hitbox(_hitboxes[_hitboxes.size()-1].get());
}

/*
          _ --A[]   -- C[0]
         -    _ U[] -- C[1]
        /    - 
       / .../
      /    /
     A[0] U[0]
     |    |
     |    |
     |    |
     |    |
     |    |
    B[0] B[1]          

     Y ^
       |
       |
       |
       |
       O--------> x
      /
     /
    Z
*/

ArcDoor::doorExtract ArcDoor::extract_base(PlanarSymmetry<glm::vec4>& sym, const doorExtract& base, float z, float dark)
{
	auto& relpos   = this->get_array_buffer<RELATIVE_POS>();
	auto& colors   = this->get_array_buffer<COLOR>();

	doorExtract ret;

	ret.B.x = sym.place_symmetrical(relpos[base.B.x] + vvv::z()*z, colors[base.B.x]*dark);
	ret.B.y = sym.place_symmetrical(relpos[base.B.y] + vvv::z()*z, colors[base.B.y]*dark);
	ret.C.x = sym.place_symmetrical(relpos[base.C.x] + vvv::z()*z, colors[base.C.x]*dark);
	ret.C.y = sym.place_symmetrical(relpos[base.C.y] + vvv::z()*z, colors[base.C.y]*dark);
	
	for(uint i=0; i<base.A.size(); i++)
	{
		ret.A.push_back(sym.place_symmetrical(relpos[base.A[i]] + vvv::z()*z, colors[base.A[i]]*dark));
		ret.U.push_back(sym.place_symmetrical(relpos[base.U[i]] + vvv::z()*z, colors[base.U[i]]*dark));
	}

	return ret;
}

ArcDoor::doorExtract ArcDoor::extract_base_colored(PlanarSymmetry<glm::vec4>& sym, const doorExtract& base, float z, const glm::vec4& color)
{
	auto& relpos   = this->get_array_buffer<RELATIVE_POS>();

	doorExtract ret;

	ret.B.x = sym.place_symmetrical(relpos[base.B.x] + vvv::z()*z, color);
	ret.B.y = sym.place_symmetrical(relpos[base.B.y] + vvv::z()*z, color);
	ret.C.x = sym.place_symmetrical(relpos[base.C.x] + vvv::z()*z, color);
	ret.C.y = sym.place_symmetrical(relpos[base.C.y] + vvv::z()*z, color);
	
	for(uint i=0; i<base.A.size(); i++)
	{
		ret.A.push_back(sym.place_symmetrical(relpos[base.A[i]] + vvv::z()*z, color));
		ret.U.push_back(sym.place_symmetrical(relpos[base.U[i]] + vvv::z()*z, color));
	}

	return ret;
}

void ArcDoor::dark_base(const doorExtract& base, float dark)
{
	auto& colors   = this->get_array_buffer<COLOR>();

	colors.change_subvalue(base.B[0], colors[base.B[0]]*dark);
	colors.change_subvalue(base.B[1], colors[base.B[1]]*dark);
	colors.change_subvalue(base.C[0], colors[base.C[0]]*dark);
	colors.change_subvalue(base.C[1], colors[base.C[1]]*dark);

	for(uint i=0; i<base.A.size(); i++)
	{
		colors.change_subvalue(base.A[i], colors[base.A[i]]*dark);
		colors.change_subvalue(base.U[i], colors[base.U[i]]*dark);
	}
}


void ArcDoor::build_base()
{
	auto& relpos   = this->get_array_buffer<RELATIVE_POS>();
	auto& colors   = this->get_array_buffer<COLOR>();
	auto& elements = this->get_elements();

	PlanarSymmetry<glm::vec4> sym(
		{vec3(0), vec3(1,0,0)},
		&relpos,
		&elements,
		&colors
	);

	float ex = 0.5;

	doorExtract base;

	base.B = vec2(sym.place_symmetrical(vec3(-ex, 0, 0), _wall_color),
		          sym.place_symmetrical(vec3(-ex + _wall_thickness, 0, 0), _wall_color));

	float dangle = float(M_PI/3.0)/float(_arc_discr);

	for(uint i=0; i<_arc_discr; i++)
	{
		float ra = 1;
		float rb = 1.0f - 2.0f*_wall_thickness;
		vec3 apoint = geom::circle_point(
			vec3(ex,_arc_begin, 0),
			vec3(-1,0,0),
			vec3(0,0,1),
			-dangle*float(i),
			ra
		);
		vec3 upoint = geom::circle_point(
			vec3(ex-_wall_thickness,_arc_begin, 0),
			vec3(-1,0,0),
			vec3(0,0,1),
			-dangle*float(i),
			rb
		);
		
		apoint.y = _arc_begin + (apoint.y - _arc_begin)/(ra*sin(M_PI/3.0))*(1.0f- _arc_begin);
		upoint.y = _arc_begin + (upoint.y - _arc_begin)/(rb*sin(M_PI/3.0))*(1.0f- _arc_begin-_wall_thickness);

		base.A.push_back(sym.place_symmetrical(apoint, _wall_color));
		base.U.push_back(sym.place_symmetrical(upoint, _wall_color));
	}

	base.C = vec2(sym.place_asymmetrical(vec3(0,1.0f,0), _wall_color),
			      sym.place_asymmetrical(vec3(0,1.0f - _wall_thickness,0), _wall_color));
	
	for(uint i=0; i<_hbx_arc_discr; i++)
	{
		uint a_indexf = (uint)(0.5f+float(i)/float(_hbx_arc_discr)*float(base.A.size()));
		vec3 a = relpos[base.A[a_indexf]];
 
		_A.push_back(vec2(a.x, a.y));
	}
	vec3 ca = relpos[base.C[0]];
	_A.push_back(vec2(ca.x, ca.y));

	doorExtract entry = this->extract_base(sym, base, 1);
	doorExtract door  = this->extract_base(sym, base, _door_depth, 0.4f);

	sym.link_rectangle(base.B[0] , entry.B[0], entry.A[0], base.A[0], false);
	sym.link_rectangle(entry.B[0], entry.A[0], entry.U[0], entry.B[1], true);
	sym.link_rectangle(door.B[1] , entry.B[1], entry.U[0], door.U[0], true);

	for(uint i=0; i<base.A.size()-1; i++)
	{
		sym.link_rectangle(base.A[i], entry.A[i], entry.A[i+1], base.A[i+1], false);
		sym.link_rectangle(door.U[i], entry.U[i], entry.U[i+1], door.U[i+1], true);
		sym.link_rectangle(entry.U[i], entry.U[i+1], entry.A[i+1], entry.A[i], false);
	}

	sym.link_rectangle(base.A[base.A.size()-1], entry.A[base.A.size()-1], entry.C[0], base.C[0], false);
	sym.link_rectangle(entry.U[base.A.size()-1], door.U[base.A.size()-1], door.C[1], entry.C[1], false);
	sym.link_rectangle(entry.U[base.A.size()-1], entry.C[1], entry.C[0], entry.A[base.A.size()-1], false);


	doorExtract realdoor =  this->extract_base_colored(sym, base, _door_depth, _door_color);
	for(uint i=0; i<realdoor.U.size()-1; i++)
	{
		sym.link_triangle(realdoor.B[1], realdoor.U[i], realdoor.U[i+1], true);
	}
	sym.link_triangle(realdoor.B[1], realdoor.U[realdoor.U.size()-1], realdoor.C[1], true);
	sym.link_triangle(realdoor.B[1], sym.symmetrical(realdoor.B[1]), realdoor.C[1]);


	this->dark_base(base, 0.5);
	colors.change_subvalue(entry.B[1], colors[entry.B[1]]*0.6f);
}

