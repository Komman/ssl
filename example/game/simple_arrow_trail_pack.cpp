#include "simple_arrow_trail_pack.hpp"

using namespace std;
using namespace glm;

std::unique_ptr<Uniform<float>> SimpleArrowTrailPack::trail_last_time = NULL;


SimpleArrowTrailPack::SimpleArrowTrailPack()
	: SimpleArrowPack(),
	  _trails(0),
	  _last_backpos(0),
	  _trail_last_time(1.6f)
{
	this->init_trail_last_time();
}

void SimpleArrowTrailPack::draw(GameDrawer& game_drawer)
{
	(*trail_last_time) = _trail_last_time;
	this->SimpleArrowPack::draw(game_drawer);
}

void SimpleArrowTrailPack::init_trail_last_time()
{
	if(trail_last_time == NULL)
	{
		trail_last_time = std::make_unique<Uniform<float>>(TRAIL_LAST_TIME_UNIFORM_NAME, 0.0f);
	} 
}


void SimpleArrowTrailPack::pre_add_arrow(const Arrow& arrow,
					  					 const simpleArrowGeometry& look)
{
	this->SimpleArrowPack::pre_add_arrow(arrow, look);

	auto config = this->get_fether_config(this->size());
	_trails.push_back(std::make_unique<ArrowTrail<float>>
		(config.base, config.plan, 0.995f));
	_last_backpos.push_back(arrow.get_position());
}

void SimpleArrowTrailPack::pre_pop_last_arrow()
{
	this->SimpleArrowPack::pre_pop_last_arrow();

	_trails.pop_back();
	_last_backpos.pop_back();
}

void SimpleArrowTrailPack::pre_clear()
{
	this->SimpleArrowPack::pre_clear();

	_trails.clear();
	_last_backpos.clear();
}

void SimpleArrowTrailPack::post_set_arrow(unsigned int index,
										  const Arrow& arrow,
					 					  const simpleArrowGeometry& look)
{
	this->SimpleArrowPack::post_set_arrow(index, arrow, look);

	if(_last_backpos[index] != arrow.get_back_position())
	{
		_trails[index]->set_trail(arrow.get_position() - arrow.get_length()*arrow.get_directionN(), arrow.get_directionN(), 0.0f);
		_last_backpos[index] = arrow.get_back_position();
	}
	// _trails[index]->shorten(ingame::gtime() - _trail_last_time);	

	const auto& times = _trails[index]->get_start_times();
	if(   times.size()>0 
	   && ingame::gtime() - times[times.size() - 1] > _trail_last_time)
	{
		_trails[index]->clear();
	}
} 

void SimpleArrowTrailPack::move_last_arrow(unsigned int dst_index)
{
	#ifdef SSL_DEBUG
	if(_trails.size() < 1)
	{
		ssl::err("SimpleArrowTrailPack::move_last_arrow(unsigned int dst_index) called while the vectr is empty");
	}
	#endif

	const auto& arr = this->get_arrow(this->size() - 1);

	this->SimpleArrowPack::post_set_arrow(dst_index, *(arr.arrow), arr.look);
	_trails[dst_index] = std::move(_trails[_trails.size() - 1]);
	_last_backpos[dst_index] = _last_backpos[_last_backpos.size() - 1];
	
	this->SimpleArrowPack::pre_pop_last_arrow();
	_trails.pop_back();
	_last_backpos.pop_back();
}


const std::vector<std::unique_ptr<ArrowTrail<float>>>& SimpleArrowTrailPack::get_trails() const
{
	return _trails;
}