#ifndef _ARC_DOOR_HPP_
#define _ARC_DOOR_HPP_

#include "../motor/physical_instances.hpp"
#include "../motor/planar_symmetry.hpp"
#include "../motor/stored_vao_object.hpp"
#include "../motor/hitbox_context.hpp"

struct arcDoor
{
	glm::vec3 pos;
	glm::vec2 dir2D; // IT IS A VEC2
	glm::vec3 size;
	glm::vec3 wall_color;
};

class ArcDoor : public StoredVAObject<PhysicalInstances<2,
										glm::vec3,
										glm::vec4,
										glm::vec4,
										glm::vec4,
										glm::vec3>>
{
public:
	ArcDoor(HitboxContext& context);

	void add_door(const arcDoor& door);

private:
	enum buffersRole {RELATIVE_POS=0, COLOR, DEP_SIZEY, DIR2D_SIZEXZ, INST_COLOR};

	struct doorExtract
	{
		glm::uvec2 B;
		glm::uvec2 C;
		std::vector<uint> A;
		std::vector<uint> U;
	};

	void build_base();
	doorExtract extract_base(PlanarSymmetry<glm::vec4>& sym, const doorExtract& base, float z, float dark = 1);
	doorExtract extract_base_colored(PlanarSymmetry<glm::vec4>& sym, const doorExtract& base, float z, const glm::vec4& color);
	void dark_base(const doorExtract& base, float dark);

private:
	uint _arc_discr;
	uint _hbx_arc_discr;

	float _wall_thickness;
	float _arc_begin;
	float _door_depth;

	std::vector<glm::vec2> _A;

	glm::vec4 _wall_color;
	glm::vec4 _door_color;
};

#endif //_ARC_DOOR_HPP_
