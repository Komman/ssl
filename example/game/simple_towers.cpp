#include "simple_towers.hpp"

#include "../motor/angular_symmetry.hpp"
#include "../motor/paver_hitbox.hpp"
#include "../motor/square_pyramid_hitbox.hpp"
#include "castle_colors.hpp"


using namespace glm;

SimpleTower::SimpleTower(HitboxContext& hbx_context, Lanterns& lanterns, VerticalShadower& vshadower)
	: CastleBuildingInst(hbx_context,
		{
			{},
			{valuedPolygon2DInfo{
				vec2(0),
				{
					vec2(-1, -1),
					vec2(-1, +1),
					vec2(+1, +1),
					vec2(+1, -1)
				},
				1
			}}
		},
		STATIC_DRAW,
		STATIC_DRAW,
		STATIC_DRAW,
		DYNAMIC_DRAW,
		STATIC_DRAW),
	  _vshadower(vshadower),
	  _lanterns(lanterns),
	  _towers_storage(),
	  _head_bot(-2.5),
	  _head_top(-4.5),
	  _base_color(castle_colors::SIMPLE_TOWER()),
	  _roof_color(castle_colors::SIMPLE_TOWER_ROOF()),
	  _win_color(castle_colors::SIMPLE_TOWER_WIN()),
	  _corner_color(castle_colors::SIMPLE_TOWER_CORNER()),
	  _corner_size(0.12, 0.4),
	  _y_sizes{vec2(0.90, 1.5), vec2(0.85, 2.0), vec2(0.75, -2.2), vec2(0.85, _head_bot), vec2(1.00, _head_top)},
	  _roof_size(1.2,-6),
	  _win_extr(0.05)
{
	this->build_relative_base();
}

uint SimpleTower::get_towers_amount() const
{
	return this->get_array_buffer<CENTERS_HEIGHT>().size();
}

void SimpleTower::set_tower_height(uint towerid, float h)
{
	#ifdef MOTOR_DEBUG
	if(this->get_hitboxes().size() != this->get_towers_amount())
	{
		mot::err("SimpleTower::set_tower_height(): hitboxes adding problem");
	}
	#endif

	if(towerid >= this->get_towers_amount())
	{
		mot::err("SimpleTower::set_tower_height(): towerid out of range");
	}

	auto& centh = this->get_array_buffer<CENTERS_HEIGHT>();
	centh.change_subvalue(towerid, vec4(centh[towerid].x, centh[towerid].y, centh[towerid].z, h));

	_towers_storage[towerid].moving_hitbox->tp(vvv::y()*h);
}

float SimpleTower::get_tower_height(uint towerid) const
{
	if(towerid >= this->get_towers_amount())
	{
		mot::err("SimpleTower::get_tower_height(): towerid out of range");
	}

	return this->get_array_buffer<CENTERS_HEIGHT>()[towerid].w;
}

glm::vec3 SimpleTower::get_tower_position(uint towerid) const
{
	if(towerid >= this->get_towers_amount())
	{
		mot::err("SimpleTower::get_tower_position(): towerid out of range");
	}
	
	auto& centh = this->get_array_buffer<CENTERS_HEIGHT>();
	return vec3(centh[towerid].x, centh[towerid].y, centh[towerid].z);
}

void SimpleTower::place_building_only(const simpleTower& tower, float h)
{
	auto t = tower;
	t.position.y += h;

	this->add_tower(t);
}

simpleTower SimpleTower::generate_building(const genParam& g)
{
	return {
		.position = vec3(g.position.x, 0.0f, g.position.y),
		.orientationN = vec3(g.dirN.x, 0, g.dirN.y),
		.height = g.maxsize,
		.size = g.maxsize*4.0f
	};
}

planarPlace SimpleTower::get_place(const simpleTower& g)
{
	return {
		.center = geom::projplan(g.position),
		.size   = vec2(g.size),
		.dirN   = geom::projplan(g.orientationN)
	};
}


void SimpleTower::add_tower(const simpleTower& tower)
{
	this->get_array_buffer<CENTERS_HEIGHT>().add_value(vec4(tower.position, tower.height));
	this->get_array_buffer<ORIENTATION_N_SIZE>().add_value(vec4(tower.orientationN, tower.size));

	_lanterns.add_lantern({
		.position   = tower.position + tower.orientationN*tower.size/2.0f + vvv::y()*2.5f*tower.size,
		.directionN = tower.orientationN,
		.color      = vec3(1.0,0.4,0.2),
		.size       = 1.2f*tower.size/3.0f
	});

	_vshadower.add_square(tower.position, tower.orientationN, vec2(tower.size)*1.2f);


	auto whole_tower = std::make_unique<HitboxFusion>(tower.position);

	vec3 dirx  = tower.orientationN;
	vec3 dirz  = glm::cross(tower.orientationN, vvv::y());
	vec3 sdiry = vvv::y()*(tower.size*abs(_roof_size.y)/2.0f + tower.height) - vvv::y()*tower.height;

	// I : STATIC PART
	// 0 : BASE
	whole_tower->fusion_hitbox<PaverHitbox>(paverInfo	{
		.bottom  = - (dirx+dirz)*tower.size*_y_sizes[0].x/2.0f,
		.sized_x = dirz*tower.size*_y_sizes[0].x,
		.sized_y = vvv::y()*tower.size*_y_sizes[0].y/2.0f,
		.sized_z = dirx*tower.size*_y_sizes[0].x
	});
	// 1 : BASE BLUFF
	whole_tower->fusion_hitbox<PaverHitbox>(paverInfo	{
		.bottom  = - ((dirx+dirz)*_y_sizes[1].x - vvv::y()*_y_sizes[0].y)*tower.size/2.0f,
		.sized_x = dirz*tower.size*_y_sizes[1].x,
		.sized_y = vvv::y()*tower.size*(_y_sizes[1].y -_y_sizes[0].y)/2.0f,
		.sized_z = dirx*tower.size*_y_sizes[1].x
	});

	// II : MOVING PART

	auto  moving_hbx = std::make_unique<HitboxFusion>(vvv::y()*tower.height);
	float max_height = 100.0f; 

	// 2 : CENTRAL PILER
	moving_hbx->fusion_hitbox<PaverHitbox>(paverInfo	{
		.bottom  = - ((dirx+dirz)*_y_sizes[2].x - vvv::y()*_y_sizes[1].y)*tower.size/2.0f - vvv::y()*(tower.height+max_height),
		.sized_x = dirz*tower.size*_y_sizes[2].x,
		.sized_y = vvv::y()*(tower.size*(abs(_y_sizes[3].y) -_y_sizes[1].y)/2.0f + tower.height + max_height),
		.sized_z = dirx*tower.size*_y_sizes[2].x
	});
	// 3 : HEAD
	moving_hbx->fusion_hitbox<PaverHitbox>(paverInfo	{
		.bottom  = - ((dirx+dirz)*_y_sizes[4].x - vvv::y()*abs(_y_sizes[3].y))*tower.size/2.0f + vvv::y()*tower.height - vvv::y()*tower.height,
		.sized_x = dirz*tower.size*_y_sizes[4].x,
		.sized_y = vvv::y()*(tower.size*(abs(_y_sizes[4].y) - abs(_y_sizes[3].y))/2.0f),
		.sized_z = dirx*tower.size*_y_sizes[4].x
	});
	// 4 : ROOF PILER
	moving_hbx->fusion_hitbox<SquarePyramidHitbox>(alignedSquarePyramidShape{
		.base_center  = vvv::y()*(abs(_head_top)*tower.size/2.0f + tower.height) - vvv::y()*tower.height, 
		.dir_base_XN  = dirx,
		.dir_base_YN  = dirz,
		.base_size_XY = vec2(tower.size*_roof_size.x),
		.top          = sdiry 
	});

	_towers_storage.push_back({tower, whole_tower->fusion_hitbox(std::move(moving_hbx))});

	_hitboxes.push_back(std::move(whole_tower));
	_hbx_context->add_hitbox(_hitboxes[_hitboxes.size()-1].get());
}

SimpleTower::towerStorage SimpleTower::get_tower_info(uint towerid) const
{
	if(towerid >= _towers_storage.size())
	{
		mot::err("SimpleTower::get_tower_info(): towerid out of range");
	}

	return _towers_storage[towerid];
}


uint SimpleTower::add_base_vertex(const glm::vec3& pos, const glm::vec3& color)
{
	auto& relpos = this->get_array_buffer<RELATIVE_POS>();
	auto& colors = this->get_array_buffer<COLORS>();

	relpos.add_value(pos);
	colors.add_value(color);

	return relpos.size() - 1;
}

SimpleTower::squareBase SimpleTower::build_square_base(float h, float size, const glm::vec3& color)
{
	squareBase ret;

	ret.push_back(this->add_base_vertex(vec3( size, h, size), color));
	ret.push_back(this->add_base_vertex(vec3(-size, h, size), color));
	ret.push_back(this->add_base_vertex(vec3(-size, h,-size), color));
	ret.push_back(this->add_base_vertex(vec3( size, h,-size), color));

	return ret;
}


SimpleTower::cubeBase SimpleTower::build_cube_base(float hbot, float htop, float size, const glm::vec3& color_bot, const glm::vec3& color_top)
{
	auto& relpos   = this->get_array_buffer<RELATIVE_POS>();
	auto& elements = this->get_elements();
	cubeBase ret;

	uint firsti = relpos.size();

	ret.first  = this->build_square_base(hbot, size, color_bot);
	ret.second = this->build_square_base(htop, size, color_top);

	elements.add_rectangle(firsti+0, firsti+4, firsti+5, firsti+1);
	elements.add_rectangle(firsti+1, firsti+5, firsti+6, firsti+2);
	elements.add_rectangle(firsti+2, firsti+6, firsti+7, firsti+3);
	elements.add_rectangle(firsti+3, firsti+7, firsti+4, firsti+0);

	return ret;
}

void SimpleTower::link_square_bases(const cubeBase& bot_top)
{
	auto& elements = this->get_elements();

	if(bot_top.first.size() != bot_top.second.size())
	{
		ssl::err("SimpleTower::link_square_bases(): bot and top of different sizes");
	}

	for(uint i=0; i<bot_top.first.size(); i++)
	{
		elements.add_rectangle(bot_top.first[i], bot_top.second[i], bot_top.second[(i+1)%bot_top.first.size()], bot_top.first[(i+1)%bot_top.first.size()]);
	}
}

void SimpleTower::link_square_bases(const cubeBase& bbot, const cubeBase& btop)
{
	cubeBase botop;
	botop.first  = bbot.second;
	botop.second = btop.first;

	this->link_square_bases(botop);
}

void SimpleTower::build_window(float head_bot, float head_top, float xdec)
{
	auto& relpos = this->get_array_buffer<RELATIVE_POS>();
	auto& colors = this->get_array_buffer<COLORS>();
	auto& elements = this->get_elements();

	AngularSymmetry<glm::vec3> sym(
		cylindricDivisor({.axe_point = vec3(0), .axe_directionN = vvv::y(), .divisor = 4}),
		&relpos,
		&elements,
		&colors
	);

	float hmiddle = (head_bot + head_top)/2.0f; 
	float wsize   = abs(head_bot - head_top)/4.0f;

	std::vector<uint> ids_in;
	std::vector<uint> ids_out;

	ids_in.push_back(sym.place_symmetrical(vec3(-wsize*0.1f + xdec, hmiddle - wsize, -1), _win_color));
	ids_in.push_back(sym.place_symmetrical(vec3(+wsize*0.1f + xdec, hmiddle - wsize, -1), _win_color));
	ids_in.push_back(sym.place_symmetrical(vec3(+wsize*0.1f + xdec, hmiddle + wsize, -1), _win_color));
	ids_in.push_back(sym.place_symmetrical(vec3(-wsize*0.1f + xdec, hmiddle + wsize, -1), _win_color));

	for(auto i : ids_in)
	{
		ids_out.push_back(sym.place_symmetrical(relpos[i] - vec3(0,0,_win_extr), _win_color*0.5f));
	}
	sym.link_rectangle(ids_out[0], ids_out[1], ids_out[2], ids_out[3]);
	
	if(ids_out.size() != ids_in.size())
	{
		ssl::err("Impossible here : " + SSL_FILE_AND_LINE);
	}

	for(uint i=0; i<ids_out.size(); i++)
	{
		sym.link_rectangle(ids_out[i], ids_in[i], ids_in[(i+1)%ids_out.size()], ids_out[(i+1)%ids_out.size()]);
	}
}

void SimpleTower::build_corner(float head_bot, float head_top)
{
	auto& relpos = this->get_array_buffer<RELATIVE_POS>();
	auto& colors = this->get_array_buffer<COLORS>();
	auto& elements = this->get_elements();

	AngularSymmetry<glm::vec3> sym(
		cylindricDivisor({.axe_point = vec3(0), .axe_directionN = vvv::y(), .divisor = 4}),
		&relpos,
		&elements,
		&colors
	);

	std::vector<uint> ids_top;
	std::vector<uint> ids_bot;

	float top_coeff = 0.5f;

	ids_top.push_back(sym.place_symmetrical(vec3(-1 - _corner_size.x, head_top, -1 - _corner_size.x), _corner_color*top_coeff));
	ids_top.push_back(sym.place_symmetrical(vec3(-1 + _corner_size.x, head_top, -1 - _corner_size.x), _corner_color*top_coeff));
	ids_top.push_back(sym.place_symmetrical(vec3(-1 + _corner_size.x, head_top, -1 + _corner_size.x), _corner_color*0.0f));
	ids_top.push_back(sym.place_symmetrical(vec3(-1 - _corner_size.x, head_top, -1 + _corner_size.x), _corner_color*top_coeff));

	for(auto i : ids_top)
	{
		ids_bot.push_back(sym.place_symmetrical(relpos[i] + vec3(_corner_size.x*0.8f,
																 _corner_size.y*abs(head_top-head_bot),
																 _corner_size.x*0.8f)
						  ,colors[i]/top_coeff));
	}
	sym.link_rectangle(ids_bot[0], ids_bot[1], ids_bot[2], ids_bot[3]);
	
	if(ids_bot.size() != ids_top.size())
	{
		ssl::err("Impossible here : " + SSL_FILE_AND_LINE);
	}

	for(uint i=0; i<ids_bot.size(); i++)
	{
		sym.link_rectangle(ids_bot[i], ids_top[i], ids_top[(i+1)%ids_bot.size()], ids_bot[(i+1)%ids_bot.size()]);
	}
}


void SimpleTower::build_relative_base()
{
	// auto& relpos = this->get_array_buffer<RELATIVE_POS>();
	// auto& colors = this->get_array_buffer<COLORS>();
	auto& elements = this->get_elements();

	float dark_low   = 0.9;
	float dark_inter = 0.77;

	cubeBase b1 = this->build_cube_base(0.0         ,  _y_sizes[0].y     , _y_sizes[0].x, _base_color*dark_low, _base_color*dark_low);
	cubeBase b2 = this->build_cube_base(_y_sizes[0].y, _y_sizes[1].y     , _y_sizes[1].x, _base_color*dark_low, _base_color);
	cubeBase b3 = this->build_cube_base(_y_sizes[1].y, _y_sizes[2].y-0.2f, _y_sizes[2].x, _base_color*dark_inter, _base_color*dark_inter);

	this->link_square_bases(b1, b2);
	this->link_square_bases(b2, b3);

	cubeBase b4 = this->build_cube_base(_y_sizes[2].y, _y_sizes[3].y, _y_sizes[3].x, _base_color, _base_color*dark_low);
	this->link_square_bases(b3, b4);

	cubeBase b5 = this->build_cube_base(_head_bot, _head_top, _y_sizes[4].x, _base_color, _base_color*0.7f);
	this->link_square_bases(b4, b5);

	squareBase sb1 = this->build_square_base(_head_top, 1.0, _roof_color*0.4f);
	squareBase sb2 = this->build_square_base(_head_top, _roof_size.x, _roof_color*0.8f);
	uint roofi     = this->add_base_vertex(vec3(0,_roof_size.y,0), _roof_color);

	this->link_square_bases({sb1, sb2});

	for(uint i=0; i<sb2.size(); i++)
	{
		elements.add_triangle(sb2[(i+1)%sb2.size()], sb2[i], roofi);
	}

	this->build_window(_head_bot, _head_top, +0.3);
	this->build_window(_head_bot, _head_top, -0.3);
	this->build_corner(_head_bot, _head_top);
}

