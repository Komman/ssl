#ifndef _CASTLE_SUPPORT_HPP_
#define _CASTLE_SUPPORT_HPP_

#include "../motor/angular_symmetry.hpp"
#include "../motor/vertical_shadower.hpp"
#include "../motor/hitbox_context.hpp"

#include "castle_building_inst.hpp"


struct castleSupport
{
	glm::vec3 center;
	glm::vec2 dirN2D;
	glm::vec3 size;
};

class CastleSupport : public CastleBuildingInst<castleSupport,
											 2,
											 glm::vec3,
											 glm::vec3,
											 glm::vec4,
											 glm::vec4>
{
public:
	CastleSupport(HitboxContext& hbx_context, VerticalShadower* vshadower);

	void add_support(const castleSupport& support);

	GETTER_BUILDER_COPY(float, thickness)

protected:
	void place_building_only(const castleSupport& building, float h) override;
	castleSupport generate_building(const genParam& g) override;
	planarPlace  get_place(const castleSupport& g) override;


private:
	enum bufferRoles {RELATIVE_POS=0, COLOR, POS_SIZEY, DIR2D_SIZEXZ};
	using SymType = AngularSymmetry<glm::vec3>;

	void build_base();
	void link_growbase(SymType& sym, uint i1, uint i2);

private:
	VerticalShadower* _vshadower;

	glm::vec3 _base_color;

	float _thickness;

	float _yB;
	float _xC;
	float _yD;
	float _yE;
	float _yF;
	float _xG;
	float _yH;
};

#endif //_CASTLE_SUPPORT_HPP_
