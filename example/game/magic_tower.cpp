#include "magic_tower.hpp"

#include "../utils/geom.hpp"
#include "../motor/cone_hitbox.hpp"
#include "../motor/cylinder_hitbox.hpp"
#include "castle_colors.hpp"

using namespace glm;


MagicTower::MagicTower(HitboxContext& hbx_context, StaticLigthor& slightor, VerticalShadower& vshadower)
	: StoredVAObject(hbx_context,
			castleGridPlacing{
				{valuedCircleInfo{vec2(0), 1, 1}},
				{}
			},
			STATIC_DRAW,
			STATIC_DRAW,
			STATIC_DRAW,
			STATIC_DRAW,
			STATIC_DRAW),
	  _slightor(slightor),
	  _vshadower(vshadower),
	  _circlediscr(6),
	  _base_color(castle_colors::MAGIC_TOWER_BASE()
),
	  _room_color(castle_colors::MAGIC_TOWER_ROOM()
),
	  _roof_color(castle_colors::MAGIC_TOWER_ROOF()
),
	  _win_color(castle_colors::MAGIC_TOWER_WIN()
),
	  _y_fristE(0.2),
	  _y_secE(0.25),
	  _x_fit(0.8),
	  _x_ffit(_x_fit-0.1),
	  _underoom(1,1.2),
	  _upperoom(1,4.2),
	  _roofext(_upperoom + vec2(0.5,0)),
	  _roofinter(_roofext.x/2.0f, _roofext.y+1.0f),
	  _generation({vec3(0), -1, -1})
{
	this->build_base();
}


void MagicTower::place_building_only(const magicTower& building, float h)
{
	auto nb = building;
	nb.position.y += h;

	this->add_tower(nb);
}

magicTower MagicTower::generate_building(const genParam& g)
{
	float antimax = 0.1f;

	return magicTower{
		.position = vec3(g.position.x, 0.0f, g.position.y),
		.height   = utils::random_float(g.maxsize*antimax*2.0f, g.maxsize*antimax*15.0f),
		.size     = g.maxsize*antimax
	};
}

planarPlace  MagicTower::get_place(const magicTower& g)
{
	return {
		.center = geom::projplan(g.position),
		.size   = vec2(g.size),
		.dirN   = vec2(1,0)
	};
}

void MagicTower::add_base_vertex(const glm::vec3& pos, const glm::vec3& color, float room)
{
	auto& relpos = this->get_array_buffer<RELATIVE_POS>();
	auto& colors = this->get_array_buffer<COLORS_ROOM>();

	relpos.add_value(pos);
	colors.add_value(vec4(color, room));
}

float MagicTower::room_center_height(const magicTower& tower)
{
	return tower.height + tower.size*1.6f;
}

void MagicTower::add_tower(const magicTower& tower, bool shadow)
{
	this->get_array_buffer<CENTERS_HEIGHT>().add_value(vec4(tower.position, tower.height));
	this->get_array_buffer<SIZE>().add_value(tower.size);

	if(shadow)
	{
		_vshadower.add_circle(tower.position, tower.size*1.1f);	
	}

	float angled = float(M_PI)/(_circlediscr);
	vec3 driN = vec3(cos(angled),0,sin(angled));
	auto total_hbx = std::make_unique<HitboxFusion>();

	total_hbx->fusion_hitbox<CylinderHitbox>(cylinderInfo{
		.bottom = tower.position,
		.to_top = vvv::y()*tower.height*_y_secE,
		.base_orientationN = driN,
		.radius = tower.size,
	}, _circlediscr);

	total_hbx->fusion_hitbox<CylinderHitbox>(cylinderInfo{
		.bottom = tower.position + vvv::y()*tower.height*_y_secE,
		.to_top = vvv::y()*(tower.height*(1.0f-_y_secE) + (_underoom.y-1.0f)*tower.size),
		.base_orientationN = driN,
		.radius = tower.size*_x_ffit,
	}, _circlediscr);

	total_hbx->fusion_hitbox<CylinderHitbox>(cylinderInfo{
		.bottom = tower.position + vvv::y()*(tower.height + (_underoom.y-1.0f)*tower.size),
		.to_top = vvv::y()*(_upperoom.y-1.0f)*tower.size,
		.base_orientationN = driN,
		.radius = tower.size*_upperoom.x,
	}, _circlediscr);

	const vec2& A = _roofext;
	const vec2& B = _roofinter;
	float ycomput = A.y - (B.y-A.y)/(B.x-A.x)*A.x;
	total_hbx->fusion_hitbox<ConeHitbox>(coneInfo{
		.bottom = tower.position + vvv::y()*(tower.height + (_upperoom.y-1.0f)*tower.size),
		.to_top = vvv::y()*(ycomput-_roofext.y)*tower.size,
		.base_orientationN = driN,
		.radius = tower.size*_roofext.x,
	}, _circlediscr);

	total_hbx->fusion_hitbox<ConeHitbox>(coneInfo{
		.bottom = tower.position + vvv::y()*(tower.height + (_roofinter.y-1.0f)*tower.size),
		.to_top = vvv::y()*(_roof_top_h - _roofinter.y)*tower.size,
		.base_orientationN = driN,
		.radius = tower.size*_roofinter.x,
	}, _circlediscr);

	_hitboxes.push_back(std::move(total_hbx));
	_hbx_context->add_static_hitbox(_hitboxes[_hitboxes.size()-1].get());

}

uint MagicTower::place_symmetrical(AngularSymmetry<glm::vec4>& sym, const glm::vec3& pos, const glm::vec3& color, float room)
{
	return sym.place_symmetrical(geom::circle_point(
		vec3(0, pos.y, 0),
		vec3(1,0,0),
		vec3(0,1,0),
		-float(2.0f*M_PI)/float(2*_circlediscr),
		pos.x),
		vec4(color, room)
	);
}

/*
	   Window
      3 ----- 2
 Y    |       |
      |       |
 ^    |       |
 |    0-------1
 |
 *---> Z

*/
void MagicTower::build_windows(AngularSymmetry<glm::vec4>& sym, float height_min, float height_max)
{
	auto& relpos   = this->get_array_buffer<RELATIVE_POS>();
	
	//                         Z    Y
	vec2 winsize_coeff = vec2(0.7, 0.5);
	float win_extr     = 0.05;
	uint  wind_count   = 6;

	float base_dark = 0.6f;

	float angle     = float(2.0f*M_PI) / float(_circlediscr);
	float sinangle  = sin(angle/2.0f);
	float winheight = winsize_coeff.y*(height_max-height_min);
	float windepth  = cos(angle/2.0f);
	float dheight_start =  (height_max-height_min)*(1.0f-winsize_coeff.y)/2.0f;

	vec3 leftwin_bot  = vec3(windepth, height_min + dheight_start, -sinangle*winsize_coeff.x); 
	vec3 rightwin_bot = vec3(windepth, height_min + dheight_start, +sinangle*winsize_coeff.x);
	vec3 rightwin_top = vec3(windepth, height_max - dheight_start, +sinangle*winsize_coeff.x);
	vec3 leftwin_top  = vec3(windepth, height_max - dheight_start, -sinangle*winsize_coeff.x); 

	uvec4 base_win;
	base_win[0] = sym.place_symmetrical(leftwin_bot , vec4(_win_color*base_dark, -1));
	base_win[1] = sym.place_symmetrical(rightwin_bot, vec4(_win_color*base_dark, -1));
	base_win[2] = sym.place_symmetrical(rightwin_top, vec4(_win_color*base_dark, -1));
	base_win[3] = sym.place_symmetrical(leftwin_top , vec4(_win_color*base_dark, -1));

	uvec4 base_win_extr;
	base_win_extr[0] = sym.place_symmetrical(leftwin_bot  + vvv::x()*win_extr, vec4(_win_color, -1));
	base_win_extr[1] = sym.place_symmetrical(rightwin_bot + vvv::x()*win_extr, vec4(_win_color, -1));
	base_win_extr[2] = sym.place_symmetrical(rightwin_top + vvv::x()*win_extr, vec4(_win_color, -1));
	base_win_extr[3] = sym.place_symmetrical(leftwin_top  + vvv::x()*win_extr, vec4(_win_color, -1));

	uvec4 base_win_int;
	base_win_int[0] = sym.place_symmetrical(relpos[base_win[0]]+ vec3(0,+1,+1)*win_extr, vec4(_win_color*base_dark, -1));
	base_win_int[1] = sym.place_symmetrical(relpos[base_win[1]]+ vec3(0,+1,-1)*win_extr, vec4(_win_color*base_dark, -1));
	base_win_int[2] = sym.place_symmetrical(relpos[base_win[2]]+ vec3(0,-1,-1)*win_extr, vec4(_win_color*base_dark, -1));
	base_win_int[3] = sym.place_symmetrical(relpos[base_win[3]]+ vec3(0,-1,+1)*win_extr, vec4(_win_color*base_dark, -1));

	uvec4 base_win_extr_int;
	base_win_extr_int[0] = sym.place_symmetrical(relpos[base_win_extr[0]] + vec3(0,+1,+1)*win_extr, vec4(_win_color, -1));
	base_win_extr_int[1] = sym.place_symmetrical(relpos[base_win_extr[1]] + vec3(0,+1,-1)*win_extr, vec4(_win_color, -1));
	base_win_extr_int[2] = sym.place_symmetrical(relpos[base_win_extr[2]] + vec3(0,-1,-1)*win_extr, vec4(_win_color, -1));
	base_win_extr_int[3] = sym.place_symmetrical(relpos[base_win_extr[3]] + vec3(0,-1,+1)*win_extr, vec4(_win_color, -1));

	for(uint i=0; i<4; i++)
	{
		sym.link_rectangle(base_win_extr[i], base_win_extr[(i+1)%4], base_win_extr_int[(i+1)%4], base_win_extr_int[i], true);
		sym.link_rectangle(base_win_extr[i], base_win_extr[(i+1)%4], base_win[(i+1)%4], base_win[i]);
		sym.link_rectangle(base_win_extr_int[i], base_win_extr_int[(i+1)%4], base_win_int[(i+1)%4], base_win_int[i], true);
	}

	float zmin = leftwin_bot.z  + win_extr;
	float zmax = rightwin_bot.z - win_extr;

	for(uint miniwin=1; miniwin<wind_count; miniwin++)
	{
		float h =  height_min + dheight_start;
		h += float(miniwin)/float(wind_count)*(winheight - win_extr);

		uvec4 bar_base;
		bar_base[0] = sym.place_symmetrical(vec3(windepth, h, zmin)           , vec4(_win_color*base_dark, -1));
		bar_base[1] = sym.place_symmetrical(vec3(windepth, h, zmax)           , vec4(_win_color*base_dark, -1));
		bar_base[2] = sym.place_symmetrical(vec3(windepth, h + win_extr, zmax), vec4(_win_color*base_dark, -1));
		bar_base[3] = sym.place_symmetrical(vec3(windepth, h + win_extr, zmin), vec4(_win_color*base_dark, -1));

		uvec4 bar_extr;
		bar_extr[0] = sym.place_symmetrical(vec3(windepth + win_extr, h, zmin)           , vec4(_win_color, -1));
		bar_extr[1] = sym.place_symmetrical(vec3(windepth + win_extr, h, zmax)           , vec4(_win_color, -1));
		bar_extr[2] = sym.place_symmetrical(vec3(windepth + win_extr, h + win_extr, zmax), vec4(_win_color, -1));
		bar_extr[3] = sym.place_symmetrical(vec3(windepth + win_extr, h + win_extr, zmin), vec4(_win_color, -1));

		sym.link_rectangle(bar_extr[0], bar_extr[1], bar_extr[2], bar_extr[3], true);
		sym.link_rectangle(bar_extr[0], bar_extr[1], bar_base[1], bar_base[0], false);
		sym.link_rectangle(bar_extr[3], bar_extr[2], bar_base[2], bar_base[3], true);
	}
}

/*
              
   0         / 
            / 
           /         
         / 
       /
     /     _roofext
        |    
        |
        | 
   1    |   _underoom
         \
          \    0
*/

void MagicTower::build_head(AngularSymmetry<glm::vec4>& sym, uint bot)
{
	// auto& colors   = this->get_array_buffer<COLORS_ROOM>();
	// auto& elements = this->get_elements();

	_roof_top_h = _roofinter.y + 3.0f;

	uint underoom_i  = this->place_symmetrical(sym, vec3(_underoom.x, _underoom.y, 0), _room_color*0.8f, 0);
	uint upperoom_i  = this->place_symmetrical(sym, vec3(_upperoom.x, _upperoom.y, 0), _room_color*1.0f, 1);
	uint underoof_i  = this->place_symmetrical(sym, vec3(_upperoom.x, _upperoom.y, 0), _roof_color*0.5f, 1);
	uint roofext_i   = this->place_symmetrical(sym, vec3(_roofext.x, _roofext.y, 0), _roof_color, 1);
	uint roofinter_i = this->place_symmetrical(sym, vec3(_roofinter.x, _roofinter.y, 0), _roof_color, 2);
	uint rooftop_i   = sym.place_asymmetrical(vec3(0, _roof_top_h, 0), vec4(_roof_color, 3));

	sym.link_rectangle(bot, sym.symmetrical(1, bot), sym.symmetrical(1, underoom_i), underoom_i);
	sym.link_rectangle(underoom_i, sym.symmetrical(1, underoom_i), sym.symmetrical(1, upperoom_i), upperoom_i);

	sym.link_rectangle(underoof_i, sym.symmetrical(1, underoof_i), sym.symmetrical(1, roofext_i), roofext_i);
	sym.link_rectangle(roofext_i, sym.symmetrical(1, roofext_i), sym.symmetrical(1, roofinter_i), roofinter_i);
	sym.link_triangle(roofinter_i, sym.symmetrical(1, roofinter_i), rooftop_i);

	this->build_windows(sym, _underoom.y, _upperoom.y);
}
/*


          | 1
          |
          |
   _x_fit | _y_secE
         / 
     1  /   _y_fristE
        |
        |
     1  |   0

*/

void MagicTower::build_base()
{
	auto& relpos   = this->get_array_buffer<RELATIVE_POS>();
	auto& colors   = this->get_array_buffer<COLORS_ROOM>();
	auto& elements = this->get_elements();

	AngularSymmetry<glm::vec4> sym(
		{
			.axe_point      = vec3(0),
			.axe_directionN = vec3(0,1,0),
			.divisor        = _circlediscr
		},
		&relpos,
		&elements,
		&colors
	);

	// vec2 : .x : size, .y : height
	vec2 base_shad = vec2(1.0, 0.1);
	vec2 base_extr = vec2(1.0, _y_fristE);
	vec2 base_fitI = vec2(_x_fit, _y_secE);
	vec2 base_fit  = vec2(_x_ffit, base_fitI.y);
	vec2 base_fit_top = vec2(_x_ffit, 1.0);

	uint base_shadi = this->place_symmetrical(sym, vec3(1,           0, 0), _base_color*0.4f, 0.0);
	uint base_bot   = this->place_symmetrical(sym, vec3(1, base_shad.y, 0), _base_color*0.8f, 0.0);
	uint base_top   = this->place_symmetrical(sym, vec3(1, base_extr.y, 0), _base_color, 0.0);
	
	uint base_fitiI    = this->place_symmetrical(sym, vec3(base_fitI.x, base_fitI.y,0), _base_color, 0.0);
	uint base_fiti_bot = this->place_symmetrical(sym, vec3(base_fit.x, base_fit.y,0), _base_color*0.8f, 0.0);
	
	uint base_fiti_top = this->place_symmetrical(sym, vec3(base_fit_top.x, base_fit_top.y,0), _base_color, 0.0);

	sym.link_rectangle(base_shadi, sym.symmetrical(1, base_shadi), sym.symmetrical(1, base_bot), base_bot);
	sym.link_rectangle(base_bot, sym.symmetrical(1, base_bot), sym.symmetrical(1, base_top), base_top);
	sym.link_rectangle(base_top, sym.symmetrical(1, base_top), sym.symmetrical(1, base_fitiI), base_fitiI);
	sym.link_rectangle(base_fitiI, sym.symmetrical(1, base_fitiI), sym.symmetrical(1, base_fiti_bot), base_fiti_bot);
	sym.link_rectangle(base_fiti_bot, sym.symmetrical(1, base_fiti_bot), sym.symmetrical(1, base_fiti_top), base_fiti_top);

	this->build_head(sym, base_fiti_top);
}

