#ifndef _CASTLE_EMPTY_INSIDE_HPP_
#define _CASTLE_EMPTY_INSIDE_HPP_

#include "castle_inside_face.hpp"

namespace CASTLE
{
	class EmptyInside : public InsideFace
	{
	public:
		EmptyInside(const std::vector<insideDoor>& doors_to_set, FaceStructure* outside, float height);

		virtual void build(const std::vector<insideDoor>& doors_to_set) override;

	private:
		float _height;
	};

};

#endif //_CASTLE_EMPTY_INSIDE_HPP_
