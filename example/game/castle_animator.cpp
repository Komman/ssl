#include "castle_animator.hpp"

namespace CASTLE
{
	CastleAnimator::CastleAnimator(const castleTools& tools, const glm::vec3& position)
		: CastlePlacer(tools, position)
	{

	}

	void CastleAnimator::animate(float dt)
	{
		this->watching_towers().animate(dt);
		this->moving_elevators().animate(dt);
	}
};

