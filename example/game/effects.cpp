#include "effects.hpp"

#include "../motor/gutils.hpp"
#include "../motor/light_intensity_valuator.hpp"

#include <memory>

namespace effects
{
	static DynamicLightor*  _dynamic_lightor = NULL;
	static TimedLightsStorer*  _timed_dynamic_lightor = NULL;
	static ShaderGenerator* _particles_gen   = NULL;
	static CloudsDispatchor* _could_dispatchor = NULL;
	static PackedCloudsDispatchor* _packed_could_dispatchor = NULL;
	static HoledDisc* _holed_disc = NULL;
	static ExploSphere* _explospheres = NULL;
	static ExploBibos* _explobibo = NULL;
	static MeshedRayLuminor* _meshedlight_luminor = NULL;

	static Drawer* _simple_trail_drawer = NULL;
	static Drawer* _explodisc_drawer = NULL;
	static Drawer* _explosphere_drawer = NULL;
	static Drawer* _explobibo_drawer = NULL;
	static Drawer* _meshedlight_drawer = NULL;

	void init()
	{
		if(_dynamic_lightor != NULL)
		{
			err("Call to effects::init() while it is already initialized");
		}

		_dynamic_lightor         = new DynamicLightor("dynamic_lightor", 20);
		_timed_dynamic_lightor   = new TimedLightsStorer();
		_particles_gen           = new ShaderGenerator("shaders/particles.glsl");
		_could_dispatchor        = new CloudsDispatchor(*_particles_gen);
		_packed_could_dispatchor = new PackedCloudsDispatchor(*_particles_gen, "packed_could_dispatchor");
		_holed_disc              = new HoledDisc(STATIC_DRAW, 15, "holed_disc");
		_explospheres            = new ExploSphere(std::vector<timedCubeInstance>(0), std::string("explospheres"));
		_explobibo               = new ExploBibos(std::string("explobibo"));
		_meshedlight_luminor     = new MeshedRayLuminor();

		_simple_trail_drawer     = new Drawer(*_particles_gen, "simple_trail_vertex", "simple_trail_fragment", {"all_lights_smooth"});
		_simple_trail_drawer->depth(true);
		_simple_trail_drawer->culling(false);
		// _simple_trail_drawer->blending(true);
		// _simple_trail_drawer->depth_mask(false);
		// _simple_trail_drawer->set_blend_equation(GL_MAX);

		_explodisc_drawer = new Drawer(*_particles_gen, "explodisc_vertex", "explodisc_fragment");
		_explodisc_drawer->depth(true);
		_explodisc_drawer->culling(false);

		_explosphere_drawer = new Drawer(*_particles_gen, "explosphere_vertex", "explosphere_fragment");
		_explosphere_drawer->depth(true);
		_explosphere_drawer->culling(false);
		
		_explobibo_drawer = new Drawer(*_particles_gen, "explobibo_vertex", "explobibo_fragment");
		_explobibo_drawer->depth(true);
		_explobibo_drawer->depth_function(GL_LEQUAL);
		_explobibo_drawer->depth_mask(false);
		_explobibo_drawer->blending(true);
		_explobibo_drawer->set_blend_function(GL_ONE);
		_explobibo_drawer->set_blend_equation(GL_FUNC_ADD);

		_meshedlight_drawer = new Drawer(*_particles_gen, "meshed_light_vertex", "meshed_light_fragment");
		_meshedlight_drawer->culling(false);
		_meshedlight_drawer->depth(true);
		_meshedlight_drawer->depth_mask(false);
		_meshedlight_drawer->blending(true);
		_meshedlight_drawer->set_blend_function(GL_ONE);
		_meshedlight_drawer->set_blend_equation(GL_FUNC_ADD);
	}

	void free()
	{
		if(_dynamic_lightor == NULL)
		{
			err("Call to effects::free() while it is already freed");
		}

		delete _particles_gen;
		delete _timed_dynamic_lightor;
		delete _dynamic_lightor;
		delete _could_dispatchor;
		delete _packed_could_dispatchor;
		delete _holed_disc;
		delete _explospheres;
		delete _explobibo;
		delete _meshedlight_luminor;

		delete _simple_trail_drawer;
		delete _explodisc_drawer;
		delete _explosphere_drawer;
		delete _explobibo_drawer;
		delete _meshedlight_drawer;
	}


	CloudsDispatchor& clouds()
	{
		#ifdef SSL_DEBUG
		if(_could_dispatchor == NULL)
		{
			ssl::err("call to effects::clouds() while effects::init() has not been called");
		}
		#endif

		return *_could_dispatchor;
	}
	PackedCloudsDispatchor& packed_clouds()
	{
		#ifdef SSL_DEBUG
		if(_packed_could_dispatchor == NULL)
		{
			ssl::err("call to effects::packed_clouds() while effects::init() has not been called");
		}
		#endif

		return *_packed_could_dispatchor;
	}

	DynamicLightor& dynamic_lightor()
	{
		#ifdef SSL_DEBUG
		if(_dynamic_lightor == NULL)
		{
			ssl::err("call to effects::dynamic_lightor() while effects::init() has not been called");
		}
		#endif

		return (*_dynamic_lightor);
	}

	TimedLightsStorer& timed_dynamic_lightor()
	{
		#ifdef SSL_DEBUG
		if(_timed_dynamic_lightor == NULL)
		{
			ssl::err("call to effects::timed_dynamic_lightor() while effects::init() has not been called");
		}
		#endif

		return (*_timed_dynamic_lightor);
	}

	HoledDisc& explodisc()
	{
		#ifdef SSL_DEBUG
		if(_holed_disc == NULL)
		{
			ssl::err("call to effects::explodisc() while effects::init() has not been called");
		}
		#endif

		return (*_holed_disc);
	}

	ExploSphere& explospheres()
	{
		#ifdef SSL_DEBUG
		if(_explospheres == NULL)
		{
			ssl::err("call to effects::explospheres() while effects::init() has not been called");
		}
		#endif

		return (*_explospheres);
	}

	ExploBibos& explobibos()
	{
		#ifdef SSL_DEBUG
		if(_explobibo == NULL)
		{
			ssl::err("call to effects::explobibos() while effects::init() has not been called");
		}
		#endif

		return (*_explobibo);
	}

	MeshedRayLuminor& meshedlight_luminor()
	{
		#ifdef SSL_DEBUG
		if(_meshedlight_luminor == NULL)
		{
			ssl::err("call to effects::meshedlight_luminor() while effects::init() has not been called");
		}
		#endif

		return (*_meshedlight_luminor);
	}







	Drawer& simple_trail_drawer()
	{
		#ifdef SSL_DEBUG
		if(_simple_trail_drawer == NULL)
		{
			ssl::err("call to effects::simple_trail_drawer() while effects::init() has not been called");
		}
		#endif

		return *_simple_trail_drawer;
	}

	Drawer& explodisc_drawer()
	{
		#ifdef SSL_DEBUG
		if(_explodisc_drawer == NULL)
		{
			ssl::err("call to effects::explodisc_drawer() while effects::init() has not been called");
		}
		#endif

		return *_explodisc_drawer;
	}

	Drawer& explosphere_drawer()
	{
		#ifdef SSL_DEBUG
		if(_explosphere_drawer == NULL)
		{
			ssl::err("call to effects::explosphere_drawer() while effects::init() has not been called");
		}
		#endif

		return *_explosphere_drawer;
	}

	Drawer& explobibo_drawer()
	{
		#ifdef SSL_DEBUG
		if(_explobibo_drawer == NULL)
		{
			ssl::err("call to effects::explobibo_drawer() while effects::init() has not been called");
		}
		#endif

		return *_explobibo_drawer;
	}

	Drawer& meshedlight_drawer()
	{
		#ifdef SSL_DEBUG
		if(_meshedlight_drawer == NULL)
		{
			ssl::err("call to effects::_meshedlight_drawer() while effects::init() has not been called");
		}
		#endif

		return *_meshedlight_drawer;
	}









	void add_debug_disc(const glm::vec3& pos, const glm::vec3& color, float size)
	{
		_holed_disc->add({
			.center = pos,
			.color  = color,
			.radius = size,
			.thickness = size/4.0f,
			.duration  = 2.0f,
			.speed     = size/2.0f
		});
	}

	void debug_point(const glm::vec3& point, float size, const glm::vec4& color)
	{
		effects::clouds().add_particle(Particle(
			point,
			utils::random_vec3(glm::vec3(-1.0f),glm::vec3(1.0f))*size,
			glm::vec3(0),
			color,
			size,
			0.1,
			1
		));
	}

	void debug_dirpoint(const glm::vec3& point, const glm::vec3& speed, float size, const glm::vec4& color)
	{
		effects::clouds().add_particle(Particle(
			point,
			speed,
			glm::vec3(0),
			color,
			size,
			0.1,
			1
		));
	}
};