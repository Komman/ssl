#ifndef _EFFECTS_HPP_
#define _EFFECTS_HPP_

#include "../motor/dynamic_lightor.hpp"
#include "../motor/timed_cube_instances.hpp"
#include "../motor/squared_bibolars.hpp"
#include "../motor/explo_meshed_ray_light.hpp"
#include "../motor/timed_lights_storer.hpp"

#include "ingame.hpp"
#include "holed_disc.hpp"
#include "clouds_dispatchor.hpp"

using ExploSphere = StoredVAObject<TimedCubeInstances>;
using ExploBibos  = StoredVAObject<SquaredBibolars>;

namespace effects
{
	using namespace ssl;

	void init();
	void free();

	// The color of the particle is its bloom multiplier
	// And if it is negative, the bloom is camera_distance-ajusted
	CloudsDispatchor& clouds();
	// The color of the particle is its bloom multiplier
	// And if it is negative, the bloom is camera_distance-ajusted
	PackedCloudsDispatchor& packed_clouds();
	DynamicLightor& dynamic_lightor();
	TimedLightsStorer& timed_dynamic_lightor();
	HoledDisc& explodisc();
	ExploSphere& explospheres();
	ExploBibos& explobibos();
	MeshedRayLuminor& meshedlight_luminor();

	Drawer& simple_trail_drawer();
	Drawer& explodisc_drawer();
	Drawer& explosphere_drawer();
	Drawer& explobibo_drawer();
	Drawer& meshedlight_drawer();



	void add_homogenous_explosion(const glm::vec3& position,
							  float intensity,
							  float duration,
							  const glm::vec3& explosion_color,
							  const glm::vec3& disc_color,
							  const glm::vec3& clouds_color,
							  const glm::vec3& opaque_light_color);

	
	enum exploColors {LIGHT_COLOR=0, SPHERE_COLOR, DISC_COLOR, CLOUDS_COLOR, BIBO_COLOR, LIGHT_PEAKS_COLOR, 
				      EXPLO_COLORS_AMOUNT};
	
	
	void add_explosion_light(const glm::vec3& position,
							 const glm::vec3& color,
							 float intensity,
							 float duration);
	
	void add_big_explosion_light(const glm::vec3& position,
							 const glm::vec3& color,
							 float intensity,
							 float duration);

	void particles_explosion(const glm::vec3& position,
							 float size,
							 float cloud_ejection,
							 const glm::vec3& color,
							 // negative coeff for constant halo while distance increases
							 float bloomcoef_short,
							 float bloomcoef_long,
							 float duration,
							 uint short_clouds_count,
							 uint long_clouds_count,
							 const glm::vec3& orientation,
							 float light_on_bloom_affection_coeff = 1.0f);

	void add_custom_explosion(const glm::vec3& position,
						      float intensity,
						      float duration,
						      const glm::vec3 colors[EXPLO_COLORS_AMOUNT],
						      float explodisc_radius, // =0 if no explosisc
						      float explodisc_thickness,
						      float clouds_impact,
						      uint  short_clouds_count,
						      uint  long_clouds_count,
						      float cloud_ejection,
						      float cloud_size,
						      float sphere_radius, //=0 if no spheres
						      float explobibo_size,//=0 if no bibo
						      uint  peaks_lights_amount, // it is dobbled by symmetry
						      float peaks_lights_angle);

	void add_debug_disc(const glm::vec3& pos,
						const glm::vec3& color = glm::vec3(1,0,0),
						float size = 4.0f);
	
	void debug_point(const glm::vec3& point, float size = 0.3f, const glm::vec4& color = glm::vec4(0.1,0.1,1,2));
	void debug_dirpoint(const glm::vec3& point, const glm::vec3& speed, float size = 0.3f, const glm::vec4& color = glm::vec4(0.1,0.1,1,2));
};

#endif //_EFFECTS_HPP_
