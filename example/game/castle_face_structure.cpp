#include "castle_face_structure.hpp"

#include "../motor/meshbuild.hpp"
#include "../motor/building.hpp"
#include "../motor/holed_wall_hitbox.hpp"
#include "../motor/geo_cylinder_hitbox.hpp"
#include "../motor/wall_hitbox.hpp"
#include "../motor/fan_hitbox.hpp"
#include "../motor/hanoi_hitbox.hpp"

#include "castle_base.hpp"
#include "castle_colors.hpp"
#include "castle_placer.hpp"
#include "castle_empty_inside.hpp"

using namespace std;
using namespace glm;

namespace CASTLE
{

	FaceStructure::FaceStructure(const baseBuiling& base, uint face)
		: _base(base.base),
		  _builds(base),
		  _face(face),
		  _outgoing_doors(),
		  _doors_placed(false),
		  _sizes({
		  	.windows = vec2(2.8f, 4.2f)*this->ND(),
		  	.pilers = vec2(2.8f, 1.12f)*this->ND(),
		  	.counterforts = vec3(1.82f,
		  						 -1.82f/this->counterfort_geom().size*this->counterfort_geom().height_startgirder,
		  						 1.82f/this->counterfort_geom().size*this->counterfort_geom().bot_center.z
			)*this->ND(),
		  	.loopholes = vec2(0.4f, 2.2f)*this->ND()
		  })
	{

	}

	std::unique_ptr<InsideFace> FaceStructure::build()
	{
		std::vector<Edge> walltobuild = this->architecture().face_edges(_face);

		std::vector<doorWall> to_build;

		for(const auto& e : walltobuild)
		{
			edgeDoorPosition doorpos = {
				.edge = {0,0},
				.altitude = 0.0f,
				.position = -1.0f,
				.height = 0.0f,
				.width = 0.0f
			};
			doorOnWall role = NO_DOOR;

			for(const auto& d : _incoming_doors)
			{
				if(d.edge.isomorphic(e))
				{
					doorpos = d;
					role = INCOMING;
					break;
				}
			}
			for(const auto& d : _outgoing_doors)
			{
				if(d.edge.isomorphic(e))
				{
					#ifdef MOTOR_DEBUG
					if(role != NO_DOOR)
					{
						mot::err("FaceStructure::build(): an edge as an incoming and outgoing door");
					}
					#endif

					doorpos = d;
					role = OUTGOING;
					break;
				}
			}

			to_build.push_back({
				.edge = e,
				.oface = this->architecture().opposite_face(e, _face),
				.descending = !(this->architecture().descending_edge(e, _face)),
				.dh = this->architecture().ascending_height(e, _face),
				.role = role,
				.door = doorpos,
				.infos = ((role != NO_DOOR) ? this->architecture().get_door_infos(e) : doorInfo()),
				.opbuild = this->architecture().get_structure(this->architecture().opposite_face(e, _face))
			});
		}

		this->build_face(to_build);
		this->build_wall_deco(to_build);

		std::vector<insideDoor> inside_doors = this->to_inside_doors(to_build);

		// TODO FOR BALCONS
		// this->add_additional_inside_doors(inside_doors);

		auto ret = this->inside_face(inside_doors);

		if(ret == NULL && !(_face == this->architecture().get_border_face()))
		{
			return std::make_unique<EmptyInside>(inside_doors, this, this->height()-this->floor_height());
		}
		else
		{
			return ret;
		}
	}

	std::unique_ptr<InsideFace> FaceStructure::inside_face(const std::vector<insideDoor>& doors)
	{
		return NULL;
		// return std::make_unique<InsideFace>(doors, this);
	}


	std::vector<insideDoor> FaceStructure::to_inside_doors(const std::vector<doorWall>& walls_to_build)
	{
		std::vector<insideDoor>  ret;

		for(const doorWall& d : walls_to_build)
		{	
			ret.push_back(insideDoor{
				.door = (d.role != NO_DOOR) && !(d.infos.place == OUTSIDE && d.role == OUTGOING),
				.type = d.infos.type,
				.pos = d.door
			});
			ret.back().pos.edge = d.edge;
		}

		return ret;
	}

	const globalArchi& FaceStructure::basoul() const
	{
		return _base->get_soul();
	}

	float FaceStructure::door_minh() const
	{
		return _base->get_params().door_height;
	}

	float FaceStructure::safe_height() const
	{
		return _base->get_params().safe_height;
	}

	float FaceStructure::safe_distance() const
	{
		return _base->get_params().safe_distance;
	}

	ArrayBuffer<glm::vec3>& FaceStructure::mesh_vertices()
	{
		return _builds.mesh->get_array_buffer<0>();
	}
	
	ArrayBuffer<glm::vec3>& FaceStructure::mesh_colors()
	{
		return _builds.mesh->get_array_buffer<1>();
	}

	ArrayBuffer<glm::vec3>& FaceStructure::inmesh_vertices()
	{
		return _builds.inside->get_array_buffer<0>();
	}
	
	ArrayBuffer<glm::vec3>& FaceStructure::inmesh_colors()
	{
		return _builds.inside->get_array_buffer<1>();
	}

	ElementBuffer& FaceStructure::get_elements()
	{
		return _builds.mesh->get_elements();
	}

	ElementBuffer& FaceStructure::get_inelements()
	{
		return _builds.inside->get_elements();
	}

	const glm::vec3& FaceStructure::walls_color() const
	{
		return _base->get_params().color;
	}

	const glm::vec3& FaceStructure::inside_color() const
	{
		return _base->get_params().incolor;
	}

	bool FaceStructure::is_ext_door(const doorWall& d)
	{
		return (d.role != NO_DOOR && d.infos.place == OUTSIDE);
	}

	float FaceStructure::facade_height(const Edge& e) const
	{
		ArrayBuffer<glm::vec3>& pos = _builds.mesh->get_array_buffer<0>();
		reliefEdge re = _base->edge_vertices_indices(e);

		return pos[re.first.top].y - pos[re.first.bot].y;
	}

	std::vector<FaceStructure::Edge> FaceStructure::descending_adjacent_edges() const
	{
		return this->architecture().descending_adjacent_edges(_face);
	}

	float FaceStructure::allowed_down(const Edge& e, float downh)
	{
		return std::max(this->facade_height(e), downh);
	}

	float FaceStructure::allowed_down()
	{
		std::vector<Edge> descending = this->descending_adjacent_edges();
	
		if(descending.size() == 0)
		{
			return -1.0f;
		}

		float m = this->allowed_down(descending[0], 0.0f);

		for(uint i=1; i<descending.size(); i++)
		{
			m = std::min(m, this->allowed_down(descending[i], 0.0f));
		}

		return m;
	}

	glm::vec3 FaceStructure::dirN(const edgeIndex& edgei) const
	{
		ArrayBuffer<glm::vec3>& pos = _builds.mesh->get_array_buffer<0>();
		
		return glm::normalize(pos[edgei.second] - pos[edgei.first]);
	}

	glm::vec3 FaceStructure::dirN(const indexedWall& wall) const
	{
		return this->dirN(wall.edgei);
	}

	std::vector<FaceStructure::doorWall> FaceStructure::extract_walls_to_build(const std::vector<indexedWall>& walls) const
	{
		std::vector<doorWall> ret;

		for(const indexedWall& w : walls)
		{
			ret.push_back(w.wall);
		}

		return ret;
	}

	std::vector<uint> FaceStructure::extract_indices(const std::vector<indexedWall>& walls) const
	{
		std::vector<uint> ret;

		for(const indexedWall& w : walls)
		{
			ret.push_back(w.edgei.first);
		}

		return ret;
	}

	std::vector<indexRange> FaceStructure::ext_miniborders(const std::vector<indexedWall>& walls, float min_dh) const
	{
		return this->ext_miniborders(this->extract_walls_to_build(walls), min_dh);
	}

	std::vector<indexRange> FaceStructure::ext_miniborders(const std::vector<doorWall>& walls_to_build, float min_dh) const
	{
		if(walls_to_build.size() < 3)
		{
			mot::err("FaceStructure::ext_miniborders(): face of size <3");
		}

		std::vector<indexRange> ret;
		uint start = 0;
		uint N = walls_to_build.size();
		bool descending = true;
		bool onedescending = false;

		for(uint i=0; i<N; i++)
		{
			if(walls_to_build[i].dh < min_dh)
			{
				onedescending = true;

				if(!descending)
				{
					start = i;
					break;
				}
			}
			else
			{
				descending = false;
			}
		}
		if(descending)
		{
			return {{0, N-1}};
		}
		if(!onedescending)
		{
			return {};
		}

		uint modi=start;
		while(modi < N+start)
		{
			ret.push_back({modi, modi});

			while(walls_to_build[modi%N].dh < min_dh)
			{
				modi++;
			}

			ret[ret.size()-1].second = modi - 1;

			while(!(walls_to_build[modi%N].dh < min_dh))
			{
				modi++;
			}
		}

		return ret;
	}

	Base* FaceStructure::get_base() const
	{
		return _base;
	}

	uint FaceStructure::get_face() const
	{
		return _face;
	}

	float FaceStructure::ND() const
	{
		return this->architecture().geom_params().ND;
	}

	edgeIndex FaceStructure::edge_vertices_indices(const Edge& e) const
	{
		return _base->edge_vertices_indices(e, _face);
	}

	std::vector<FaceStructure::indexedWall> FaceStructure::index_wall(const std::vector<doorWall>& walls_to_build, const std::vector<uint>& startindices) const
	{
		if(walls_to_build.size() != startindices.size())
		{
			mot::err("FaceStructure::index_wall(): the two vectors must have the same size");
		}

		std::vector<indexedWall> ret;
		uint N = walls_to_build.size();

		for(uint i=0; i<N; i++)
		{
			uint i1 = i;
			uint i2 = (i+1)%N;

			ret.push_back({
				.wall = walls_to_build[i],
				.edgei = {startindices[i1], startindices[i2]}
			});
		}

		return ret;
	}

	float FaceStructure::floor_height()
	{
		return this->architecture().get_height(this->architecture().get_border_face());
	}

	std::vector<edgeDoorPosition> FaceStructure::compute_doors_building(const std::vector<edgeDoorPosition>& imposed_doors,
											   							const std::vector<Edge> doors_to_set)
	{
		std::vector<edgeDoorPosition> ret;

		for(const Edge& e : doors_to_set)
		{
			float h = this->floor_height();

			if(this->architecture().get_door_place(e) == OUTSIDE)
			{
				h = this->height();
			}

			ret.push_back(edgeDoorPosition{
				.edge = e,
				.altitude = h,
				.position = 0.5f,
				#warning Value of height and width should be compute (to ensures there is no overlapping)
				.height   = this->door_minh()*1.2f,
				.width    = this->door_minh()*1.0f
			});
		}

		return ret;
	}

	bool FaceStructure::vertices_colors_aligned() const
	{
		return (_builds.mesh->get_array_buffer<0>().size()   == _builds.mesh->get_array_buffer<1>().size())
			&& (_builds.inside->get_array_buffer<0>().size() == _builds.inside->get_array_buffer<1>().size());
	}

	builtStructures& FaceStructure::build_structures()
	{
		return _base->build_structures();
	}


	float FaceStructure::height() const
	{
		return this->architecture().get_height(_face);
	}

	float FaceStructure::wall_radius() const
	{
		return _base->get_params().wall_radius;
	}

	float FaceStructure::min_roof_h() const
	{
		return this->architecture().geom_params().min_roof_h;
	}

	float FaceStructure::floor_h() const
	{
		return this->architecture().geom_params().floor_h;
	}

	float FaceStructure::minwidth() const
	{
		return _base->get_params().minwidth;
	}


	bool FaceStructure::are_door_placed() const
	{
		return _doors_placed;
	}

	doorPlace FaceStructure::door_place(const Edge& e)
	{
		return this->architecture().get_door_place(e);
	}

	glm::vec3 FaceStructure::barycenter() const
	{
		return geom::mean(_builds.mesh->get_array_buffer<0>().subset(this->mesh_vertices_indices()));
	}

	void FaceStructure::place_conspace_wall_hitbox(const std::vector<glm::vec3>& cycle, float dy, float enlargement)
	{
		wallInfo wall;

		if(enlargement < 0.0f)
		{
			wall = {
				.excycle = cycle,
				.incycle = geom::conspace_enlarge_cycle(cycle, vec3(0), enlargement, abs(enlargement)),
				.sized_direction = vvv::y()*dy
			};
		}
		else
		{
			wall = {
				.excycle = geom::conspace_enlarge_cycle(cycle, vec3(0), enlargement, abs(enlargement)),
				.incycle = cycle,
				.sized_direction = vvv::y()*dy
			};
		}
		this->add_hitbox(std::make_unique<WallHitbox>(wall));
	}

	void FaceStructure::place_bissetrice_wall_hitbox(const std::vector<glm::vec3>& cycle, float dy, float enlargement)
	{
		wallInfo wall;

		if(enlargement < 0.0f)
		{
			wall = {
				.excycle = cycle,
				.incycle = geom::bissectrice_enlarge_cycle(cycle, vec3(0), enlargement),
				.sized_direction = vvv::y()*dy
			};
		}
		else
		{
			wall = {
				.excycle = geom::bissectrice_enlarge_cycle(cycle, vec3(0), enlargement),
				.incycle = cycle,
				.sized_direction = vvv::y()*dy
			};
		}
		this->add_hitbox(std::make_unique<WallHitbox>(wall));
	}

	void FaceStructure::place_conspace_wall_hitbox(const std::vector<uint>& cycle, float dy, float enlargement)
	{
		this->place_conspace_wall_hitbox(_builds.mesh->get_array_buffer<0>().subset(cycle), dy, enlargement);
	}

	void FaceStructure::place_conspace_wall_hitbox(const std::pair<uint, uint>& cycle, float dy, float enlargement)
	{
		this->place_conspace_wall_hitbox(utils::uirange(cycle), dy, enlargement);
	}

	void FaceStructure::place_bissetrice_wall_hitbox(const std::vector<uint>& cycle, float dy, float enlargement)
	{
		this->place_bissetrice_wall_hitbox(_builds.mesh->get_array_buffer<0>().subset(cycle), dy, enlargement);
	}

	void FaceStructure::place_bissetrice_wall_hitbox(const std::pair<uint, uint>& cycle, float dy, float enlargement)
	{
		this->place_bissetrice_wall_hitbox(utils::uirange(cycle), dy, enlargement);
	}

	void FaceStructure::place_ingeocylinder_hitbox(const std::vector<uint>& cycle_indices, const glm::vec3& sized_direction)
	{
		geoCylinderInfo cylinder = {
			.cycle = this->inmesh_vertices().subset(cycle_indices),
			.sized_direction = sized_direction
		};
		this->add_hitbox(std::make_unique<GeoCylinderHitbox>(cylinder));
	}

	void FaceStructure::place_geocylinder_hitbox(const std::vector<uint>& cycle_indices, const glm::vec3& sized_direction)
	{
		geoCylinderInfo cylinder = {
			.cycle = this->mesh_vertices().subset(cycle_indices),
			.sized_direction = sized_direction
		};
		this->add_hitbox(std::make_unique<GeoCylinderHitbox>(cylinder));
	}

	void FaceStructure::set_doors_positions(const std::vector<edgeDoorPosition>& imposed_doors,
											const std::vector<Edge>& doors_to_set)
	{
		_incoming_doors = imposed_doors;

		if(_doors_placed)
		{
			mot::err("FaceStructure::set_doors_positions(): try to set position of doors already placed");
		}

		_outgoing_doors = this->compute_doors_building(imposed_doors, doors_to_set);

		if(_outgoing_doors.size() != doors_to_set.size())
		{
			mot::err("FaceStructure::set_doors_positions(): the doors_to_set and the returned doors places have not the same doors amount");
		}

		_doors_placed = true;
	}

	const std::vector<edgeDoorPosition>& FaceStructure::outgoing_door_positions() const
	{
		if(!_doors_placed)
		{
			mot::err("FaceStructure::outgoing_door_positions(): try to get position of doors that are not placed yet");
		}

		return _outgoing_doors;
	}

	const TheoricalArchitecture& FaceStructure::architecture() const
	{
		return _base->architecture();
	}


	/*
		============================
		TOOLS FOR STRUCTURE BUILDING
		============================
	*/

	void FaceStructure::build_face(const std::vector<doorWall>& walls_to_build)
	{
		this->fill_walls(walls_to_build);

		// std::vector<indexedWall> insidewalls = this->wall_inside_base_vertices(walls_to_build, this->floor_h(), this->wall_radius());
		
		// insidewalls = this->build_inside_walls(insidewalls, this->height()-this->floor_h(), this->inside_color());

		meshbuild::fill_cycle(
			this->mesh_vertices(),
			this->get_elements(),
			this->mesh_vertices_indices(),
			true
		);

		// meshbuild::fill_cycle(
		// 	this->inmesh_vertices(),
		// 	this->get_inelements(),
		// 	this->topcycle(insidewalls),
		// 	false
		// );
	}

	bool FaceStructure::deco_need_to_avoid_door(const doorWall& w, float bot_y)
	{
		if(!this->is_ext_door(w))
			return false;

		return bot_y < w.door.height;
	}

	static float compute_max_x(const std::vector<glm::vec3>& XYshadowsBT)
	{
		float maxX = 0.0f;
		for(uint i=0; i<XYshadowsBT.size(); i++)
		{
			maxX = std::max(maxX, XYshadowsBT[i].x);
		}

		return maxX;
	}

	float FaceStructure::topdeco_height() const
	{
		return std::max(_sizes.windows.y, _sizes.loopholes.y);
	}

	void FaceStructure::build_wall_deco(const std::vector<doorWall>& walls_to_build)
	{
		ArrayBuffer<glm::vec3>& pos = _builds.mesh->get_array_buffer<0>();
		float size = this->wall_radius()*1.4f;
		uint N = walls_to_build.size();

		for(uint i=0; i<N; i++)
		{
			doorWall w = walls_to_build[i];

			if(w.dh > size + this->safe_height())
			{
				wallInfos infos = this->wall_infos(w);
				uint rd = rand()%4;

				if(rd==0)
				{
					this->place_pilers_range(w, 0.0f);
				}
				if(rd==1)
				{
					this->place_counterforts_range(w, 0.0f);
				}
				if(rd==2)
				{
					this->place_window_range(w, this->topfaceh(w)-_sizes.windows.y);
				}
				if(rd==3)
				{
					this->place_loophole_range(w, this->topfaceh(w)-_sizes.loopholes.y);
				}
			}
		}
	}

	float FaceStructure::closest_lantern(const glm::vec3& pos)
	{
		auto& lanterns = *(this->get_base()->build_structures().lanterns);
		auto  lpos = lanterns.lanterns_positions();

		if(lpos.size() == 0)
		{
			mot::err("FaceStructure::closest_lantern(): no laterns");
		}
		float dmin = glm::distance(lpos[0], pos);

		for(const auto& p : lpos)
		{
			float d = glm::distance(p, pos);

			if(d<dmin)
			{
				dmin = d;
			}
		}

		return dmin;
	}

	bool FaceStructure::try_lantern(const glm::vec3& pos, const glm::vec3& dirN)
	{
		float h = this->height()-pos.y;

		if(glm::dot(this->get_base()->get_params().tools->sky->get_sunlight_direction(), dirN) < 0
		&& this->closest_lantern(pos) >= 40.0f*this->ND())
		{
			this->get_base()->build_structures().lanterns->add_custom_lantern(lanternDisposition{
					.position   = pos,
					.directionN = dirN,
					.color = glm::vec3(0.3,0.4,0.8),
					.size = this->player_size()
				},
				{
					glm::vec3(0.9,0.6,0.3),
					40.0f*this->ND(),
					500.0f*this->ND(),
					1.5f*this->ND()
				}
			);

			return true;
		}

		return false;
	}

	const FaceStructure::detailSizes& FaceStructure::sizes() const
	{
		return _sizes;
	}

	const counterFort& FaceStructure::counterfort_geom() const 
	{
		return _base->build_structures().counter_forts->get_geometry();
	}

	float FaceStructure::counterfort_minh() const
	{
		return _sizes.counterforts.y;
	}

	void FaceStructure::place_pilers_range(const doorWall& w, float bot_y, float spacing_coeff)
	{
		wallInfos infos = this->wall_infos(w);
		float size = _sizes.pilers.x;
		this->place_pilers_range(w, size, bot_y, this->topfaceh(w)*0.85f, size*spacing_coeff);
	}

	std::vector<planarYPlace> FaceStructure::place_counterforts_range(const doorWall& w, float bot_y)
	{
		wallInfos infos = this->wall_infos(w);
		return this->place_counterforts_range(w, _sizes.counterforts.x, bot_y, infos.height-this->safe_height()-bot_y, _sizes.counterforts.x*2.3f);
	}

	void FaceStructure::place_window_range(const doorWall& w, float bot_y)
	{
		wallInfos infos = this->wall_infos(w);
		float size = _sizes.windows.x;
		this->place_window_range(w, size, bot_y, _sizes.windows.y, size);
	}

	void FaceStructure::place_loophole_range(const doorWall& w, float bot_y)
	{
		wallInfos infos = this->wall_infos(w);
		this->place_loophole_range(w, _sizes.loopholes.x, bot_y, _sizes.loopholes.y, _sizes.loopholes.y*1.4f);
	}

	void FaceStructure::place_pilers_range(const doorWall& w, float bot_y, float margin, float interspace, const std::vector<glm::vec3>& XYshadowsBT, float slopeh, float zflat)
	{
		float maxX = compute_max_x(XYshadowsBT);

		if(XYshadowsBT.size() < 2)
		{
			mot::err("FaceStructure::place_pilers_range(): not a piler because XYshadowsBT.size() < 2");
		}
		
		wallInfos    infos = this->wall_infos(w);
		vector<vec3> pos   = this->sample_decorations(w, bot_y, margin, maxX+interspace, maxX*zflat, true, true);

		for(const vec3& v : pos)
		{
			this->place_fullsided_wallpiler(v, infos.normalN, XYshadowsBT, zflat, slopeh, this->walls_color());
		}
	}

	void FaceStructure::place_pilers_range(const doorWall& w, float width, float bot_y, float height, float interspace)
	{
		float zflat = _sizes.pilers.y/_sizes.pilers.x;
		
		vector<vec3> xyshadow = {
			vec3(width*1.0f, height*0.00f, 0.7f),
			vec3(width*1.0f, height*0.20f, 1.0f),
			vec3(width*0.8f, height*0.20f, 0.84f),
			vec3(width*0.8f, height*0.95f-width, 0.94f)
		};

		this->place_pilers_range(w, bot_y, width*0.7f, interspace, xyshadow, width, zflat);
	}

	std::vector<planarYPlace> FaceStructure::place_counterforts_range(const doorWall& w, float width, float bot_y, float height, float interspace)
	{
		std::vector<planarYPlace> ret;

		float csize = _base->build_structures().counter_forts->get_geometry().size;
		float size  = width/csize;
		float extd  = _base->build_structures().counter_forts->get_geometry().bot_center.z*size;
		extd += csize*size/2.0f;

		wallInfos    infos = this->wall_infos(w);
		vector<vec3> pos   = this->sample_decorations(w, bot_y+height, width, width+interspace, extd);

		for(const vec3& v : pos)
		{		
			planarYPlace pos = {
				.center = v,
				.size   = vec3(size, height, size),
				.dirN   = -geom::projplan(infos.normalN)
			};

			this->build_structures().counter_forts->add_static_instance(pos);
			
			pos.center.y = this->height()+bot_y;
			pos.center += -infos.normalN*(extd+csize*size/2.0f);
			ret.push_back(pos);
		}

		return ret;
	}

	void FaceStructure::place_window_range(const doorWall& w, float width, float bot_y, float height, float interspace)
	{
		wallInfos    infos = this->wall_infos(w);
		vector<vec3> pos   = this->sample_decorations(w, bot_y, width, width+interspace, width*0.1f, this->deco_need_to_avoid_door(w, bot_y-height/2.0f), false);

		for(const vec3& v : pos)
		{		
			planarYPlace pos = {
				.center = v,
				.size   = vec3(width, height, width*0.2f),
				.dirN   = -geom::projplan(infos.normalN)
			};

			this->build_structures().face_arcs->add_static_instance(pos);
		}
	}

	void FaceStructure::place_loophole_range(const doorWall& w, float width, float bot_y, float height, float interspace)
	{
		wallInfos    infos = this->wall_infos(w);
		vector<vec3> pos   = this->sample_decorations(w, bot_y + _sizes.loopholes.y/2.0f, width*2.0f, width+interspace, width*0.1f, this->deco_need_to_avoid_door(w, bot_y-_sizes.loopholes.y/2.0f), false);

		for(const vec3& v : pos)
		{		
			building::square_base::top(
				this->mesh_vertices(),
				this->get_elements(),
				orientedPlan{
					.point = v-infos.normalN*width*0.05f,
					.dirxN = vvv::y(),
					.diryN = infos.dirxN*(_face == this->architecture().get_border_face() ? -1.0f : 1.0f)
				},
				vec2(height, width*0.2f)
			);
		}

		this->complete_color_buffer(vec3(0));
	}

	FaceStructure::wallInfos FaceStructure::wall_infos(const doorWall& w) const
	{
		if(w.descending)
		{
			mot::err("FaceStructure::wall_infos(): called on descending wall");
		}

		auto down = this->down_position(w.edge);
		auto top  = this->up_position(w.edge);

		wallInfos ret;
		ret.bot     = {down[0], down[1]},
		ret.top     = {top[0] , top[1] },
		ret.dirxN   = glm::normalize(down[1] - down[0]);
		ret.normalN = -geom::trigo_normal(ret.dirxN); 
		ret.length  = glm::length(down[1] - down[0]); 
		ret.height  = abs(w.dh); 

		if(_face == this->architecture().get_border_face())
		{
			ret.normalN *= -1.0f;
		}

		return ret;
	}

	std::vector<glm::vec3> FaceStructure::sample_decorations(const doorWall& w, float height_from_bot, float margin, float width, float extdepth)
	{
		return this->sample_decorations(w, height_from_bot, margin, width, extdepth, false, false);
	}
	
	void FaceStructure::place_debug_piler(const glm::vec3& pos, const glm::vec3& color)
	{	
		building::paver::topsided(this->mesh_vertices(),
			this->get_elements(),
			ycentredPaverInfo{
				.bottomy = pos,
				.sized_x = vvv::x()*0.5f,
				.sized_y = vvv::y()*50.0f,
				.sized_z = vvv::z()*0.5f
		});
		this->complete_color_buffer(color);
	}

	// static bool tt = false;

	bool FaceStructure::in_face(const glm::vec3& p)
	{
		// if(tt)
		// this->place_debug_piler(p);

		return geom::point_in_polygon3D(p, this->mesh_vertices().subset(this->mesh_vertices_indices()));
	}


	bool FaceStructure::square_in_face(const glm::vec3& p, const glm::vec3& dirxN, float size)
	{
		vec3 dirzN = geom::trigo_normal(dirxN);

		return this->in_face(p) 
			&& this->in_face(p + (+ dirxN + dirzN)*size/2.0f) 
			&& this->in_face(p + (- dirxN + dirzN)*size/2.0f) 
			&& this->in_face(p + (- dirxN - dirzN)*size/2.0f) 
			&& this->in_face(p + (+ dirxN - dirzN)*size/2.0f);
	}

	std::vector<glm::vec3> FaceStructure::sample_decorations(const doorWall& w, float height_from_bot, float margin, float width, float extdepth, bool avoid_door, bool forcemiddle)
	{
		wallInfos infos = this->wall_infos(w);
		vec3 p1 = infos.top.first;
		vec3 p2 = infos.top.second;

		float Lt = glm::length(p2 - p1);
		float L  = Lt-2.0f*margin;
		float Y = this->height();
		int amount = L/width;
		float amountf = amount;
		float resti = (L-amountf*width)/(amountf+1.0f);

		if(amount < 0)
		{
			return {};
		}
		std::vector<glm::vec3> ret;

		if(amount == 0 && forcemiddle && !avoid_door)
		{
			vec3 pos = geom::yfixed(geom::middle(p1, p2), Y+height_from_bot);
			// this->place_debug_piler(pos, vec3(0,1,0));
			return {pos};
		}

		for(int i=0; i<amount; i++)
		{
			float d = margin + width/2.0f + resti + float(i)*(width+resti);
			vec3 p    = p1 + infos.dirxN*d;
			vec3 pext1 = p  - infos.normalN*extdepth - infos.dirxN*width/2.0f;
			vec3 pext2 = p  - infos.normalN*extdepth + infos.dirxN*width/2.0f;

			// building::paver::topsided(this->mesh_vertices(), this->get_elements(), ycentredPaverInfo{pext, vvv::x()*0.1f, vvv::y()*20.0f, vvv::z()*0.1f});
			// this->complete_color_buffer(vec3(1,0,0));

			if(_face == this->architecture().get_border_face()
			|| (this->in_face(pext1) && this->in_face(pext2)))
			{
				if(avoid_door
				&& this->is_ext_door(w)
				&& d+width/2.0f>w.door.position*Lt-w.door.width/2.0f
				&& d-width/2.0f<w.door.position*Lt+w.door.width/2.0f)
				{
					// this->place_debug_piler(vec3(p.x, Y + height_from_bot, p.z));
					continue;
				}

				ret.push_back(vec3(p.x, Y + height_from_bot, p.z));
			}
			else
			{
				// if(!geom::point_in_polygon3D(pext1, this->mesh_vertices().subset(this->mesh_vertices_indices()))) this->place_debug_piler(vec3(pext1.x, Y + height_from_bot, pext1.z), vec3(0,0,1));
				// if(!geom::point_in_polygon3D(pext2, this->mesh_vertices().subset(this->mesh_vertices_indices()))) this->place_debug_piler(vec3(pext2.x, Y + height_from_bot, pext2.z), vec3(0,0,1));
			}
		}

		return ret;
	}


	std::vector<uint> FaceStructure::topcycle(const std::vector<indexedWall>& walls) const
	{
		std::vector<uint> ret;

		for(const auto& w : walls)
		{
			ret.push_back(w.edgei.first);
		}

		return ret;
	}

	void FaceStructure::check_buffer_sizes()
	{
		if(!this->vertices_colors_aligned())
		{
			mot::err("FaceStructure::check_buffer_sizes(): buffers not aligned");
		}
	}

	std::vector<uint> FaceStructure::mesh_vertices_indices() const
	{
		return _base->face_vertices_indices(_face);
	}

	std::vector<uint> FaceStructure::mesh_vertices_indices(const std::vector<doorWall>& walls_to_build) const
	{
		std::vector<uint> ret;
		for(const auto& w : walls_to_build)
		{
			ret.push_back(this->edge_vertices_indices(w.edge).first);
		}

		return ret;
	}

	std::pair<uint, uint> FaceStructure::extrude_cycle(const std::vector<uint>& cycle,
								    		const glm::vec3& sized_direction,
								    		const glm::vec3& color,
								    		bool flip_face)
	{
		if(!this->vertices_colors_aligned())
		{
			mot::err("FaceStructure::extrude_cycle(): buffers not aligned");
		}

		auto ret = meshbuild::extrude_cycle(
			this->mesh_vertices(),
			this->get_elements(),
			cycle,
			sized_direction,
			flip_face
		);

		this->complete_color_buffer(color);

		return ret;
	}

	std::pair<uint, uint> FaceStructure::extrude_cycle(const std::pair<uint, uint>& cycle,
								    		const glm::vec3& sized_direction,
								    		const glm::vec3& color,
								    		bool flip_face)
	{
		return this->extrude_cycle(utils::uirange(cycle), sized_direction, color, flip_face);
	}


	std::pair<uint, uint> FaceStructure::bissectrice_enlarge_cycle(const std::vector<uint>& cycle,
									    				float distance,
									    				const glm::vec3& translation,
								    					const glm::vec3& color,
									    				bool flip_face)
	{
		if(!this->vertices_colors_aligned())
		{
			mot::err("FaceStructure::bissectrice_enlarge_cycle(): buffers not aligned");
		}

		auto ret = meshbuild::bissectrice_enlarge_cycle(
			this->mesh_vertices(),
			this->get_elements(),
			cycle,
			distance,
			translation,
			flip_face
		);

		this->complete_color_buffer(color);

		return ret;
	}



	std::pair<uint, uint> FaceStructure::conspace_enlarge_cycle(
		const std::vector<uint>& cycle,
    	float distance,
    	float curve_precision_distance,
    	const glm::vec3& translation,
    	const glm::vec3& color,
    	bool flip_face,
    	bool fill)
	{
		auto ret = meshbuild::conspace_enlarge_cycle(
			this->mesh_vertices(),
			this->get_elements(),
			cycle,
			distance,
			translation, 
			flip_face, 
			curve_precision_distance,
			fill
		);
		this->complete_color_buffer(color);

		return ret;
	}


	std::pair<uint, uint> FaceStructure::bissectrice_enlarge_cycle(const std::pair<uint, uint>& cycle,
									    				float distance,
									    				const glm::vec3& translation,
								    					const glm::vec3& color,
									    				bool flip_face)
	{
		return this->bissectrice_enlarge_cycle(utils::uirange(cycle), distance, translation, color, flip_face);
	}

	std::pair<uint, uint> FaceStructure::centerdir_enlarge_cycle(const std::vector<uint>& cycle,
							    				float distance,
							    				const glm::vec3& translation,
						    					const glm::vec3& color,
							    				bool flip_face)
	{
		auto ret = meshbuild::pointdirected_extruding(
			this->mesh_vertices(),
			this->get_elements(), 
			cycle,
			geom::mean(this->mesh_vertices().subset(cycle)),
			-distance,
			translation,
			flip_face);
		this->complete_color_buffer(color);

		return ret;
	}

	std::pair<uint, uint> FaceStructure::centerdir_enlarge_cycle(const std::pair<uint, uint>& cycle,
							    				float distance,
							    				const glm::vec3& translation,
						    					const glm::vec3& color,
							    				bool flip_face)
	{
		return this->centerdir_enlarge_cycle(utils::uirange(cycle), distance, translation, color, flip_face);
	}

	Pair<glm::vec3> FaceStructure::down_position(const Edge& e) const
	{
		reliefEdge re = _base->edge_vertices_indices(e);

		return {_builds.mesh->get_array_buffer<0>()[re.first.bot], _builds.mesh->get_array_buffer<0>()[re.second.bot]};
	}

	Pair<glm::vec3> FaceStructure::up_position(const Edge& e) const
	{
		reliefEdge re = _base->edge_vertices_indices(e);

		return {_builds.mesh->get_array_buffer<0>()[re.first.top], _builds.mesh->get_array_buffer<0>()[re.second.top]};
	}

	float FaceStructure::player_size()
	{
		return this->get_base()->get_params().tools->player->MAX_SIZE;
	}

	float FaceStructure::topfaceh(const doorWall& w) const
	{
		return (this->wall_infos(w).height)-(this->safe_height());
	}

	void FaceStructure::fill_walls(const std::vector<doorWall> walls)
	{
		for(const doorWall& w : walls)
		{
			if(w.descending)
			{
				if(w.role != NO_DOOR && w.infos.place == OUTSIDE)
				{
					this->fill_wall_with_door(w);
				}
				else
				{
					this->fill_wall(w.edge);
				}				
			}
		}
	}

	void FaceStructure::fill_wall(const Edge& e)
	{
		reliefEdge re = _base->edge_vertices_indices(e);

		_builds.mesh->get_elements().add_rectangle(
			re.first.top,
			re.first.bot,
			re.second.bot,
			re.second.top,
			true
		);
	}

	void FaceStructure::fill_wall_with_door(const doorWall& e)
	{
		// {
		// 	Pair<glm::vec3> edgepos = this->down_position(e.edge);
		// 	glm::vec3 dir = edgepos[1] - edgepos[0];

		// 	_base->build_structures().arc_doors->add_door(arcDoor{
		// 		.pos        = geom::yfixed(glm::mix(edgepos[0], edgepos[1], e.door.position), e.door.altitude),
		// 		.dir2D      = -glm::normalize(geom::trigo_normal(geom::projplan(dir))),
		// 		.size       = glm::vec3(glm::min(glm::length(dir)*0.2f, this->door_minh()*0.7f), this->door_minh(), this->door_minh()*0.5f),
		// 		.wall_color = glm::vec3(0.5)
		// 	});
		// 	this->fill_wall(e.edge);

		// 	return;
		// }

		reliefEdge re = _base->edge_vertices_indices(e.edge);
		ArrayBuffer<glm::vec3>& pos = _builds.mesh->get_array_buffer<0>();
		ArrayBuffer<glm::vec3>& colors = _builds.mesh->get_array_buffer<1>();

		float x = e.door.position;
		float y = e.door.altitude;

		/*
			v1 --- v4
			|       |
			|       |
			|       |
			v2 --- v3
		*/

		uint i1 = re.first.top ;
		uint i2 = re.first.bot ;
		uint i3 = re.second.bot;
		uint i4 = re.second.top;

		// vec3 v1 = pos[i1];
		vec3 v2 = pos[i2];
		vec3 v3 = pos[i3];
		// vec3 v4 = pos[i4];
		
		float totalength = glm::length(v3 - v2);
		float doorl = e.door.width;

		vec3 color = (colors[re.first.top]+colors[re.first.bot]+colors[re.second.bot]+colors[re.second.top])/4.0f;

		glm::vec3 dirXN = glm::normalize(v3 - v2);

		uint j1 = _builds.mesh->add_vertex(geom::yfixed(v2+dirXN*(totalength*x - doorl/2.0f), y+e.door.height*1.0f), color);
		uint j2 = _builds.mesh->add_vertex(geom::yfixed(v2+dirXN*(totalength*x - doorl/2.0f), y+e.door.height*0.0f), color);
		uint j3 = _builds.mesh->add_vertex(geom::yfixed(v2+dirXN*(totalength*x + doorl/2.0f), y+e.door.height*0.0f), color);
		uint j4 = _builds.mesh->add_vertex(geom::yfixed(v2+dirXN*(totalength*x + doorl/2.0f), y+e.door.height*1.0f), color);

		_builds.mesh->get_elements().add_rectangle(i1, i2, j2, j1, true);
		_builds.mesh->get_elements().add_rectangle(i2, i3, j3, j2, true);
		_builds.mesh->get_elements().add_rectangle(i3, i4, j4, j3, true);
		_builds.mesh->get_elements().add_rectangle(i4, i1, j1, j4, true);
		
		float L = 0.5f;
		uint start = pos.size();

		dooredWall door = {
			.bot1 = pos[j2] - dirXN*L + vvv::y()*0.0f,
			.bot2 = pos[j3] + dirXN*L + vvv::y()*0.0f,
			.diryN = vvv::y(),
			.inside_dir1N = -geom::trigo_normal(dirXN),
			.inside_dir2N = -geom::trigo_normal(dirXN),
			.thickness = L,
			.height = e.door.height+L,
			.door_xpos = 0.5f,
			.door_size = glm::distance(pos[j2], pos[j3]),
			.door_height = e.door.height
		};

		building::doored_wall::frontbridged(pos, _builds.mesh->get_elements(), door, true);
	
		this->complete_color_buffer(this->inside_color());
		for(uint i=0; i<4; i++) {colors.change_subvalue(start+i, colors[start+i]*0.7f);}

		this->add_hitbox(std::make_unique<HoledWallHitbox>(door));
	}

	void FaceStructure::complete_color_buffer(const glm::vec3& color, float randomness)
	{
		BaseMesh& mesh = *(_builds.mesh);
		while(mesh.get_array_buffer<1>().size() < mesh.get_array_buffer<0>().size())
		{
			mesh.get_array_buffer<1>().add_value(color*utils::random_float(1.0f-randomness, 1.0f));
		}
		
		BaseMesh& inside = *(_builds.inside);
		while(inside.get_array_buffer<1>().size() < inside.get_array_buffer<0>().size())
		{
			inside.get_array_buffer<1>().add_value(color*utils::random_float(1.0f-randomness, 1.0f));
		}
	}

	void FaceStructure::add_hitbox(std::unique_ptr<BasicHitbox>&& hbx)
	{
		_builds.hbxs->push_back(std::move(hbx));
	}

	// std::vector<FaceStructure::indexedWall> FaceStructure::wall_inside_base_vertices(const std::vector<doorWall>& walls_to_build, float height, float wall_size)
	// {
	// 	ArrayBuffer<glm::vec3>& pos = _builds.inside->get_array_buffer<0>();
	// 	std::vector<indexedWall> ret;
	// 	uint starti = pos.size();

	// 	for(uint i=0; i<walls_to_build.size(); i++)
	// 	{
	// 		const auto& w = walls_to_build[i];

	// 		uint u = (i+walls_to_build.size()-1)%walls_to_build.size();

	// 		glm::vec3 v1 = geom::yfixed(this->down_position(walls_to_build[u].edge)[0], height);
	// 		glm::vec3 v2 = geom::yfixed(this->down_position(w.edge)[0], height);
	// 		glm::vec3 v3 = geom::yfixed(this->down_position(w.edge)[1], height);

	// 		pos.add_value(geom::yfixed(v2, height)-geom::trigo_bissectriceN(v1, v2, v3, vvv::y())*wall_size);
	// 		ret.push_back({
	// 			.wall  = w,
	// 			.edgei = {starti+i, uint(starti+((i+1)%walls_to_build.size()))}
	// 		});
	// 	}
	// 	this->complete_color_buffer(this->inside_color());

	// 	return ret;
	// }

	// std::vector<FaceStructure::indexedWall> FaceStructure::build_inside_walls(const std::vector<indexedWall>& walls, float height, const glm::vec3& color)
	// {
	// 	std::vector<indexedWall> ret;
	// 	this->check_buffer_sizes();

	// 	for(uint i=0; i<walls.size(); i++)
	// 	{
	// 		indexedWall w = walls[i];

	// 		if(w.wall.role != NO_DOOR)
	// 		{
	// 			float dh = w.wall.door.altitude - this->floor_h();
	// 			if(dh <= 0.0001f)
	// 			{
	// 				w = this->build_inside_cut_wall_facade(w, w.wall.door.position, w.wall.door.width, w.wall.door.height, color, false);
	// 				w = this->build_inside_wall_facade(w, height-w.wall.door.height, color);
	// 			}
	// 			else
	// 			{
	// 				if(w.wall.door.altitude + w.wall.door.height < this->height())
	// 				{
	// 					this->put_floor(dh, castle_colors::FLOOR());
	// 					this->put_roof(dh, castle_colors::FLOOR()*0.4f);

	// 					w = this->build_inside_wall_facade(w, dh, color);
	// 					w = this->build_inside_cut_wall_facade(w, w.wall.door.position, w.wall.door.width, w.wall.door.height, color, true);
	// 					w = this->build_inside_wall_facade(w, height-dh-w.wall.door.height, color);
	// 				}
	// 				else
	// 				{
	// 					w = this->build_inside_wall_facade(w, height, color);
	// 				}
	// 			}	
	// 		}
	// 		else
	// 		{
	// 			w = this->build_inside_wall_facade(w, height, color);
	// 		}
	// 		ret.push_back(w);
	// 	}

	// 	this->put_floor(0.0f, castle_colors::FLOOR());

	// 	return ret;
	// }

	// FaceStructure::indexedWall FaceStructure::build_inside_wall_facade(const indexedWall& wall, float height, const glm::vec3& color)
	// {
	// 	ArrayBuffer<glm::vec3>& inpos  = _builds.inside->get_array_buffer<0>();
	// 	ElementBuffer& elements     = this->get_inelements();
		
	// 	vec3 down1 = geom::yfixed(this->down_position(wall.wall.edge)[0], inpos[wall.edgei.first ].y);
	// 	vec3 down2 = geom::yfixed(this->down_position(wall.wall.edge)[1], inpos[wall.edgei.second].y);

	// 	uint i1 = inpos.add_value(inpos[wall.edgei.first] + vvv::y()*height);
	// 	uint i2 = inpos.add_value(inpos[wall.edgei.second] + vvv::y()*height);
	// 	this->complete_color_buffer(color);

	// 	elements.add_rectangle(wall.edgei.first, wall.edgei.second, i2, i1, false);
	
	// 	transPaverInfo paver = {
	// 		.square_base = {
	// 			inpos[wall.edgei.first],
	// 			inpos[wall.edgei.second],
	// 			down2,
	// 			down1
	// 		},
	// 		.sized_direction = vvv::y()*height
	// 	};
	// 	this->add_hitbox(std::make_unique<PaverHitbox>(paver));


	// 	return indexedWall{.wall = wall.wall, .edgei = edgeIndex{i1, i2}};
	// }

	// FaceStructure::indexedWall FaceStructure::build_inside_cut_wall_facade(const indexedWall& wall, float cut_center_coeff, float cut_width, float height, const glm::vec3& color, bool fill_cutfloor)
	// {
	// 	ArrayBuffer<glm::vec3>& inpos  = _builds.inside->get_array_buffer<0>();
	// 	ElementBuffer& elements     = this->get_inelements();

	// 	vec3 down1 = geom::yfixed(this->down_position(wall.wall.edge)[0], inpos[wall.edgei.first ].y);
	// 	vec3 down2 = geom::yfixed(this->down_position(wall.wall.edge)[1], inpos[wall.edgei.second].y);

	// 	vec3  dirN      = down2 - down1;
	// 	float wallength = glm::length(dirN);
	// 	dirN = dirN/wallength;

		
	// 	/*
	// 		           OUTSIDE
	// 			j1 --- j2    j3 --- j4
	// 		           |     |
	// 		   i1 ---- i2    i3 ------------ i4
	// 		            INSIDE
	// 	*/

	// 	uint j1 = inpos.add_value(down1 + dirN*(wallength*0.0f             + cut_width*0.0f));
	// 	uint j2 = inpos.add_value(down1 + dirN*(wallength*cut_center_coeff - cut_width*0.5f));
	// 	uint j3 = inpos.add_value(down1 + dirN*(wallength*cut_center_coeff + cut_width*0.5f));
	// 	uint j4 = inpos.add_value(down1 + dirN*(wallength*1.0f             + cut_width*0.0f));
		
	// 	// uint j1up = inpos.add_value(inpos[j1] + vvv::y()*height);
	// 	uint j2up = inpos.add_value(inpos[j2] + vvv::y()*height);
	// 	uint j3up = inpos.add_value(inpos[j3] + vvv::y()*height);
	// 	// uint j4up = inpos.add_value(inpos[j4] + vvv::y()*height);
		
	// 	uint i1 = wall.edgei.first;
	// 	uint i4 = wall.edgei.second;
	// 	uint i2 = inpos.add_value(geom::trigo_crossing_problem(inpos[j1], inpos[j4], inpos[i1], inpos[i4], inpos[j2]));
	// 	uint i3 = inpos.add_value(geom::trigo_crossing_problem(inpos[j1], inpos[j4], inpos[i1], inpos[i4], inpos[j3]));

	// 	uint i1up = inpos.add_value(inpos[i1] + vvv::y()*height);
	// 	uint i2up = inpos.add_value(inpos[i2] + vvv::y()*height);
	// 	uint i3up = inpos.add_value(inpos[i3] + vvv::y()*height);
	// 	uint i4up = inpos.add_value(inpos[i4] + vvv::y()*height);
		
	// 	this->complete_color_buffer(color);

	// 	elements.add_rectangle(i1, i2, i2up, i1up, false);
	// 	elements.add_rectangle(i4, i3, i3up, i4up, true);

	// 	elements.add_rectangle(j2, i2, i2up, j2up, true);
	// 	elements.add_rectangle(j3, i3, i3up, j3up, false);

	// 	elements.add_rectangle(j2up, j3up, i3up, i2up, false);
		
	// 	if(fill_cutfloor)
	// 	{
	// 		elements.add_rectangle(j2, j3, i3, i2, true);
	// 	}
	
	// 	transPaverInfo paver1 = {
	// 		.square_base = {
	// 			inpos[j1],
	// 			inpos[j2],
	// 			inpos[i2],
	// 			inpos[i1]
	// 		},
	// 		.sized_direction = vvv::y()*height
	// 	};
	// 	transPaverInfo paver2 = {
	// 		.square_base = {
	// 			inpos[j3],
	// 			inpos[j4],
	// 			inpos[i4],
	// 			inpos[i3]
	// 		},
	// 		.sized_direction = vvv::y()*height
	// 	};
	// 	this->add_hitbox(std::make_unique<PaverHitbox>(paver1));
	// 	this->add_hitbox(std::make_unique<PaverHitbox>(paver2));

	// 	return indexedWall{.wall = wall.wall, .edgei = edgeIndex{i1up, i4up}};
	// }

	// void FaceStructure::build_inside_walls(const std::vector<doorWall>& walls_to_build, float wall_size, float floor_h, float roof_h, bool place_roof, float inside_wall_overhegiht)
	// {
	// 	if(floor_h + this->door_minh() > roof_h)
	// 	{
	// 		mot::err("FaceStructure::build_inside_walls(): inside not enough tall");
	// 	}

	// 	ArrayBuffer<glm::vec3>& pos = _builds.inside->get_array_buffer<0>();
	// 	std::vector<uint> tofill;

	// 	for(uint i=0; i<walls_to_build.size(); i++)
	// 	{
	// 		const auto& w = walls_to_build[i];

	// 		uint u = (i+walls_to_build.size()-1)%walls_to_build.size();
	// 		uint j = (i+1)%walls_to_build.size();

	// 		glm::vec3 v1 = geom::yfixed(this->down_position(walls_to_build[u].edge)[0], floor_h);
	// 		glm::vec3 v2 = geom::yfixed(this->down_position(w.edge)[0], floor_h);
	// 		glm::vec3 v3 = geom::yfixed(this->down_position(w.edge)[1], floor_h);
	// 		glm::vec3 v4 = geom::yfixed(this->down_position(walls_to_build[j].edge)[1], floor_h);

	// 		float h = roof_h;

	// 		tofill.push_back(pos.size());
	// 		pos.add_value(geom::yfixed(v2, h)-geom::trigo_bissectriceN(v1, v2, v3, vvv::y())*wall_size);

	// 		h = h - this->architecture().geom_params().floor_h;

	// 		dooredWall d = {
	// 			.bot1 = v2,// + vvv::y()*200.0f,
	// 			.bot2 = v3,// + vvv::y()*200.0f,
	// 			.diryN = vvv::y(),
	// 			.inside_dir1N = -geom::trigo_bissectriceN(v1, v2, v3, vvv::y()),
	// 			.inside_dir2N = -geom::trigo_bissectriceN(v2, v3, v4, vvv::y()),
	// 			.thickness = this->wall_radius(),
	// 			.height = h,
	// 			.door_xpos = 0.5f,
	// 			.door_size = w.door.width,
	// 			.door_height = w.door.height
	// 		};

	// 		if(d.buildable())
	// 		{
	// 			uint starti = pos.size();

	// 			building::doored_wall::infrontsided(pos, _builds.inside->get_elements(), d, true);
	// 			this->add_hitbox(std::make_unique<HoledWallHitbox>(d));
			
	// 			pos.change_subvalue(starti+8+3, pos[starti+8+3] + vvv::y()*inside_wall_overhegiht);
	// 			pos.change_subvalue(starti+8+2, pos[starti+8+2] + vvv::y()*inside_wall_overhegiht);
	// 		}
	// 		else
	// 		{
	// 			mot::err("FaceStructure::build_inside_walls(): try to build a non buildable wall");
	// 		}
	// 	}

	// 	if(place_roof)
	// 	{
	// 		meshbuild::fill_cycle(pos, _builds.inside->get_elements(), tofill, false);
	// 	}

	// 	this->complete_color_buffer(this->inside_color());
	// }

	void FaceStructure::put_roofloor(const std::vector<uint>& cycle, float h)
	{
		meshbuild::fill_cycle(
			this->mesh_vertices(),
			this->get_elements(),
			cycle,
			true
		);

		geoCylinderInfo cylinder = {
			.cycle = this->mesh_vertices().subset(cycle),
			.sized_direction = -vvv::y()*h
		};
		this->add_hitbox(std::make_unique<GeoCylinderHitbox>(cylinder));
	}

	uint FaceStructure::wall_intersection(const std::vector<doorWall>& walls_to_build, const directedLine3D& l)
	{
		float dmin = std::numeric_limits<float>::max();
		uint ret = std::numeric_limits<uint>::max();

		for(uint i=0; i<walls_to_build.size(); i++)
		{
			const doorWall& d = walls_to_build[i]; 
			auto edgepos = this->up_position(d.edge);

			Segment2D pos2D = {
				geom::projplan(edgepos[0]),
				geom::projplan(edgepos[1])
			};
			directedLine2D d2d = geom::projplan(l);

			auto inter = geom::halfline_segment_intersection(d2d, pos2D);

			if(inter.intersection)
			{
				if(inter.distance_from_start < dmin)
				{
					dmin = inter.distance_from_start;
					ret = i;
				}
			}
		}

		// if(ret >= walls_to_build.size())
		// {
		// 	auto vrtcs = this->mesh_vertices().subset(this->mesh_vertices_indices());
		// 	base2D b = geom::find_trigo_plan(vrtcs);
		// 	SpaceGrid s(geom::projplan(vrtcs, b), 500, 4.0f);
		// 	s.set_thin_line(geom::projplan(l.point, b), geom::projplan(l.point + l.dirN*200.0f, b), 3);
		// 	s.print_ppm("nani.ppm");

		// 	mot::err("FaceStructure::wall_intersection(): no intersection");
		// }

		// auto vrtcs = this->mesh_vertices().subset(this->mesh_vertices_indices());
		// base2D b = geom::find_trigo_plan(vrtcs);
		// SpaceGrid s(geom::projplan(vrtcs, b), 500, 4.0f);
		// s.set_thin_line(geom::projplan(l.point, b), geom::projplan(l.point + l.dirN*200.0f, b), 3);
		// s.set_thin_line(
		// 	geom::projplan(this->up_position(walls_to_build[ret].edge)[0], b),
		// 	geom::projplan(this->up_position(walls_to_build[ret].edge)[1], b),
		// 4);
		// s.print_ppm("nani.ppm");

		return ret;
	}


	void FaceStructure::place_window(const glm::vec3& onwallcenter, const glm::vec3& dirN, const glm::vec3& size)
	{
		float ND = this->ND();
	
		ArrayBuffer<glm::vec3>& basebuffer  = this->mesh_vertices();
		ArrayBuffer<glm::vec3>& colorbuffer = this->mesh_colors();
		ElementBuffer&          elements    = this->get_elements();

		vec3 wincolor   = castle_colors::CATHEDRAL_WINDOW();
		vec3 winglass   = castle_colors::CATHEDRAL_WINDOW_GLASS();
		vec3 pilercolor = castle_colors::CATHEDRAL_WINDOW_PILERS();
		vec3 roofcolor  = castle_colors::CATHEDRAL_WINDOW_ROOF();

		// Non coeficiented
		float border_size = 0.3f*ND;
		float overoof     = 1.0f*ND;
		float pilerspace  = 1.8f*ND;
		float pilersize   = 0.8f*ND;
		float roofthick   = 0.6f*ND;
	 
		// COeficiented
		float roofstart   = 0.7f;
		float zdepth      = 0.7f;
		float roofext     = 0.2f;

		vec3 dirxN = glm::cross(vvv::y(), dirN);

		// WINDOW INSIDE
		uint inside = basebuffer.size();
		building::paver::v(basebuffer, paverInfo{
			.bottom  = onwallcenter + dirN*size.z*(1.0f-zdepth) - vvv::y()*(size.y/2.0f - roofthick) - dirxN*(size.x/2.0f - border_size - overoof),
			.sized_x = dirxN*(size.x-overoof*2.0f-border_size*2.0f),
			.sized_y = vvv::y()*(size.y*roofstart - roofthick),
			.sized_z = dirN*size.z*(zdepth-roofext),
		});

		// WINDOW OUTSIDE
		paverInfo ext_paver = {
			.bottom  = onwallcenter - vvv::y()*(size.y/2.0f) - dirxN*(size.x/2.0f - overoof),
			.sized_x = dirxN*(size.x-overoof*2.0f),
			.sized_y = vvv::y()*(size.y*roofstart),
			.sized_z = dirN*size.z*(1.0f-roofext),
		};
		uint ext = basebuffer.size();
		building::paver::v(basebuffer, ext_paver);

		// LINKING WINDOW INSIDE OUTSIDE
		elements.add_rectangle(inside+3, inside+0, inside+4, inside+7);
		elements.add_rectangle(inside+0, inside+3, inside+2, inside+1);
		elements.add_rectangle(inside+1, inside+2, inside+6, inside+5);

		elements.add_rectangle(ext+3, ext+0, ext+4, ext+7, true);
		elements.add_rectangle(ext+0, ext+3, ext+2, ext+1, true);
		elements.add_rectangle(ext+1, ext+2, ext+6, ext+5, true);

		elements.add_rectangle(inside+7, inside+3, ext+3, ext+7, true);
		elements.add_rectangle(inside+3, inside+2, ext+2, ext+3, true);
		elements.add_rectangle(inside+2, inside+6, ext+6, ext+2, true);

		elements.add_rectangle(inside+4, inside+0, inside+1, inside+5);

		// COLORS
		while(colorbuffer.size() < basebuffer.size())
		{
			colorbuffer.add_value(wincolor);
		}
		colorbuffer.change_subvalue(inside+0, winglass);
		colorbuffer.change_subvalue(inside+1, winglass);
		colorbuffer.change_subvalue(inside+5, winglass*0.6f);
		colorbuffer.change_subvalue(inside+4, winglass*0.6f);

		// PIILERS PLACING
		vec3 startpiler = (basebuffer[inside+0] + basebuffer[inside+3])/2.0f + dirxN*pilerspace;
		vec3 endpiler   = (basebuffer[inside+1] + basebuffer[inside+2])/2.0f - dirxN*pilerspace;
		vec3 pilersdir  = endpiler - startpiler;
		float pilerspacetot = glm::dot(pilersdir, dirxN);

		if(pilerspacetot >= 0)
		{
			uint piler_count = uint(pilerspacetot/pilerspace/2.0f);
			for(uint i=0; i<=piler_count; i++)
			{
				float coeff = float(i)/float(piler_count);
				vec3 pos = startpiler*(1.0f-coeff) + endpiler*coeff;
				building::paver::sided(basebuffer, elements, paverInfo{
					.bottom  = pos - (dirxN*pilersize + dirN*pilersize)/2.0f,
					.sized_x = dirxN*pilersize,
					.sized_y = vvv::y()*(size.y - 2.0f*roofthick),
					.sized_z = dirN*pilersize
				});
			}
		}
		while(colorbuffer.size() < basebuffer.size())
		{
			colorbuffer.add_value(pilercolor);
		}

		// ROOF PLACING
		uint roofi = basebuffer.size();
		building::paver::sided(basebuffer, elements, paverInfo{
			.bottom  = onwallcenter + vvv::y()*size.y/2.0f + dirxN*(size.x/2.0f-2.0f*overoof), 
			.sized_x = dirN*size.z,
			.sized_y = -dirxN*(size.x - 4.0f*overoof),
			.sized_z = -vvv::y()*roofthick
		});	

		vec3 transroof = (basebuffer[ext+5]-basebuffer[roofi+3])*2.0f;
		building::paver::topsided(basebuffer, elements, transPaverInfo{
			{
				basebuffer[roofi+3],
				basebuffer[roofi+2],
				basebuffer[roofi+1],
				basebuffer[roofi+0]
			},
			transroof
		});;
		
		transroof = (basebuffer[ext+4]-basebuffer[roofi+7])*2.0f;
		building::paver::topsided(basebuffer, elements, transPaverInfo{
			{
				basebuffer[roofi+4],
				basebuffer[roofi+5],
				basebuffer[roofi+6],
				basebuffer[roofi+7]
			},
			transroof
		});

		while(colorbuffer.size() < basebuffer.size())
		{
			colorbuffer.add_value(roofcolor);
		}

		// LINK WINDOW TO ROOF
		vec3 joinroof1 = basebuffer[roofi+0] - basebuffer[inside+5];
		basebuffer.change_subvalue(inside+5, basebuffer[inside+5] + joinroof1);
		basebuffer.change_subvalue(inside+6, basebuffer[inside+6] + joinroof1);

		vec3 joinroof2 = basebuffer[roofi+4] - basebuffer[inside+4];
		basebuffer.change_subvalue(inside+4, basebuffer[inside+4] + joinroof2);
		basebuffer.change_subvalue(inside+7, basebuffer[inside+7] + joinroof2);

		auto hbx = std::make_unique<HitboxFusion>();

		ext_paver.sized_y = vvv::y()*size.y;
		hbx->fusion_hitbox<PaverHitbox>(ext_paver);

		this->add_hitbox(std::move(hbx));
	}

	std::pair<uint, uint> FaceStructure::bluffup(const std::vector<doorWall>& walls_to_build, float undery, float h, float delargement)
	{
		ArrayBuffer<glm::vec3>& vertices = this->mesh_vertices();
		// ArrayBuffer<glm::vec3>& colors   = this->mesh_colors();
		ElementBuffer& elements    = this->get_elements();
		std::vector<uint> vindices = this->mesh_vertices_indices(walls_to_build);

		if(delargement >= this->wall_radius())
		{
			mot::err("FaceStructure::bluffup(): cannot bluffup more than the wall radius");
		}

		float delargefree = this->wall_radius() - delargement;
		
		for(auto i : vindices)
		{
			if(vertices[i].y - (undery + h) <= this->height() - this->safe_height())
			{
				mot::err("FaceStructure::bluffup(): try to bluffup under safe height:\n vertices[i].y - (undery + h) <= this->height() - this->safe_height():\n "
					+ to_string(vertices[i].y) + " - ("
					+ to_string(undery) + " + "
					+ to_string(h) + ") <= "
					+ to_string(this->height()) + " - "
					+ to_string(this->safe_height()));
			}
		}

		meshbuild::translate(this->mesh_vertices(), vindices, -vvv::y()*(undery+h));
	
		auto indices = meshbuild::bissectrice_enlarge_cycle(vertices, elements, vindices, -delargement, glm::vec3(0), true);
		this->complete_color_buffer(this->walls_color()*0.8f);

		wallInfo wall;
		wall.excycle = vertices.subset(utils::uirange(indices));

		indices = this->extrude_cycle(indices, vvv::y()*h, this->walls_color()*0.6f, true);
		indices = this->bissectrice_enlarge_cycle(indices, +delargement, glm::vec3(0), this->walls_color()*1.0f, true);	
		indices = this->extrude_cycle(indices, vvv::y()*undery, this->walls_color(), true);
	
		wall.incycle = geom::bissectrice_enlarge_cycle(wall.excycle, vec3(0), -delargefree);
		wall.sized_direction = vvv::y()*h;
		this->add_hitbox(std::make_unique<WallHitbox>(wall));

		return indices;
	}

	std::vector<uint> FaceStructure::build_full_border(const std::vector<uint>& startindices, float height, float thickness, float underhbx)
	{
		ArrayBuffer<glm::vec3>& vertices = this->mesh_vertices();
		ElementBuffer& elements   = this->get_elements();

		auto indices = meshbuild::extrude_cycle(vertices, elements, startindices, vvv::y()*height, true);
		this->complete_color_buffer(this->walls_color());

		auto inindices = meshbuild::bissectrice_enlarge_cycle(vertices, elements, indices, -thickness, glm::vec3(0), true);
		this->complete_color_buffer(this->walls_color());
	
		wallInfo wall = {
			.excycle = vertices.subset(utils::uirange(indices)),
			.incycle = vertices.subset(utils::uirange(inindices)),
			.sized_direction = -vvv::y()*(height + underhbx)
		};
		this->add_hitbox(std::make_unique<WallHitbox>(wall));

		return utils::uirange(this->extrude_cycle(inindices, -vvv::y()*height, this->walls_color()*0.9f, true));
	}

	std::vector<uint> FaceStructure::build_border(const std::vector<indexedWall>& walls, float height, float thickness, float underhbx, float height_to_build_border)
	{
		using namespace glm;
		using namespace std;
		ArrayBuffer<glm::vec3>& vertices = this->mesh_vertices();
		ArrayBuffer<glm::vec3>& colors = this->mesh_colors();
		ElementBuffer& elements   = this->get_elements();
		std::vector<uint> ret;
		uint N = walls.size();

		std::vector<indexRange> miniborders = this->ext_miniborders(walls, height_to_build_border);

		// If the border is the whole cycle
		if(miniborders.size() == 1 && miniborders[0] == indexRange{0, N-1})
		{
			return this->build_full_border(this->extract_indices(walls), height, thickness, underhbx);
		}
		if(miniborders.size() == 0)
		{
			return this->extract_indices(walls);
		}

		for(uint borderi=0; borderi<miniborders.size(); borderi++)
		{
			const indexRange& b = miniborders[borderi];
			const indexRange& bnext = miniborders[(borderi+1)%miniborders.size()];

			std::vector<uint> wallup;
			std::vector<uint> wallupback;
			std::vector<uint> wallback;

			float occulisioncoeff = 0.92f;
			uint beforeindex = ret.size();
			ret.push_back(std::numeric_limits<uint>::max());

			for(uint modi=b.first; modi<=b.second+1; modi++)
			{
				uint h = (modi+N-1)%N;
				uint i = modi%N;

				vec3 prev = vertices[walls[h].edgei.first];
				vec3 cur  = vertices[walls[i].edgei.first];
				vec3 next = vertices[walls[i].edgei.second];

				vec3 dirinsideN = -geom::trigo_bissectriceN(prev, cur, next, vvv::y());

				wallup.push_back(vertices.add_value(cur + vvv::y()*height));	
				wallupback.push_back(vertices.add_value(vertices[vertices.size()-1] + dirinsideN*thickness));	
				this->complete_color_buffer(this->walls_color());
				wallback.push_back(vertices.add_value(vertices[vertices.size()-1] - vvv::y()*height));
				this->complete_color_buffer(this->walls_color()*occulisioncoeff);
				
				ret.push_back(wallback[wallback.size()-1]);
			}

			#ifdef MOTOR_DEBUG
			if(wallup.size() != wallupback.size()
			|| wallup.size() != wallback.size())
			{
				mot::err("OpenStructure::build_border(): error in algorithm: different layer sizes");
			}
			#endif

			auto hbx = std::make_unique<HitboxFusion>();
			
			uint incr=0;
			for(uint modi=b.first; modi<=b.second; modi++)
			{
				elements.add_rectangle(
					walls[modi%N].edgei.first,
					walls[modi%N].edgei.second,
					wallup[incr+1],
					wallup[incr+0],
					true
				);
				elements.add_rectangle(
					wallup[incr+0],
					wallup[incr+1],
					wallupback[incr+1],
					wallupback[incr+0],
					true
				);
				elements.add_rectangle(
					wallupback[incr+0],
					wallupback[incr+1],
					wallback[incr+1],
					wallback[incr+0],
					true
				);

				transPaverInfo paver = {
					.square_base = {
						vertices[wallup[incr+0]],
						vertices[wallup[incr+1]],
						vertices[wallupback[incr+1]],
						vertices[wallupback[incr+0]]
					},
					.sized_direction = -vvv::y()*(height+underhbx)
				};
				incr++;

				hbx->fusion_hitbox<PaverHitbox>(paver);
			}

			this->add_hitbox(std::move(hbx));

			uint start = vertices.size();

			vec3 nextdirN = this->dirN(walls[(b.second+1)%N]);
			vec3 prevdirN = -this->dirN(walls[(b.first+N-1)%N]);

			vertices.add_value(vertices[walls[b.second%N].edgei.second] + nextdirN*thickness);
			this->complete_color_buffer(this->walls_color()*occulisioncoeff);
			vertices.add_value(vertices[vertices.size()-1] + vvv::y()*height);
			this->complete_color_buffer(this->walls_color());

			vertices.add_value(vertices[walls[b.first%N].edgei.first] + prevdirN*thickness);
			this->complete_color_buffer(this->walls_color()*occulisioncoeff);
			vertices.add_value(vertices[vertices.size()-1] + vvv::y()*height);
			this->complete_color_buffer(this->walls_color());

			ret[beforeindex] = start+2;
			ret.push_back(start+0);

			elements.add_rectangle(
				wallback[wallback.size()-1],
				wallupback[wallupback.size()-1],
				start+1,
				start+0,
				true
			);
			elements.add_rectangle(
				wallback[0],
				wallupback[0],
				start+3,
				start+2,
				false
			);

			elements.add_triangle(
				wallup[wallup.size()-1],
				wallupback[wallupback.size()-1],
				start+1,
				false
			);
			elements.add_triangle(
				wallup[0],
				wallupback[0],
				start+3,
				true
			);

			bevelShape bevel1 = {
				.triangle_base = {
					vertices[wallup[wallup.size()-1]],
					vertices[wallupback[wallupback.size()-1]],
					vertices[start+1]
				},
				.sized_direction = -vvv::y()*(height+underhbx)
			};
			bevelShape bevel2 = {
				.triangle_base = {
					vertices[wallup[0]],
					vertices[wallupback[0]],
					vertices[start+3]
				},
				.sized_direction = -vvv::y()*(height+underhbx)
			};
			this->add_hitbox(std::make_unique<BevelHitbox>(bevel1));
			this->add_hitbox(std::make_unique<BevelHitbox>(bevel2));
			
			uint modi=b.second+1;
			while((modi%N) != (bnext.first+N-1)%N)
			{
				ret.push_back(walls[modi%N].edgei.second);
				colors.change_subvalue(ret[ret.size()-1], colors[ret[ret.size()-1]]*occulisioncoeff);
				modi++;
			}
		}

		return ret;
	}

	void FaceStructure::place_hatroof(const std::vector<glm::vec3>& place, float flath, float flatw, float hatoph, const glm::vec3& color1, const glm::vec3& color2)
	{
		using namespace glm;
		using namespace std;
		ArrayBuffer<glm::vec3>& vertices = this->mesh_vertices();
		ArrayBuffer<glm::vec3>& colors = this->mesh_colors();
		ElementBuffer& elements   = this->get_elements();

		if(place.size() < 3)
		{
			mot::err("FaceStructure::place_hatroof(): cycle of length < 3");
		}
		
		pair<uint, uint> indices = {vertices.size(), vertices.size()+place.size()-1};
		for(const auto& p : place)
		{
			vertices.add_value(p);
			colors.add_value(color1);
		}

		geoCylinderInfo cylinder = {
			.cycle = vertices.subset(indices),
			.sized_direction = vvv::y()*flath
		};
		this->add_hitbox(std::make_unique<GeoCylinderHitbox>(cylinder));

		meshbuild::fan_cycle(vertices, elements, indices, geom::mean(vertices.subset(indices)), false);
		colors.add_value(color1*0.75f);

		indices = this->extrude_cycle(indices, vvv::y()*flath, color1, true);
		indices = this->centerdir_enlarge_cycle(indices, -flatw, vec3(0), color1*0.7f, true);
		
		indices = meshbuild::copytranslate(vertices, indices, vec3(0));
		this->complete_color_buffer(color2);

		vec3 roof_top = geom::mean(cylinder.cycle) + vvv::y()*hatoph;
		meshbuild::fan_cycle(vertices, elements, indices, roof_top, true);
		colors.add_value(color2*1.2f);
	

		fanInfo fan = {
			.cycle  = vertices.subset(indices),
			.origin = roof_top
		};
		this->add_hitbox(std::make_unique<FanHitbox>(fan));
	}

	std::pair<uint, uint> FaceStructure::place_fullsided_piler_vertices(const glm::vec3& botcenter, const glm::vec3& dirN, const std::vector<glm::vec3>& XYshadowsBT, const glm::vec3& color)
	{
		ArrayBuffer<glm::vec3>& vertices = this->mesh_vertices();
		ElementBuffer& elements   = this->get_elements();

		std::vector<vec2> thick_heights;
		for(const auto& v : XYshadowsBT)
		{
			thick_heights.push_back({v.x, v.y});
		}

		float maxX = compute_max_x(XYshadowsBT);
		spaceBase base = geom::orthogonal_spacebase(dirN, vvv::y());

		uint starti = vertices.size();
		building::hanoi::squared::topsided(vertices, elements, thick_heights, botcenter-dirN*maxX/2.0f, base);
		this->complete_color_buffer(color);

		return {starti, vertices.size()-1};
	}

	std::pair<uint, uint> FaceStructure::place_fullsided_wallpiler_vertices(const glm::vec3& botcenter, const glm::vec3& dirN, const std::vector<glm::vec3>& XYshadowsBT, float zcoeff, float incline_h, const glm::vec3& color)
	{
		ArrayBuffer<glm::vec3>& vertices = this->mesh_vertices();
		ArrayBuffer<glm::vec3>& colors = this->mesh_colors();

		auto ret = this->place_fullsided_piler_vertices(botcenter, dirN, XYshadowsBT, color);
		float maxX = compute_max_x(XYshadowsBT);

		for(uint i=0; i<XYshadowsBT.size(); i++)
		{
			vertices.change_subvalue(ret.first+i*4+1, vertices[ret.first+i*4+1] + dirN*(maxX-XYshadowsBT[i].x)/2.0f);
			vertices.change_subvalue(ret.first+i*4+2, vertices[ret.first+i*4+2] + dirN*(maxX-XYshadowsBT[i].x)/2.0f);
			
			vertices.change_subvalue(ret.first+i*4+0, vertices[ret.first+i*4+0] + dirN*((XYshadowsBT[i].x+maxX)/2.0f - (XYshadowsBT[i].x)*zcoeff));
			vertices.change_subvalue(ret.first+i*4+3, vertices[ret.first+i*4+3] + dirN*((XYshadowsBT[i].x+maxX)/2.0f - (XYshadowsBT[i].x)*zcoeff));

			for(uint s=0; s<4; s++)
			{
				uint ibot = ret.first+i*4+s+0;
				colors.change_subvalue(ibot, colors[ibot]*XYshadowsBT[i].z);
			}
		}

		{
			uint i1 = vertices.size()-2;
			uint i2 = vertices.size()-3;
			vertices.change_subvalue(i1, vertices[i1] + vvv::y()*incline_h);
			vertices.change_subvalue(i2, vertices[i2] + vvv::y()*incline_h);
		}

		return ret;
	}

	void FaceStructure::place_hanoi_hibox(const std::pair<uint, uint>& indices)
	{
		this->add_hitbox(std::make_unique<HanoiHitbox>(this->mesh_vertices().subset(indices)));
	}

	void FaceStructure::place_fullsided_wallpiler(const glm::vec3& botcenter, const glm::vec3& dirN, const std::vector<glm::vec3>& XYshadowsBT, float zcoeff, float incline_h, const glm::vec3& color)
	{
		auto indices = this->place_fullsided_wallpiler_vertices(botcenter, dirN, XYshadowsBT, zcoeff, incline_h, color);
		this->place_hanoi_hibox(indices);
	}

	void FaceStructure::place_fullsided_piler(const glm::vec3& botcenter, const glm::vec3& dirN, const std::vector<glm::vec3>& XYshadowsBT, const glm::vec3& color)
	{
		auto indices = this->place_fullsided_piler_vertices(botcenter, dirN, XYshadowsBT, color);
		this->place_hanoi_hibox(indices);
	}

	void FaceStructure::build_topdeco(const doorWall& w)
	{
		if(w.dh > this->topdeco_height() + this->safe_height())
		{
			wallInfos infos = this->wall_infos(w);
			uint rd = rand()%2;

			if(rd==0)
			{
				this->place_window_range(w, this->topfaceh(w)-_sizes.windows.y);
			}
			if(rd==1)
			{
				this->place_loophole_range(w, this->topfaceh(w)-_sizes.loopholes.y);
			}
		}
	}

	void FaceStructure::place_cathedral_deco(const doorWall& w)
	{	
		wallInfos infos = this->wall_infos(w);

		if(w.dh > this->counterfort_minh()*1.5f + this->safe_height())
		{
			if(rand()%2 == 0)
			{
				this->place_counterforts_range(w, 0.0f);
			}
			else
			{
				float l = _sizes.pilers.x*2.0f;
				float s = l*0.2f;
				float h = this->topfaceh(w)-s;

				vector<vec3> xyshadow = {
					vec3(l*1.0f, h*0.00f, 0.7f),
					vec3(l*1.0f, h*0.20f, 1.0f),
					vec3(l*0.9f, h*0.20f, 0.7f),
					vec3(l*0.9f, h*0.25f, 1.0f),
					vec3(l*0.7f, h*0.25f, 0.85f),
					vec3(l*0.7f, h*0.89f, 1.0f),
					vec3(l*0.55f, h*0.89f, 0.85f),
					vec3(l*0.55f, h*0.95f, 1.0f),
					vec3(l*0.5f, h*0.95f, 0.85f),
					vec3(l*0.5f, h*1.00f-s, 1.0f)
				};

				this->place_pilers_range(w, 0.0f, l, l*2.0f, xyshadow, s, _sizes.pilers.y/_sizes.pilers.x);
			}
		}
		else
		{
			int rd = rand()%2;

			if(rd==0)
			{
				this->build_topdeco(w);
			}
			if(rd==1)
			{
				// this->build_topdeco(w);
			}
		}
	}

	void FaceStructure::place_bigpiler(const doorWall& w, float bot_y, float xsizecoeff)
	{
		wallInfos infos = this->wall_infos(w);
		float l = infos.length*xsizecoeff;
		float s = infos.height*0.02f;
		float h = this->topfaceh(w)-s;

		vector<vec3> xyshadow = {
			vec3(l*1.00f, h*0.00f, 0.7f),
			vec3(l*1.00f, h*0.10f, 1.0f),
			vec3(l*0.98f, h*0.10f, 0.7f),
			vec3(l*0.98f, h*0.25f, 1.0f),
			vec3(l*0.85f, h*0.25f, 0.85f),
			vec3(l*0.85f, h*0.80f, 0.85f),
			vec3(l*0.98f, h*0.80f, 0.85f),
			vec3(l*0.98f, h*1.00f-s, 1.0f)
		};

		this->place_pilers_range(w, bot_y, 0.0f, l*0.02, xyshadow, s, _sizes.pilers.x/l*0.5f);
	}

	void FaceStructure::place_counterfort_support(const planarYPlace& counterfortpos, float height, float sizecoeff)
	{
		float size = counterfortpos.size.x*this->counterfort_geom().size;

		vector<vec3> xyshadow = {
			vec3(size*sizecoeff*1.00f, height*0.00f, 0.7f),
			vec3(size*sizecoeff*1.00f, height*0.70f, 1.0f),
			vec3(size*sizecoeff*0.60f, height*1.00f, 1.0f)
		};

		this->place_fullsided_piler(counterfortpos.center, geom::unprojplan(counterfortpos.dirN), xyshadow, this->walls_color());
	}

	std::pair<uint, uint> FaceStructure::put_floor(float h, const glm::vec3& color)
	{
		this->check_buffer_sizes();

		ArrayBuffer<glm::vec3>& pos = this->inmesh_vertices();
		ElementBuffer& elements = this->get_inelements();
		std::vector<vec3> cycle;

		if(h == 0.0f)
		{
			cycle = _base->face_out_vertices(_face, this->floor_height()+h);
		}
		else
		{
			cycle = _base->face_in_vertices(_face, this->floor_height()+h);
		}

		if(cycle.size() == 0)
		{
			mot::err("try to put an ampty floor");
		}

		std::pair<uint, uint> cy = {pos.size(), pos.size() + cycle.size()-1};

		for(const auto& p : cycle)
		{
			pos.add_value(p);
		}
		this->complete_color_buffer(color);

		meshbuild::fill_cycle(pos, elements, cy, true);

		return cy;
	}
	std::pair<uint, uint> FaceStructure::put_roof(float h, const glm::vec3& color)
	{
		this->check_buffer_sizes();

		ArrayBuffer<glm::vec3>& pos = this->inmesh_vertices();
		ElementBuffer& elements = this->get_inelements();
		std::vector<vec3> cycle = _base->face_in_vertices(_face, this->floor_height()+h);

		if(cycle.size() == 0)
		{
			mot::err("try to put an ampty floor");
		}

		std::pair<uint, uint> cy = {pos.size(), pos.size() + cycle.size()-1};

		for(const auto& p : cycle)
		{
			pos.add_value(p);
		}
		this->complete_color_buffer(color);

		meshbuild::fill_cycle(pos, elements, cy, false);

		return cy;
	}

	void FaceStructure::put_roofloor(float h, float underthick, const glm::vec3& color)
	{
		auto i1 = this->put_floor(h, color);
		auto i2 = this->put_roof(h-underthick, color*0.8f);

		geoCylinderInfo cylinder = {
			.cycle = this->inmesh_vertices().subset(i2),
			.sized_direction = vvv::y()*underthick
		};
		this->add_hitbox(std::make_unique<GeoCylinderHitbox>(cylinder));
	}

	/*
		From upside

	         ^  |            |  ^
	         |  |            |  | zcoeff
	         |  |            |  |
	       L |  |            |  V  
	         |   \          /
	         |    \        /
	         V     -______-

                 <----------->
	               coeff*X = L
	 

		From side

		  ^ ------------
		  |        |   /
		h |        |  /
		  |        | /
		  v _______|/
                     
		  <--------> Z = zcoeff*L

	*/

	bool FaceStructure::place_fat_tower_deco(const doorWall& w, float bot_y, float xcoeff, float h)
	{
		if(w.dh <= 0)
		{
			mot::err("FaceStructure::place_fat_tower_deco(): wall is descening");
		}
		if(w.dh <= bot_y + h + this->safe_height())
		{
			return false;
		}
		this->check_buffer_sizes();

		float zcoeff = 0.5f;
		float barrierh = h*0.04f;
		vec2 towercoeff = vec2(0.6f, this->basoul().fatower_ycoeff);
		std::vector<uint> barrier;

		ArrayBuffer<glm::vec3>& vertices = this->mesh_vertices();
		ElementBuffer& elements = this->get_elements();
		wallInfos infos = this->wall_infos(w);

		float tl = infos.length;
		float L  = tl*xcoeff;
		float Z  = L*zcoeff;

		vec3 p1 = infos.bot.first ; p1.y = this->height() + bot_y + h;
		vec3 p2 = infos.bot.second; p2.y = this->height() + bot_y + h;
	
		vec3 middle = geom::middle(p1, p2);

		p1 = middle + infos.dirxN*L/2.0f;
		p2 = middle - infos.dirxN*L/2.0f;
		middle.y -= h;

		if(!this->square_in_face(middle-infos.normalN*L*0.51f, infos.normalN, L))
		{
			return false;
		}

		// PLACE BASE VERTICES
		uint start = vertices.size();
		vertices.add_value(p1);
		vertices.add_value(p2);
		this->complete_color_buffer(this->walls_color()*0.7f);
		vertices.add_value(p1-infos.normalN*Z);
		vertices.add_value(p2-infos.normalN*Z);
		this->complete_color_buffer(this->walls_color());

		barrier.push_back(start+0);

		uint boti = vertices.size();
		vertices.add_value(middle);
		this->complete_color_buffer(this->walls_color()*0.6f);
		vertices.add_value(middle-infos.normalN*Z+vvv::y()*h*0.3f);
		this->complete_color_buffer(this->walls_color()*0.6f);

		elements.add_rectangle(start+0, start+2, boti+1, boti+0, false);
		elements.add_rectangle(start+1, start+3, boti+1, boti+0, true);
		
		// PLACING TOWER
		vec3 tcenter = geom::yfixed(vertices[boti+1], this->height()+bot_y+h);
	
		if(L > 20.0f*this->ND() && this->basoul().magicircl_amount > 0)
		{
			magicTowerCircle towers = {
				.position = tcenter,
				.tower_count = 4u+(rand()%5),
				.radius = L*0.45f,
				.towers_size = L*0.1f,
				.minmax_height = vec2(2.0f, 6.0f)*L
			};
			this->build_structures().circles->add_tower_circle(towers, false);
			_base->consume_magic_circle();
		}
		else
		{
			fatTower pos = {
				.center = tcenter,
				.dirN2D = -geom::projplan(infos.normalN),
				.size   = vec3(towercoeff.x, towercoeff.y, towercoeff.x)*L
			};
			this->build_structures().fat_towers->add_tower(pos, false);
		}

		auto hbx = std::make_unique<HitboxFusion>();

		// BASE HITBOX
		complexBevelShape bevel = {
			.triangle_base_up = {
				vertices[start+0],
				vertices[start+1],
				vertices[boti+0]
			},
			.triangle_base_down = {
				vertices[start+2],
				vertices[start+3],
				vertices[boti+1]
			}
		};
		hbx->fusion_hitbox<BevelHitbox>(bevel);
	
		// PLACING ARC
		uint dirscn = 10;
		uint arci = vertices.size();

		for(uint i=0; i<=dirscn; i++)
		{
			float coeff = (float)(i)/(float)(dirscn);

			barrier.push_back(vertices.size());
			vertices.add_value(geom::circle_point_plan(tcenter, infos.dirxN, -infos.normalN, (float)(M_PI)*coeff, L*(1.0f-zcoeff)));
		
			if(i > 0)
			{
				elements.add_triangle(boti+1, arci+i-1, arci+i, false);

				prismInfo prism = {
					.points = {
						vertices[arci+i-1],
						vertices[arci+i],
						vertices[boti+1],
						tcenter
					}
				};
				hbx->fusion_hitbox<PrismHitbox>(prism);
			}
		}
		this->complete_color_buffer(this->walls_color());
		barrier.push_back(start+1);

		auto indices1 = this->bissectrice_enlarge_cycle(barrier, 0.0f, vvv::y()*barrierh, this->walls_color(), true);
		auto indices2 = this->bissectrice_enlarge_cycle(indices1, -barrierh, vvv::y()*0.0f, this->walls_color(), true);
		auto indices3 = this->bissectrice_enlarge_cycle(indices2, 0.0f, -vvv::y()*barrierh, this->walls_color(), true);

		meshbuild::fill_cycle(vertices, elements, indices3, true);

		wallInfo wall = {
			.excycle = vertices.subset(barrier),
			.incycle = vertices.subset(indices3),
			.sized_direction = vvv::y()*barrierh
		};
		hbx->fusion_hitbox<WallHitbox>(wall);
		this->add_hitbox(std::move(hbx));

		return true;
	}

	glm::vec3 FaceStructure::middletop(const doorWall& w)
	{
		auto pos = this->up_position(w.edge);
		return geom::middle(pos[0], pos[1]);
	}

	float FaceStructure::wall_projdistances(const doorWall& w1, const doorWall& w2)
	{
		auto pos1 = this->up_position(w1.edge);
		auto pos2 = this->up_position(w2.edge);
		return geom::distance_segment_segment({geom::projplan(pos1[0]), geom::projplan(pos1[1])}, {geom::projplan(pos2[0]), geom::projplan(pos2[1])});
	}


	/*
		===============
		FLOOR STRUCTURE
		===============
	*/

	FloorStructure::FloorStructure(const baseBuiling& base, uint face)
		: FaceStructure(base, face)
	{

	}
	
	void FloorStructure::build_wall_deco(const std::vector<doorWall>& walls_to_build)
	{
		uint N = walls_to_build.size();

		for(uint i=0; i<N; i++)
		{
			const doorWall& w = walls_to_build[i];

			if(w.dh < 0)
				continue;

			if(w.dh > this->counterfort_minh()*1.5f + this->safe_height())
			{
				this->place_cathedral_deco(w);
			}
			else
			{
				int rd = rand()%10;
				if(rd >= 0 && rd <= 2)
				{
					this->place_pilers_range(w, 0.0f);
				}
				if(rd >= 3 && rd <= 3)
				{
					this->place_bigpiler(w, 0.0f, 0.26f);
				}
			}
		}
	}

	void FloorStructure::build_face(const std::vector<doorWall>& walls_to_build)
	{
	
	}
};
