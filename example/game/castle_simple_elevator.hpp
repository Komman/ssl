#ifndef _CASTLE_SIMPLE_ELEVATOR_HPP_
#define _CASTLE_SIMPLE_ELEVATOR_HPP_

#include "../motor/stored_vao_object.hpp"
#include "../motor/planar_building_instance.hpp"

namespace CASTLE
{
	struct elevatorInfo
	{
		glm::vec2 height_range;
		float heightcoeff;
		float speed;
	};

	class SimpleElevator : public StoredVAObject<PlanarInstances<elevatorInfo, glm::vec3, glm::vec3>>
	{
	public:
		SimpleElevator(HitboxContext& contex, const glm::vec3& color);

		void add_elevator(const planarYPlace& place, const glm::vec2& height_range);
		void animate(float dt);

	protected:
		// Center is top
		std::unique_ptr<BasicHitbox> build_hitbox(const planarYPlace& place) const override;

	private:
		enum buffersRole {RELATIVE_POS=0, COLOR};
		
		void build_base();

	private:
		glm::vec3 _color;
		float _height;
	};

};

#endif //_CASTLE_SIMPLE_ELEVATOR_HPP_
