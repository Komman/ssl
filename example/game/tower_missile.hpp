#ifndef _TOWER_MISSILE_HPP_
#define _TOWER_MISSILE_HPP_

#include "hitboxed_arrow.hpp"

#include "../motor/linear_valuator.hpp"
#include "../motor/dynamic_light.hpp"
#include "../motor/light_intensity_valuator.hpp"
#include "../motor/regular_signaler.hpp"
#include "../motor/entity_context.hpp"

struct towerMissileStart
{
	glm::vec3 position;
	glm::vec3 directionN;
	float     length;
};

class TowerMissile : public HitboxedArrow
{
public:
	TowerMissile(const towerMissileStart& start);
	virtual ~TowerMissile() {}

	void animate(const PhysicalContext& context, float dt);

protected:
	virtual void planting_reaction(const glm::vec3& position,
								   const glm::vec3& normal,
								   const glm::vec3& impact_speed);
	virtual bool to_destroy() override;

	
private:
	float _light_intensity;
	float _duration;
	float _time;

	RegularSignaler _smoke_emission;
};

#endif //_TOWER_MISSILE_HPP_
