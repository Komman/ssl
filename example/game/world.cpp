#include "world.hpp"

#include "ingame.hpp"

using namespace std;

std::unique_ptr<Drawer> BasicWorld::static_gate_identifior = NULL;
std::unique_ptr<Drawer> BasicWorld::static_gate_holer      = NULL;


BasicWorld::BasicWorld()
	: _required_gates(0),
	  _linked_gates()
{
	if(static_gate_identifior == NULL)
	{
		ShaderGenerator gen("motor/shaders/worlds.glsl");

		static_gate_identifior = std::make_unique<Drawer>(gen, "vertex_only" , "empty_fragment_identifor");
		static_gate_holer      = std::make_unique<Drawer>(gen, "holer_vertex", "empty_fragment_holer");

		static_gate_identifior->depth(true);
		static_gate_identifior->stencil_testing(true);
		static_gate_identifior->stencil_function(GL_ALWAYS, 1);
		static_gate_identifior->stencil_op(GL_KEEP, GL_KEEP, GL_REPLACE);

		static_gate_holer->depth(true);
		static_gate_holer->depth_mask(GL_TRUE);
		static_gate_holer->depth_function(GL_ALWAYS);
		static_gate_holer->stencil_testing(true);
		static_gate_holer->stencil_function(GL_EQUAL, 1);
		static_gate_holer->stencil_op(GL_KEEP, GL_KEEP, GL_KEEP);

		gen.free();
	}
}

unsigned int BasicWorld::total_gates_amount()
{
	return _required_gates;
}

unsigned int BasicWorld::linked_gates()
{
	return _linked_gates.size();
}

unsigned int BasicWorld::remaining_gates()
{
	return this->total_gates_amount() - this->linked_gates();
}

void BasicWorld::link_gate_out(BasicWorld* gate_world)
{
	if(this->remaining_gates() == 0)
	{
		err("BasicWorld::link_gate_out: try to link a gate while there is no remaining world gates");
	}

	_linked_gates.push_back(gate_world);
}

void BasicWorld::link_two_worlds(BasicWorld* w1, BasicWorld* w2)
{
	w1->link_gate_out(w2);
	w2->link_gate_out(w1);
}

void BasicWorld::require_gate()
{
	_required_gates++;
}

BasicWorld* BasicWorld::get_gate_linked_world(unsigned int gate_index)
{
	if(gate_index >= _linked_gates.size())
	{
		err("BasicWorld::get_gate_linked_world(): require a gate that is out of bound: "
			+ to_string(gate_index) + " / " + to_string(_linked_gates.size()));
	}

	return _linked_gates[gate_index];
}


void BasicWorld::draw(AbsoluteObjects& objs)
{
	this->draw_world(objs);
}

/*
	ControlledDrawsWorld
*/


ControlledDrawsWorld::ControlledDrawsWorld()
	: BasicWorld(),
	  _drawers_controller()
{

}

DrawerPackController& ControlledDrawsWorld::drawer_controller()
{
	return _drawers_controller;
}

// void ControlledDrawsWorld::close_gate_draw(GLuint mask_value)
// {
// 	_draw_in_gate = true;

// 	_drawers_controller.stencil_testing(true);
// 	_drawers_controller.stencil_mask(0xFF);
// 	_drawers_controller.stencil_function(GL_EQUAL, mask_value, 0xFF);
// 	_drawers_controller.stencil_op(GL_KEEP, GL_KEEP, GL_KEEP);

// 	this->close_gate_draw_no_stencil();

// 	_drawers_controller.stencil_testing(false);

// 	_draw_in_gate = false;
// }
