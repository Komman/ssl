#ifndef _PROCEDURAL_TREE_HPP_
#define _PROCEDURAL_TREE_HPP_

#include "../../ssl/ssl.hpp"
#include "../motor/basic_hitbox.hpp"

#include <glm/glm.hpp>
#include <vector>

using namespace ssl;

#define PROCEDURAL_TREE_SET_MEMBER(type, arg)\
inline void set##arg(const type& value) {arg=value;}

class ProceduralTree : public ElementDrawableObject
{
public:

	ProceduralTree(const glm::vec3& position,
				   const glm::vec3& direction        = glm::vec3(0.0,1.0,0.0),
				   const glm::vec3& orientation      = glm::vec3(1.0,0.0,0.0),
				   float trunc_size_multiplier       = 6.0,
				   float trunc_width_multiplier      = 1.6,
				   float branch_size                 = 2.0,
				   float branch_width                = 0.5,
				   float spreading                   = 0.5,
				   const glm::vec4& root_color       = glm::vec4(dark(BROWN)/4.0f, 1.0),
				   const glm::vec4& leaves_colors    = glm::vec4(dark(BROWN)*1.9f, 1.0),
				   int levels                        = 6,
				   float final_leave_size            = 0.4,
				   float final_leave_height          = 0.2,
				   const std::vector<glm::vec4>&  final_leave_color= {
							glm::vec4(ORANGE/1.5f, 1.0),
							glm::vec4(ORANGE/3.0f, 1.0),
							glm::vec4(ORANGE/3.0f, 1.0),
							glm::vec4(ORANGE/3.0f, 1.0)
						},
				const std::vector<glm::vec3>& all_offset_positions = {glm::vec3(0.0)});


	const std::vector<const BasicBuffer*>& get_buffers() const;
	const ElementBuffer&                   get_elements() const;

	const VertexArrayObject& get_vao() const;

	PROCEDURAL_TREE_SET_MEMBER(float, _trunc_size_multiplier)
	PROCEDURAL_TREE_SET_MEMBER(float, _trunc_width_multiplier)
	PROCEDURAL_TREE_SET_MEMBER(float, _branch_size)
	PROCEDURAL_TREE_SET_MEMBER(float, _branch_width)
	PROCEDURAL_TREE_SET_MEMBER(float, _spreading)
	PROCEDURAL_TREE_SET_MEMBER(float, _total_levels)
	PROCEDURAL_TREE_SET_MEMBER(float, _final_leave_height)
	PROCEDURAL_TREE_SET_MEMBER(float, _final_leave_size)
	PROCEDURAL_TREE_SET_MEMBER(glm::vec3, _position)

	void rebuild();
	void animate(float dt);

	const std::vector<BasicHitbox::storedHitbox>& get_hitboxes() const;

private:

	std::vector<std::pair<glm::vec3, uint>> add_branch(glm::vec3 base[3],
					uint base_indices[3],
				    float branch_size,
				    float branch_width,
				    float spreading,
				    const glm::vec4& root_color,
				    const glm::vec4& leaves_colors,
				    float level_coeff);

	glm::vec3 get_direction(glm::vec3 base[3]);

	glm::vec4 get_level_color(const glm::vec4& root_color,
				   			  const glm::vec4& leaves_colors,
				   			  float level_coeff);

	void stop_branch(uint end_indices[3]);

	void add_final_leave(const std::vector<std::pair<glm::vec3, uint>>&  branch);

	void recursive_building(const std::vector<std::pair<glm::vec3, uint>>&  start_branch,
						uint level,
					    float branch_size,
					    float branch_width);

	ArrayBuffer<glm::vec3> _points;
	ArrayBuffer<glm::vec4> _colors;
	ArrayBuffer<glm::vec3> _normals;
	ElementBuffer          _indices;
	ArrayBuffer<glm::vec3> _deps;

	std::vector<BasicHitbox::storedHitbox> _hitboxes;

	glm::vec3 _position;
	glm::vec3 _direction;
	glm::vec3 _orientation;
	glm::vec4 _root_color;
	glm::vec4 _leaves_colors;

	std::vector<glm::vec4> _final_leave_color;

	float _trunc_size_multiplier;
	float _trunc_width_multiplier;
	float _branch_size;
	float _branch_width;
	float _spreading;
	int   _total_levels;
	float _final_leave_height;
	float _final_leave_size;
	float _dark_coeff_under_leaves;

	VertexArrayObject _vao;
	mutable std::vector<const BasicBuffer*> _draw_buffers;
};


#endif //_PROCEDURAL_TREE_HPP_
