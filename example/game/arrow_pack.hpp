#ifndef _ARROW_PACK_HPP_
#define _ARROW_PACK_HPP_

#include <vector>

#include "ingame.hpp"
#include "effects.hpp"
#include "basic_arrow_pack.hpp"
#include "arrow.hpp"

using namespace ssl;

ShaderGenerator* get_arrow_shader_generator();

template<typename ArrowGeometrty>
class ArrowPack : public BasicArrowPack
{
public:
	using arrowId   = unsigned int;

	struct arrowType
	{
		std::unique_ptr<Arrow> arrow;
		ArrowGeometrty         look;
	};

public:
	ArrowPack(); // NOT THREAD SAFE

	virtual void animate(const PhysicalContext& context, float dt) override;
	void         clear()           override;
	unsigned int size()      const override;
	
	arrowId add_arrow(std::unique_ptr<Arrow>&& arrow, const ArrowGeometrty& look);
	arrowId add_arrow(arrowType&& arrow);

	void delete_arrow(arrowId id);
	void print() const;

	const arrowType& get_arrow(unsigned int index)  const;
	const arrowType& operator[](unsigned int index) const;

	virtual ~ArrowPack() {}

protected:
	/*
		Implement a vector of arrows
	*/  
	virtual void pre_add_arrow(const Arrow& arrow,
							   const ArrowGeometrty& look) =0;
	virtual void post_set_arrow(unsigned int index,
		  						const Arrow& arrow,
		  						const ArrowGeometrty& look) =0;
	virtual void pre_pop_last_arrow() =0;
	virtual void pre_clear() =0;

	/*
		To define if the moving of an arrow (from the last index
		of the vector to another index) has a strict different
		behavior than just calling post_set_arrow() on this index
		and the last arrow and then pop the last arrow
	*/
	virtual void move_last_arrow(unsigned int dst_index)
	{
		if(_arrows.size() < 1) {ssl::err("ArrowPack::move_last_arrow() called while _arrows.size() = 0");}

		const auto& arr = _arrows[_arrows.size() - 1].arrow;

		this->post_set_arrow(dst_index, *(arr.arrow), arr.look);
		this->pre_pop_last_arrow();
	}

private:
	void swapop_indexed_arrow(unsigned int arrow_index);

	struct memArrow
	{
		arrowType arrow;
		arrowId	  id;
	};

private:
	std::vector<memArrow> _arrows;
	arrowId               _current_arrow_id;
};

template<typename ArrowGeometrty>
ArrowPack<ArrowGeometrty>::ArrowPack()
	: BasicArrowPack(),
	  _current_arrow_id(0)
{

}

template<typename ArrowGeometrty>
const typename ArrowPack<ArrowGeometrty>::arrowType& ArrowPack<ArrowGeometrty>::get_arrow(unsigned int index) const
{
	#ifdef SSL_DEBUG
	if(index >= _arrows.size())
	{
		err(" ArrowPack<ArrowGeometrty>::get_arrow(): index out of bound");
	}
	#endif

	return _arrows[index].arrow;
}

template<typename ArrowGeometrty>
const typename ArrowPack<ArrowGeometrty>::arrowType& ArrowPack<ArrowGeometrty>::operator[](unsigned int index) const
{
	return this->get_arrow(index);
}

template<typename ArrowGeometrty>
void ArrowPack<ArrowGeometrty>::animate(const PhysicalContext& context, float dt)
{
	for(int i=0; i<(int)_arrows.size(); i++)
	{
		auto& cur = (_arrows[i].arrow.arrow);

		cur->animate(context, dt);


		if(cur->to_destroy())
		{
			this->swapop_indexed_arrow(i);
			i--;
		}
		else
		{
			const auto& arr = this->get_arrow(i);
			this->post_set_arrow(i, *(arr.arrow), arr.look);	
		}
	}
}

template<typename ArrowGeometrty>
void ArrowPack<ArrowGeometrty>::clear()
{
	this->pre_clear();

	_arrows.clear();
}

template<typename ArrowGeometrty>
unsigned int ArrowPack<ArrowGeometrty>::size() const
{
	return _arrows.size();
}

template<typename ArrowGeometrty>
typename ArrowPack<ArrowGeometrty>::arrowId ArrowPack<ArrowGeometrty>::add_arrow(std::unique_ptr<Arrow>&& arrow, const ArrowGeometrty& look)
{
	arrowType ret = {
		.arrow = std::move(arrow),
		.look  = look
	};

	return this->add_arrow(std::move(ret));
}

template<typename ArrowGeometrty>
typename ArrowPack<ArrowGeometrty>::arrowId ArrowPack<ArrowGeometrty>::add_arrow(arrowType&& arrow)
{
	this->pre_add_arrow(*(arrow.arrow), arrow.look);

	arrowId ret = _current_arrow_id;

	_arrows.push_back({std::move(arrow), _current_arrow_id});
	_current_arrow_id++;

	// std::cout<<_arrows[_arrows.size() - 1].id<<endl;
	return ret;
}

template<typename ArrowGeometrty>
void ArrowPack<ArrowGeometrty>::delete_arrow(arrowId id)
{
	if(_arrows.size() == 0)
		return;

	int index=-1;
	for(int i=0; i<(int)_arrows.size(); i++)
	{
		if(_arrows[i]->id == id)
		{
			index = i;
			break;
		}
	}

	if(index == -1)
	{
		ssl::err("ArrowPack<arrowType>::delete_arrow(arrowId id): asked for non existing arrow");
	}

	this->swapop_indexed_arrow(index);
}


template<typename ArrowGeometrty>
void ArrowPack<ArrowGeometrty>::swapop_indexed_arrow(unsigned int arrow_index)
{
	if(arrow_index != _arrows.size() - 1)
	{
		this->move_last_arrow(arrow_index);

		_arrows[arrow_index] = std::move(_arrows[_arrows.size()-1]);
	}
	else
	{
		this->pre_pop_last_arrow();
	}

	_arrows.pop_back();
}

template<typename ArrowGeometrty>
void ArrowPack<ArrowGeometrty>::print() const
{
	this->check_sizes();

	std::cout<<"ArrowPack (size="<<this->size()<<"): {";
	for(int i=0; i<(int)_arrows.size();i++)
	{
		std::cout<<_arrows[i].id;
		if(i != (int)_arrows.size()-1)
		{
			std::cout<<", ";
		}
	}
	std::cout<<"}";
}

#endif //_ARROW_PACK_HPP_
