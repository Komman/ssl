#ifndef _ROOMED_GRAPH_HPP_
#define _ROOMED_GRAPH_HPP_

#include "../motor/bordered_graph.hpp"

class RoomedGraph;
class PreRoomedGraph : public BorderedGraph
{
protected:
	PreRoomedGraph(const BorderedGraph& graph);

};

/*
	A roomed graph is a graph that:
	- Is planar
	- Is connected
	- Has no edge inside a same face
	- No self loops

	There always exists such a graph given more
	than 3 points and a border since considering
	only the border is a rommed graph.
*/

class RoomedGraph : public PreRoomedGraph
{
public:
	/*
		The graph must be planar
	*/
	RoomedGraph(const BorderedGraph& graph);

	/*
		A roomed graph cannot be modfied with these methods
		because they can unroom the graph
	*/
	void delaunay_triangulation() = delete;
	void set_chicken_triangulation() = delete;
	void remove_small_degree(uint max_degree, const std::unordered_set<uint>& dontremove = {}) = delete;
	void remove_big_degree(uint min_degree, const std::unordered_set<uint>& dontremove = {}) = delete;
	void remove_close_vertices(float distance, const std::unordered_set<uint>& dontremove = {}) = delete;
	void remove_border(const std::vector<uint>& border) = delete;
	void remove_self_loops() = delete;
	uint add_vertex() = delete;
	void add_edge(const Edge& e) = delete;
	void add_oriented_edge(const Edge& e) = delete;
	void remove_vertex(uint vertex) = delete;
	void remove_oriented_edge(const Edge& e) = delete;
	void remove_edge(const Edge& e) = delete;
};

#endif //_ROOMED_GRAPH_HPP_
