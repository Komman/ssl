#ifndef _CASTLE_PLACER_HPP_
#define _CASTLE_PLACER_HPP_

#include <memory>

#include "../utils/space_grid.hpp"
#include "../utils/plan_graph.hpp"

#include "../motor/static_lightor.hpp"
#include "../motor/hitbox_context.hpp"
#include "../motor/reliefed_plan_dual_graph.hpp"
#include "../motor/facechitecture.hpp"
#include "../motor/geo_cylinder_hitbox.hpp"

#include "castle_graph_buildor.hpp"
#include "castle_positioner.hpp"
#include "watching_towers.hpp"
#include "simple_wall.hpp"
#include "lanterns.hpp"
#include "magic_tower.hpp"
#include "magic_towers_circle.hpp"
#include "arc_bridge.hpp"
#include "arc_door.hpp"
#include "casern.hpp"
#include "fat_tower.hpp"
#include "curch.hpp"
#include "face_arc.hpp"
#include "counter_fort.hpp"
#include "sky.hpp"
#include "archer.hpp"
#include "castle_base.hpp"
#include "castle_simple_elevator.hpp"

struct castleTools
{
	Ground*           ground;
	StaticLigthor*    slightor;
	VerticalShadower* vshadower;
	HitboxContext*    context;
	Sky*              sky;
	Archer*           player;
};

namespace CASTLE
{

	class CastlePlacer : public CASTLE::GraphBuildor
	{
	public:
		CastlePlacer(const castleTools& tools, const glm::vec3& position);

		void build();

		const WatchingTowers& simple_towers()  const;
		const MagicTower&     magic_towers()   const;
		const SimpleWall&     simple_walls()   const;
		const Lanterns&       lanterns()       const;
		const ArcBridge&      arc_bridges()    const;
		const ArcDoor&        arc_doors()      const;
		const FatTower&       fat_towers()     const;
		const FatTower&       squares_towers() const;
		const CastleSupport&  supports()       const;
		const Church&         churchs()        const;
		const FaceArc&        face_arcs()      const;
		const CounterFort&    counter_forts()  const;
		const SimpleElevator& elevators()      const;

		const PlanGraph& base_graph() const;
		const std::vector<std::unique_ptr<BasicHitbox>>& base_hitboxes() const;

		const StoredDrawableObject<glm::vec3, glm::vec3>& base() const;
		const CASTLE::Base& real_base() const;

	protected:
		WatchingTowers& watching_towers();
		SimpleElevator& moving_elevators();

	private:
		using Edge = Facechitecture::Edge;

		struct aTower
		{
			std::vector<uint> base_cycle;
			// x: delta x reducing of radius
			// y: heights
			std::vector<glm::vec2> sizes;
			float wall_size;
			glm::vec3 color;
			glm::vec3 lastcolor;
			glm::vec3 roofcolor;
		};

	private:
		void place_ext();
		void place_inside();
		void place_inside_base();

		void place_inside_base_old();
		glm::vec3 get_ext_direction(const glm::vec3& prev_tower,
							        const glm::vec3& cur_tower,
							        const glm::vec3& next_tower);



		
	private:
		castleTools _tools;

		SpaceGrid _grindside;
		PlanGraph _base_graph;

		std::unique_ptr<CASTLE::TheoricalArchitecture> _architecture;
		std::unique_ptr<CASTLE::Base> _real_base;

		StoredDrawableObject<glm::vec3, glm::vec3> _base;
		std::vector<std::unique_ptr<BasicHitbox>>  _base_hbx;
		glm::vec3 _base_color;
		float _verteices_spacing;

		Lanterns       _lanterns;
		WatchingTowers _simple_towers;
		MagicTower     _magic_towers;
		SimpleWall     _simple_walls;
		ArcBridge      _arc_bridges;
		ArcDoor        _arc_doors;
		FatTower       _fat_towers;
		FatTower       _squares_towers;
		CastleSupport  _support;
		Church         _churchs;
		FaceArc        _face_arcs;
		CounterFort    _counter_forts;
		SimpleElevator _elevators;

		Casern        	  _caserns;
		MagicTowersCircle _circles;
		bool _circles_placed;
	};
};

#endif //_CASTLE_PLACER_HPP_
