#ifndef _FACE_ARC_HPP_
#define _FACE_ARC_HPP_

#include "../motor/stored_vao_object.hpp"
#include "../motor/planar_building_instance.hpp"

class FaceArc : public StoredVAObject<PlanarBuildingInstances<glm::vec3, glm::vec3>>
{
public:
	FaceArc(HitboxContext& contex, const glm::vec3& color, float angle, float thickness_coeff);

	void place_range(const planarYPlace& place, float arc_length, float interspace);
	void place_range_no_contact(const planarYPlace& place, float arc_length, float interspace, const std::vector<std::unique_ptr<BasicHitbox>>&  hbx);

protected:
	std::unique_ptr<BasicHitbox> build_hitbox(const planarYPlace& place) const override;

private:
	enum buffersRole {RELATIVE_POS=0, COLOR};
	
	void build_base();

private:
	glm::vec3 _color;
	float _angle;
	float _thickness_coeff;
};

#endif //_FACE_ARC_HPP_
