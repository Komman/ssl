#ifndef _HOLED_DISC_HPP_
#define _HOLED_DISC_HPP_

#include "../../ssl/src/uniform.hpp"
#include "../../ssl/src/vertex_array_object.hpp"

#include "../motor/stored_drawable_object.hpp"
	
/*
	vec3: relative_pos (and on a normalized disc) and extrm_info
				extrm_info: +0.0f if inf
				             1.0f if sup
	vec3 (instance) : sphere center
	vec4 (instance) : .x : radius
		 			  .y : thickness
		 			  .z : start time
		 			  .w : last time
	vec4 (instance) : color and speed
*/
class HoledDisc : public StoredDrawableObject<glm::vec4, glm::vec3, glm::vec4, glm::vec4>
{
public:
	struct discParams
	{
		const glm::vec3& center;
		const glm::vec3& color;
		float radius;
		float thickness;
		float duration;
		float speed;
	};

public:	
	/*
		precision : number of circle discretisation
	*/
	HoledDisc(ssl::draw_type drtype,
			  unsigned int precision,
			  const std::string& name);
	virtual ~HoledDisc() {}

	void animate(float dt);
	void add(const discParams& params);

	uint instances_amount() const;
	const VertexArrayObject& get_vao() const;

private:
	enum buffersRole {RELATIVE_POS=0, CENTER, INFOS, COLOR_SPEED};

	void check_sizes() const;
	void swapop(uint instance_index);
	bool has_expired(uint instance_index) const;

private:
	std::vector<glm::vec3> _normalized;
	Uniform<float> _time;
	VertexArrayObject _vao;
};

#endif //_HOLED_DISC_HPP_
