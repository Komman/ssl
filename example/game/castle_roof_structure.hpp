#ifndef _CASTLE_ROOF_STRUCTURE_HPP_
#define _CASTLE_ROOF_STRUCTURE_HPP_

#include "castle_face_structure.hpp"

namespace CASTLE
{
	class RoofStructure : public FaceStructure
	{
	public:
		RoofStructure(const baseBuiling& base, uint face);

	protected:
		void build_face(const std::vector<doorWall>& walls_to_build) override;
		void build_wall_deco(const std::vector<doorWall>& walls_to_build) override;

	private:
		float _underbluf;
		float _height;
		float _topheight;
		float _pilheight_coeff;
	};
};


#endif //_CASTLE_ROOF_STRUCTURE_HPP_
