#include "test_arrow.hpp"

#include "ingame.hpp"
#include "effects.hpp"

using namespace std;
using namespace glm;

TestArrow::TestArrow(const arrowStart& start)
	: Arrow(start),
	  _light_intensity(100.0f),
	  _planted_time(-1.0f),
	  _duration(3.5f),
	  _smoke_emission(30.0f),
	  _light(effects::dynamic_lightor(), {glm::vec3(0.0), glm::vec3(1.0, 0.8, 0.8), 0.0f, 100.1f})
{
	this->init();
}

void TestArrow::init()
{

}

void TestArrow::animate(const PhysicalContext& context, float dt)
{
	this->Arrow::animate(context, dt);

	uint smoke_amount = _smoke_emission.animate(dt); 

	for(uint i=0; i<smoke_amount; i++)
	{
		Particle p(this->get_position() - this->get_directionN()*this->get_length()*0.9f,
				   vec4(1.0,0.6,0.3, -0.008)*utils::random_float(2.0, 4.0),
				   this->get_size()*0.75f,	
				   1.0f,
				   1.0f);

		p.set_speed(-this->get_directionN()*this->get_size()*6.0f + utils::random_vec3(vec3(-1.0), vec3(1.0))*this->get_size());

		effects::clouds().add_particle(p);
	}
	

	// if(!this->is_planted())
	// {
	// 	effects::clouds().add_particle(Particle(
	// 		this->get_position() - this->get_directionN()*this->get_length(),
	// 		utils::random_directionN()*0.2f,
	// 		glm::vec3(0.0),
	// 		0.05f,	
	// 		1.0f,
	// 		3.0f
	// 	));
	// }

	if(!(_light.is_destroyed()))
	{
		_light.set_position(this->get_position() - this->get_directionN()*this->get_length());
	}	
}


void TestArrow::planting_reaction(const glm::vec3& position, const glm::vec3& normal, const glm::vec3& impact_speed)
{
	// this->unplant();
	// this->launch(this->get_position(), impact_speed - vvv::y() * 2.0f * impact_speed.y);

	float intensity = _light_intensity * this->get_size() / 2.0f;

	_planted_time = ingame::gtime();

	vec3 plant_impact = position + vvv::y()*3.0f*intensity/_light_intensity;

	_light.destroy();

	// effects::add_homogenous_explosion(plant_impact,
	// 							      _light_intensity,
	// 							      _duration,
	// 							      vec3(1.00, 0.20, 0.20),
	// 							      vec3(0.80, 0.45, 0.05),
	// 							      vec3(1.00, 0.80, 0.50),
	// 							      vec3(1.00, 0.40, 0.40));

	effects::add_homogenous_explosion(plant_impact,
								      intensity,
								      utils::random_float(0.0, _duration*2.0f),
								      utils::random_vec3()*0.8f,
								      utils::random_vec3(),
								      utils::random_vec3(),
								      utils::random_vec3());
}

bool TestArrow::to_destroy()
{	
	if(_planted_time == -1.0f)
	{
		return false;
	}
	else
	{
		return (ingame::gtime() - _planted_time > 0.0f);
	}
	
	// return false;
	// return this->is_planted();
}

