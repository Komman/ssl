#ifndef _GAME_CONSOLE_HPP_
#define _GAME_CONSOLE_HPP_

#include "../motor/window_console.hpp"
#include "archer.hpp"
#include "castle_animator.hpp"

struct ConsoleObjects
{
	Archer* archer;
	SharedRessources* shared_ressources;
	CASTLE::CastleAnimator* castle;
};

class GameConsole : public WindowConsole
{
public:
	enum gameCommands {TP, MOVE, SPEED, JUMP, GAMEMODE, BLOOM, TARGET, UP, DIR, DIRX, DIRY, DIRZ, COLLISIONS,
					   GAME_COMMANDS_AMOUNT};
	enum controlableTypes {NO_TYPE, FLOAT, VEC3, INTEGER,
						   GAME_TYPES_AMOUT};
	struct commandArgs
	{
		std::string command;
		int arg_number; //-1 if not fixed
		controlableTypes type;
	};

	static inline const commandArgs GCOMMANDS[GAME_COMMANDS_AMOUNT] = {
		{"/tp", 2, VEC3},
		{"/move", 2, VEC3},
		{"/speed", 2, FLOAT},
		{"/jump", 2, FLOAT},
		{"/gamemode", 2, INTEGER},
		{"/bloom", 2, INTEGER},
		{"/target", 2, INTEGER},
		{"/up", 2, FLOAT},
		{"/dir", 1},
		{"/x", 1},
		{"/y", 1},
		{"/z", 1},
		{"/col", 2, INTEGER}
	};

public:
	GameConsole(ConsoleObjects& objs, MousekeySwitcher* switcher, int write_key, float lasting);

	void todo(const std::string& msg);

protected:
	std::string response(const std::vector<std::string>& msg);

private:
	std::string check(const std::vector<std::string>& msg);
	void show_vector(const glm::vec3& dirN, const glm::vec4& particle_color);

private:
	ConsoleObjects _objs;

};

#endif //_GAME_CONSOLE_HPP_
