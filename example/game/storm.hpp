#ifndef _STORM_HPP_
#define _STORM_HPP_

#include "../motor/tritube.hpp"
#include "ground.hpp"

class Storm : public Tritube
{
public:

	static inline std::string TOUCH_ADD_NAME    = "touch";
	static inline std::string POWER_ADD_NAME    = "power";
	static inline std::string THIKNESS_ADD_NAME = "size";

	Storm(const Ground& ground,
		  const std::string& uniform_pre_name,
		  const glm::vec3& start_position,
		  float power,
		  float thikness,
		  float largery,
		  float steps);

	void regenerate(const glm::vec3& start_position,
					float power,
				    float thikness,
				    float largery,
				    float steps);

	void set_impact(const glm::vec3& position);
	void compute_impact();

	void change_powered_color(const glm::vec4& new_powered_color);
	const glm::vec4& powered_color(){return _powered_color;}

//private:

	glm::vec3 compute_touch();

	const Ground&      _ground;
	Uniform<glm::vec3> _touch;
	Uniform<float>     _power;
	Uniform<float>     _thikness;
    float              _largery;
    float              _steps;

	Uniform<glm::vec4> _powered_color;

};

#endif //_STORM_HPP_
