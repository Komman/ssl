#ifndef _UNIVERSE_HPP_
#define _UNIVERSE_HPP_

#include "../motor/shared_ressources.hpp"
#include "world.hpp"

class Universe
{
public:
	Universe();

	void animate(float dt);
	void draw();

	void set_current_world(BasicWorld* world);

private:
	BasicWorld* _world;
};

#endif //_UNIVERSE_HPP_
