#include "castle_open_structure.hpp"

#include "../../ssl/src/vvv.hpp"
#include "../utils/mutils.hpp"

#include "../motor/building.hpp"
#include "../motor/meshbuild.hpp"
#include "../motor/wall_hitbox.hpp"
#include "../motor/paver_hitbox.hpp"
#include "../motor/bevel_hitbox.hpp"

#include "castle_colors.hpp"
#include "castle_placer.hpp"

namespace CASTLE
{
	OpenStructure::OpenStructure(const baseBuiling& base, uint face)
		: FaceStructure(base, face),
		  _barrierw(this->wall_radius()*0.5f),
	  	  _underbluf(0.5f*this->ND()),
	  	  _bluff((rand()%5 >= 2))
	{

	}

	void OpenStructure::build_face(const std::vector<doorWall>& walls_to_build)
	{		
		std::vector<uint> startindices;

		if(_bluff)
		{
			startindices = utils::uirange(this->bluffup(walls_to_build, _underbluf, _underbluf*2.0f, this->wall_radius()*0.4f));
		}
		else
		{
			startindices = this->mesh_vertices_indices(walls_to_build);
		}
		
		auto indices = this->build_border(this->index_wall(walls_to_build, startindices),  _barrierw, _barrierw, _bluff ? _underbluf : _underbluf*3.0f, _underbluf*2.0f + _barrierw*1.5f);

		this->put_roofloor(indices, _underbluf);
		
		this->fill_walls(walls_to_build);
	}

	void OpenStructure::build_wall_deco(const std::vector<doorWall>& walls_to_build)
	{
		bool cuve = true;
		float cuveminh = 5.0f*this->ND();

		for(uint i=0; i<walls_to_build.size(); i++)
		{
			doorWall w = walls_to_build[i];

			if(w.dh < cuveminh)
			{
				cuve = false;
				break;
			}
		}

		if(cuve)
		{
			this->build_wall_deco_cuve(walls_to_build);
		}
		else
		{
			this->build_wall_deco_exposed(walls_to_build);
		}
	}

	void OpenStructure::build_wall_deco_cuve(const std::vector<doorWall>& walls_to_build)
	{
		uint N = walls_to_build.size();
		bool  lantern  = false;
		float hlantern = this->player_size()*4.0f;

		for(uint i=0; i<N; i++)
		{
			doorWall w = walls_to_build[i];
			
			wallInfos infos = this->wall_infos(w);

			if(!lantern && w.dh > this->counterfort_minh()*1.5f + this->safe_height())
			{
				float h = this->height() + hlantern;
				if(this->is_ext_door(w))
				{
					h = w.door.altitude + this->player_size()*1.0f + w.door.height;
				}

				lantern = this->try_lantern(geom::yfixed(geom::middle(infos.bot.first, infos.bot.second), h), -infos.normalN);
			}
		}
	}

	void OpenStructure::build_wall_deco_exposed(const std::vector<doorWall>& walls_to_build)
	{
		uint N = walls_to_build.size();
		bool  lantern  = false;
		float hlantern = this->player_size()*4.0f;

		for(uint i=0; i<N; i++)
		{
			doorWall w = walls_to_build[i];
			doorWall wn = walls_to_build[(i+1)%N];

			if(w.dh <= 0)
				continue;

			wallInfos infos = this->wall_infos(w);

			if(!lantern && wn.dh > 0)
			{
				wallInfos infosn = this->wall_infos(wn);
				glm::vec3 bissN = -geom::trigo_bissectriceN(infos.bot.first, infos.bot.second, infosn.bot.second);
				
				
				glm::vec3 pos = infos.bot.second + vvv::y()*hlantern;

				if(w.dh  > hlantern+this->safe_height()
				&& wn.dh > hlantern+this->safe_height())
				{
					lantern = this->try_lantern(pos, bissN);
				}
			}

			if(!lantern && w.dh > this->counterfort_minh()*1.5f + this->safe_height())
			{
				float h = hlantern;
				if(this->is_ext_door(w))
				{
					h = this->player_size()*1.0f + w.door.height;
				}

				lantern = this->try_lantern(geom::yfixed(geom::middle(infos.bot.first, infos.bot.second), w.door.altitude + h), -infos.normalN);
			}

			if(w.opbuild->get_role() == CATHEDRAL)
			{
				this->place_cathedral_deco(w);
				continue;
			}

			if(w.dh > this->counterfort_minh()*3.0f + this->safe_height())
			{
				if(infos.length < _sizes.pilers.x*6.0f)
				{
					if(infos.length > _sizes.pilers.x*4.0f)
					{
						this->place_counterforts_range(w, 0.0f);
					}
					else
					{
						this->place_pilers_range(w, 0.0f);
					}
				}
				else
				{
					uint rd = rand()%3;

					if(rd>=0 && rd<=0)
					{
						this->place_pilers_range(w, 0.0f);
					}
					if(rd>=1 && rd<=2)
					{
						this->place_counterforts_range(w, 0.0f);
					}
				}
			}
			else
			{
				if((rand()%4)<=3)
				{
					if(w.dh > this->counterfort_minh()*1.5f + this->safe_height())
					{
						if((rand()%3)<=1)
						{
							if(this->is_ext_door(w) && infos.length > _sizes.pilers.x*8.0f)
							{
								this->place_pilers_range(w, 0.0f, 1.0f);
							}
							else
							{
								this->place_window_range(w, this->topfaceh(w)-_sizes.windows.y);
							}
						}
						else
						{
							this->place_window_range(w, this->topfaceh(w)-_sizes.windows.y);
						}
					}
					else
					{
						if(w.dh > _sizes.loopholes.y + this->safe_height() && rand()%2==0)
						{
							this->place_loophole_range(w, this->topfaceh(w)-_sizes.loopholes.y);
						}
					}
				}
			}
		}
	}

};
