#ifndef _DRAWABLE_ARROW_PACK_HPP_
#define _DRAWABLE_ARROW_PACK_HPP_

#include "buffered_arrow_pack.hpp"

#include "../motor/game_drawer.hpp"

template<typename ArrowGeometrty, uint FIRST_INSTANCE_BUFFER, typename... Types>
class DrawableArrowPack : public BufferedArrowPack<ArrowGeometrty, FIRST_INSTANCE_BUFFER, Types...>
{
public:
	template<typename Type>
	struct storedDrawType
	{
		using type = ssl::draw_type;
	};

public:
	DrawableArrowPack(ssl::draw_type element_draw_type, typename storedDrawType<Types>::type ... buffers_draw_type);

	void draw(GameDrawer& game_drawer);

protected:
	virtual GameDrawer::gameDrawTypes get_typed_draw() const =0;
	virtual Drawer& get_drawer() const =0;
};


template<typename ArrowGeometrty, uint FIRST_INSTANCE_BUFFER, typename... Types>
DrawableArrowPack<ArrowGeometrty, FIRST_INSTANCE_BUFFER, Types...>::DrawableArrowPack(ssl::draw_type element_draw_type, typename storedDrawType<Types>::type ... buffers_draw_type)
	: BufferedArrowPack<ArrowGeometrty, FIRST_INSTANCE_BUFFER, Types...>(element_draw_type, buffers_draw_type...)
{

}

template<typename ArrowGeometrty, uint FIRST_INSTANCE_BUFFER, typename... Types>
void DrawableArrowPack<ArrowGeometrty, FIRST_INSTANCE_BUFFER, Types...>::draw(GameDrawer& game_drawer)
{
	game_drawer.draw_typed(this->get_typed_draw(), this->get_drawer(), this->get_drawable().get_vao());
}

#endif //_DRAWABLE_ARROW_PACK_HPP_
