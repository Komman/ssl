#include "watching_towers.hpp"
#include "effects.hpp"

using namespace std;
using namespace glm;

WatchingTowers::WatchingTowers(const BasicTarget* target, HitboxContext& hbx_context, Lanterns& lanterns, VerticalShadower& vshadower)
	: SimpleTower(hbx_context, lanterns, vshadower),
	  _target(target),
	  _towers_life(),
	  _cannons(2.0f)
{

}

glm::vec3 WatchingTowers::get_head_position(uint towerid) const
{
	return this->get_tower_position(towerid)
		 + vvv::y()*(_towers_life[towerid].starth + _towers_life[towerid].height.get_value())
		 + vvv::y()*this->get_tower_info(towerid).tower.size*2.0f;
}

static float trange = 50.0f;


void WatchingTowers::update_towers_life()
{
	uint n = this->get_towers_amount();

	bool toreupdate = (n == _towers_life.size());
	
	while(_towers_life.size() < n)
	{
		uint i = _towers_life.size();
		auto tinfos = this->get_tower_info(i).tower;

		TowerShooter shooter(tinfos.position, trange);
		shooter.set_target(_target);

		#warning On "tinfos.size*3.0f", if the missile is inclined, it can touch the tower
		_towers_life.push_back({
			.starth = this->get_tower_height(i),
			.maxdh  = 17.0f,
			.target_range  = trange,
			.light_range = trange*3.5f,
			.dress_range = trange*4.5f,
			.cannon_size = tinfos.size*3.0f,
			.shotping_coeff = 0.2f,
			.bissecdirN = vvv::x(),
			.behavior = SLEEPING,
			.height = SmoothSetter<float>(0,12),
			.deployment = SmoothSetter<float>(0,4),
			.cannondirN = SmoothSetter<vec3>(vvv::x(), 0.5f),
			.shooter = shooter,
			.lamp = MeshedLampRay(lampRay{
				.color   = vec3(1,0.2,0.2),
				.center  = tinfos.position,
				.dirN    = vvv::x(),
				.opening = 0.5f,
				.range   = 800.0f
			})
		});

		_cannons.add_instance(towerCannon{
			.position = tinfos.position + vvv::y()*this->get_tower_height(i),
			.orientationN = vvv::x(),
			.size = 0.0f,
			.light = 0
		});
	}

	if(toreupdate)
	{
		int S = _towers_life.size();
		for(int i=0; i<S; i++)
		{
			int h = (i + S-1) % S;
			int j = (i + 1) % S;
		
			vec2 htoi = geom::safe_normalize(geom::projplan(this->get_tower_position(i) - this->get_tower_position(h))); 
			vec2 itoj = geom::safe_normalize(geom::projplan(this->get_tower_position(j) - this->get_tower_position(i)));
	
			vec3 biss = geom::unprojplan(geom::trigo_normal(htoi));

			 _towers_life[i].bissecdirN = geom::unprojplan(htoi - itoj);
			 _towers_life[i].bissecdirN *= (glm::dot(biss, _towers_life[i].bissecdirN) > 0) ? 1.0f : -1.0f;
			 _towers_life[i].bissecdirN = geom::safe_normalize(_towers_life[i].bissecdirN);
		}
	}
}

void WatchingTowers::reset_target(const BasicTarget* target)
{
	_target = target;

	for(uint i=0; i<_towers_life.size(); i++)
	{
		_towers_life[i].shooter.set_target(target);
	}
}

void WatchingTowers::animate_tower(uint tower_id, float dt)
{
	if(tower_id >= _towers_life.size())
	{
		mot::err("WatchingTowers::animate_tower(): wrong tower id");
	}

	glm::vec3  tpos  = this->get_tower_position(tower_id);
	towerLife& tlife = _towers_life[tower_id];
	float distarget  = glm::distance(tpos, _target->position());

	// this->set_tower_growth(tower_id, dt, tlife, distarget);
	// this->position_cannon(tower_id, tlife, dt);

	// if(tlife.behavior == SHOOTING && this->full_deployed(tlife))
	// {
	// 	this->try_to_shot(tlife);
	// }
}

glm::vec3 WatchingTowers::cannon_position(uint tower_id) const
{
	return this->get_head_position(tower_id) - vvv::y()*1.0f;
}


void WatchingTowers::position_cannon(uint tower_id, towerLife& tlife, float dt)
{
	if(!(this->tower_deployed(tlife)))
	{
		tlife.shooter.reload();
	}
	vec3 center   = this->cannon_position(tower_id);
	vec3 shootpos = center + tlife.cannondirN.get_value()*tlife.deployment.get_value();
	
	float calight = 0;
	tlife.lamp.disable();

	if(tlife.behavior == SHOOTING || tlife.behavior == TARGETING)
	{
		tlife.cannondirN.set_objective_value(geom::safe_normalize(_target->position() - center));
		calight = -3.0f;
	}
	if(tlife.behavior == SPYING)
	{	
		if(tlife.cannondirN.objective_reached())
		{
			tlife.cannondirN.set_objective_value(geom::safe_normalize(tlife.bissecdirN + geom::yfixed(utils::random_vec3(vec3(-1), vec3(1)), utils::random_float(-1.0f,0.0f))));
		}
	}

	if(tlife.deployment.get_value() == 0.0f)
	{
		tlife.cannondirN.set_value_force(tlife.bissecdirN);
		tlife.cannondirN.set_objective_value(tlife.bissecdirN);
	}

	if(this->full_deployed(tlife) && (tlife.behavior == SPYING || tlife.behavior == TARGETING))
	{
		calight = 4.5f;
		tlife.lamp.enable();
		tlife.lamp.set_ray(lampRay{
			.color   = vec3(1,0.7,0.4)*1.5f,
			.center  = center,
			.dirN    = tlife.cannondirN.get_value(),
			.opening = 0.03f,
			.range   = tlife.light_range
		});	
	}

	_cannons.change_cannon(tower_id, towerCannon{
		.position = center,
		.orientationN = tlife.cannondirN.get_value(),
		.size = std::min(tlife.deployment.get_value()*(1.0f+tlife.shotping_coeff), tlife.cannon_size),
		.light = calight
	});

	tlife.shooter.set_position(shootpos);
	tlife.shooter.animate(dt);
	tlife.cannondirN.animate(dt);
}

void WatchingTowers::try_to_shot(towerLife& tlife)
{
	bool shot = tlife.shooter.reloadable_shoot(utils::random_float(2.0f,10.0f));
			
	if(shot)
	{
		tlife.deployment.set_value_force(tlife.cannon_size/(1.0f+tlife.shotping_coeff)*0.8f);
	}
}

bool WatchingTowers::full_deployed(towerLife& tlife) const
{
	return (this->tower_deployed(tlife) && tlife.deployment.get_value() == tlife.cannon_size);
}

bool WatchingTowers::tower_deployed(towerLife& tlife) const
{
	return (tlife.height.get_value() == tlife.maxdh);
}

#include "game_console.hpp"

void WatchingTowers::set_tower_growth(uint tower_id, float dt, towerLife& tlife, float distarget)
{
	// SET HEIGHT AND STATES
	if(distarget < tlife.dress_range)
	{
		tlife.height.set_objective_value(tlife.maxdh);
		if(tlife.height.get_value() == tlife.maxdh)
		{
			tlife.deployment.set_objective_value(tlife.cannon_size);
		}

		if(this->full_deployed(tlife))
		{
			if(tlife.behavior != SHOOTING)
			{
				if(distarget < tlife.light_range)
				{
					tlife.behavior = SPYING;
				}

				if(distarget < tlife.target_range)
				{
					tlife.behavior = TARGETING;
				}
			}
		}

		if(distarget > tlife.light_range)
		{
			tlife.behavior = DRESSING;
		}
	}
	else
	{
		tlife.behavior = SLEEPING;
		tlife.deployment.set_objective_value(0.0f);

		if(tlife.deployment.get_value() == 0.0f)
		{
			tlife.height.set_objective_value(0.0f);
		}
	}

	if(tlife.behavior == SPYING || tlife.behavior == TARGETING)
	{	
		if(glm::dot(tlife.cannondirN.get_value(), geom::safe_normalize(_target->position() - this->cannon_position(tower_id))) > 0.994f)
		{
			tlife.behavior = SHOOTING;
			tlife.deployment.set_value_force(tlife.cannon_size/(1.0f+tlife.shotping_coeff));		
		}
	}

	tlife.deployment.animate(dt);
	if(tlife.height.animate(dt))
	{
		this->set_tower_height(tower_id, tlife.starth + tlife.height.get_value());
	}
}

void WatchingTowers::animate(float dt)
{
	this->update_towers_life();

	for(uint i=0; i<_towers_life.size(); i++)
	{
		this->animate_tower(i, dt);
	}
}

const TowerCannon& WatchingTowers::cannons() const
{
	return  _cannons;
}

