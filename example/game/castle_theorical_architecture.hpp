#ifndef _CASTLE_THEORICAL_ARCHITECTURE_HPP_
#define _CASTLE_THEORICAL_ARCHITECTURE_HPP_

#include "castle_labyrinth_graph.hpp"

namespace CASTLE
{
	enum doorPlace {INSIDE, OUTSIDE};

	struct doorInfo
	{
		doorPlace place;
		doorType  type;
	};	

	class TheoricalArchitecture : public LabyrinthGraph
	{
	public:
		TheoricalArchitecture(const BorderedGraph& g, const graphHeightParam& params);

		doorPlace get_door_place(const Edge& e) const;
		doorInfo get_door_infos(const Edge& e) const;

	private:
		void set_doors();
		doorInfo random_door(uint f1, uint f2, doorPlace door_place);

	private:
		std::unordered_map<Edge, doorInfo, Graph::hashEdge> _doors;
	};
};

#endif //_CASTLE_THEORICAL_ARCHITECTURE_HPP_
