#include "tower_cannon.hpp"

#include "../motor/building.hpp"

using namespace std;
using namespace glm;

TowerCannon::TowerCannon(float size)
	: StoredVAObject(STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, DYNAMIC_DRAW, DYNAMIC_DRAW),
	  _size(size)
{
	this->build_mesh();
}

void TowerCannon::build_mesh()
{
	auto& mesh   = this->get_array_buffer<RELATIVE_POS>();
	auto& colors = this->get_array_buffer<COLORS>();

	std::vector<glm::vec2> thick_heights = {
		vec2(_size*1.0f, 0.0f/6.0f),
		vec2(_size*1.0f, 2.0f/6.0f),
		vec2(_size*0.7f, 2.0f/6.0f),
		vec2(_size*0.7f, 3.0f/6.0f),
		vec2(_size*1.0f, 3.0f/6.0f),
		vec2(_size*1.0f, 4.0f/6.0f),
		vec2(_size*0.7f, 4.0f/6.0f),
		vec2(_size*0.7f, 5.0f/6.0f),
		vec2(_size*1.0f, 5.0f/6.0f),
		vec2(_size*1.0f, 6.0f/6.0f),
		vec2(_size*0.5f, 6.0f/6.0f),
		vec2(_size*0.5f, 5.5f/6.0f),
		vec2(_size*0.1f, 5.5f/6.0f)
	};

	for(uint i=0; i<4; i++) {colors.add_value(vec3(0.1f)*1.0f);}
	for(uint i=0; i<4; i++) {colors.add_value(vec3(0.1f)*1.0f);}
	for(uint i=0; i<4; i++) {colors.add_value(vec3(0.1f)*1.3f);}
	for(uint i=0; i<4; i++) {colors.add_value(vec3(0.1f)*1.3f);}
	for(uint i=0; i<4; i++) {colors.add_value(vec3(0.1f)*1.0f);}
	for(uint i=0; i<4; i++) {colors.add_value(vec3(0.1f)*1.0f);}
	for(uint i=0; i<4; i++) {colors.add_value(vec3(0.1f)*1.3f);}
	for(uint i=0; i<4; i++) {colors.add_value(vec3(0.1f)*1.3f);}
	for(uint i=0; i<4; i++) {colors.add_value(vec3(0.1f)*1.0f);}
	for(uint i=0; i<4; i++) {colors.add_value(vec3(0.1f)*1.0f);}
	for(uint i=0; i<4; i++) {colors.add_value(vec3(0.1f)*1.0f);}
	for(uint i=0; i<4; i++) {colors.add_value(vec3(0.1f)*1.0f);}
	for(uint i=0; i<4; i++) {colors.add_value(vec3(0.1f)*1.0f);}

	building::hanoi::squared::sided(
		mesh, 
		this->get_elements(),
		thick_heights,
		vec3(0),
		spaceBase{
			vvv::x(),
			vvv::y(),
			vvv::z()
		}
	);

	while(colors.size() < mesh.size())
	{
		colors.add_value(vec3(0.3f));
	}

	if(mesh.size() != colors.size())
	{
		mot::err("TowerCannon::build_mesh(): unbalanced buffers sizes");
	}
}

void TowerCannon::add_instance_in_buffers(const towerCannon& infos)
{
	this->get_array_buffer<DEPS_LIGHT>().add_value(vec4(infos.position, infos.light));
	this->get_array_buffer<ORIENTATION_N_SIZE>().add_value(vec4(infos.orientationN, infos.size));
}

void TowerCannon::change_cannon(uint instance_index, const towerCannon& infos)
{
	this->get_array_buffer<DEPS_LIGHT>().change_subvalue(instance_index, vec4(infos.position, infos.light));
	this->get_array_buffer<ORIENTATION_N_SIZE>().change_subvalue(instance_index, vec4(infos.orientationN, infos.size));
}

