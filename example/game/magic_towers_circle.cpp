#include "magic_towers_circle.hpp"

#include "../utils/geom.hpp"

using namespace glm;

MagicTowersCircle::MagicTowersCircle(MagicTower* towers,
						  			 ArcBridge*  bridges,
						  			 ArcDoor*    doors)
	: CastleBuildingPlacer(
		{
			{
				valuedCircleInfo{
					vec2(0),
					1,
					1
				}
			},
			{}
		}),
	  _doors_walls_color(vec3(0.7,0.5,0.3)*0.8f),
	  _towers(towers),
	  _bridges(bridges),
	  _doors(doors)
{
	
}

void MagicTowersCircle::place_building_only(const magicTowerCircle& builing, float h) 
{
	auto b = builing;
	b.position.y += h;

	this->add_tower_circle(b);
}

magicTowerCircle MagicTowersCircle::generate_building(const genParam& g) 
{
	uint amount = 4 + rand()%6;
	float tr   = utils::random_float(g.maxsize*0.15f, g.maxsize*0.25f)/(1.0f + amount/20.0f);

	return magicTowerCircle{
		.position      = vec3(g.position.x, 0.0f, g.position.y),
		.tower_count   = amount,
		.radius        = g.maxsize-tr,	
		.towers_size   = tr,
		.minmax_height = vec2(
			utils::random_float(g.maxsize, g.maxsize*5.0f),
			utils::random_float(g.maxsize*6.0f, g.maxsize*13.0f))
	};
}

planarPlace MagicTowersCircle::get_place(const magicTowerCircle& g) 
{
	return {
		.center = geom::projplan(g.position),
		.size   = vec2(g.radius),
		.dirN   = vec2(1,0)
	};
}

void MagicTowersCircle::add_tower_circle(const magicTowerCircle& mcircle, bool shadow)
{
	this->add_tower_circle(
		mcircle.position,
		mcircle.tower_count,
		mcircle.radius,
		mcircle.towers_size,
		mcircle.minmax_height
	);
}

void MagicTowersCircle::add_tower_circle(const glm::vec3& position,
						 				 uint  tower_count,
						 				 float radius,
						 				 float towers_size,
						 				 const glm::vec2& minmax_height,
						 				 bool shadow)
{
	std::vector<magicTower> towers_conf;
	for(uint i=0; i<tower_count; i++)
	{
		float coeff  = float(i) / float(tower_count);
		float angle  = coeff*float(2.0f*M_PI);
		float height = glm::mix(minmax_height.x, minmax_height.y, coeff);
		vec3  pos    = geom::circle_point(position, vvv::x(), vvv::y(), angle, radius - towers_size);

		towers_conf.push_back({
			.position = pos,  
			.height   = height,
			.size     = towers_size
		});

		_towers->add_tower(towers_conf[towers_conf.size()-1], shadow);
	}



	for(uint i=0; i<towers_conf.size()-1; i++)
	{
		const auto& t = towers_conf[i];

		vec3 dir = (towers_conf[i+1].position - t.position);
		vec2 dir2D = vec2(dir.x, dir.z);
		dir = normalize(dir);

		_bridges->add_bridge({
			.start        = t.position + vvv::y()*(t.height - t.size*1.5f),
			.direction2DN = normalize(dir2D),
			.size         = vec3(t.size/2.0f,
								 (minmax_height.y - minmax_height.x)/4.0f,
								 glm::length(dir2D))
		});

		_doors->add_door({
			.pos          = t.position + vvv::y()*(t.height - t.size*1.5f) + dir*t.size/2.0f,
			.dir2D        = normalize(dir2D),
			.size         = vec3(t.size/2.2f,
								 t.size/2.2f,
								 t.size*0.3f),
			.wall_color   = _doors_walls_color
		});

		_doors->add_door({
			.pos          = t.position + vvv::y()*(t.height - t.size*1.5f) + dir*(glm::length(dir2D) - t.size/2.0f),
			.dir2D        = -normalize(dir2D),
			.size         = vec3(t.size/2.2f,
								 t.size/2.2f,
								 t.size*0.3f),
			.wall_color   = _doors_walls_color
		});
	}
}
