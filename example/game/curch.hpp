#ifndef _CURCH_HPP_
#define _CURCH_HPP_

#include "../motor/vertical_shadower.hpp"
#include "../motor/planar_symmetry.hpp"

#include "castle_building_inst.hpp"
#include "arc_door.hpp"

struct churchInfo
{
	glm::vec3 center;
	glm::vec2 dirN2D;
	glm::vec2 size;
};

class Church : public CastleBuildingInst<churchInfo,
									 2,
									 glm::vec3,
									 glm::vec3,
									 glm::vec4,
									 glm::vec4>
{
public:
	Church(HitboxContext& context, VerticalShadower* vshadower, ArcDoor* door);

	void add_church(const churchInfo& church);

protected:
	void place_building_only(const churchInfo& building, float h) override;
	churchInfo generate_building(const genParam& g) override;
	planarPlace  get_place(const churchInfo& g) override;


private:
	enum bufferRoles {RELATIVE_POS=0, COLOR, POS_SIZEY, DIR2D_SIZEXZ};

	void add_hitboxes(const churchInfo& church);

	void build_base();

	struct roofBuiling
	{
		/*
		  roof_indices
			  2
             / \
			0---1
		*/
		std::vector<uint>         roof_indices;
		PlanarSymmetry<glm::vec3> sym;
	};
	// Returns the roof base
	roofBuiling build_facade();
	void build_roof(roofBuiling& roof);
	void build_towers();
	void place_probells(const glm::vec3& center,
						float thick,
						const glm::vec3& size);
	void place_probell(const glm::vec3& center,
					   const glm::vec3& face_dirN,
					   const glm::vec3& size);


private:
	VerticalShadower* _vshadower;
	ArcDoor*          _door;

	float _xz;
	float _piler_size;
	float _size;
	float _roof_height;
	float _roof_over;
	float _roof_thickness;
	float _fulldist;
	float _fulldist_in;
	float _towers_heighs;
	float _toweroof_height;
	float _toweroof_over;
	float _lessover_roof_front;

	// xy: pos, z: size
	std::vector<glm::vec3> _tower_pos_size;
	/*
		first : x : radius
				y : height
		second: color
	*/
	std::vector<std::pair<glm::vec2, glm::vec3>> _tower_relief;

	glm::vec3 _base_color;
	glm::vec3 _roof_color;
	glm::vec3 _tower_color;
	glm::vec3 _tower_roof_color;
};

#endif //_CURCH_HPP_
