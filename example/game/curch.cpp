#include "curch.hpp"

#include "../motor/gutils.hpp"
#include "../motor/paver_hitbox.hpp"
#include "../motor/bevel_hitbox.hpp"
#include "../motor/hitbox_fusion.hpp"
#include "../motor/angular_symmetry.hpp"
#include "../motor/building.hpp"

using namespace std;
using namespace glm;

static float xy_coeff = 2;

Church::Church(HitboxContext& context, VerticalShadower* vshadower, ArcDoor* door)
	: CastleBuildingInst(context,
		{
			{},
			{
				valuedPolygon2DInfo{
					vec2(0),
					{
						vec2(-1.1f, -xy_coeff*(1.0f + 0.1f*1.2f))/2.0f,
						vec2(-1.1f, +xy_coeff*(1.0f + 0.1f*1.2f))/2.0f,
						vec2(+1.1f, +xy_coeff*(1.0f + 0.1f*1.2f))/2.0f,
						vec2(+1.1f, -xy_coeff*(1.0f + 0.1f*1.2f))/2.0f
					},
					1
				}
			}
		},
		STATIC_DRAW,
		STATIC_DRAW,
		STATIC_DRAW,
		STATIC_DRAW,
		STATIC_DRAW),
	 _vshadower(vshadower),
	 _door(door),
	 _xz(xy_coeff),
	 _piler_size(0.15),
	 _size(1.0f - _piler_size*0.8),
	 _roof_height(0.5),
	 _roof_over(0.1),
	 _roof_thickness(0.1),
	 _toweroof_height(1.6),
	 _toweroof_over(1.2),
	 _lessover_roof_front(0.6f),
	 _tower_pos_size({vec3(0,-0.3*_xz, 0.42)}),
	 _base_color(vec3(0.6,0.4,0.5)),
	 _roof_color(vec3(0.4,0.4,0.6)),
	 _tower_color(vec3(0.6,0.5,0.5)),
	 _tower_roof_color(vec3(0.5,0.5,0.9))
{
	this->build_base();
}

void Church::place_building_only(const churchInfo& building, float h)
{
	auto b = building;
	b.center.y += h;

	this->add_church(b);
}

churchInfo Church::generate_building(const genParam& g)
{
	return {
		.center = vec3(g.position.x, 0.0f, g.position.y),
		.dirN2D = g.dirN,
		.size   = vec2(g.maxsize, g.maxsize*utils::random_float(0.6f, 1.2f))
	};
}

planarPlace Church::get_place(const churchInfo& g)
{
	return {
		.center = geom::projplan(g.center),
		.size   = vec2(g.size.x),
		.dirN   = g.dirN2D
	};
}


void Church::add_church(const churchInfo& church)
{
	this->get_array_buffer<POS_SIZEY>().add_value(vec4(church.center, church.size.y));
	this->get_array_buffer<DIR2D_SIZEXZ>().add_value(vec4(church.dirN2D.x, church.dirN2D.y, church.size.x, church.size.x));

	vec3  dirN3Dz = vec3(church.dirN2D.x, 0, church.dirN2D.y);
	_vshadower->add_square(church.center, dirN3Dz, vec2(church.size.x, church.size.x*_xz) + vec2(church.size.x)*0.2f);

	_door->add_door(arcDoor{
		church.center + dirN3Dz*church.size.x*_xz/2.0f*_size,
		vec2(dirN3Dz.x, dirN3Dz.z),
		vec3(
			0.4*church.size.x,
			0.8*church.size.y,
			0.1*church.size.x
		),
		_base_color
	});

	this->add_hitboxes(church);
}

void Church::add_hitboxes(const churchInfo& church)
{
	vec3  dirN3Dz = vec3(church.dirN2D.x, 0, church.dirN2D.y);
	vec3  dirN3Dx = glm::cross(vvv::y(), dirN3Dz);
	vec3  bottom  = church.center - (dirN3Dz*church.size.x*_xz + dirN3Dx*church.size.x)*_size/2.0f;

	vec3 sidedX = dirN3Dx*church.size.x;
	vec3 sidedZ = dirN3Dz*church.size.x;

	auto hbx = std::make_unique<HitboxFusion>();	
	auto hbx_base = std::make_unique<HitboxFusion>();
	auto hbx_roof = std::make_unique<HitboxFusion>();
	auto hbx_towers = std::make_unique<HitboxFusion>();

	hbx_base->fusion_hitbox<PaverHitbox>(paverInfo{
		.bottom  = bottom,
		.sized_x = sidedX*_size,
		.sized_y = vvv::y()*(1.0f+_fulldist)*church.size.y,
		.sized_z = sidedZ*_size*_xz
	});

	vec3 tocorners = sidedX + sidedZ;

	std::vector<vec3> piler_centers = building::square_base::pos(
		orientedPlan{
			church.center,
			dirN3Dx,
			dirN3Dz
		},
		vec2(church.size.x*(1.0f - _piler_size), church.size.x*(_xz - _piler_size))
	);

	for(const auto& center : piler_centers)
	{
		hbx_base->fusion_hitbox<PaverHitbox>(paverInfo{
			.bottom  = center - tocorners*_piler_size/2.0f,
			.sized_x = sidedX*_piler_size,
			.sized_y = vvv::y()*church.size.y,
			.sized_z = sidedZ*_piler_size,
		});
	}

	vec3 dbaseroof = vvv::y()*church.size.y - dirN3Dz*church.size.x*_size*_xz/2.0f;

	hbx_roof->fusion_hitbox<BevelHitbox>(bevelShape{
		.triangle_base   = {
				church.center + dbaseroof + vvv::y()*_roof_height*church.size.y,
				church.center + dbaseroof - dirN3Dx*church.size.x*_size/2.0f,
				church.center + dbaseroof + dirN3Dx*church.size.x*_size/2.0f
			},
		.sized_direction = dirN3Dz*church.size.x*_size*_xz
	});

	paverInfo roofl = {
		.bottom  = church.center + dbaseroof - sidedZ*_roof_over*_xz*_lessover_roof_front*2.0f + vvv::y()*(_roof_height+_roof_thickness)*church.size.y,
		.sized_x = (sidedX/2.0f - vvv::y()*_roof_height*church.size.y)*(1.0f + _roof_over*sqrt(2.0f)),
		.sized_y = -vvv::y()*_roof_thickness*church.size.y,
		.sized_z = sidedZ*_xz*(1.0f + _roof_over*_lessover_roof_front*2.0f),
	};

	hbx_roof->fusion_hitbox<PaverHitbox>(roofl);
	hbx_roof->fusion_hitbox<PaverHitbox>(geom::symmetrical(roofl, {church.center, dirN3Dx}));

	for(const auto t : _tower_pos_size)
	{

		for(uint i=0; i<_tower_relief.size()-1; i+=2)
		{
			const auto r  = _tower_relief[i];
			const auto rn = _tower_relief[i+1];
			float radius = t.z * r.first.x;
			vec3 center_bot = church.center + vvv::y()*church.size.y + (dirN3Dx*(t.x - radius/2.0f) + dirN3Dz*(t.y - radius/2.0f))*church.size.x;

			hbx_towers->fusion_hitbox<PaverHitbox>(paverInfo{
				.bottom  = center_bot  + vvv::y()*r.first.y*church.size.y,
				.sized_x = sidedX*radius,
				.sized_y = vvv::y()*(rn.first.y - r.first.y)*church.size.y,
				.sized_z = sidedZ*radius,
			});
		}

		vec3 dectower = (dirN3Dx*t.x + dirN3Dz*t.y)*church.size.x;	

		hbx_towers->fusion_hitbox<SquarePyramidHitbox>(alignedSquarePyramidShape{
			.base_center  = church.center + dectower + vvv::y()*(1.0f + _towers_heighs)*church.size.y,
			.dir_base_XN  = dirN3Dx,
			.dir_base_YN  = dirN3Dz,
			.base_size_XY = vec2(t.z*_toweroof_over*church.size.x),
			.top          = church.center + dectower + vvv::y()*(1.0f+_towers_heighs + _toweroof_height)*church.size.y
		});
	}

	hbx->fusion_hitbox(std::move(hbx_base));
	hbx->fusion_hitbox(std::move(hbx_roof));
	hbx->fusion_hitbox(std::move(hbx_towers));

	_hitboxes.push_back(std::move(hbx));
	_hbx_context->add_hitbox(_hitboxes[_hitboxes.size()-1].get());
}


/*       
   _piler_size
	  <--->
	 15----14             11----10
	  |     19------------18    |
	 12-23-13     ^        8-22-9
	    |         |          |
	    |         |          |
	    |         |          |
	    |         |          |
	    |         | _size    |
	    |         |          |
	    |         |          |
	    |         |          |
	    |         |          |
	  3-20-2      V        7-21-6
	  |     16------------17    |
	  0----1               4----5

	convex ordered:

	 15----14             11---10
	  |     13-----------12     |
	 16-17        ^          8--9
	    |         |          |
	    |         |          |
	    |         |          |
	    |         |          |
	    |         | _size    |
	    |         |          |
	    |         |          |
	    |         |          |
	    |         |          |
	19--18        V          7--6
	 |      2------------3     |
	 0-----1               4----5
	        
*/

void Church::build_base()
{
	auto roof = this->build_facade();
	this->build_roof(roof);
	this->build_towers();
}


Church::roofBuiling Church::build_facade()
{
	auto& relpos   = this->get_array_buffer<RELATIVE_POS>();
	auto& colors   = this->get_array_buffer<COLOR>();
	auto& elements = this->get_elements();

	roofBuiling roof={
		{},
		PlanarSymmetry<glm::vec3>(
			infinitePlan{
				.point   = vec3(0),
				.normalN = vvv::z()
			},
			&relpos,
			&elements,
			&colors
		)
	};

	std::vector<vec3> piler_centers = building::square_base::pos(
		orientedPlan{
			vec3(0),
			vvv::x(),
			vvv::z()
		},
		vec2(1.0f - _piler_size, (_xz - _piler_size))
	);

	for(const auto& p : piler_centers)
	{
		building::square_base::v(
			relpos,
			orientedPlan{
				p,
				vvv::x(),
				vvv::z()
			},
			vec2(_piler_size, _piler_size)
		);
	}

	building::square_base::v(
		relpos,
		orientedPlan{
			vec3(0),
			vvv::x(),
			vvv::z()
		},
		vec2(1.0f - 2.0f*_piler_size, _size*_xz)
	);
	building::square_base::v(
		relpos,
		orientedPlan{
			vec3(0),
			vvv::x(),
			vvv::z()
		},
		vec2(_size, (_xz - 2.0f*_piler_size))
	);

	uint base = relpos.size();
	building::extruding::convex::sided(
		relpos,
		elements,
		{0,1,16,17,4,5,6,21,22,9,10,11,18,19,14,15,12,23,20,3},
		vvv::y()
	);
	
	valued_fill(relpos.size(), arrayBufferArg<vec3>{colors, _base_color});

	for(uint i=0; i<base; i++)
	{
		colors.change_subvalue(i, colors[i]*0.6f);
	}

	uint toproof  = roof.sym.place_symmetrical(
		(relpos[base+1]+relpos[base+4])/2.0f
		+ vvv::y()*_roof_height

		, _roof_color*0.6f
	);

	// Completing the triangle under roof
	uint tritop = roof.sym.place_symmetrical((relpos[base+2] + relpos[base+3])/2.0f + vvv::y()*_roof_height, _base_color*0.2f);
	elements.add_triangle(base+12, base+13, roof.sym.symmetrical(tritop), true);
	elements.add_triangle(base+2 , base+3 , tritop, true);
	
	// Adjusting sides to up them to the roof
	vec3 leftroofdirN = relpos[base+0] - relpos[toproof];
	float defaultrooflength = glm::length(leftroofdirN);
	leftroofdirN = glm::normalize(leftroofdirN);
	vec3 rightroofdirN = glm::normalize(relpos[base+5] - relpos[toproof]);

	relpos.change_subvalue(toproof, relpos[toproof] - vvv::z()*_roof_over*_xz*_lessover_roof_front);
	relpos.change_subvalue(roof.sym.symmetrical(toproof), relpos[roof.sym.symmetrical(toproof)] + vvv::z()*_roof_over*_xz*_lessover_roof_front);

	uint leftroof = roof.sym.place_symmetrical(relpos[toproof] + leftroofdirN*(defaultrooflength + _roof_over), _roof_color*0.6f);
	uint rightroof = roof.sym.place_symmetrical(relpos[toproof] + rightroofdirN*(defaultrooflength + _roof_over), _roof_color*0.6f);

	roof.roof_indices.push_back(leftroof);
	roof.roof_indices.push_back(rightroof);
	roof.roof_indices.push_back(toproof);

	_fulldist    = _roof_height*(1.0f-_size)/_size;
	_fulldist_in = _roof_height*_piler_size*2.0f;

	relpos.change_subvalue(base+7 , relpos[base+7 ] + vvv::y()*_fulldist);
	relpos.change_subvalue(base+8 , relpos[base+8 ] + vvv::y()*_fulldist);
	relpos.change_subvalue(base+17, relpos[base+17] + vvv::y()*_fulldist);
	relpos.change_subvalue(base+18, relpos[base+18] + vvv::y()*_fulldist);

	relpos.change_subvalue(base+2 , relpos[base+2 ] + vvv::y()*_fulldist_in);
	relpos.change_subvalue(base+3 , relpos[base+3 ] + vvv::y()*_fulldist_in);
	relpos.change_subvalue(base+12, relpos[base+12] + vvv::y()*_fulldist_in);
	relpos.change_subvalue(base+13, relpos[base+13] + vvv::y()*_fulldist_in);
	relpos.change_subvalue(base+1 , relpos[base+1 ] + vvv::y()*_fulldist_in);
	relpos.change_subvalue(base+4 , relpos[base+4 ] + vvv::y()*_fulldist_in);
	relpos.change_subvalue(base+11, relpos[base+11] + vvv::y()*_fulldist_in);
	relpos.change_subvalue(base+14, relpos[base+14] + vvv::y()*_fulldist_in);

	return roof;
}


void Church::build_roof(roofBuiling& roof)
{
	auto& relpos   = this->get_array_buffer<RELATIVE_POS>();
	auto& colors   = this->get_array_buffer<COLOR>();
	auto& elements = this->get_elements();

	uint startup = relpos.size();

	building::extruding::convex::sided(
		relpos,
		elements,
		{
			roof.roof_indices[0],
			roof.roof_indices[2],
			roof.roof_indices[1],
			roof.sym.symmetrical(roof.roof_indices[1]),
			roof.sym.symmetrical(roof.roof_indices[2]),
			roof.sym.symmetrical(roof.roof_indices[0])
		},
		vvv::y()*_roof_thickness
	);

	elements.add_rectangle(roof.roof_indices[0],
						   roof.roof_indices[2],
						   roof.sym.symmetrical(roof.roof_indices[2]),
						   roof.sym.symmetrical(roof.roof_indices[0]));
	elements.add_rectangle(roof.roof_indices[2],
						   roof.roof_indices[1],
						   roof.sym.symmetrical(roof.roof_indices[1]),
						   roof.sym.symmetrical(roof.roof_indices[2]));
	elements.add_rectangle(startup + 0,
						   startup + 1,
						   startup + 4,
						   startup + 5, true);
	elements.add_rectangle(startup + 1,
						   startup + 2,
						   startup + 3,
						   startup + 4, true);

	while(colors.size() < relpos.size())
	{
		colors.add_value(_roof_color);
	}
}

void Church::build_towers()
{
	using stageInfo = building::relief::stageInfo<vec3>;

	auto& relpos   = this->get_array_buffer<RELATIVE_POS>();
	auto& colors   = this->get_array_buffer<COLOR>();
	auto& elements = this->get_elements();

	float h = 0;

	_tower_relief = {
		{vec2(0.9 , h += 0.0), _tower_color*0.5f},
		{vec2(0.9 , h += 1.8), _tower_color*0.9f},
		{vec2(1.0 , h += 0.0), _tower_color},
		{vec2(1.0 , h += 0.2), _tower_color},
		{vec2(0.9 , h += 0.0), _tower_color*0.7f},
		{vec2(0.9 , h += 0.4), _tower_color*0.9f},
		{vec2(1.0 , h += 0.0), _tower_color},
		{vec2(1.0 , h += 0.1), _tower_color},
		{vec2(1.1 , h += 0.0), _tower_color},
		{vec2(1.1 , h += 0.6), _tower_color},
		{vec2(1.0 , h += 0.0), _tower_color},
	};
	_towers_heighs = h;


	uint bell_stage = _tower_relief.size() - 2;

	vec3 alignN = glm::normalize(vvv::z()+vvv::x());

	for(const auto& p : _tower_pos_size)
	{
		AngularSymmetry<vec3> sym(
			cylindricDivisor{
				vec3(p.x, 1, p.y),
				vvv::y(),
				4
			},
			&relpos,
			&elements, 
			&colors
		);

		float sqrt2 = sqrt(2.0f);

		std::vector<stageInfo> stages;
		for(const auto& r : _tower_relief)
		{
			stages.push_back({
				.relief = vec2(r.first.x*p.z/sqrt2, r.first.y),
				.values = {r.second}
			});

		}



		building::relief::sided(sym, alignN, stages);

		building::cone::full_ext(
			relpos,
			elements,
			vec3(p.x, 1+h, p.y),
			vvv::y()*_toweroof_height,
			alignN,
			_toweroof_over*p.z/sqrt2,
			4
		);

		while(colors.size() < relpos.size())
		{
			colors.add_value(_tower_roof_color);
		}

		float bh  = (_tower_relief[bell_stage].first.y + _tower_relief[bell_stage-1].first.y)/2.0f; 
		float bhw = (_tower_relief[bell_stage].first.y - _tower_relief[bell_stage-1].first.y); 
		float bxw = _tower_relief[bell_stage].first.x*p.z; 

		this->place_probells(
			vec3(p.x, bh+1.0f, p.y),
			_tower_relief[bell_stage].first.x*p.z/sqrt(2),
			vec3(bxw*0.9f, bhw*0.8f, 0.02)		
		);
	}
}

void Church::place_probells(const glm::vec3& center,
									 float thick,
									 const glm::vec3& size)
{
	for(uint i=0; i<4; i++)
	{
		vec3 dirN = glm::normalize(geom::circle_point(
			vec3(0),
			vvv::x(),
			vvv::y(),
			float(i)*float(M_PI)/2.0f,
			1.0f
		));

		this->place_probell(
			center + dirN*thick/sqrt(2.0f),
			dirN,
			size
		);
	}
}

void Church::place_probell(const glm::vec3& center,
						 			const glm::vec3& face_dirN,
						 			const glm::vec3& size)
{
	auto& relpos   = this->get_array_buffer<RELATIVE_POS>();
	auto& colors   = this->get_array_buffer<COLOR>();
	auto& elements = this->get_elements();

	vec3 dirxN = glm::cross(vvv::y(), face_dirN);

	vec3 plan_size = dirxN*size.x	- vvv::y()*size.y;

	// building::paver::topsided(relpos, elements, paverInfo{
	// 	.bottom  = center - plan_size/2.0f,
	// 	.sized_x = dirxN*size.x,
	// 	.sized_y = face_dirN*size.z,
	// 	.sized_z = -vvv::y()*size.y
	// });

	orientedPlan p = {
		.point = center,
		.dirxN = dirxN,
		.diryN = vvv::y()
	};

	building::crown::squared_topsided(
		relpos,
		elements,
		p,
		vec2(size.x, size.y),
		size.z,
		size.y*0.2
	);

	while(colors.size() < relpos.size())
	{
		colors.add_value(vec3(0.2));
	}
}