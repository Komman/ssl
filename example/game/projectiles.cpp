#include "projectiles.hpp"

static std::unique_ptr<ArrowPackMerger> _arrows = NULL;

namespace projectiles
{
	void init(SharedRessources& shared_ressources)
	{	
		#ifdef SSL_DEBUG
		if(_arrows != NULL)
		{
			ssl::err("call to projectiles::init() while it has already been init");
		}
		#endif

		SimpleArrowTrailPack::init_trail_last_time();
		_arrows = std::make_unique<ArrowPackMerger>();
	}
	void free()
	{	
		#ifdef SSL_DEBUG
		if(_arrows == NULL)
		{
			ssl::err("call to projectiles::free() while it has already been freed");
		}
		#endif

		_arrows.reset();
	}

	ArrowPackMerger& arrows()
	{
		#ifdef SSL_DEBUG
		if(_arrows == NULL)
		{
			ssl::err("call to projectiles::arrows() while projectiles::init() has not been called");
		}
		#endif

		return *_arrows;
	}
};