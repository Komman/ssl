#ifndef _CASTLE_GRAPH_BUILDOR_HPP_
#define _CASTLE_GRAPH_BUILDOR_HPP_

#include "../utils/plan_graph.hpp"
#include "../motor/thick_graph.hpp"
#include "../motor/dual_multi_graph.hpp"

#include "castle_positioner.hpp"
#include "roomed_graph.hpp"
#include "castle_role_graph.hpp"
#include "castle_theorical_architecture.hpp"

namespace CASTLE
{
	struct graphParameters
	{
		castlePhysicalContext context;
		glm::vec3 position;
	};

	struct theoricalParameters
	{
		float safe_distance;

		graphHeightParam heights;
	};	

	class GraphBuildor : public CastlePositioner
	{
	public:
		GraphBuildor(const graphParameters& params);

		/*
			The TheoricalArchitecture ensures that every pair
			edge-edge or vertex-edge or vertex-vertex
			are at least separated of "safe_distance". 
		*/
		TheoricalArchitecture compute_theorical_architecture(const theoricalParameters& parameters);

	private:
		/*
			The TheoricalArchitecture algorithm does the following steps:
			1. place bordered points	
			2. build a thick planar graph
			3. force it to be a thick roomed graph
			4. build its dual graph
			5. choose the role graph
			6. design the relief labyrinth graph
			7. arrange the theocture graph  
		*/


		BorderedGraph create_points();
		ThickGraph build_thick_planar_graph(const BorderedGraph& points, float safe_distance);
		RoomedGraph compute_roomed_graph(float safe_distance);
		DualMultiGraph build_dual(const BorderedGraph& graph);

		void check_border_safe(const BorderedGraph& points, float safe_distance);

	private:
		graphParameters _params;

	};	
};

#endif //_CASTLE_GRAPH_BUILDOR_HPP_
