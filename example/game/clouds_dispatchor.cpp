#include "clouds_dispatchor.hpp"

#include "../motor/premade_buffers.hpp"

CloudsDispatchor::CloudsDispatchor(ShaderGenerator& generator)
	: ParticleDispatchor(),
	  _drawer(generator, "coulds_effects_vertex", "coulds_effects_fragment"),
	  _vao({{&(premade_buffers::orthonormal_cube().get_points()), 0},
	  		{&(this->get_dep_LBAC()), 1},
	  		{&(this->get_colors()), 1},
	  		{&(this->get_infos()), 1}},
	  	&(premade_buffers::orthonormal_cube().get_elements()))
{
	_drawer.depth(true);
}

void CloudsDispatchor::draw(GameDrawer& game_drawer, bool use_vao)
{
	if(use_vao)
	{
		game_drawer.draw_bloomed(_drawer, _vao);
	}
	else
	{
		game_drawer.draw_bloomed(_drawer,  premade_buffers::orthonormal_cube().get_elements(), premade_buffers::orthonormal_cube().get_points(), this->get_dep_LBAC(), this->get_colors(), this->get_infos());	
	}
}

const VertexArrayObject& CloudsDispatchor::get_vao() const
{
	return _vao;
}


PackedCloudsDispatchor::PackedCloudsDispatchor(ShaderGenerator& generator, const std::string& name)
	: PackedParticleDispatchor(name),
	  _drawer(generator, "packed_clouds_effects_vertex", "coulds_effects_fragment"),
	  _vao({{&(premade_buffers::orthonormal_cube().get_points()), 0},
	  		{&(this->get_dep_LBAC()), 1},
	  		{&(this->get_colors()), 1},
	  		{&(this->get_start_speeds()), 1},
	  		{&(this->get_start_accelerations()), 1},
	  		{&(this->get_infos()), 1}},
	  	&(premade_buffers::orthonormal_cube().get_elements()))
{
	_drawer.depth(true);
}

void PackedCloudsDispatchor::draw(GameDrawer& game_drawer, bool use_vao)
{
	if(use_vao)
	{
		game_drawer.draw_bloomed(_drawer, _vao);
	}
	else
	{
		game_drawer.draw_bloomed(_drawer,  premade_buffers::orthonormal_cube().get_elements(), premade_buffers::orthonormal_cube().get_points(), this->get_dep_LBAC(), this->get_colors(),this->get_start_speeds(),this->get_start_accelerations(),this->get_infos());	
	}
}

const VertexArrayObject& PackedCloudsDispatchor::get_vao() const
{
	return _vao;
}
