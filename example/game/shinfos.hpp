#ifndef _SHINFOS_HPP_
#define _SHINFOS_HPP_

#include "absolute_objects.hpp"

namespace shinfos
{
	void init(AbsoluteObjects& objs);
	void free();

	Archer* player();
};

#endif //_SHINFOS_HPP_
