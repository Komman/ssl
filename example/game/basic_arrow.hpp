#ifndef _BASIC_ARROW_HPP_
#define _BASIC_ARROW_HPP_

#include <glm/glm.hpp>
#include "../../ssl/ssl.hpp"
#include "../motor/gutils.hpp"
#include "../motor/physical_point.hpp"
#include "../motor/basic_hitbox.hpp"

#include "physical_context.hpp"
#include "basic_ground.hpp"

using namespace ssl;

class BasicArrow : public BasicPhysicalPoint
{
public:
	BasicArrow(const glm::vec3& position,
			   const glm::vec3& speed,
			   const glm::vec3& acceleration);
	virtual ~BasicArrow() {};

	virtual float get_length() const =0;
	virtual float get_head_planted_length() const =0;
	virtual const BasicHitbox& get_hitbox() const =0;

	virtual void animate(const PhysicalContext& context, float dt);
	virtual void launch(const glm::vec3& position,
						const glm::vec3& start_speed);

	virtual bool  to_destroy();

	void move(const glm::vec3& dposition) override;
	void tp(const glm::vec3& position) override;

	void unplant();
	bool is_planted() const;

	bool has_moved_since_last_animation() const;
	bool position_difference(const BasicArrow& arrow);

	GETTER_BUILDER(glm::vec3, position)
	GETTER_BUILDER(glm::vec3, directionN)
	GETTER_BUILDER(glm::vec3, speed)
	GETTER_BUILDER(glm::vec3, acceleration)

	void set_speed(const glm::vec3& speed);
	void set_position(const glm::vec3& position);
	void set_acceleration(const glm::vec3& acceleration);
	void set_directionN(const glm::vec3& directionN);

protected:
	virtual void planting_reaction(const glm::vec3& position,
								   const glm::vec3& normal,
								   const glm::vec3& impact_speed);

	virtual void launching_reaction(const glm::vec3& position,
								    const glm::vec3& speed);
	virtual void update_hitbox() =0;


private:
	void animate(float dt) override;
	void plant(const glm::vec3& position,
			   const glm::vec3& normal,
			   const glm::vec3& impact_speed);

private:
	glm::vec3 _position;
	glm::vec3 _directionN;
	glm::vec3 _speed;
	glm::vec3 _acceleration;

	bool _flying;
	bool _has_moved; // Since last animation

};

#endif //_BASIC_ARROW_HPP_
