#include "physical_context.hpp"
#include "../motor/motor_debug.hpp"
#include "../utils/geom.hpp"

using namespace std;
using namespace glm;

PhysicalContext::PhysicalContext(const HitboxContext* icontext)
	: _context(icontext)
{
	if(icontext == NULL)
	{
		mot::err("PhysicalContext::PhysicalContext(): no context");
	}
}

const HitboxContext& PhysicalContext::context() const
{
	return *_context;
}


