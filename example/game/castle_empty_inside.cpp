#include "castle_empty_inside.hpp"

#include "../motor/meshbuild.hpp"

#include "castle_base.hpp"
#include "castle_colors.hpp"
#include "archer.hpp"

using namespace glm;
using namespace std;

namespace CASTLE
{
	EmptyInside::EmptyInside(const std::vector<insideDoor>& doors_to_set, FaceStructure* outside, float height)
		: InsideFace(doors_to_set, outside),
		  _height(height)
	{

	}

	void EmptyInside::build(const std::vector<insideDoor>& doors_to_set)
	{
		walledInside indices = this->build_walls(doors_to_set, _height);
		meshbuild::fill_cycle(this->mesh(), this->elements(), indices.top, false);
	
		float elsize = 3.0f;
		float jumph  = Archer::MIN_JUMP_HEIGHT;

		for(uint i=0; i<doors_to_set.size(); i++)
		{
			if(doors_to_set[i].door && doors_to_set[i].pos.altitude > this->floor_h() + jumph)
			{
				doorFullInfo d = this->compute_infos(doors_to_set[i]);

				#warning this->wall_radius()*0.7f: bissectrice delargemeng make the wall thinner than this->wall_radius()
				
				this->get_base()->build_structures().elevators->add_elevator({
						.center = geom::middle(d.edge.first, d.edge.second) + d.dirdoorN*(elsize/2.0f+this->wall_radius()*0.7f),
						.size   = vec3(elsize,100,elsize),
						.dirN   = geom::projplan(d.dirdoorN)
					},
					glm::vec2(this->floor_h()+jumph*0.1f, d.door.altitude-jumph*0.1f)
				);
			}
		}
	}
};
