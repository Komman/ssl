#include "arc_hitbox.hpp"

#include "../motor/building.hpp"
#include "../utils/geom.hpp"

using namespace std;
using namespace glm;

ArcHitbox::ArcHitbox(const arcInfo& shape)
	: ShapedHitboxFusion(shape)
{
	this->build();
}

void ArcHitbox::build()
{
	const arcInfo& arc = this->get_shape();

	std::vector<glm::vec3> points = building::arc::thicked_vertices(
		arc.bottom,
		glm::normalize(arc.sized_x),
		glm::normalize(arc.sized_y),
		arc.sized_z,
		arc.thickness,
		arc.angle,
		arc.points_amount,
		vec2(glm::length(arc.sized_x), glm::length(arc.sized_y))
	);

	uint points_amount = arc.points_amount;
	uint topdi = 2*(points_amount-1);

	for(uint i=0; i<(uint)points_amount-1; i++)
	{
		uint cur  = 2*i;
		uint next = std::min(2*(i+1), topdi);

		this->fusion_hitbox<PaverHitbox>(transPaverInfo{
			.square_base = {	
				points[cur],
				points[next],   
				points[2*(points_amount-1)+1 + next],
				points[2*(points_amount-1)+1 + cur]
			},
			.sized_direction = arc.sized_z
		});

		cur  = 2*i+1;
		next = std::min(2*(i+1)+1, topdi);
		this->fusion_hitbox<PaverHitbox>(transPaverInfo{
			.square_base = {	
				points[cur],
				points[next],   
				points[2*(points_amount-1)+1 + next],
				points[2*(points_amount-1)+1 + cur]
			},
			.sized_direction = arc.sized_z
		});
	}
}
