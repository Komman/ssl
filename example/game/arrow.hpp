#ifndef _ARROW_HPP_
#define _ARROW_HPP_

#include "basic_arrow.hpp"

struct arrowStart
{
	glm::vec3 position;
	glm::vec3 directionN;
	float length;
	float size;
};

class Arrow : public BasicArrow
{
public:
	Arrow(const arrowStart& arrow);
	
	virtual ~Arrow() {}

	virtual float get_head_planted_length() const override;
	virtual float get_length() const override;

	GETTER_BUILDER_COPY(float, size)
	GETTER_BUILDER_COPY(float, gravity)

	SETTER_BUILDER(float, gravity)

	glm::vec3 get_back_position() const;

private:
	float _length;
	float _size;

	float _gravity;
};

#endif //_ARROW_HPP_
