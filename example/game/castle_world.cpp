#include "castle_world.hpp"

#include "../../ssl/src/color.hpp"

#include "../motor/debug_draw.hpp"
#include "../motor/reliefed_plan_dual_graph.hpp"
#include "../motor/wall_hitbox.hpp"

#include "ingame.hpp"
#include "shareders.hpp"
#include "../motor/building.hpp"

using namespace std;
using namespace glm;

static PlanGraph g;

static float groundsize = 1000.0f;

static VertexArrayObject* vao_in  = NULL;
static VertexArrayObject* vao_out = NULL;

CastleWorld::CastleWorld(AbsoluteObjects& objs, SharedRessources& shared_ressources)
	: _shared_ressources(shared_ressources),
	  _game_drawer(_shared_ressources.tools().game_drawer),
	  _gates(),
	  _far_colors({}, STATIC_DRAW),
	  _ground(vec3(0, 0, 0), groundsize, 40, 10.0, 4, 100.0, 60.0, 400.0),
	  _vshadower(uvec2(_ground.size()), _ground.size()/2.0f),
	  _sligthor("lightor", vec2(-_ground.size()/2.0f), vec2(_ground.size()/2.0f), {64,64}, 100, 10000),
	  _hbx_context(staticHitboxDisc{spaceBox{
		  		.min = -vec3(groundsize/2.0f),
		  		.max = +vec3(groundsize/2.0f)
		  	},
		  	100
	  }),
	  _shadows("isoshadow", uvec2(1024, 2048), vec3(1, 2, 3)*500.0f + vvv::y()*50.0f, -vec3(1, 2, 3)*50.0f + vvv::y()*200.0f, vec2(400,500)),
	  _nigth(shared_ressources, this->drawer_controller()),
	  _castle({&_ground, &_sligthor, &_vshadower, &_hbx_context, &_nigth, objs.archer}, vec3(0,30,0)),
	  _catsle_building(false),
	  _towers_toshadow(true),
	  _walls_toshadow(true),
	  _ground_gen("shaders/ground.glsl"),
	  _tree_pencil(_ground_gen, "tree_vertex", "tree_fragment", {"all_lights_smooth"}, this->drawer_controller()),
	  _ground_pencil(_ground_gen, "ground_vertex", "ground_fragment", this->drawer_controller())
{
	_tree_pencil.depth(true);
	_ground_pencil.depth(true);

	_vshadower.clear_shadow();

	_castle.build();
	_hbx_context.add_static_hitboxes(_ground.get_hitboxes());

	build_forest(_trees, _ground);
	this->project_static_shadows(_vshadower);

	_shadows.clear();
	_shadows.add_mesh(*((ArrayBuffer<vec3>*)_castle.real_base().get_mesh().get_buffers()[0]), _castle.real_base().get_mesh().get_elements(), IsometricShadower::PRIMITIVE_ID, 1);
	_shadows.add_mesh(*((ArrayBuffer<vec3>*)_castle.real_base().get_inside_mesh().get_buffers()[0]), _castle.real_base().get_inside_mesh().get_elements(), IsometricShadower::PRIMITIVE_ID, 1);
	_shadows.add_instanced_planar_mesh(_castle.fat_towers().get_elements(), *((ArrayBuffer<vec3>*)_castle.fat_towers().get_buffers()[0]), *((ArrayBuffer<vec4>*)_castle.fat_towers().get_buffers()[2]), *((ArrayBuffer<vec4>*)_castle.fat_towers().get_buffers()[3]), IsometricShadower::CONSTANT, 16);
	_shadows.add_instanced_planar_mesh(_castle.supports().get_elements(), *((ArrayBuffer<vec3>*)_castle.supports().get_buffers()[0]), *((ArrayBuffer<vec4>*)_castle.supports().get_buffers()[2]), *((ArrayBuffer<vec4>*)_castle.supports().get_buffers()[3]), IsometricShadower::CONSTANT, 16);
	_shadows.add_instanced_planar_mesh(_castle.churchs().get_elements(), *((ArrayBuffer<vec3>*)_castle.churchs().get_buffers()[0]), *((ArrayBuffer<vec4>*)_castle.churchs().get_buffers()[2]), *((ArrayBuffer<vec4>*)_castle.churchs().get_buffers()[3]), IsometricShadower::CONSTANT, 16);
	_shadows.add_instanced_planar_mesh(_castle.squares_towers().get_elements(), *((ArrayBuffer<vec3>*)_castle.squares_towers().get_buffers()[0]), *((ArrayBuffer<vec4>*)_castle.squares_towers().get_buffers()[2]), *((ArrayBuffer<vec4>*)_castle.squares_towers().get_buffers()[3]), IsometricShadower::CONSTANT, 16);
	_shadows.add_instanced_O3L1_mesh(_castle.simple_walls().get_elements(), *((ArrayBuffer<vec3>*)_castle.simple_walls().get_buffers()[0]), *((ArrayBuffer<vec4>*)_castle.simple_walls().get_buffers()[2]), *((ArrayBuffer<vec4>*)_castle.simple_walls().get_buffers()[3]), IsometricShadower::CONSTANT, 16);
	_shadows.add_instanced_tower_mesh(_castle. magic_towers().get_elements(), *((ArrayBuffer<vec3>*)_castle. magic_towers().get_buffers()[0]), *((ArrayBuffer<vec4>*)_castle. magic_towers().get_buffers()[2]), *((ArrayBuffer<float>*)_castle. magic_towers().get_buffers()[3]), IsometricShadower::CONSTANT, 16);
	_shadows.add_instanced_planar_mesh(_castle.arc_doors().get_elements(), *((ArrayBuffer<vec3>*)_castle.arc_doors().get_buffers()[0]), *((ArrayBuffer<vec4>*)_castle.arc_doors().get_buffers()[2]), *((ArrayBuffer<vec4>*)_castle.arc_doors().get_buffers()[3]), IsometricShadower::CONSTANT, 16);
	_shadows.add_instanced_planar_mesh(_castle.arc_bridges().get_elements(), *((ArrayBuffer<vec3>*)_castle.arc_bridges().get_buffers()[0]), *((ArrayBuffer<vec4>*)_castle.arc_bridges().get_buffers()[2]), *((ArrayBuffer<vec4>*)_castle.arc_bridges().get_buffers()[3]), IsometricShadower::CONSTANT, 16);
	_shadows.add_instanced_planar_mesh(_castle.face_arcs().get_elements(), *((ArrayBuffer<vec3>*)_castle.face_arcs().get_buffers()[0]), *((ArrayBuffer<vec4>*)_castle.face_arcs().get_buffers()[2]), *((ArrayBuffer<vec4>*)_castle.face_arcs().get_buffers()[3]), IsometricShadower::CONSTANT, 16);
	_shadows.add_instanced_planar_mesh(_castle.counter_forts().get_elements(), *((ArrayBuffer<vec3>*)_castle.counter_forts().get_buffers()[0]), *((ArrayBuffer<vec4>*)_castle.counter_forts().get_buffers()[2]), *((ArrayBuffer<vec4>*)_castle.counter_forts().get_buffers()[3]), IsometricShadower::CONSTANT, 16);
	_shadows.add_instanced_O3tower_mesh(_castle.simple_towers().get_elements(), *((ArrayBuffer<vec3>*)_castle.simple_towers().get_buffers()[0]), *((ArrayBuffer<vec4>*)_castle.simple_towers().get_buffers()[2]), *((ArrayBuffer<vec4>*)_castle.simple_towers().get_buffers()[3]), IsometricShadower::CONSTANT, 16);
	_shadows.compute_triangle_antialiasing();

	_sligthor.print();
	_sligthor.compute_static_ligth_map();

	_hbx_context.add_static_hitboxes(_trees->get_hitboxes());
	_hbx_context.add_hitbox(&(objs.archer->get_hitbox()));

	vao_in  = new VertexArrayObject(_castle.real_base().get_inside_mesh().compute_vao());
	vao_out = new VertexArrayObject(_castle.real_base().get_mesh().compute_vao());
}

void CastleWorld::add_gate(ElementBuffer& mesh_indices, ArrayBuffer<glm::vec3>& mesh)
{
	gateDraw g = {
		&mesh_indices,
		&mesh
	};

	int count = (int)(mesh.size()) - (int)(_far_colors.size());
	for(int i=0; i<count; i++)
	{
		_far_colors.add_value(vec4(0.4f, 0.1f, 0.2f, 1.0f));
	}

	_gates.push_back(g);
	this->require_gate();
}

	
void CastleWorld::project_static_shadows(VerticalShadower& vshadower)
{
	vshadower.add_shadow_elements_instance(_trees->get_buffers()[0], _trees->get_buffers()[2], _trees->get_elements());
	
	vshadower.compute_shadowmap();
}

VerticalShadower& CastleWorld::get_vertical_shadower()
{
	return _vshadower;
}


const BasicGround& CastleWorld::get_ground() const
{
	return _ground;
}

const HitboxContext& CastleWorld::get_hbx_context() const
{
	return _hbx_context;
}


void CastleWorld::build_forest(std::unique_ptr<ProceduralTree>& trees, const Ground& ground)
{
	if(trees != NULL)
	{
		err("CastleWorld::build_forest(): ptr should be null for being initialized");
	}

	vector<vec3> all_offset_positions;

	int dispers = 800;

	for(uint i=0;i<100;i++)
	{
		float x = float(rand() % dispers - dispers/2);
		float z = float(rand() % dispers - dispers/2);

		float y = ground.get_y(x, z)-0.5f;

		if(y<80.0 && ground.castled_map_code(vec3(x,y,z))==0)
		{
			all_offset_positions.push_back(vec3(
				x,
				y,
				z
			));
		}
		else
		{
			i--;
		}
	}

	std::vector<glm::vec4>  final_leave_color= {
		vec4(vec3(1.0,0.8,0.1)/1.2f, 0.4),
		vec4(vec3(1.0,0.5,0.1)/1.6f, 0.4),
		vec4(vec3(1.0,0.5,0.1)/1.6f, 0.4),
		vec4(vec3(1.0,0.5,0.1)/1.6f, 0.4)
	};

	trees = std::make_unique<ProceduralTree>(
		vec3(0.0,0.1,0.0),
		vec3(0.0,1.0,0.0),
		vec3(1.0,0.0,0.0),
		6.0,
		1.2,
		1.0,
		0.4,
		0.4,
		vec4(dark(BROWN), 1.0),
		vec4(dark(BROWN)*2.0f, 1.0),
		7,
		0.25,
		0.15,
		final_leave_color,
		all_offset_positions);
}


void CastleWorld::far_gate_draw(const ElementBuffer& indices,
							    const ArrayBuffer<glm::vec3>& vertices)
{
	_ground_pencil.culling(false);
	_ground_pencil.draw_elements_in_framebuffer(_shared_ressources.framebuffers().frame, indices, vertices, _far_colors);
	_ground_pencil.culling(true);
}
void CastleWorld::close_gate_draw(GLuint mask_value)
{
	auto& drawer_ctrl = this->drawer_controller();

	drawer_ctrl.stencil_testing(true);
	drawer_ctrl.stencil_function(GL_LEQUAL, mask_value);

	_ground_pencil.draw_object(_shared_ressources.framebuffers().frame, (ElementDrawableObject&)_ground);
	_nigth.draw();
	
	drawer_ctrl.stencil_testing(false);
}

#include <glm/gtx/string_cast.hpp>


BasicWorld* CastleWorld::animate(AbsoluteObjects& objs, float dt)
{
	objs.clouds->animate(dt);
	objs.packed_clouds->animate(dt);
	objs.all_arrows->animate(PhysicalContext{&_hbx_context}, dt);
	objs.timed_dynamic_lightor->animate(dt);
	objs.explodiscs->animate(dt);
	objs.explospheres->animate(dt);
	objs.explobibos->animate(dt);
	objs.exploraylights->animate(dt);

	_castle.animate(dt);
	_trees->animate(dt);

	objs.archer->frame_update(dt);
	
	// if(objs.archer->key_pressed(SSL_KEY(TAB)))   {_castle.set_time_build(2.0f);}
	// if(objs.archer->key_pressed(SSL_KEY(ENTER))) {_castle.set_time_build(0.8f);}	
	if(objs.archer->key_pressed('A')) {_catsle_building=true;}
	if(_catsle_building)
	{
		// _castle.dynamic_build(dt);
	}

	_ground.animate(dt);
	_nigth.animate(dt);

	// if(_castle.ready_for_tower_shadow() && _towers_toshadow) {_castle.draw_tower_shadows(_vshadower); _vshadower.compute_shadowmap(); _towers_toshadow=false;}
	// if(_castle.ready_for_wall_shadow()  && _walls_toshadow ) {_castle.draw_wall_shadows(_vshadower);  _vshadower.compute_shadowmap(); _walls_toshadow=false;}

	objs.dynamic_lightor->update_GPU(); 

	return NULL;
}

static bool USE_VAO = true;

void CastleWorld::draw_world(AbsoluteObjects& objs)
{
	_game_drawer.record_start("clean");
	_game_drawer.clear_frames();

	_game_drawer.record_start("arrows");
	objs.all_arrows->draw(_game_drawer);
	
	for(const auto& tr : objs.all_arrows->get_simple_trails().get_trails())
	{
		_game_drawer.draw_MS(effects::simple_trail_drawer() , tr->get_indices(), tr->get_vertices(), tr->get_start_times());
	}

	_game_drawer.record_start("player");
	objs.archer->draw(_game_drawer);


	if(USE_VAO)
	{
		_game_drawer.record_start("particles");
		_game_drawer.draw_bloomed(effects::explodisc_drawer(), objs.explodiscs->get_vao());
		_game_drawer.draw_bloomed(effects::explosphere_drawer(), objs.explospheres->get_vao());
		objs.clouds->draw(_game_drawer, true);
		objs.packed_clouds->draw(_game_drawer, true);
		_game_drawer.draw_additif(effects::explobibo_drawer(), (objs.explobibos->get_vao()));
		_game_drawer.draw_bloomed_only(effects::meshedlight_drawer(), objs.exploraylights->get_vao());

		_game_drawer.record_start("ground");
		_game_drawer.draw_MS(_ground_pencil, (ElementDrawableObject&)_ground);

		_game_drawer.record_start("castle_base");
		// _game_drawer.draw_MS(shareders::castle_base(), (ElementDrawableObject&)_castle.base());
		// _game_drawer.draw_MS(shareders::castle_base(), (ElementDrawableObject&)_castle.real_base().get_mesh());
		// _game_drawer.draw_MS(shareders::castle_inside(), (ElementDrawableObject&)_castle.real_base().get_inside_mesh());
		_game_drawer.draw_MS(shareders::castle_base(),  *vao_out);
		_game_drawer.draw_MS(shareders::castle_inside(), *vao_in);
		
		_game_drawer.record_start("castle");
		_game_drawer.draw_MS(shareders::tower_drawer(), _castle.simple_towers().get_vao());
		_game_drawer.draw_MS(shareders::planar_builds(), _castle.arc_bridges().get_vao());
		_game_drawer.draw_MS(shareders::planar_builds(), _castle.supports().get_vao());
		_game_drawer.draw_MS(shareders::planar_builds(), _castle.fat_towers().get_vao());	
		_game_drawer.draw_MS(shareders::planar_builds(), _castle.squares_towers().get_vao());
		_game_drawer.draw_MS(shareders::planar_builds(), _castle.churchs().get_vao());
		_game_drawer.draw_MS(shareders::planar_builds(), _castle.face_arcs().get_vao());
		_game_drawer.draw_MS(shareders::planar_builds(), _castle.elevators().get_vao());
		_game_drawer.draw_MS(shareders::counterforts(), _castle.counter_forts().get_vao());
		_game_drawer.draw_MS(shareders::arc_doors(), _castle.arc_doors().get_vao());
		_game_drawer.draw_bloomed_MS(shareders::tower_cannons(), _castle.simple_towers().cannons().get_vao());
		_game_drawer.draw_bloomed_MS(shareders::wall_drawer(), _castle.simple_walls().get_vao());
		_game_drawer.draw_bloomed_MS(shareders::mtower_drawer(), _castle.magic_towers().get_vao());
		_game_drawer.draw_bloomed_MS(shareders::lantern_drawer(), _castle.lanterns().get_vao());
		
		_game_drawer.record_start("trees");
		_game_drawer.draw_MS(_tree_pencil  , _trees->get_vao());


		_game_drawer.record_start("night");
		_nigth.draw();
	}
	else
	{
		_game_drawer.record_start("particles");
		_game_drawer.draw_bloomed(effects::explodisc_drawer(), (ElementDrawableObject&)(*objs.explodiscs));
		_game_drawer.draw_bloomed(effects::explosphere_drawer(), (ElementDrawableObject&)(*objs.explospheres));
		objs.clouds->draw(_game_drawer);
		objs.packed_clouds->draw(_game_drawer);
		_game_drawer.draw_additif(effects::explobibo_drawer(), (ElementDrawableObject&)(*(objs.explobibos)));
		_game_drawer.draw_bloomed_only(effects::meshedlight_drawer(), (ElementDrawableObject&)(*(objs.exploraylights)));

		_game_drawer.record_start("ground");
		_game_drawer.draw_MS(_ground_pencil, (ElementDrawableObject&)_ground);

		_game_drawer.record_start("castle_base");
		// _game_drawer.draw_MS(shareders::castle_base(), (ElementDrawableObject&)_castle.base());
		_game_drawer.draw_MS(shareders::castle_base(), (ElementDrawableObject&)_castle.real_base().get_mesh());
		_game_drawer.draw_MS(shareders::castle_inside(), (ElementDrawableObject&)_castle.real_base().get_inside_mesh());

		_game_drawer.record_start("castle");
		_game_drawer.draw_MS(shareders::tower_drawer(), (ElementDrawableObject&)_castle.simple_towers());
		_game_drawer.draw_MS(shareders::planar_builds(), (ElementDrawableObject&)_castle.arc_bridges());
		_game_drawer.draw_MS(shareders::planar_builds(), (ElementDrawableObject&)_castle.supports());
		_game_drawer.draw_MS(shareders::planar_builds(), (ElementDrawableObject&)_castle.fat_towers());
		_game_drawer.draw_MS(shareders::planar_builds(), (ElementDrawableObject&)_castle.squares_towers());
		_game_drawer.draw_MS(shareders::planar_builds(), (ElementDrawableObject&)_castle.churchs());
		_game_drawer.draw_MS(shareders::planar_builds(), (ElementDrawableObject&)_castle.face_arcs());
		_game_drawer.draw_MS(shareders::counterforts(), (ElementDrawableObject&)_castle.counter_forts());
		_game_drawer.draw_MS(shareders::arc_doors(), (ElementDrawableObject&)_castle.arc_doors());
		_game_drawer.draw_bloomed_MS(shareders::tower_cannons(), (ElementDrawableObject&)_castle.simple_towers().cannons());
		_game_drawer.draw_bloomed_MS(shareders::wall_drawer(), (ElementDrawableObject&)_castle.simple_walls());
		_game_drawer.draw_bloomed_MS(shareders::mtower_drawer(), (ElementDrawableObject&)_castle.magic_towers());
		_game_drawer.draw_bloomed_MS(shareders::lantern_drawer(), (ElementDrawableObject&)_castle.lanterns());
		
		_game_drawer.record_start("trees");
		_game_drawer.draw_MS(_tree_pencil  , (ElementDrawableObject&)(*_trees));


		_game_drawer.record_start("night");
		_nigth.draw();
	}

	
	float ygraph = 200;
	if(objs.archer->key_pressed('G'))
	{
		debug_draw::graph(_castle.base_graph(), 2.0f, vvv::y()*(ygraph));
	}
	if(objs.archer->key_pressed('H'))
	{
		for(const auto& h : _castle.base_hitboxes())
		{
			debug_draw::hitbox(*h);
			// debug_draw::space_box(*h);
		}

		for(const auto& h : _castle.real_base().get_hitboxes())
		{
			debug_draw::hitbox(*h);
			// debug_draw::space_box(*h);
		}

		for(const auto& h : _castle.simple_towers().get_hitboxes())
		{
			debug_draw::hitbox(*h);
			// debug_draw::space_box(*h);
		}

		for(const auto& h : _castle.counter_forts().get_hitboxes())
		{
			debug_draw::hitbox(*h);
			// debug_draw::space_box(*h);
		}

		for(const auto& h : _ground.get_hitboxes())
		{
			debug_draw::hitbox(*h);
		}

		for(const auto& h : _castle.elevators().get_hitboxes())
		{
			debug_draw::hitbox(*h);
		}

		for(const auto& h : _trees->get_hitboxes())
		{
			debug_draw::hitbox(*h);
		}
	}
	// debug_draw::hitbox(objs.archer->get_hitbox());

// 		vector<vec3> points = geom::unprojplan({vec2(-27.583948, -34.185131), vec2(-19.101793, -33.692871), vec2(-20.877140, -37.212467), vec2(-22.805595, -32.217285), vec2(-22.079189, -30.988798), vec2(-22.385696, -32.919472), vec2(-26.050785, -42.627426), vec2(-30.860443, -38.398933), }
// ,
// 											   ygraph);
	// vector<vec3> points = {vec3(125.291809, 31.000000, 49.645412), vec3(137.099106, 31.000000, -16.976377), vec3(142.224792, 31.000000, -65.284531), vec3(92.045784, 31.000000, -122.447105), vec3(30.232002, 31.000000, -149.313705), vec3(-21.958761, 31.000000, -162.254242), vec3(-94.579498, 31.000000, -138.414413), vec3(-95.465591, 31.000000, -56.520676), vec3(-126.780457, 31.000000, -22.521805), vec3(-117.697220, 31.000000, 36.360634), vec3(-96.324219, 31.000000, 80.137352), vec3(-38.069950, 31.000000, 85.577240), vec3(-1.896661, 31.000000, 152.222366), vec3(49.212383, 31.000000, 104.762634), vec3(85.602364, 31.000000, 76.484482), };

	// effects::debug_point(points[0], 10, vec4(1,0,0,1));
	// effects::debug_point(points[1], 10, vec4(0,1,0,1));
	// for(uint i=0; i<points.size(); i++)
	// {
	// 	auto p = points[i];
	// 	effects::debug_point(p, 0.2, vec4(1)*float(i)/float(points.size()-1));
	// }
	// effects::debug_point(geom::unprojplan(_castle.base_graph()[1], ygraph), 3, vec4(1,0,0,1));
	// effects::debug_point(geom::unprojplan(_castle.base_graph()[155], ygraph), 3, vec4(0,1,0,1));
	// effects::debug_point(geom::unprojplan(_castle.base_graph()[12], ygraph), 3, vec4(0,0,0,1));
	// effects::debug_point(geom::unprojplan(_castle.base_graph()[143], ygraph), 3, vec4(1,1,1,1));
	// vector<uint> ipoints = {34,23,12,1,155,144,133};
	// for(uint i=0; i<ipoints.size(); i++)
	// {
	// 	effects::debug_point(geom::unprojplan(_castle.base_graph()[ipoints[i]], ygraph), 3, vec4(1)*float(i)/float(ipoints.size()));
	// }

	// DEBUG
	#warning TO DELETE
	debug_draw::draw(_game_drawer);


	_game_drawer.typed_draw(GameDrawer::MS_DRAW);
	_game_drawer.typed_draw(GameDrawer::BLOOM_MS_DRAW);
	_game_drawer.typed_draw(GameDrawer::BLOOM_MS_ONLY_DRAW);
	
	_game_drawer.draw_multisampled();
	
	_game_drawer.typed_draw(GameDrawer::NORMAL_DRAW);

	this->draw_gates();

	_game_drawer.typed_draw(GameDrawer::BLOOM_DRAW);
	_game_drawer.typed_draw(GameDrawer::BLOOM_ONLY_DRAW);
	_game_drawer.typed_draw(GameDrawer::BLENDING_DRAW);
	_game_drawer.typed_draw(GameDrawer::BLOOMED_BLENDING_DRAW);
	
	_game_drawer.apply_bloom();

	_game_drawer.typed_draw(GameDrawer::ADDITIF_DRAW);

	_game_drawer.screen_pass();

	// debug_draw::texture(_shadows.get_coordinates_id_texture());
}

void CastleWorld::draw_gates()
{
	if(this->linked_gates() != _gates.size())
	{
		err("CastleWorld::draw_world(): not the good gate amount");
	}

	auto& gate_op_framebuffer = _shared_ressources.framebuffers().frame_and_bloom;

	for(unsigned int i=0; i<_gates.size(); i++)
	{
		auto& gate  = _gates[i];
		auto  world = this->get_gate_linked_world(i);

		bool far_draw = false;

		if(far_draw)
		{
			world->far_gate_draw(*(gate.mesh_indices), *(gate.mesh));
		}
		else
		{
			GLuint mask_value = (i+1);

			static_gate_identifior->stencil_function(GL_ALWAYS, mask_value);
			static_gate_holer->stencil_function(GL_EQUAL, mask_value);

			static_gate_identifior->draw_elements_in_framebuffer(gate_op_framebuffer, *(gate.mesh_indices), *(gate.mesh));
			static_gate_holer->draw_elements_in_framebuffer(gate_op_framebuffer, *(gate.mesh_indices), *(gate.mesh));
		
			world->close_gate_draw(mask_value);
		}
	}
}

CASTLE::CastleAnimator& CastleWorld::get_castle()
{
	return _castle;
}

