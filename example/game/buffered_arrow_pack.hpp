#ifndef _BUFFERED_ARROW_PACK_HPP_
#define _BUFFERED_ARROW_PACK_HPP_

#include <type_traits>

#include "../motor/stored_instances_buffers.hpp"
#include "../motor/stored_vao_object.hpp"

#include "arrow_pack.hpp"

template<typename ArrowGeometrty, uint FIRST_INSTANCE_BUFFER, typename... Types>
class BufferedArrowPack : public ArrowPack<ArrowGeometrty>
{
public:
	using StoredBuffers = StoredVAObject<StoredInstancesBuffers<FIRST_INSTANCE_BUFFER, Types...>>; 
	using InstanceBuffersIndices = typename StoredInstancesBuffers<FIRST_INSTANCE_BUFFER, Types...>::InstanceBuffersIndices;
	using tupleBrutType = typename StoredDrawableObject<Types...>::tupleBrutType;
	using tupleType = typename StoredDrawableObject<Types...>::tupleType;
	
	template<uint Index>
	using brutType = typename std::tuple_element<Index, tupleBrutType>::type;
	template<uint Index>
	using arrayBufferType = typename std::tuple_element<Index, tupleType>::type;

	template<typename Type>
	struct storedDrawType
	{
		using type = ssl::draw_type;
	};

	template<typename TupleType>
	static auto instances_tuple(const TupleType& tuple)
	{
		return tmplt::subtuple(tuple, InstanceBuffersIndices{});
	};


	using InstancesTuple = decltype(instances_tuple(std::declval<tupleBrutType>())); 

public:
	BufferedArrowPack(ssl::draw_type element_draw_type, typename storedDrawType<Types>::type ... buffers_draw_type);

	const ElementBuffer& element_buffer() const;
	ElementBuffer&       element_buffer();

	template<std::size_t BufferIndex>
	const arrayBufferType<BufferIndex>& array_buffer() const;
	template<std::size_t BufferIndex>
	arrayBufferType<BufferIndex>& array_buffer();

	const StoredBuffers& get_drawable() const;

protected:
	void pre_add_arrow(const Arrow& arrow,
					   const ArrowGeometrty& look) override;
	void post_set_arrow(unsigned int index,
						const Arrow& arrow,
						const ArrowGeometrty& look) override;
	void pre_pop_last_arrow() override;
	void pre_clear() override;

	/*
		Returns a tuple where each element is the final value of the 
		instance in the array buffer.
		Thus, this tuple has a size of (sizeof...(Types) - FIRST_INSTANCE_BUFFER)
		and its types are the types of the instances buffers.
	*/
	virtual InstancesTuple buffer_filling(const Arrow& arrow, const ArrowGeometrty& look) const =0;

private:
	template<std::size_t... Is>
	void pop_back_specifics_buffers(std::index_sequence<Is...>);
	template<std::size_t... Is>
	void clear_specifics_buffers(std::index_sequence<Is...>);
	template<std::size_t... Is>
	void add_empty_values_specifics_buffers(std::index_sequence<Is...>);
	template<std::size_t... Is>
	void fill_specifics_buffers(uint array_buffer_index, const InstancesTuple& tuple_values, std::index_sequence<Is...>);
	template<std::size_t Index>
	void check_and_change(uint array_buffer_index, const brutType<Index>& value);
	template<std::size_t Index>
	static brutType<Index> default_value() {return brutType<Index>();}

private:
	StoredBuffers _buffers;
};



#define BufferedArrowPackMACRO(retType) template<typename ArrowGeometrty, uint FIRST_INSTANCE_BUFFER, typename... Types>\
									    retType BufferedArrowPack<ArrowGeometrty, FIRST_INSTANCE_BUFFER, Types...>
#define BAPMACRO BufferedArrowPack<ArrowGeometrty, FIRST_INSTANCE_BUFFER, Types...>

BufferedArrowPackMACRO()::BufferedArrowPack(ssl::draw_type element_draw_type, typename storedDrawType<Types>::type ... buffers_draw_type)
	: ArrowPack<ArrowGeometrty>(),
	  _buffers(element_draw_type, buffers_draw_type...)
{

}

template<typename ArrowGeometrty, uint FIRST_INSTANCE_BUFFER, typename... Types>
template<std::size_t... Is>
void BufferedArrowPack<ArrowGeometrty, FIRST_INSTANCE_BUFFER, Types...>::pop_back_specifics_buffers(std::index_sequence<Is...>)
{
	(((_buffers.template get_array_buffer<Is>()).pop_value()), ...);
}

template<typename ArrowGeometrty, uint FIRST_INSTANCE_BUFFER, typename... Types>
template<std::size_t... Is>
void BufferedArrowPack<ArrowGeometrty, FIRST_INSTANCE_BUFFER, Types...>::clear_specifics_buffers(std::index_sequence<Is...>)
{
	(((_buffers.template get_array_buffer<Is>()).clear()), ...);
}

template<typename ArrowGeometrty, uint FIRST_INSTANCE_BUFFER, typename... Types>
template<std::size_t... Is>
void BufferedArrowPack<ArrowGeometrty, FIRST_INSTANCE_BUFFER, Types...>::add_empty_values_specifics_buffers(std::index_sequence<Is...>)
{
	(( (_buffers.template get_array_buffer<Is>()).add_value(default_value<Is>()) ), ...);
}

template<typename ArrowGeometrty, uint FIRST_INSTANCE_BUFFER, typename... Types>
template<std::size_t Index>
void BufferedArrowPack<ArrowGeometrty, FIRST_INSTANCE_BUFFER, Types...>::check_and_change(uint array_buffer_index, const brutType<Index>& value)
{
	// static_assert(std::is_same<decltype((_buffers.template get_array_buffer<Index>()).get_subvalue(array_buffer_index)), const brutType<Index>& >::value == true);

	auto& buffer = _buffers.template get_array_buffer<Index>();

	if(buffer[array_buffer_index] != value)
	{
		buffer.change_subvalue(array_buffer_index, value);
	}
}

template<typename ArrowGeometrty, uint FIRST_INSTANCE_BUFFER, typename... Types>
template<std::size_t... Is>
void BufferedArrowPack<ArrowGeometrty, FIRST_INSTANCE_BUFFER, Types...>::fill_specifics_buffers(uint array_buffer_index, const InstancesTuple& tuple, std::index_sequence<Is...>)
{
	static_assert(std::tuple_size_v<InstancesTuple> == sizeof...(Types) - FIRST_INSTANCE_BUFFER, "BufferedArrowPack::fill_specifics_buffers(): wrong instance buffer sizes");

	((this->check_and_change<Is>(array_buffer_index, std::get<Is - FIRST_INSTANCE_BUFFER>(tuple))), ...);
}

template<typename ArrowGeometrty, uint FIRST_INSTANCE_BUFFER, typename... Types>
const typename BAPMACRO::StoredBuffers& BAPMACRO::get_drawable() const
{
	return _buffers;
}

template<typename ArrowGeometrty, uint FIRST_INSTANCE_BUFFER, typename... Types>
ElementBuffer&   BAPMACRO::element_buffer()
{
	return _buffers.get_elements();
}

template<typename ArrowGeometrty, uint FIRST_INSTANCE_BUFFER, typename... Types>
template<std::size_t BufferIndex>
typename BAPMACRO::template arrayBufferType<BufferIndex>& BAPMACRO::array_buffer()
{
	return _buffers.template get_array_buffer<BufferIndex>();
}

template<typename ArrowGeometrty, uint FIRST_INSTANCE_BUFFER, typename... Types>
const ElementBuffer&   BAPMACRO::element_buffer() const
{
	return _buffers.get_elements();
}

template<typename ArrowGeometrty, uint FIRST_INSTANCE_BUFFER, typename... Types>
template<std::size_t BufferIndex>
const typename BAPMACRO::template arrayBufferType<BufferIndex>& BAPMACRO::array_buffer() const
{
	return _buffers.template get_array_buffer<BufferIndex>();
}
	

BufferedArrowPackMACRO(void)::pre_add_arrow(const Arrow& arrow,
					   const ArrowGeometrty& look)
{
	uint index = this->size();
	this->add_empty_values_specifics_buffers(InstanceBuffersIndices{});
	this->post_set_arrow(index, arrow, look);
}

BufferedArrowPackMACRO(void)::post_set_arrow(unsigned int index,
					const Arrow& arrow,
					const ArrowGeometrty& look)
{
	this->fill_specifics_buffers(index, this->buffer_filling(arrow, look), InstanceBuffersIndices{});
}

BufferedArrowPackMACRO(void)::pre_pop_last_arrow()
{
	#ifdef SSL_DEBUG
	if(this->size() == 0)
	{
		err("BufferedArrowPack::pre_pop_last_arrow(): try to pop while no there are no arrows");
	}
	#endif

	this->pop_back_specifics_buffers(InstanceBuffersIndices{});
}

BufferedArrowPackMACRO(void)::pre_clear()
{
	this->clear_specifics_buffers(InstanceBuffersIndices{});
}


#undef BAPMACRO
#undef BufferedArrowPackMACRO

#endif //_BUFFERED_ARROW_PACK_HPP_
