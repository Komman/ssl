#ifndef _CASTLE_CIRCLE_BUMP_HPP_
#define _CASTLE_CIRCLE_BUMP_HPP_

#include "castle_face_structure.hpp"

namespace CASTLE
{
	class CircleBump : public FaceStructure
	{
	public:
		CircleBump(const baseBuiling& base, uint face);

	protected:
		void build_face(const std::vector<doorWall>& walls_to_build) override;
		void build_wall_deco(const std::vector<doorWall>& walls_to_build) override;

	private:
		float _underbluf;
		float _height;
		float _topheight;
		float _pilheight_coeff;
	};
};


#endif //_CASTLE_CIRCLE_BUMP_HPP_
