#ifndef _ABSOLUTE_OBJECTS_HPP_
#define _ABSOLUTE_OBJECTS_HPP_

#include "../motor/dynamic_lightor.hpp"
#include "../motor/stored_vao_object.hpp"

#include "archer.hpp"
#include "arrow_pack_merger.hpp"
#include "clouds_dispatchor.hpp"
#include "holed_disc.hpp"
#include "../motor/timed_cube_instances.hpp"

struct AbsoluteObjects
{
	Archer*                 archer;
	ArrowPackMerger*        all_arrows;
	CloudsDispatchor*       clouds;
	PackedCloudsDispatchor* packed_clouds;
	DynamicLightor*         dynamic_lightor;
	TimedLightsStorer*      timed_dynamic_lightor;
	HoledDisc*              explodiscs;
	ExploSphere*            explospheres;
	ExploBibos*             explobibos;
	MeshedRayLuminor*       exploraylights;
};


#endif //_ABSOLUTE_OBJECTS_HPP_
