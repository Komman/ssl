#ifndef _GROUND_HPP_
#define _GROUND_HPP_

#include "basic_ground.hpp"

#include "../motor/linear_valuator.hpp"
#include "../motor/straight_line.hpp"

using namespace ssl;

class Ground : public BasicGround
{
public:
	Ground(const glm::vec3& position,
		   float size,
		   int cut,
		   float ymax,
		   int mountains_count,
		   float mountains_height,
		   float mountains_height_bomb,
		   float under_depth);

	const glm::vec3& get_center() const;

	 // projected on the ground
	float get_y(float x, float z) const override;
	float get_ymax() const;
	float size() const override;

	void nimportequoi();
	void animate(float dt);

	const std::vector<const BasicBuffer*>& get_buffers() const override;
	const ElementBuffer&                   get_elements() const override;

	// virtual void dark_color(const glm::vec3& position, float coeff);

	int map_size() const;
	// 0: nothing, 1: wall, 2: castle inside
	int castled_map_code(const glm::vec3& position) const;

	void hole_circular(const std::vector<glm::vec2>& points2D, const glm::vec3& center);

	void generate_hitboxes();
	const std::vector<BasicHitbox::storedHitbox>& get_hitboxes() const;

private:
	void       init_colors();
	glm::vec4  get_color_ground(float ycoeff) const;
	glm::uvec2 index_to_coord(uint index) const;
	uint       point_to_index(const glm::vec2&  point) const;
	uint       point_to_index(const glm::vec3&  point) const;
	uint       coord_to_index(const glm::uvec2& coord) const;
	glm::uvec2 point_to_coord(glm::vec2 point) const;
	glm::uvec2 point_to_coord(glm::vec3 point) const;
	glm::vec3  coord_to_point(const glm::uvec2& coord) const;
	/*
		top_or_bot : if top: x+ and y+
		             if bot: x- and y-
	*/
	glm::uvec2 inter_coord(const glm::vec2& inter_point, StraightLine::singleAxis inter_axis, bool top_or_bot);

	void mark_wall(const glm::vec2&  point);
	void mark_wall(const glm::vec3&  point);
	void mark_wall(const glm::uvec2& coord);

	void mark_line(std::vector<std::vector<int>>& map, glm::vec2 start,  glm::vec2 dst);

	void delete_marked_ground(const std::vector<std::vector<int>>& map, const glm::vec3& center);

private:
	ArrayBuffer<glm::vec3> _vertexs;
	ArrayBuffer<glm::vec4> _colors;
	ArrayBuffer<glm::vec4> _normal_angles;
	ElementBuffer          _indices;

	mutable std::vector<const BasicBuffer*> _buffers_cache;

	glm::vec3  _position;
	
	float _size;
	int   _cut;
	float _ymax;
	bool  _holed;

	LinearValuator<glm::vec3> _color_variation;

	std::vector<std::vector<int>> _map;
	std::vector<BasicHitbox::storedHitbox> _hitboxes;

	std::vector<glm::vec2> _test;
	std::vector<glm::vec3> _test2;
};

#endif //_GROUND_HPP_
