#include "castle_circle_bump.hpp"

#include "../../ssl/src/vvv.hpp"
#include "../utils/mutils.hpp"

#include "../motor/building.hpp"
#include "../motor/meshbuild.hpp"
#include "../motor/fan_hitbox.hpp"

#include "castle_colors.hpp"

namespace CASTLE
{
	CircleBump::CircleBump(const baseBuiling& base, uint face)
		: FaceStructure(base, face),
	  	  _underbluf(0.5f*this->ND()),
	  	  _height(2.0f*this->ND()),
	  	  _topheight(15.0f*this->ND()),
	  	  _pilheight_coeff(1.2f)
	{

	}
	
	void CircleBump::build_wall_deco(const std::vector<doorWall>& walls_to_build)
	{
		uint N = walls_to_build.size();
		float overh = _topheight*_pilheight_coeff;
		int towerplaced = -1;
		doorWall towerdoor;
		float towerh = this->ND()*15.0f;
		float tower_botmin = _topheight*0.3f;

		for(uint i=0; i<N; i++)
		{
			const doorWall& w = walls_to_build[i];

			if(w.dh <= 0 || w.opbuild->get_role() == CATHEDRAL)
				continue;

			// TRY TOWER
			float tower_botmax = tower_botmin;//std::max(tower_botmin, w.dh-this->safe_height()-towerh);
			if((towerplaced == -1) && this->place_fat_tower_deco(w, utils::random_float(tower_botmin, tower_botmax), 0.7f, towerh))
			{
				towerplaced = i;
				towerdoor = w;
				continue;
			}
		}

		for(uint i=0; i<N; i++)
		{
			const doorWall& w = walls_to_build[i];

			if(w.dh <= 0)
				continue;

			if(i == towerplaced)
				continue;

			if(((towerplaced == -1) || this->wall_projdistances(towerdoor, w) >= this->counterfort_minh()*1.0f)
			&& (w.dh > overh + this->counterfort_minh()*1.5f + this->safe_height()))
			{
				if(rand()%3==0)
				{
					this->place_pilers_range(w, 0.0f);
				}
				else
				{
					std::vector<planarYPlace> cpos = this->place_counterforts_range(w, 0.0f);
					
					for(planarYPlace p : cpos)
					{
						float countersize = p.size.x*this->counterfort_geom().size;

						if(this->square_in_face(p.center, geom::unprojplan(p.dirN), countersize*2.0f))
						{	
							this->place_counterfort_support(p, overh, 2.0f);
						}
					}
				}
			}	
			else
			{
				this->build_topdeco(w);
			}
		}
	}

	void CircleBump::build_face(const std::vector<doorWall>& walls_to_build)
	{		
		ArrayBuffer<glm::vec3>& vertices   = this->mesh_vertices();
		ArrayBuffer<glm::vec3>& colors     = this->mesh_colors();
		ArrayBuffer<glm::vec3>& invertices = this->inmesh_vertices();
		ElementBuffer& elements   = this->get_elements();
		ElementBuffer& inelements = this->get_inelements();

		std::vector<uint> startindices;
		startindices = utils::uirange(this->bluffup(walls_to_build, _underbluf, _underbluf*2.0f, this->wall_radius()*0.4f));

		#warning Center may not be in the face
		#warning Aconvexity can make roof crossing some others faces 
		glm::vec3 top = this->barycenter() + vvv::y()*(_height+_topheight); 
		meshbuild::translate(vertices, startindices, vvv::y()*_height);
		this->place_geocylinder_hitbox(startindices, -vvv::y()*(_underbluf+_height));
		for(uint i : startindices) {colors.change_subvalue(i, colors[i]*0.6f);}
		
		std::pair<uint, uint> rindices = meshbuild::copytranslate(vertices, startindices, glm::vec3(0));
		this->complete_color_buffer(castle_colors::CIRCL_BUMP_UNPROBABLE(), 0.2f);
		meshbuild::fan_cycle(vertices, elements, rindices, top, true);
		this->complete_color_buffer(castle_colors::CIRCL_BUMP_UNPROBABLE()*1.2f);

		this->fill_walls(walls_to_build);
	
		// std::vector<uint> cyclei = this->topcycle(insidewalls);
		// meshbuild::fill_cycle(invertices, inelements, cyclei, false);
		// this->place_ingeocylinder_hitbox(cyclei, vvv::y()*_underbluf*2.0f);
	
		fanInfo fan = {
			.cycle = vertices.subset(rindices),
			.origin = top
		};
		this->add_hitbox(std::make_unique<FanHitbox>(fan));
	}
};
