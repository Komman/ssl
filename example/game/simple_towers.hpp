#ifndef _SIMPLE_TOWERS_HPP_
#define _SIMPLE_TOWERS_HPP_

#include "../motor/physical_instances.hpp"
#include "../motor/hitbox_context.hpp"
#include "../motor/vertical_shadower.hpp"

#include "castle_building_inst.hpp"
#include "lanterns.hpp"

using namespace ssl;

struct simpleTower
{
	glm::vec3 position;
	glm::vec3 orientationN;
	float     height;
	float     size;
};

class SimpleTower : public CastleBuildingInst<simpleTower,
							2,
							glm::vec3,
							glm::vec3,
							glm::vec4,
							glm::vec4>
{
public:
	SimpleTower(HitboxContext& hbx_context, Lanterns& lanterns, VerticalShadower& vshadower);

	void add_tower(const simpleTower& tower);

protected:
	void place_building_only(const simpleTower& place, float h) override;
	simpleTower generate_building(const genParam& g) override;
	planarPlace get_place(const simpleTower& g) override;

	struct towerStorage
	{
		simpleTower  tower;
		BasicHitbox* moving_hitbox;
	};

	/*
		For now, towers can't be deleted, thus
		tower indices are their indentifiors 
	*/
	uint get_towers_amount()  const;
	void  set_tower_height(uint towerid, float h);
	float get_tower_height(uint towerid) const;
	towerStorage get_tower_info(uint towerid) const;
	glm::vec3 get_tower_position(uint towerid) const;

private:
	enum bufferRoles {RELATIVE_POS=0, COLORS, CENTERS_HEIGHT, ORIENTATION_N_SIZE};
	
	// Bottom and top indices
	using squareBase = std::vector<uint>;
	using cubeBase   = std::pair<squareBase, squareBase>;
	
	void build_relative_base();
	void build_window(float head_bot, float head_top, float xdec);
	void build_corner(float head_bot, float head_top);
	uint add_base_vertex(const glm::vec3& pos, const glm::vec3& color);
	squareBase build_square_base(float h, float size, const glm::vec3& color);
	cubeBase   build_cube_base(float hbot, float htop, float size, const glm::vec3& color_bot, const glm::vec3& color_top);
	void link_square_bases(const cubeBase& bot_top);
	void link_square_bases(const cubeBase& bbot, const cubeBase& btop);


private:
	VerticalShadower& _vshadower;
	Lanterns&         _lanterns;

	std::vector<towerStorage> _towers_storage;

	float _head_bot;
	float _head_top;

	glm::vec3 _base_color;
	glm::vec3 _roof_color;
	glm::vec3 _win_color;
	glm::vec3 _corner_color;
	glm::vec2 _corner_size;

	glm::vec2 _y_sizes[5];
	glm::vec2 _roof_size;
	float _win_extr;
};


#endif //_SIMPLE_TOWERS_HPP_
