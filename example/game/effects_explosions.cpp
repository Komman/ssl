#include "effects.hpp"

#include "../utils/geom.hpp"
#include "../motor/gutils.hpp"
#include "../motor/light_intensity_valuator.hpp"

#include <memory>

using namespace glm;

namespace effects
{
	void add_homogenous_explosion(const glm::vec3& position,
								  float intensity,
								  float duration,
								  const glm::vec3& explosion_color,
								  const glm::vec3& disc_color,
								  const glm::vec3& clouds_color,
								  const glm::vec3& opaque_light_color)
	{
		glm::vec3 colors[EXPLO_COLORS_AMOUNT];
		colors[LIGHT_COLOR]       = glm::max(explosion_color, glm::vec3(0.1));
		colors[SPHERE_COLOR]      = explosion_color;
		colors[DISC_COLOR]        = disc_color;
		colors[CLOUDS_COLOR]      = clouds_color;
		colors[BIBO_COLOR]        = opaque_light_color;
		colors[LIGHT_PEAKS_COLOR] = opaque_light_color/1.0f;

		add_custom_explosion(position,
			intensity*3.0f,
			duration,
			colors,
			intensity/50.0f,
			intensity/33.0f,
			intensity*20.0f,
			uint(2.0f*intensity),
			uint(intensity/33.0f),
			intensity*0.4f,
			intensity*0.4f/100.0f,
			intensity*0.15f,
			intensity*0.8f,
			uint(intensity/30.0f+0.5),
			float(M_PI/32.0f));
	}

	void particles_explosion(const glm::vec3& position,
							 float size,
							 float cloud_ejection,
							 const glm::vec3& color,
							 float bloomcoef_short,
							 float bloomcoef_long,
							 float duration,
							 uint short_clouds_count,
							 uint long_clouds_count,
							 const glm::vec3& orientation,
							 float light_on_bloom_affection_coeff)
	{
		for(unsigned int i=0; i<short_clouds_count; i++)
		{
			float ejectcoeff = utils::random_float(0.0f,cloud_ejection/8.0f);

			vec3 orient = glm::normalize(utils::random_vec3(vec3(-1.0,0.0,-1.0), vec3(1.0)))*(cloud_ejection + ejectcoeff);
			orient = geom::rotate_redirect(orient, vvv::y(), orientation);
			
			Particle p(
				position,
				orient,
				glm::vec3(0.0),
				glm::vec4(color, bloomcoef_short),
				size,
				1.0f,
				duration + utils::random_float(0.0f,duration/1.5f)
			);
			p.set_light_bloom_affection(light_on_bloom_affection_coeff);

			effects::packed_clouds().add_particle(p);
		}
		for(unsigned int i=0; i<long_clouds_count; i++)
		{
			vec3 orient = glm::normalize(utils::random_vec3(vec3(-1.0,0.0,-1.0), vec3(1.0)))*cloud_ejection*10.0f;
			orient = geom::rotate_redirect(orient, vvv::y(), orientation);

			Particle p(
				position,
				orient,
				glm::vec3(0.0),
				glm::vec4(color, bloomcoef_long),
				size*4.0f,
				1.0f,
				0.6f + utils::random_float(0.0f,0.5f)
			);
			p.set_light_bloom_affection(light_on_bloom_affection_coeff);

			effects::packed_clouds().add_particle(p);
		}
	}

	void add_big_explosion_light(const glm::vec3& position,
							 const glm::vec3& color,
							 float intensity,
							 float duration)
	{
		using LightValuator = LightIntensityValuator<LinearValuator<dynamicLight>>;
		using namespace glm;

		std::unique_ptr<LightValuator> explolight = std::make_unique<LightValuator>(dynamicLight({
			position,
			color,
			intensity,
			3.1f}));

		explolight->add_value(1.0f, intensity);
		explolight->add_value(5.0f, intensity*100.0f);
		explolight->add_value(0.0f, 0.0f);
		explolight->force_total_duration(duration);

		if(effects::dynamic_lightor().available_light_slots() > 0)
		{
			effects::timed_dynamic_lightor().add_light(effects::dynamic_lightor(), std::move(explolight));
		}
	}

	void add_explosion_light(const glm::vec3& position,
							 const glm::vec3& color,
							 float intensity,
							 float duration)
	{
		using LightValuator = LightIntensityValuator<LinearValuator<dynamicLight>>;
		using namespace glm;

		std::unique_ptr<LightValuator> explolight = std::make_unique<LightValuator>(dynamicLight({
			position,
			color,
			intensity,
			3.1f}));

		explolight->add_value(1.0f, intensity);
		explolight->add_value(1.5f, intensity*100.0f);
		explolight->add_value(0.0f, 0.0f);
		explolight->force_total_duration(duration);

		if(effects::dynamic_lightor().available_light_slots() > 0)
		{
			effects::timed_dynamic_lightor().add_light(effects::dynamic_lightor(), std::move(explolight));
		}
	}

	void add_custom_explosion(const glm::vec3& position,
						      float intensity,
						      float duration,
						      const glm::vec3 colors[EXPLO_COLORS_AMOUNT],
						      float explodisc_radius, // =0 if no explosisc
						      float explodisc_thickness,
						      float clouds_impact,
						      uint  short_clouds_count,
						      uint  long_clouds_count,
						      float cloud_ejection,
						      float cloud_size,
						      float sphere_radius,   //=0 if no spheres
						      float explobibo_size,  //=0 if no bibo
						      uint  peaks_lights_amount, // it is dobbled by symmetry
						      float peaks_lights_angle)
	{
		effects::add_big_explosion_light(position, colors[LIGHT_COLOR], intensity, duration);		

		if(explodisc_radius > 0.0f)
		{
			effects::explodisc().add({.center     = position,
			      .color      = colors[DISC_COLOR],
			      .radius     = explodisc_radius,
			      .thickness  = explodisc_thickness,
			      .duration   = duration,
			      .speed      = 15.0f});

		}
		
		effects::clouds().explode(position, clouds_impact, 0.02f);

		effects::particles_explosion(position, cloud_size, cloud_ejection, colors[CLOUDS_COLOR], 10.0f, 15.0f, duration, short_clouds_count, long_clouds_count, vvv::y());

		if(sphere_radius > 0.0f)
		{
			effects::explospheres().add_cube({
				.position = position,
				.color    = colors[SPHERE_COLOR],
				.size     = sphere_radius,
				.duration = duration
			});
		}

		if(explobibo_size > 0.0f)
		{
			effects::explobibos().add_instance({
				.center   = position,
				.size     = vec2(explobibo_size,explobibo_size/20.0f),
				.color    = colors[BIBO_COLOR],
				.duration = duration
			});
		}

		for(uint i=0; i<peaks_lights_amount; i++)
		{
			float range = sphere_radius * 20.0f;

			vec3 traject = utils::random_vec3(vec3(-range, 0.0f, -range), vec3(range));

			effects::meshedlight_luminor().add_ray(std::move(std::make_unique<ExploMeshedRayLight>(
				position,
				traject,
				peaks_lights_angle,
				colors[LIGHT_PEAKS_COLOR],
				duration/2.0f
			)));

			effects::meshedlight_luminor().add_ray(std::move(std::make_unique<ExploMeshedRayLight>(
				position,
				vec3(-traject.x, traject.y, -traject.z),
				peaks_lights_angle/2.0f,
				colors[LIGHT_PEAKS_COLOR],
				duration/2.0f
			)));
		}
	}
};