#include "storm.hpp"

using namespace ssl;
using namespace std;
using namespace glm;

Storm::Storm(const Ground& ground,
			 const std::string& uniform_pre_name,
			 const glm::vec3& start_position,
			 float power,
			 float thikness,
			 float largery,
			 float steps)
	: Tritube(start_position, vvv::y(), thikness),
	  _ground(ground),
	  _touch(uniform_pre_name + TOUCH_ADD_NAME, vec3(0.0)),
	  _power(uniform_pre_name + POWER_ADD_NAME, power),
	  _thikness(uniform_pre_name + THIKNESS_ADD_NAME, thikness),
	  _largery(largery),
	  _steps(steps),
	  _powered_color(uniform_pre_name + "powered_color", vec4(0.5,0.5,1.8, 1.0))
{
	this->regenerate(start_position, power, thikness, largery, steps);
}

void Storm::regenerate(const glm::vec3& start_position,
					   float power,
			           float thikness,
			           float largery,
			           float steps)
{
	_power = power;

	_thikness = thikness;
	_largery = largery;
	_steps = steps;

	this->born(start_position, vvv::y(), thikness);

	while(this->extremity().y > _ground.get_y(this->extremity().x, this->extremity().z))
	{
		vec3 growing_direction = vec3(utils::random_float(-largery,largery),
									  -steps,
									  utils::random_float(-largery,largery));
		
		this->grow(growing_direction, thikness);
	}



	this->compute_impact();
}

void Storm::change_powered_color(const glm::vec4& new_powered_color)
{
	_powered_color = new_powered_color;
}

vec3 Storm::compute_touch()
{
	return vec3(this->extremity().x, _ground.get_y(this->extremity().x, this->extremity().z) + 2.0f, this->extremity().z);
}


void Storm::set_impact(const glm::vec3& position)
{
	_touch = position;
}

void Storm::compute_impact()
{
	_touch = this->compute_touch();
}
