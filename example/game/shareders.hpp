#ifndef _SHAREDERS_HPP_
#define _SHAREDERS_HPP_

#include "../../ssl/ssl.hpp"

using namespace ssl;

namespace shareders
{
	void init();
	void free();

	void try_to_initialize();

	Drawer& mesh_viewer();
	Drawer& tower_bomb();
	Drawer& brut_color();
	Drawer& brut_ground();
	Drawer& tower_drawer();
	Drawer& mtower_drawer();
	Drawer& wall_drawer();
	Drawer& planar_builds();
	Drawer& counterforts();
	Drawer& arc_doors();
	Drawer& lantern_drawer();
	Drawer& castle_base();
	Drawer& castle_inside();
	Drawer& tower_cannons();
	Drawer& text_renderer();
	// Drawer& fat_tower_drawer();
};

#endif //_SHAREDERS_HPP_
