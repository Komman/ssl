#include "sword.hpp"

using namespace glm;

Sword::Sword(const glm::vec3& position)
	: StoredDrawableObject(STATIC_DRAW, STATIC_DRAW, STATIC_DRAW)
{
	this->build_sword();

	this->center_on_pos(position);
}

void Sword::center_on_pos(const glm::vec3& position)
{
	for(uint i=0; i<this->get_array_buffer<POSITIONS>().size(); i++)
	{
		const vec3& v = this->get_array_buffer<POSITIONS>()[i];
		this->get_array_buffer<POSITIONS>().change_subvalue(i, position + v);
	}
}

void Sword::build_sword()
{
	this->add_vertex(vec3(10,1,1), vec3(1,0,0));
	this->add_vertex(vec3(1,10,1), vec3(0,1,0));
	this->add_vertex(vec3(1,1,10), vec3(0,0,1));

	this->get_elements().add_triangle(0,1,2);
}
