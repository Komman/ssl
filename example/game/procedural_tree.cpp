#include "procedural_tree.hpp"

#include <stdlib.h>

#include <math.h>
#include <glm/gtx/rotate_vector.hpp>

#include "../../ssl/src/vertex_array_object.hpp"

#include "../utils/geom.hpp"
#include "../motor/bevel_hitbox.hpp"

using namespace glm;
using namespace std;

ProceduralTree::ProceduralTree(const glm::vec3& position,
							   const glm::vec3& direction,
							   const glm::vec3& orientation,
							   float trunc_size_multiplier,
							   float trunc_width_multiplier,
							   float branch_size,
							   float branch_width,
				    		   float spreading,
							   const glm::vec4& root_color,
							   const glm::vec4& leaves_colors,
							   int total_levels,
							   float final_leave_size,
							   float final_leave_height,
							   const vector<vec4>&  final_leave_color,
							   const std::vector<vec3>& all_offset_positions)

	: _points(STATIC_DRAW), _colors(STATIC_DRAW), _normals(STATIC_DRAW), _indices(STATIC_DRAW), _deps(STATIC_DRAW),
	  _hitboxes(),
	  _position(position), 
	  _direction(direction), 
	  _orientation(orientation), 
	  _root_color(root_color), 
	  _leaves_colors(leaves_colors), 
	  _final_leave_color(final_leave_color),
	  _trunc_size_multiplier(trunc_size_multiplier), 
	  _trunc_width_multiplier(trunc_width_multiplier), 
	  _branch_size(branch_size), 
	  _branch_width(branch_width), 
	  _spreading(spreading), 
	  _total_levels(total_levels), 
	  _final_leave_height(final_leave_height),
	  _final_leave_size(final_leave_size),
	  _dark_coeff_under_leaves(0.5f),
	  _vao(std::vector<bufferDivisor>({{&_points, 0}, {&_colors, 0}/*, {&_normals, 0}*/, {&_deps, 1}}), &_indices)
{
	if(_final_leave_color.size() == 0)
	{
		err("ProceduralTree::ProceduralTree(): final_leave_color.size()=0 (must be >=1)");
	}

	if(all_offset_positions.size() == 0)
	{
		_deps.add_value(vec3(0.0));
	}

	this->rebuild();

	// Must be after the rebuild() call
	for(auto& dep_vec : all_offset_positions)
	{
		_deps.add_value(dep_vec);
		
		auto hbx = std::make_unique<BevelHitbox>(bevelShape{
			.triangle_base = {
				dep_vec+_points[0],
				dep_vec+_points[1],
				dep_vec+_points[2]
			},
			.sized_direction = vvv::y()*(trunc_size_multiplier*branch_size)
		});
		_hitboxes.push_back(std::move(hbx));
	}
}

void ProceduralTree::animate(float dt)
{	

}

const VertexArrayObject& ProceduralTree::get_vao() const
{
	return _vao;
}


const std::vector<BasicHitbox::storedHitbox>& ProceduralTree::get_hitboxes() const
{
	return _hitboxes;
}

void ProceduralTree::rebuild()
{

	_points.change_value({});
	_colors.change_value({});
	_normals.change_value({});
	_indices.change_value({});

	vec3 directions[3] = {
		glm::normalize(glm::rotate(glm::normalize(_orientation), float(2.0*M_PI*2.0/3.0), _direction)),
		glm::normalize(glm::rotate(glm::normalize(_orientation), float(1.0*M_PI*2.0/3.0), _direction)),
		glm::normalize(glm::rotate(glm::normalize(_orientation), float(0.0*M_PI*2.0/3.0), _direction))
	};

	vec3 root[3];

	uint base_indices[3]={0,1,2};

	for(uint i=0;i<3;i++)
	{
		root[i] = _position + directions[i]*_branch_width*_trunc_width_multiplier;

		_points.add_value(root[i]);
		_colors.add_value(_root_color);
		_normals.add_value(directions[i]);
		_indices.add_value(i);
	}
	
	auto new_base = this->add_branch(root, base_indices, _branch_size*_trunc_size_multiplier, _branch_width, _spreading, _root_color, _leaves_colors, 1.0-float(_total_levels-1)/float(_total_levels));
	
	recursive_building(new_base, _total_levels-2, _branch_size, _branch_width);

}

void ProceduralTree::recursive_building(const std::vector<std::pair<glm::vec3, uint>>&  start_branch,
						uint level,
					    float branch_size,
					    float branch_width)
{

	float level_anticoeff = float(level)/float(_total_levels);
	float level_coeff = 1.0-level_anticoeff;

	if(level <= 0)
	{
		if(_final_leave_size != 0.0 && _final_leave_height != 0.0)
		{
			for(const auto& lastb : start_branch)
			{
				_colors.change_subvalue(lastb.second, _colors[lastb.second]*_dark_coeff_under_leaves);
			}
			add_final_leave(start_branch);
		}
		return;
	}

	vec3 root[3];
	uint base_indices[3];

	//int chance_to_stop = 4;

	for(uint i=0;i<3;i++)
	{
		int iadd = (i+1)%3;

		root[0]=start_branch[3].first;
		root[1]=start_branch[i].first;
		root[2]=start_branch[iadd].first;

		base_indices[0]=start_branch[3].second;
		base_indices[1]=start_branch[i].second;
		base_indices[2]=start_branch[iadd].second;

		if(true)//rand() % chance_to_stop > 0)
		{
			auto new_branch = this->add_branch(root, base_indices, branch_size, branch_width * (level_anticoeff+0.1), _spreading, _root_color, _leaves_colors, level_coeff);
			recursive_building(new_branch, level-1, branch_size/1.4, branch_width/1.4f);
		}
		else
		{
			this->stop_branch(base_indices);
		}
	}
}

void ProceduralTree::add_final_leave(const std::vector<std::pair<glm::vec3, uint>>&  branch)
{
	vec3 middle(0.0);

	for(uint i=0;i<3;i++)
	{
		middle+=branch[i].first;
	}
	middle/=3.0;

	vec3 leave_direction = -glm::normalize(glm::cross(branch[0].first-middle, branch[1].first-middle));

	uint start_size = _points.size();

	_points.add_value(middle);
	_colors.add_value(_final_leave_color[2%_final_leave_color.size()]*_dark_coeff_under_leaves);
	_normals.add_value(-leave_direction);

	vec3 first_dir = glm::normalize((branch[2].first-middle));
	int leave_prec = 5;

	for(int i=0;i<leave_prec;i++)
	{
		vec3 tmp_direction = glm::rotate(first_dir,
										 float(i)*2.0f*(float)(M_PI)/(float)(leave_prec),
										 leave_direction);
												
		_points.add_value(middle+tmp_direction*_final_leave_size);
		
		_colors.add_value(_final_leave_color[(i+1) % (_final_leave_color.size() - 1)]);
		_normals.add_value(tmp_direction);
	}
	
	_points.add_value(middle+leave_direction*_final_leave_height);
	_normals.add_value(leave_direction);
	_colors.add_value(_final_leave_color[0]);


	for(int i=0;i<leave_prec;i++)
	{
		_indices.add_value(start_size);
		_indices.add_value(start_size+1+(i+1)%leave_prec);
		_indices.add_value(start_size+1+i);
	}

	for(int i=0;i<leave_prec;i++)
	{
		_indices.add_value(_points.size()-1);
		_indices.add_value(start_size + 1 + i);
		_indices.add_value(start_size + 1 + (i+1)%leave_prec);
	}
}

void ProceduralTree::stop_branch(uint end_indices[3])
{
	_indices.add_value(end_indices[0]);
	_indices.add_value(end_indices[2]);
	_indices.add_value(end_indices[1]);
}

std::vector<std::pair<glm::vec3, uint>>  ProceduralTree::add_branch(glm::vec3 base[3],
								uint base_indices[3],
							    float branch_size,
							    float branch_width,
							    float spreading,
							    const glm::vec4& root_color,
							    const glm::vec4& leaves_colors,
							    float level_coeff)
{
	std::vector<std::pair<glm::vec3, uint>> ret;

	vec3 grow_norm_direction = glm::normalize(this->get_direction(base));
	vec3 grow_direction = grow_norm_direction*branch_size;

	uint first_index = _points.size();

	vec3 middle(0.0);

	for(uint i=0;i<3;i++)
	{
		middle+=base[i];
	}
	middle = middle/3.0f + grow_direction;

	for(int i=0;i<3;i++)
	{
		vec3 point_direction = glm::normalize(glm::rotate(glm::normalize(base[2]+grow_direction-middle), -float(i)*float(M_PI*2.0/3.0), grow_direction));
		vec3 new_point = middle + point_direction*branch_width;
	
		ret.push_back(make_pair(new_point, _points.size()));

		_points.add_value(new_point);
		_colors.add_value(this->get_level_color(_root_color, _leaves_colors, level_coeff));
		_normals.add_value(point_direction);

		_indices.add_value(base_indices[i]);
		_indices.add_value(first_index + i);
		_indices.add_value(first_index + (i+1)%3);

		_indices.add_value(base_indices[i]);
		_indices.add_value(first_index + (i+1)%3);
		_indices.add_value(base_indices[(i+1)%3]);
	}

	vec3 new_point = middle+grow_norm_direction*branch_width*_spreading;

	ret.push_back(make_pair(new_point, _points.size()));

	_points.add_value(new_point);
	_normals.add_value(grow_norm_direction);
	_colors.add_value(this->get_level_color(_root_color, _leaves_colors, level_coeff));

	return ret;
}

glm::vec4 ProceduralTree::get_level_color(const glm::vec4& root_color,
							   			  const glm::vec4& leaves_colors,
							   			  float level_coeff)
{
	return (_leaves_colors*level_coeff + _root_color*float(1.0-level_coeff));
}

glm::vec3 ProceduralTree::get_direction(glm::vec3 base[3])
{
	vec3 v1 = base[0] - base[1];
	vec3 v2 = base[0] - base[2];
	return glm::cross(v2, v1);
}

const std::vector<const BasicBuffer*>& ProceduralTree::get_buffers() const
{
	//_draw_buffers = {&_points, &_colors, &_normals, &_deps};
	_draw_buffers = {&_points, &_colors, &_deps};
	return _draw_buffers;
}

const ElementBuffer& ProceduralTree::get_elements() const
{
	return _indices;
}
