#include "castle_support.hpp"

#include "../motor/hitbox_fusion.hpp"
#include "../motor/paver_hitbox.hpp"
#include "../motor/rectangular_frame_hitbox.hpp"

using namespace glm;

CastleSupport::CastleSupport(HitboxContext& hbx_context, VerticalShadower* vshadower)
	: CastleBuildingInst(hbx_context,
		{
			{},
			{
				// valuedPolygon2DInfo{
				// 	vec2(0),
				// 	{
				// 		vec2(-1,-1)/2.0f,
				// 		vec2(-1,+1)/2.0f,
				// 		vec2(+1,+1)/2.0f,
				// 		vec2(+1,-1)/2.0f
				// 	},
				// 	1
				// },
				valuedPolygon2DInfo{
					vec2(0),
					{
						vec2(-1,-1)*0.5f,
						vec2(-1,+1)*0.5f,
						vec2(+1,+1)*0.5f,
						vec2(+1,-1)*0.5f
					},
					UP_AVAILABLE
				}
			}
		},
		STATIC_DRAW,
		STATIC_DRAW,
		STATIC_DRAW,
		STATIC_DRAW,
		STATIC_DRAW),
	  _vshadower(vshadower),
	  _base_color(0.7,0.5,0.3),
	  _thickness(0.05),
	  _yB(_thickness),
	  _xC(1.0f - _thickness),
	  _yD(1.0f - 3.0f*_thickness),
	  _yE(1.0f - _thickness),
	  _yF(1.00),
	  _xG(_xC),
	  _yH(_yE)
{
	this->build_base();
}

void CastleSupport::place_building_only(const castleSupport& building, float h)
{
	auto b = building;
	b.center.y += h;

	this->add_support(b);
}

castleSupport CastleSupport::generate_building(const genParam& g)
{
	return {
		.center = vec3(g.position.x, 0.0f, g.position.y),
		.dirN2D = g.dirN,
		.size   = vec3(g.maxsize*utils::random_float(0.1f, 1.0f), utils::random_float(g.maxsize/3.0f, g.maxsize*2.0f), g.maxsize)
	};
}

planarPlace CastleSupport::get_place(const castleSupport& g)
{
	return {
		.center = geom::projplan(g.center),
		.size   = geom::projplan(g.size),
		.dirN   = g.dirN2D
	};
}

void CastleSupport::add_support(const castleSupport& support)
{
	this->get_array_buffer<POS_SIZEY>().add_value(vec4(support.center, support.size.y));
	this->get_array_buffer<DIR2D_SIZEXZ>().add_value(vec4(support.dirN2D.x, support.dirN2D.y, support.size.x, support.size.z));

	vec3 dirN3D = vec3(support.dirN2D.x, 0, support.dirN2D.y);
	
	vec3 dirxN     = (dirN3D);
	vec3 dirzN     = (glm::cross(dirN3D, vvv::y()));
	vec3 bottomdec = (dirxN*support.size.z + dirzN*support.size.x)/2.0f;

	_vshadower->add_square(support.center, dirN3D, vec2(support.size.x, support.size.z)*1.12f);

	auto total_hbx = std::make_unique<HitboxFusion>();

	total_hbx->fusion_hitbox<PaverHitbox>(paverInfo{
		.bottom  = support.center - bottomdec,
		.sized_x = dirzN*support.size.x,
		.sized_y = vvv::y()*support.size.y*_yB,
		.sized_z = dirxN*support.size.z
	});

	total_hbx->fusion_hitbox<PaverHitbox>(paverInfo{
		.bottom  = support.center - bottomdec*_xC + vvv::y()*support.size.y*_yB,
		.sized_x = dirzN*support.size.x*_xC,
		.sized_y = vvv::y()*support.size.y*(_yH-_yB),
		.sized_z = dirxN*support.size.z*_xC
	});

	float yappr = (_yD + _yE)/2.0f;
	total_hbx->fusion_hitbox<RectangularFrameHitbox>(rectangularFrameInfo{
		.center     = support.center + vvv::y()*support.size.y*yappr,
		.radius_x   = dirzN*support.size.x/2.0f,
		.height_dir = vvv::y()*support.size.y*(_yF-yappr),
		.radius_z   = dirxN*support.size.z/2.0f,
		.thicknesses_coeffs = vec2(1.0f-_xG)
	});

	_hitboxes.push_back(std::move(total_hbx));
	_hbx_context->add_hitbox(_hitboxes[_hitboxes.size()-1].get());
}

/*
    F-G
    | |
    E H--
     \
      D
      |
      |
      |
    B-C
    |
	A
*/

void CastleSupport::link_growbase(SymType& sym, uint i1, uint i2)
{
	sym.link_rectangle(i1, sym.symmetrical(1, i1), sym.symmetrical(1, i2), i2);
}

void CastleSupport::build_base()
{
	auto& relpos = this->get_array_buffer<RELATIVE_POS>();
	auto& colors = this->get_array_buffer<COLOR>();
	auto& elements = this->get_elements();

	float ex = 0.5;

	float yB = _yB;
	float xC = _xC;
	float yD = _yD;
	float yE = _yE;
	float yF = _yF;
	float xG = _xG;
	float yH = _yH;

	AngularSymmetry<glm::vec3> sym(
		{vec3(0), vvv::y(), 4},
		&relpos,
		&elements,
		&colors
	);

	vec3 align = vec3(ex, 1, ex);

	uint A = sym.place_symmetrical(align*vec3( 1,  0,  1), _base_color*0.6f);
	uint B = sym.place_symmetrical(align*vec3( 1, yB,  1), _base_color);
	uint C = sym.place_symmetrical(align*vec3(xC, yB, xC), _base_color*0.8f);
	uint D = sym.place_symmetrical(align*vec3(xC, yD, xC), _base_color);
	uint E = sym.place_symmetrical(align*vec3( 1, yE,  1), _base_color*0.7f);
	uint F = sym.place_symmetrical(align*vec3( 1, yF,  1), _base_color);
	uint G = sym.place_symmetrical(align*vec3(xG, yF, xG), _base_color);
	uint H = sym.place_symmetrical(align*vec3(xG, yH, xG), _base_color*0.8f);

	this->link_growbase(sym, A, B);
	this->link_growbase(sym, B, C);
	this->link_growbase(sym, C, D);
	this->link_growbase(sym, D, E);
	this->link_growbase(sym, E, F);
	this->link_growbase(sym, F, G);
	this->link_growbase(sym, G, H);

	sym.link_rectangle(
		sym.symmetrical(0, H),
		sym.symmetrical(1, H),
		sym.symmetrical(2, H),
		sym.symmetrical(3, H)
	);
}

