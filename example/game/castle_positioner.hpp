#ifndef _CASTLE_POSITIONER_HPP_
#define _CASTLE_POSITIONER_HPP_

#include "../motor/gutils.hpp"
#include "../motor/stored_drawable_object.hpp"
#include "../motor/hitbox_fusion.hpp"
#include "../motor/hitbox_context.hpp"

#include "ground.hpp"

struct castlePhysicalContext
{
	HitboxContext* context;
	Ground*        ground;
};

class CastlePositioner
{
public:
	// /!\ The ground of the 
	CastlePositioner(const castlePhysicalContext& context, const glm::vec3& position);

	GETTER_BUILDER(glm::vec3, position)
	GETTER_BUILDER_COPY(float, enclosure_radius)
	GETTER_BUILDER_COPY(float, floor_height)

	glm::vec3 get_floor_potision() const;

	const std::vector<glm::vec3>&                     enclosure_towers() const;
	const HitboxFusion& get_floor_hbx() const;

	const spaceBox& minmax_coord() const;

private:
	void place_enclosure(float radius, uint tower_count);

private:
	castlePhysicalContext _context;

	glm::vec3 _position;
 
	float _enclosure_radius;
	int   _ext_towers_count; 
	float _floor_height;

	Uniform<float> _floor_absolute_height;
	Uniform<glm::vec3> _floor_color;

	std::vector<glm::vec3> _enclosure_towers;

	HitboxFusion _floor_hbx;

	mutable spaceBox _minmaxbox;
};

#endif //_CASTLE_POSITIONER_HPP_
