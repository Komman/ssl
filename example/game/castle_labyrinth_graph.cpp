#include "castle_labyrinth_graph.hpp"

namespace CASTLE
{
	LabyrinthGraph::LabyrinthGraph(const BorderedGraph& g, const graphHeightParam& params)
		: RoleGraph(g, params),
		  _doored_edges()
	{
		this->create_labyrinth();
	}

	void LabyrinthGraph::create_labyrinth()
	{
		auto faces = this->all_faces();
		auto face_edges = this->all_face_edges();
		std::unordered_map<uint, uint> faces_extdoors;

		for(uint f : faces) {faces_extdoors[f] = 0;}

		for(uint i=0; i<face_edges.size(); i++)
		{
			const auto& e = face_edges[i];

			uint f1 = e.face_edge.first;
			uint f2 = e.face_edge.second;
			uint proba = 50;

			// const BasicRole& r1 = *this->get_structure(f1);
			// const BasicRole& r2 = *this->get_structure(f2);
			bool from1 = this->possible_ext_door(f1, f2, e.edge);
			bool from2 = this->possible_ext_door(f2, f1, e.edge);

			if((from1 && faces_extdoors[f1]<=1)
			|| (from2 && faces_extdoors[f2]<=1))
			{
				proba = 100;

				if(from1){faces_extdoors[f1]++;}
				if(from2){faces_extdoors[f2]++;}
			}

			if(rand()%100u <= proba)
			{
				_doored_edges.insert(this->order(e.edge));
			}
		}
	}

	bool LabyrinthGraph::possible_ext_door(uint facefrom, uint facein, const Edge& e) const
	{
		return this->get_structure(facefrom)->is_roof_open() && this->ascending_height(e, facefrom) > this->get_min_door_h();
	}

	bool LabyrinthGraph::is_there_door(const Edge& e) const
	{
		auto ret = _doored_edges.find(this->order(e));
		return !(ret == _doored_edges.end());
	}

	std::vector<RoleGraph::Edge> LabyrinthGraph::doored_descending_adjacent_edges(uint face) const
	{
		using  namespace std;
		std::vector<Edge> all = this->face_edges(face);
		std::vector<Edge> ret;

		for(const Edge& e : all)
		{
			uint f = this->opposite_face(e, face);

			if(this->ascending_face_edge({f, face}) && this->is_there_door(e))
			{
				ret.push_back(e);
			}
		}

		return ret;
	}

	std::vector<RoleGraph::Edge> LabyrinthGraph::doored_ascending_adjacent_edges(uint face) const
	{
		using  namespace std;
		std::vector<Edge> all = this->face_edges(face);
		std::vector<Edge> ret;

		for(const Edge& e : all)
		{
			uint f = this->opposite_face(e, face);

			if(!(this->ascending_face_edge({f, face})) && this->is_there_door(e))
			{
				ret.push_back(e);
			}
		}

		return ret;
	}	
};
