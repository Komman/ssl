#ifndef _CASTLE_FACE_STRUCTURE_HPP_
#define _CASTLE_FACE_STRUCTURE_HPP_

#include "../motor/stored_drawable_object.hpp"
#include "../motor/hitbox_context.hpp"

#include "castle_theorical_architecture.hpp"
#include "counter_fort.hpp"
#include "castle_inside_face.hpp"

namespace CASTLE
{
	class Base;
	struct builtStructures;

	using BaseMesh = StoredDrawableObject<glm::vec3, glm::vec3>;
	using BaseHbxs = std::vector<std::unique_ptr<BasicHitbox>>;

	// Incoming = imposed (from lower altitude) and outgoing is the opposite
	enum doorOnWall {NO_DOOR=0, INCOMING, OUTGOING};

	struct baseBuiling
	{
		Base*     base;
		BaseMesh* mesh;
		BaseMesh* inside;
		BaseHbxs* hbxs;
	};

	struct globalArchi
	{
		float fatower_ycoeff;
		uint magicircl_amount;
	};

	using indexRange = std::pair<uint, uint>;

	class FaceStructure
	{
	public:
		using Edge = BasicGraph::Edge;

	public:
		FaceStructure(const baseBuiling& base, uint face);

		std::unique_ptr<InsideFace> build();

		void set_doors_positions(const std::vector<edgeDoorPosition>& imposed_doors,
								 const std::vector<Edge>& doors_to_set);
		bool are_door_placed() const; 
		const std::vector<edgeDoorPosition>& outgoing_door_positions() const; 

		uint  get_face() const;
		Base* get_base() const;

		// (absolute)
		float height() const;
		// minimum height space that a door has
		float door_minh() const;
		/*
			minimum distance between any pair of
			vertices or bewteen any vertex with any
			edge that is not adjacent to it
		*/
		float safe_distance() const;
		/*
			any facade that links two faces that have
			an outside door has a height of at least:
			door_minh() + safe_height()
		*/ 
		float safe_height() const;
		/*
			facade_height(e) is always greater than
			door_minh() + safe_height()
		*/
		float facade_height(const Edge& e) const;
		float wall_radius() const;
		// Height of the lowest roof (relative)
		float min_roof_h() const;
		float floor_h() const;
		// every paths must have a t least this diwth
		float minwidth() const;

		const glm::vec3& walls_color() const;
		const glm::vec3& inside_color() const;

	public:
		/*
			The returned vector must have the same size
			than "doors_to_set" and must contains same
			face edges in the same order.

			edge orientation comes from the CASTLE::Base class.
		*/
		virtual std::vector<edgeDoorPosition> compute_doors_building(const std::vector<edgeDoorPosition>& imposed_doors,
																	 const std::vector<Edge> doors_to_set);

	protected:
		struct doorWall
		{
			Edge edge;
			uint oface; // oposite face
			bool descending;
			float dh; // altitude difference between faces
			doorOnWall role;
			//only if role != NO_DOOR
			edgeDoorPosition door;
			doorInfo infos; 
			BasicRole* opbuild;
		};

		/*
			Edges are in cycle in trigonometric order
		
			Only the descending walls are to build
			Only the incoming doors are to build
			The build must guarantee that it is possible to go to outcoming doors
		*/
		virtual void build_face(const std::vector<doorWall>& walls_to_build);
		virtual void build_wall_deco(const std::vector<doorWall>& walls_to_build);
		virtual std::unique_ptr<InsideFace> inside_face(const std::vector<insideDoor>& doors);

	public:
		Pair<glm::vec3> down_position(const Edge& e) const;
		Pair<glm::vec3> up_position(const Edge& e) const;
		// Returns vertices indices of the top, in same order than "walls_to_build"
		// Do not place any hitbox
		std::pair<uint, uint> bluffup(const std::vector<doorWall>& walls_to_build, float undery, float h, float delargement);
		void put_roofloor(const std::vector<uint>& cycle, float h);
		// Relative h
		std::pair<uint, uint> put_floor(float h, const glm::vec3& color);
		std::pair<uint, uint> put_roof(float h, const glm::vec3& color);
		// With hibotx
		void put_roofloor(float h, float underthick, const glm::vec3& color);
		void add_hitbox(std::unique_ptr<BasicHitbox>&& hbx);
		template<typename HitboxType, typename... ConstructorArgs>
		void add_hitbox(ConstructorArgs&&... args);


	protected:
		struct indexedWall
		{
			doorWall  wall;
			edgeIndex edgei;
		};

		using vec3Edge = std::pair<glm::vec3, glm::vec3>;

		struct wallInfos
		{
			vec3Edge   bot;
			vec3Edge   top;
			glm::vec3  normalN;
			glm::vec3  dirxN;
			float      length;
			float      height;
		};

		struct detailSizes
		{
			glm::vec2 windows; // x: width, y: height 
			glm::vec2 pilers; // x: width, y: depth
			glm::vec3 counterforts; // x: width, y: min height, z: depth
			glm::vec2 loopholes; // x: width, y: height
			float fatower_ycoeff;
		};

		const TheoricalArchitecture& architecture() const;

		const globalArchi& basoul() const;
		glm::vec3 middletop(const doorWall& w);
		doorPlace door_place(const Edge& e);
		std::vector<Edge> descending_adjacent_edges() const;
		void fill_walls(const std::vector<doorWall> walls);
		void fill_wall(const Edge& e);
		void fill_wall_with_door(const doorWall& e);
		void complete_color_buffer(const glm::vec3& color, float randomness = 0.0f);
		// Returns max(facade_height(e), downh)
		float allowed_down(const Edge& e, float downh);
		/*
			Returns the minimum allowed_down() among the
			descending edges
			Returns -1.0f if there is no desending edges
		*/
		float allowed_down();
		void build_inside_walls(const std::vector<doorWall>& walls_to_build, float wall_size, float floor_h, float roof_h, bool place_roof = true, float inside_wall_overhegiht = 0.0f);
		edgeIndex edge_vertices_indices(const Edge& e) const;
		ArrayBuffer<glm::vec3>& mesh_vertices();
		ArrayBuffer<glm::vec3>& mesh_colors();
		ArrayBuffer<glm::vec3>& inmesh_vertices();
		ArrayBuffer<glm::vec3>& inmesh_colors();
		ElementBuffer& get_elements();
		ElementBuffer& get_inelements();
		std::pair<uint, uint> extrude_cycle(const std::vector<uint>& cycle,
								    		const glm::vec3& sized_direction,
								    		const glm::vec3& color,
								    		bool flip_face);
		std::pair<uint, uint> extrude_cycle(const std::pair<uint, uint>& cycle,
								    		const glm::vec3& sized_direction,
								    		const glm::vec3& color,
								    		bool flip_face);
		std::pair<uint, uint> bissectrice_enlarge_cycle(const std::vector<uint>& cycle,
									    				float distance,
									    				const glm::vec3& translation,
								    					const glm::vec3& color,
									    				bool flip_face);
		std::pair<uint, uint> bissectrice_enlarge_cycle(const std::pair<uint, uint>& cycle,
									    				float distance,
									    				const glm::vec3& translation,
								    					const glm::vec3& color,
									    				bool flip_face);
		std::pair<uint, uint> conspace_enlarge_cycle(
			const std::vector<uint>& cycle,
	    	float distance,
	    	float curve_precision_distance,
	    	const glm::vec3& translation,
	    	const glm::vec3& color,
	    	bool flip_face,
	    	bool fill
	    );
		std::vector<insideDoor> to_inside_doors(const std::vector<doorWall>& walls_to_build);
		bool vertices_colors_aligned() const;
		std::vector<uint> mesh_vertices_indices() const;
		// Returns vertices in the same order than "walls_to_build" (first index of each edge)
		std::vector<uint> mesh_vertices_indices(const std::vector<doorWall>& walls_to_build) const;
		// walls_to_build must have the same size than startindices
		std::vector<indexedWall> index_wall(const std::vector<doorWall>& walls_to_build, const std::vector<uint>& startindices) const;
		std::vector<doorWall> extract_walls_to_build(const std::vector<indexedWall>& walls) const;
		std::vector<uint> extract_indices(const std::vector<indexedWall>& walls) const;
		/*
			Returns pair of indices (indices of the inupt vector) that represent the ext walls.
			The ext walls are walls that are descending of a height of at minimum "min_dh" 
			
			- indices are ordered (increasing), so they might be greater than
			  walls_to_build.size() for the miniborder that cross the index 0
			  Thus they are mod(walls_to_build.size())
		*/
		std::vector<indexRange> ext_miniborders(const std::vector<doorWall>& walls_to_build, float min_dh) const;
		std::vector<indexRange> ext_miniborders(const std::vector<indexedWall>& walls, float min_dh) const;
		glm::vec3 dirN(const edgeIndex& edgei) const;
		glm::vec3 dirN(const indexedWall& wall) const;
		// In the INSIDE mesh
		// std::vector<indexedWall> wall_inside_base_vertices(const std::vector<doorWall>& walls_to_build, float height, float wall_size);
		// Returns the same as input but where edges indieces are the top of the wall placed
		// std::vector<indexedWall> build_inside_walls(const std::vector<indexedWall>& walls, float height, const glm::vec3& color);
		// Return the same wall but the indices edge is the top of the wall
		// indexedWall build_inside_wall_facade(const indexedWall& wall, float height, const glm::vec3& color);
		// indexedWall build_inside_cut_wall_facade(const indexedWall& wall, float cut_center_coeff, float cut_width, float height, const glm::vec3& color, bool fill_cutfloor);
		std::vector<uint> topcycle(const std::vector<indexedWall>& walls) const;
		glm::vec3 barycenter() const;
		void place_geocylinder_hitbox(const std::vector<uint>& cycle_indices, const glm::vec3& sized_direction);
		void place_ingeocylinder_hitbox(const std::vector<uint>& cycle_indices, const glm::vec3& sized_direction);
		float ND() const;
		void place_bissetrice_wall_hitbox(const std::vector<glm::vec3>& cycle, float dy, float enlargement);
		void place_bissetrice_wall_hitbox(const std::vector<uint>& cycle, float dy, float enlargement);
		void place_bissetrice_wall_hitbox(const std::pair<uint, uint>& cycle, float dy, float enlargement);
		void place_conspace_wall_hitbox(const std::vector<glm::vec3>& cycle, float dy, float enlargement);
		void place_conspace_wall_hitbox(const std::vector<uint>& cycle, float dy, float enlargement);
		void place_conspace_wall_hitbox(const std::pair<uint, uint>& cycle, float dy, float enlargement);
		void place_window(const glm::vec3& onwallcenter, const glm::vec3& dirN, const glm::vec3& size);
		// May not be synchronized withe th wall start vertices indices
		std::vector<uint> build_border(const std::vector<indexedWall>& walls, float height, float thickness, float underhbx, float height_to_build_border);
		std::vector<uint> build_full_border(const std::vector<uint>& startindices, float height, float thickness, float underhbx);
		std::pair<uint, uint> centerdir_enlarge_cycle(const std::vector<uint>& cycle,
							    				float distance,
							    				const glm::vec3& translation,
						    					const glm::vec3& color,
							    				bool flip_face);
		std::pair<uint, uint> centerdir_enlarge_cycle(const std::pair<uint, uint>& cycle,
							    				float distance,
							    				const glm::vec3& translation,
						    					const glm::vec3& color,
							    				bool flip_face);
		// Center delarge place by flatw
		void place_hatroof(const std::vector<glm::vec3>& place, float flath, float flatw, float hatoph, const glm::vec3& color1, const glm::vec3& color2);
		void place_fullsided_piler(const glm::vec3& botcenter,
								   const glm::vec3& dirN,
								   const std::vector<glm::vec3>& XYshadowsBT,
								   const glm::vec3& color);
		void place_fullsided_wallpiler(const glm::vec3& botcenter,
									   const glm::vec3& dirN,
									   const std::vector<glm::vec3>& XYshadowsBT,
									   float zcoeff,
									   float incline_h,
									   const glm::vec3& color);
		// only for ascending walls
		wallInfos wall_infos(const doorWall& w) const;
		// Returns the centers positions
		// If some linear sampled positions + extdepth*normalN falls into
		// another face, they will not be in the returned vector
		std::vector<glm::vec3> sample_decorations(const doorWall& w, float height, float margin, float width, float extdepth, bool avoid_door, bool forcemiddle);
		std::vector<glm::vec3> sample_decorations(const doorWall& w, float height, float margin, float width, float extdepth);
		void place_hanoi_hibox(const std::pair<uint, uint>& indices);
		std::pair<uint, uint> place_fullsided_piler_vertices(const glm::vec3& botcenter, const glm::vec3& dirN, const std::vector<glm::vec3>& XYshadowsBT, const glm::vec3& color);
		std::pair<uint, uint> place_fullsided_wallpiler_vertices(const glm::vec3& botcenter, const glm::vec3& dirN, const std::vector<glm::vec3>& XYshadowsBT, float zcoeff, float incline_h, const glm::vec3& color);
		builtStructures& build_structures();
		bool is_ext_door(const doorWall& d);
		void place_pilers_range(const doorWall& w, float bot_y, float margin, float interspace, const std::vector<glm::vec3>& XYshadowsBT, float slopeh, float zflat);
		void place_pilers_range(const doorWall& w, float width, float bot_y, float height, float interspace);
		// Returns the bot positions of each counterfort
		std::vector<planarYPlace> place_counterforts_range(const doorWall& w, float width, float bot_y, float height, float interspace);
		void place_window_range(const doorWall& w, float width, float bot_y, float height, float interspace);
		void place_loophole_range(const doorWall& w, float width, float bot_y, float height, float interspace);
		void place_pilers_range(const doorWall& w, float bot_y, float spacing_coeff = 2.0f);
		std::vector<planarYPlace> place_counterforts_range(const doorWall& w, float bot_y);
		void place_window_range(const doorWall& w, float bot_y);
		void place_loophole_range(const doorWall& w, float bot_y);
		const detailSizes& sizes() const;
		// Relative height
		float topfaceh(const doorWall& w) const;
		const counterFort& counterfort_geom() const; 
		float counterfort_minh() const;
		void place_cathedral_deco(const doorWall& w);
		void place_debug_piler(const glm::vec3& pos, const glm::vec3& color = glm::vec3(1,0,0));
		bool deco_need_to_avoid_door(const doorWall& w, float bot_y);
		float topdeco_height() const;
		void place_bigpiler(const doorWall& w, float boy, float xsizecoeff);
		void build_topdeco(const doorWall& w);
		float player_size();
		float closest_lantern(const glm::vec3& pos);
		bool try_lantern(const glm::vec3& pos, const glm::vec3& dirN);
		void place_counterfort_support(const planarYPlace& counterfortpos, float height, float sizecoeff);
		bool in_face(const glm::vec3& p);
		bool square_in_face(const glm::vec3& p, const glm::vec3& dirxN, float size);
		float floor_height();
		void check_buffer_sizes();
		// Retruns if it has been placed
		bool place_fat_tower_deco(const doorWall& w, float bot_y, float xcoeff, float h);
		float wall_projdistances(const doorWall& w1, const doorWall& w2);
		// Returns the index of the crossing wall, projecting all points on a y-fixed plan
		// returns uint_max if intersection not found (in corner for intsance)
		uint wall_intersection(const std::vector<doorWall>& walls_to_build, const directedLine3D& l);

	private:
		Base* _base;
		baseBuiling _builds;

		uint  _face;

		std::vector<edgeDoorPosition> _outgoing_doors;
		std::vector<edgeDoorPosition> _incoming_doors;
		bool _doors_placed;

	protected:
		detailSizes _sizes;
	};

	class FloorStructure : public FaceStructure
	{
	public:
		FloorStructure(const baseBuiling& base, uint face);
	
	protected:
		void build_face(const std::vector<doorWall>& walls_to_build) override;
		void build_wall_deco(const std::vector<doorWall>& walls_to_build) override;
	};


	template<typename HitboxType, typename... ConstructorArgs>
	void FaceStructure::add_hitbox(ConstructorArgs&&... args)
	{
		this->add_hitbox(std::make_unique<HitboxType>(args...));
	}

};


#endif //_CASTLE_FACE_STRUCTURE_HPP_
