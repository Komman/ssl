#ifndef _BASIC_ARROW_PACK_HPP_
#define _BASIC_ARROW_PACK_HPP_

#include "../../ssl/ssl.hpp"

#include "physical_context.hpp"

class BasicArrowPack
{
public:
	BasicArrowPack() {};
	virtual ~BasicArrowPack() {};

	virtual void animate(const PhysicalContext& context, float dt) =0;
	virtual void clear() =0;
	virtual unsigned int size() const =0;
};

#endif //_BASIC_ARROW_PACK_HPP_
