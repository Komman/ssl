#ifndef _CASTLE_LABYRINTH_GRAPH_HPP_
#define _CASTLE_LABYRINTH_GRAPH_HPP_

#include <unordered_map>

#include "castle_role_graph.hpp"

namespace CASTLE
{
	class LabyrinthGraph : public RoleGraph
	{
	public:
		LabyrinthGraph(const BorderedGraph& g, const graphHeightParam& params);

		bool is_there_door(const Edge& e) const;
		
		std::vector<Edge> doored_descending_adjacent_edges(uint face) const;
		std::vector<Edge> doored_ascending_adjacent_edges(uint face) const;

		bool possible_ext_door(uint facefrom, uint facein, const Edge& e) const;

	private:
		void create_labyrinth();

	private:
		std::unordered_set<Edge, Graph::hashEdge> _doored_edges;
	};
};

#endif //_CASTLE_LABYRINTH_GRAPH_HPP_
