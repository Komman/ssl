#include "castle_simple_elevator.hpp"

#include "../utils/geom.hpp"

#include "../motor/paver_hitbox.hpp"
#include "../motor/meshbuild.hpp"
#include "../motor/building.hpp"

using namespace glm;
using namespace std;
using namespace ssl;

namespace CASTLE
{
	SimpleElevator::SimpleElevator(HitboxContext& contex, const glm::vec3& color)
		: StoredVAObject(contex, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW, DYNAMIC_DRAW, STATIC_DRAW),
		  _color(color)
	{
		this->build_base();
	}

	void SimpleElevator::add_elevator(const planarYPlace& place, const glm::vec2& height_range)
	{
		this->add_instance(place, elevatorInfo{
			.height_range = height_range,
			.heightcoeff  = 0.0f,
			.speed = 3.0f
		});
	}	

	void SimpleElevator::animate(float dt)
	{
		auto& els = this->get_instances();

		for(auto& e : els)
		{
			elevatorInfo& storage = e.storage();
			storage.heightcoeff += dt*storage.speed/(storage.height_range.y - storage.height_range.x);

			if(storage.heightcoeff > 1.0f)
			{
				storage.heightcoeff = 1.0f;
				storage.speed = -abs(storage.speed);
			}
			if(storage.heightcoeff < 0.0f)
			{
				storage.heightcoeff = 0.0f;
				storage.speed = abs(storage.speed);
			}

			e.tp(geom::yfixed(e.get_center(), glm::mix(
				storage.height_range.x,
				storage.height_range.y,
				storage.heightcoeff
			)));
		}
	}

	std::unique_ptr<BasicHitbox> SimpleElevator::build_hitbox(const planarYPlace& place) const
	{
		vec3 dirzN = geom::unprojplan(place.dirN);
		vec3 dirxN = glm::cross(vvv::y(), dirzN);

		auto hbx = std::make_unique<HitboxFusion>(place.center);

		hbx->fusion_hitbox<PaverHitbox>(ycentredPaverInfo{
			.bottomy = vec3(0),
			.sized_x = dirxN*place.size.x,
			.sized_y = -vvv::y()*place.size.y,
			.sized_z = dirzN*place.size.z
		});

		return hbx;
	}

	void SimpleElevator::build_base()
	{
		auto& elements = this->get_elements();
		auto& relpos   = this->get_array_buffer<RELATIVE_POS>();
		auto& colors   = this->get_array_buffer<COLOR>();

		building::paver::fullsided(
			relpos,
			elements,
			ycentredPaverInfo{
			.bottomy = vec3(0,-1,0),
			.sized_x = vvv::x(),
			.sized_y = vvv::y(),
			.sized_z = vvv::z()
			}
		);

		for(uint i=0; i<4; i++) {colors.add_value(_color*1.0f);}
		for(uint i=0; i<4; i++) {colors.add_value(_color*0.5f);}
	}
};
