#ifndef _PROJECTILES_HPP_
#define _PROJECTILES_HPP_

#include "arrow_pack_merger.hpp"

namespace projectiles
{
	void init(SharedRessources& shared_ressources);
	void free();

	ArrowPackMerger& arrows();
};

#endif //_PROJECTILES_HPP_
