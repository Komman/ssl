#include "arrow.hpp"

#include "ingame.hpp"

using namespace std;
using namespace glm;

Arrow::Arrow(const arrowStart& arrow)
	: BasicArrow(arrow.position, arrow.directionN, vvv::y()*ingame::gravity()),
	  _length(arrow.length),
	  _size(arrow.size)
{

}

float Arrow::get_length() const
{
	return _length;
}


glm::vec3 Arrow::get_back_position() const
{
	return this->get_position() - this->get_directionN()*_length;
}

float Arrow::get_head_planted_length() const
{
	return this->get_length()*0.065f;
}
