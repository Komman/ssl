
#ifndef _FAT_TOWER_HPP_
#define _FAT_TOWER_HPP_

#include "../motor/angular_symmetry.hpp"
#include "../motor/vertical_shadower.hpp"

#include "castle_building_inst.hpp"
#include "arc_door.hpp"

struct fatTower
{
	glm::vec3 center;
	glm::vec2 dirN2D;
	glm::vec3 size;
};

class FatTower :  public CastleBuildingInst<fatTower,
										2,
										glm::vec3,
										glm::vec3,
										glm::vec4,
										glm::vec4>
{
public:
	FatTower(HitboxContext& hbx_context, VerticalShadower* vshadower, ArcDoor* door, uint sides_amount, const glm::vec3& color);

	void add_tower(const fatTower& tower, bool shadows = true);

protected:
	void place_building_only(const fatTower& building, float h) override;
	fatTower generate_building(const genParam& g) override;
	planarPlace  get_place(const fatTower& g) override;

private:
	enum bufferRoles {RELATIVE_POS=0, COLOR, POS_SIZEY, DIR2D_SIZEXZ};
	using SymType = AngularSymmetry<glm::vec3>;

	void build_base();
	void link_growbase(SymType& sym, uint i1, uint i2);
	void build_loophole(SymType& sym, float x, float y, float h);

private:
	VerticalShadower* _vshadower;
	ArcDoor*          _door;

	uint _circlediscr;
	
	glm::vec3 _base_color;

	float _thickness;
	float _yB;
	float _xC;
	float _yD;
	float _yDm;
	float _yE;
	float _yF;
	float _xG;
	float _yH;
	float _middle_thickness;
	float _loophole_thickness;
};


#endif //_FAT_TOWER_HPP_
