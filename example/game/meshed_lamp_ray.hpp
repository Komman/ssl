#ifndef _MESHED_LAMP_RAY_HPP_
#define _MESHED_LAMP_RAY_HPP_

#include "../motor/meshed_ray_luminor.hpp"

struct lampRay
{
	glm::vec3 color;
	glm::vec3 center;
	glm::vec3 dirN;
	float opening;
	float range;
};

class MeshedLampRay : public MeshedRayLight
{
public:
	MeshedLampRay(const lampRay& ray);

	void set_ray(const lampRay& ray);

	const std::vector<rayVertex>& get_vertices() const override;
	const std::vector<uint>&      get_elements() const override;

	void animate(float dt) override;

private:
	void compute_vertices();

private:
	lampRay _ray;

	std::vector<rayVertex> _vertices;
};

#endif //_MESHED_LAMP_RAY_HPP_
