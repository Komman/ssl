#include "basic_ground.hpp"

glm::vec3 BasicGround::get_xyz(float x, float z) const
{
	return glm::vec3(x, this->get_y(x, z), z);
}

float BasicGround::get_y(const glm::vec3& position) const
{
	return this->get_y(position.x, position.z);
}

glm::vec3 BasicGround::get_xyz(const glm::vec3& position) const
{
	return this->get_xyz(position.x, position.z);
}

float BasicGround::get_y(const glm::vec2& position) const
{
	return this->get_y(position.x, position.y);
}

glm::vec3 BasicGround::get_xyz(const glm::vec2& position) const
{
	return this->get_xyz(position.x, position.y);
}

glm::vec3 BasicGround::get_max_y_position(const glm::vec3& src,
										  const glm::vec3& dest,
										  int precision) const
{
	glm::vec3 maxypos;
	glm::vec3 srcY  = this->get_xyz(src);
	glm::vec3 destY = this->get_xyz(dest);
	if(srcY.y > destY.y)
	{
		maxypos = srcY;
	}
	else
	{
		maxypos = destY;
	}

	for(int i=0; i<precision; i++)
	{
		glm::vec3 new_pos = glm::mix(src, dest, float(i+1)/float(precision+1));
		new_pos = this->get_xyz(new_pos);
		if(new_pos.y > maxypos.y)
		{
			maxypos = new_pos;
		}
	}

	return maxypos;
}