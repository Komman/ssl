#ifndef _PHYSICAL_CONTEXT_HPP_
#define _PHYSICAL_CONTEXT_HPP_

#include "../motor/hitbox_context.hpp"

#include "basic_ground.hpp"

class PhysicalContext
{
public:
	PhysicalContext(const HitboxContext* icontext);

	const HitboxContext& context() const;

private:
	const HitboxContext* _context;
};

#endif //_PHYSICAL_CONTEXT_HPP_
