#ifndef _CASTLE_BASE_HPP_
#define _CASTLE_BASE_HPP_

#include "../utils/vectored_map.hpp"

#include "../motor/hitbox_context.hpp"

#include "castle_face_structure.hpp"
#include "lanterns.hpp"
#include "magic_tower.hpp"
#include "magic_towers_circle.hpp"
#include "arc_bridge.hpp"
#include "arc_door.hpp"
#include "fat_tower.hpp"
#include "face_arc.hpp"
#include "curch.hpp"
#include "counter_fort.hpp"
#include "archer.hpp"
#include "watching_towers.hpp"
#include "simple_wall.hpp"
#include "castle_support.hpp"
#include "castle_simple_elevator.hpp"

#include "castle_theorical_architecture.hpp"

struct castleTools;

namespace CASTLE
{	
	struct builtStructures
	{
		Lanterns*          lanterns;
		WatchingTowers*    simple_towers;
		MagicTower*        magic_towers;
		SimpleWall*        simple_walls;
		ArcBridge*         arc_bridges;
		ArcDoor*           arc_doors;
		FatTower*          fat_towers;
		FatTower*          squares_towers;
		CastleSupport*     support;
		Church*            churchs;
		FaceArc*           face_arcs;
		CounterFort*       counter_forts;
		MagicTowersCircle* circles;
		SimpleElevator*    elevators;
	};

	struct baseParams
	{
		HitboxContext* context;
		castleTools* tools;

		float safe_distance;
		float wall_radius;
		float door_height;
		float safe_height;
		float minwidth;

		glm::vec3 color;
		glm::vec3 incolor;
	};

	/*
		Indices are indices in the mesh
	*/
	struct reliefVertex
	{
		uint bot;
		uint top;
	};
	using reliefEdge = std::pair<reliefVertex, reliefVertex>;

	class Base
	{
	public:	
		using Edge = BasicGraph::Edge;

	public:	
		Base(const TheoricalArchitecture& architecture, const baseParams& params, const builtStructures& buildings);

		void build();
		void build_floor(const std::vector<glm::vec3>& enclosure, float y);

		const baseParams& get_params() const;
		const globalArchi& get_soul() const;
		const TheoricalArchitecture& architecture() const;
		const StoredDrawableObject<glm::vec3, glm::vec3>& get_mesh() const;
		const StoredDrawableObject<glm::vec3, glm::vec3>& get_inside_mesh() const;
		StoredDrawableObject<glm::vec3, glm::vec3>& get_inside_mesh();
		const std::vector<std::unique_ptr<BasicHitbox>>&  get_hitboxes() const;

		std::vector<glm::vec3> face_out_vertices(uint face, float h);
		std::vector<glm::vec3> face_in_vertices(uint face, float h);
	
		// Keeps the orientation of the edge, indices in the mesh
		reliefEdge edge_vertices_indices(const Edge& e) const;
		edgeIndex  edge_vertices_indices(const Edge& e, uint face) const;

		std::vector<uint> face_vertices_indices(uint face) const;

		builtStructures& build_structures();
		castleTools& tools();

		void consume_magic_circle();

	private:
		std::vector<glm::vec3> face_shift_vertices(uint face, float h, float shift);
		void fill_faces();
		/*
			Returns true if all adjacent faces that are not
			ready have a greater height.
		*/
		bool ready_to_set_doors(uint face) const;
		void set_doors_structures();
		std::unique_ptr<FaceStructure> generate_face_strcture(uint face);
		/*
			The face must be ready to set doors to call this method
		*/
		void set_face_doors_positions(uint face);
		// the edges in the edgeDoorPosition can be the mirror of the asked one
		std::vector<edgeDoorPosition> edges_infos(const std::vector<Edge>& edges) const;
		void place_base_vertices();
		reliefEdge relief_edge_indices_registred(const Edge& e);
		Edge order(const Edge& e) const;

	private:
		const TheoricalArchitecture& _architecture;

		StoredDrawableObject<glm::vec3, glm::vec3> _mesh;
		StoredDrawableObject<glm::vec3, glm::vec3> _inside;
		std::vector<std::unique_ptr<BasicHitbox>>  _hbxs;

		VectoredMap<std::unique_ptr<FaceStructure>> _faces;
		std::unordered_map<Edge, edgeDoorPosition, BasicGraph::hashEdge> _placed_doors;
		std::unordered_map<Edge, reliefEdge, BasicGraph::hashEdge> _relief_edges;

		baseParams _params;
		builtStructures _built;
		globalArchi _soul;
	};
};

#endif //_CASTLE_BASE_HPP_
