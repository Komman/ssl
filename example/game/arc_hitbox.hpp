#ifndef _ARC_HITBOX_HPP_
#define _ARC_HITBOX_HPP_

#include "../motor/shaped_hitbox_fusion.hpp"
#include "../motor/paver_hitbox.hpp"

struct arcInfo
{
	glm::vec3 bottom;
	glm::vec3 sized_x;
	glm::vec3 sized_y;
	glm::vec3 sized_z;
	float thickness;
	float angle;
	int   points_amount;
};

class ArcHitbox : public ShapedHitboxFusion<arcInfo>
{
public:
	ArcHitbox(const arcInfo& shape);

private:
	void build();
};

#endif //_ARC_HITBOX_HPP_
