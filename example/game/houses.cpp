#include "houses.hpp"

#include "../utils/geom.hpp"
#include "../motor/planar_symmetry.hpp"


using namespace glm;
using namespace std;

Houses::Houses(const geomParams& params, const std::vector<glm::vec3>& dep)
	: _geom(params),
	  _pos({}, STATIC_DRAW),
	  _infos({}, STATIC_DRAW),
	  _deps(dep, STATIC_DRAW),
	  _elements({}, STATIC_DRAW),
	  _buffers({&_pos, &_infos, &_deps})
{
	this->build_base();
	this->build_roof();
	this->build_door();
}

void Houses::build_base()
{
	unsigned int start = _pos.size();

	this->add_vertex(vec3(+1.0f, 0.0f, +1.0f) * _geom.size, vec2(0.8, 0.0)); // 0
	this->add_vertex(vec3(-1.0f, 0.0f, +1.0f) * _geom.size, vec2(0.8, 0.0)); // 1
	this->add_vertex(vec3(-1.0f, 0.0f, -1.0f) * _geom.size, vec2(0.8, 0.0)); // 2
	this->add_vertex(vec3(+1.0f, 0.0f, -1.0f) * _geom.size, vec2(0.8, 0.0)); // 3

	for(unsigned int i=start; i<start+4; i++)
	{
		this->add_vertex(_pos[i] + vvv::y()*_geom.size.y, vec2(1.0, 0.0)); // 4, 5, 6, 7
	}
	
	_elements.add_hexahedron({4,5,6,7,0,1,2,3});

	this->add_vertex(geom::middle(_pos[4], _pos[5]) + vvv::y()*_geom.roof.height, vec2(1.0, 0.0)); // 8
	this->add_vertex(geom::middle(_pos[6], _pos[7]) + vvv::y()*_geom.roof.height, vec2(1.0, 0.0)); // 9

	_elements.add_triangle(4,8,5);
	_elements.add_triangle(6,9,7);
}

/*

      __---17-__
    21__---11-__20
    15___--9--__14\
    7\\          6\\
    |\\\          \\\
	| \\\  __--16-__\\
    |  \\18__--10-___19
    3   \12___--8--__13
     \   4            5
      \  |            |
       \ |            |
 latdir \|            |  
 <-----  0------------1    
          \
           \
           _\| transdir
*/

void Houses::build_roof()
{
	const auto& roof     = _geom.roof;
 
 	vec3 transdir = glm::normalize(_pos[0] - _pos[3]) * roof.overoof;
 	vec3 latdir   = glm::normalize(_pos[0] - _pos[1]) * roof.overoof;

	this->add_vertex(_pos[8] + transdir, vec2(0.5, 1.0)); // 10
	this->add_vertex(_pos[9] - transdir, vec2(0.5, 1.0)); // 11

	float a = glm::length(transdir + latdir);
	float H = roof.height;
	float l = glm::length(_pos[4] - _pos[8] + transdir + latdir);
	float desc = a * tan(asin(H/l));

	this->add_vertex(_pos[4] + transdir + latdir - vvv::y()*desc, vec2(0.5, 1.0)); // 12
	this->add_vertex(_pos[5] + transdir - latdir - vvv::y()*desc, vec2(0.5, 1.0)); // 13
	this->add_vertex(_pos[6] - transdir - latdir - vvv::y()*desc, vec2(0.5, 1.0)); // 14
	this->add_vertex(_pos[7] - transdir + latdir - vvv::y()*desc, vec2(0.5, 1.0)); // 15

	for(unsigned int i = 10; i <= 15; i++)
	{
		this->add_vertex(_pos[i] + vvv::y()*roof.width, vec2(0.6, 1.0));
		// 16, 17, 18, 19, 20, 21
	}

	_elements.add_rectangle(18,21,17,16);
	_elements.add_rectangle(16,17,20,19);

	_elements.add_rectangle(15,12,10,11);
	_elements.add_rectangle(11,10,13,14);

	_elements.add_rectangle(12,15,21,18);
	_elements.add_rectangle(13,19,20,14);

	_elements.add_rectangle(10,12,18,16);
	_elements.add_rectangle(13,10,16,19);

	_elements.add_rectangle(21,15,11,17);
	_elements.add_rectangle(17,11,14,20);
}

/*
	Door (Symmetrical)

	

   s7-------
   |s5 ------
   | |\
   | | s6  ---
   | | |s4
   | |   |
   | | | |
   | |   |
   | | | |
   | |   |
   | | | |
   s3| s2|
	s1  s0          

*/

void Houses::build_door()
{
	const auto& door  = _geom.door;
 	
 	vec3 latdir   = glm::normalize(_pos[0] - _pos[1]);
 	vec3 transdir = glm::normalize(_pos[0] - _pos[3]);
	
 	vec3 door_center = geom::middle(_pos[0], _pos[1]);

 	PlanarSymmetry sym({door_center, latdir}, &_pos, &_elements, &_infos);

	unsigned int s0 = sym.place_symmetrical(door_center + latdir*(door.width - door.extract/2.0f) + transdir*door.extract, vec2(1.0, -1.0));
	unsigned int s1 = sym.place_symmetrical(door_center + latdir*(door.width + door.extract/2.0f) + transdir*door.extract, vec2(1.0, -1.0));
	unsigned int s2 = sym.place_symmetrical(door_center + latdir*(door.width - door.extract/2.0f), vec2(0.7, -1.0));
	unsigned int s3 = sym.place_symmetrical(door_center + latdir*(door.width + door.extract/2.0f), vec2(0.7, -1.0));

	unsigned int s4 = sym.place_symmetrical(_pos[s0] + vvv::y()*(door.height - door.extract), vec2(1.0, -1.0));
	unsigned int s5 = sym.place_symmetrical(_pos[s1] + vvv::y()*door.height                 , vec2(1.0, -1.0));
	unsigned int s6 = sym.place_symmetrical(_pos[s2] + vvv::y()*(door.height - door.extract), vec2(0.7, -1.0));
	unsigned int s7 = sym.place_symmetrical(_pos[s3] + vvv::y()*door.height                 , vec2(0.7, -1.0));

	sym.link_rectangle(s0, s1, s5, s4);
	sym.link_rectangle(s7, s5, s1, s3);
	sym.link_rectangle(s6, s2, s0, s4);

	sym.link_rectangle(s4, s5, sym.symmetrical(s5), sym.symmetrical(s4));
	sym.link_rectangle(s6, s4, sym.symmetrical(s4), sym.symmetrical(s6));
	sym.link_rectangle(s5, s7, sym.symmetrical(s7), sym.symmetrical(s5));

	unsigned int s02 = sym.place_symmetrical(geom::middle(_pos[s0], _pos[s2]), vec2(1.0, -2.0));
	unsigned int s64 = sym.place_symmetrical(geom::middle(_pos[s6], _pos[s4]), vec2(1.0, -2.0)); 

	sym.link_rectangle(s02, s64, sym.symmetrical(s64), sym.symmetrical(s02));
}

unsigned int Houses::add_vertex(const glm::vec3& position, const glm::vec2& infos)
{
	unsigned int ret = _pos.size();

	_pos.add_value(position);
	_infos.add_value(infos);

	return ret;
}


const std::vector<const BasicBuffer*>& Houses::get_buffers()  const
{
	return _buffers;
}

const ElementBuffer& Houses::get_elements() const
{
	return _elements;
}

