#ifndef _MAGIC_TOWER_HPP_
#define _MAGIC_TOWER_HPP_

#include "../motor/angular_symmetry.hpp"
#include "../motor/static_lightor.hpp"
#include "../motor/vertical_shadower.hpp"
#include "../motor/hitbox_context.hpp"

#include "castle_building_inst.hpp"
#include "../motor/stored_vao_object.hpp"

using namespace ssl;

struct magicTower
{
	glm::vec3 position;
	float     height;
	float     size;
};	

class MagicTower : public StoredVAObject<CastleBuildingInst<magicTower,
							2,
							glm::vec3,
							glm::vec4,
							glm::vec4,
							float>>
{
public:
	MagicTower(HitboxContext& hbx_context, StaticLigthor& slightor, VerticalShadower& vshadower);

	void add_tower(const magicTower& tower, bool shadow = true);

protected:
	void place_building_only(const magicTower& building, float h) override;
	magicTower generate_building(const genParam& g) override;
	planarPlace  get_place(const magicTower& g) override;

private:
	enum bufferRoles {RELATIVE_POS=0, COLORS_ROOM, CENTERS_HEIGHT, SIZE};

	void build_base();
	void build_head(AngularSymmetry<glm::vec4>& sym, uint bot);
	void build_windows(AngularSymmetry<glm::vec4>& sym, float height_min, float height_max);
	void add_base_vertex(const glm::vec3& pos, const glm::vec3& color, float room);
	uint place_symmetrical(AngularSymmetry<glm::vec4>& sym, const glm::vec3& pos, const glm::vec3& color, float room);
	float room_center_height(const magicTower& tower);

private:
	StaticLigthor&    _slightor;
	VerticalShadower& _vshadower;

	uint _circlediscr;

	glm::vec3 _base_color;
	glm::vec3 _room_color;
	glm::vec3 _roof_color;
	glm::vec3 _win_color;

	float _y_fristE;
	float _y_secE;
	float _x_fit;
	float _x_ffit;
	float _roof_top_h;

	glm::vec2 _underoom;
	glm::vec2 _upperoom;
	glm::vec2 _roofext;
	glm::vec2 _roofinter;

	magicTower _generation;
};

#endif //_MAGIC_TOWER_HPP_
