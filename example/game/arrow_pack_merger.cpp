#include "arrow_pack_merger.hpp"

ArrowPackMerger::ArrowPackMerger()
	: _simple_trails(),
	  _tower_bombs()
{

}

const SimpleArrowTrailPack& ArrowPackMerger::get_simple_trails() const
{
	return _simple_trails;
}


void ArrowPackMerger::animate(const PhysicalContext& context, float dt)
{
	_simple_trails.animate(context, dt);
	_tower_bombs.animate(context, dt);
}

void ArrowPackMerger::draw(GameDrawer& game_drawer)
{
	_simple_trails.draw(game_drawer);
	_tower_bombs.draw(game_drawer);
}

void ArrowPackMerger::launch_tower_bomb(std::unique_ptr<Arrow>&& start, const glm::vec3& directionN, float speed)
{
	start->launch(start->get_position(), directionN*speed);

	towerBomb bomb = {
		.color = glm::vec3(0.0,0.5,0.0)
	};

	_tower_bombs.add_arrow(std::move(start), bomb);
}

void ArrowPackMerger::launch_simple_trail(std::unique_ptr<Arrow>&& start, const glm::vec3& directionN, float speed)
{
	start->launch(start->get_position(), directionN*speed);

	simpleArrowGeometry geo = {
		.head_coeff  = 0.1f,
		.shaft_color = glm::vec3(0.3,0.2,0.03)/1.2f,
		.head_color  = glm::vec3(0.3,0.3,0.3)
	};

	_simple_trails.add_arrow(std::move(start), geo);
}