#include "ground.hpp"

#include <glm/gtx/string_cast.hpp>

#include "../motor/gutils.hpp"
#include "../motor/primary_hitbox.hpp"
#include "../utils/geom.hpp"

#include "shinfos.hpp"

using namespace glm;
using namespace std;

Ground::Ground(const glm::vec3& position,
			   float size,
			   int cut,
			   float ymax,
			   int mountains_count,
			   float mountains_height,
			   float mountains_height_bomb,
			   float under_depth)

	: _vertexs(STATIC_DRAW),
	  _colors(STATIC_DRAW),
	  _normal_angles(STATIC_DRAW),
	  _indices(STATIC_DRAW),
	  _buffers_cache(3),
	  _position(position),
	  _size(size),
	  _cut(cut),
	  _ymax(ymax),
	  _holed(false),
	  _color_variation({}),
	  _map(_cut, std::vector<int>(_cut, 0)),
	  _hitboxes()
{
	std::vector<vec3> mountains_pics;

	for(int i=0; i<mountains_count; i++)
	{
		mountains_pics.push_back(position + vec3(
			utils::random_float(-1.0,1.0)*2.0f*_size/5.0,
			utils::random_float(0.8,1.0)*mountains_height,
			utils::random_float(-1.0,1.0)*2.0f*_size/5.0
			));
	}

	float ymin = position.y + mountains_height;
	float ymax_mount = position.y;

	for(int x=0;x<_cut;x++)
	{
		for(int y=0;y<_cut;y++)
		{
			float xcoord = position.x + float(x)/float(_cut)*_size - _size/2.0f;
			float zcoord = position.z + float(y)/float(_cut)*_size - _size/2.0f;
			float ycoord = position.y + utils::random_float(0.0, _ymax);


			if(y!=0 && x!=0 && x!=_cut-1 && y!=_cut-1)
			{
				for(auto& mount_coord : mountains_pics)
				{
					float dis = glm::distance(vec2(mount_coord.x, mount_coord.z), vec2(xcoord, zcoord));
					float mountains_coeff = 1.0/(1.0+dis*dis/mountains_height_bomb/mountains_height_bomb);
					ycoord+=mountains_coeff*(mount_coord.y-position.y);
				}
			}
			else
			{
				ycoord = position.y;
			}


			_vertexs.add_value(vec3(xcoord, ycoord, zcoord));

			if(ycoord<ymin)
			{
				ymin=ycoord;
			}
			if(ycoord>ymax_mount)
			{
				ymax_mount=ycoord;
			}

			if(x>0 && y>0)
			{
				int array_coord = x*_cut + y;
				_indices.add_rectangle(array_coord - _cut - 1,
									   array_coord - _cut,
									   array_coord,
									   array_coord - 1);
			}
		}
	}

	_ymax += mountains_height;


	this->init_colors();

	vec3 sub_ymin(0.0, ymin-position.y, 0.0);
	for(uint i=0;i<_vertexs.size();i++)
	{
		_vertexs.change_subvalue(i, _vertexs.get_subvalue(i)-sub_ymin);

		float ycoeff = (_vertexs.get_subvalue(i).y-position.y)/(ymax_mount-ymin);
		vec4 color=this->get_color_ground(ycoeff);

		_colors.add_value(color);
	}

	for(uint i=0;i<_vertexs.size();i++)
	{
		ivec2 coord = this->index_to_coord(i);

		if(coord.x*coord.y == 0 || coord.x == _cut-1 || coord.y == _cut-1)
		{
			_normal_angles.add_value(vec4(1));
		}
		else
		{
			vec4 angles;

			vec2 vertexX = vec2(_vertexs[i].x, _vertexs[i].y);
			vec2 vertexZ = vec2(_vertexs[i].z, _vertexs[i].y);

			const auto& prevX = _vertexs[this->coord_to_index(coord + ivec2(-1, 0))];
			const auto& prevZ = _vertexs[this->coord_to_index(coord + ivec2( 0,-1))];
			const auto& nextX = _vertexs[this->coord_to_index(coord + ivec2( 1, 0))];
			const auto& nextZ = _vertexs[this->coord_to_index(coord + ivec2( 0, 1))];

			vec2 vertex_prev_X = vec2(prevX.x, prevX.y);
			vec2 vertex_prev_Z = vec2(prevZ.z, prevZ.y);
			vec2 vertex_next_X = vec2(nextX.x, nextX.y);
			vec2 vertex_next_Z = vec2(nextZ.z, nextZ.y);

			vec2 dirXprev = vertexX - vertex_prev_X;
			vec2 dirZprev = vertexZ - vertex_prev_Z;
			vec2 dirXnext = vertex_next_X - vertexX;
			vec2 dirZnext = vertex_next_Z - vertexZ;

			vec2 normalXprev = glm::normalize(vec2(-dirXprev.y, dirXprev.x));
			vec2 normalZprev = glm::normalize(vec2(-dirZprev.y, dirZprev.x));
			vec2 normalXnext = glm::normalize(vec2(-dirXnext.y, dirXnext.x));
			vec2 normalZnext = glm::normalize(vec2(-dirZnext.y, dirZnext.x));

			vec2 normalX = glm::normalize((normalXprev + normalXnext)/2.0f);
			vec2 normalZ = glm::normalize((normalZprev + normalZnext)/2.0f);

			// cout<<to_string(vertex_prev_Z)
			// <<" -> "<<to_string(vertexZ)
			// <<" -> "<<to_string(vertex_next_Z)
			// <<" =path1=> "<<to_string(dirZprev)
			// <<" =path2=> "<<to_string(dirZnext)
			// <<" ====> "<<to_string(normalZ)
			// <<endl;

			angles.x = normalX.x;
			angles.y = normalX.y;
			angles.z = normalZ.x;
			angles.w = normalZ.y;

			_normal_angles.add_value(angles);
		}
	}

	// exit(0);

	_vertexs.add_value(position + vec3(0.0, -under_depth, 0.0));
	_colors.add_value(vec4(dark(BROWN),1.0));
	_normal_angles.add_value(vec4(-float(M_PI)));

	_indices.add_value(_vertexs.size()-1);
	_indices.add_value(_cut-1);
	_indices.add_value(0);

	_indices.add_value(_vertexs.size()-1);
	_indices.add_value(_cut*_cut-1);
	_indices.add_value(_cut-1);

	_indices.add_value(_vertexs.size()-1);
	_indices.add_value(_cut*(_cut-1));
	_indices.add_value(_cut*_cut-1);

	_indices.add_value(_vertexs.size()-1);
	_indices.add_value(0);
	_indices.add_value(_cut*(_cut-1));
}

void Ground::init_colors()
{
	vec3 wheet_col = vec3(0.1, 0.4, 0.1);

	_color_variation.add_value(0.3, wheet_col*0.8f);
	_color_variation.add_value(0.8, wheet_col);
	_color_variation.add_value(0.5, vec3(1.0, 1.0, 1.0)*0.4f);
	_color_variation.add_value(0.0, vec3(1.0, 1.0, 1.0)*0.5f);

	_color_variation.force_total_duration(1.0f);
}

void Ground::nimportequoi()
{
	for(int i=0;i<(int)_vertexs.size();i++)
	{
		_vertexs.change_subvalue(i, _vertexs[i]+vvv::y()*float(i)*window::delta_time()/10.0f);
	}
}

glm::vec4 Ground::get_color_ground(float ycoeff) const
{
	// return vec4(vec3(glm::normalize(vec3(

	// 									ycoeff*ycoeff*utils::random_float(5.0,15.0) + utils::random_float(0.2,0.3)/(0.01+ycoeff),
	// 									utils::random_float(0.5,0.8)/(0.01+ycoeff),
	// 									ycoeff*ycoeff*8.0f + utils::random_float(0.3,0.35)/(0.01+ycoeff)
									
	// 								)))/(2.0f-ycoeff/2.0f), 1.0f);

	return vec4(_color_variation.evaluate(ycoeff) + utils::random_vec3(vec3(-1.0f),vec3(1.0f))/30.0f, 1.0f);
}

// void Ground::dark_color(const glm::vec3& position, float coeff)
// {
// 	float x = position.x - _position.x;
// 	float z = position.z - _position.z;

// 	ivec2 icoord = ivec2((int)((x +_size/2.0f)/_size*float(_cut)),
// 						 (int)((z +_size/2.0f)/_size*float(_cut)));

// 	int lin_coord = icoord.x*_cut + icoord.y;

// 	int coords[3]={
// 		lin_coord,
// 		lin_coord+1,
// 		lin_coord+int(_cut)
// 	};

// 	float invd[3];

// 	for(int i=0; i<3; i++)
// 	{
// 		float d = glm::distance(_vertexs[coords[i]], position);
// 		invd[i] = 1.0f/(d/100.0f+1.0f);
// 		_colors.change_subvalue(coords[i], _colors[coords[i]]*invd[i]);
// 	}
// }


float Ground::get_y(float x, float z) const
{
	x -= _position.x;
	z -= _position.z;

	ivec2 icoord = ivec2((int)((x +_size/2.0)/_size*float(_cut)),
						 (int)((z +_size/2.0)/_size*float(_cut)));
	int lin_coord = icoord.x*_cut + icoord.y;
	float case_l = _size/float(_cut);

	//cout<<"i:("<<icoord.x<<" ";
	//cout<<icoord.x<<"/"<<_cut<<" ";
	//cout<<icoord.y<<"/"<<_cut<<endl;

	if(icoord.x<0 || icoord.y<0 || icoord.x>=_cut-1 || icoord.y>=_cut-1)
	{
		// war("out of ground");
		return _position.y;
	}
	// if(   _map[std::min(icoord.x+0, _cut-1)][std::min(icoord.y+0, _cut-1)] >= 1
	//    || _map[std::min(icoord.x+1, _cut-1)][std::min(icoord.y+0, _cut-1)] >= 1
	//    || _map[std::min(icoord.x+0, _cut-1)][std::min(icoord.y+1, _cut-1)] >= 1
	//    || _map[std::min(icoord.x+1, _cut-1)][std::min(icoord.y+1, _cut-1)] >= 1)
	// {
	// 	return _position.y;
	// }

	int ids[3];

	ids[0]=0;

	//cout<<(x+_size/2.0)/case_l<<" "<< float(icoord.x)<<" ";
	//cout<<(z+_size/2.0)/case_l<<" "<<float(icoord.y)<<"   ";

	float localx = x+_size/2.0-float(icoord.x)*case_l;
	float localz = z+_size/2.0-float(icoord.y)*case_l;

	//cout<<"lcl:("<<localx<<" "<<localz<<") ";
	bool trigo_triangle = localx > localz;

	if(trigo_triangle)
	{
		//cout<<" r ";
		ids[1]=_cut+1;
		ids[2]=_cut;
	}
	else
	{
		//cout<<" l ";
		ids[1]=_cut+1;
		ids[2]=1;


	}

	//cout<<lin_coord;

		//cout<<" choosen: ";
	float ret = 0.0;


	vec3 triangle_xyz[3];
	float triangle_y[3];

	for(int i=0;i<3;i++)
	{
		triangle_xyz[i]=_vertexs.get_subvalue(lin_coord + ids[i]);
	}
	for(int i=0;i<3;i++)
	{
		triangle_y[i]=triangle_xyz[i].y;
		//triangle_xyz[i]+=0.4f;
	}

	// ist.set_multicolor({vec4(dark(RED), 1.0), vec4(dark(dark(RED)), 1.0), vec4(dark(dark(dark(RED))), 1.0)});
	// if(trigo_triangle)
	// {
	// 	ist.triangle(triangle_xyz[0], triangle_xyz[1], triangle_xyz[2]);
	// }
	// else
	// {
	// 	ist.triangle(triangle_xyz[1], triangle_xyz[0], triangle_xyz[2]);
	// }
	

	vec2 relative_coord = vec2(localx/case_l, localz/case_l);

	//cout<<"relv:("<<relative_coord.x<<" "<<relative_coord.y<<") "<<endl;
	float hypoternus_interpol = abs(glm::dot(relative_coord, glm::normalize(vec2(1.0f, 1.0f))));
	float divid_interpol      = abs(glm::dot(relative_coord, glm::normalize(vec2(-1.0f, 1.0f))));
	hypoternus_interpol=(hypoternus_interpol-divid_interpol)/(sqrt(2.0)-2.0*divid_interpol);
	divid_interpol*=sqrt(2.0);

	//cout<<divid_interpol<<" ";
	//cout<<hypoternus_interpol<<endl;
	//cout<<divid_interpol<<endl;

	float interpol[2];
	interpol[0]=triangle_y[0]*(1.0-divid_interpol) + triangle_y[2]*divid_interpol;
	interpol[1]=triangle_y[1]*(1.0-divid_interpol) + triangle_y[2]*divid_interpol;

	ret = interpol[0]*(1.0-hypoternus_interpol) + interpol[1]*hypoternus_interpol;

	// vec3 final_pos = vec3(x, ret, z);
	// ist.set_multicolor({vec4((GREEN), 1.0), vec4((dark(GREEN)), 1.0), vec4((dark(dark(GREEN))), 1.0)});
	// ist.ortho_square(final_pos, 1.0);

	return ret;
}

const std::vector<const BasicBuffer*>& Ground::get_buffers() const
{
	_buffers_cache[0] = &_vertexs;
	_buffers_cache[1] = &_colors;
	_buffers_cache[2] = &_normal_angles;

	return _buffers_cache;
}

const ElementBuffer& Ground::get_elements() const
{
	return _indices;
}

float Ground::size() const
{
	return _size;
}

int Ground::map_size() const
{
	return _cut;
}

#include "effects.hpp"

void Ground::animate(float dt)
{
	for(auto& v :_test)
	{
		if(utils::k_chance_on_n(1,30))
		effects::add_debug_disc(this->get_xyz(v.x, v.y) + vvv::y()*5.0f, vec3(1,0,0), 1.0f);
	}
	for(auto& v :_test2)
	{
		if(utils::k_chance_on_n(1,30))
		effects::add_debug_disc(v + vvv::y()*5.0f, vec3(0,0,1), 1.0f);
	}
}

glm::uvec2 Ground::index_to_coord(uint index) const
{
	return vec2(index/_cut, index%_cut);
}

uint Ground::coord_to_index(const glm::uvec2& coord) const
{
	return coord.x * _cut + coord.y;
}

glm::uvec2 Ground::point_to_coord(glm::vec3 point) const
{
	return this->point_to_coord(vec2(point.x, point.z));
}

glm::vec3 Ground::coord_to_point(const glm::uvec2& coord) const
{
	float case_l = _size/float(_cut);
	return this->get_xyz(_position - vec3(_size, 0.0f, _size)/2.0f + vec3(coord.x, 0.0f, coord.y)*case_l);
}

uint Ground::point_to_index(const glm::vec2& point) const
{
	return this->coord_to_index(this->point_to_coord(point));
}

uint Ground::point_to_index(const glm::vec3& point) const
{
	return this->point_to_index(vec2(point.x, point.z));
}

glm::uvec2 Ground::point_to_coord(glm::vec2 point) const
{
	point.x -= _position.x;
	point.y -= _position.z;

	ivec2 icoord = ivec2((int)((point.x +_size/2.0)/_size*float(_cut)),
				  		 (int)((point.y +_size/2.0)/_size*float(_cut)));

	if(icoord.x<0 || icoord.y<0 || icoord.x>=_cut-1 || icoord.y>=_cut-1)
	{
		err("Ground::point_to_coord(): Point out of the ground");
	}

	return uvec2(icoord);
}

void Ground::mark_wall(const glm::vec2&  point)
{
	this->mark_wall(this->point_to_coord(point));
}

void Ground::mark_wall(const glm::vec3&  point)
{
	this->mark_wall(vec2(point.x, point.z));
}

void Ground::mark_wall(const glm::uvec2& coord)
{
	_colors.change_subvalue(this->coord_to_index(coord), vec4(1.0f));
}

glm::uvec2 Ground::inter_coord(const glm::vec2& inter_point, StraightLine::singleAxis inter_axis, bool top_or_bot)
{
	vec2 dep(0,0);
	uvec2 udep(0,0);
	float safe_dep = _size/float(_cut)/2.0f;

	if(inter_axis == StraightLine::X_AXIS)
	{
		dep.y = safe_dep;
		udep.x = 1;
	}
	else
	{
		dep.x = safe_dep;
		udep.y = 1;
	}

	return this->point_to_coord(inter_point + dep) + udep*(top_or_bot ? 1u : 0u);
}

void Ground::mark_line(std::vector<std::vector<int>>& map, glm::vec2 start,  glm::vec2 dst)
{
	float case_l = _size/float(_cut);

	ivec2 dir              = geom::unit_direction(start, dst);
	vec2  dirf             = vec2(dir);
	vec2  intersections[2] = {start, start};
	uvec2 start_coord      = this->point_to_coord(start);
	
	StraightLine traj(start, dst);
	StraightLine::singleAxis line_axis[2] = {StraightLine::Y_AXIS, StraightLine::X_AXIS};

	// bool first = true;
	vec2 mx;

	for(uint axis = 0; axis<2; axis++)
	{
		mx[axis] = _position[axis] - _size/2.0f + float(start_coord[axis] + ((dir[axis]==1) ? 1 : 0))*case_l;
	}

	float segdist = glm::distance(start, dst);

	for(int iter=0; iter <= 2*_cut; iter++)
	{
		StraightLine line[2] = {
			StraightLine(line_axis[0], mx[0]),
			StraightLine(line_axis[1], mx[1])
		};

		vec2 distances;
		for(uint axis = 0; axis<2; axis++)
		{
			if(!traj.is_parrallel(line[axis])) 
			{
				intersections[axis] = traj && line[axis];
				distances[axis]     = glm::distance(start, intersections[axis]);
   			}
			else
			{
				intersections[axis] = dst;
				distances[axis]     = 2.0f*segdist; // 2x for not having floating point precision problems
			}
		}

		uint saxis = (distances.x < distances.y) ? 0 : 1;

		uvec2 inter_corner1 = this->inter_coord(intersections[saxis], line_axis[saxis], false);
		uvec2 inter_corner2 = this->inter_coord(intersections[saxis], line_axis[saxis], true);
		
		// _test.push_back(intersections[saxis]);
		// _test2.push_back(this->coord_to_point(inter_corner1));
		// _test2.push_back(this->coord_to_point(inter_corner2));

		// auto new_vertex = this->get_xyz(intersections[saxis]);
		// _vertexs.change_subvalue(this->coord_to_index(inter_corner1), first ? this->get_xyz(start.x, start.y) : new_vertex);
		// _vertexs.change_subvalue(this->coord_to_index(inter_corner2), first ? this->get_xyz(start.x, start.y) : new_vertex);
		
		map[inter_corner1.x][inter_corner1.y] = 1;
		map[inter_corner2.x][inter_corner2.y] = 1;

		mx[saxis] += case_l * dirf[saxis];
		// first = false;

		if(iter == 2*_cut)
		{
			ssl::err("Ground::mark_line(): too many iteration (theorically impossible)");
		}

		if(distances.x > segdist && distances.y > segdist)
		{
			// cout<<"iter: "<<iter<<" start:"<<to_string(start)<<" & dst:"<<to_string(dst)<<endl;
			break;
		}
	}
}

void Ground::hole_circular(const std::vector<glm::vec2>& points2D, const glm::vec3& center)
{
	if(_holed)
	{
		ssl::err("Ground::hole_circular() already holed (can be holed only one time)");
	}

	for(int x=0; x<_cut; x++)
	{
		for(int y=0; y<_cut; y++)
		{
			_map[x][y] = 0;
		}
	}

	for(int i=0; i<(int)points2D.size(); i++)
	{
		vec2 check = points2D[i] - vec2(_position.x, _position.z) + vec2(_size/2.0f);
		if(check.x > _size
		|| check.y > _size
		|| check.x < 0.0f
		|| check.y < 0.0f)
		{
			err("Ground::hole_circular() asked for a point that is not in the ground");
		}
	}

	for(int i=0; i<(int)points2D.size(); i++)
	{
		this->mark_line(_map,
						points2D[i],
						points2D[(i+1)%((int)points2D.size())]);
	}
	
	uvec2 ucenter = this->point_to_coord(vec2(center.x, center.z));

	// utils::print_vecvector_niceint(_map);
	utils::fill_bordered_map(_map, 2, 1, ucenter);
	// utils::print_vecvector_niceint(_map);

	for(int x=1; x<_cut-1;x++)
	{
		for(int y=1; y<_cut-1;y++)
		{
			if(    _map[x+0][y+0] == 0 &&
				(( _map[x+1][y+0] == 1
				&& _map[x-1][y+0] == 1) ||
				(  _map[x+0][y+1] == 1
				&& _map[x+0][y-1] == 1)))
			{
				_map[x][y] = 2;
			}
		}
	}

	this->delete_marked_ground(_map, center);

	_holed = true;
}

void Ground::delete_marked_ground(const std::vector<std::vector<int>>& map, const glm::vec3& center)
{
	for(int i=0; i<6*(_cut-1)*(_cut-1); i+=3)
	{
		uint count = 0;
		uint count2 = 0;
		for(int j=i; j<i+3; j++)
		{
			if(_indices[j] < _cut*_cut)
			{
				glm::uvec2 pos = this->index_to_coord(_indices[j]);

				if(map[pos.x][pos.y] != 0)
				{
					count++;
				}
				if(map[pos.x][pos.y] == 2)
				{
					count2++;
				}
			}
		}

		if(count >= 1)
		{
			_vertexs.change_subvalue(_indices[i+0], _vertexs[_indices[i+0]] - vvv::y()*(_vertexs[_indices[i+0]].y - center.y));
			_vertexs.change_subvalue(_indices[i+1], _vertexs[_indices[i+1]] - vvv::y()*(_vertexs[_indices[i+1]].y - center.y));
			_vertexs.change_subvalue(_indices[i+2], _vertexs[_indices[i+2]] - vvv::y()*(_vertexs[_indices[i+2]].y - center.y));
		}
		if(count2 >= 3)
		{
			_indices.change_subvalue(i+0, 0);
			_indices.change_subvalue(i+1, 0);
			_indices.change_subvalue(i+2, 0);
		}
	}

	for(int i=0; i<(int)_indices.size(); i+=3)
	{
		if(   _indices[i+0]==0
		   && _indices[i+1]==0
		   && _indices[i+2]==0)
		{
			_indices.change_subvalue(i+0, _indices[_indices.size() - 3]);
			_indices.change_subvalue(i+1, _indices[_indices.size() - 2]);
			_indices.change_subvalue(i+2, _indices[_indices.size() - 1]);
			
			_indices.pop_value();
			_indices.pop_value();
			_indices.pop_value();

			i-=3;
		}
	}
}

void Ground::generate_hitboxes()
{
	_hitboxes.clear();

	for(int x=0;x<_cut-1;x++)
	{
		for(int y=0;y<_cut-1;y++)
		{
			uint i  = this->coord_to_index(uvec2(x,y));
			uint ip = this->coord_to_index(uvec2(x+1,y+1));
			uint ix = this->coord_to_index(uvec2(x+1,y));
			uint iy = this->coord_to_index(uvec2(x,y+1));

			prismInfo p{.points = {
				_position+_vertexs[ix],
				_position+_vertexs[i],
				_position+_vertexs[ip],
				vec3(0)
			}};
			p.points[3] = (p.points[0]+p.points[1]+p.points[2])/3.0f - vvv::y()*2.0f;

			auto hbx1 = std::make_unique<PrismHitbox>(p);

			p.points[0] = _position+_vertexs[iy];
			p.points[3] = (p.points[0]+p.points[1]+p.points[2])/3.0f - vvv::y()*2.0f;

			auto hbx2 = std::make_unique<PrismHitbox>(p);

			_hitboxes.push_back(std::move(hbx1));
			_hitboxes.push_back(std::move(hbx2));
		}
	}
}


int Ground::castled_map_code(const glm::vec3& position) const
{
	glm::uvec2 coord = this->point_to_coord(position);
	return _map[coord.x][coord.y];
}

const glm::vec3& Ground::get_center() const
{
	return _position;
}

float Ground::get_ymax() const
{
	return _ymax;
}

const std::vector<BasicHitbox::storedHitbox>& Ground::get_hitboxes() const
{
	return _hitboxes;
}


