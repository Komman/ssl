#ifndef _CASTLE_BUILDING_PLACER_HPP_
#define _CASTLE_BUILDING_PLACER_HPP_

#include "castle_building.hpp"

template<typename BuildingInfo>
class CastleBuildingPlacer : public CastleBuilding
{
public:
	CastleBuildingPlacer(const castleGridPlacing& grid_draw);

	void generate(const genParam& g) override;

	void place(SpaceGrid& spacegrid, float h) override;
	void place_building_only(float h) override;

	planarPlace get_generated_place() const override;

protected:	
	virtual BuildingInfo generate_building(const genParam& g) =0;
	virtual planarPlace  get_place(const BuildingInfo& g) =0;
	virtual void place_building_only(const BuildingInfo& building, float h) =0;

	void not_generated_error() const;

private:
	planarPlace  _place;
	BuildingInfo _building;

	bool _first_generated;
};






template<typename BuildingInfo>
CastleBuildingPlacer<BuildingInfo>::CastleBuildingPlacer(const castleGridPlacing& grid_draw)
	: CastleBuilding(grid_draw),
	  _first_generated(false)
{

}

template<typename BuildingInfo>
planarPlace CastleBuildingPlacer<BuildingInfo>::get_generated_place() const
{
	if(!_first_generated)
	{
		mot::err("Try to get the generated place while not generated");
	}

	return _place;
}


template<typename BuildingInfo>
void CastleBuildingPlacer<BuildingInfo>::generate(const genParam& g)
{
	_building = this->generate_building(g);
	_place    = this->get_place(_building);

	_first_generated = true;
}


template<typename BuildingInfo>
void CastleBuildingPlacer<BuildingInfo>::place(SpaceGrid& spacegrid, float h)
{
	if(!_first_generated)
	{
		this->not_generated_error();
	}

	this->place_grid_only(spacegrid, _place);
	this->place_building_only(h);
}

template<typename BuildingInfo>
void CastleBuildingPlacer<BuildingInfo>::place_building_only(float h)
{
	if(!_first_generated)
	{
		this->not_generated_error();
	}

	this->place_building_only(_building, h);
}

template<typename BuildingInfo>
void CastleBuildingPlacer<BuildingInfo>::not_generated_error() const
{
	mot::err("Try to build while not generated");
}


#endif //_CASTLE_BUILDING_PLACER_HPP_
