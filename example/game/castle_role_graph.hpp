#ifndef _CASTLE_ROLE_GRAPH_HPP_
#define _CASTLE_ROLE_GRAPH_HPP_

#include <memory>

#include "../motor/stored_face_graph.hpp"
#include "../motor/dual_multi_graph.hpp"

#include "castle_role.hpp"

namespace CASTLE
{
	struct faceInfo
	{
		float height;
		std::unique_ptr<CASTLE::BasicRole> structure;
	};

	struct graphHeightParam
	{
		float floor_h; // min height
		float min_roof_h; // min height
		float door_height; // min height
		float ND; // distance coeff

		glm::vec2 center;
	};


	class RoleGraph : public StoredFaceGraph<DualMultiGraph, faceInfo> 
	{
	public:
		using Structure = std::unique_ptr<CASTLE::BasicRole>;

	public:
		RoleGraph(const BorderedGraph& g, const graphHeightParam& params);

		void set_height(uint face, float h);
		void set_structure(uint face, Structure&& structure);

		float get_height(uint face) const;
		CASTLE::BasicRole* get_structure(uint face) const;

		const glm::vec2& get_center() const;
		// return the face that is lower, and the higher
		Edge faces_orientation(uint face1, uint face2) const;
		Edge faces_orientation(const Edge& face_edge) const;

		const graphHeightParam& geom_params() const;
		float get_min_door_h() const;

		std::pair<float, float> inborder_face_adjacent_minmaxhights(uint face) const;

		bool ascending_face_edge(const Edge& face_edge) const;
		bool descending_edge(const Edge& e, uint face) const;
		float ascending_height(const Edge& e, uint face) const;
		bool place_for_ext_door(uint face1, uint face2) const;
		// Edges are a subset of the face edges cycle in trigonometric order
		std::vector<Edge> descending_adjacent_edges(uint face) const;
		std::vector<Edge> ascending_adjacent_edges(uint face) const;

	public:


	private:
		void give_heights();
		void give_roles();
		void adapt_height_to_roles();

	private:
		graphHeightParam _params;

		float _heights_variance;
	};
};

#endif //_CASTLE_ROLE_GRAPH_HPP_
