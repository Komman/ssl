#ifndef _CASTLE_INSIDE_FACE_HPP_
#define _CASTLE_INSIDE_FACE_HPP_

#include "../motor/stored_drawable_object.hpp"
#include "../motor/hitbox_context.hpp"

#include "castle_theorical_architecture.hpp"

namespace CASTLE
{
	struct edgeDoorPosition
	{
		BasicGraph::Edge edge;
		float altitude;
		// From 0 to 1, position on the segment [edge.first -> edge.second]
		float position;
		float height;
		float width;
	};

	struct insideDoor
	{
		bool door;
		doorType type;
		edgeDoorPosition pos;
	};

	struct edgeIndex
	{
		uint first;
		uint second;
	};

	struct indexedInWall
	{
		insideDoor door;
		edgeIndex  edgei;
	};

	struct walledInside
	{
		// cycles
		std::vector<uint> bot;
		std::vector<uint> top;
	};

	struct doorFullInfo
	{
		edgeDoorPosition door;
		Segment3D edge;
		Segment3D bottom;
		Segment3D top;
		glm::vec3 dirdoorN;
		glm::vec3 centerbot;
	};

	class FaceStructure;
	class Base;

	class InsideFace
	{
	public:
		InsideFace(const std::vector<insideDoor>& doors_to_set, FaceStructure* outside);
		void build();

	// virtual methods
	protected:
		virtual void build(const std::vector<insideDoor>& doors_to_set);


	//tools methods
	protected:
		Base* get_base() const;
		ssl::ArrayBuffer<glm::vec3>& mesh();
		ssl::ArrayBuffer<glm::vec3>& colors();
		ssl::ElementBuffer& elements();
		const glm::vec3& inside_color() const;
		void complete_color_buffer(const glm::vec3& color, float randomness = 0.0f);
		void check_buffer_sizes();
		std::vector<indexedInWall> wall_inside_base_vertices(const std::vector<insideDoor>& doors_to_set, float height, float wall_size);
		float floor_h() const;
		float height() const;
		float wall_radius() const;
		indexedInWall build_inside_wall_facade(const indexedInWall& wall, float height, const glm::vec3& color);
		indexedInWall build_inside_cut_wall_facade(const indexedInWall& wall, float cut_center_coeff, float cut_width, float height, const glm::vec3& color, bool fill_cutfloor);
		// Returns the same as input but where edges indieces are the top of the wall placed
		std::vector<indexedInWall> build_inside_walls(const std::vector<indexedInWall>& walls, float height, const glm::vec3& color);
		walledInside build_walls(const std::vector<insideDoor>& doors_to_set, float h);
		void add_hitbox(std::unique_ptr<BasicHitbox>&& hbx);
		template<typename HitboxType, typename... ConstructorArgs>
		void add_hitbox(ConstructorArgs&&... args);
		doorFullInfo compute_infos(const insideDoor& door);

		FaceStructure* outside();

	private:
		FaceStructure* _outside;

		std::vector<insideDoor> _doors_to_set;
	};





	template<typename HitboxType, typename... ConstructorArgs>
	void InsideFace::add_hitbox(ConstructorArgs&&... args)
	{
		this->add_hitbox(std::make_unique<HitboxType>(args...));
	}
};

#endif //_CASTLE_INSIDE_FACE_HPP_
