#ifndef _TOWER_BOMBS_HPP_
#define _TOWER_BOMBS_HPP_

#include "drawable_arrow_pack.hpp"


struct towerBomb
{
	glm::vec3 color;
};

class TowerBombs : public DrawableArrowPack<towerBomb,
											3,
											glm::vec3, // REL_POS
											glm::vec3, // COLORS
											glm::vec3, // BLOOM_COLORS
											glm::vec4, // CENTERS & SIZE_Y
											glm::vec4, // COLORS  & SIZE_XZ
											glm::vec3> // DIRECTIONN
{
public:
	TowerBombs();

public:
	enum bufferRole {RELATIVE_POS=0, COLOR, BLOOM_COLOR, CENTER};

protected:
	InstancesTuple buffer_filling(const Arrow& arrow, const towerBomb& look) const override;
	GameDrawer::gameDrawTypes get_typed_draw() const override;
	/*
		NOT THREAD SAFE
	*/
	Drawer& get_drawer() const override;

private:

};

#endif //_TOWER_BOMBS_HPP_
