#ifndef _COUNTER_FORT_HPP_
#define _COUNTER_FORT_HPP_

#include "../motor/planar_building_instance.hpp"
#include "../motor/vertical_shadower.hpp"
#include "../motor/hitbox_fusion.hpp"
#include "../motor/stored_vao_object.hpp"

struct counterFort
{
	glm::vec3 bot_center;
	float size;
	float height_startgirder;
	glm::vec3 endgirder;
};

// The size.y if the descending height
class CounterFort : public StoredVAObject<PlanarBuildingInstances<glm::vec3, glm::vec3>>
{
public:
	CounterFort(HitboxContext& contex, VerticalShadower* vshadower, const counterFort& geometry, const glm::vec3& color);

	const counterFort& get_geometry() const;

protected:
	std::unique_ptr<BasicHitbox> build_hitbox(const planarYPlace& place) const override;
	void add_instance_AO(const planarYPlace& place) override;

private:
	enum buffersRole {RELATIVE_POS=0, COLOR};
	
	void build_base();
	glm::vec3 underbluff_pos() const;

private:
	counterFort _geometry;
	glm::vec3 _color;
	HitboxFusion _normal_hitbox;

	VerticalShadower* _vshadower;
};

#endif //_COUNTER_FORT_HPP_
