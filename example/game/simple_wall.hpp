#ifndef _SIMPLE_WALL_HPP_
#define _SIMPLE_WALL_HPP_

#include "../motor/physical_instances.hpp"
#include "../motor/vertical_shadower.hpp"
#include "../motor/hitbox_context.hpp"
#include "../motor/paver_hitbox.hpp"
#include "../motor/stored_vao_object.hpp"

using namespace ssl;

struct simpleWall
{
	glm::vec3 start;
	glm::vec3 dst;
	float     height;
};

class SimpleWall : public StoredVAObject<PhysicalInstances<2,
							glm::vec3,
							glm::vec3,
							glm::vec4,
							glm::vec4>>
{
public:
	enum bufferRoles {RELATIVE_POS=0, COLORS, CENTERS_HEIGHT, ORIENTATION_N_LENGTH};

public:
	SimpleWall(HitboxContext& hbx_context, VerticalShadower& vshadower);

	void add_wall(const simpleWall& wall);

private:
	void build_relative_base();
	uint add_base_vertex(const glm::vec3& pos, const glm::vec3& color);
	void build_niches(const glm::vec3 starts[4], const glm::vec3 dsts[4], uint corner_left, uint middle);

private:
	VerticalShadower& _vshadower;

	glm::vec3 _wall_color;

	float _size;
	float _niche_size;
	uint  _niche_count;
};


#endif //_SIMPLE_WALL_HPP_
