#ifndef _BASIC_UNIFORM_HPP_
#define _BASIC_UNIFORM_HPP_

#include <GL/glew.h>

#include "shader_builder.hpp"
#include "texture.hpp"
#include "basic_uniform_slot.hpp"

namespace ssl
{
	class BasicUniform
	{
	public:
		//static
		static constexpr int SINGLE_ELEMENT = ShaderParser::ARRAY_COUNT_SINGLE_ELEMENT;

		struct reference
		{
			uint slot_indice;
			uint controller_indice;
		};

		/*
			THE METHOD set_slot() MUST BE CALL BY THE UNIFORM THAT
		 	IMPLEMENTS THE METHOD get_instance() AT ITS INITIALIZATION
		 */
		void set_slot();

		static std::vector<reference> add_shader_uniforms(GLuint shader_ID, const std::vector<std::string>& uniform_names); 
		static void update_uniforms_data(const std::vector<reference>& uniform_references);
		static void print_slots();
		static std::string get_uniform_name(const reference& ref);

		template<typename TextureType>
		static void change_texture_slot_binding(const reference& texture_binding, const Texture<TextureType>& new_texture);

	protected:

		virtual BasicUniformSlot* get_instance()=0;

		BasicUniform(const uniformInfo& infos);

		uint get_indice() const;

	private:
		int _indice;

		// global uniform slot storage
		static std::vector<BasicUniformSlot*> all_slots; 
	};
};



namespace ssl
{
	template<typename TextureType>
	void BasicUniform::change_texture_slot_binding(const reference& texture_binding, const Texture<TextureType>& new_texture)
	{
		all_slots[texture_binding.slot_indice]->change_texture_controller_binding(texture_binding.controller_indice, new_texture);
	}

};

#endif //_BASIC_UNIFORM_HPP_
