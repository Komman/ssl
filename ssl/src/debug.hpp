#ifndef _DEBUG_HPP_
#define _DEBUG_HPP_

#include "color.hpp"
#include "utils.hpp"

#include <string>

#define DEBUG
// Prints trace of opengl mains calls
#define SSL_PRINTS
// Add checking operations that does change the function's mathematical complexity
// for example, a O(1) function coud takes a O(n) time
#define SSL_EXPENSIVE_DEBUG

#define GRAPH_DEBUG


#ifdef DEBUG
// Add checking operations that doesn't change the function's mathematical complexity
#define SSL_DEBUG
#endif

#ifdef SSL_DEBUG
#define SSL_GL_DEBUG
#endif


namespace ssl
{
	class ssl_error : public std::exception
	{
	public:
		const char * what() const noexcept override;
	};

	enum SSLPrintTypes {SSL_PRINTS_DRAW, SSL_PRINTS_MEMORY, SSL_PRINTS_MEMORY_FLOW, SSL_PRINTS_PARAMS_SETTING, SSL_PRINTS_LAZY_OP,
						SSL_PRINTS_TYPES_AMOUNT};

	static inline std::string ERR_COLOR    = TERM::ORANGE;
	static inline std::string WAR_COLOR    = TERM::CYAN;
	static inline std::string PRINTS_COLOR = TERM::BLUE;

	void err(const std::string& msg);
	void war(const std::string& msg);
	void msg(const std::string& msg);
	void enable_prints(SSLPrintTypes print_type);
	void disable_prints(SSLPrintTypes print_type);
	bool are_prints_enable(SSLPrintTypes print_type);
	void prints_msg(SSLPrintTypes print_type, const std::string& msg);
	void bouh();

	std::string opengl_error_code(uint error_code);
};

#ifdef SSL_GL_DEBUG
#define GL(instr) instr; {uint ssl_err_code = glGetError(); if(ssl_err_code != GL_NO_ERROR) {err("OPENGL ERROR: " + SSL_FILE_AND_LINE + ssl::TERM::NOCOL +  "\n" + std::string(#instr) + "\nError code : " + ssl::opengl_error_code(ssl_err_code));}}
#else
#define GL(instr) instr
#endif

#endif //_DEBUG_HPP_
