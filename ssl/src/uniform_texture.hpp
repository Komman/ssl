#ifndef _UNIFORM_TEXTURE_HPP_
#define _UNIFORM_TEXTURE_HPP_

#include <glm/glm.hpp>

#include "texture.hpp"
#include "typed_uniform.hpp"
#include "uniform_texture_slot.hpp"
#include "debug.hpp"

namespace ssl
{
	template<typename Type, typename TextureType>
	class UniformTexture  : public TypedUniform<TextureType>
	{
	public:

		UniformTexture(const std::string& name, const glm::uvec3& dimension_size, void* data = NULL,
					   GLint minmag_filter = DEFAULT::MINMAG_FILTER,
					   GLint wrap          = DEFAULT::TEXTURE_WRAP);
		
		void copy(const UniformTexture& unif_tex);

		unsigned int       get_ID() const;
		const glm::uvec3&  get_dimension_size() const;

		const TextureType& get_texture() const;

		GLint get_minmag_filter() const;
		GLint get_wrap() const;

		void change_data(const glm::uvec3& dimension_size, void* data);

	protected:

		UniformTexture();
		UniformTexture(const UniformTexture&);
		UniformTexture& operator=(const UniformTexture&);
		UniformTexture& operator=(UniformTexture&&);

		BasicUniformSlot* get_instance();

	private:

		TextureUniformSlot<Type, TextureType> _slot;

	};
};



namespace ssl
{
	template<typename Type, typename TextureType>
	UniformTexture<Type, TextureType>::UniformTexture(const std::string& name, const glm::uvec3& dimension_size, void* data,
													  GLint minmag_filter, GLint wrap)
		: TypedUniform<TextureType>(name, BasicUniform::SINGLE_ELEMENT),
		  _slot(name, dimension_size, data, minmag_filter, wrap)
	{
		this->set_slot();
	}

	template<typename Type, typename TextureType>
	BasicUniformSlot* UniformTexture<Type, TextureType>::get_instance()
	{
		return &_slot;
	}

	template<typename Type, typename TextureType>
	GLint UniformTexture<Type, TextureType>::get_minmag_filter() const
	{
		return _slot.get_texture().get_minmag_filter();
	}
	
	template<typename Type, typename TextureType>
	GLint UniformTexture<Type, TextureType>::get_wrap() const
	{
		return _slot.get_texture().get_wrap();
	}


	template<typename Type, typename TextureType>
	void UniformTexture<Type, TextureType>::change_data(const glm::uvec3& dimension_size, void* data)
	{
		_slot.change_data(dimension_size, data);
	}

	template<typename Type, typename TextureType>
	const TextureType& UniformTexture<Type, TextureType>::get_texture() const
	{
		return _slot.get_texture();
	}

	template<typename Type, typename TextureType>
	void UniformTexture<Type, TextureType>::copy(const UniformTexture& unif_tex)
	{
		_slot.copy(unif_tex._slot);
	}

	template<typename Type, typename TextureType>
	unsigned int       UniformTexture<Type, TextureType>::get_ID() const
	{
		return _slot.get_ID();
	}

	template<typename Type, typename TextureType>
	const glm::uvec3&  UniformTexture<Type, TextureType>::get_dimension_size() const
	{
		return _slot.get_dimension_size();
	}


};

#endif //_UNIFORM_TEXTURE_HPP_
