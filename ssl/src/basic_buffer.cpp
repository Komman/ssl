#include "basic_buffer.hpp"

#include <GL/glew.h>
#include "debug.hpp"

namespace ssl
{
	BasicBuffer::BasicBuffer()
		: _data_has_changed(true)
	{
		GL(glGenBuffers(1, &_ID)); 
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY, "Buffer created: " + std::to_string(_ID));
		}
		#endif
	}

	void BasicBuffer::free()
	{
		if(_ID != 0)
		{
			#ifdef SSL_PRINTS
			if(ssl::are_prints_enable(SSL_PRINTS_MEMORY))
			{
				ssl::prints_msg(SSL_PRINTS_MEMORY, "Buffer deleted: " + std::to_string(_ID));
			}
			#endif

			GL(glDeleteBuffers(1, &_ID)); 
			_ID = 0;
		}
	}


	BasicBuffer::BasicBuffer(BasicBuffer&& b)
	{
		_ID = b.get_ID();
		b._ID = 0;
	}

	BasicBuffer& BasicBuffer::operator=(BasicBuffer&&  b)
	{
		this->free();
		_ID = b.get_ID();
		b._ID = 0;
		return *this;
	} 

	BasicBuffer::~BasicBuffer()
	{
		this->free();
	}	


	unsigned int BasicBuffer::get_ID() const
	{
		return _ID;
	}

	void BasicBuffer::data_has_changed()
	{
		_data_has_changed = true;
	}

	void BasicBuffer::bind() const
	{
		this->bind_buffer();
		if(_data_has_changed)
		{
			this->update_GPU_data_if_bound();
			_data_has_changed=false;
		}
	}

	uint BasicBuffer::bind_and_target_vertex_location(uint location) const
	{
		this->bind();
		return this->target_vertex_location(location);
	}

	void BasicBuffer::instant_update_GPU() const
	{
		this->bind();
	}

	void BasicBuffer::update_GPU() const
	{
		if(_data_has_changed)
		{
			this->bind_buffer();
			this->update_GPU_data_if_bound();
			_data_has_changed=false;
		}
	}	
};

