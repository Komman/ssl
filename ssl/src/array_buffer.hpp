#ifndef _ARRAY_BUFFER_HPP_
#define _ARRAY_BUFFER_HPP_

#include "gl_buffer.hpp"
#include "buffer_data_type.hpp"
#include "debug.hpp"

#include <GL/glew.h>
#include <vector>


namespace ssl
{

	template<typename Type>
	class ArrayBuffer : public GLBuffer<Type>
	{
		/*
			ONLY SUPPORT GLSL TYPES
			DO NOT SUPPORT ARRAY OF TYPES (vec[], mat[], float[] ...)
			SUPPORT OF DOUBLE PRECISION / INTEGER NOT TESTED
		*/

	public:
		ArrayBuffer(const std::vector<Type>& initial_data,
					draw_type type);
		ArrayBuffer(draw_type type) : ArrayBuffer({}, type) {}
		// ArrayBuffer(const std::vector<Type>& initial_data) : ArrayBuffer(initial_data, DEFAULT::MEMORY_DRAW_TYPE) {}
		// ArrayBuffer() : ArrayBuffer({}, DEFAULT::MEMORY_DRAW_TYPE) {}

		// Returns the subset of the buffer with elements at indices gives by the argument "indices"
		std::vector<Type> subset(const std::vector<unsigned int>& indices) const;
		std::vector<Type> subset(const std::pair<unsigned int, unsigned int>& indices_range_included) const;

	protected:

		ArrayBuffer& operator=(const ArrayBuffer& b); 
		ArrayBuffer(const ArrayBuffer& b);
		ArrayBuffer& operator=(ArrayBuffer&& b); 
		ArrayBuffer(ArrayBuffer&& b);
		
	private:

	};
};



namespace ssl
{
	template<typename Type>
	ArrayBuffer<Type>::ArrayBuffer(const std::vector<Type>& initial_data,
							 	   draw_type type)
		:	
			GLBuffer<Type>(initial_data, GL_ARRAY_BUFFER, type)
	{

	}

	template<typename Type>
	std::vector<Type> ArrayBuffer<Type>::subset(const std::vector<unsigned int>& indices) const
	{
		std::vector<Type> ret;

		for(uint i=0; i<indices.size(); i++)
		{
			if(indices[i] >= this->size())
			{
				ssl::err("ArrayBuffer<Type>::subset(): index out of range");
			}

			ret.push_back(this->get_subvalue(indices[i]));
		}

		return ret;
	}

	template<typename Type>
	std::vector<Type> ArrayBuffer<Type>::subset(const std::pair<unsigned int, unsigned int>& indices_range_included) const
	{
		if(indices_range_included.first > indices_range_included.second)
		{
			ssl::err("ArrayBuffer<Type>::subset(): indices_range_included.first > indices_range_included.second");
		}

		std::vector<Type> ret;

		for(uint i=indices_range_included.first; i<=indices_range_included.second; i++)
		{
			if(i >= this->size())
			{
				ssl::err("ArrayBuffer<Type>::subset(): index out of range");
			}

			ret.push_back(this->get_subvalue(i));
		}

		return ret;
	}

};

#endif //_ARRAY_BUFFER_HPP_
