#ifndef _COLOR_HPP_
#define _COLOR_HPP_

#include <string>
#include <glm/glm.hpp>

namespace ssl
{
	constexpr glm::vec3 BLACK  = glm::vec3(0.0,0.0,0.0);
	constexpr glm::vec3 RED    = glm::vec3(1.0,0.0,0.0);
	constexpr glm::vec3 GREEN  = glm::vec3(0.0,1.0,0.0);
	constexpr glm::vec3 BLUE   = glm::vec3(0.0,0.0,1.0);
	constexpr glm::vec3 PURPLE = glm::vec3(1.0,0.0,1.0);
	constexpr glm::vec3 YELLOW = glm::vec3(1.0,1.0,0.0);
	constexpr glm::vec3 CYAN   = glm::vec3(0.0,1.0,1.0);
	constexpr glm::vec3 WHITE  = glm::vec3(1.0,1.0,1.0);
	constexpr glm::vec3 PINK   = glm::vec3(0.8,0.1,0.5);
	constexpr glm::vec3 BROWN  = glm::vec3(0.6,0.3,0.0);
	constexpr glm::vec3 ORANGE = glm::vec3(1.0,0.5,0.0);

	glm::vec3 random_color();

	namespace DEFAULT
	{
		constexpr float DARKER_COEF = 0.3;
	};

	glm::vec3 dark(const glm::vec3& color, float dark_coef = DEFAULT::DARKER_COEF);

	namespace TERM
	{
		const std::string BLACK  = "\033[1;30m";
		const std::string RED    = "\033[1;31m";
		const std::string GREEN  = "\033[1;32m";
		const std::string ORANGE = "\033[1;33m";
		const std::string BLUE   = "\033[1;34m";
		const std::string PURPLE = "\033[1;35m";
		const std::string CYAN   = "\033[1;36m";
		const std::string WHITE  = "\033[1;37m";
		const std::string NOCOL  = "\033[0m";

		std::string colored(const std::string& str, const std::string& color);
	};

	namespace TERMB
	{
		const std::string BLACK  = "\033[7;30m";
		const std::string RED    = "\033[7;31m";
		const std::string GREEN  = "\033[7;32m";
		const std::string ORANGE = "\033[7;33m";
		const std::string BLUE   = "\033[7;34m";
		const std::string PURPLE = "\033[7;35m";
		const std::string CYAN   = "\033[7;36m";
		const std::string WHITE  = "\033[7;37m";
		const std::string NOCOL  = "\033[0m";
	};

};

#endif //_COLOR_HPP_
