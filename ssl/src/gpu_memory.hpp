#ifndef _GPU_MEMORY_HPP_
#define _GPU_MEMORY_HPP_

#include <GL/glew.h>

namespace ssl
{
	void memory_barrier(GLbitfield type);
};

#endif //_GPU_MEMORY_HPP_
