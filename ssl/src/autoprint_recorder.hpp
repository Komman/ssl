#ifndef _AUTOPRINT_RECORDER_HPP_
#define _AUTOPRINT_RECORDER_HPP_

#include <string>

#include "basic_recorder.hpp"

namespace ssl
{
	class AutoPrintRecorder : public BasicRecorder
	{
	public:
		AutoPrintRecorder(const std::string& name = "", bool opengl_rec = false);

		void start();
		void stop();

		void use_gl(bool gl);

		static void START(){quick.start();}
		static void STOP() {quick.stop();}

		static void GL_START(bool gl = true){gl_quick.use_gl(gl); gl_quick.start();}
		static void GL_STOP( bool gl = true){gl_quick.use_gl(gl); gl_quick.stop(); }

	private:
		std::string _name;
		uint64_t    _milisecs;
		bool        _rec_opengl;

		static AutoPrintRecorder quick;
		static AutoPrintRecorder gl_quick;
	};
};

#endif //_AUTOPRINT_RECORDER_HPP_
