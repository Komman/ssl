#include "shader.hpp"

#include "global_drawing.hpp"

namespace ssl
{
	Shader::Shader(
		const BasicShaderParser& parser,
		const std::string& vertex_shader_name,
	  	const std::string& fragment_shader_name,
		const std::vector<std::string>& fragment_modifiers
	)
		: ShaderBuilder(parser, vertex_shader_name, fragment_shader_name, fragment_modifiers),
		  _max_vertex_location_count(0),
		  _instanced_locations(0),
		  _draw_type(GL_TRIANGLES),
		  _max_drawn_index(std::numeric_limits<GLsizei>::max()),
		  _uniform_used(0),
		  _not_same_size_warning(false),
		  _initialized(false)
	{	
		if(this->ShaderBuilder::is_initialized())
		{
			this->Shader::initialize();
		}
	}

	bool Shader::try_to_initialize()
	{
		if(! this->ShaderBuilder::is_initialized())
		{
			if(this->ShaderBuilder::try_to_initialize())
			{
				if(_initialized)
				{
					err("Impossible to happen here (because it can't be initialized whitout ShaderBuilder initialization): " + SSL_FILE_AND_LINE);
				}

				this->Shader::initialize();

				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			if(!_initialized)
			{
				this->Shader::initialize();
			}
			return true;
		}
	}

	void Shader::has_to_initialize()
	{
		if(! this->ShaderBuilder::is_initialized())
		{
			this->ShaderBuilder::has_to_initialize();

			if(_initialized)
			{
				err("Impossible to happen here (because it can't be initialized whitout ShaderBuilder initialization): " + SSL_FILE_AND_LINE);
			}

			this->Shader::initialize();
		}
		else
		{
			if(!_initialized)
			{
				this->Shader::initialize();
			}
		}	
	}

	bool Shader::is_initialized() const
	{
		return _initialized;
	}


	void Shader::initialize()
	{
		if(_initialized)
		{
			err("Shader::initialize(): already initialized");
		}

		_max_vertex_location_count = compute_max_vertex_location_count();
		_instanced_locations = get_instanced_locations();

		_drawing_buffers.resize(this->get_vertex_ins().size());
		this->use();
		_uniform_used = BasicUniform::add_shader_uniforms(this->get_ID(), this->get_uniforms_names());
		
		_initialized = true;
	}

	void Shader::update_uniforms()
	{
		BasicUniform::update_uniforms_data(_uniform_used);
	}

	uint Shader::store_locations(std::vector<uint>& buffer_locations, uint location, uint location_size)
	{
		buffer_locations.push_back(location);
		return location_size;
	}

	void Shader::check_location_total(uint current_location, uint location_max_count)
	{
		if(current_location != location_max_count)
		{
			err("Shader::draw/draw_elements(...): Location error :\n" + TERM::NOCOL
				+"Sum of shaders location:         " + TERM::CYAN + std::to_string(location_max_count) + "\n" + TERM::NOCOL
				+"Sum of passed buffers locations: " + TERM::CYAN + std::to_string(current_location));
		}
	}

	std::string Shader::string_from_vertex_location() 
	{
		std::string ret="";
		for(uint i = 0; i < this->get_vertex_ins().size(); i++)
		{
			ret+=std::to_string(this->get_vertex_ins()[i].location);
			if(i!=this->get_vertex_ins().size()-1)
			{
				ret+=", ";
			}
		}
		return ret;
	}

	void Shader::check_every_locations(const std::vector<uint>& all_locations)
	{
		uint cur_loc = 0;
		for(uint i=0; i<all_locations.size(); i++)
		{
			if(all_locations[i] != cur_loc)
			{
				err("Shader::draw/draw_elements(...): Probably invalid types of buffer passed: wrong location set:" + TERM::NOCOL
					+ "Vertex shader location set : " + this->string_from_vertex_location()
					+ "\nBuffer passed locations : " + utils::string_from_vector(all_locations));
			}
			cur_loc += this->get_vertex_ins()[i].location_count;
		}
	}

	void Shader::enable_attribute_locations(uint count)
	{
		for(uint i=0; i<count; i++)
		{
			#ifdef SSL_PRINTS
			if(ssl::are_prints_enable(SSL_PRINTS_PARAMS_SETTING))
			{
				ssl::prints_msg(SSL_PRINTS_PARAMS_SETTING, "glEnableVertexAttribArray( " + std::to_string(i) + " )");
			}
			#endif

			GL(glEnableVertexAttribArray(i));
		}

	}

	void Shader::disable_attribute_locations(uint count)
	{
		for(int i=count-1; i>=0; i--) 
		{
			#ifdef SSL_PRINTS
			if(ssl::are_prints_enable(SSL_PRINTS_PARAMS_SETTING))
			{
				ssl::prints_msg(SSL_PRINTS_PARAMS_SETTING, "glDisableVertexAttribArray( " + std::to_string(i) + " )");
			}
			#endif

			GL(glDisableVertexAttribArray(i));
		}
	}

	uint Shader::compute_max_vertex_location_count()
	{
		uint location_max_count = 0;
		for(uint i=0; i<this->get_vertex_ins().size(); i++)
		{	
			location_max_count += this->get_vertex_ins()[i].location_count;
		}
		return location_max_count;
	}

	std::vector<uint> Shader::get_instanced_locations()
	{
		std::vector<uint> ret;
		for(auto& vert_in : this->get_vertex_ins())
		{
			if(vert_in.instanced)
			{
				for(uint relative_location = 0 ;
					relative_location < vert_in.location_count;
					relative_location++)
				{
					ret.push_back(vert_in.location + relative_location);
				}
			}
		}
		return ret;
	}

	std::vector<uint> Shader::buffers_divisors()
	{
		std::vector<uint> ret;
		for(auto& vert_in : this->get_vertex_ins())
		{
			if(vert_in.instanced)
			{
				ret.push_back(1);
			}
			else
			{
				ret.push_back(0);
			}
		}
		return ret;
	}

	void Shader::set_drawing_mode(GLenum opengl_draw_mode)
	{
		_draw_type = opengl_draw_mode;
	}

	void Shader::draw(const std::vector<const BasicBuffer*>& buffers)
	{
		this->Shader::has_to_initialize();

		this->vectorBuffer_draw(buffers);
	}

	GLenum Shader::get_drawing_mode(GLenum opengl_draw_mode)
	{
		return _draw_type;
	}

	GLsizei Shader::get_max_drawn_index()
	{
		return _max_drawn_index;
	}

	void Shader::draw_object(const DrawableObject& obj)
	{
		this->Shader::has_to_initialize();

		GLsizei old_max_index = this->get_max_drawn_index();
		this->set_max_drawn_index(obj.get_draw_max_index());
		
		this->draw(obj.get_buffers());
		
		this->set_max_drawn_index(old_max_index);
	}

	void Shader::draw_object(const ElementDrawableObject& obj)
	{
		this->Shader::has_to_initialize();

		GLsizei old_max_index = this->get_max_drawn_index();
		this->set_max_drawn_index(obj.get_draw_max_index());
		
		this->draw_elements(obj.get_elements(), obj.get_buffers());
		
		this->set_max_drawn_index(old_max_index);
	}

	void Shader::draw_elements(const ElementBuffer& elements, const std::vector<const BasicBuffer*>& buffers)
	{
		this->Shader::has_to_initialize();

		this->vectorBuffer_draw_elements(elements, buffers);
	}

	void Shader::draw(const VertexArrayObject& vao)
	{
		this->Shader::has_to_initialize();

		std::vector<const BasicBuffer*> buffers = vao.get_buffers();

		#ifdef SSL_DEBUG
		this->check_size(buffers);
		this->check_types(buffers);
		this->check_non_instances_sizes(buffers);
		#endif

		if(vao.elements_bound())
		{
			if(this->no_need_to_draw_element(*(vao.get_elements()), buffers))
				return;
		}
		else
		{
			if(this->no_need_to_draw(buffers))
				return;
		}

		// BIND VERTEX ARRAY OBJECT
		vao.bind();

		// USE SHADER
		this->use();
		this->update_uniforms();

		// DRAW
		if(vao.elements_bound())
		{
			this->direct_draw_elements(*(vao.get_elements()), buffers);
		}
		else
		{
			this->direct_draw(buffers);
		}

		global_drawing::bind_default_vao();
	}

	const textureBinding Shader::get_texture_binding(const std::string& texture_name)
	{
		this->Shader::has_to_initialize();

		#ifdef SSL_DEBUG
		if(!ShaderPreBuilder::is_uniform_texture(texture_name))
		{
			err("Try to call get_texture_binding() on a non-texture-typed uniform:\n"
				+ TERM::NOCOL + "uniform variable " + TERM::BLUE + texture_name + TERM::NOCOL + " is of type "
				+ TERM::RED + ShaderPreBuilder::get_uniform_info(texture_name).str_type + TERM::NOCOL + " that is not a tetxure");
		}
		#endif

		for(const auto& binding : _uniform_used)
		{
			if(BasicUniform::get_uniform_name(binding) == texture_name)
			{
				return binding;
			}
		}

		err("We are in trouble (impossible happened): uniform name known but no found it in uniforms slots: " + SSL_FILE_AND_LINE); 
		return _uniform_used[0]; // to shut up the compiler
	}

	void Shader::vectorBuffer_draw(const std::vector<const BasicBuffer*>& buffers)
	{
		#ifdef SSL_DEBUG
		this->check_size(buffers);
		this->check_types(buffers);
		this->check_non_instances_sizes(buffers);
		#endif

		if(this->no_need_to_draw(buffers))
			return;

		global_drawing::bind_default_vao();
		this->use();
		this->update_uniforms();

		// Enable all locations
		this->enable_attribute_locations(_max_vertex_location_count);

		// BINDING OF ALL BUFFERS
		this->bind_and_target_location_all_buffers(buffers);

		// DRAW
		this->direct_draw(buffers);

		// Diable all array locations
		// this->disable_attribute_locations(_max_vertex_location_count);	
	}

	void Shader::vectorBuffer_draw_elements(const ElementBuffer& elements, const std::vector<const BasicBuffer*>& buffers)
	{
		#ifdef SSL_DEBUG
		this->check_size(buffers);
		this->check_types(buffers);
		this->check_instances_sizes(buffers);
		#endif

		#ifdef SSL_EXPENSIVE_DEBUG
		this->check_element_binding(elements, buffers);
		#endif

		if(this->no_need_to_draw_element(elements, buffers))
			return;

		global_drawing::bind_default_vao();
		this->use();
		this->update_uniforms();

		// Enable all locations
		this->enable_attribute_locations(_max_vertex_location_count);

		// BINDING OF ALL BUFFERS
		this->bind_and_target_location_all_buffers(buffers);

		// DRAW
		elements.bind();
		this->direct_draw_elements(elements, buffers);

		// Diable all locations
		// this->disable_attribute_locations(_max_vertex_location_count);	
	}

	void Shader::dispatch_instance_locations()
	{
		for(int loc=0; loc<(int)_max_vertex_location_count; loc++)
		{
			#ifdef SSL_PRINTS
			if(ssl::are_prints_enable(SSL_PRINTS_PARAMS_SETTING))
			{
				ssl::prints_msg(SSL_PRINTS_PARAMS_SETTING, "glVertexAttribDivisor( " 
					+ std::to_string(loc) + ", 0 )");
			}
			#endif

			GL(glVertexAttribDivisor(loc, 0));
		}

		for(auto& inst_loc : _instanced_locations)
		{
			#ifdef SSL_PRINTS
			if(ssl::are_prints_enable(SSL_PRINTS_PARAMS_SETTING))
			{
				ssl::prints_msg(SSL_PRINTS_PARAMS_SETTING, "glVertexAttribDivisor( " 
					+ std::to_string(inst_loc) + ", 1 )");
			}
			#endif

			GL(glVertexAttribDivisor(inst_loc, 1));
		}
	}


	uint Shader::bind_and_target_location_all_buffers(const std::vector<const BasicBuffer*>& buffers)
	{
		uint current_location = 0;

		#ifdef SSL_DEBUG
		std::vector<uint> all_locations;
		all_locations.reserve(this->get_vertex_ins().size());

		// BINDING OF ALL BUFFERS
		for(auto buff : buffers)
		{
			current_location+=store_locations(all_locations, current_location,  buff->bind_and_target_vertex_location(current_location));
		}

		this->check_location_total(current_location, _max_vertex_location_count);
		this->check_every_locations(all_locations);

		#else

		// BINDING OF ALL BUFFERS
		for(auto buff : buffers)
		{
			current_location+=buff->bind_and_target_vertex_location(current_location);
		}
		
		#endif

		this->dispatch_instance_locations();

		return current_location;
	}

	void Shader::set_max_drawn_index(GLsizei max_index)
	{
		_max_drawn_index = max_index;
	}

	void Shader::unset_max_drawn_index()
	{
		_max_drawn_index = std::numeric_limits<GLsizei>::max();
	}

	std::string Shader::get_msg_buffer_sizes(const std::vector<const BasicBuffer*>& buffers)
	{
		std::string ret = "(";
		for(int i=0; i<(int)buffers.size(); i++)
		{
			const auto& e = buffers[i];

			ret+=std::to_string(e->size());
			if(i < (int)buffers.size()-1)
			{
				ret+=", ";
			}
		}
		ret+=")";

		return ret;
	}

			
	bool Shader::no_need_to_draw(const std::vector<const BasicBuffer*>& buffers)
	{
		if(_instanced_locations.size() == 0)
		{
			return (this->get_non_instance_buffer_size(buffers) == 0);
		}
		else
		{
			return (this->get_instance_count(buffers) == 0);
		}
	}

	bool Shader::no_need_to_draw_element(const ElementBuffer& elements, const std::vector<const BasicBuffer*>& buffers)
	{
		if(_instanced_locations.size() == 0)
		{
			return (elements.indices_count() == 0);
		}
		else
		{
			return (this->get_instance_count(buffers) == 0);
		}
	}

	void Shader::direct_draw_elements(const ElementBuffer& elements, const std::vector<const BasicBuffer*>& buffers)
	{
		if(_instanced_locations.size() == 0)
		{
			#ifdef SSL_PRINTS
			if(ssl::are_prints_enable(SSL_PRINTS_DRAW))
			{
				ssl::prints_msg(SSL_PRINTS_DRAW, TERM::RED + "DRAWCALL " + TERM::ORANGE + "on " + std::to_string(buffers.size()) + " array buffers of sizes "  + this->get_msg_buffer_sizes(buffers) + " and " + std::to_string((int)elements.indices_count()) + " elements");
			}
			#endif

			int elem_count = elements.indices_count();

			if(elem_count > 0)
			{
				GL(glDrawElements(_draw_type, std::min(elem_count, (int)_max_drawn_index), GL_UNSIGNED_INT, (void*)(0)));
			}
			else
			{
				ssl::err("Impossible, should be prevented before (by the no_need_to_draw_element() method)");
			}
		}
		else 
		{	
			#ifdef SSL_PRINTS
			if(ssl::are_prints_enable(SSL_PRINTS_DRAW))
			{
				ssl::prints_msg(SSL_PRINTS_DRAW, TERM::RED + "INSTANCE DRAWCALL " + TERM::ORANGE + "(" + std::to_string(this->get_instance_count(buffers)) + " instances) on " + std::to_string(buffers.size()) + " array buffers of sizes "  + this->get_msg_buffer_sizes(buffers) + " and " + std::to_string((int)elements.indices_count()) + " elements");
			}
			#endif

			int inst_count = this->get_instance_count(buffers);

			if(inst_count > 0)
			{
				GL(glDrawElementsInstanced(_draw_type, std::min((int)elements.indices_count(), (int)_max_drawn_index), GL_UNSIGNED_INT, (void*)(0), inst_count));
			}
			else
			{
				ssl::err("Impossible, should be prevented before (by the no_need_to_draw_element() method)");
			}
		}
	}

	void Shader::direct_draw(const std::vector<const BasicBuffer*>& buffers)
	{
		if(_instanced_locations.size() == 0)
		{
			#ifdef SSL_PRINTS
			if(ssl::are_prints_enable(SSL_PRINTS_DRAW))
			{
				ssl::prints_msg(SSL_PRINTS_DRAW, TERM::RED + "DRAWCALL " + TERM::ORANGE + "on " + std::to_string(buffers.size()) + " array buffers of sizes " + this->get_msg_buffer_sizes(buffers));
			}
			#endif

			int buffsize = this->get_non_instance_buffer_size(buffers);

			if(buffsize > 0)
			{
				GL(glDrawArrays(_draw_type, 0, std::min((int)_max_drawn_index, buffsize)));
			}
			else
			{
				ssl::err("Impossible, should be prevented before (by the no_need_to_draw() method)");
			}
		}
		else 
		{
			#ifdef SSL_PRINTS
			if(ssl::are_prints_enable(SSL_PRINTS_DRAW))
			{
				ssl::prints_msg(SSL_PRINTS_DRAW, TERM::RED + "INSTANCE DRAWCALL " + TERM::ORANGE + "on " + std::to_string(buffers.size()) + " array buffers of sizes " + this->get_msg_buffer_sizes(buffers));
			}
			#endif

			int inst_count = this->get_instance_count(buffers);

			if(inst_count > 0)
			{
				GL(glDrawArraysInstanced(_draw_type, 0, std::min((int)_max_drawn_index, (int)this->get_non_instance_buffer_size(buffers)), inst_count));
			}
			else
			{
				ssl::err("Impossible, should be prevented before (by the no_need_to_draw() method)");
			}
		}
	}

	void Shader::check_size(const std::vector<const BasicBuffer*>& buffers)
	{
		if(this->get_vertex_ins().size() != buffers.size())
		{
			err("Shader::draw/draw_elements(...) : Invalid buffers number for this Shader:\n" + TERM::NOCOL
				+ "Vertex Shader " + TERM::RED + "in" + TERM::NOCOL + " variables: " + std::to_string(this->get_vertex_ins().size())
				+ "\nBuffers passed as arguments : " + std::to_string(buffers.size()));
		}
	}


	void Shader::check_element_binding(const ElementBuffer& elements, const std::vector<const BasicBuffer*>& buffers)
	{
		uint min_buffer_size = UINT_MAX;

		for(uint i=0; i<buffers.size(); i++)
		{
			if(!this->get_vertex_ins()[i].instanced && buffers[i]->size() < min_buffer_size)
			{
				min_buffer_size = buffers[i]->size();
			}
		}

		for(auto& ind : elements.get_value())
		{
			if(ind >= min_buffer_size)
			{
				err("Shader::draw_elements(...): The ElementBuffer contains indices that are out of bound of some buffer(s) passed:" + TERM::NOCOL
					+"\nAn indice in the ElementBuffer is: " + TERM::RED + std::to_string(ind) + TERM::NOCOL
					+"\nAnd a passed buffer has only " + TERM::RED + std::to_string(min_buffer_size) + TERM::NOCOL + " values");
			}
		}
	}

	void Shader::check_instances_sizes(const std::vector<const BasicBuffer*>& buffers)
	{
		if(_instanced_locations.size() == 0)
		{
			return;
		}

		std::vector<int> buffer_instance_sizes;
		buffer_instance_sizes.reserve(buffers.size());

		for(uint i=0; i<buffers.size(); i++)
		{
			if(this->get_vertex_ins()[i].instanced)
			{
				buffer_instance_sizes.push_back(buffers[i]->size());
			}
			else
			{
				buffer_instance_sizes.push_back(-1);
			}
		}

		int instance_size = -1;

		for(auto& s : buffer_instance_sizes)
		{
			if(instance_size == -1  &&  s != -1)
			{
				instance_size = s;
			}
			else
			{
				if(s != -1 && instance_size != s)
				{
					err("Shader::draw/draw_elements(...): All instancing buffer must have the same size:" + TERM::NOCOL
						+"\nHere, an instance buffer has a size of " + TERM::RED + std::to_string(instance_size) + TERM::NOCOL
						+"\nAnd an other has a size of " + TERM::RED + std::to_string(s) + TERM::NOCOL);
				}
			}
		}
	}

	void Shader::check_non_instances_sizes(const std::vector<const BasicBuffer*>& buffers)
	{
		if(_not_same_size_warning)
			return;

		std::vector<int> buffer_sizes;
		buffer_sizes.reserve(buffers.size());

		for(uint i=0; i<buffers.size(); i++)
		{
			if(this->get_vertex_ins()[i].instanced == false)
			{
				buffer_sizes.push_back(buffers[i]->size());
			}
			else
			{
				buffer_sizes.push_back(-1);
			}
		}

		int buffer_size = -1;

		for(auto& s : buffer_sizes)
		{
			if(buffer_size == -1  &&  s != -1)
			{
				buffer_size = s;
			}
			else
			{
				if(s != -1 && buffer_size != s)
				{
					war("Shader::draw(...): All buffers have not the same size:" + TERM::NOCOL
						+"\nHere, a buffer has a size of " + TERM::RED + std::to_string(buffer_size) + TERM::NOCOL
						+"\nAnd an other has a size of " + TERM::RED + std::to_string(s) + TERM::NOCOL);
					
					_not_same_size_warning = true;
				}
			}
		}
	}

	uint Shader::get_instance_count(const std::vector<const BasicBuffer*>& buffers)
	{
		int count=-1;

		for(uint i=0; i<buffers.size(); i++)
		{
			if(this->get_vertex_ins()[i].instanced)
			{
				count = buffers[i]->size();
				break;
			}
		}

		#ifdef SSL_DEBUG
		if(count == -1)
		{
			err("Try to call Shader::get_instance_count(...) while there is no instancing buffer found");
		}
		#endif
		
		return count;
	}

	uint Shader::get_non_instance_buffer_size(const std::vector<const BasicBuffer*>& buffers)
	{
		int count=-1;

		for(uint i=0; i<buffers.size(); i++)
		{
			if(this->get_vertex_ins()[i].instanced == false)
			{
				count = buffers[i]->size();
				break;
			}
		}

		#ifdef SSL_DEBUG
		if(count == -1)
		{
			err("Try to call Shader::get_non_instance_buffer_size(...) while there is no buffer found");
		}
		#endif
		
		return count;
	}


	void Shader::check_types(const std::vector<const BasicBuffer*>& buffers)
	{
		for(uint i=0; i<buffers.size(); i++)
		{
			if(this->get_vertex_ins()[i].type != buffers[i]->get_self_type_id())
			{
				err("Shader::draw/draw_elements(...): A type is not respected (vertex shader types and buffer types are not the same)");
			}
		}
	}
};

