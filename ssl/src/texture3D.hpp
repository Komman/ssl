#ifndef _TEXTURE3D_HPP_
#define _TEXTURE3D_HPP_


#include "texture.hpp"

namespace ssl
{
	template<typename Type>
	class Texture3D : public Texture<Type>
	{
	public:
		Texture3D(const glm::uvec3&  size,
				  void*              data          = NULL,
				  GLint              minmag_filter = DEFAULT::MINMAG_FILTER,
				  GLint              wrap          = DEFAULT::TEXTURE_WRAP);


		glm::uvec3 size()            const;
		GLenum     get_gl_texture_type()       const override;	
		attachementType get_attachement_type() const override;


	protected:

		void change_data_if_bound(const glm::uvec3& dimension_size, void* data) override;
		
		void attach_on_framebuffer_if_bound(uint attachement_index)           const override;
		void tex_image_if_bound(const glm::uvec3& dimension_size, void* data) const override;
		bool mipmaps_enable()                                                 const override;
		void definitive_resize_GPU(const glm::uvec3& new_dimension_size)                 const override;


	private:

	};
};


namespace ssl
{
	template<typename Type>
	Texture3D<Type>::Texture3D(const glm::uvec3& size,
							   void* data,
							   GLint minmag_filter,
							   GLint wrap)
		: Texture<Type>(size, minmag_filter, wrap)
	{
		this->initialize(data);
	}

	template<typename Type>
	attachementType Texture3D<Type>::get_attachement_type() const
	{
		return COLOR_ATTACHEMENT;
	}

	template<typename Type>
	glm::uvec3 Texture3D<Type>::size() const
	{
		return this->get_dimension_size();		
	}

	template<typename Type>
	void Texture3D<Type>::tex_image_if_bound(const glm::uvec3& dimension_size, void* data) const
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY_FLOW))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY_FLOW, "Creating texture3D data of texture id:" + std::to_string(this->get_ID()) 
				+ ", with data of size: "
				+ std::to_string(dimension_size.x) + "x"
				+ std::to_string(dimension_size.y) + "x"
				+ std::to_string(dimension_size.z));
		}
		#endif

		GL(glTexImage3D(this->get_gl_texture_type(),
			 			0,
			 			Texture<Type>::get_GL_internal_format(),
			 			dimension_size.x,
			 			dimension_size.y,
			 			dimension_size.z,
			 			0,
			 			Texture<Type>::get_GL_valid_format(),
			 			Texture<Type>::get_GL_data_type(),
			 			data));
	}

	template<typename Type>
	void Texture3D<Type>::change_data_if_bound(const glm::uvec3& dimension_size, void* data)
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY_FLOW))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY_FLOW, "Updating texture3D data of texture id:" + std::to_string(this->get_ID()));
		}
		#endif

		GL(glTexSubImage3D(
			this->get_gl_texture_type(),
			0,
			0,
			0,
			0,
			dimension_size.x,
			dimension_size.y,
			dimension_size.z,
			Texture<Type>::get_GL_valid_format(),
			Texture<Type>::get_GL_data_type(),
			data
		));
	}

	template<typename Type>
	GLenum Texture3D<Type>::get_gl_texture_type() const
	{
		return GL_TEXTURE_3D;
	}

	template<typename Type>
	void Texture3D<Type>::definitive_resize_GPU(const glm::uvec3& new_dimension_size) const
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY_FLOW))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY_FLOW, "Resizing texture2D id:" + std::to_string(this->get_ID()) 
				+ ", with size: "
				+ std::to_string(new_dimension_size.x) + "x"
				+ std::to_string(new_dimension_size.y) + "x"
				+ std::to_string(new_dimension_size.y));
		}
		#endif

		GL(glTextureStorage3D(this->get_ID(), 1, Texture<Type>::get_GL_internal_format(), new_dimension_size.x, new_dimension_size.y, new_dimension_size.z));
	}

	template<typename Type>
	bool Texture3D<Type>::mipmaps_enable() const
	{
		return true;
	}

	template<typename Type>
	void Texture3D<Type>::attach_on_framebuffer_if_bound(uint attachement_index) const
	{
		err("Can't attach a 3D texture to a frambuffer (theoritically yes but TODO)");
	}
}

#endif //_TEXTURE3D_HPP_
