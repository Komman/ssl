#include "compute_shader.hpp"

#include <glm/gtx/string_cast.hpp>

namespace ssl
{
	ComputeShader::ComputeShader(const BasicShaderParser& parser,
								 const std::string& compute_name, 
								 const glm::uvec3&  local_size)
		: ComputeShaderBuilder(parser, compute_name, local_size)
	{
		_uniform_used = BasicUniform::add_shader_uniforms(this->get_ID(), this->_uniforms);
	}

	void ComputeShader::update_uniforms()
	{
		BasicUniform::update_uniforms_data(_uniform_used);
	}

	void ComputeShader::dispatch(const glm::uvec3&  block_count)
	{
		this->use();
		this->update_uniforms();

		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_DRAW))
		{
			ssl::prints_msg(SSL_PRINTS_DRAW, "DISPATCH COMPUTE CALL with dimensions: " + glm::to_string(block_count));
		}
		#endif

		GL(glDispatchCompute(block_count.x, block_count.y, block_count.z));
	}
};

