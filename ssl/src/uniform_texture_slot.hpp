#ifndef _UNIFORM_TEXTURE_SLOT_HPP_
#define _UNIFORM_TEXTURE_SLOT_HPP_

#include "texture.hpp"
#include "basic_uniform_slot.hpp"
#include "uniform_texture_controller.hpp"

namespace ssl
{
	template<typename Type, typename TextureType>
	class TextureUniformSlot : public BasicUniformSlot
	{
	public:

		TextureUniformSlot(const std::string& name,
						   const glm::uvec3&   dimension_size,
						   void* data,
						   GLint minmag_filter,
						   GLint wrap);

		void copy(const TextureUniformSlot& source);

		unsigned int       get_ID() const;
		const glm::uvec3&  get_dimension_size() const;

		const TextureType& get_texture() const;

		void change_data(const glm::uvec3& dimension_size, void* data);

	protected:

		BasicUniformController* alloc_instance(GLuint shader_to_link, uint texture_count) override; 
		void* get_data() const override;
		uint  get_data_count() const override;

	private:

		 TextureType _tex;
	};
};


namespace ssl
{
	template<typename Type, typename TextureType>
	TextureUniformSlot<Type, TextureType>::TextureUniformSlot(const std::string& name,
											     			  const glm::uvec3&   dimension_size,
											     			  void* data,
											     			  GLint minmag_filter,
											     			  GLint wrap)
		: BasicUniformSlot(name),
		  _tex(dimension_size, data, minmag_filter, wrap)
	{
		
	}

	template<typename Type, typename TextureType>
	const TextureType& TextureUniformSlot<Type, TextureType>::get_texture() const
	{
		return _tex;
	}

	template<typename Type, typename TextureType>
	void TextureUniformSlot<Type, TextureType>::change_data(const glm::uvec3& dimension_size, void* data)
	{
		_tex.change_data(dimension_size, data);
	}

	
	
	template<typename Type, typename TextureType>
	void TextureUniformSlot<Type, TextureType>::copy(const TextureUniformSlot& source)
	{
		_tex.copy(source._tex);
	}

	template<typename Type, typename TextureType>
	unsigned int       TextureUniformSlot<Type, TextureType>::get_ID() const
	{
		return _tex.get_ID();
	}

	template<typename Type, typename TextureType>
	const glm::uvec3&  TextureUniformSlot<Type, TextureType>::get_dimension_size() const
	{
		return _tex.get_dimension_size();
	}

	template<typename Type, typename TextureType>
	BasicUniformController* TextureUniformSlot<Type, TextureType>::alloc_instance(GLuint shader_to_link, uint texture_count)
	{
		BasicUniformController* tex_instance = new UniformTextureController<Type>(this->get_name(), shader_to_link, &_tex ,texture_count);
		return tex_instance;
	}

	template<typename Type, typename TextureType>
	void* TextureUniformSlot<Type, TextureType>::get_data() const
	{
		return (void*)(0);
	}

	template<typename Type, typename TextureType>
	uint  TextureUniformSlot<Type, TextureType>::get_data_count() const
	{
		return 1;
	} 
};

#endif //_UNIFORM_TEXTURE_SLOT_HPP_
