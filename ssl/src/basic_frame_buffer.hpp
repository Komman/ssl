#ifndef _BASIC_FRAME_BUFFER_HPP_
#define _BASIC_FRAME_BUFFER_HPP_

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>

#include "types_id.hpp"

namespace ssl
{
	namespace DEFAULT
	{
		constexpr GLenum CLEAR_MASK = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT;
	};

	class BasicFrameBuffer
	{
	public:
		virtual void bind()   const =0;
		virtual void unbind() const =0;
		virtual unsigned int get_ID() const =0;
		virtual const glm::uvec2& frame_size() const =0;
	
		void set_clear_color(const glm::vec4& color);
		void clear(GLbitfield mask = DEFAULT::CLEAR_MASK);
	
		const glm::vec4& get_clear_color();

		virtual const std::vector<types_id>& get_attachements_types() const=0;

	protected:
		BasicFrameBuffer(const glm::vec4& clear_color);

	private:
		glm::vec4 _clear_color;
	};
};

#endif //_BASIC_FRAME_BUFFER_HPP_
