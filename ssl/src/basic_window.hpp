#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp> 
#include <string>

#include "window.hpp"

namespace ssl
{
	class BasicWindow
	{
	public:
		BasicWindow(const std::string& name);
		BasicWindow(const std::string& name, unsigned int window_divisor);
		BasicWindow(unsigned int size_x, unsigned int size_y, const std::string& name);

		void reset_frame_time();
		void clear();
		void flip();
		void close();

		void set_char_callback(void(*f)(GLFWwindow* w, unsigned int codepoint));
		void set_key_callback(void(*f) (GLFWwindow *window, int key, int scancode, int action, int mods));

		bool  should_close()     const;
		const glm::ivec2& size() const;

		static float delta_time();
		static void  create_context();
		static void  destroy_context();

	protected:
		friend const glm::ivec2& window::size();
		
		friend glm::dvec2 mouse::position_pixel();
		friend glm::dvec2 mouse::position();
		friend void       mouse::set(const glm::dvec2& pos);
		friend void       mouse::set_pixel(const glm::dvec2& pos);
		friend void       mouse::set_visibility(bool visibility);
		friend bool       mouse::in_game();
		friend void       mouse::quit_game();
		friend void       mouse::enter_game();
		friend bool       keyboard::key_pressed(int key);
		friend void       keyboard::init();
		friend bool       window::should_close();

	private:
		void configure();

		GLFWwindow* _window;
		glm::ivec2  _win_size;

		int    _fps;
	    double _t_fps;
	    float  _time_old; 

	    char _creation_step = 0;

	    static float _dt;
	};
};
