#ifndef _BASIC_SHADER_PARSER_HPP_
#define _BASIC_SHADER_PARSER_HPP_

namespace ssl
{

	namespace DEFAULT
	{
		const inline std::string& GENERATED_SHADERS_DIRECTORY_NAME = "generated_shaders";
		const inline std::string& PREPROCESSOR_SHADER_NAME         = "global_shader.glsl";
	};

	
	class BasicShaderParser
	{

		/*
			PARSING FORMAT:
			
			-To use uniform variable:               uniform::name
			-To use uniform array variable:         uniform::name[indice]
			-To use a variable that has not
			been declared in a function core:		local::name
			-To use a function:                     local::function_name(args...)
			-To use a instance variable in
			a vertex shader input:                  in::instance name
		*/


	public:

		static constexpr int ARRAY_COUNT_SINGLE_ELEMENT = -1;
		static constexpr int ARRAY_COUNT_NOT_SIZED      = -2;

		struct argDec
		{
			std::string inout;
			std::string type;
			std::string name;
			int         array_count;
		};

		struct functionInfo
		{
			std::string              function_type;  // TYPE
			std::string              function_name;	 // FUNCTION_NAME
			std::vector<std::string> uniforms;       // {UNIFORM_NAME}, ...
			std::vector<std::string> local_vars;     // {LOCAL_VAR_NAME}, ...
			std::vector<std::string> local_funcs;    // {LOCAL_FUNC_NAME}, ...
			
			std::vector<argDec> args;

			std::string function_core;  

			void show() const;
		};

		// Get a parsed content of the function
		virtual functionInfo              get_function(const std::string& function_name) const =0;
		// Get the lines of the variable/MACRO declaration
		virtual std::vector<std::string>  get_var_dec(const std::string& variable_name) const =0;

		// Returns is the shader file is open
		virtual bool is_sourced() const =0;

		// Returns "" if there is no file name
		virtual std::string get_file_name() const =0;



		static inline std::string macro_token    = "#";
		static inline std::string uniform_key    = "uniform::";
		static inline std::string local_key      = "local::";
		static inline std::string out_key        = "out";
		static inline std::string in_key         = "in";
		static inline std::string inout_key      = "inout";
		static inline std::string instance_key   = "in::instance";
		static inline std::string glsl_delim     = " \n/\\*-+!&|<>,:;#{}^)=%?.[]";

		static std::string array_ext(int count)
		{
			if(count == ARRAY_COUNT_SINGLE_ELEMENT)
				return "";

			if(count == ARRAY_COUNT_NOT_SIZED)
				return "[]";

			return ("[" + std::to_string(count) + "]");
		}

	};
};

#endif //_BASIC_SHADER_PARSER_HPP_
