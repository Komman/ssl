#ifndef _UNIFORM_IMAGE3D_HPP_
#define _UNIFORM_IMAGE3D_HPP_

#include "uniform_texture.hpp"
#include "image3D.hpp"

namespace ssl
{
	template<typename Type>
	class UniformImage3D : public UniformTexture<Type, Image3D<Type>> 
	{
	public:
		UniformImage3D(const std::string& name,
					   const glm::uvec3&  size,
					   const std::vector<Type>& data = std::vector<Type>(0));

		glm::uvec3 size();

	protected:

	private:
	};
};


namespace ssl
{
	template<typename Type>
	UniformImage3D<Type>::UniformImage3D(const std::string& name,
						 			   	 const glm::uvec3&  size,
						 			   	 const std::vector<Type>& data)
		: UniformTexture<Type, Image3D<Type>>(name, size, data.size() == 0 ? NULL : (void*) data.data())
	{
		if(data.size() != 0 && data.size() < size.x*size.y*size.z)
		{
			err("try to build a Image3D of a total size of "+ std::to_string(size.x)+"x"
				+ std::to_string(size.y) + "x" + std::to_string(size.z) + " = "  + std::to_string(size.x*size.y*size.z) 
				+ " but the data vector passed has a size of " + std::to_string(data.size()));
		}
	}

	template<typename Type>
	glm::uvec3 UniformImage3D<Type>::size()
	{
		return this->get_dimension_size();
	}
};

#endif //_UNIFORM_IMAGE3D_HPP_
