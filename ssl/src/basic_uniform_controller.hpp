#ifndef _BASIC_UNIFORM_CONTROLLER_HPP_
#define _BASIC_UNIFORM_CONTROLLER_HPP_

#include <string>
#include <GL/glew.h>

namespace ssl
{
	class BasicUniformController
	{
	public:
		BasicUniformController(const std::string& name, GLuint shader_ID);

		void up_to_date(bool updated);
		bool is_up_to_date();

		void send_data_to_GPU(void* data, int array_count) const;

		virtual void activate_before_draw() const =0;

		virtual ~BasicUniformController(){}

		void print() const;

	protected:
		virtual void send_typed_data_to_GPU(void* data, int array_count) const =0;
		
		GLuint get_ID() const;

	private:
		void link_to_shader(const std::string& name, GLuint shader_ID);

		bool   _updated;
		GLuint _ID;
	};
};

#endif //_BASIC_UNIFORM_CONTROLLER_HPP_
