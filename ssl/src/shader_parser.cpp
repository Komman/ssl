#include "shader_parser.hpp"
#include "shader_preprocessor.hpp"
#include <sys/stat.h>
#include <filesystem>

#include "debug.hpp"

using namespace std;
using namespace ssl;

namespace ssl
{
	ShaderParser::ShaderParser(const std::string& source_file_path)
		: _file_name("")
	{
		this->open(source_file_path);
	}

	void ShaderParser::open(const std::string& global_shader_file_path)
	{
		_file_name = global_shader_file_path;
		_file.open(global_shader_file_path);

		if(!_file.is_open())
		{
			err("Failed to open ToParseShader: " + global_shader_file_path);
		}
	}

	std::string ShaderParser::get_file_name() const
	{
		return _file_name;
	}

	void ShaderParser::free()
	{
		_parsed_cache.clear();
		_var_dec_cache.clear();
	
		if(_file.is_open())
		{
			_file.close();
		}
		
		_file_name = "";
	}

	bool ShaderParser::is_sourced() const
	{
		return _file.is_open();
	}

	ShaderParser::~ShaderParser()
	{
		this->free();
	}

	void ShaderParser::functionInfo::show() const
	{
		std::cout<<"function_type: "<<function_type<<std::endl;
		std::cout<<"function_name: "<<function_name<<std::endl;
		std::cout<<"uniforms: "   ; utils::print_vector(uniforms)   ; std::cout<<std::endl;
		std::cout<<"local_vars: " ; utils::print_vector(local_vars) ; std::cout<<std::endl;
		std::cout<<"local_funcs: "; utils::print_vector(local_funcs); std::cout<<std::endl;
		std::cout<<"args: {";
		for(auto& arg : args) {std::cout<<arg.inout<<" "<<arg.type<<" "<<arg.name<<array_ext(arg.array_count)<<std::endl;}
		std::cout<<"}"<<std::endl;
		std::cout<<"function_core: "<<function_core<<std::endl;
	}

	ShaderParser::functionInfo ShaderParser::get_function(const std::string& function_name) const
	{
		if(_parsed_cache.find(function_name) != _parsed_cache.end())
		{
			return _parsed_cache[function_name];
		}

		ShaderParser::functionInfo parsed = this->parse_function(function_name);

		_parsed_cache[function_name] = parsed;
		return parsed;
	}

	std::vector<std::string> ShaderParser::get_var_dec(const std::string& variable_name) const
	{
		if(_var_dec_cache.find(variable_name) != _var_dec_cache.end())
		{
			return _var_dec_cache[variable_name];
		}

		std::vector<std::string> variable_declaration = this->harvest_variable_declaration(variable_name);
		_var_dec_cache[variable_name] = variable_declaration;
		return variable_declaration;
	}

	void ShaderParser::check_file_open() const
	{
		if(!_file.is_open())
		{
			err("try to read in ShaderParser that is already freed");
		}
	}

	std::string ShaderParser::catch_var_header_str(std::ifstream& file, const std::string& str) const
	{
		string line;

		while(getline(file, line))
		{
			for(unsigned int i=0; i<line.size();i++)
			{
				if(utils::is_string_detected(line, i, str))
				{	
					bool return_ok = false;
					if(line.size()==str.size())
					{
						return_ok = true;
					}
					else if(line.size()>str.size() && ( line[i+str.size()] == ' ' 
													 || line[i+str.size()] == '\t'
													 || line[i+str.size()] == '\n'
													 || line[i+str.size()] == '['
													 || line[i+str.size()] == '='))
					{
						return_ok = true;
					}

					if(return_ok)
					{
						return line;
					}
				}
			}
		}
		return NOT_CATCHED;
	}


	std::string ShaderParser::catch_func_header_str(std::ifstream& file, const std::string& str) const
	{
		string line;

		while(getline(file, line))
		{
			for(int i=0; i<(int)(line.size());i++)
			{
				if(utils::is_string_detected(line, i, str))
				{	
					if(i>0 && !utils::is_in_string(line[i-1], ShaderPreprocessor::separators))
					{
						continue;
					}

					bool return_ok = false;
					if(line.size()==str.size())
					{
						return_ok = true;
					}
					else if(line.size()>str.size() && ( line[i+str.size()] == ' ' 
													 || line[i+str.size()] == '\t'
													 || line[i+str.size()] == '\n'
													 || line[i+str.size()] == '('))
					{
						return_ok = true;
					}

					if(return_ok)
					{
						return line;
					}
				}
			}
		}
		return NOT_CATCHED;
	}

	

	//deplace the file to the first '{' of the function declaration
	string ShaderParser::catch_func_dec(std::ifstream& file, const std::string& function_name) const
	{
		string func_dec = this->catch_func_header_str(file, function_name);
		if(func_dec == NOT_CATCHED)
		{
			err("ShaderParser : function "+ TERM::BLUE + function_name + ERR_COLOR + " not found");
		}

		string line;
		while(getline(file, line))
		{
			for(char c : line)
			{
				if(c == '{')
				{
					return func_dec;
				}
				else
				{
					if(c == '}')
					{
						err("ShaderParser::catch_func_dec: function declaration closed before being catched");
					}
					func_dec.push_back(c);
				}
			}
		}

		return func_dec;
	} 

	int ShaderParser::array_count_from_arg(string arg) const
	{
		if(arg == " ")
			err("Impossible: " + SSL_FILE_AND_LINE);

		int i=0;
		int end = arg.size()-1;

		if(arg[0] == ' ')
			i++;

		if(arg[end] == ' ')
			end--;

		if(arg[i] == CLOSE_BRACKET_ARRAY_DECLARATION)
			return ARRAY_COUNT_NOT_SIZED;

		return std::stoi(arg.substr(i, end - i));
	}

	void ShaderParser::fill_head_fun_infos(functionInfo& infos, const std::vector<std::string>& splited_dec) const
	{
		infos.function_type = splited_dec[0];
		infos.function_name = splited_dec[1];

		vector<string> arg_inout_attrib = {
			in_key,
			out_key,
			inout_key,
			instance_key
		};

		unsigned int i=2;
		while(i<splited_dec.size())
		{
			argDec arg_dec = {"", "", "", ARRAY_COUNT_SINGLE_ELEMENT};

			if(utils::in_vector(arg_inout_attrib, splited_dec[i]))
			{
				arg_dec.inout=splited_dec[i];
				i++;
			}
			
			arg_dec.type = splited_dec[i];

			i++;
			if(i>=splited_dec.size())
				err("Invalid argument number in function " + TERM::CYAN + infos.function_type + " " + infos.function_name + "(...)" + TERM::NOCOL);

			arg_dec.name = splited_dec[i];

			if(i+1<splited_dec.size() && utils::is_in_string(CLOSE_BRACKET_ARRAY_DECLARATION, splited_dec[i+1]))
			{
				arg_dec.array_count = array_count_from_arg(splited_dec[i+1]);
				i++;
			}
			else
			{
				arg_dec.array_count = ARRAY_COUNT_SINGLE_ELEMENT;
			}

			infos.args.push_back(arg_dec);

			i++;
		}
	}

	//returns the number of character of the uniform::UNIFORM_NAME
	unsigned int ShaderParser::push_uniform(const string& line, unsigned int i, ShaderParser::functionInfo& infos) const
	{
		unsigned int c_count = uniform_key.size();
		i+=c_count;

		string uniform_name="";
		uniform_name.reserve(32);
		while(i<line.size() && (!utils::is_in_string(line[i], glsl_delim)))
		{
			uniform_name.push_back(line[i]);
			i++;
			c_count++;
		}
		utils::push_if_not_in(infos.uniforms, uniform_name);

		return c_count;
	}

	

	//returns the number of character of the local::LOCAL_NAME
	unsigned int ShaderParser::push_local(const string& line, unsigned int i, ShaderParser::functionInfo& infos) const
	{
		unsigned int c_count = local_key.size();
		i+=c_count;

		string local_name="";
		local_name.reserve(32);
		bool added = false;
		while(i<line.size() && (!utils::is_in_string(line[i], glsl_delim)))
		{
			if(line[i] == '(')
			{
				utils::push_if_not_in(infos.local_funcs, local_name);
				added = true;
				break;
			}
			local_name.push_back(line[i]);
			
			i++;
			c_count++;
		}
		if(!added)
		{
			if(i+1<line.size() && utils::is_in_string(line[i], ShaderPreprocessor::separators) && line[i+1] == '(')
			{
				utils::push_if_not_in(infos.local_funcs, local_name);
			}
			else
			{
				utils::push_if_not_in(infos.local_vars, local_name);
			}
		}

		return c_count;
	}

	void ShaderParser::fill_func_core_infos(ShaderParser::functionInfo& infos, std::ifstream& file) const
	{
		string line;
		string func_core_str = "";
		func_core_str.reserve(256 * sizeof(char));
		int acc_count = 1;
		while(getline(file, line))
		{
			if(line == "{")
				acc_count++;

			if(line == "}")
				acc_count--;

			if(acc_count==0)
				break;
		
			for(unsigned int i=0; i<line.size(); i++)
			{
				func_core_str.push_back(line[i]);
			}
			func_core_str.push_back('\n');

			for(unsigned int i=0; i<line.size(); i++)
			{
				if(utils::is_string_detected(line, i, uniform_key))
					i+=push_uniform(line, i, infos);

				if(utils::is_string_detected(line, i, local_key))
					i+=push_local(line, i, infos);
			}
		}
		func_core_str.push_back('\n');

		infos.function_core = func_core_str;
	}

	ShaderParser::functionInfo ShaderParser::parse_function(const std::string& function_name) const
	{
		this->check_file_open();
		_file.clear();
		_file.seekg(0);

		functionInfo infos;

		string func_dec = catch_func_dec(_file, function_name);

		if(func_dec == NOT_CATCHED)
		{
			err("ShaderParser: " + TERM::BLUE  + function_name + ERR_COLOR + " function asked not found");
		}

		vector<string> splited_dec = utils::split_novoid(func_dec, " ,()[\t");

		fill_head_fun_infos(infos, splited_dec);

		fill_func_core_infos(infos, _file);

		return infos;
	}

	std::vector<std::string> ShaderParser::harvest_variable_declaration(const std::string& variable_name) const
	{
		this->check_file_open();
		_file.clear();
		_file.seekg(0);

		string line = this->catch_var_header_str(_file, variable_name);

		if(line == NOT_CATCHED)
		{
			err("Cannot find declaration of variable " + TERM::BLUE + variable_name);
		}

		std::vector<std::string> lines(0);

		do
		{
			lines.push_back(line);
			for(char c : line)
			{
				if(c == ';')
				{
					return lines;
				}
			}

		} while(getline(_file, line));

		return lines;
	}


};

