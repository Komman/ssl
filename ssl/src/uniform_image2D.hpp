#ifndef _UNIFORM_IMAGE2D_HPP_
#define _UNIFORM_IMAGE2D_HPP_

#include "uniform_texture.hpp"
#include "image2D.hpp"

namespace ssl
{
	template<typename Type>
	class UniformImage2D : public UniformTexture<Type, Image2D<Type>> 
	{
	public:
		UniformImage2D(const std::string& name,
					   const glm::uvec2&  size,
					   const std::vector<Type>& data = std::vector<Type>(0));

		glm::uvec2 size();

	protected:

	private:
	};
};


namespace ssl
{
	template<typename Type>
	UniformImage2D<Type>::UniformImage2D(const std::string& name,
						 			   	 const glm::uvec2&  size,
						 			   	 const std::vector<Type>& data)
		: UniformTexture<Type, Image2D<Type>>(name, glm::uvec3(size.x, size.y, 1), data.size() == 0 ? NULL : (void*) data.data())
	{
		if(data.size() != 0 && data.size() < size.x*size.y)
		{
			err("try to build a Image2D of a total size of "+ std::to_string(size.x)+"x"
				+ std::to_string(size.y) + " = "  + std::to_string(size.x*size.y) 
				+ " but the data vector passed has a size of " + std::to_string(data.size()));
		}
	}

	template<typename Type>
	glm::uvec2 UniformImage2D<Type>::size()
	{
		return glm::uvec2(this->get_dimension_size().x, this->get_dimension_size().y);
	}
};


#endif //_UNIFORM_IMAGE2D_HPP_
