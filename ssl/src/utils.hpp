#ifndef _UTILS_HPP_
#define _UTILS_HPP_

#include <vector>
#include <string>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cstdint>


#define SSL_FILE_AND_LINE ("(" + std::string(__FILE__) + std::string(", line ") + std::to_string(__LINE__) + ")")

namespace utils
{
	template<typename T>
	void                     print_vector(const std::vector<T>& v);
	template<typename T>
	bool                     in_vector(const std::vector<T>& v, T e);
	template<typename T>
	void                     push_if_not_in(std::vector<T>& v, T e);
	template<typename T>
	std::string              string_from_vector(const std::vector<T> v);

	inline bool              is_string_detected(const std::string& line, unsigned int indice, const std::string& to_detect);
	
	bool                     is_in_string(char c, const std::string& str);
	
	std::vector<std::string> split_novoid(const std::string& str, const std::string& delimitors);
	
	unsigned int             extract_directory_index(const std::string& str);
	std::string              extract_directory(const std::string& str);
	std::pair<std::string, std::string>  extract_directory_and_file(const std::string& str);

	uint64_t                 get_miliseconds();		
	uint64_t                 get_nanoseconds();

	float random_float(float minimum, float maximum, int precision = 10000);
};


namespace utils
{
	inline bool is_string_detected(const std::string& line, unsigned int indice, const std::string& to_detect)
	{
		if((int)(line.size())-(int)(indice) < (int)(to_detect.size()))
			return false;

		unsigned int j=0;
		while(indice<line.size() && j<to_detect.size())
		{
			if(line[indice] != to_detect[j])
				return false;

			indice++;
			j++;
		}

		return true;
	}

	template<typename T>
	void print_vector(const std::vector<T>& v)
	{
		std::cout<<"{";
		for(auto& e : v)
		{
			std::cout<<" "<<e<<",";
		}
		std::cout<<"}";
	}

	template<typename T>
	bool in_vector(const std::vector<T>& v, T e)
	{
		return (!(std::find(v.begin(), v.end(), e) == v.end()));
	}

	template<typename T>
	void push_if_not_in(std::vector<T>& v, T e)
	{
		if(!in_vector(v, e))
		{
			v.push_back(e);
		}
	}

	template<typename T>
	std::string  string_from_vector(const std::vector<T> v)
	{
		std::string ret="";
		for(uint i = 0; i < v.size(); i++)
		{
			ret+=std::to_string(v[i]);
			if(i!=v.size()-1)
			{
				ret+=", ";
			}
		}
		return ret;
	}
};

#endif //_UTILS_HPP_
