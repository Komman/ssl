#ifndef _SHADER_BUILDER_HPP_
#define _SHADER_BUILDER_HPP_

#include "shader_parser.hpp"
#include "shader_pre_builder.hpp"
#include "types_id.hpp"

#include <string>
#include <vector>

namespace ssl
{

	/*
		TOFIX:

				/!\                           /!\  
	
		- DO NOT SUPPORT VERTEX/FRAGEMNT SHADERS THAT USES FUNCTIONS THAT HAVE 
		A LOCAL VARIABLE OF THE SAME NAME OF THEIR "IN" OR "OUT" ARGUMENTS
				 (UPDATE: MAYBE, NOT SURE)

				/!\                           /!\   
			
	*/

	class ShaderBuilder : public ShaderPreBuilder
	{
	public:

		ShaderBuilder(const BasicShaderParser& parser,
					  const std::string& vertex_shader_name,
			          const std::string& fragment_shader_name,
			   		  const std::vector<std::string>& fragment_modifiers);
		virtual ~ShaderBuilder() {}

		const std::vector<std::string>& get_uniforms_names();

		static unsigned int location_count(const std::string& type);


	protected:
		ShaderBuilder();

		// If all uniforms variable are declared, then initialize and compile the shader, and returns true
		// Else, do nothing and returns false
		bool try_to_initialize();
		// Force initialization of the shader, if all uniforms variable are not declared, raises an error
		void has_to_initialize();

		bool is_initialized() const;

		virtual std::string additionnal_name() const;

		struct vertexIN
		{
			unsigned int location;
			unsigned int location_count;
			bool         instanced;
			types_id     type;
		};

		const std::vector< vertexIN >&  get_vertex_ins();
		const std::vector< types_id >&  get_fragment_outs();

		const std::string&  get_vertex_function_name() const;
		const std::string&  get_fragment_function_name() const;

	private:
		static const uint VERTEX   = 0;
		static const uint FRAGMENT = 1;

		static inline std::string VERTEX_TO_FRAGMENT_ADD_ARG_NAME = "ssl_vertfragname_";

		void initialize();
		void print_lazy_compilation();
		void generate_shader(std::vector<std::ofstream>& files,
							 std::vector<std::string>&   shader_names,
   							const std::vector<std::string>& fragment_modifiers);
		void check_infos(std::vector<ShaderParser::functionInfo>& infos);
		void write_ins_outs(std::vector<std::ofstream>&              files,
					        std::vector<ShaderParser::functionInfo>& infos,
					        const std::vector<std::string>& fragment_modifiers);
		void write_necessary(std::vector<std::ofstream>&              files,
					         std::vector<ShaderParser::functionInfo>& infos,
   							 const std::vector<std::string>& fragment_modifiers);
		void write_vertex_out(std::vector<std::ofstream>&                     files,
					          std::vector<ShaderParser::functionInfo>&        infos,
					          std::vector<std::vector<ShaderParser::argDec>>& ins,
					          std::vector<std::vector<ShaderParser::argDec>>& outs
					          );
		void add_vertex_main_out_declarations(std::vector<ShaderParser::functionInfo>&        infos,
									          std::vector<std::vector<ShaderParser::argDec>>& ins,
									          std::vector<std::vector<ShaderParser::argDec>>& outs);
		void write_fragment_modifiers(ShaderParser::functionInfo& frag_infos,
									  const std::vector<std::string>& fragment_modifiers);
		void check_fragment_modifier(ShaderParser::functionInfo& frag_infos,
									 ShaderParser::functionInfo& modifier_infos);
		void check_modifiers_link_to_fragment(const ShaderParser::argDec& frag_arg,
											  const ShaderParser::argDec& modif_arg,
											  bool inout_arg,
											  const ShaderParser::functionInfo& frag_infos,
											  const ShaderParser::functionInfo& frag_modifier_func);
		int indices_to_next_in_argument(const std::vector<const ShaderParser::argDec*>& fragment_ordered_args, int fragment_arg_index);
		
		static std::string key_type_cond(const ShaderParser::argDec& arg, uint shader_type);

	private:
		std::vector< vertexIN > _vertex_ins;
		std::vector< types_id > _fragment_outs;
		std::vector< std::string > _uniforms;

		std::string _vertex_function_name;
		std::string _fragment_function_name;

		std::vector<std::string> _fragment_modifiers;

		bool _initialized;
		bool _constructor;
	};
};

#endif //_SHADER_BUILDER_HPP_
