#ifndef _SHADER_HPP_
#define _SHADER_HPP_

#include "debug.hpp"
#include "shader_builder.hpp"
#include "basic_uniform.hpp"
#include "basic_buffer.hpp"
#include "element_buffer.hpp"
#include "drawable_object.hpp"
#include "texture.hpp"
#include "uniform_texture_1D.hpp"
#include "uniform_texture_2D.hpp"
#include "uniform_texture_3D.hpp"
#include "vertex_array_object.hpp"

#include <string>
#include <vector>

namespace ssl
{
	using textureBinding = BasicUniform::reference;

	class Shader : public ShaderBuilder
	{
	public:
		Shader(const BasicShaderParser& parser,
			   const std::string& vertex_shader_name,
			   const std::string& fragment_shader_name,
			// Functions called on th fragment result, they must have
			// the same argument count as fragment "out" arguments count,
			// and all of them must be "inout"
			// By default, no functions are called
			   const std::vector<std::string>& fragment_modifiers  =  {}
		);
		virtual ~Shader(){}

		// If all uniforms variable are declared, then initialize and compile the shader, and returns true
		// Else, do nothing and returns false
		bool try_to_initialize();
		// Force initialization of the shader, if all uniforms variable are not declared, raises an error
		void has_to_initialize();

		bool is_initialized() const;
		
		void   set_drawing_mode(GLenum opengl_draw_mode);
		GLenum get_drawing_mode(GLenum opengl_draw_mode);

		void    set_max_drawn_index(GLsizei max_index);
		void    unset_max_drawn_index();
		GLsizei get_max_drawn_index();

		template<typename... Buffers>
		void draw_elements(const ElementBuffer& elements, const Buffers & ... buff);

		template<typename... Buffers>
		void draw(const Buffers & ... buff);

		void draw_elements(const ElementBuffer& elements, const std::vector<const BasicBuffer*>& buffers);
		void draw(const std::vector<const BasicBuffer*>& buffers);

		void draw_object(const DrawableObject& obj);
		void draw_object(const ElementDrawableObject& obj);

		void draw(const VertexArrayObject& vao);

		// complexity: O({used uniform count} * string copy)
		const textureBinding get_texture_binding(const std::string& texture_name);		
		
		// complexity: O(1)
		template<typename TextureType>
		void change_texture_binding(const textureBinding& texture_binding, const Texture<TextureType>& new_texture);
		template<typename TextureType>
		void change_texture_binding(const textureBinding& texture_binding, const UniformTexture1D<TextureType>& new_texture);
		template<typename TextureType>
		void change_texture_binding(const textureBinding& texture_binding, const UniformTexture2D<TextureType>& new_texture);
		template<typename TextureType>
		void change_texture_binding(const textureBinding& texture_binding, const UniformTexture3D<TextureType>& new_texture);

		std::vector<uint> buffers_divisors();

	private:
		Shader();
		Shader(const Shader& shader_to_copy);
		Shader(Shader&& shad);
		Shader& operator=(Shader&& shad);
		Shader& operator=(const Shader& shad);

		void vectorBuffer_draw(const std::vector<const BasicBuffer*>& buffers);
		void vectorBuffer_draw_elements(const ElementBuffer& elements, const std::vector<const BasicBuffer*>& buffers);

		void initialize();
		void update_uniforms();
		// Returns the number of location used
		uint bind_and_target_location_all_buffers(const std::vector<const BasicBuffer*>& buffers);
		void check_size(const std::vector<const BasicBuffer*>& buffers);
		void check_types(const std::vector<const BasicBuffer*>& buffers);
		void check_element_binding(const ElementBuffer& elements, const std::vector<const BasicBuffer*>& buffers);
		void check_instances_sizes(const std::vector<const BasicBuffer*>& buffers);
		void check_non_instances_sizes(const std::vector<const BasicBuffer*>& buffers);
		uint get_instance_count(const std::vector<const BasicBuffer*>& buffers);
		uint get_non_instance_buffer_size(const std::vector<const BasicBuffer*>& buffers);
		void direct_draw_elements(const ElementBuffer& elements, const std::vector<const BasicBuffer*>& buffers);
		void direct_draw(const std::vector<const BasicBuffer*>& buffers);

		bool no_need_to_draw(const std::vector<const BasicBuffer*>& buffers);
		bool no_need_to_draw_element(const ElementBuffer& elements, const std::vector<const BasicBuffer*>& buffers);
		void check_location_total(uint current_location, uint location_max_count);
		void check_every_locations(const std::vector<uint>& all_locations);
		std::string string_from_vertex_location();
		void enable_attribute_locations(uint count);
		void disable_attribute_locations(uint count);
		uint compute_max_vertex_location_count();
		std::vector<uint> get_instanced_locations();
		void dispatch_instance_locations();
		std::string get_msg_buffer_sizes(const std::vector<const BasicBuffer*>& buffers);

		template<typename... Buffers>
		void check_variadic_args(const Buffers & ... buffs);

		
		static uint store_locations(std::vector<uint>& buffer_locations, uint location, uint location_size);
	
	// Members
	private:

		uint                                 _max_vertex_location_count;
		std::vector<uint>                    _instanced_locations;
		GLenum                               _draw_type;
		GLsizei                              _max_drawn_index;
		std::vector<BasicUniform::reference> _uniform_used;
		std::vector<const BasicBuffer*>      _drawing_buffers;

		bool _not_same_size_warning;
		bool _initialized;
	};
};






#include "utils.hpp"
#include <GL/glew.h>

namespace ssl
{
	template<typename... Buffers>
	void Shader::draw(const Buffers & ... buff)
	{
		this->Shader::has_to_initialize();

		#ifdef SSL_DEBUG
		this->check_variadic_args(buff...);
		#endif

		uint i=0;
		((_drawing_buffers[i]=&buff, i++), ...);

		this->vectorBuffer_draw(_drawing_buffers);
	}

	template<typename... Buffers>
	void Shader::draw_elements(const ElementBuffer& elements, const Buffers & ... buff)
	{
		this->Shader::has_to_initialize();
		
		#ifdef SSL_DEBUG
		this->check_variadic_args(buff...);
		#endif

		uint i=0;
		((_drawing_buffers[i]=&buff, i++), ...);

		this->vectorBuffer_draw_elements(elements, _drawing_buffers);
	}


	template<typename... Buffers>
	void Shader::check_variadic_args(const Buffers & ... buffs)
	{
		if(this->get_vertex_ins().size() != sizeof...(buffs))
		{
			err("Shader::draw/draw_elements(...) : Invalid argument number for this Shader:\n" + TERM::NOCOL
				+ "Vertex Shader " + TERM::RED + "in" + TERM::NOCOL + " variables: " + std::to_string(this->get_vertex_ins().size())
				+ "\nBuffers passed as arguments : " + std::to_string(sizeof...(buffs)));
		}
	}		
	
	template<typename TextureType>
	void Shader::change_texture_binding(const textureBinding& texture_binding, const Texture<TextureType>& new_texture)
	{
		this->Shader::has_to_initialize();
		BasicUniform::change_texture_slot_binding(texture_binding, new_texture);
	}	
	
	template<typename TextureType>
	void Shader::change_texture_binding(const textureBinding& texture_binding, const UniformTexture1D<TextureType>& new_texture)
	{
		this->Shader::has_to_initialize();
		BasicUniform::change_texture_slot_binding(texture_binding, new_texture.get_texture());
	}
	template<typename TextureType>
	void Shader::change_texture_binding(const textureBinding& texture_binding, const UniformTexture2D<TextureType>& new_texture)
	{
		this->Shader::has_to_initialize();
		BasicUniform::change_texture_slot_binding(texture_binding, new_texture.get_texture());
	}
	template<typename TextureType>
	void Shader::change_texture_binding(const textureBinding& texture_binding, const UniformTexture3D<TextureType>& new_texture)
	{
		this->Shader::has_to_initialize();
		BasicUniform::change_texture_slot_binding(texture_binding, new_texture.get_texture());
	}


};

#endif //_SHADER_HPP_
