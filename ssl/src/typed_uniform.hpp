#ifndef _TYPED_UNIFORM_HPP_
#define _TYPED_UNIFORM_HPP_

#include "basic_uniform.hpp"
#include "texture.hpp"




namespace ssl
{
	template<typename Type>
	class TypedUniform : public BasicUniform
	{
	public:

	protected:
		TypedUniform(const std::string& name,
					 int array_count);

	private:
		TypedUniform(){}
		std::string get_str_type() const;
		std::string get_layout()   const;
	};

	template<typename Type>
	TypedUniform<Type>::TypedUniform(const std::string& name,
						             int array_count)
		: BasicUniform({this->get_str_type(),
						name,
						this->get_layout(),
						array_count})
	{
		
	}

	
};


#endif //_TYPED_UNIFORM_HPP_
