#include "basic_uniform.hpp"
#include "debug.hpp"

#include "shader_parser.hpp"

#include <string>

using namespace std;

namespace ssl
{
	std::vector<BasicUniformSlot*> BasicUniform::all_slots;



	BasicUniform::BasicUniform(const uniformInfo& infos)
		: _indice(-1)
	{
		ShaderPreBuilder::declare_uniform(infos);
	}

	std::vector<BasicUniform::reference> BasicUniform::add_shader_uniforms(unsigned int shader_ID, const std::vector<std::string>& uniform_names)
	{
		std::vector<reference> refs(0);

		unsigned int texture_count = 0;

		for(unsigned int s=0; s<all_slots.size(); s++)
		{
			BasicUniformSlot* slot = all_slots[s];

			for(unsigned int n=0; n<uniform_names.size(); n++)
			{
				const string& name = uniform_names[n];

				if(slot->get_name() == name)
				{
					reference ref;
					ref.slot_indice = s;
					ref.controller_indice = slot->add_uniform(shader_ID, texture_count);
					refs.push_back(ref);

					if(ShaderPreBuilder::is_uniform_texture(name))
					{
						GL(glUniform1i(glGetUniformLocation(shader_ID, name.c_str()), texture_count));
						texture_count++;
					}
				}
			}
		}
	
		return refs;
	}

	void BasicUniform::update_uniforms_data(const std::vector<reference>& uniform_references)
	{
		for(auto& ref : uniform_references)
		{
			#ifdef SSL_DEBUG
			if(ref.slot_indice >= all_slots.size())
				err("update_uniforms_data: slot out of range requested : " + to_string(ref.slot_indice) + " / " + to_string(all_slots.size()));
			#endif

			all_slots[ref.slot_indice]->send_data_to_shader(ref.controller_indice);
		}
	}

	std::string BasicUniform::get_uniform_name(const reference& ref)
	{
		return all_slots[ref.slot_indice]->get_name();
	}

	void BasicUniform::set_slot()
	{
		#ifdef SSL_DEBUG
		if(_indice != -1)
		{
			err("BasicUniform::set_slot: An uniform can be set only one time");
		}
		#endif
		
		_indice = all_slots.size();
		all_slots.push_back(this->get_instance());
	}

	unsigned int BasicUniform::get_indice() const
	{
		return _indice;
	}

	void BasicUniform::print_slots()
	{
		cout<<"Uniform slots: {"<<endl;
		for(unsigned int s=0; s<all_slots.size(); s++)
		{
			BasicUniformSlot* slot = all_slots[s];

			cout<<"\t"<<s<<": "<<slot->get_name()<<"\t";
			slot->print();
			cout<<endl;
		}
		cout<<"}"<<endl;
	}

};

