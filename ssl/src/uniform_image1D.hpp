#ifndef _UNIFORM_IMAGE1D_HPP_
#define _UNIFORM_IMAGE1D_HPP_

#include "uniform_texture.hpp"
#include "image1D.hpp"

namespace ssl
{
	template<typename Type>
	class UniformImage1D : public UniformTexture<Type, Image1D<Type>> 
	{
	public:
		UniformImage1D(const std::string& name,
					   unsigned int       size,
					   const std::vector<Type>& data = std::vector<Type>(0));

		unsigned int size();

	protected:

	private:
	};
};


namespace ssl
{
	template<typename Type>
	UniformImage1D<Type>::UniformImage1D(const std::string& name,
						 			   	 unsigned int       size,
						 			   	 const std::vector<Type>& data)
		: UniformTexture<Type, Image1D<Type>>(name, glm::uvec3(size, 1, 1), data.size() == 0 ? NULL : (void*) data.data())
	{
		if(data.size() != 0 && data.size() < size)
		{
			err("try to build a Image1D of a total size of "+ std::to_string(size) 
				+ " but the data vector passed has a size of " + std::to_string(data.size()));
		}
	}

	template<typename Type>
	unsigned int UniformImage1D<Type>::size()
	{
		return this->get_dimension_size().x;
	}
};

#endif //_UNIFORM_IMAGE1D_HPP_
