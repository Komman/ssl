#include "control_camera.hpp"

#include <vector>
#include <iostream>

#include "window.hpp"

using namespace std;
using namespace ssl;
using namespace glm;

namespace ssl
{
	ControlCamera::ControlCamera(const glm::vec3& initial_position, float min_view_distance, float max_view_distance, const std::vector<int>& control_keys, float view_field)
		: BasicCamera(initial_position, min_view_distance, max_view_distance, view_field),
		  _control_keys(control_keys),
		  _block_mouse(true)
	{
		mouse::set_pixel(dvec2(double(window::size().x), double(window::size().y))/2.0);
		_set_mouse_pixel = mouse::position_pixel();
	}

	void ControlCamera::set_new_control_keys(const std::vector<int>& control_keys)
	{
		_control_keys = control_keys;
	}

	void ControlCamera::frame_update()
	{		
		// KEYS
		vector<unsigned int> pressed_keys_indices(0);

		for(unsigned int i=0; i<_control_keys.size(); i++)
		{
			if(keyboard::key_pressed(_control_keys[i]))
			{
				pressed_keys_indices.push_back(i);
			}
		}
		this->keys_reaction(pressed_keys_indices);


		if(_block_mouse && keyboard::key_pressed(SSL_KEY(ESCAPE)))
		{
			_block_mouse=false;
			mouse::set_visibility(true);
		}

		// MOUSE
		dvec2 current_mouse_pos = mouse::position_pixel();

		if(_block_mouse)
		{
			this->mouse_reaction(current_mouse_pos - _set_mouse_pixel);
			mouse::set_pixel(_set_mouse_pixel);
		}

		// ADDITIONNAL ACTIONS

		this->frame_update_more_actions();
	}
};
