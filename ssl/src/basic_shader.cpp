#include "basic_shader.hpp"

#include <GLFW/glfw3.h>
#include <vector>
#include <glm/glm.hpp> 
#include <iostream> 
#include <fstream>
#include <cstring>

#include "debug.hpp"
#include "color.hpp"
#include "autoprint_recorder.hpp"

using namespace std;

namespace ssl
{
	GLuint BasicShader::_CURRENT_SHADER_ID = 0;

	BasicShader::BasicShader()
		: _ID(0)
	{
		
	}

	BasicShader::~BasicShader()
	{
		GL(glDeleteProgram(_ID));
	}
	
	std::string BasicShader::additionnal_name() const
	{
		return "basic shader";
	}


	void BasicShader::use() const
	{
		if(_ID == 0)
		{
			err("Try to use BasicShader::use() while no shader is loaded");
		}

		if(_CURRENT_SHADER_ID != _ID)
		{
			#ifdef SSL_PRINTS
			if(ssl::are_prints_enable(SSL_PRINTS_DRAW))
			{
				ssl::prints_msg(SSL_PRINTS_DRAW, TERM::CYAN + "USING SHADER N: " + PRINTS_COLOR + std::to_string(_ID) + "\t(" + this->additionnal_name() + PRINTS_COLOR + ")");
			}
			#endif

			GL(glUseProgram(_ID));
			_CURRENT_SHADER_ID = _ID;
		}
	}

	GLuint BasicShader::get_ID() const
	{
		if(_ID == 0)
		{
			err("Try to use BasicShader::get_ID() while no shader is loaded");
		}
		
		return _ID;
	}

	void BasicShader::print_error_msg(const std::vector<char>& err_msg, const std::string& file_path) const
	{
		err_loc        loc         = get_error_location(err_msg);
		vector<string> error_lines = this->get_lines(file_path, loc.line - LINE_PRINTING_COUT_ERROR -2, loc.line + LINE_PRINTING_COUT_ERROR -2);

		cout<<TERM::RED<<"GLSL ERROR:"<<TERM::NOCOL<<endl;
		printf("%s\n", &err_msg[0]);
		cout<<TERM::ORANGE<<"In generated  shader code:"<<TERM::NOCOL<<endl;
		cout << TERM::ORANGE << " | ..." << endl;

		for(uint l=0; l<error_lines.size(); l++)
		{

			if(l != LINE_PRINTING_COUT_ERROR)
			{
				cout << TERM::ORANGE << " | " << TERM::NOCOL;
				cout << error_lines[l] <<endl;
			}
			else
			{
				cout<<TERM::RED;

				cout << " X ";

				uint char_i = 0;
				while(char_i<error_lines[l].size() && char_i<loc.character-1)
				{
					cout<<error_lines[l][char_i];
					char_i++;
				}

				cout<<TERM::ORANGE;

				cout<<error_lines[l][char_i];
				char_i++;

				cout<<TERM::RED;

				while(char_i<error_lines[l].size())		
				{
					cout<<error_lines[l][char_i];
					char_i++;
				}

				cout<<TERM::NOCOL<<endl;
			}
		}
		cout << TERM::ORANGE << " |..." << endl;
	}

	BasicShader::err_loc BasicShader::get_error_location(const std::vector<char>& err_msg) const
	{
		err_loc loc;
		string number="";
		bool nvidia_compilation = false;

		if(err_msg.size() == 0)
		{
			err("call to get_error_location while the error message is empty");
		}

		// File number
		unsigned int i=0;
		while(i<err_msg.size() && err_msg[i]!=':' && err_msg[i]!='(')
		{
			number.push_back(err_msg[i]);
			i++;
		}
		if(number=="")
		{
			loc.file = 0;
		}
		else
		{
			loc.file = stoi(number);
		}
		

		if(i<err_msg.size() && err_msg[i] == '(')
			nvidia_compilation = true;

		// Line number
		i++;
		number="";
		while(i<err_msg.size() && err_msg[i]!='(' && err_msg[i]!=')')
		{
			number.push_back(err_msg[i]);
			i++;
		}
		loc.line = stoi(number);

		// Character number
		if(nvidia_compilation)
		{
			loc.character = 0;
		}
		else
		{
			i++;
			number="";
			while(i<err_msg.size() && err_msg[i]!=')')
			{
				number.push_back(err_msg[i]);
				i++;
			}
			loc.character = stoi(number);
		}
		

		return loc;
	}

	vector<string> BasicShader::get_lines(const std::string& file_path, unsigned int begin, unsigned int end) const
	{
		vector<string> selected_lines(0);
		ifstream getter(file_path);

		if(!getter.is_open())
			err("Sahder error catching (compilation) : cannot open " + file_path);

		string line; 
		uint n =0;
		while(getline(getter, line))
		{
			if(n>=begin && n<=end)
			{
				selected_lines.push_back(line);
			}
			n++;
		}

		getter.close();

		return selected_lines;
	}


	void BasicShader::load(const std::string& vertex_file_path, const std::string& fragment_file_path)
	{ 
 
	    // Crée les shaders 
	    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER); 
	    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER); 
	 
	    // Lit le code du vertex shader à partir du fichier
	    std::string VertexShaderCode; 
	    std::ifstream VertexShaderStream(vertex_file_path, std::ios::in); 
	    if(VertexShaderStream.is_open()) 
	    { 
	        std::string Line = ""; 
	        while(getline(VertexShaderStream, Line)) 
	            VertexShaderCode += "\n" + Line; 
	        VertexShaderStream.close(); 
	    } 
	 
	    // Lit le code du fragment shader à partir du fichier
	    std::string FragmentShaderCode; 
	    std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in); 
	    if(FragmentShaderStream.is_open()){ 
	        std::string Line = ""; 
	        while(getline(FragmentShaderStream, Line)) 
	            FragmentShaderCode += "\n" + Line; 
	        FragmentShaderStream.close(); 
	    } 
	 
	    GLint Result = GL_FALSE; 
	    int InfoLogLength; 
	 
	    // Compile le vertex shader 
	    if constexpr (PRINT_SHADERS_LOAD)
	    {
	    	cout<<TERM::PURPLE<<"Compiling vertex shader   : "<<TERM::NOCOL	<<vertex_file_path;
	    	std::cout.flush();

	    }
	    char const * VertexSourcePointer = VertexShaderCode.c_str(); 
	    glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL); 
	    glCompileShader(VertexShaderID); 
	 	
	    // Vérifie le vertex shader 
	    glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result); 
	    glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength); 

	    std::vector<char> VertexShaderErrorMessage(InfoLogLength+1); 
	    glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]); 
	    if constexpr (PRINT_SHADERS_LOAD)
	    {	
	    	if(InfoLogLength == 0)
	    	{
	    		cout<<TERM::GREEN<<" Compiled"<<TERM::NOCOL<<endl;
	    	}
	    	else
	    	{
	    		cout<<TERM::RED<<" Failed"<<endl<<endl<<TERM::NOCOL;
	    		print_error_msg(VertexShaderErrorMessage, vertex_file_path); 
		    	cout<<TERM::NOCOL;
		    	exit(-1);
	    	}
	    }
	    if constexpr (PRINT_SHADERS_COMPILATION && !PRINT_SHADERS_LOAD)
	    {	
	    	if(InfoLogLength != 0)
	    	{
	    		cout<<TERM::RED<<"Compilation of "<<vertex_file_path<<" Failed"<<endl<<endl<<TERM::NOCOL;
	    		print_error_msg(VertexShaderErrorMessage, vertex_file_path); 
		    	cout<<TERM::NOCOL;
		    	exit(-1);
	    	}
	    }
	 
	    // Compile le fragment shader 
	    if constexpr (PRINT_SHADERS_LOAD)
	    {
	    	cout<<TERM::PURPLE<<"Compiling fragment shader : "<<TERM::NOCOL<<fragment_file_path;
	    	std::cout.flush(); 
	    }
	    char const * FragmentSourcePointer = FragmentShaderCode.c_str(); 
	    glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL); 
	    glCompileShader(FragmentShaderID); 
	 	
	    // Vérifie le fragment shader 
	    glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result); 
	    glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength); 
	    std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1); 
	    glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]); 
	    if constexpr (PRINT_SHADERS_LOAD)
	    {
	    	if(InfoLogLength == 0)
	    	{
	    		cout<<TERM::GREEN<<" Compiled"<<TERM::NOCOL<<endl;
	    	}
	    	else
	    	{
	    		cout<<TERM::RED<<" Failed"<<endl<<endl<<TERM::NOCOL;
	    		print_error_msg(FragmentShaderErrorMessage, fragment_file_path);  
		    	cout<<TERM::NOCOL;
		    	exit(-1);
	    	}
	    }
	    if constexpr (PRINT_SHADERS_COMPILATION && !PRINT_SHADERS_LOAD)
	    {	
	    	if(InfoLogLength != 0)
	    	{
	    		cout<<TERM::RED<<"Compilation of "<<fragment_file_path<<" Failed"<<endl<<endl<<TERM::NOCOL;
	    		print_error_msg(FragmentShaderErrorMessage, fragment_file_path);  
		    	cout<<TERM::NOCOL;
		    	exit(-1);
	    	}
	    }
	 
	    // Lit le programme
	    if constexpr (PRINT_SHADERS_LOAD)
	    {
	    	cout<<TERM::CYAN<<"Linking program :"<<TERM::NOCOL;
	    	std::cout.flush(); 
	    }
	    _ID = glCreateProgram(); 
	    glAttachShader(_ID, VertexShaderID); 
	    glAttachShader(_ID, FragmentShaderID); 
	    glLinkProgram(_ID); 
	 
	    // Vérifie le programme
	    glGetProgramiv(_ID, GL_LINK_STATUS, &Result); 
	    glGetProgramiv(_ID, GL_INFO_LOG_LENGTH, &InfoLogLength); 
	    std::vector<char> ProgramErrorMessage( std::max(InfoLogLength, int(1)) ); 
	    glGetProgramInfoLog(_ID, InfoLogLength, NULL, &ProgramErrorMessage[0]); 
	    if constexpr (PRINT_SHADERS_LOAD)
	    {
	    	if(InfoLogLength == 0)
	    	{
	    		cout<<TERM::GREEN<<" Linked"<<TERM::NOCOL<<endl;
	    	}
	    	else
	    	{
	    		cout<<TERM::RED<<" Failed"<<endl<<endl<<TERM::NOCOL;
	    		printf("%s\n", &ProgramErrorMessage[0]);
	    		cout<<TERM::NOCOL;
		    	exit(-1);
	    	}
	    }
	 
	    GL(glDeleteShader(VertexShaderID)); 
	    GL(glDeleteShader(FragmentShaderID)); 
	}

	void BasicShader::load_compute(const std::string& compute_path)
	{ 
 
	    GLuint compute_id = glCreateShader(GL_COMPUTE_SHADER); 
	 
	    std::string compute_code; 
	    std::ifstream compute_stream(compute_path, std::ios::in); 
	    if(compute_stream.is_open()) 
	    { 
	        std::string line = ""; 
	        while(getline(compute_stream, line)) 
	            compute_code += "\n" + line; 
	        compute_stream.close(); 
	    } 
	    else
	    {
	    	err("Cannot open compute shader file : " + compute_path);
	    }
	 
	    GLint Result = GL_FALSE; 
	    int InfoLogLength; 
	 
	    if constexpr (PRINT_SHADERS_LOAD)
	    {
	    	cout<<TERM::PURPLE<<"Compiling compute shader  : "<<TERM::NOCOL	<<compute_path;
	    	std::cout.flush();

	    }

	    char const * compute_SourcePointer = compute_code.c_str(); 
	    glShaderSource(compute_id, 1, &compute_SourcePointer , NULL); 
	    glCompileShader(compute_id); 
	 	
	    glGetShaderiv(compute_id, GL_COMPILE_STATUS, &Result); 
	    glGetShaderiv(compute_id, GL_INFO_LOG_LENGTH, &InfoLogLength); 

	    std::vector<char> compute_ShaderErrorMessage(InfoLogLength+1); 
	    glGetShaderInfoLog(compute_id, InfoLogLength, NULL, &compute_ShaderErrorMessage[0]); 
	    if constexpr (PRINT_SHADERS_LOAD)
	    {	
	    	if(InfoLogLength == 0)
	    	{
	    		cout<<TERM::GREEN<<" Compiled"<<TERM::NOCOL<<endl;
	    	}
	    	else
	    	{
	    		cout<<TERM::RED<<" Failed"<<endl<<endl<<TERM::NOCOL;
	    		print_error_msg(compute_ShaderErrorMessage, compute_path); 
		    	cout<<TERM::NOCOL;
		    	exit(-1);
	    	}
	    }

	    if constexpr (PRINT_SHADERS_LOAD)
	    {
	    	cout<<TERM::CYAN<<"Linking program :"<<TERM::NOCOL;
	    	std::cout.flush(); 
	    }

	    _ID = glCreateProgram(); 
	    glAttachShader(_ID, compute_id); 
	    glLinkProgram(_ID); 
	 
	    glGetProgramiv(_ID, GL_LINK_STATUS, &Result); 
	    glGetProgramiv(_ID, GL_INFO_LOG_LENGTH, &InfoLogLength); 
	    std::vector<char> ProgramErrorMessage( std::max(InfoLogLength, int(1)) ); 
	    glGetProgramInfoLog(_ID, InfoLogLength, NULL, &ProgramErrorMessage[0]); 
	    if constexpr (PRINT_SHADERS_LOAD)
	    {
	    	if(InfoLogLength == 0)
	    	{
	    		cout<<TERM::GREEN<<" Linked"<<TERM::NOCOL<<endl;
	    	}
	    	else
	    	{
	    		cout<<TERM::RED<<" Failed"<<endl<<endl<<TERM::NOCOL;
	    		printf("%s\n", &ProgramErrorMessage[0]);
	    		cout<<TERM::NOCOL;
		    	exit(-1);
	    	}
	    }
	 
	    GL(glDeleteShader(compute_id)); 
	}
};