#ifndef _BASIC_CAMERA_HPP_
#define _BASIC_CAMERA_HPP_

#include "view.hpp"

namespace ssl
{
	class BasicCamera : public View
	{
	public:
		BasicCamera(const glm::vec3& initial_position,
					float min_view_distance,
					float max_view_distance,
					float view_field = DEFAULT::VIEW_FIELD);

		virtual void frame_update()=0;
	};
};

#endif //_BASIC_CAMERA_HPP_
