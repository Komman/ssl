#include "uniform_camera.hpp"

#define CREATE_CAMERA_UNIFORM(name, type)\
	if(name ## _uniform_name != "")\
	{\
		_##name = std::make_unique<Uniform<glm::type>>(name ## _uniform_name,  this->get_ ## name ());\
	}

#define CREATE_CAMERA_CHANGECALL(name)\
		if(_##name.get() != NULL)\
		{\
			_##name->change_value(this->get_##name());\
		}

namespace ssl
{
	UniformCamera::UniformCamera(const glm::vec3& initial_position,
								 float min_view_distance,
								 float max_view_distance,
								 float speed,
								 const std::string& MVP_uniform_name,
								 const std::string& position_uniform_name,
								 const std::string& direction_uniform_name,
								 const std::string& right_vector_uniform_name,
								 const std::string& up_head_uniform_name)
		: Camera(initial_position, min_view_distance, max_view_distance, speed)
	{
		CREATE_CAMERA_UNIFORM(MVP, mat4)
		CREATE_CAMERA_UNIFORM(position,     vec3)
		CREATE_CAMERA_UNIFORM(direction,    vec3)
		CREATE_CAMERA_UNIFORM(right_vector, vec3)
		CREATE_CAMERA_UNIFORM(up_head,      vec3)
	}

	void UniformCamera::update_uniforms()
	{
		CREATE_CAMERA_CHANGECALL(MVP)
		CREATE_CAMERA_CHANGECALL(position)
		CREATE_CAMERA_CHANGECALL(direction)
		CREATE_CAMERA_CHANGECALL(right_vector)
		CREATE_CAMERA_CHANGECALL(up_head)
	}


	void UniformCamera::frame_update()
	{
		this->ControlCamera::frame_update();
		this->update_uniforms();
	}
};

