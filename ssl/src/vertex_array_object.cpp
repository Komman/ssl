#include "vertex_array_object.hpp"

#include "shader.hpp"
#include "debug.hpp"

using namespace std;

namespace ssl
{
	VertexArrayObject::VertexArrayObject(VertexArrayObject&& vao)
		: _id(vao._id),
		  _buffers(vao._buffers),
		  _elements(vao._elements),
		  _instanced(vao._instanced),
		  _buffers_vector_only(vao._buffers_vector_only)
	{
		vao._id = 0;
	}


	VertexArrayObject::VertexArrayObject(const std::vector<bufferDivisor>& buffers, const ElementBuffer* elements)
		: _buffers(buffers),
		  _elements(elements),
		  _instanced(false),
		  _buffers_vector_only()
	{
		this->bind_and_atach_buffers();	
		if(_elements != NULL)
		{
			_elements->bind();
			_elements->update_GPU();
		}
		this->unbind();
	}
	
	VertexArrayObject::VertexArrayObject(const std::vector<bufferDivisor>& buffers)
		: VertexArrayObject(buffers, NULL)
	{

	}

	VertexArrayObject::VertexArrayObject(const std::vector<const BasicBuffer*>& buffers)
		: VertexArrayObject(compute_divisors_no_instances(buffers))
	{
		
	}

	VertexArrayObject::VertexArrayObject(const std::vector<const BasicBuffer*>& buffers, const ElementBuffer* elements)
		: VertexArrayObject(compute_divisors_no_instances(buffers), elements)
	{

	}

	VertexArrayObject::VertexArrayObject(Shader* shader, const std::vector<const BasicBuffer*>& buffers)
		: VertexArrayObject(compute_divisors(shader, buffers))
	{
		
	}

	void VertexArrayObject::unbind() const
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY_FLOW))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY_FLOW, "glBindVertexArray( " + std::to_string(0) + " )");
		}
		#endif

		GL(glBindVertexArray(0));
	}

	const std::vector<const BasicBuffer*>& VertexArrayObject::get_buffers() const
	{
		if(_buffers_vector_only.size() == 0)
		{
			for(const bufferDivisor& b : _buffers)
			{
				_buffers_vector_only.push_back(b.buffer);	
			}
		}

		return _buffers_vector_only;
	}

	void VertexArrayObject::bind_and_atach_buffers()
	{	
		GL(glGenVertexArrays(1, &_id));

		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY, "glGenVertexArrays( 1, " + std::to_string(_id) + " )");
		}
		#endif

		this->build();
	}

	bool VertexArrayObject::elements_bound() const
	{
		return (_elements != NULL);
	}

	bool VertexArrayObject::instanced() const
	{
		return _instanced;
	}

	const ElementBuffer* VertexArrayObject::get_elements() const
	{
		return _elements;
	}

	void VertexArrayObject::bind() const
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY_FLOW))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY_FLOW, "glBindVertexArray( " + std::to_string(_id) + " )");
		}
		#endif

		this->update_buffers_GPU();
		GL(glBindVertexArray(_id));
	}

	void VertexArrayObject::update_buffers_GPU() const
	{
		for(uint i=0; i<_buffers.size(); i++)
		{
			_buffers[i].buffer->update_GPU();
		}

		if(_elements != NULL)
		{
			_elements->update_GPU();
		}
	}

	void VertexArrayObject::build()
	{
		this->bind();

		uint location = 0;
		for(uint i=0; i<_buffers.size(); i++)
		{
			if(_buffers[i].divisor != 0)
			{
				_instanced = true;
			}

			#ifdef SSL_PRINTS
			if(ssl::are_prints_enable(SSL_PRINTS_PARAMS_SETTING))
			{
				ssl::prints_msg(SSL_PRINTS_PARAMS_SETTING, "glEnableVertexAttribArray( " + std::to_string(location) + " )");
				ssl::prints_msg(SSL_PRINTS_PARAMS_SETTING, "glVertexAttribDivisor( " + std::to_string(location) + ", " + std::to_string(_buffers[i].divisor) + " )");
			}
			#endif

			GL(glEnableVertexAttribArray(location));
			GL(glVertexAttribDivisor(location, _buffers[i].divisor));

			_buffers[i].buffer->bind();
			location += _buffers[i].buffer->target_vertex_location(location);
		}
	}
	
	VertexArrayObject::~VertexArrayObject()
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY, "glBindVertexArray( " + std::to_string(_id) + " )");
		}
		#endif

		if(_id > 0)
		{
			GL(glDeleteVertexArrays(1, &_id));
		}
	}


	std::vector<bufferDivisor> VertexArrayObject::compute_divisors_no_instances(const std::vector<const BasicBuffer*>& buffers)
	{
		std::vector<bufferDivisor> ret;
		
		for(uint i=0; i<buffers.size(); i++)
		{
			ret.push_back(bufferDivisor{buffers[i], 0u});
		}
		
		return ret;
	}

	std::vector<bufferDivisor> VertexArrayObject::compute_divisors(Shader* shader, const std::vector<const BasicBuffer*>& buffers)
	{
		std::vector<bufferDivisor> ret;

		std::vector<uint> divisors;

		if(shader != NULL)
		{
			divisors = shader->buffers_divisors();		
		}
		else
		{
			divisors = std::vector<uint>(buffers.size(), 0);
		}

		if(divisors.size() != buffers.size())
		{
			ssl::err("VertexArrayObject::compute_divisors(): the shader must have the same input buffer than the passed buffer vector");
		}	

		for(uint i=0; i<buffers.size(); i++)
		{
			ret.push_back(bufferDivisor{buffers[i], divisors[i]});
		}

		return ret;
	}
};

