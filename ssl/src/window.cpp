#include <iostream>
#include <string>
#include <map>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp> 

#include "window.hpp"
#include "basic_window.hpp"
#include "debug.hpp"
#include "global_drawing.hpp"


using namespace ssl;
using namespace std;
using namespace glm;

namespace ssl
{
	static glVersion gl_version_cache = {0,0,0};
	static BasicWindow* win;
	static int KEYS_AZERTY_TO_QWERTY[GLFW_KEY_LAST];
	static int KEYS_QWERTY_TO_AZERTY[GLFW_KEY_LAST];

	static std::map<int, int> OPENGL_TO_GLSL_VERSIONS = {
		{200, 110},
		{210, 120},
		{300, 130},
		{310, 140},
		{320, 150}
	} ;

    static inline void setk_azerty_to_qwerty(int key1, int key2)
	{
		KEYS_AZERTY_TO_QWERTY[key1]=key2;
		KEYS_QWERTY_TO_AZERTY[key2]=key1;
	}
    static inline void switch_keys(int key1, int key2)
	{
		setk_azerty_to_qwerty(key1, key2);
		setk_azerty_to_qwerty(key2, key1);
	}
	static void adapt_keyboard_to_azerty()
	{
		for(int i=0;i<GLFW_KEY_LAST;i++)
		{
			KEYS_AZERTY_TO_QWERTY[i]=i;
			KEYS_QWERTY_TO_AZERTY[i]=i;
		}
		//TOFAIREQUELQUECHOSE POUR CET INCAPABLE DE GLFW ET AZERTY
		switch_keys('A','Q');
		switch_keys('W','Z');
		setk_azerty_to_qwerty((unsigned int)'M',';');
		setk_azerty_to_qwerty((unsigned int)';',',');
		setk_azerty_to_qwerty((unsigned int)',','M');

		setk_azerty_to_qwerty((unsigned int)':','.');
		setk_azerty_to_qwerty((unsigned int)'!','/');
		// setk_azerty_to_qwerty((unsigned int)'§','?');
		setk_azerty_to_qwerty((unsigned int)'/','>');
		setk_azerty_to_qwerty((unsigned int)'\'','<');
	}


	namespace window
	{
		void create(uint size_x, uint size_y, const std::string& name)
		{
			win =new BasicWindow(size_x, size_y, name);
			global_drawing::antialiasing(true);
		}
		void create(const std::string& name)
		{
			win = new BasicWindow(name);
			global_drawing::antialiasing(true);
		}
		void create(const std::string& name, unsigned int window_divisor)
		{
			win = new BasicWindow(name, window_divisor);
			global_drawing::antialiasing(true);
		}
		void clear()
		{
			win->clear();
		}
		void flip()
		{
			win->flip();
		}
		void reset_frame_time()
		{
			win->reset_frame_time();
		}
		void set_char_callback(void(*f)(GLFWwindow* w, unsigned int codepoint))
		{
			win->set_char_callback(f);
		}
		void set_key_callback(void(*f) (GLFWwindow *window, int key, int scancode, int action, int mods))
		{
			win->set_key_callback(f);
		}
		void close()
		{
			win->close();
			delete win;
		}
		const glm::ivec2& size()
		{
			return win->_win_size;
		}
		float delta_time()
		{
			return BasicWindow::delta_time();
		}
		bool should_close()
		{
			return glfwWindowShouldClose(win->_window);
		}
		void clear_color(const glm::vec4& color)
		{
			global_drawing::clear_color(color);
		}

		static void init_gl_version()
		{
			glGetIntegerv(GL_MAJOR_VERSION, &(gl_version_cache.major)); 
			glGetIntegerv(GL_MINOR_VERSION, &(gl_version_cache.minor)); 
			
			int parsed_opengl = 100*gl_version_cache.major + 10*gl_version_cache.minor;
			auto mversion = OPENGL_TO_GLSL_VERSIONS.find(parsed_opengl);
			if(mversion != OPENGL_TO_GLSL_VERSIONS.end())
			{
				gl_version_cache.glsl=mversion->second;
			}
			else
			{
				gl_version_cache.glsl=parsed_opengl;
			}
		}

		glVersion get_gl_version()
		{
			if(gl_version_cache.major == 0)
			{
				init_gl_version();
			}
			return gl_version_cache;
		}
	};

	namespace mouse
	{
		static bool mouse_in_game = true;

		glm::dvec2 position_pixel()
		{
			dvec2 p;
			GL(glfwGetCursorPos(win->_window, &p.x, &p.y));
			return p;
		}
		void set_pixel(const glm::dvec2& pos)
		{
			GL(glfwSetCursorPos(win->_window, pos.x, pos.y));
		}
		glm::dvec2 position()
		{
			ivec2 w = win->size();
			dvec2 p = mouse::position_pixel();
			return dvec2(p.x/double(w.x), p.y/double(w.y));
		}
		void set(const glm::dvec2& pos)
		{
			ivec2 w  = win->size();
			dvec2 wp = dvec2(double(w.x), double(w.y));
			mouse::set_pixel(pos*wp);
		}
		void set_visibility(bool visibility)
		{
			if(visibility)
			{
				GL(glfwSetInputMode(win->_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL));
			}
			else
			{
				GL(glfwSetInputMode(win->_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN));
			}
		}
		bool in_game()
		{
			return mouse_in_game;
		}
		void quit_game()
		{
			mouse_in_game=false;
			mouse::set_visibility(1);
		}
		void enter_game()
		{
			mouse_in_game=true;
			mouse::set_visibility(0);
		}
	};

	namespace keyboard
	{
		int azery_to_qwerty(int key)
		{
			if(key >= GLFW_KEY_LAST || key < 0)
			{
				return key;
				// err("ssl::azery_to_qwerty(key): non valid key: " + std::to_string(key));
			}
			return KEYS_AZERTY_TO_QWERTY[key];
		}
		int qwerty_to_azerty(int key)
		{
			if(key >= GLFW_KEY_LAST || key < 0)
			{
				return key;
				// err("ssl::qwerty_to_azerty(key): non valid key: " + std::to_string(key));
			}
			return KEYS_QWERTY_TO_AZERTY[key];
		}

		void init()
		{
			adapt_keyboard_to_azerty();
		}

		bool key_pressed(int key)
		{
			return glfwGetKey(win->_window, azery_to_qwerty(key)) == GLFW_PRESS;
		}
	};

};

/*   29/06/2022
==18010== Memcheck, a memory error detector
==18010== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==18010== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==18010== Command: ./run
==18010== 
==18010== 
==18010== HEAP SUMMARY:
==18010==     in use at exit: 278,284 bytes in 3,633 blocks
==18010==   total heap usage: 38,161 allocs, 34,528 frees, 15,390,480 bytes allocated
==18010== 
==18010== LEAK SUMMARY:
==18010==    definitely lost: 7,157 bytes in 4 blocks
==18010==    indirectly lost: 0 bytes in 0 blocks
==18010==      possibly lost: 0 bytes in 0 blocks
==18010==    still reachable: 271,127 bytes in 3,629 blocks
==18010==         suppressed: 0 bytes in 0 blocks
==18010== Rerun with --leak-check=full to see details of leaked memory
==18010== 
==18010== For lists of detected and suppressed errors, rerun with: -s
==18010== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 2 from 2)
*/

/*

====== PASSING TO NVIDIA DRIVERS (01/08/2022) ======

==10297== 
==10297== HEAP SUMMARY:
==10297==     in use at exit: 216,419 bytes in 1,673 blocks
==10297==   total heap usage: 16,283 allocs, 14,610 frees, 413,040,882 bytes allocated
==10297== 
==10297== LEAK SUMMARY:
==10297==    definitely lost: 7,176 bytes in 4 blocks
==10297==    indirectly lost: 144,289 bytes in 802 blocks
==10297==      possibly lost: 896 bytes in 1 blocks
==10297==    still reachable: 64,058 bytes in 866 blocks
==10297==         suppressed: 0 bytes in 0 blocks
==10297== Rerun with --leak-check=full to see details of leaked memory
==10297== 
==10297== Use --track-origins=yes to see where uninitialised values come from
==10297== For lists of detected and suppressed errors, rerun with: -s
==10297== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)




====== UPDATING NVIDIA DRIVERS (version 515.65.01) (06/08/2022) ======

==16041== Memcheck, a memory error detector
==16041== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==16041== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==16041== Command: ./run
==16041== 
==16041== Warning: unimplemented fcntl command: 1033

...

==16041== 
==16041== HEAP SUMMARY:
==16041==     in use at exit: 234,835 bytes in 1,788 blocks
==16041==   total heap usage: 23,201 allocs, 21,413 frees, 426,436,621 bytes allocated
==16041== 
==16041== LEAK SUMMARY:
==16041==    definitely lost: 8,476 bytes in 26 blocks
==16041==    indirectly lost: 160,462 bytes in 893 blocks
==16041==      possibly lost: 1,792 bytes in 2 blocks
==16041==    still reachable: 64,105 bytes in 867 blocks
==16041==         suppressed: 0 bytes in 0 blocks
==16041== Rerun with --leak-check=full to see details of leaked memory
==16041== 
==16041== For lists of detected and suppressed errors, rerun with: -s
==16041== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)



========= 02/08/2022 ===========

==48110== HEAP SUMMARY:
==48110==     in use at exit: 234,835 bytes in 1,788 blocks
==48110==   total heap usage: 38,365 allocs, 36,577 frees, 750,697,932 bytes allocated
==48110== 
==48110== LEAK SUMMARY:
==48110==    definitely lost: 8,292 bytes in 25 blocks
==48110==    indirectly lost: 151,426 bytes in 841 blocks
==48110==      possibly lost: 11,012 bytes in 55 blocks
==48110==    still reachable: 64,105 bytes in 867 blocks
==48110==         suppressed: 0 bytes in 0 blocks
==48110== Rerun with --leak-check=full to see details of leaked memory
==48110== 
==48110== For lists of detected and suppressed errors, rerun with: -s
==48110== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)


========= 26/09/2022 ===========

==13243== HEAP SUMMARY:
==13243==     in use at exit: 234,787 bytes in 1,788 blocks
==13243==   total heap usage: 32,776 allocs, 30,988 frees, 699,192,533 bytes allocated
==13243== 
==13243== LEAK SUMMARY:
==13243==    definitely lost: 8,292 bytes in 25 blocks
==13243==    indirectly lost: 160,528 bytes in 892 blocks
==13243==      possibly lost: 1,862 bytes in 4 blocks
==13243==    still reachable: 64,105 bytes in 867 blocks
==13243==         suppressed: 0 bytes in 0 blocks
==13243== Rerun with --leak-check=full to see details of leaked memory


*/
