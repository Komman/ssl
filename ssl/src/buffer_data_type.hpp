#ifndef _BUFFER_DATA_TYPE_HPP_
#define _BUFFER_DATA_TYPE_HPP_

#include <glm/glm.hpp> 

#define SSL_GENREATE_BUFFER_DATA_TYPE(buffer_type, data_type)\
	template<>\
	struct buffer_data_type<buffer_type>{\
		using type = data_type;\
	};

namespace ssl
{
	template<typename Type>
	struct buffer_data_type{
		using type = Type;
	};

	//TODO: matIxJ

	SSL_GENREATE_BUFFER_DATA_TYPE(glm::mat4, float)
	SSL_GENREATE_BUFFER_DATA_TYPE(glm::mat3, float)
	SSL_GENREATE_BUFFER_DATA_TYPE(glm::mat2, float)

	SSL_GENREATE_BUFFER_DATA_TYPE(glm::vec4, float)
	SSL_GENREATE_BUFFER_DATA_TYPE(glm::vec3, float)
	SSL_GENREATE_BUFFER_DATA_TYPE(glm::vec2, float)
	SSL_GENREATE_BUFFER_DATA_TYPE(float, float)


	SSL_GENREATE_BUFFER_DATA_TYPE(glm::dmat4, double)
	SSL_GENREATE_BUFFER_DATA_TYPE(glm::dmat3, double)
	SSL_GENREATE_BUFFER_DATA_TYPE(glm::dmat2, double)

	SSL_GENREATE_BUFFER_DATA_TYPE(glm::dvec4, double)
	SSL_GENREATE_BUFFER_DATA_TYPE(glm::dvec3, double)
	SSL_GENREATE_BUFFER_DATA_TYPE(glm::dvec2, double)
	SSL_GENREATE_BUFFER_DATA_TYPE(double, double)


	SSL_GENREATE_BUFFER_DATA_TYPE(glm::ivec4, int)
	SSL_GENREATE_BUFFER_DATA_TYPE(glm::ivec3, int)
	SSL_GENREATE_BUFFER_DATA_TYPE(glm::ivec2, int)
	SSL_GENREATE_BUFFER_DATA_TYPE(int, int)


	SSL_GENREATE_BUFFER_DATA_TYPE(glm::uvec4, uint)
	SSL_GENREATE_BUFFER_DATA_TYPE(glm::uvec3, uint)
	SSL_GENREATE_BUFFER_DATA_TYPE(glm::uvec2, uint)
	SSL_GENREATE_BUFFER_DATA_TYPE(uint, uint)


	SSL_GENREATE_BUFFER_DATA_TYPE(glm::bvec4, bool)
	SSL_GENREATE_BUFFER_DATA_TYPE(glm::bvec3, bool)
	SSL_GENREATE_BUFFER_DATA_TYPE(glm::bvec2, bool)
	SSL_GENREATE_BUFFER_DATA_TYPE(bool, bool)
};

#endif //_BUFFER_DATA_TYPE_HPP_
