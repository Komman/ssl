#ifndef _BASIC_UNIFORM_SLOT_HPP_
#define _BASIC_UNIFORM_SLOT_HPP_

#include <GL/glew.h>
#include <string>
#include <vector>

#include "basic_uniform_controller.hpp"
#include "uniform_texture_controller.hpp"
#include "texture.hpp"
#include "shader_pre_builder.hpp"

namespace ssl
{
	class BasicUniformSlot
	{
	public:
		BasicUniformSlot(const std::string& name);
		virtual ~BasicUniformSlot();
		//return the indice of the uniform
		uint add_uniform(GLuint shader_to_link, uint texture_count);

		//	ANY CLASS THAT INHERIT OF THIS ONE MUST CALL change_data() EVERY TIMES
		//	THAT THE DATA RETURNED BY get_data() CHANGES
		void change_data();

		// send uniform data to GPU only if the controller is not up to date
		void send_data_to_shader(uint indice);

		const std::string& get_name() const;

		void print() const;
		void clear();

		template<typename TextureType>
		void change_texture_controller_binding(unsigned int texture_controller, const Texture<TextureType>& new_texture);

	protected:
		virtual BasicUniformController* alloc_instance(GLuint shader_to_link, uint texture_count) =0; 
		virtual void* get_data() const =0;
		virtual uint  get_data_count() const =0;



	private:
		// because of the destructor that calls delete
		BasicUniformSlot(const BasicUniformSlot& slot);
		BasicUniformSlot(BasicUniformSlot&& slot);
		BasicUniformSlot& operator=(const BasicUniformSlot& slot);
		BasicUniformSlot& operator=(BasicUniformSlot&& slot);

		std::string _name;

		std::vector<BasicUniformController*> _controllers;
	};
};

namespace ssl
{
	template<typename TextureType>
	void BasicUniformSlot::change_texture_controller_binding(unsigned int texture_controller, const Texture<TextureType>& new_texture)
	{
		#ifdef SSL_EXPENSIVE_DEBUG
		if(!ShaderPreBuilder::is_uniform_texture(this->get_name()))
		{
			err("BasicUniform::change_texture_slot_binding(): try to change slot of a non-texture uniform:\n" + TERM::NOCOL
				+ "The texture_binding is a reference to the uniform \"" + TERM::BLUE + this->get_name() + TERM::NOCOL
				+ " that has the non-texture type: " + TERM::RED + ShaderPreBuilder::get_uniform_info(this->get_name()).str_type + TERM::NOCOL);
		}
		#endif

		//TODO: security for checking it is the good texture type
		UniformTextureController<TextureType>* controller = (UniformTextureController<TextureType>*)_controllers[texture_controller];
		controller->change_texture(new_texture);
	}

};

#endif //_BASIC_UNIFORM_SLOT_HPP_
