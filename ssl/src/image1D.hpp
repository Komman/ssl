#ifndef _IMAGE1D_HPP_
#define _IMAGE1D_HPP_

#include "texture1D.hpp"

namespace ssl
{
	template<typename Type>
	class Image1D : public Texture1D<Type>
	{
	public:
		Image1D(unsigned int       size,
				void*              data          = NULL,
				GLint              minmag_filter = DEFAULT::MINMAG_FILTER,
				GLint              wrap          = DEFAULT::TEXTURE_WRAP);


		Image1D(const glm::uvec3&  dimension_size,
				void* data,
				GLint minmag_filter,
				GLint wrap);

		void bind() const override;

	protected:

	private:

	};
};


namespace ssl
{
	template<typename Type>
	Image1D<Type>::Image1D(unsigned int       size,
						   void* data,
						   GLint minmag_filter,
						   GLint wrap)
		: Texture1D<Type>(size, data, minmag_filter, wrap)
	{

	}

	template<typename Type>
	Image1D<Type>::Image1D(const glm::uvec3&  dimension_size,
						   void* data,
						   GLint minmag_filter,
						   GLint wrap)
		: Texture1D<Type>(dimension_size, data, minmag_filter, wrap)
	{

	}

	template<typename Type>
	void Image1D<Type>::bind() const
	{
		GL(glBindImageTexture(0, this->get_ID(), 0, GL_FALSE, 0, GL_READ_WRITE, this->get_GL_internal_format()));
	}
};

#endif //_IMAGE1D_HPP_
