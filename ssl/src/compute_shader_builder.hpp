#ifndef _COMPUTE_SHADER_BUILDER_HPP_
#define _COMPUTE_SHADER_BUILDER_HPP_

#include "shader_pre_builder.hpp"


namespace ssl
{
	class ComputeShaderBuilder : public ShaderPreBuilder
	{
	public:
		ComputeShaderBuilder(const BasicShaderParser& parser,
							 const std::string& compute_name, 
							 const glm::uvec3&  local_size);

		ComputeShaderBuilder(const BasicShaderParser& parser,
							 const std::string& compute_name, 
							 const glm::uvec2&  local_size)
			: ComputeShaderBuilder(parser, compute_name, glm::uvec3(local_size.x, local_size.y, 1)) {}

		ComputeShaderBuilder(const BasicShaderParser& parser,
							 const std::string& compute_name, 
							 unsigned int       local_size)
			: ComputeShaderBuilder(parser, compute_name, glm::uvec3(local_size, 1, 1)) {}

	protected:

		std::vector< std::string > _uniforms;

	private:

		void generate_code(std::ofstream& file);
		void write_headers(std::ofstream& file);

		void write_main(        std::ofstream& file, const ShaderParser::functionInfo& start_info);
		void write_dependencies(std::ofstream& file, const ShaderParser::functionInfo& start_info);

	private:

		const std::string _function_name; 
		const glm::uvec3  _local_size;
	};
};

#endif //_COMPUTE_SHADER_BUILDER_HPP_
