#include "basic_uniform_slot.hpp"

#include <iostream>

#include "debug.hpp"

using namespace std;

namespace ssl
{
	BasicUniformSlot::BasicUniformSlot(const std::string& name)
		: _name(name),
		  _controllers(0)
	{
		
	}

	BasicUniformSlot::~BasicUniformSlot()
	{
		this->clear();
	}

	void BasicUniformSlot::clear()
	{
		for(BasicUniformController* controller : _controllers)
		{
			delete controller;
		}
		_controllers.clear();
	}

	uint BasicUniformSlot::add_uniform(GLuint shader_to_link, uint texture_count)
	{
		uint indice = _controllers.size();
		BasicUniformController* instance = this->alloc_instance(shader_to_link, texture_count);
		_controllers.push_back(instance);
		return indice;
	}

	const std::string& BasicUniformSlot::get_name() const
	{
		return _name;
	}

	void BasicUniformSlot::change_data()
	{
		for(BasicUniformController* controller : _controllers)
		{
			controller->up_to_date(false);
		}
	}

	void BasicUniformSlot::send_data_to_shader(uint indice)
	{
		#ifdef SSL_DEBUG
		if(indice >= _controllers.size())
		{
			err("try to send_data_to_shader on a non-existing indice :" + std::to_string(indice)
				+ "\nSize of the slot: " + std::to_string(_controllers.size()));
		}
		#endif
		if(!_controllers[indice]->is_up_to_date())
		{
			#ifdef SSL_PRINTS
			if(ssl::are_prints_enable(SSL_PRINTS_DRAW))
			{
				prints_msg(SSL_PRINTS_DRAW, "Updating uniform: " + TERM::PURPLE + _name);
			}
			#endif

			//cout<<this->get_data_count()<<endl;
			_controllers[indice]->send_data_to_GPU(this->get_data(), this->get_data_count());
		}
		_controllers[indice]->up_to_date(true);
		_controllers[indice]->activate_before_draw();
	}

	 void BasicUniformSlot::print() const
	 {
	 	for(BasicUniformController* controller : _controllers)
	 	{
	 		controller->print();
	 		cout<<" ";
	 	}
	 }
};

