#ifndef _UNIFORM_TEXTURE_2D_HPP_
#define _UNIFORM_TEXTURE_2D_HPP_

#include "uniform_texture.hpp"
#include "texture2D.hpp"

namespace ssl
{
	template<typename Type>
	class UniformTexture2D : public UniformTexture<Type, Texture2D<Type>>
	{
	public:	
		UniformTexture2D(const std::string& name,
						 const glm::uvec2&  size,
						 const std::vector<Type>& data,
						 GLint minmag_filter,
						 GLint wrap          = DEFAULT::TEXTURE_WRAP);

		UniformTexture2D(const std::string& name,
						 const glm::uvec2&  size,
						 GLint minmag_filter,
						 GLint wrap          = DEFAULT::TEXTURE_WRAP)
			: UniformTexture2D<Type>(name, size, std::vector<Type>(0), minmag_filter, wrap) {}

		UniformTexture2D(const std::string& name,
						 const glm::uvec2&  size,
						 const std::vector<Type>& data = std::vector<Type>(0))
			: UniformTexture2D<Type>(name, size, data, DEFAULT::MINMAG_FILTER, DEFAULT::TEXTURE_WRAP) {}

		virtual ~UniformTexture2D() {}

		glm::uvec2 size() const;

	};
};

namespace ssl
{
	template<typename Type>
	UniformTexture2D<Type>::UniformTexture2D(const std::string& name,
						 			   		 const glm::uvec2&  size,
						 			   		 const std::vector<Type>& data,
						 					 GLint minmag_filter,
						 					 GLint wrap)
		
		: UniformTexture<Type, Texture2D<Type>>(name,
												glm::uvec3(size.x, size.y, 1),
												data.size() == 0 ? NULL : (void*) data.data(),
												minmag_filter,
												wrap)
	{
		if(data.size() != 0 && data.size() < size.x*size.y)
		{
			err("try to build a Texture2D of a total size of "+ std::to_string(size.x)+"x"
				+ std::to_string(size.y) + " = "  + std::to_string(size.x*size.y) 
				+ " but the data vector passed has a size of " + std::to_string(data.size()));
		}
	}

	template<typename Type>
	glm::uvec2 UniformTexture2D<Type>::size() const
	{
		return glm::uvec2(this->get_dimension_size().x, this->get_dimension_size().y);
	}
};

#endif //_UNIFORM_TEXTURE_2D_HPP_
