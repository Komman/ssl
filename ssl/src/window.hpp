#ifndef _WINDOW_HPP_
#define _WINDOW_HPP_

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp> 

#include <string>

#define SSL_KEY(K) GLFW_KEY_ ## K

namespace ssl
{
	constexpr int  ANTIALIASING   = 4; 
	constexpr bool PRINT_FPS      = true;

	namespace DEFAULT
	{
		constexpr bool ENABLE_BLEND      = false;
		constexpr bool ENABLE_CULLING    = true;
		constexpr bool ENABLE_DEPTH      = false;
		constexpr bool ENABLE_DEPTH_MASK = true;
	};

	struct glVersion
	{
		GLint major;
		GLint minor;
		GLint glsl;
	};

	namespace window
	{
		void create(uint size_x, uint size_y, const std::string& name);
		void create(const std::string& name);
		void create(const std::string& name, unsigned int window_divisor);
		void clear();
		void clear_color(const glm::vec4& color);
		void flip();
		void close();

		void set_char_callback(void(*f)(GLFWwindow* w, unsigned int codepoint));
		void set_key_callback(void(*f) (GLFWwindow *window, int key, int scancode, int action, int mods));

		glVersion get_gl_version();

		const glm::ivec2& size();
		bool should_close();
		void reset_frame_time();
		float delta_time();
	};

	namespace mouse
	{
		// mouse position relative to the window:
		// mouse::position().x/y € [0.0,1.0] if the mouse is in the window
		glm::dvec2 position();
		void       set(const glm::dvec2& pos);
		// mouse position in pixel
		glm::dvec2 position_pixel();
		void       set_pixel(const glm::dvec2& pos);

		void set_visibility(bool visibility);
		bool in_game();
		void quit_game();
		void enter_game();
	};

	namespace keyboard
	{
		int azery_to_qwerty(int key);// Translate glfw key from qwerty to azerty
		int qwerty_to_azerty(int key);// Translate glfw key from azerty to qwerty
		
		bool key_pressed(int key);;
		void init();
	};
};

#endif //_WINDOW_HPP_
