#include "uniform_controller.hpp"

#include <glm/glm.hpp> 


namespace ssl
{

	//TODO:
	// matIxJ


	// === MATRIX ==
	template<>
	void UniformController<glm::mat4>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniformMatrix4fv(get_ID(), array_count, GL_FALSE, (const GLfloat*)data));
	}

	template<>
	void UniformController<glm::mat3>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniformMatrix3fv(this->get_ID(), array_count, GL_FALSE, (const GLfloat*)data));
	}

	template<>
	void UniformController<glm::mat2>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniformMatrix2fv(this->get_ID(), array_count, GL_FALSE, (const GLfloat*)data));
	}

	// VECTOR

	template<>
	void UniformController<glm::vec4>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniform4fv(this->get_ID(), array_count, (const GLfloat*)data));
	}
	template<>
	void UniformController<glm::vec3>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniform3fv(this->get_ID(), array_count, (const GLfloat*)data));
	}
	template<>
	void UniformController<glm::vec2>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniform2fv(this->get_ID(), array_count, (const GLfloat*)data));
	}

	template<>
	void UniformController<glm::ivec4>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniform4iv(this->get_ID(), array_count, (const GLint*)data));
	}
	template<>
	void UniformController<glm::ivec3>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniform3iv(this->get_ID(), array_count, (const GLint*)data));
	}
	template<>
	void UniformController<glm::ivec2>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniform2iv(this->get_ID(), array_count, (const GLint*)data));
	}

	template<>
	void UniformController<glm::uvec4>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniform4uiv(this->get_ID(), array_count, (const GLuint*)data));
	}
	template<>
	void UniformController<glm::uvec3>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniform3uiv(this->get_ID(), array_count, (const GLuint*)data));
	}
	template<>
	void UniformController<glm::uvec2>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniform2uiv(this->get_ID(), array_count, (const GLuint*)data));
	}

	template<>
	void UniformController<glm::bvec4>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniform4uiv(this->get_ID(), array_count, (const GLuint*)data));
	}
	template<>
	void UniformController<glm::bvec3>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniform3uiv(this->get_ID(), array_count, (const GLuint*)data));
	}
	template<>
	void UniformController<glm::bvec2>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniform2uiv(this->get_ID(), array_count, (const GLuint*)data));
	}


	//SCALAR

	template<>
	void UniformController<float>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniform1fv(this->get_ID(), array_count, (const GLfloat*)data));
	}
	template<>
	void UniformController<int>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniform1iv(this->get_ID(), array_count, (const GLint*)data));
	}
	template<>
	void UniformController<uint>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		GL(glUniform1uiv(this->get_ID(), array_count, (const GLuint*)data));
	}
};

