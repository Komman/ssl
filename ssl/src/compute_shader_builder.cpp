#include "compute_shader_builder.hpp"

#include "shader_preprocessor.hpp"
#include "utils.hpp"

using namespace std;

namespace ssl
{
	ComputeShaderBuilder::ComputeShaderBuilder(const BasicShaderParser& parser,
											   const std::string& compute_name, 
											   const glm::uvec3&  local_size)
		: ShaderPreBuilder(parser),
		  _uniforms(0),
		  _function_name(compute_name),
		  _local_size(local_size)
	{
		string shaders_file_path = utils::extract_directory(_parser->get_file_name()) + "/"; 
		string compute_path = shaders_file_path + compute_name + ".glsl";

		ofstream file(compute_path);

		this->generate_code(file);

		file.close();

		this->load_compute(compute_path);
	}

	void ComputeShaderBuilder::generate_code(std::ofstream& file)
	{
		this->write_headers(file);
		
		ShaderParser::functionInfo start_info = _parser->get_function(_function_name);

		this->write_dependencies(file, start_info);
		this->write_main(file, start_info);
	}


	void ComputeShaderBuilder::write_headers(std::ofstream& file)
	{
		file<<ShaderPreprocessor::glsl_version()<<endl<<endl;
		file<<"layout ("
			<<"  local_size_x = "<<_local_size.x
			<<", local_size_y = "<<_local_size.y
			<<", local_size_z = "<<_local_size.z
			<<") in;"<<endl;
	}

	void ComputeShaderBuilder::write_dependencies(std::ofstream& file, const ShaderParser::functionInfo& start_info)
	{
		vector<string> vars;
		vector<string> funcs;

		this->fill_dependency(start_info, vars, funcs, _uniforms);

		file<<endl;
		this->write_uniforms(file, _uniforms);
		file<<endl;
		this->write_vars(file, vars);
		file<<endl;
		this->write_funcs(file, funcs);
		file<<endl;
	}

	void ComputeShaderBuilder::write_main(std::ofstream& file, const ShaderParser::functionInfo& start_info)
	{
		file<<"void main() // Generated from the compute shader " << _function_name<<endl;
		file << "{" << endl;
		string main_core;
		arrange_code(start_info.function_core, main_core);
		file << main_core << endl;
		file << "}" << endl;
	}

};

