#ifndef _TEXTURE_MUTLISAMPLE_HPP_
#define _TEXTURE_MUTLISAMPLE_HPP_

#include "texture.hpp"
#include "window.hpp"
#include "debug.hpp"

namespace ssl
{
	// TextureMultisample is a 2-dimension texture that allows anti-aliasing

	template<typename Type>
	class TextureMultisample : public Texture<Type>
	{
	public:
		TextureMultisample(const glm::uvec2& size,
				  		   GLint              minmag_filter = DEFAULT::MINMAG_FILTER,
				  		   GLint              wrap          = DEFAULT::TEXTURE_WRAP);

		TextureMultisample(const glm::uvec3&  dimension_size,
						   void* data,
				  		   GLint minmag_filter,
				  		   GLint wrap);

		glm::uvec2 size()            const;
		attachementType get_attachement_type() const override;
		GLenum     get_gl_texture_type()       const override;
	
	protected:
		
		void change_data_if_bound(const glm::uvec3& dimension_size, void* data) override;
		void attach_on_framebuffer_if_bound(uint attachement_index)            const override;
		void tex_image_if_bound(const glm::uvec3& dimension_size, void* data)  const override;
		bool mipmaps_enable()                                                  const override;
		void definitive_resize_GPU(const glm::uvec3& new_dimension_size)                  const override;
		
		bool wrap_enable() const override;
	
	private:

		void copy(const Texture<Type>& tex);
	};
};

namespace ssl
{
	template<typename Type>
	TextureMultisample<Type>::TextureMultisample(const glm::uvec2& size,
												 GLint minmag_filter,
												 GLint wrap)
		: TextureMultisample<Type>(glm::uvec3(size.x, size.y, 1), NULL, minmag_filter, wrap)
	{

	}


	template<typename Type>
	TextureMultisample<Type>::TextureMultisample(const glm::uvec3&  dimension_size,
												 void* data,
												 GLint minmag_filter,
												 GLint wrap)
		: Texture<Type>(dimension_size, minmag_filter, wrap)
	{
		#ifdef SSL_DEBUG
		if(data != NULL)
			err("TextureMultisample should be created with data: " + SSL_FILE_AND_LINE);
		#endif

		this->initialize(NULL);
	}

	template<typename Type>
	attachementType TextureMultisample<Type>::get_attachement_type() const
	{
		return COLOR_ATTACHEMENT;
	}

	template<typename Type>
	glm::uvec2 TextureMultisample<Type>::size() const
	{
		return glm::vec2(this->get_dimension_size().x, this->get_dimension_size().y);		
	}

	template<typename Type>
	void TextureMultisample<Type>::tex_image_if_bound(const glm::uvec3& dimension_size, void* data) const
	{
		#ifdef SSL_DEBUG
		if(data != NULL)
			err("A TextureMultisample should not have loadable data : " + SSL_FILE_AND_LINE);
		#endif

		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY_FLOW))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY_FLOW, "Creating texture2D multisampling data of texture id:" + std::to_string(this->get_ID()) 
				+ ", with data of size: "
				+ std::to_string(dimension_size.x) + "x"
				+ std::to_string(dimension_size.y));
		}
		#endif

		GL(glTexImage2DMultisample(this->get_gl_texture_type(),
						 		   ANTIALIASING,
						 		   Texture<Type>::get_GL_internal_format(),
						 		   dimension_size.x,
						 		   dimension_size.y,
						 		   GL_TRUE));
	}

	template<typename Type>
	void TextureMultisample<Type>::change_data_if_bound(const glm::uvec3& dimension_size, void* data)
	{
		err("TODO: " + SSL_FILE_AND_LINE);
	}

	template<typename Type>
	GLenum TextureMultisample<Type>::get_gl_texture_type() const
	{
		return GL_TEXTURE_2D_MULTISAMPLE;
	}

	template<typename Type>
	void TextureMultisample<Type>::definitive_resize_GPU(const glm::uvec3& new_dimension_size) const
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY_FLOW))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY_FLOW, "Resizing texture2D multisampling id:" + std::to_string(this->get_ID()) 
				+ ", with size: "
				+ std::to_string(new_dimension_size.x) + "x"
				+ std::to_string(new_dimension_size.y));
		}
		#endif

		GL(glTextureStorage2DMultisample(this->get_ID(), ANTIALIASING, Texture<Type>::get_GL_internal_format(), new_dimension_size.x, new_dimension_size.y, GL_TRUE));
	}

	template<typename Type>
	bool TextureMultisample<Type>::mipmaps_enable() const
	{
		return false;
	}

	template<typename Type>
	bool TextureMultisample<Type>::wrap_enable() const
	{
		return false;
	}

	template<typename Type>
	void TextureMultisample<Type>::attach_on_framebuffer_if_bound(uint attachement_index) const
	{
		GL(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + attachement_index, this->get_gl_texture_type(), this->get_ID(), 0));
	}
};

#endif //_TEXTURE_MUTLISAMPLE_HPP_
