#include "gl_buffer.hpp"

#include "debug.hpp"

using namespace glm;
using namespace std;


#define VERTEX_ATTRIB_POINTER_FUNCTION_GL_FLOAT  		  glVertexAttribPointer
#define VERTEX_ATTRIB_POINTER_FUNCTION_GL_DOUBLE 		  glVertexAttribLPointer
#define VERTEX_ATTRIB_POINTER_FUNCTION_GL_INT    		  glVertexAttribIPointer
#define VERTEX_ATTRIB_POINTER_FUNCTION_GL_UNSIGNED_INT    glVertexAttribIPointer


#define VERTEX_ATTRIB_POINTER_1_LOCATION_FLOAT(type, gl_type)\
	template<>\
	uint GLBuffer<type>::vertexAttribPointer(GLuint location, GLint size)\
	{\
		GL(VERTEX_ATTRIB_POINTER_FUNCTION_ ## gl_type(\
		   location,                  		\
		   size,                     		\
		   gl_type,           				\
		   GL_FALSE,           				\
		   0,                  				\
		   (void*)0            				\
		));									\
		return vertex_location_size();							\
	}

#define VERTEX_ATTRIB_POINTER_1_LOCATION_NOFLOAT(type, gl_type)\
	template<>\
	uint GLBuffer<type>::vertexAttribPointer(GLuint location, GLint size)\
	{\
		GL(VERTEX_ATTRIB_POINTER_FUNCTION_ ## gl_type(\
		   location,                  		\
		   size,                     		\
		   gl_type,           				\
		   0,                  				\
		   (void*)0            				\
		));									\
		return vertex_location_size();							\
	}

#define LOCATION_GLSL_SIZE sizeof(float)*4

namespace ssl
{
	//TODO : matrix IxJ

	VERTEX_ATTRIB_POINTER_1_LOCATION_FLOAT(mat2,  GL_FLOAT)

	VERTEX_ATTRIB_POINTER_1_LOCATION_FLOAT(vec4,  GL_FLOAT)
	VERTEX_ATTRIB_POINTER_1_LOCATION_FLOAT(vec3,  GL_FLOAT)
	VERTEX_ATTRIB_POINTER_1_LOCATION_FLOAT(vec2,  GL_FLOAT)
	VERTEX_ATTRIB_POINTER_1_LOCATION_FLOAT(float, GL_FLOAT)

	VERTEX_ATTRIB_POINTER_1_LOCATION_NOFLOAT(uvec4,  GL_UNSIGNED_INT)
	VERTEX_ATTRIB_POINTER_1_LOCATION_NOFLOAT(uvec3,  GL_UNSIGNED_INT)
	VERTEX_ATTRIB_POINTER_1_LOCATION_NOFLOAT(uvec2,  GL_UNSIGNED_INT)
	VERTEX_ATTRIB_POINTER_1_LOCATION_NOFLOAT(uint,   GL_UNSIGNED_INT)

	VERTEX_ATTRIB_POINTER_1_LOCATION_NOFLOAT(ivec4,  GL_INT)
	VERTEX_ATTRIB_POINTER_1_LOCATION_NOFLOAT(ivec3,  GL_INT)
	VERTEX_ATTRIB_POINTER_1_LOCATION_NOFLOAT(ivec2,  GL_INT)
	VERTEX_ATTRIB_POINTER_1_LOCATION_NOFLOAT(int,   GL_INT)

	VERTEX_ATTRIB_POINTER_1_LOCATION_NOFLOAT(bvec4,  GL_INT)
	VERTEX_ATTRIB_POINTER_1_LOCATION_NOFLOAT(bvec3,  GL_INT)
	VERTEX_ATTRIB_POINTER_1_LOCATION_NOFLOAT(bvec2,  GL_INT)
	VERTEX_ATTRIB_POINTER_1_LOCATION_NOFLOAT(bool,   GL_INT)

	VERTEX_ATTRIB_POINTER_1_LOCATION_NOFLOAT(dvec2,  GL_DOUBLE)
	VERTEX_ATTRIB_POINTER_1_LOCATION_NOFLOAT(double, GL_DOUBLE)
	
	template<>
	uint GLBuffer<mat3>::vertex_location_size()
	{
		return 3;							
	}	
	template<>
	uint GLBuffer<mat3>::vertexAttribPointer(GLuint location, GLint size)
	{
		GL(glVertexAttribPointer(location+0, 3, GL_FLOAT, GL_FALSE, sizeof(mat3),( void*)(0*sizeof(vec3))));									
		GL(glVertexAttribPointer(location+1, 3, GL_FLOAT, GL_FALSE, sizeof(mat3),( void*)(1*sizeof(vec3))));									
		GL(glVertexAttribPointer(location+2, 3, GL_FLOAT, GL_FALSE, sizeof(mat3),( void*)(2*sizeof(vec3))));									
		return vertex_location_size();							
	}

	template<>
	uint GLBuffer<mat4>::vertex_location_size()
	{
		return 4;							
	}	
	template<>
	uint GLBuffer<mat4>::vertexAttribPointer(GLuint location, GLint size)
	{
		GL(glVertexAttribPointer(location+0, 4, GL_FLOAT, GL_FALSE, sizeof(mat4),( void*)(0*sizeof(vec4))));									
		GL(glVertexAttribPointer(location+1, 4, GL_FLOAT, GL_FALSE, sizeof(mat4),( void*)(1*sizeof(vec4))));									
		GL(glVertexAttribPointer(location+2, 4, GL_FLOAT, GL_FALSE, sizeof(mat4),( void*)(2*sizeof(vec4))));									
		GL(glVertexAttribPointer(location+3, 4, GL_FLOAT, GL_FALSE, sizeof(mat4),( void*)(3*sizeof(vec4))));									
		return vertex_location_size();							
	}

	template<>
	uint GLBuffer<dmat2>::vertex_location_size()
	{
		return 2;							
	}		
	template<>
	uint GLBuffer<dmat2>::vertexAttribPointer(GLuint location, GLint size)
	{
		GL(glVertexAttribLPointer(location+0, 2, GL_DOUBLE, sizeof(dmat2),(void*)(0*sizeof(dvec2))));									
		GL(glVertexAttribLPointer(location+1, 2, GL_DOUBLE, sizeof(dmat2),(void*)(1*sizeof(dvec2))));									
		return vertex_location_size();							
	}

	template<>
	uint GLBuffer<dmat3>::vertex_location_size()
	{
		return 4;							
	}
	template<>
	uint GLBuffer<dmat3>::vertexAttribPointer(GLuint location, GLint size)
	{
		GL(glVertexAttribLPointer(location+0, 3, GL_DOUBLE, sizeof(dmat3),( void*)(0*sizeof(dvec2))));									
		GL(glVertexAttribLPointer(location+1, 3, GL_DOUBLE, sizeof(dmat3),( void*)(1*sizeof(dvec2))));									
		GL(glVertexAttribLPointer(location+2, 3, GL_DOUBLE, sizeof(dmat3),( void*)(2*sizeof(dvec2))));									
		GL(glVertexAttribLPointer(location+3, 3, GL_DOUBLE, sizeof(dmat3),( void*)(3*sizeof(dvec2))));									
		return vertex_location_size();							
	}		

	template<>
	uint GLBuffer<dmat4>::vertex_location_size()
	{
		return 8;							
	}
	template<>
	uint GLBuffer<dmat4>::vertexAttribPointer(GLuint location, GLint size)
	{
		GL(glVertexAttribLPointer(location+0, 4, GL_DOUBLE, sizeof(dmat3),( void*)(0*sizeof(dvec2))));									
		GL(glVertexAttribLPointer(location+1, 4, GL_DOUBLE, sizeof(dmat3),( void*)(1*sizeof(dvec2))));									
		GL(glVertexAttribLPointer(location+2, 4, GL_DOUBLE, sizeof(dmat3),( void*)(2*sizeof(dvec2))));									
		GL(glVertexAttribLPointer(location+3, 4, GL_DOUBLE, sizeof(dmat3),( void*)(3*sizeof(dvec2))));									
		GL(glVertexAttribLPointer(location+4, 4, GL_DOUBLE, sizeof(dmat3),( void*)(4*sizeof(dvec2))));									
		GL(glVertexAttribLPointer(location+5, 4, GL_DOUBLE, sizeof(dmat3),( void*)(5*sizeof(dvec2))));									
		GL(glVertexAttribLPointer(location+6, 4, GL_DOUBLE, sizeof(dmat3),( void*)(6*sizeof(dvec2))));									
		GL(glVertexAttribLPointer(location+7, 4, GL_DOUBLE, sizeof(dmat3),( void*)(7*sizeof(dvec2))));									
		return vertex_location_size();							
	}		

};

