#ifndef _VERTEX_ARRAY_OBJECT_HPP_
#define _VERTEX_ARRAY_OBJECT_HPP_

#include "gl_buffer.hpp"
#include "element_buffer.hpp"

namespace ssl
{
	class Shader;

	struct bufferDivisor
	{
		const BasicBuffer* buffer;
		// For instances buffers 
		uint divisor; // 0 for normal, and >=1 for instance
	};

	class VertexArrayObject
	{
	public:
		VertexArrayObject(VertexArrayObject&& vao);
		// If there are instance buffers
		VertexArrayObject(const std::vector<bufferDivisor>& buffers);
		VertexArrayObject(const std::vector<bufferDivisor>& buffers, const ElementBuffer* elements);
		VertexArrayObject(const std::vector<const BasicBuffer*>& buffers);
		VertexArrayObject(const std::vector<const BasicBuffer*>& buffers, const ElementBuffer* elements);
		VertexArrayObject(Shader* shader, const std::vector<const BasicBuffer*>& buffers);
		virtual ~VertexArrayObject();

		// NULL if not elements
		const ElementBuffer* get_elements() const;

		bool elements_bound() const;
		bool instanced() const;

		const std::vector<const BasicBuffer*>& get_buffers() const;

		void bind() const;
		void unbind() const;

		// Should be useless
		void update_buffers_GPU() const;

	protected:
		void bind_and_atach_buffers();
		void build();

		static std::vector<bufferDivisor> compute_divisors(Shader* shader, const std::vector<const BasicBuffer*>& buffers);
		static std::vector<bufferDivisor> compute_divisors_no_instances(const std::vector<const BasicBuffer*>& buffers);

	private:
		VertexArrayObject(const VertexArrayObject& vao);

	private:
		GLuint _id;
		std::vector<bufferDivisor> _buffers;
		ElementBuffer const* _elements;
		bool _instanced;

		mutable std::vector<const BasicBuffer*> _buffers_vector_only;
	};
};

#endif //_VERTEX_ARRAY_OBJECT_HPP_
