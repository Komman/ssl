#ifndef _UNIFORM_ARRAY_HPP_
#define _UNIFORM_ARRAY_HPP_

#include "typed_uniform.hpp"
#include "uniform_slot.hpp"
#include "basic_uniform_slot.hpp"
#include "uniform.hpp"

#include <string>
#include <vector>
#include <glm/glm.hpp> 

namespace ssl
{
	/*
		ONLY SUPORT GLSL TYPES
		DO NOT SUPPOR DOUBLE PRECISION (double, dvec2/3/4, dmat4, dmat3 and dmat2)
	*/
	
	template<typename Type>
	class UniformArray : public TypedUniform<Type>
	{
	public:
		static inline std::string SIZE_ADD_NAME = "_size";

		//class
		UniformArray(const std::string& name,
					 unsigned int max_size,
					 const std::vector<Type>& value);
		

		void change_value(const std::vector<Type>& new_data);
		void change_subvalue(uint indice, const Type& value);
		void add_value(const Type& value);
		
		const Type& get_subvalue(uint indice) const;
		const std::vector<Type>& get_value()  const;
		unsigned int max_size() const;
		
		UniformArray<Type>& operator=(const std::vector<Type>& value);
		UniformArray<Type>& operator=(const UniformArray<Type>& u);

		const Type& operator[](uint indice) const;

	protected:
		UniformArray(){}
		UniformArray(const UniformArray<Type>& u){}
		UniformArray(UniformArray<Type>&& u){}
		UniformArray<Type>& operator=(UniformArray<Type>&& u){}

		BasicUniformSlot* get_instance();

	private:
		void update_size();

		unsigned int _max_size;

		UniformSlot<Type> _slot;
		Uniform<uint>     _array_size;
	};

};


// === TEMPLATIZED IMPLEMENTATION === 

namespace ssl
{	
	template<typename Type>
	UniformArray<Type>::UniformArray(const std::string& name,
									 unsigned int max_size,
						   	    	 const std::vector<Type>& value)
		: 	TypedUniform<Type>(name, max_size),
			_max_size(max_size),
			_slot(name, value),
			_array_size(name + SIZE_ADD_NAME, value.size())
	{
		this->set_slot();
	}

	template<typename Type>
	BasicUniformSlot* UniformArray<Type>::get_instance()
	{
		return &_slot;
	}

	template<typename Type>
	void UniformArray<Type>::change_value(const std::vector<Type>& value)
	{
		#ifdef SSL_DEBUG
		if(value.size() > _max_size)
		{
			ssl::err("UniformArray<Type>::change_value(const vector<Type>& value): vector sized more than the UniformArray size");
		}
		#endif
		_slot.change_value(value);
		this->update_size();
	}

	template<typename Type>
	void UniformArray<Type>::change_subvalue(uint indice, const Type& value)
	{
		#ifdef SSL_DEBUG
		if(indice >= _slot.get_value().size())
		{
			err("UniformArray<Type>::change_subvalue: indice out of range :" + std::to_string(indice) + "/" + std::to_string(_slot.get_value().size()));
		}
		#endif
		_slot.change_subvalue(indice, value);
	}

	template<typename Type>
	const Type& UniformArray<Type>::get_subvalue(uint indice) const
	{
		#ifdef SSL_DEBUG
		if(indice >= _slot.get_value().size())
		{
			err("UniformArray<Type>::get_subvalue: indice out of range :" + std::to_string(indice) + "/" + std::to_string(_slot.get_value().size()));
		}
		#endif

		return _slot.get_value()[indice];
	}

	template<typename Type>
	const std::vector<Type>& UniformArray<Type>::get_value() const
	{
		return _slot.get_value();
	}

	template<typename Type>
	unsigned int UniformArray<Type>::max_size() const
	{
		return _max_size;
	}


	template<typename Type>
	void UniformArray<Type>::update_size()
	{
		_array_size.change_value(_slot.get_value().size());
	}


	template<typename Type>
	void UniformArray<Type>::add_value(const Type& value)
	{
		#ifdef SSL_DEBUG
		if(_slot.get_value().size() > _max_size)
		{
			err("void UniformArray<Type>::add_value(const Type& value): can't add a value because the array has already its maximum size : " + std::to_string(_max_size));
		}
		#endif
		_slot.add_value(value);
		this->update_size();
	}

	template<typename Type>
	UniformArray<Type>& UniformArray<Type>::operator=(const std::vector<Type>& value)
	{
		this->change_value(value);
		return *this;
	}

	template<typename Type>
	UniformArray<Type>& UniformArray<Type>::operator=(const UniformArray<Type>& u)
	{
		this->change_value(u.get_value());
		return *this;
	}

	template<typename Type>
	const Type& UniformArray<Type>::operator[](uint indice) const
	{
		#ifdef SSL_DEBUG
		if(indice >= this->get_value().size())
		{
			err("const Type& UniformArray<Type>::operator[](uint indice): indice out of range");
		}
		#endif
		return this->get_subvalue(indice);
	}

};

#endif //_UNIFORM_ARRAY_HPP_
