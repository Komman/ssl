#ifndef _ELEMENT_BUFFER_HPP_
#define _ELEMENT_BUFFER_HPP_

#include <vector>

#include "gl_buffer.hpp"

namespace ssl
{
	class ElementBuffer : public GLBuffer<unsigned int>
	{
	public:
		ElementBuffer(const std::vector<unsigned int>& initial_data,
					  draw_type type);
		ElementBuffer(draw_type type) : ElementBuffer({}, type) {}

		// ElementBuffer() : ElementBuffer({}, DEFAULT::MEMORY_DRAW_TYPE) {}
		virtual ~ElementBuffer() {}

		unsigned int indices_count() const;

		void add_triangle(const std::vector<unsigned int>& vertexs_indices, bool flip_face = false);
		void add_triangle(unsigned int vertex_index1,
						  unsigned int vertex_index2,
						  unsigned int vertex_index3,
						  bool flip_face = false);
		void pop_triangle();
		
		void add_rectangle(const std::vector<unsigned int>& vertexs_indices, bool flip_face = false);
		void add_rectangle(unsigned int vertex_index1,
						   unsigned int vertex_index2,
						   unsigned int vertex_index3,
						   unsigned int vertex_index4,
						   bool flip_face = false);
		void pop_rectangle();

		/* vertexs_indices is a vector of the 8 indices of the hexahedron's points
		  Faces will be build between indices:
			
			0 1 2 3
			7 6 5 4
			0 3 7 4
			1 5 6 2
			0 4 5 1
			2 6 7 3

			as this 3D drawing shows:

			3----------2
			|\         |\
			| 0----------1
			| |        | |
			| |        | |
			7-|------- 6 |
			 \|         \|
			  4----------5

		*/
		void add_hexahedron(const std::vector<unsigned int>& vertexs_indices);
		void pop_hexahedron();

	protected:

	private:
	};
};



#endif //_ELEMENT_BUFFER_HPP_
