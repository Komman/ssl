#include "uniform_view.hpp"
#include "debug.hpp"

namespace ssl
{
	UniformView::UniformView(const glm::vec3& initial_position,
					float min_view_distance,
					float max_view_distance,
					float view_field,
					const std::string& MVP_uniform_name,
				    const std::string& position_uniform_name,
				    const std::string& direction_uniform_name,
				    const std::string& right_vector_uniform_name,
				    const std::string& up_head_uniform_name)
		: View(initial_position, min_view_distance, max_view_distance, view_field),
		  _MVP(std::make_unique<Uniform<glm::mat4>>(MVP_uniform_name, this->get_MVP())),
		  _position(std::make_unique<Uniform<glm::vec3>>(position_uniform_name, this->get_position())),
		  _direction(std::make_unique<Uniform<glm::vec3>>(direction_uniform_name, this->get_direction())),
		  _right_vector(std::make_unique<Uniform<glm::vec3>>(right_vector_uniform_name, this->get_right_vector())),
		  _up_head(std::make_unique<Uniform<glm::vec3>>(up_head_uniform_name, this->get_up_head()))
	{

	}	

	void UniformView::update_uniforms()
	{
		#ifdef SSL_DEBUG
		if(_MVP.get() == NULL
		|| _position.get() == NULL
		|| _direction.get() == NULL
		|| _right_vector.get() == NULL
		|| _up_head.get() == NULL)
		{
			err("UniformView::update_uniforms(): uniform not initialized");
		}
		#endif

		if(_MVP->get_value() != this->get_MVP())
		{
			_MVP->change_value(this->get_MVP());
		}
		if(_position->get_value() != this->get_position())
		{
			_position->change_value(this->get_position());
		}
		if(_direction->get_value() != this->get_direction())
		{
			_direction->change_value(this->get_direction());
		}
		if(_right_vector->get_value() != this->get_right_vector())
		{
			_right_vector->change_value(this->get_right_vector());
		}
		if(_up_head->get_value() != this->get_up_head())
		{
			_up_head->change_value(this->get_up_head());
		}
	}
};
