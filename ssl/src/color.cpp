#include "color.hpp"

#include <stdlib.h>

namespace ssl
{
	glm::vec3 dark(const glm::vec3& color, float dark_coef)
	{
		return color*dark_coef;
	}

	glm::vec3 random_color()
	{
		return glm::vec3(
			float(rand() % 256 ) / float(256),
			float(rand() % 256 ) / float(256),
			float(rand() % 256 ) / float(256)
		);
	}

	std::string TERM::colored(const std::string& str, const std::string& color)
	{
		return color + str + TERM::NOCOL;
	}

};