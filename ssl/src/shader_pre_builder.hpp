#ifndef _SHADER_PRE_BUILDER_HPP_
#define _SHADER_PRE_BUILDER_HPP_

#include "shader_parser.hpp"
#include "basic_shader.hpp"
#include "debug.hpp"

#include <vector>
#include <string>
#include <memory>

namespace ssl
{

	struct uniformInfo
	{
		std::string str_type;
		std::string name;
		std::string layout;
		
		int array_count;
	};

	class ShaderPreBuilder : public BasicShader
	{
	public:
		static void declare_uniform(const uniformInfo& infos);
		static bool is_uniform_texture(const std::string& uniform_name);	

		static uniformInfo get_uniform_info(const std::string& uniform_name);
		static bool is_uniform_declared(const std::string& uniform_name);

	protected:
		ShaderPreBuilder(const BasicShaderParser& parser);
		virtual ~ShaderPreBuilder() {}

		void fill_dependency(const ShaderParser::functionInfo& start_info,
						     std::vector<std::string>& vars,
						     std::vector<std::string>& funcs,
						     std::vector<std::string>& uniforms);
		

		void write_uniforms(std::ofstream& file, const std::vector<std::string>& uniforms);
		void write_vars(    std::ofstream& file, const std::vector<std::string>& vars);
		void write_funcs(   std::ofstream& file, const std::vector<std::string>& funcs);

		static void arrange_code(const std::string& in, std::string& out);
		static void write_local_function_head(std::ofstream& file, const ShaderParser::functionInfo& infos);
		static void write_local_function(std::ofstream& file, const ShaderParser::functionInfo& infos);
		static void write_local_function_declaration(std::ofstream& file, const ShaderParser::functionInfo& infos);
		

	protected:

		const BasicShaderParser* _parser;

		static std::map<std::string, uniformInfo> uniform_infos;

	private:
	};
};

#endif //_SHADER_PRE_BUILDER_HPP_
