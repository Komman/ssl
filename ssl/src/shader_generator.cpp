#include "shader_generator.hpp"
#include "debug.hpp"

#include <sys/stat.h>
#include <filesystem>

using namespace std;


namespace ssl
{
	uint ShaderGenerator::generated_id = 0;

	static void create_shaders_file(const std::string& theorical_path)
	{
		struct stat info;
		if(stat(theorical_path.c_str(), &info) != 0)
		{
		    if(!std::filesystem::create_directory(theorical_path))
		    {
		    	err("Failed to create directory : " + theorical_path);
		    }	
		}
		else
		{
			std::error_code errorCode;
			if(!std::filesystem::remove_all(theorical_path, errorCode))
			{
				war("Failed to clean generated shader folder : " + theorical_path);
				std::cout << errorCode.message() << std::endl;
			}

			if(!std::filesystem::create_directory(theorical_path))
		    {
		    	err("Failed to create directory : " + theorical_path);
		    }

			if(!(info.st_mode & S_IFDIR))
			{
		    	err("A file named " + theorical_path + "exists (while trying to create a directory with this name)");
			} 
		}
	}

	static string last_folder_opened = "";

	std::string ShaderGenerator::generate_global_shader(const std::string& shader_file_path,
														uint generated_id,
														const std::vector<uniformRenaming>& uniform_renaming)
	{
		string dir        = utils::extract_directory(shader_file_path) + "/";
		last_folder_opened = dir + DEFAULT::GENERATED_SHADERS_DIRECTORY_NAME + to_string(generated_id) + "/";

		create_shaders_file(last_folder_opened);

		ShaderPreprocessor preprocessor(shader_file_path, uniform_renaming);
		preprocessor.compile(last_folder_opened + DEFAULT::PREPROCESSOR_SHADER_NAME );

		return last_folder_opened + DEFAULT::PREPROCESSOR_SHADER_NAME;
	}

	ShaderGenerator::ShaderGenerator(const std::string& global_shader_file_path)
		: ShaderGenerator(global_shader_file_path, std::vector<uniformRenaming>(0))
	{

	}

	ShaderGenerator::ShaderGenerator(const std::string& global_shader_file_path,
									 const std::vector<uniformRenaming>& uniform_renaming)

		: ShaderParser(generate_global_shader(global_shader_file_path, ShaderGenerator::generated_id, uniform_renaming))
	{
		_folder_path = last_folder_opened;
		ShaderGenerator::generated_id++;
	}

	ShaderGenerator::~ShaderGenerator()
	{
		if(DEFAULT::REMOVE_GENERATED_SHADERS)
		{
			std::error_code errorCode;
			if(!std::filesystem::remove_all(_folder_path, errorCode))
			{
				war("Failed to remove generated shader folder : " + _folder_path);
				std::cout << errorCode.message() << std::endl;
			}
		}
	}

};

