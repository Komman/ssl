#include "debug.hpp"
#include "color.hpp"

#include <iostream>
#include <GL/glew.h>

using namespace ssl;
using namespace std;

static std::string ssl_error_string = "SSL GENERAL ERROR";

#define RETURN_OPENGL_ERROR_STR_CODE(ERROR_GL_CODE) if(error_code == ERROR_GL_CODE) {return #ERROR_GL_CODE;}


namespace ssl
{
	const char* ssl_error::what() const noexcept
	{
	    return ssl_error_string.c_str();
	}

	void err(const string& msg)
	{
		cout<< endl<< TERM::RED<< "SSL ERROR: " + ERR_COLOR + msg + TERM::NOCOL<< endl<< endl;
		throw ssl_error();
	}
	void war(const string& msg)
	{
		cout<<TERM::PURPLE<< "SSL WARNING: " + WAR_COLOR + msg + TERM::NOCOL<<endl;
	}
	void msg(const std::string& msg)
	{
		cout<< msg<< endl;
	}

	void bouh()
	{
		cout<< TERM::CYAN<< "BOUH"<< TERM::NOCOL<< endl;
	}


	std::string opengl_error_code(uint error_code)
	{
		
		RETURN_OPENGL_ERROR_STR_CODE(GL_INVALID_ENUM)
		RETURN_OPENGL_ERROR_STR_CODE(GL_INVALID_VALUE)
		RETURN_OPENGL_ERROR_STR_CODE(GL_INVALID_OPERATION)
		RETURN_OPENGL_ERROR_STR_CODE(GL_INVALID_FRAMEBUFFER_OPERATION)
		RETURN_OPENGL_ERROR_STR_CODE(GL_OUT_OF_MEMORY)

		return "NO_ERROR";
	}

	static bool _prints_enable[SSL_PRINTS_TYPES_AMOUNT] = {false, false, false};

	void prints_msg(SSLPrintTypes print_type, const std::string& msg)
	{
		if(_prints_enable[print_type])
		{
			cout << TERM::ORANGE << "SSL PRINTS: " << PRINTS_COLOR << msg << TERM::NOCOL <<endl;
		}
	}

	void enable_prints(SSLPrintTypes print_type)
	{
		_prints_enable[print_type] = true;
	}

	void disable_prints(SSLPrintTypes print_type)
	{
		_prints_enable[print_type] = false;
	}

	bool are_prints_enable(SSLPrintTypes print_type)
	{
		return _prints_enable[print_type];
	}
};