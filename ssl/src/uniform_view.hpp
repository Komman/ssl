#ifndef _UNIFORM_VIEW_HPP_
#define _UNIFORM_VIEW_HPP_

#include <memory>
#include "view.hpp"
#include "uniform.hpp"

namespace ssl
{
	class UniformView : public View
	{
	public:
		UniformView(const glm::vec3& initial_position,
					float min_view_distance,
					float max_view_distance,
					float view_field,
					const std::string& MVP_uniform_name,
				    const std::string& position_uniform_name,
				    const std::string& direction_uniform_name,
				    const std::string& right_vector_uniform_name,
				    const std::string& up_head_uniform_name);

		void update_uniforms();

	private:
		std::unique_ptr<Uniform<glm::mat4>> _MVP;
		std::unique_ptr<Uniform<glm::vec3>> _position;
		std::unique_ptr<Uniform<glm::vec3>> _direction;
		std::unique_ptr<Uniform<glm::vec3>> _right_vector;
		std::unique_ptr<Uniform<glm::vec3>> _up_head;
	};

};

#endif //_UNIFORM_VIEW_HPP_
