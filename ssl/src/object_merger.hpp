#ifndef _OBJECT_MERGER_HPP_
#define _OBJECT_MERGER_HPP_

#include <tuple>

#include "drawable_object.hpp"

namespace ssl
{
	/*
		BASIC OBJECT MERGER
	*/

	template<typename T>
	struct mergedBufferDrawType
	{
		using type = draw_type;
	};

	template<typename DrawableObjectType, typename... DrawTypes>
	class BasicObjectMerger : public DrawableObjectType
	{
	public:

		using bufferTypes    = std::tuple<DrawTypes...>;

	public:
		BasicObjectMerger(const std::vector<DrawableObjectType*> objects, typename mergedBufferDrawType<DrawTypes>::type... buffer_draw_types);

		const std::vector<const BasicBuffer*>&  get_buffers() const override;
		const std::vector<DrawableObjectType*>& get_objects() const;

		// Returns the indice of the object
		unsigned int add_object(DrawableObjectType* object);
		void merge_objects();
		// void remove_object(unsigned int index);

	
	protected:

		void check_all_object_buffer_size() const;
		void check_object(DrawableObjectType* object) const;

		virtual void merge_additionnal_operations() {}

	private:

		void check_object_buffers_size(DrawableObjectType* object) const;
		void check_object_size(DrawableObjectType* object) const;
		void check_object_buffers_type(DrawableObjectType* object) const;
		template<std::size_t... Is>
    	void check_object_all_buffers(DrawableObjectType* object, std::index_sequence<Is...>) const;
    	template<typename BufferType, std::size_t buffer_index>
    	void check_object_array_buffer(DrawableObjectType* object) const;
		
		template<std::size_t... Is>
    	void add_object_all_buffers(DrawableObjectType* object, std::index_sequence<Is...>);
    	template<typename BufferType, std::size_t buffer_index>
    	void add_object_array_buffer(DrawableObjectType* object);
		template<std::size_t... Is>
    	void init_all_array_buffers(std::index_sequence<Is...>);
    	template<std::size_t buffer_index>
    	void init_array_buffer();

	private:
		std::vector<DrawableObjectType*>      _objects;
		std::tuple<ArrayBuffer<DrawTypes>...> _array_buffers;
		std::vector<const BasicBuffer*>       _buffers;
	};





	/*
		OBJECT MERGER
    */

	template<typename... DrawTypes>
	class ObjectMerger : public BasicObjectMerger<DrawableObject, DrawTypes...>
	{
	public:
		ObjectMerger(const std::vector<DrawableObject*> objects, typename mergedBufferDrawType<DrawTypes>::type... buffer_draw_types);

	private:

	};



	/*
		ELEMENT OBJECT MERGER
    */

	template<typename... DrawTypes>
	class ElementObjectMerger : public BasicObjectMerger<ElementDrawableObject, DrawTypes...>
	{
	public:
		ElementObjectMerger(const std::vector<ElementDrawableObject*> objects,
							ssl::draw_type elements_buffer_draw_type,
							typename mergedBufferDrawType<DrawTypes>::type... buffer_draw_types);

		const ElementBuffer& get_elements() const override;
		
	protected:

		void merge_additionnal_operations();

	private:

		ElementBuffer _elements;
		
	};
};


#include "object_merger_impl.hpp"

#endif //_OBJECT_MERGER_HPP_
