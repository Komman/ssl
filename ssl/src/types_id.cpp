#include "types_id.hpp"

#include <glm/glm.hpp>
#include <map>

#include "debug.hpp"

using namespace glm;

#define APPLY_MACRO_FOR_ALL_TYPES(MACRO)\
	MACRO(vec4)\
	MACRO(vec3)\
	MACRO(vec2)\
	MACRO(float)\
	MACRO(mat4)\
	MACRO(mat3)\
	MACRO(mat2)\
\
	MACRO(dvec4)\
	MACRO(dvec3)\
	MACRO(dvec2)\
	MACRO(double)\
	MACRO(dmat4)\
	MACRO(dmat3)\
	MACRO(dmat2)\
\
	MACRO(ivec4)\
	MACRO(ivec3)\
	MACRO(ivec2)\
	MACRO(int)\
\
	MACRO(uvec4)\
	MACRO(uvec3)\
	MACRO(uvec2)\
	MACRO(uint)\
\
	MACRO(bvec4)\
	MACRO(bvec3)\
	MACRO(bvec2)\
	MACRO(bool)


#define DECLARE_GET_TYPE_ID(type)\
	template<>\
	types_id get_type_id<type>()\
	{\
		return SSL_ ## type;\
	}

#define PUT_STRTYPE_IN_MAP(type) {#type, SSL_ ## type},
#define PUT_STRTYPE_IN_STR_VECTOR(type) #type,

namespace ssl
{
	static std::map<std::string, types_id> strtypes_to_typeid = {
		APPLY_MACRO_FOR_ALL_TYPES(PUT_STRTYPE_IN_MAP)
		{"no_type", SSL_NO_TYPE}
	};
	static std::string strtypes[SSL_TYPES_ID_COUNT] = {
		"no_type",
		APPLY_MACRO_FOR_ALL_TYPES(PUT_STRTYPE_IN_STR_VECTOR)
		
	};

	APPLY_MACRO_FOR_ALL_TYPES(DECLARE_GET_TYPE_ID)

	types_id get_type_id(const std::string& str_type)
	{
		const auto& it = strtypes_to_typeid.find(str_type);
		#ifdef SSL_DEBUG
		if(it == strtypes_to_typeid.end())
		{
			return SSL_NO_TYPE;
		}
		#endif

		return it->second;
	}

	std::string get_str_type_id(types_id type)
	{
		#ifdef SSL_DEBUG
		if((int)type >= SSL_TYPES_ID_COUNT || (int)type < 0)
		{
			err("Wrong type asked in function get_str_type_id(types_id): " + SSL_FILE_AND_LINE);
		}
		#endif

		return strtypes[type];
	}
};

