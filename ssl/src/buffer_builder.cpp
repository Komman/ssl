#include "buffer_builder.hpp"

#include "vvv.hpp"
#include "debug.hpp"

using namespace std;
using namespace glm;

namespace ssl
{
	// ALL PTRS MUST BE INITIALIZED WITH NULL PTR
	ArrayBuffer<vec2>* _rectangle       = NULL;
	ArrayBuffer<vec2>* _filled_screen   = NULL;
	ArrayBuffer<vec3>* _filled_screen3D = NULL;

	namespace buffers
	{
		template<typename T>
		static const T& check_and_return(T* buffer_ptr)
		{
			#ifdef SSL_DEBUG
			if(buffer_ptr == NULL)
			{
				err("buffer_builder not initialized");
			}
			#endif

			return *buffer_ptr;
		}

		const ArrayBuffer<vec2>& rectangle()
		{
			return check_and_return(_rectangle);
		}
		const ArrayBuffer<vec2>& screenfill()
		{
			return check_and_return(_filled_screen);
		}
		const ArrayBuffer<vec3>& screenfill3D()
		{
			return check_and_return(_filled_screen3D);
		}
	};




	namespace buffer_builder
	{
		std::vector<glm::vec3> hexahedron_points(const glm::vec3& origin,
												 const glm::vec3& direction1,
												 const glm::vec3& direction2,
												 const glm::vec3& direction3)
		{
			std::vector<glm::vec3> ret;
			vec3 extremity = origin + direction1 + direction2 + direction3;

			ret.push_back(origin);
			ret.push_back(origin + direction1);
			ret.push_back(extremity - direction3);
			ret.push_back(origin + direction2);
			
			ret.push_back(origin + direction3);
			ret.push_back(extremity - direction2);
			ret.push_back(extremity);
			ret.push_back(extremity - direction1);

			return ret;
		}

		std::vector<glm::vec3> aligned_square_points(const glm::vec3& center, float size)
		{
			vec3 origin = center - vec3(size, size, size)/2.0f;
			return hexahedron_points(origin,
									 vvv::x()*size,
									 vvv::y()*size,
									 vvv::z()*size);
		}


		template<typename T>
		static void quickfree(T* ptr)
		{
			if(ptr != NULL)
			{
				delete ptr;
			}
		}
		static void quickfree_all_buffers()
		{
			quickfree(_rectangle);
			quickfree(_filled_screen);
			quickfree(_filled_screen3D);
		}


		void init()
		{
			quickfree_all_buffers();

			_rectangle       = new ArrayBuffer<vec2>({vec2(0.0,0.0), vec2(1.0,0.0), vec2(1.0,1.0),
													  vec2(0.0,0.0), vec2(1.0,1.0), vec2(0.0,1.0)}, STATIC_DRAW);
			_filled_screen   = new ArrayBuffer<vec2>({vec2(-1.0,-1.0), vec2(3.0,-1.0), vec2(-1.0,3.0)}, STATIC_DRAW);
			_filled_screen3D = new ArrayBuffer<vec3>({vec3(-1.0, -1.0, 0.0), vec3(3.0, -1.0, 0.0), vec3(-1.0, 3.0, 0.0)}, STATIC_DRAW);
		}		

		void free()
		{
			static bool is_free = false;
			if(!is_free)
			{
				quickfree_all_buffers();
			}
			is_free = true;
		}
	};
};

