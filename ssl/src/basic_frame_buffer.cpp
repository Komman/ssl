#include "basic_frame_buffer.hpp"

#include "global_drawing.hpp"
#include "debug.hpp"
#include "autoprint_recorder.hpp"

#include <string>

namespace ssl
{
	BasicFrameBuffer::BasicFrameBuffer(const glm::vec4& clear_color)
		: _clear_color(clear_color)
	{

	}

	void BasicFrameBuffer::set_clear_color(const glm::vec4& color)
	{
		_clear_color = color;
	}

	static std::string bitfield_mask_string(GLbitfield mask)
	{	
		static constexpr GLbitfield MASK_BITS = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT;
		std::string ret = "";

		// std::bitset<sizeof(GLbitfield)*8> x(mask);
		// std::cout<<x<<std::endl;
		// x =  GL_COLOR_BUFFER_BIT;
		// std::cout<<x<<std::endl;
		// x =  GL_DEPTH_BUFFER_BIT;
		// std::cout<<x<<std::endl;
		// x =  GL_STENCIL_BUFFER_BIT;
		// std::cout<<x<<std::endl;

		if(((mask & MASK_BITS) & GL_COLOR_BUFFER_BIT  )  != 0)
		{
			if(ret.size() > 0) {ret += " & ";}
			ret+="GL_COLOR_BUFFER_BIT";
		}
		if(((mask & MASK_BITS) & GL_DEPTH_BUFFER_BIT   ) != 0)
		{
			if(ret.size() > 0) {ret += " & ";}
			ret+="GL_DEPTH_BUFFER_BIT";
		}
		if(((mask & MASK_BITS) & GL_STENCIL_BUFFER_BIT)  != 0)
		{
			if(ret.size() > 0) {ret += " & ";}
			ret+="GL_STENCIL_BUFFER_BIT";
		}

		return ret;
	}

	void BasicFrameBuffer::clear(GLbitfield mask)
	{
		this->bind();

		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_DRAW))
		{
			ssl::prints_msg(SSL_PRINTS_DRAW, "Clearing (" + TERM::WHITE + bitfield_mask_string(mask) +
							PRINTS_COLOR + ") of Framebuffer N:" 
							+ TERM::ORANGE + std::to_string(this->get_ID()) + PRINTS_COLOR
							+ " (size: " + TERM::CYAN
							+ std::to_string(this->frame_size().x) + std::string("x") + std::to_string(this->frame_size().y) 
							+ PRINTS_COLOR + ")");
		}
		#endif
		
		const glm::vec4& current = global_drawing::get_clear_color();

		global_drawing::clear_color(_clear_color);
		global_drawing::clear(mask);
		global_drawing::clear_color(current);

		this->unbind();
	}

	const glm::vec4& BasicFrameBuffer::get_clear_color()
	{
		return _clear_color;
	}

};

