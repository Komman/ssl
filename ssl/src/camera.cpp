#include "camera.hpp"

#include <iostream>

#include "window.hpp"
#include "vvv.hpp"
#include "constants.hpp"

using namespace ssl;
using namespace std;

namespace ssl
{
	Camera::Camera(const glm::vec3& initial_position,
				   float min_view_distance,
				   float max_view_distance,
				   float speed,
				   float sensibility,
				   const std::vector<int>& control_keys,
				   float view_field)
		
		: ControlCamera(initial_position, min_view_distance, max_view_distance, control_keys, view_field),
		  _speed(speed),
		  _sensibility(sensibility),
		  _speed_vector(glm::vec3(0.0f))
	{
		mouse::set_visibility(false);
	}

	void Camera::set_sensibility(float sensibility)
	{
		_sensibility = sensibility;
	}

	void Camera::set_speed(float speed)
	{
		_speed = speed;
	}

	const glm::vec3& Camera::get_speed_vector()
	{
		return _speed_vector;
	}


	void Camera::keys_reaction(const std::vector<unsigned int>& pressed_keys_indices)
	{
		_speed_vector = glm::vec3(0.0, 0.0, 0.0);

		//TOOPT (0) (with function pointer)
		for(unsigned int k : pressed_keys_indices)
		{
			if(k == CAMERA_CONTROL_RIGHT)
			{
				_speed_vector+= this->get_right_vector();
			}
			if(k == CAMERA_CONTROL_LEFT)
			{
				_speed_vector-= this->get_right_vector();
			}
			if(k == CAMERA_CONTROL_UP)
			{
				_speed_vector+= this->get_up_head();
			}
			if(k == CAMERA_CONTROL_DOWN)
			{
				_speed_vector-= this->get_up_head();
			}
			if(k == CAMERA_CONTROL_FRONT)
			{
				_speed_vector+= glm::cross(this->get_up_head(), this->get_right_vector());
			}
			if(k == CAMERA_CONTROL_BACK)
			{
				_speed_vector-= glm::cross(this->get_up_head(), this->get_right_vector());
			}
		}

		if(glm::length(_speed_vector) > ssl::EPSILON)
		{
			_speed_vector = glm::normalize(_speed_vector)*this->_speed;
			this->tp(this->get_position() + _speed_vector*ssl::window::delta_time());
		}
	}

	void Camera::mouse_reaction(const glm::vec2& mouse_moving_pixel)
	{
		this->rotate(-mouse_moving_pixel*_sensibility);
	}
};
