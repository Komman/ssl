#include "shader_preprocessor.hpp"

#include "debug.hpp"
#include "color.hpp"
#include "utils.hpp"
#include "window.hpp"
#include "basic_shader_parser.hpp"
#include "basic_shader.hpp"

using namespace std;

namespace ssl
{
	static string ERR_QUOTE_ARG = "#SSL_ERROR_QUOTE_ARG#";

	static inline string get_quote_arg(const string& word)
	{
		if(word.size()<2 || word[0]!='\"' || word[word.size()-1]!='\"')
		{
			return ERR_QUOTE_ARG;
		}
		return word.substr(1, word.size()-2);
	} 

	static inline string get_arg(const string& word)
	{
		unsigned int begin=0;
		unsigned int end  =0;
		for(begin=0; begin<word.size(); begin++)
		{
			if(!utils::is_in_string(word[begin], ShaderPreprocessor::separators))
				break;
		}
		for(end=word.size()-1; end>0; end--)
		{
			if(!utils::is_in_string(word[end], ShaderPreprocessor::separators))
				break;
		}
		return word.substr(begin, end-begin+1);
	}

	std::string ShaderPreprocessor::glsl_version()
	{
		return "#version " + std::to_string(window::get_gl_version().glsl) + " core";
	}


	ShaderPreprocessor::ShaderPreprocessor(const std::string& main_file_path)
		: ShaderPreprocessor(main_file_path, std::vector<uniformRenaming>(0))
	{	

	}

	ShaderPreprocessor::ShaderPreprocessor(const std::string& main_file_path, const std::vector<uniformRenaming>& uniform_renaming)
		: 
			_is_commented_line(false),
			_uniform_renaming(uniform_renaming)
	{	
		if constexpr (PRINT_SHADERS_LOAD)
		{
			cout<<TERM::PURPLE<<"Preprocessing             : "<<TERM::NOCOL<<main_file_path<<"..."<<endl;
		}

		unsigned int end_directory;
		for(end_directory = main_file_path.size()-1; end_directory>0; end_directory--)
		{
			if(main_file_path[end_directory] == '/')
				break;
		}

		_in_file_directory = main_file_path.substr(0, end_directory+1);
		_in_file_name      = main_file_path.substr(end_directory+1);
	}


	void ShaderPreprocessor::compile(const std::string& out_file_path)
	{
		ofstream out_file(out_file_path);
		ifstream in_file(_in_file_directory + _in_file_name);
		if(!out_file.is_open())
		{
			err("Failed to write in " + out_file_path);
		}
		if(!in_file.is_open())
		{
			err("Failed to read in "  + _in_file_directory + _in_file_name);
		}

		_already_included_files.clear();
		this->global_preprocess(in_file, out_file);
		_already_included_files.clear();

		in_file.close();
		out_file.close();
	}

	void ShaderPreprocessor::global_preprocess(std::ifstream& in_file, std::ofstream& out_file)
	{
		this->preprocess(in_file, out_file);
	}

	void ShaderPreprocessor::preprocess(std::ifstream& in_file, std::ofstream& out_file)
	{
		string line;
		while(getline(in_file, line))
		{
			string perfect_line = "";
			perfect_line_syntax(line, perfect_line);
			preprocess_line(out_file, perfect_line);
		}
	}

	void ShaderPreprocessor::perfect_line_syntax(const std::string& line, std::string& perfect_line)
	{
		perfect_line.reserve(line.size());

		for(unsigned int i=0; i<line.size(); i++)
		{
			if(utils::is_string_detected(line, i, line_comment))
				return;

			if(utils::is_string_detected(line, i, comment_begin))
				_is_commented_line = true;


			if(!_is_commented_line)
			{
				//  - every uniform name is replaced by its new name (uniform_renaming argument in constructor) 
				if(utils::is_string_detected(line, i, BasicShaderParser::uniform_key))
				{
					i += BasicShaderParser::uniform_key.size();
					perfect_line += BasicShaderParser::uniform_key;

					if(i>=line.size())
					{
						err("Unnamed uniform called here: " + TERM::RED + line);
					}

					for(const auto& rename : _uniform_renaming)
					{
						if(utils::is_string_detected(line, i, rename.first))
						{
							i += rename.first.size();
							perfect_line += rename.second;
							break;
						}
					}
				}

				if(utils::is_in_string(line[i], separators) && perfect_line.size()!=0 && perfect_line[perfect_line.size()-1]==line[i])
				{
					continue;
				}
				// - there is a '\n' BEFORE AND AFTER every '{' and '}'
				bool has_to_end_line=false;
				if(line[i]=='}' || line[i]=='{')
				{
					has_to_end_line=true;
				}

				//  - every bracket ']' closure is not preceded by a space, tab or '\n'
				if(i+1<line.size() && line[i+1]==']')
				{
					if(utils::is_in_string(line[i], separators) || line[i] == '\n')
						continue;
				}

				if(has_to_end_line)
				{
					perfect_line.push_back('\n');
				}

				perfect_line.push_back(line[i]);

				if(has_to_end_line)
				{
					perfect_line.push_back('\n');
				}
			}
			else
			{
				if(utils::is_string_detected(line, i, comment_end))
				{
					_is_commented_line = false;
					i++;
				}
			}
		}
	}

	void ShaderPreprocessor::include(std::ofstream& out_file, const std::string& included_file_path, const std::string& include_line)
	{
		ifstream included_file(included_file_path);
		if(!included_file.is_open())
		{
			err("ShaderPreprocessor: in line :\n" + TERM::NOCOL + include_line + TERM::RED + "\n\nerror: file " + TERM::WHITE + included_file_path + TERM::RED + " not found");
		}

		this->preprocess(included_file, out_file);
		
		included_file.close();
	}

	void ShaderPreprocessor::preprocess_line(std::ofstream& out_file, const std::string& line)
	{
		for(unsigned int i=0; i<line.size(); i++)
		{
			if(utils::is_string_detected(line, i, KEY_INCLUDE))
			{
				string rest     = line.substr(i + KEY_INCLUDE.size());
				string included = get_quote_arg(get_arg(rest));

				if(included == ERR_QUOTE_ARG)
				{
					err("Compiling " + _in_file_directory + _in_file_name +", line:\n" + TERM::NOCOL + line + TERM::RED +"\nerror: WRONG BRACKETS");
				}

				if(_already_included_files.find(_in_file_directory + included) == _already_included_files.end())
				{
					string old_in_file_directory =_in_file_directory;
					auto file_dec = utils::extract_directory_and_file(included);
					_in_file_directory += file_dec.first;

					// cout<<"decomposing "<<file_dec.first <<" | "<<file_dec.second<<endl;
					// cout<<"including "<<_in_file_directory<<file_dec.second<<endl;
					
					this->include(out_file, _in_file_directory + file_dec.second, line);
					
					_in_file_directory = old_in_file_directory;
					_already_included_files.emplace(_in_file_directory + included);
				}
				
				return;
			}
		}

		out_file << line << endl;
	}

};
