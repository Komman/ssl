#ifndef _CAMERA_HPP_
#define _CAMERA_HPP_

#include "control_camera.hpp"
#include "window.hpp"

#include <vector>

namespace ssl
{
	namespace DEFAULT
	{
		namespace CAMERA
		{
			const std::vector<int> CONTROL_KEYS = {
				SSL_KEY(D),
				SSL_KEY(Q),
				SSL_KEY(Z),
				SSL_KEY(S),
				SSL_KEY(SPACE),
				SSL_KEY(LEFT_SHIFT)
			};

			constexpr float SENSIBILITY = 0.001;
			constexpr float SPEED = 1.00;
		}
	};


	class Camera : public ControlCamera
	{
	public:
		Camera(const glm::vec3& initial_position,
			   float min_view_distance,
			   float max_view_distance,
			   float speed                          = DEFAULT::CAMERA::SPEED, 
			   float sensibility                    = DEFAULT::CAMERA::SENSIBILITY, 
			   const std::vector<int>& control_keys = DEFAULT::CAMERA::CONTROL_KEYS,
			   float view_field                     = DEFAULT::VIEW_FIELD);

		enum CONTROL_KEY_INDICES {
			CAMERA_CONTROL_RIGHT=0,
			CAMERA_CONTROL_LEFT,
			CAMERA_CONTROL_FRONT,
			CAMERA_CONTROL_BACK,
			CAMERA_CONTROL_UP,
			CAMERA_CONTROL_DOWN,

			CAMERA_NUMBER_CONTROLS
		 };

		 const glm::vec3& get_speed_vector();

		void set_sensibility(float sensibility);
		void set_speed(float speed);

	protected:
		virtual void keys_reaction(const std::vector<unsigned int>& pressed_keys_indices);
		virtual void mouse_reaction(const glm::vec2& mouse_moving_pixel);


	private:
		float _speed;
		float _sensibility;

		glm::vec3 _speed_vector;
	};
};

#endif //_CAMERA_HPP_
