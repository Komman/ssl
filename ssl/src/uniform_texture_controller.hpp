#ifndef _UNIFORM_TEXTURE_CONTROLLER_HPP_
#define _UNIFORM_TEXTURE_CONTROLLER_HPP_

#include <iostream>
#include <glm/glm.hpp>

#include "basic_uniform_controller.hpp"
#include "debug.hpp"
#include "texture.hpp"

namespace ssl
{
	template<typename Type>
	class UniformTextureController : public BasicUniformController
	{
	public:
		UniformTextureController(const std::string name,
						  		 GLuint shader_to_link,
						  		 Texture<Type>* tex_ptr,
						  		 uint texture_binding_location);
		
		void activate_before_draw() const override;
		void change_texture(const Texture<Type>& new_texture);

	protected:

		void send_typed_data_to_GPU(void* data, int array_count) const override;

	private:

		Texture<Type> * _tex_ptr;
		uint _texture_binding_location;
	};

};

namespace ssl
{
	template<typename Type>
	UniformTextureController<Type>::UniformTextureController(const std::string name,
						  		    						 GLuint shader_to_link,
						  		    						 Texture<Type>* tex_ptr,
						  		    						 uint texture_binding_location)
		: BasicUniformController(name, shader_to_link),
		  _tex_ptr(tex_ptr),
		  _texture_binding_location(texture_binding_location)
	{	
		this->up_to_date(true);
	}

	template<typename Type>
	void UniformTextureController<Type>::send_typed_data_to_GPU(void* data, int array_count) const
	{
		err("An uniform texture shoudl never been updated to the GPU throught send_typed_data_to_GPU()");
		//_tex_ptr = (Texture<Type>*)(data);
	}

	template<typename Type>
	void UniformTextureController<Type>::activate_before_draw() const
	{
		_tex_ptr->bind_on_texture_location(_texture_binding_location);
        //std::cout<<"bound on "<< _texture_binding_location <<std::endl;
	}
	
	template<typename Type>
	void UniformTextureController<Type>::change_texture(const Texture<Type>& new_texture)
	{
		_tex_ptr = ((Texture<Type>*)&new_texture);
	}

};

#endif //_UNIFORM_TEXTURE_CONTROLLER_HPP_
