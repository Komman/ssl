#include <iostream>
#include <string>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp> 

#include "basic_window.hpp"
#include "debug.hpp"
#include "global_drawing.hpp"
#include "window.hpp"

using namespace ssl;
using namespace std;
using namespace glm;

namespace ssl
{
	float BasicWindow::_dt = 0.0;

	float BasicWindow::delta_time()
	{
		return _dt;
	}

	static void ssl_glfw_error_callback(int, const char* err_str)
	{
	    std::cout << "GLFW Error: " << err_str << std::endl;
	}

	void BasicWindow::create_context()
	{
		glfwSetErrorCallback(ssl_glfw_error_callback);
		
		if( !glfwInit() ) 
		{
		    err("Failed to initialize GLFW");  
		}

		GL(glfwWindowHint(GLFW_SAMPLES, 4)); // antialiasing 4x 
	}

	static void(*_char_callback)(GLFWwindow* w, unsigned int codepoint) = NULL;
	static void(*_key_callback)(GLFWwindow *window, int key, int scancode, int action, int mods) = NULL;

	static void func_char_callback(GLFWwindow* w, unsigned int codepoint)
	{
		if(_char_callback != NULL)
		{
			// The char get by the glfw char callback is azert/qwerty independent 
			_char_callback(w,codepoint);
		}
	}
	static void func_key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
	{
		if(_key_callback != NULL)
		{
			// But the jey get by the glfw key callback is needs to be translated in azerty 
			_key_callback(window, keyboard::qwerty_to_azerty(key), scancode, action, mods);
		}
	}

	void BasicWindow::set_char_callback(void(*f)(GLFWwindow* w, unsigned int codepoint))
	{
		_char_callback = f;
		GL(glfwSetCharCallback(_window, func_char_callback));
	}

	void BasicWindow::set_key_callback(void(*f) (GLFWwindow *window, int key, int scancode, int action, int mods))
	{
		_key_callback = f;
		GL(glfwSetKeyCallback(_window, func_key_callback));
	}

	void BasicWindow::destroy_context()
	{
		GL(glfwTerminate());
	}

	void BasicWindow::configure()
	{
		GL(glfwMakeContextCurrent(_window)); 
		 
		// Initialise GLEW 
		glewExperimental=true; // Nécessaire pour le profil core
		if (glewInit() != GLEW_OK)
		    err("Failed to initialize GLEW"); 

		GL(glfwSetInputMode(_window, GLFW_STICKY_KEYS, GL_FALSE));

		_t_fps=glfwGetTime();
		_time_old=glfwGetTime();

		if constexpr(DEFAULT::ENABLE_CULLING)
		{
			global_drawing::culling(true);
		}

		if constexpr(DEFAULT::ENABLE_BLEND)
		{
			global_drawing::blending(true);
		}
		global_drawing::set_blend_function();

		if constexpr(DEFAULT::ENABLE_DEPTH)
		{
			global_drawing::depth(true);
		}
	}

	

	BasicWindow::BasicWindow(const std::string& name)
	{
		if(_creation_step != 0)
		{
			err("BasicWindow already created");
		}
		_creation_step=1;
		_fps=0;

		int pos_x;
		int pos_y;
		int size_x;
		int size_y;

		GLFWmonitor* primary = glfwGetPrimaryMonitor();
		
		glfwGetMonitorWorkarea(primary, &pos_x, &pos_y, &size_x, &size_y);

		//TOMOD
		size_x += pos_x % size_x;
		size_y += pos_y % size_y;


		// cout<<"Window creation: pos = ("<<pos_x<<", "<<pos_y<<"), and size = ("<<size_x<<", "<<size_y<<")"<<endl;

		// Ouvre une fenêtre et crée son contexte OpenGL
		_window = glfwCreateWindow(size_x, size_y, name.c_str(), primary, NULL); 
		if( _window == NULL ){ 
			GL(glfwTerminate());
		    err("Failed to open GLFW window."); 
		}
		_win_size=ivec2(size_x,size_y); 

		this->configure();
	}

	BasicWindow::BasicWindow(const std::string& name, unsigned int window_divisor)
	{
		if(_creation_step != 0)
		{
			err("BasicWindow already created");
		}
		_creation_step=1;
		_fps=0;

		int size_x;
		int size_y;
		GLFWmonitor* primary = glfwGetPrimaryMonitor();
		glfwGetMonitorWorkarea(primary, NULL, NULL, &size_x, &size_y);
		int intdivisor = window_divisor;
		size_x/=intdivisor;
		size_y/=intdivisor;
		size_x*=intdivisor;
		size_y*=intdivisor;

		// Ouvre une fenêtre et crée son contexte OpenGL
		_window = glfwCreateWindow(size_x, size_y, name.c_str(), NULL, NULL); 
		if( _window == NULL ){ 
			GL(glfwTerminate());
		    err("Failed to open GLFW window."); 
		}
		_win_size=ivec2(size_x,size_y); 

		this->configure();
	}


	BasicWindow::BasicWindow(uint size_x, uint size_y, const string& name)
	{
		if(_creation_step != 0)
		{
			err("BasicWindow already created");
		}
		_creation_step=1;
		_fps=0;

		// Ouvre une fenêtre et crée son contexte OpenGL
		_window = glfwCreateWindow(size_x, size_y, name.c_str(), NULL, NULL); 
		if( _window == NULL ){ 
			GL(glfwTerminate());
		    err("Failed to open GLFW window."); 
		}
		_win_size=ivec2(size_x,size_y); 

		this->configure();
	}

	void BasicWindow::reset_frame_time()
	{
		_time_old=glfwGetTime();
	}

	void BasicWindow::flip()
	{
		if(_creation_step != 1)
		{
			err("Try to BasicWindow::flip while BasicWindow is closed");
		}

		#ifdef SSL_PRINTS
        prints_msg(SSL_PRINTS_DRAW, TERM::WHITE + "========================================");
        prints_msg(SSL_PRINTS_DRAW, TERM::BLACK + "========================================");
        prints_msg(SSL_PRINTS_DRAW, TERM::WHITE + "===========   WINDOW  FLIP  ============");
        prints_msg(SSL_PRINTS_DRAW, TERM::BLACK + "========================================");
        prints_msg(SSL_PRINTS_DRAW, TERM::WHITE + "========================================");
        #endif
		
		GL(glfwSwapBuffers(_window));

		GL(glfwPollEvents());
	    _dt=glfwGetTime()-_time_old;
	    this->reset_frame_time();

        if(glfwGetTime()-_t_fps>1.0)
        {
            _t_fps=glfwGetTime();

            if constexpr (PRINT_FPS == true)
            {
            	printf("FPS: %d\n", _fps);
            }

            _fps=0;
        }

		_fps++; 
	}

	bool BasicWindow::should_close() const
	{
		if(_creation_step != 1)
		{
			err("Try to call BasicWindow::should_close while BasicWindow is closed");
		}

		return glfwWindowShouldClose(_window);
	}

	void BasicWindow::close()
	{
		if(_creation_step != 1)
		{
			if(_creation_step == 0)
				err("BasicWindow not created");
			if(_creation_step == 2)
				err("BasicWindow already closed");
		}
		_creation_step=2;
		GL(glfwDestroyWindow(_window));
	}

	const ivec2& BasicWindow::size() const
	{
		return _win_size;
	}

	void BasicWindow::clear()
	{
		if(_creation_step != 1)
		{
			err("Try to call BasicWindow::clear while BasicWindow is closed");
		}		
		global_drawing::clear();
	}
};
