#include "shader_builder.hpp"

#include <sys/types.h>
#include <sys/stat.h>
#include <vector>
#include <fstream>
#include <filesystem>

#include "shader_preprocessor.hpp"
#include "shader_parser.hpp"
#include "color.hpp"
#include "utils.hpp"

using namespace std;
using namespace ssl;
using functionInfo = ShaderParser::functionInfo;


namespace ssl
{
	struct varInfo
	{
		string  type;
		int     count;
	};


	static vector<string> flat_types={
		"double",
		"dvec2",
		"dvec3",
		"dvec4",
		"dmat4",
		"dmat3",
		"dmat2",
		"int",
		"uint",
		"ivec2",
		"ivec3",
		"ivec4",
		"uvec2",
		"uvec3",
		"uvec4"
	};

	unsigned int ShaderBuilder::location_count(const std::string& type)
	{
		static map<string, unsigned int> location_type={
			//TODO
			// NOT IMPLEMENTED YET:

			// matIxJ

			{"mat2", 1},
			{"mat3", 3},
			{"mat4", 4},

			{"dmat2", 2},
			{"dmat3", 5},
			{"dmat4", 8},

			{"ivec2", 1},
			{"ivec3", 1},
			{"ivec4", 1},

			{"uvec2", 1},
			{"uvec3", 1},
			{"uvec4", 1},

			{"bvec2", 1},
			{"bvec3", 1},
			{"bvec4", 1},

			{"vec2", 1},
			{"vec3", 1},
			{"vec4", 1},

			{"dvec2", 1},
			{"dvec3", 2},	
			{"dvec4", 2},

			{"int", 1},
			{"uint", 1},
			{"float", 1},
			{"double", 1},
			{"bool", 1}
		};

		#ifdef SSL_DEBUG
		if(location_type.find(type) == location_type.end())
		{
			err("Compiling shaders: " + TERM::RED +  "in" + ERR_COLOR + " argument of the vertex shader has not a valid type:\n"
				+ "type: " + TERM::BLUE + type + ERR_COLOR +" is not allowed");
		}
		#endif

		return location_type[type];
	}



	ShaderBuilder::ShaderBuilder(const BasicShaderParser& parser,
								 const std::string& vertex_shader_name,
			  	                 const std::string& fragment_shader_name,
			   					 const std::vector<std::string>& fragment_modifiers)
		: ShaderPreBuilder(parser),
		  _vertex_ins(0),
		  _fragment_outs(0),
		  _uniforms(0),
		  _vertex_function_name(vertex_shader_name),
		  _fragment_function_name(fragment_shader_name),
		  _fragment_modifiers(fragment_modifiers),
		  _initialized(false),
		  _constructor(true)
	{
		this->try_to_initialize();
		_constructor = false;	
	}

	void ShaderBuilder::print_lazy_compilation()
	{
		if(ssl::are_prints_enable(SSL_PRINTS_LAZY_OP))
		{
			ssl::prints_msg(SSL_PRINTS_LAZY_OP, TERM::CYAN + "Lazy compilation of shader" + TERM::WHITE + " n:"
												+ std::to_string(this->get_ID()) + TERM::NOCOL + " (" 
												+ this->get_vertex_function_name() + " --> " + this->get_fragment_function_name() + ")");
		}
	}

	void ShaderBuilder::has_to_initialize()
	{
		if(!_initialized)
		{
			this->initialize();
		}
	}

	bool ShaderBuilder::try_to_initialize()
	{
		if(!_initialized)
		{
			std::vector<std::string> vars;
			std::vector<std::string> funcs;
			std::vector<std::string> uniforms;

			this->fill_dependency(_parser->get_function(_vertex_function_name), vars, funcs, uniforms);
			for(auto& modif : _fragment_modifiers)
			{
				funcs.push_back(modif);
			}
			for(auto& modif : _fragment_modifiers)
			{
				fill_dependency(_parser->get_function(modif), vars, funcs, uniforms);
			}
			this->fill_dependency(_parser->get_function(_fragment_function_name), vars, funcs, uniforms);


			bool all_uniforms_declared = true;
			for(auto u : uniforms)
			{
				// cout<<u<<endl;

				if(!(ShaderPreBuilder::is_uniform_declared(u)))
				{
					all_uniforms_declared = false;
					break;
				}
			}

			if(all_uniforms_declared)
			{
				this->initialize();
			}
		}

		return _initialized;
	}

	void ShaderBuilder::initialize()
	{
		if(_initialized)
		{
			err("ShaderBuilder::initialize(): already initialized");
		}

		string shader_name = _vertex_function_name + "_" + _fragment_function_name;

		vector<ofstream> files;
		vector<string>   names;

		string shaders_file_path = utils::extract_directory(_parser->get_file_name()) + "/"; 

		string gen_vertex_shader_path   = shaders_file_path + shader_name + ".vert";
		string gen_fragment_shader_path = shaders_file_path + shader_name + ".frag";

		files.push_back(ofstream(gen_vertex_shader_path));
		files.push_back(ofstream(gen_fragment_shader_path));

		names.push_back(_vertex_function_name);
		names.push_back(_fragment_function_name);

		if(!files[VERTEX].is_open())
		{
			err("Failed to generate the vertex shader : " + gen_vertex_shader_path);
		}
		if(!files[FRAGMENT].is_open())
		{
			err("Failed to generate the fragment shader : " + gen_fragment_shader_path);
		}

		this->generate_shader(files, names, _fragment_modifiers);

		for(auto& f : files)
		{
			f.close();
		}

		this->load(gen_vertex_shader_path, gen_fragment_shader_path);
		
		_fragment_modifiers.clear();
		_initialized = true;

		#ifdef SSL_PRINTS
		if(!_constructor)
		{
			this->print_lazy_compilation();
		}
		#endif
	}


	std::string ShaderBuilder::additionnal_name() const
	{
		return TERM::GREEN + _vertex_function_name + " -> " + _fragment_function_name;
	}


	void ShaderBuilder::generate_shader(std::vector<std::ofstream>& files,
							 	        std::vector<std::string>&   names,
			   							const std::vector<std::string>& fragment_modifiers)
	{
		if(files.size() != names.size())
			err("Files and names have not the same element quantity " + SSL_FILE_AND_LINE );

		vector<ShaderParser::functionInfo> infos;
		infos.push_back(_parser->get_function(names[VERTEX]));
		infos.push_back(_parser->get_function(names[FRAGMENT]));

		this->check_infos(infos);

		for(auto& f : files)
			f << ShaderPreprocessor::glsl_version() << endl << endl;

		this->write_ins_outs(files, infos, fragment_modifiers);

		this->write_necessary(files, infos, fragment_modifiers);

	}

	void ShaderBuilder::write_necessary(std::vector<std::ofstream>&              files,
					         	 		std::vector<ShaderParser::functionInfo>& infos,
			   							const std::vector<std::string>& fragment_modifiers)
	{
		if(files.size() != 2 || infos.size() != 2)
		{
			err("write_necessary must be called with the fragment and vertex files/infos");
		}

		for(unsigned int shid=0; shid<2; shid++)
		{
			auto& file = files[shid];
			auto& info = infos[shid];

			vector<string> local_vars;
			vector<string> local_funcs;

			if(shid==FRAGMENT)
			{
				for(auto& modif : fragment_modifiers)
				{
					local_funcs.push_back(modif);
				}
				for(auto& modif : fragment_modifiers)
				{
					fill_dependency(_parser->get_function(modif), local_vars, local_funcs, _uniforms);
				}
			}

			fill_dependency(info, local_vars, local_funcs, _uniforms);

			file << endl;
			this->write_uniforms(file, _uniforms);

			file << endl;
			this->write_vars(file, local_vars);

			this->write_funcs(file, local_funcs);

			file << endl;
			file << "void main() // Generated from : " << infos[shid].function_type << " " << infos[shid].function_name << "( ... )" << endl;
			file << "{" << endl;
			string main_core;
			arrange_code(info.function_core, main_core);
			file << main_core << endl;
			file << "}" << endl;
		}
	}

	// flat for doubles types
	string ShaderBuilder::key_type_cond(const ShaderParser::argDec& arg, uint shader_type)
	{
		if(shader_type == FRAGMENT && utils::in_vector(flat_types, arg.type))
		{
			return "flat ";
		}
		return "";
	}

	void ShaderBuilder::write_ins_outs(std::vector<std::ofstream>& files,
					                   std::vector<ShaderParser::functionInfo>&   infos,
					                   const std::vector<std::string>& fragment_modifiers)
	{
		vector<  vector<  ShaderParser::argDec >  > ins(2,  vector<ShaderParser::argDec>(0));
		vector<  vector<  ShaderParser::argDec >  > outs(2, vector<ShaderParser::argDec>(0));
//    vert/frag |  args 


		uint current_location = 0;
		for(uint shid=0; shid<2; shid++)
		{
			for(const auto& args : infos[shid].args)
			{
				if(shid == FRAGMENT && args.inout == ShaderParser::out_key)
				{
					_fragment_outs.push_back(get_type_id(args.type));
				}

				if(args.inout == ShaderParser::in_key ||  args.inout == ShaderParser::instance_key || args.inout == "")
				{
					ins[shid].push_back(args);

					if(shid == VERTEX)
					{
						if(args.array_count == ShaderParser::ARRAY_COUNT_NOT_SIZED)
							err("vertex shader " + TERM::RED + "in" + ERR_COLOR + " arguments cannot have a not-fixed size");

						uint number_location_used = location_count(args.type)*abs(args.array_count);

						files[shid] << "layout(location = " << current_location << ") ";
						_vertex_ins.push_back({
							.location       = current_location,
							.location_count = number_location_used,
							.instanced      = (args.inout == ShaderParser::instance_key),
							.type           = get_type_id(args.type)
						});
						
						current_location += number_location_used;
					}

					files[shid] << key_type_cond(args, shid) << "in " << args.type << " " << args.name << ShaderParser::array_ext(args.array_count) << ";";


					if(args.inout == ShaderParser::instance_key)
					{
						if(shid == FRAGMENT)
							err("Fragment shader cannot have instance arguments\nThey must be send by the vertex shader");
						
						files[shid] << " // Instance variable";
					}

					files[shid] << endl;
				}

				if(args.inout == ShaderParser::inout_key)
					err(TERM::CYAN + "void " + infos[shid].function_name + "( ... )" + ERR_COLOR + ": Vertex/Fragment shaders cannot have \"" + TERM::RED + "inout" + ERR_COLOR + "\" arguments");
				
				if(args.inout == ShaderParser::out_key)
				{
					outs[shid].push_back(args);
				}
			}

			files[shid] << endl;
		}

		uint frag_out_locations = 0;
		for(auto& frag_out : outs[FRAGMENT])
		{
			files[FRAGMENT]<< "layout (location = "<<frag_out_locations<<") out " << frag_out.type << " " << frag_out.name << ShaderParser::array_ext(frag_out.array_count) << ";" << endl;
			frag_out_locations+=location_count(frag_out.type);
		}

		if(outs[VERTEX].size() != ins[FRAGMENT].size())
		{
			err("Shader compilation: vertex shader function must have as much" + TERM::RED + " out " + ERR_COLOR + "arguments as fragment shader's " + TERM::RED +  "in" + ERR_COLOR +" arguments\n"
				+ TERM::NOCOL + "Here: vertex shader = " + TERM::CYAN + "void " + infos[VERTEX].function_name + "( ... )" + TERM::BLUE + " (" + to_string(outs[VERTEX].size()) + " out args) " + TERM::NOCOL + " and fragment shader = " 
				+ TERM::CYAN + "void " + infos[VERTEX].function_name + "( ... )" + TERM::BLUE + " (" + to_string(outs[FRAGMENT].size()) + " in args)");
		}

		infos[VERTEX].function_core+="\n\t\n\t// ADDED BY SSL; To send good data names to the fragment shader\n";
		
		this->write_vertex_out(files, infos, ins, outs);

		this->add_vertex_main_out_declarations(infos, ins, outs);

		this->write_fragment_modifiers(infos[FRAGMENT], fragment_modifiers);

	}

	static std::vector<const ShaderParser::argDec*>  get_outs_then_ins_args(const ShaderParser::functionInfo& args_info)
	{
		std::vector<const ShaderParser::argDec*> ret;
		std::vector<const ShaderParser::argDec*> ins;

		for(const ShaderParser::argDec& arg : args_info.args)
		{
			if(arg.inout == ShaderParser::out_key)
			{
				ret.push_back(&arg);
			}
			else if(arg.inout == ShaderParser::in_key)
			{
				ins.push_back(&arg);
			}
			else if(arg.inout == ShaderParser::inout_key)
			{
				err("inout arguments in a function that is not supposed to allow it");
			}
		}

		for(const ShaderParser::argDec* arg_ptr : ins)
		{
			ret.push_back(arg_ptr);
		}

		return ret;
	}

	void ShaderBuilder::check_fragment_modifier(ShaderParser::functionInfo& frag_infos,
									  			ShaderParser::functionInfo& modifier_infos)
	{
		if(modifier_infos.args.size() > frag_infos.args.size())
		{
			err("The fragment modifier " + TERM::BLUE + modifier_infos.function_name + ERR_COLOR 
				+  " takes " + to_string(modifier_infos.args.size()) + " arguments so it cannot be attached to the fragment shader "
				+ TERM::BLUE + frag_infos.function_name + ERR_COLOR +  " that takes " + to_string(frag_infos.args.size()) + " arguments");
		}
	}

	void ShaderBuilder::check_modifiers_link_to_fragment(const ShaderParser::argDec& frag_arg,
														 const ShaderParser::argDec& modif_arg,
														 bool inout_arg,
														 const ShaderParser::functionInfo& frag_infos,
														 const ShaderParser::functionInfo& frag_modifier_func)
	{
		if(frag_arg.type != modif_arg.type)
		{
			err("Wrong type in fragment modifier " + TERM::BLUE + frag_modifier_func.function_name + ERR_COLOR + ": try to link argument " + TERM::RED + modif_arg.type + TERM::BLUE  + " " + modif_arg.name + ERR_COLOR
				+ " on the argument " + TERM::RED + frag_arg.type + TERM::BLUE + " " + frag_arg.name + ERR_COLOR + " of the fragment shader " + TERM::BLUE + frag_infos.function_name + ERR_COLOR );
		}

		if(modif_arg.inout == ShaderParser::out_key)
		{
			err("A fragment modifier cannot have " + TERM::RED + "out" + ERR_COLOR + " arguments (here: " + TERM::BLUE 
				+ frag_modifier_func.function_name + ERR_COLOR + " has the argument: "  ")");
		}
		else if(modif_arg.inout == ShaderParser::inout_key)
		{
			if(!inout_arg)
			{
				err("The fragment modifier " + TERM::BLUE + frag_modifier_func.function_name + ERR_COLOR + " has the argument " + TERM::BLUE 
					+ modif_arg.name + ERR_COLOR + " that is " + TERM::RED + "inout" + ERR_COLOR + " , but after an " + TERM::RED + "in" + ERR_COLOR + " argument (illegal order)"); 
			}
			if(frag_arg.inout != ShaderParser::out_key)
			{
				err("The fragment modifier " + TERM::BLUE + frag_modifier_func.function_name + ERR_COLOR + " has too many " + TERM::RED + "inout" + ERR_COLOR 
					+ " arguments for the fragment shader " + TERM::BLUE + frag_infos.function_name + ERR_COLOR);
			}
		}
		else if(modif_arg.inout == ShaderParser::in_key)
		{
			if(frag_arg.inout != ShaderParser::in_key)
			{
				err("The fragment modifier " + TERM::BLUE + frag_modifier_func.function_name + ERR_COLOR + " has too many " + TERM::RED + "in" + ERR_COLOR 
					+ " arguments for the fragment shader " + TERM::BLUE + frag_infos.function_name + ERR_COLOR);
			}
		}
	}

	int ShaderBuilder::indices_to_next_in_argument(const std::vector<const ShaderParser::argDec*>& fragment_ordered_args, int fragment_arg_index)
	{
		int count = 0;
		while(fragment_arg_index+count < (int)(fragment_ordered_args.size()) && (fragment_ordered_args[fragment_arg_index + count])->inout != ShaderParser::in_key)
		{
			count++;
		}
		return count;
	}

	void ShaderBuilder::write_fragment_modifiers(ShaderParser::functionInfo& frag_infos,
									  			 const std::vector<std::string>& fragment_modifiers)
	{
		for(auto& modifier : fragment_modifiers)
		{
			auto frag_modifier_func = _parser->get_function(modifier);

			this->check_fragment_modifier(frag_infos, frag_modifier_func);

			frag_infos.function_core += "\t";
			frag_infos.function_core += modifier;
			frag_infos.function_core += "(";

			bool inout_arg = true;
			int fragment_arg_index = 0;
			auto fragment_ordered_args = get_outs_then_ins_args(frag_infos);

			if(fragment_ordered_args.size() < frag_infos.args.size())
				err("More arguments in fragment modifiers than fragment shader found");


			for(uint i=0; i<frag_modifier_func.args.size(); i++)
			{
				auto& modif_arg = frag_modifier_func.args[i];
				
				if(inout_arg && modif_arg.inout == ShaderParser::in_key)
				{
					fragment_arg_index += this->indices_to_next_in_argument(fragment_ordered_args, fragment_arg_index);
				}
				if(fragment_arg_index == (int)(fragment_ordered_args.size()))
				{
					err("Wrong arguments count in fragment modifier " + TERM::BLUE + frag_modifier_func.function_name + ERR_COLOR + ": try to link argument " + TERM::BLUE + modif_arg.type + " " + modif_arg.name + ERR_COLOR
						+ " while the fragment shader " + TERM::BLUE + frag_infos.function_name + ERR_COLOR + " has not enough " + TERM::RED + "in" + ERR_COLOR + " arguments to link with");
				}
				if(modif_arg.inout == ShaderParser::in_key)
				{
					inout_arg = false;
				}

				auto& frag_arg = *(fragment_ordered_args[fragment_arg_index]);

				this->check_modifiers_link_to_fragment(frag_arg, modif_arg, inout_arg, frag_infos, frag_modifier_func);

				if(i != 0)	frag_infos.function_core += ", ";
				frag_infos.function_core += frag_arg.name;
				fragment_arg_index++;
			}

			frag_infos.function_core += ");\n";
		}
	}

	void ShaderBuilder::add_vertex_main_out_declarations(std::vector<ShaderParser::functionInfo>& infos,
												         vector<vector<ShaderParser::argDec>>&    ins,
												         vector<vector<ShaderParser::argDec>>&    outs)
	{
		string premain;
		for(auto& out_vertex : outs[VERTEX])
		{
			bool already_out = false;
			for(auto& in_fragment : ins[FRAGMENT])
			{
				if(out_vertex.name == in_fragment.name)
				{
					already_out = true;
					break;
				}
			}

			if(!already_out)
			{
				premain+= "\t" + out_vertex.type + " " + out_vertex.name + ShaderParser::array_ext(out_vertex.array_count) + ";\n";
			}
		}
		if(premain != "")
		{
			infos[VERTEX].function_core = premain + infos[VERTEX].function_core;
		}
	}

	void ShaderBuilder::write_vertex_out( std::vector<std::ofstream>&              files,
								          std::vector<ShaderParser::functionInfo>& infos,
								          vector<vector<ShaderParser::argDec>>&    ins,
								          vector<vector<ShaderParser::argDec>>&    outs)
	{
		for(unsigned int arg_id = 0; arg_id < outs[VERTEX].size(); arg_id++)
		{
			auto& vert_out = outs[VERTEX][arg_id];
			auto& frag_in  = ins[FRAGMENT][arg_id];
			
			// check types
			if(vert_out.type != frag_in.type)
			{
				err("Shader compilation: vertex" + TERM::RED + " out" + ERR_COLOR + " arguments must be of the same type as fragment" + TERM::RED + " in" + ERR_COLOR + " arguments\n"
					+ TERM::NOCOL + "Here: vertex shader = " + TERM::PURPLE + "void " + infos[VERTEX].function_name + "( ... )" + TERM::NOCOL + " and fragment shader = " + TERM::PURPLE + "void " + infos[VERTEX].function_name + "( ... )\n"
					+ TERM::NOCOL + "And argument " + TERM::CYAN +  vert_out.name + TERM::BLUE + " (" + vert_out.type + ")" + TERM::NOCOL + " is not the same type as " + TERM::CYAN + frag_in.name + TERM::BLUE + " (" + frag_in.type + ")");
			}

			files[VERTEX] << "out " << frag_in.type << " " << frag_in.name << ShaderParser::array_ext(frag_in.array_count) << ";" << endl;

			if(vert_out.name != frag_in.name)
			{
				infos[VERTEX].function_core += "\t" + frag_in.name + " = " + vert_out.name + ";\n";
			}
		}
	}	

	void ShaderBuilder::check_infos(std::vector<ShaderParser::functionInfo>& infos)
	{
		if(infos[VERTEX].function_type != "void")
			err("Shader constructor: vertex shader can't return something (function type must be void)" + TERM::NOCOL 
				 + "\nHere, function " + TERM::BLUE + infos[VERTEX].function_name + "(...)" + TERM::NOCOL + " has the return type " + TERM::RED + infos[VERTEX].function_type + TERM::NOCOL);
		if(infos[FRAGMENT].function_type != "void")
			err("Shader constructor: fragment shader can't return something (function type must be void)" + TERM::NOCOL 
				 + "\nHere, function " + TERM::BLUE + infos[FRAGMENT].function_name + "(...)" + TERM::NOCOL + " has the return type " + TERM::RED + infos[FRAGMENT].function_type + TERM::NOCOL);
	}

	const std::vector<std::string>& ShaderBuilder::get_uniforms_names()
	{
		#ifdef SSL_DEBUG
		this->has_to_initialize();
		#endif

		return _uniforms;
	}

	const std::vector< ShaderBuilder::vertexIN >&  ShaderBuilder::get_vertex_ins()
	{
		#ifdef SSL_DEBUG
		this->has_to_initialize();
		#endif

		return _vertex_ins;
	}
	const std::vector< types_id >&  ShaderBuilder::get_fragment_outs()
	{
		#ifdef SSL_DEBUG
		this->has_to_initialize();
		#endif

		return _fragment_outs;
	}

	const std::string&  ShaderBuilder::get_vertex_function_name() const
	{
		return _vertex_function_name;
	}
	const std::string&  ShaderBuilder::get_fragment_function_name() const
	{
		return _fragment_function_name;
	}

	bool ShaderBuilder::is_initialized() const
	{
		return _initialized;
	}
};

