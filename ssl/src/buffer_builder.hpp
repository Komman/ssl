#ifndef _BUFFER_BUILDER_HPP_
#define _BUFFER_BUILDER_HPP_

#include "array_buffer.hpp"

namespace ssl
{
	namespace buffers
	{
		const ArrayBuffer<glm::vec2>& rectangle();
		const ArrayBuffer<glm::vec2>& screenfill();
		const ArrayBuffer<glm::vec3>& screenfill3D();
	};


	namespace buffer_builder
	{
		std::vector<glm::vec3> hexahedron_points(const glm::vec3& origin,
												 const glm::vec3& direction1,
												 const glm::vec3& direction2,
												 const glm::vec3& direction3);
		std::vector<glm::vec3> aligned_square_points(const glm::vec3& center, float size);

		// used by ssl
		void init();
		void free();
	};
};

#endif //_BUFFER_BUILDER_HPP_
