#ifndef _DRAWING_CONTEXT_HPP_
#define _DRAWING_CONTEXT_HPP_

#include <glm/glm.hpp>

namespace ssl
{
	class DrawingContext
	{
	public:
		DrawingContext();
		virtual ~DrawingContext();

		void create();
		void bind() const;

		const glm::vec4& get_clear_color();
		void set_clear_color(const glm::vec4& color);

	private:
		bool         _initialized;
		unsigned int _VAO;	 
		glm::vec4    _clear_color;
	};
};

#endif //_DRAWING_CONTEXT_HPP_
