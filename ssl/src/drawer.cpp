#include "drawer.hpp"
#include "window.hpp"

using namespace std;
using namespace glm;

namespace ssl
{
	static Drawer::stencilParams default_stencil_params = {
		.enable    = false,
		.mask      = 0xFF,
		.function  = GL_EQUAL,
		.func_ref  = 1,
		.func_mask = 0xFF,
		.op_sfail  = GL_KEEP,
		.op_dpfail = GL_KEEP,
		.op_dppass = GL_KEEP,
	};

	Drawer::Drawer(const BasicShaderParser& parser,
				   const std::string& vertex_shader_name,
				   const std::string& fragment_shader_name,
				   const std::vector<std::string>& fragment_modifiers)
		: Shader(parser, vertex_shader_name, fragment_shader_name, fragment_modifiers),
		  _draw_params((drawParams){
		  	.view_port_origin = glm::uvec2(0.0, 0.0),
		  	.view_port_size   = glm::uvec2(0.0, 0.0),
		  	.auto_view_port   = true,
		  	.depth            = DEFAULT::ENABLE_DEPTH,
		  	.culling          = DEFAULT::ENABLE_CULLING,
		  	.blending         = DEFAULT::ENABLE_BLEND,
		  	.depth_mask       = DEFAULT::ENABLE_DEPTH_MASK,
		  	.depth_func       = global_drawing::DEFAULT::DEPTH_FUNC,
		  	.blend_sfactor    = global_drawing::DEFAULT::SFACTOR,
		  	.blend_dfactor    = global_drawing::DEFAULT::DFACTOR,
		  	.blend_func_eq    = global_drawing::DEFAULT::BLEND_EQ,
		  	.stencil_params   = default_stencil_params})
	{

	}

	void Drawer::draw(const VertexArrayObject& vao)
	{
		this->target_screen();
		this->set_all_params();
		Shader::draw(vao);
	}

	void Drawer::draw(const BasicFrameBuffer& target_fb, const VertexArrayObject& vao)
	{
		this->target_framebuffer(&target_fb);
		this->set_all_params();
		Shader::draw(vao);
	}

	void Drawer::set_draws_params(const drawParams& params)
	{
		_draw_params = params;
	}


	const Drawer::drawParams& Drawer::get_draws_params() const
	{
		return _draw_params;
	}


	void Drawer::depth(bool enable)
	{
		_draw_params.depth = enable;
	}

	void Drawer::culling(bool enable)
	{
		_draw_params.culling = enable;
	}

	void Drawer::depth_mask(bool enable)
	{
		_draw_params.depth_mask = enable;
	}

	void Drawer::depth_function(GLenum func)
	{
		_draw_params.depth_func = func;
	}


	void Drawer::custom_view_port(const glm::uvec2& origin, const glm::uvec2& size)
	{
		_draw_params.view_port_origin = origin;
		_draw_params.view_port_size   = size;
		_draw_params.auto_view_port   = false;
	}

	void Drawer::disable_custom_view_port()
	{
		_draw_params.auto_view_port = true;
	}

	void Drawer::blending(bool enable)
	{
		_draw_params.blending = enable;
	}

	//see openGL::glBlendFunc function
	void Drawer::set_blend_function(GLenum sfactor, GLenum dfactor)
	{
		_draw_params.blend_sfactor = sfactor;
		_draw_params.blend_dfactor = dfactor;
	}

	void Drawer::set_blend_equation(GLenum mode)
	{
		_draw_params.blend_func_eq = mode;
	}

	void Drawer::set_stencil_params(const stencilParams& params)
	{
		_draw_params.stencil_params = params;
	}

	void Drawer::stencil_testing(bool enable)
	{
		_draw_params.stencil_params.enable = enable;
	}

	void Drawer::stencil_mask(GLuint mask)
	{
		_draw_params.stencil_params.mask = mask;
	}

	void Drawer::stencil_function(GLenum func, GLint ref, GLuint mask)
	{
		_draw_params.stencil_params.function  = func;
		_draw_params.stencil_params.func_ref  = ref;
		_draw_params.stencil_params.func_mask = mask;
	}

	void Drawer::stencil_op(GLenum sfail, GLenum dpfail, GLenum dppass)
	{
		_draw_params.stencil_params.op_sfail  = sfail;
		_draw_params.stencil_params.op_dpfail = dpfail;
		_draw_params.stencil_params.op_dppass = dppass;
	}

	void Drawer::draw_object(const DrawableObject& obj)
	{
		this->target_screen();
		this->set_all_params();
		this->Shader::draw_object(obj);
	}

	void Drawer::draw_object(const ElementDrawableObject& obj)
	{
		this->target_screen();
		this->set_all_params();
		this->Shader::draw_object(obj);
	}

	void Drawer::draw_object(const BasicFrameBuffer& target_fb, const DrawableObject& obj)
	{
		this->target_framebuffer(&target_fb);
		this->set_all_params();
		this->Shader::draw_object(obj);
	}
	
	void Drawer::draw_object(const BasicFrameBuffer& target_fb, const ElementDrawableObject& obj)
	{
		this->target_framebuffer(&target_fb);
		this->set_all_params();
		this->Shader::draw_object(obj);
	}

	void Drawer::target_screen()
	{
		if(_draw_params.auto_view_port)
		{
			global_drawing::target_screen();
		}
		else
		{
			global_drawing::bind_screen();
			global_drawing::set_view_port(_draw_params.view_port_origin, _draw_params.view_port_size);
		}
	}

	void Drawer::target_framebuffer(const BasicFrameBuffer* framebuffer_ot_target)
	{
		if(_draw_params.auto_view_port)
		{
			global_drawing::target(framebuffer_ot_target);
		}
		else
		{
			global_drawing::bind_framebuffer(framebuffer_ot_target);
			global_drawing::set_view_port(_draw_params.view_port_origin, _draw_params.view_port_size);
		}
		
		#ifdef SSL_DEBUG
		this->check_attachements(framebuffer_ot_target);
		#endif
	}

	void Drawer::check_attachements(const BasicFrameBuffer* framebuffer_ot_target)
	{
		uint att_count = framebuffer_ot_target->get_attachements_types().size();
		uint self_att_count = this->get_fragment_outs().size();

		if(att_count != self_att_count)
		{
			err("Try to draw in a frame buffer that has " + TERM::RED + to_string(att_count) + ERR_COLOR
			 + " attachements while the drawer has a fragment shader that has " + TERM::RED + to_string(self_att_count) + ERR_COLOR + " out parameters");
		}
	}

	void Drawer::set_all_params()
	{
		global_drawing::depth(_draw_params.depth);
		global_drawing::culling(_draw_params.culling);
		global_drawing::blending(_draw_params.blending);
		global_drawing::depth_mask(_draw_params.depth_mask);
		global_drawing::stencil_testing(_draw_params.stencil_params.enable);
		global_drawing::depth_function(_draw_params.depth_func);

		if(_draw_params.blending)
		{
			global_drawing::set_blend_function(_draw_params.blend_sfactor, _draw_params.blend_dfactor);
			global_drawing::set_blend_equation(_draw_params.blend_func_eq);
		}
		if(_draw_params.stencil_params.enable)
		{
			global_drawing::stencil_op(_draw_params.stencil_params.op_sfail,
							     	   _draw_params.stencil_params.op_dpfail,
							     	   _draw_params.stencil_params.op_dppass);
			global_drawing::stencil_function(_draw_params.stencil_params.function,
											 _draw_params.stencil_params.func_ref,
											 _draw_params.stencil_params.func_mask);
		}
	}
};

