#include "depth_buffer_multisample.hpp"
#include "window.hpp"

using namespace glm;

namespace ssl
{
	DepthBufferMultisample::DepthBufferMultisample(const glm::uvec2& size)
		: Texture(uvec3(size.x, size.y, 0.0), GL_NEAREST, GL_CLAMP_TO_BORDER)
	{
		this->initialize(NULL);
	}

	DepthBufferMultisample::DepthBufferMultisample(const glm::uvec3&  dimension_size)
		: DepthBufferMultisample(uvec2(dimension_size.x, dimension_size.y))
	{

	}

	GLenum DepthBufferMultisample::get_gl_texture_type() const
	{
		return GL_TEXTURE_2D_MULTISAMPLE;
	}

	void DepthBufferMultisample::tex_image_if_bound(const glm::uvec3& dimension_size, void* data)   const
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY, "DepthBufferMultisample texture created, id: " + std::to_string(this->get_ID())
							+ ", dimension: " + std::to_string(dimension_size.x) + "x" + std::to_string(dimension_size.y));
		}
		#endif
		
		GL(glTexImage2DMultisample(this->get_gl_texture_type(),
			 			ANTIALIASING,
			 			GL_DEPTH_COMPONENT,
			 			dimension_size.x,
			 			dimension_size.y,
			 			GL_TRUE));
	}

	void DepthBufferMultisample::change_data_if_bound(const glm::uvec3& dimension_size, void* data)
	{
		err("TODO: " + SSL_FILE_AND_LINE);
	}

	void DepthBufferMultisample::definitive_resize_GPU(const glm::uvec3& new_dimension_size)        const
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY, "DepthBufferMultisample resize, id: " + std::to_string(this->get_ID())
							+ ", dimensions: " + std::to_string(new_dimension_size.x) + "x" + std::to_string(new_dimension_size.y));
		}
		#endif

		GL(glTextureStorage2DMultisample(this->get_ID(), ANTIALIASING, GL_DEPTH_COMPONENT, new_dimension_size.x, new_dimension_size.y, GL_TRUE));
	}

	attachementType DepthBufferMultisample::get_attachement_type() const
	{
		return DEPTH_ATTACHMENT;
	}

	glm::uvec2 DepthBufferMultisample::size() const
	{
		return glm::vec2(this->get_dimension_size().x, this->get_dimension_size().y);
	}

	bool DepthBufferMultisample::mipmaps_enable() const
	{
		return false;
	}

	void DepthBufferMultisample::attach_on_framebuffer_if_bound(uint attachement_index) const
	{
		GL(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, this->get_gl_texture_type(), this->get_ID(), 0));
	}

	bool DepthBufferMultisample::wrap_enable()  const 
	{
		return false;
	}

};

