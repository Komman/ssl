#include "basic_camera.hpp"

namespace ssl
{
	BasicCamera::BasicCamera(const glm::vec3& initial_position,
					float min_view_distance,
					float max_view_distance,
					float view_field)
		
		: View(initial_position,
		 		min_view_distance,
		 		max_view_distance,
		 		view_field) 
	{

	}
};

