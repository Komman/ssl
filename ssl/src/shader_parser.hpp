#ifndef _SHADER_PARSER_HPP_
#define _SHADER_PARSER_HPP_

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <map>

#include "utils.hpp"
#include "basic_shader_parser.hpp"

namespace ssl
{
	/*
		Parse a shader that is already preprocessored

		DO NOT SUPPORT VARIABLE DECLARATION LIKE:
		
			int x=5; int y=8;  // double declaration on one line
		OR
			int 
			x=5;               // type declaration on a different line

		BUT SUPPORT LIKE THIS: 

			int x[]={
				...
				...
			};

	*/

	class ShaderParser : public BasicShaderParser
	{

		/*
			PARSING FORMAT:
			
			-To use uniform variable:               uniform::name
			-To use uniform array variable:         uniform::name[indice]
			-To use a variable that has not
			been declared in a function core:		local::name
			-To use a function:                     local::function_name(args...)
			-To use a instance variable in
			a vertex shader input:                  in::instance name
		*/


	public:
		virtual ~ShaderParser();

		ShaderParser(const std::string& source_file_path);

		// Get a parsed content of the function
		virtual functionInfo              get_function(const std::string& function_name) const;
		// Get the lines of the variable/MACRO declaration
		virtual std::vector<std::string>  get_var_dec(const std::string& variable_name) const;

		std::string get_file_name() const;

		// Free memory, you will have to re-open the file
		virtual void free();
		
		// Returns is the shader file is open
		bool is_sourced() const;
		
	private:
		// Path of the shader that includes all the others
		void open(const std::string& global_shader_file_path);
		


		functionInfo             parse_function(const std::string& function_name) const;
		std::vector<std::string> harvest_variable_declaration(const std::string& variable_name) const;
		void check_file_open() const;

		// Make advance the file to the first line that contains the string and returns this line
		std::string catch_var_header_str(std::ifstream& file, const std::string& str) const;
		std::string catch_func_header_str(std::ifstream& file, const std::string& str) const;
		std::string catch_func_dec(std::ifstream& file, const std::string& function_name) const;
		int array_count_from_arg(std::string arg) const;

		//returns the number of character of the uniform::UNIFORM_NAME
		unsigned int push_uniform(const std::string& line, unsigned int i, functionInfo& infos) const;
		//returns the number of character of the local::LOCAL_NAME
		unsigned int push_local(const std::string& line, unsigned int i, functionInfo& infos) const;

		void fill_head_fun_infos(functionInfo& infos, const std::vector<std::string>& splited_dec) const;
		void fill_func_core_infos(functionInfo& infos, std::ifstream& file) const;

		static constexpr    char        CLOSE_BRACKET_ARRAY_DECLARATION = ']'; 
		static inline const std::string NOT_CATCHED  = "##SSL_LINE_NOT_FOUND"; 

		std::string           _file_name;
		mutable std::ifstream _file;
		mutable std::map<std::string, functionInfo>  _parsed_cache;
		mutable std::map<std::string, std::vector<std::string>>  _var_dec_cache;
	};
};

#endif //_SHADER_PARSER_HPP_
