#include "texture.hpp"

using namespace glm;

#define GENERATE_TEXTURE_GETTERS(type, internal_format, samplerletter)\
	template<>\
	GLint Texture<type>::get_GL_internal_format()\
	{\
		return internal_format;\
	}\

#define INTEGER_PARAMETERS(type)\
	template<>\
	void Texture<type>::set_params(GLint minmag_filter, GLint wrap)\
	{\
		if(this->mipmaps_enable())\
		{\
			GL(glTexParameteri(this->get_gl_texture_type(), GL_TEXTURE_MIN_FILTER, GL_NEAREST));\
			GL(glTexParameteri(this->get_gl_texture_type(), GL_TEXTURE_MAG_FILTER, GL_NEAREST));\
		}\
		if(this->wrap_enable())\
		{\
			glTexParameteri(this->get_gl_texture_type(), GL_TEXTURE_WRAP_S, wrap);\
	        glTexParameteri(this->get_gl_texture_type(), GL_TEXTURE_WRAP_T, wrap);\
	    }\
	}\
	template<>\
	void Texture<type>::generate_mipmaps() {}

#define GENERATE_TEXTURE_FORMAT_VALID(type, valid_format)\
	template<>\
	GLint Texture<type>::get_GL_valid_format()\
	{\
		return valid_format;\
	}


#define GENERATE_TEXTURE_DATA_TYPE(type, valid_format)\
	template<>\
	GLenum Texture<type>::get_GL_data_type()\
	{\
		return valid_format;\
	}

namespace ssl
{
	GENERATE_TEXTURE_GETTERS(float, GL_R32F, "")
	GENERATE_TEXTURE_GETTERS(vec2, GL_RG32F, "")
	GENERATE_TEXTURE_GETTERS(vec3, GL_RGB32F, "")
	GENERATE_TEXTURE_GETTERS(vec4, GL_RGBA32F, "")

	GENERATE_TEXTURE_GETTERS(int, GL_R32I, "i")
	GENERATE_TEXTURE_GETTERS(ivec2, GL_RG32I, "i")
	GENERATE_TEXTURE_GETTERS(ivec3, GL_RGB32I, "i")
	GENERATE_TEXTURE_GETTERS(ivec4, GL_RGBA32I, "i")

	GENERATE_TEXTURE_GETTERS(unsigned int, GL_R32UI, "u")
	GENERATE_TEXTURE_GETTERS(uvec2, GL_RG32UI, "u")
	GENERATE_TEXTURE_GETTERS(uvec3, GL_RGB32UI, "u")
	GENERATE_TEXTURE_GETTERS(uvec4, GL_RGBA32UI, "u")

	INTEGER_PARAMETERS(int)
	INTEGER_PARAMETERS(ivec2)
	INTEGER_PARAMETERS(ivec3)
	INTEGER_PARAMETERS(ivec4)
	INTEGER_PARAMETERS(unsigned int)
	INTEGER_PARAMETERS(uvec2)
	INTEGER_PARAMETERS(uvec3)
	INTEGER_PARAMETERS(uvec4)

	GENERATE_TEXTURE_FORMAT_VALID(float, GL_RED)
	GENERATE_TEXTURE_FORMAT_VALID(vec2, GL_RG)
	GENERATE_TEXTURE_FORMAT_VALID(vec3, GL_RGB)
	GENERATE_TEXTURE_FORMAT_VALID(vec4, GL_RGBA)

	GENERATE_TEXTURE_FORMAT_VALID(int, GL_RED_INTEGER)
	GENERATE_TEXTURE_FORMAT_VALID(ivec2, GL_RG_INTEGER)
	GENERATE_TEXTURE_FORMAT_VALID(ivec3, GL_RGB_INTEGER)
	GENERATE_TEXTURE_FORMAT_VALID(ivec4, GL_RGBA_INTEGER)

	GENERATE_TEXTURE_FORMAT_VALID(unsigned int, GL_RED_INTEGER)
	GENERATE_TEXTURE_FORMAT_VALID(uvec2, GL_RG_INTEGER)
	GENERATE_TEXTURE_FORMAT_VALID(uvec3, GL_RGB_INTEGER)
	GENERATE_TEXTURE_FORMAT_VALID(uvec4, GL_RGBA_INTEGER)


	GENERATE_TEXTURE_DATA_TYPE(float, GL_FLOAT)
	GENERATE_TEXTURE_DATA_TYPE(vec2,  GL_FLOAT)
	GENERATE_TEXTURE_DATA_TYPE(vec3,  GL_FLOAT)
	GENERATE_TEXTURE_DATA_TYPE(vec4,  GL_FLOAT)

	GENERATE_TEXTURE_DATA_TYPE(int,   GL_INT)
	GENERATE_TEXTURE_DATA_TYPE(ivec2, GL_INT)
	GENERATE_TEXTURE_DATA_TYPE(ivec3, GL_INT)
	GENERATE_TEXTURE_DATA_TYPE(ivec4, GL_INT)

	GENERATE_TEXTURE_DATA_TYPE(unsigned int,  GL_UNSIGNED_INT)
	GENERATE_TEXTURE_DATA_TYPE(uvec2, GL_UNSIGNED_INT)
	GENERATE_TEXTURE_DATA_TYPE(uvec3, GL_UNSIGNED_INT)
	GENERATE_TEXTURE_DATA_TYPE(uvec4, GL_UNSIGNED_INT)

	
};
