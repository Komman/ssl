#ifndef _UNIFORM_CONTROLLER_HPP_
#define _UNIFORM_CONTROLLER_HPP_

#include "basic_uniform_controller.hpp"
#include "debug.hpp"

namespace ssl
{
	template<typename Type>
	class UniformController : public BasicUniformController
	{
	public:
		UniformController(const std::string name,
						  GLuint shader_to_link);
		
		void activate_before_draw() const;

	protected:
		void send_typed_data_to_GPU(void* data, int array_count) const;
	};
};

namespace ssl
{
	template<typename Type>
	UniformController<Type>::UniformController(const std::string name,
						 					   GLuint shader_to_link)
		: BasicUniformController(name, shader_to_link)
	{
		this->up_to_date(false);
	}

	template<typename Type>
	void UniformController<Type>::activate_before_draw() const
	{
		
	}
};

#endif //_UNIFORM_CONTROLLER_HPP_
