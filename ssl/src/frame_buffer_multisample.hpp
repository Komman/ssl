#ifndef _FRAME_BUFFER_MULTISAMPLE_HPP_
#define _FRAME_BUFFER_MULTISAMPLE_HPP_

#include "drawable_frame_buffer.hpp"
#include "uniform_texture_multisample.hpp"
#include "frame_buffer.hpp"
#include "depth_buffer_multisample.hpp"

namespace ssl
{
	template<typename... RenderTypes>
	class FrameBufferMultisample : public DrawableFrameBuffer<RenderTypes...>
	{
		public:
			FrameBufferMultisample(const UniformTextureMultisample<RenderTypes> * ... uniform_textures);
			FrameBufferMultisample(const DepthBufferMultisample* depth_buffer, const UniformTextureMultisample<RenderTypes> * ... uniform_textures);
			FrameBufferMultisample(const DepthStencilBufferMultisample* depth_buffer, const UniformTextureMultisample<RenderTypes> * ... uniform_textures);

			void draw_content_in_framebuffer(const FrameBuffer<RenderTypes...>& target, bool copy_depth, bool copy_stencil);

		protected:
			//FrameBufferMultisample(){}
			FrameBufferMultisample(const FrameBufferMultisample& f){}
			FrameBufferMultisample& operator=(const FrameBufferMultisample&){}
			FrameBufferMultisample& operator=(FrameBufferMultisample&& f){}

		private:
	};
};

namespace ssl
{
	template<typename... RenderTypes>
	FrameBufferMultisample<RenderTypes...>::FrameBufferMultisample(const UniformTextureMultisample<RenderTypes> * ... uniform_textures)
		: DrawableFrameBuffer<RenderTypes...>(uniform_textures...)
	{

	}
	template<typename... RenderTypes>
	FrameBufferMultisample<RenderTypes...>::FrameBufferMultisample(const DepthBufferMultisample* depth_buffer, const UniformTextureMultisample<RenderTypes> * ... uniform_textures)
		: DrawableFrameBuffer<RenderTypes...>(depth_buffer, uniform_textures...)
	{

	}
	template<typename... RenderTypes>
	FrameBufferMultisample<RenderTypes...>::FrameBufferMultisample(const DepthStencilBufferMultisample* depth_buffer, const UniformTextureMultisample<RenderTypes> * ... uniform_textures)
		: DrawableFrameBuffer<RenderTypes...>(depth_buffer, uniform_textures...)
	{

	}

	template<typename... RenderTypes>
	void FrameBufferMultisample<RenderTypes...>::draw_content_in_framebuffer(const FrameBuffer<RenderTypes...>& target, bool copy_depth, bool copy_stencil)
	{
		#ifdef SSL_DEBUG
		if(this->frame_size() != target.frame_size())
			err("FrameBufferMultisample::draw_content_in_framebuffer(target) : the sizes of the two framebuffers must be the same for the draw");
		#endif

		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_DRAW))
		{
			ssl::prints_msg(SSL_PRINTS_DRAW, "FrameBufferMultisample N: " 
							+ TERM::WHITE + std::to_string(this->get_ID())  + PRINTS_COLOR
							+ " blit in FrameBuffer N: " 
							+ TERM::WHITE + std::to_string(target.get_ID()) + PRINTS_COLOR
							+ ", dimensions: " + std::to_string(this->frame_size().x) + "x" + std::to_string(this->frame_size().y));
		}
		#endif

		GL(glBindFramebuffer(GL_READ_FRAMEBUFFER, this->get_ID()));
	    GL(glBindFramebuffer(GL_DRAW_FRAMEBUFFER, target.get_ID()));

	    GLbitfield fields_copied = GL_COLOR_BUFFER_BIT;

	    if(copy_depth)
	    {
	    	fields_copied |= GL_DEPTH_BUFFER_BIT;
	    }
	    if(copy_stencil)
	    {
	    	fields_copied |= GL_STENCIL_BUFFER_BIT;
	    }

	    GL(glBlitFramebuffer(0, 0, this->frame_size().x, this->frame_size().y, 0, 0, target.frame_size().x, target.frame_size().y, fields_copied, GL_NEAREST));
	}

};


#endif //_FRAME_BUFFER_MULTISAMPLE_HPP_
