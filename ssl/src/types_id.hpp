#ifndef _TYPES_ID_HPP_
#define _TYPES_ID_HPP_

#include <string>
#include "debug.hpp"

namespace ssl
{
	//TODO: matIxJ

	enum types_id{
		SSL_NO_TYPE = 0,
		SSL_vec4,
		SSL_vec3,
		SSL_vec2,
		SSL_float,
		SSL_mat4,
		SSL_mat3,
		SSL_mat2,

		SSL_dvec4,
		SSL_dvec3,
		SSL_dvec2,
		SSL_double,
		SSL_dmat4,
		SSL_dmat3,
		SSL_dmat2,

		SSL_ivec4,
		SSL_ivec3,
		SSL_ivec2,
		SSL_int,

		SSL_uvec4,
		SSL_uvec3,
		SSL_uvec2,
		SSL_uint,

		SSL_bvec4,
		SSL_bvec3,
		SSL_bvec2,
		SSL_bool,

		SSL_TYPES_ID_COUNT
	};

	template<typename Type>
	types_id get_type_id();

	types_id get_type_id(const std::string& str_type);
	std::string get_str_type_id(types_id type);
};



namespace ssl
{

};

#endif //_TYPES_ID_HPP_
