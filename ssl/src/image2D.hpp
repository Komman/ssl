#ifndef _IMAGE2D_HPP_
#define _IMAGE2D_HPP_

namespace ssl
{
	template<typename Type>
	class Image2D : public Texture2D<Type>
	{
	public:
		Image2D(const glm::uvec2& size,
				void*              data          = NULL,
				GLint              minmag_filter = DEFAULT::MINMAG_FILTER,
				GLint              wrap          = DEFAULT::TEXTURE_WRAP);


		Image2D(const glm::uvec3&  dimension_size,
				void* data,
				GLint minmag_filter,
				GLint wrap);

		void bind() const override;

	protected:

	private:

	};
};


namespace ssl
{
	template<typename Type>
	Image2D<Type>::Image2D(const glm::uvec2&  size,
						   void* data,
						   GLint minmag_filter,
						   GLint wrap)
		: Texture2D<Type>(size, data, minmag_filter, wrap)
	{

	}

	template<typename Type>
	Image2D<Type>::Image2D(const glm::uvec3&  dimension_size,
						   void* data,
						   GLint minmag_filter,
						   GLint wrap)
		: Texture2D<Type>(dimension_size, data, minmag_filter, wrap)
	{

	}

	template<typename Type>
	void Image2D<Type>::bind() const
	{
		GL(glBindImageTexture(0, this->get_ID(), 0, GL_FALSE, 0, GL_READ_WRITE, this->get_GL_internal_format()));
	}
};

#endif //_IMAGE2D_HPP_
