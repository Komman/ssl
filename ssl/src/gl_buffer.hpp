#ifndef _GL_BUFFER_HPP_
#define _GL_BUFFER_HPP_

#include "basic_buffer.hpp"
#include "buffer_data_type.hpp"
#include "debug.hpp"

#include <GL/glew.h>
#include <vector>


namespace ssl
{
	enum glBufferReservationStrategy {GLBUFFER_OPTIMIZE_MEMORY=0, GLBUFFER_OPTIMIZE_FLOW};

	namespace DEFAULT
	{
		constexpr draw_type                   MEMORY_DRAW_TYPE         = STATIC_DRAW;
		constexpr glBufferReservationStrategy GLBUFFER_GPU_RESERVATION = GLBUFFER_OPTIMIZE_FLOW;   
	};

	template<typename Type>
	class GLBuffer : public BasicBuffer
	{
		/*
			DO NOT SUPPORT ARRAY OF TYPES
			SUPPORT OF DOUBLE PRECISION / INTEGER NOT TESTED
		*/

	public:
		GLBuffer(const std::vector<Type>& initial_data,
				 GLenum gl_buffer_type,
				 draw_type type);
		virtual ~GLBuffer() {}

		unsigned int target_vertex_location(unsigned int location) const override;
		unsigned int size() const override;

		void copy(const GLBuffer& source);
		void reserve_CPU(unsigned int element_count);

		void change_subvalue(unsigned int indice, const Type& new_value);
		const Type& get_subvalue(unsigned int indice) const;

		const Type& operator[](unsigned int index) const ;
		
		void change_value(const std::vector<Type>& new_value);
		const std::vector<Type>& get_value() const;

		void add_value(const std::vector<Type>& new_values);
		// Returns the index of the value added
		unsigned int add_value(const Type& new_value);
		void pop_value();
		void clear();

		types_id get_self_type_id() const override;

		static unsigned int vertex_location_size();

	protected:
		
		GLBuffer& operator=(const GLBuffer& b); 
		GLBuffer(const GLBuffer& b);
		GLBuffer& operator=(GLBuffer&& b); 
		GLBuffer(GLBuffer&& b);
		
		void bind_buffer() const override;
		void update_GPU_data_if_bound() const override;

		static unsigned int vertexAttribPointer(GLuint location, GLint size);

	private:
		using vectype = typename buffer_data_type<Type>::type;

		void create_data(draw_type type) const;
		void update_extrm_data_indices(unsigned int index);

		GLenum            _gl_buffer_type;
		draw_type         _drawing_type;
		mutable uint      _GPU_buffer_size;
		mutable std::vector<Type> _data;

		/* 
			Stores the range of indices of _data that has been changed
			In case of no data changed, _changed_data_extrm_indices = (-1, -1)

			exemple : If data is changed on indices 6,12,5 and 9, then _changed_data_extrm_indices = (6,13) 
					  (13 because it is 12 + 1 (the range doesn't include last element))
		*/
		mutable glm::ivec2 _changed_data_extrm_indices;
	};
};



namespace ssl
{
	template<typename Type>
	GLBuffer<Type>::GLBuffer(const std::vector<Type>& initial_data,
								   GLenum gl_buffer_type,
							 	   draw_type type)
		:	
			BasicBuffer(),
			_gl_buffer_type(gl_buffer_type),
			_drawing_type(type),
			_GPU_buffer_size(initial_data.size()),
			_data(initial_data),
			_changed_data_extrm_indices(-1,-1)
	{
		this->bind_buffer();
		create_data(type);
	}

	template<typename Type>
	void GLBuffer<Type>::create_data(draw_type type) const
	{
		if constexpr (DEFAULT::GLBUFFER_GPU_RESERVATION == GLBUFFER_OPTIMIZE_FLOW)
		{
			#ifdef SSL_PRINTS
			if(ssl::are_prints_enable(SSL_PRINTS_MEMORY))
			{
				ssl::prints_msg(SSL_PRINTS_MEMORY, "Creating/Recreating storage of buffer (id:" + std::to_string(this->get_ID()) + ") with data of size " + std::to_string(_data.capacity()));
			}
			#endif

			if(_data.capacity() > 2*_data.size())
			{
				_data.shrink_to_fit();
				_data.reserve(_data.size()*2/3);
			}
			
			GL(glBufferData(_gl_buffer_type, sizeof(Type)*_data.capacity(), (void*)_data.data(), type));
			_GPU_buffer_size = _data.capacity();
		}
		if constexpr (DEFAULT::GLBUFFER_GPU_RESERVATION == GLBUFFER_OPTIMIZE_MEMORY)
		{
			#ifdef SSL_PRINTS
			if(ssl::are_prints_enable(SSL_PRINTS_MEMORY))
			{
				ssl::prints_msg(SSL_PRINTS_MEMORY, "Creating/Recreating storage of buffer (id:" + std::to_string(this->get_ID()) + ") with data of size " + std::to_string(_data.size()));
			}
			#endif

			GL(glBufferData(_gl_buffer_type, sizeof(Type)*_data.size(), (void*)_data.data(), type));
			_GPU_buffer_size = _data.size();
		}
	}


	template<typename Type>
	void GLBuffer<Type>::update_extrm_data_indices(unsigned int index)
	{
		if(_changed_data_extrm_indices.x == -1)
		{
			#ifdef SSL_DEBUG
			if(_changed_data_extrm_indices.y != -1)
			{
				err("GLBuffer<Type>::update_extrm_data_indices(): _changed_data_extrm_indices.y != -1 while _changed_data_extrm_indices.x == -1");
			}
			#endif

			_changed_data_extrm_indices = glm::ivec2(index, index + 1);
		}
		else
		{
			_changed_data_extrm_indices.x = std::min((unsigned int)(_changed_data_extrm_indices.x), index);
			_changed_data_extrm_indices.y = std::max((unsigned int)(_changed_data_extrm_indices.y), index + 1);
		}
	}

	template<typename Type>
	void GLBuffer<Type>::bind_buffer() const
	{
		GL(glBindBuffer(_gl_buffer_type, this->get_ID()));
	}

	template<typename Type>
	void GLBuffer<Type>::update_GPU_data_if_bound() const
	{
		if(_GPU_buffer_size < _data.size()  ||  _GPU_buffer_size > 2*_data.size())
		{
			#ifdef SSL_PRINTS
			if(_GPU_buffer_size > 2*_data.size() && ssl::are_prints_enable(SSL_PRINTS_MEMORY))
			{
				prints_msg(SSL_PRINTS_MEMORY, "Recreating buffer id:" + std::to_string(this->get_ID()) + " for saving 2x more GPU memory (from size="
					+ std::to_string(_GPU_buffer_size) + " to size=" + std::to_string(_data.size()) + ")");
			}
			#endif
			
			this->create_data(_drawing_type);
		}
		else
		{
			glm::ivec2 uprange;

			if(_changed_data_extrm_indices.x >=0 && _changed_data_extrm_indices.y >=0)
			{
				uprange = glm::min(_changed_data_extrm_indices, glm::ivec2(_data.size(), _data.size()));				
			}
			else
			{
				uprange = glm::ivec2(0, _data.size());
			}
			
			// std::cout<<"("<<uprange.x<<", "<<uprange.y<<") / "<<_data.size()<<std::endl;
			
			unsigned int start_data = uprange.x;

			#ifdef SSL_PRINTS
			if(ssl::are_prints_enable(SSL_PRINTS_MEMORY_FLOW))
			{
				ssl::prints_msg(SSL_PRINTS_MEMORY_FLOW, "Filling storage (subdata) of buffer (id:" + std::to_string(this->get_ID()) 
					+ ") with data of size " + std::to_string(uprange.y-uprange.x)
					+ "  on index range: " + std::to_string(uprange.x) + "-" + std::to_string(uprange.y));
			}
			#endif

			uprange *= (int)(sizeof(Type));

			GL(glBufferSubData(_gl_buffer_type, uprange.x, uprange.y-uprange.x, _data.data() + start_data));
		}		

		_changed_data_extrm_indices = glm::ivec2(-1,-1);
	}
	
	template<typename Type>
	uint GLBuffer<Type>::vertex_location_size()
	{
		return 1;
	}
	
	template<typename Type>
	uint GLBuffer<Type>::target_vertex_location(uint location) const
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_PARAMS_SETTING))
		{
			ssl::prints_msg(SSL_PRINTS_PARAMS_SETTING, "glVertexAttribPointer( " + std::to_string(location) + " )");
		}
		#endif
		
		return vertexAttribPointer(location, sizeof(Type)/sizeof(vectype));
	}

	template<typename Type>
	void GLBuffer<Type>::copy(const GLBuffer& source)
	{
		this->change_value(source._data);
	}

	template<typename Type>
	void GLBuffer<Type>::change_subvalue(uint indice, const Type& new_value)
	{
		#ifdef SSL_DEBUG
		if(indice >= _data.size())
		{
			err("GLBuffer<Type>::change_subdata(uint indice, const Type& new_value): indice out of range: indice="
				+ std::to_string(indice) + " and _data.size()=" + std::to_string(_data.size()));
		}
		#endif
		_data[indice]=new_value;
		this->data_has_changed();
		this->update_extrm_data_indices(indice);
	}

	template<typename Type>
	const Type& GLBuffer<Type>::get_subvalue(uint indice) const
	{
		#ifdef SSL_DEBUG
		if(indice >= _data.size())
		{
			err("GLBuffer<Type>::change_subdata(uint indice, const Type& new_value): indice out of range: indice="
				+ std::to_string(indice) + " and _data.size()=" + std::to_string(_data.size()));
		}
		#endif
		return _data[indice];
	}


	template<typename Type>
	void GLBuffer<Type>::change_value(const std::vector<Type>& new_value)
	{
		_data=new_value;
		_changed_data_extrm_indices.x = 0;
		_changed_data_extrm_indices.y = (int)_data.size();
		this->data_has_changed();
	}

	// template<typename Type>
	// GLBuffer<Type>& GLBuffer<Type>::operator=(const std::vector<Type>& new_value)
	// {
	// 	this->change_value(new_value);
	// 	return *this;
	// }

	template<typename Type>
	const std::vector<Type>& GLBuffer<Type>::get_value() const
	{
		return _data;
	}

	template<typename Type>
	unsigned int GLBuffer<Type>::add_value(const Type& value)
	{
		this->update_extrm_data_indices(_data.size());

		unsigned int ret = _data.size();
		_data.push_back(value);

		this->data_has_changed();

		return ret;
	}

	template<typename Type>
	void GLBuffer<Type>::add_value(const std::vector<Type>& new_values)
	{
		if(new_values.size() > 0)
		{
			this->update_extrm_data_indices(_data.size());
			for(const auto& v : new_values)
			{
				_data.push_back(v);
			}
			this->update_extrm_data_indices((int)_data.size() - 1);
			this->data_has_changed();
		}
	}

	template<typename Type>
	void GLBuffer<Type>::pop_value()
	{
		#ifdef SSL_DEBUG
		if(_data.size() == 0)
		{
			err("Try to pop_value on a buffer that is already empty");
		}
		#endif

		_data.pop_back();
		this->data_has_changed();
	}

	template<typename Type>
	void GLBuffer<Type>::clear()
	{
		_data.clear();
		this->data_has_changed();
	}

	template<typename Type>
	uint GLBuffer<Type>::size() const
	{
		return _data.size();
	}

	template<typename Type>
	types_id GLBuffer<Type>::get_self_type_id() const
	{
		return ssl::get_type_id<Type>();
	}

	template<typename Type>
	const Type& GLBuffer<Type>::operator[](unsigned int index) const
	{
		return this->get_subvalue(index);
	}
	
	template<typename Type>
	void GLBuffer<Type>::reserve_CPU(unsigned int element_count)
	{
		_data.reserve(element_count);
	}

};

#endif //_GL_BUFFER_HPP_
