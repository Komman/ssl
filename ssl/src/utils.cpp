#include "utils.hpp"
#include <stdlib.h>

#include <chrono>

using namespace std;
using namespace std::chrono;

namespace utils
{
	bool is_in_string(char c, const string& str)
	{
		for(char cc : str)
		{
			if(cc == c)
				return true;
		}
		return false;
	}

	std::vector<std::string> split_novoid(const std::string& str, const std::string& delimitors)
	{
		string start = "";
		start.reserve(str.size());
	    std::vector<std::string> terms(1, start);

	    for(char c : str)
	    {
	    	if(is_in_string(c, delimitors))
	    	{
	    		if(terms[terms.size()-1].size() != 0)
	    		{
	    			terms.push_back(start);
	    		}
	    	}
	    	else
	    	{
	    		terms[terms.size()-1].push_back(c);
	    	}
	    }

	    if(terms[terms.size()-1].size()==0)
	    	terms.pop_back();

	    return terms;
	}

	unsigned int extract_directory_index(const std::string& str)
	{
		if(str.size() == 0)
		{
			return 0;
		}

		unsigned int end_directory;
		for(end_directory = str.size()-1; end_directory>0; end_directory--)
		{
			if(str[end_directory] == '/')
				break;
		}
		return end_directory;
	}

	std::string extract_directory(const std::string& str)
	{
		unsigned int end_directory = extract_directory_index(str);

		if(end_directory == 0)
		{
			return ".";
		}

		return str.substr(0, end_directory);
	}

	std::pair<std::string, std::string> extract_directory_and_file(const std::string& str)
	{
		std::pair<std::string, std::string> ret;	

		unsigned int end_directory = extract_directory_index(str);

		if(end_directory == 0)
		{
			ret.first  = "";
			ret.second = str;
		}
		else
		{
			ret.first  = str.substr(0, end_directory+1);
			ret.second = str.substr(end_directory+1);
		}

		return ret;
	}


	uint64_t get_miliseconds()
	{
		return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
	}
	uint64_t get_nanoseconds()
	{
		return duration_cast<nanoseconds>(system_clock::now().time_since_epoch()).count();
	}
	
	float random_float(float minimum, float maximum, int precision)
	{
		int rd = rand() % precision;
		float rd_coeff = float(rd)/float(precision);
		rd_coeff*=(maximum-minimum);
		rd_coeff+=minimum;
		return rd_coeff;
	}
};

