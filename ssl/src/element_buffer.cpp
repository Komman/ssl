#include "element_buffer.hpp"

#include <GL/glew.h>

namespace ssl
{
	ElementBuffer::ElementBuffer(const std::vector<uint>& initial_data,
					 			 draw_type type)
		: GLBuffer<uint>(initial_data, GL_ELEMENT_ARRAY_BUFFER, type)
	{

	}

	uint ElementBuffer::indices_count() const
	{
		return this->get_value().size();
	}

	void ElementBuffer::pop_triangle()
	{
		this->pop_value();
		this->pop_value();
		this->pop_value();
	}

	void ElementBuffer::pop_rectangle()
	{
		this->pop_triangle();
		this->pop_triangle();
	}

	void ElementBuffer::pop_hexahedron()
	{
		this->pop_rectangle();
		this->pop_rectangle();
		this->pop_rectangle();
		this->pop_rectangle();
		this->pop_rectangle();
		this->pop_rectangle();
	}


	void ElementBuffer::add_triangle(const std::vector<unsigned int>& vertexs_indices, bool flip_face)
	{
		#ifdef SSL_DEBUG
		if(vertexs_indices.size()!=3)
		{
			err("ElementBuffer::add_triangle(): 3 indices are required in the vector of indices");
		}
		#endif

		this->add_triangle(vertexs_indices[0],
						   vertexs_indices[1],
						   vertexs_indices[2],
						   flip_face);
	}

	void ElementBuffer::add_triangle(uint index1, uint index2, uint index3, bool flip_face)
	{
		this->add_value(index1);

		if(!flip_face)
		{
			this->add_value(index2);
			this->add_value(index3);
		}
		else
		{
			this->add_value(index3);
			this->add_value(index2);
		}
	}

	void ElementBuffer::add_rectangle(const std::vector<unsigned int>& vertexs_indices, bool flip_face)
	{
		#ifdef SSL_DEBUG
		if(vertexs_indices.size()!=4)
		{
			err("ElementBuffer::add_rectangle(): 4 indices are required in the vector of indices");
		}
		#endif

		this->add_rectangle(vertexs_indices[0],
							vertexs_indices[1],
							vertexs_indices[2],
							vertexs_indices[3], 
							flip_face);
	}
	
	void ElementBuffer::add_rectangle(uint index1, uint index2, uint index3, uint index4, bool flip_face)
	{
		this->add_triangle(index1, index2, index3, flip_face);
		this->add_triangle(index1, index3, index4, flip_face);
	}

	void ElementBuffer::add_hexahedron(const std::vector<unsigned int>& vertexs_indices)
	{
		#ifdef SSL_DEBUG
		if(vertexs_indices.size()!=8)
		{
			err("ElementBuffer::add_hexahedron(): 8 indices are required in the vector of indices");
		}
		#endif
/*
		0 1 2 3
		7 6 5 4
		0 3 7 4
		1 5 6 2
		0 4 5 1
		2 6 7 3
*/
		this->add_rectangle(vertexs_indices[0], vertexs_indices[3], vertexs_indices[2], vertexs_indices[1]);
		this->add_rectangle(vertexs_indices[7], vertexs_indices[4], vertexs_indices[5], vertexs_indices[6]);
		this->add_rectangle(vertexs_indices[0], vertexs_indices[4], vertexs_indices[7], vertexs_indices[3]);
		this->add_rectangle(vertexs_indices[1], vertexs_indices[2], vertexs_indices[6], vertexs_indices[5]);
		this->add_rectangle(vertexs_indices[0], vertexs_indices[1], vertexs_indices[5], vertexs_indices[4]);
		this->add_rectangle(vertexs_indices[2], vertexs_indices[3], vertexs_indices[7], vertexs_indices[6]);
	}
};

