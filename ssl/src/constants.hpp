#ifndef _CONSTANTS_HPP_
#define _CONSTANTS_HPP_

namespace ssl
{
	constexpr float EPSILON = 0.0001;
};

#endif //_CONSTANTS_HPP_
