#include "autoprint_recorder.hpp"
#include "utils.hpp"
#include "debug.hpp"

#include <GL/glew.h>

namespace ssl
{

	AutoPrintRecorder AutoPrintRecorder::quick;
	AutoPrintRecorder AutoPrintRecorder::gl_quick("", true);

	AutoPrintRecorder::AutoPrintRecorder(const std::string& name, bool opengl_rec)
		: _name(name), 
		  _milisecs(0),
		  _rec_opengl(opengl_rec)
	{

	}

	void AutoPrintRecorder::use_gl(bool gl)
	{
		_rec_opengl = gl;
	}


	void AutoPrintRecorder::start()
	{
		if(_rec_opengl)
		{
			glFinish();
		}
		_milisecs = utils::get_nanoseconds();
	}

	void AutoPrintRecorder::stop()
	{
		if(_rec_opengl)
		{
			glFinish();
		}
		uint64_t time_spend = utils::get_nanoseconds() - _milisecs;
		float mstime = float(time_spend)/1000000.0;
		std::string print_color = "";

		if(mstime > 5.0f)
		{
			print_color = TERM::PURPLE;
		}
		if(mstime > 6.5f)
		{
			print_color = TERM::ORANGE;
		}
		if(mstime > 8.5f)
		{
			print_color = TERM::RED;
		}

		std::cout<<std::setprecision(2)<< std::fixed;
		std::cout<<_name<<": "<<print_color<<mstime<<" ms"<<TERM::NOCOL<<std::endl;
	}
};

