#ifndef _DEPTH_BUFFER_HPP_
#define _DEPTH_BUFFER_HPP_

#include "texture.hpp"

namespace ssl
{
	class DepthBuffer : public Texture<DepthAttachement>
	{
	public:
		DepthBuffer(const glm::uvec2& size);

		DepthBuffer(const glm::uvec3&  dimension_size);

		GLenum     get_gl_texture_type() const override;
		glm::uvec2 size()            const;

	protected:

		void change_data_if_bound(const glm::uvec3& dimension_size, void* data) override;

		void tex_image_if_bound(const glm::uvec3& dimension_size, void* data)   const override;
		bool mipmaps_enable()                                        const override;
		void definitive_resize_GPU(const glm::uvec3& new_dimension_size)        const override;
		void attach_on_framebuffer_if_bound(uint attachement_index)  const override;
		bool wrap_enable()                                           const override;
	
		attachementType get_attachement_type() const override;
	

	private:

	};
};

#endif //_DEPTH_BUFFER_HPP_
