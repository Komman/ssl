namespace ssl
{
	/*
		BASIC OBJECT MERGER
	*/

	template<typename DrawableObjectType, typename... DrawTypes>
	BasicObjectMerger<DrawableObjectType, DrawTypes...>::BasicObjectMerger(const std::vector<DrawableObjectType*> objects, typename mergedBufferDrawType<DrawTypes>::type... buffer_draw_types)
		: _array_buffers(buffer_draw_types...),
		  _buffers({})

	{
		this->init_all_array_buffers(std::make_index_sequence<sizeof...(DrawTypes)>{});

		for(auto obj : objects)
		{
			this->add_object_all_buffers(obj, std::make_index_sequence<sizeof...(DrawTypes)>{});
		}
	}

	template<typename DrawableObjectType, typename... DrawTypes>
	template<std::size_t... Is>
	void BasicObjectMerger<DrawableObjectType, DrawTypes...>::init_all_array_buffers(std::index_sequence<Is...>)
	{
		((this->init_array_buffer<Is>()), ...);
	}

	template<typename DrawableObjectType, typename... DrawTypes>
	template<std::size_t buffer_index>
	void BasicObjectMerger<DrawableObjectType, DrawTypes...>::init_array_buffer()
	{
		_buffers.push_back(&(std::get<buffer_index>(_array_buffers)));
	}

	template<typename DrawableObjectType, typename... DrawTypes>
	void BasicObjectMerger<DrawableObjectType, DrawTypes...>::check_object(DrawableObjectType* object) const
	{
		this->check_object_size(object);
		this->check_object_buffers_type(object);
	}

	template<typename DrawableObjectType, typename... DrawTypes>
	void BasicObjectMerger<DrawableObjectType, DrawTypes...>::check_object_buffers_type(DrawableObjectType* object) const
	{
		this->check_object_all_buffers(object, std::make_index_sequence<sizeof...(DrawTypes)>{});
	}

	template<typename DrawableObjectType, typename... DrawTypes>
	template<std::size_t... Is>
	void BasicObjectMerger<DrawableObjectType, DrawTypes...>::check_object_all_buffers(DrawableObjectType* object, std::index_sequence<Is...>) const
	{
		((this->check_object_array_buffer<typename std::tuple_element<Is, bufferTypes>::type, Is>(object)), ...); 
	}

	template<typename DrawableObjectType, typename... DrawTypes>
	template<typename BufferType, std::size_t buffer_index>
	void BasicObjectMerger<DrawableObjectType, DrawTypes...>::check_object_array_buffer(DrawableObjectType* object) const
	{
		auto buffer_typeid = object->get_buffers()[buffer_index]->get_self_type_id();
		if(buffer_typeid != get_type_id<BufferType>())
		{
			err("BasicObjectMerger: Try to add an object with the buffer " + TERM::BLUE + "index=" + std::to_string(buffer_index) + ERR_COLOR +" of the type "
				+ TERM::RED + get_str_type_id(buffer_typeid) + ERR_COLOR + " while the BasicObjectMerger has the type " + TERM::GREEN + get_str_type_id(get_type_id<BufferType>())
				+ ERR_COLOR + " as the array buffer of this index");
		}
	}


	template<typename DrawableObjectType, typename... DrawTypes>
	void BasicObjectMerger<DrawableObjectType, DrawTypes...>::check_object_size(DrawableObjectType* object) const
	{
		if(object->get_buffers().size() != sizeof...(DrawTypes))
		{
			err("BasicObjectMerger: Try to add an object that has " + TERM::RED + std::to_string(object->get_buffers().size()) + ERR_COLOR
				+ " drawing buffers while the BasicObjectMerger has " + TERM::GREEN + std::to_string(sizeof...(DrawTypes)) + ERR_COLOR + " drawing buffers");
		}
	}


	template<typename DrawableObjectType, typename... DrawTypes>
	unsigned int BasicObjectMerger<DrawableObjectType, DrawTypes...>::add_object(DrawableObjectType* object)
	{
		#ifdef SSL_DEBUG
		this->check_object(object);
		#endif

		unsigned int final_index = _objects.size();
		_objects.push_back(object);

		return final_index;
	}


	template<typename DrawableObjectType, typename... DrawTypes>
	template<std::size_t... Is>
	void BasicObjectMerger<DrawableObjectType, DrawTypes...>::add_object_all_buffers(DrawableObjectType* object, std::index_sequence<Is...>)
	{
		#ifdef SSL_DEBUG
		if(object->get_buffers().size() != sizeof...(DrawTypes))
		{
			err(std::string("Try to add an object to a BasicObjectMerger that has not the same buffers count: ")
				+ "\nBasicObjectMerger buffer count : " + TERM::GREEN + std::to_string(sizeof...(DrawTypes))  + ERR_COLOR +
				+ "\nDrawableObject buffer count : "    + TERM::RED + std::to_string(object->get_buffers().size()));
		}
		#endif

		((this->add_object_array_buffer<typename std::tuple_element<Is, bufferTypes>::type, Is>(object)), ...); 
	}

	template<typename DrawableObjectType, typename... DrawTypes>
	void BasicObjectMerger<DrawableObjectType, DrawTypes...>::check_all_object_buffer_size() const
	{
		for(auto o : _objects)
		{
			this->check_object_buffers_size(o);
		}
	}

	template<typename DrawableObjectType, typename... DrawTypes>
	void BasicObjectMerger<DrawableObjectType, DrawTypes...>::check_object_buffers_size(DrawableObjectType* object) const
	{
		int size = -1;
		for(const BasicBuffer* buffer : object->get_buffers())
		{
			if(size == -1)
			{
				size = buffer->size();
			}
			else
			{
				if(size != (int)buffer->size())
				{
					err(std::string("All array buffers of an object added to a BasicObjectMerger are not of the same size: ")
						+ "\none is of size " + TERM::RED + std::to_string(size) + ERR_COLOR + " and an other is of size "
						+ TERM::RED + std::to_string(buffer->size()));
				}
			}
		}
	}

	template<typename DrawableObjectType, typename... DrawTypes>
	template<typename BufferType, std::size_t buffer_index>
    void BasicObjectMerger<DrawableObjectType, DrawTypes...>::add_object_array_buffer(DrawableObjectType* object)
    {
    	ArrayBuffer<BufferType>* obj_buffer = (ArrayBuffer<BufferType>*)(object->get_buffers()[buffer_index]);
    	ArrayBuffer<BufferType>& buffer     = std::get<buffer_index>(_array_buffers);
    	for(const auto& value : obj_buffer->get_value())
    	{
    		buffer.add_value(value);
    	}
    }


	template<typename DrawableObjectType, typename... DrawTypes>
	void BasicObjectMerger<DrawableObjectType, DrawTypes...>::merge_objects()
	{
		std::apply([](auto&... buffer)
		{
			unsigned int reserval = 0;
			((reserval = buffer.size(),
			  buffer.change_value({}),
			  buffer.reserve_CPU(reserval)), ...);
		}, _array_buffers);

		#ifdef SSL_DEBUG
		this->check_all_object_buffer_size();
		#endif

		for(auto o : _objects)
		{
			#ifdef SSL_DEBUG
			this->check_object(o);
			#endif

			this->add_object_all_buffers(o, std::make_index_sequence<sizeof...(DrawTypes)>{});
		}

		this->merge_additionnal_operations();
	}


	template<typename DrawableObjectType, typename... DrawTypes>
	const std::vector<const BasicBuffer*>& BasicObjectMerger<DrawableObjectType, DrawTypes...>::get_buffers() const
	{
		return _buffers;
	}

	template<typename DrawableObjectType, typename... DrawTypes>
	const std::vector<DrawableObjectType*>& BasicObjectMerger<DrawableObjectType, DrawTypes...>::get_objects() const
	{
		return _objects;
	}


    /*
		OBJECT MERGER
    */

    template<typename... DrawTypes>
	ObjectMerger<DrawTypes...>::ObjectMerger(const std::vector<DrawableObject*> objects, typename mergedBufferDrawType<DrawTypes>::type... buffer_draw_types)
		: BasicObjectMerger<DrawableObject, DrawTypes...>(objects, buffer_draw_types...)
	{
		
	}


    /*
		ELEMENT OBJECT MERGER
    */

	template<typename... DrawTypes>
	ElementObjectMerger<DrawTypes...>::ElementObjectMerger(const std::vector<ElementDrawableObject*>         objects,
														   ssl::draw_type                                    elements_buffer_draw_type,
														   typename mergedBufferDrawType<DrawTypes>::type... buffer_draw_types)
		: BasicObjectMerger<ElementDrawableObject, DrawTypes...>(objects, buffer_draw_types...),
		  _elements({}, elements_buffer_draw_type)
	{

	}

	template<typename... DrawTypes>
	void ElementObjectMerger<DrawTypes...>::merge_additionnal_operations()
	{
		_elements.change_value({});

		unsigned int index = 0;
		for(const ElementDrawableObject* o : this->get_objects())
		{
			unsigned int buffer_size = 0;
			if(o->get_buffers().size() > 0)
			{
				buffer_size = o->get_buffers()[0]->size();
			}

			for(unsigned int element : o->get_elements().get_value())
			{
				_elements.add_value(index + element);
			}

			index += buffer_size;
		}
	}

	template<typename... DrawTypes>
	const ElementBuffer& ElementObjectMerger<DrawTypes...>::get_elements() const
	{
		return _elements;
	}

};