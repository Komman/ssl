#ifndef _UNIFORM_TEXTURE_MULTISAMPLE_HPP_
#define _UNIFORM_TEXTURE_MULTISAMPLE_HPP_

#include "uniform_texture.hpp"
#include "texture_multisample.hpp"

namespace ssl
{
	template<typename Type>
	class UniformTextureMultisample : public UniformTexture<Type, TextureMultisample<Type>>
	{
	public:
		UniformTextureMultisample(const std::string& name,
								  const glm::uvec2&  size);

		glm::uvec2 size();
	};
};


namespace ssl
{
	template<typename Type>
	UniformTextureMultisample<Type>::UniformTextureMultisample(const std::string& name,
								  							   const glm::uvec2&  size)
		: UniformTexture<Type, TextureMultisample<Type>>(name, glm::uvec3(size.x, size.y, 1), NULL)
	{

	}

	template<typename Type>
	glm::uvec2 UniformTextureMultisample<Type>::size()
	{
		return glm::uvec2(this->get_dimension_size().x, this->get_dimension_size().y);
	}
};

#endif //_UNIFORM_TEXTURE_MULTISAMPLE_HPP_
