#ifndef _DRAWABLE_FRAME_BUFFER_HPP_
#define _DRAWABLE_FRAME_BUFFER_HPP_


#include <GL/glew.h>

#include "basic_frame_buffer.hpp"
#include "texture.hpp"
#include "uniform_texture_2D.hpp"
#include "uniform_texture_multisample.hpp"
#include "debug.hpp"
#include "autoprint_recorder.hpp"
#include "depth_buffer.hpp"
#include "depth_buffer_multisample.hpp"
#include "depth_stencil_buffer.hpp"
#include "depth_stencil_buffer_multisample.hpp"

namespace ssl
{

	template<typename... RenderTypes>
	class DrawableFrameBuffer : public BasicFrameBuffer
	{
		public:
			virtual ~DrawableFrameBuffer();

			void bind() const;
			void unbind() const;

			const glm::uvec2& frame_size() const;

			unsigned int get_ID() const override;

			const std::vector<types_id>& get_attachements_types() const;

		protected:
			DrawableFrameBuffer(const UniformTexture2D<RenderTypes> * ... uniform_textures);
			DrawableFrameBuffer(const UniformTextureMultisample<RenderTypes> * ... uniform_textures);

			DrawableFrameBuffer(const DepthBuffer* depth_buffer, const UniformTexture2D<RenderTypes> * ... uniform_textures);
			DrawableFrameBuffer(const DepthStencilBuffer* depth_buffer, const UniformTexture2D<RenderTypes> * ... uniform_textures);
			DrawableFrameBuffer(const DepthBufferMultisample* depth_buffer, const UniformTextureMultisample<RenderTypes> * ... uniform_textures);
			DrawableFrameBuffer(const DepthStencilBufferMultisample* depth_buffer, const UniformTextureMultisample<RenderTypes> * ... uniform_textures);

			DrawableFrameBuffer(const DrawableFrameBuffer& f){}
			DrawableFrameBuffer& operator=(const DrawableFrameBuffer&){}
			DrawableFrameBuffer& operator=(DrawableFrameBuffer&& f){}

			glm::uvec2 check_sizes(const UniformTexture2D<RenderTypes> & ... uniform_textures);
			void attach_textures(const UniformTexture2D<RenderTypes> & ... uniform_textures);
			glm::uvec2 check_sizes(const UniformTextureMultisample<RenderTypes> & ... uniform_textures);
			void attach_textures(const UniformTextureMultisample<RenderTypes> & ... uniform_textures);

			void build_frame_buffer_storage(GLenum framebuffer_format, const glm::uvec2& size);
			void build_frame_buffer_multisample_storage(GLenum framebuffer_format, const glm::uvec2& size);

		private:

			void initialize(void (DrawableFrameBuffer::*build_frame_buffer_storage_function)(GLenum framebuffer_format, const glm::uvec2& size),
							const UniformTexture2D<RenderTypes> * ... uniform_textures);
			void initialize(void (DrawableFrameBuffer::*build_frame_buffer_storage_function)(GLenum framebuffer_format, const glm::uvec2& size),
							const UniformTextureMultisample<RenderTypes> * ... uniform_textures);
		
			void generate_and_bind();
			void finish_initialization(void (DrawableFrameBuffer::*build_frame_buffer_storage_function)(GLenum framebuffer_format, const glm::uvec2& size));

			unsigned int _ID;
			unsigned int _render_ID;
			glm::uvec2   _size;
			uint         _drawing_buffers[sizeof...(RenderTypes)];

			const Texture<DepthAttachement>* _custom_depth_buffer;
			std::vector<types_id>            _attachements_types;
	};
};

namespace ssl
{
	template<typename... RenderTypes>
	DrawableFrameBuffer<RenderTypes...>::DrawableFrameBuffer(const UniformTexture2D<RenderTypes> * ... uniform_textures)
		: BasicFrameBuffer(glm::vec4(0.0,0.0,0.0,0.0)),
		  _custom_depth_buffer(NULL)
	{
		this->initialize(&DrawableFrameBuffer::build_frame_buffer_storage, uniform_textures...);
	}

	template<typename... RenderTypes>
	DrawableFrameBuffer<RenderTypes...>::DrawableFrameBuffer(const DepthBuffer* depth_buffer, const UniformTexture2D<RenderTypes> * ... uniform_textures)
		: BasicFrameBuffer(glm::vec4(0.0,0.0,0.0,0.0)),
		  _custom_depth_buffer(depth_buffer)
	{
		this->initialize(&DrawableFrameBuffer::build_frame_buffer_storage, uniform_textures...);
	}

	template<typename... RenderTypes>
	DrawableFrameBuffer<RenderTypes...>::DrawableFrameBuffer(const DepthStencilBuffer* depth_stencil_buffer, const UniformTexture2D<RenderTypes> * ... uniform_textures)
		: BasicFrameBuffer(glm::vec4(0.0,0.0,0.0,0.0)),
		  _custom_depth_buffer(depth_stencil_buffer)
	{
		this->initialize(&DrawableFrameBuffer::build_frame_buffer_storage, uniform_textures...);
	}

	template<typename... RenderTypes>
	DrawableFrameBuffer<RenderTypes...>::DrawableFrameBuffer(const UniformTextureMultisample<RenderTypes> * ... uniform_textures)
		: BasicFrameBuffer(glm::vec4(0.0,0.0,0.0,0.0)),
		  _custom_depth_buffer(NULL)
	{
		this->initialize(&DrawableFrameBuffer::build_frame_buffer_multisample_storage, uniform_textures...);
	}

	template<typename... RenderTypes>
	DrawableFrameBuffer<RenderTypes...>::DrawableFrameBuffer(const DepthBufferMultisample* depth_buffer, const UniformTextureMultisample<RenderTypes> * ... uniform_textures)
		: BasicFrameBuffer(glm::vec4(0.0,0.0,0.0,0.0)),
		  _custom_depth_buffer(depth_buffer)
	{
		this->initialize(&DrawableFrameBuffer::build_frame_buffer_multisample_storage, uniform_textures...);
	}

	template<typename... RenderTypes>
	DrawableFrameBuffer<RenderTypes...>::DrawableFrameBuffer(const DepthStencilBufferMultisample* depth_stencil_bufferMS, const UniformTextureMultisample<RenderTypes> * ... uniform_textures)
		: BasicFrameBuffer(glm::vec4(0.0,0.0,0.0,0.0)),
		  _custom_depth_buffer(depth_stencil_bufferMS)
	{
		this->initialize(&DrawableFrameBuffer::build_frame_buffer_multisample_storage, uniform_textures...);
	}

#define SSL_GENERATE_DRAWABLE_FRAMEBUFFER_INITIALIZE(FrameBufferType)\
	template<typename... RenderTypes>\
	void DrawableFrameBuffer<RenderTypes...>::initialize(void (DrawableFrameBuffer::*build_frame_buffer_storage_function)(GLenum framebuffer_format, const glm::uvec2& size),\
															 const FrameBufferType<RenderTypes> * ... uniform_textures)\
	{\
		_size = this->check_sizes(*uniform_textures...);\
\
		this->generate_and_bind();\
\
		this->attach_textures(*uniform_textures...);\
		\
		this->finish_initialization(build_frame_buffer_storage_function);\
	}

	SSL_GENERATE_DRAWABLE_FRAMEBUFFER_INITIALIZE(UniformTexture2D)
	SSL_GENERATE_DRAWABLE_FRAMEBUFFER_INITIALIZE(UniformTextureMultisample)

	template<typename... RenderTypes>
	void DrawableFrameBuffer<RenderTypes...>::generate_and_bind()	
	{
		_attachements_types.resize(sizeof...(RenderTypes));

		uint i=0;
		((_attachements_types.at(i)=get_type_id<RenderTypes>(), i++), ...);

		for(uint i=0; i<sizeof...(RenderTypes);i++)
		{
			_drawing_buffers[i] = GL_COLOR_ATTACHMENT0 + i;
		}

		GL(glGenFramebuffers(1, &_ID));
	
		GL(glBindFramebuffer(GL_FRAMEBUFFER, _ID));

		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY, "FrameBuffer created, id: " + std::to_string(this->get_ID())
							+ ", dimension: " + std::to_string(_size.x) + "x" + std::to_string(_size.y));
		}
		#endif
	}


	template<typename... RenderTypes>
	void DrawableFrameBuffer<RenderTypes...>::finish_initialization(void (DrawableFrameBuffer::*build_frame_buffer_storage_function)(GLenum framebuffer_format, const glm::uvec2& size))	
	{
		_render_ID = 0;

		if(_custom_depth_buffer == NULL)
		{
			GL(glGenRenderbuffers(1, &_render_ID));
			GL(glBindRenderbuffer(GL_RENDERBUFFER, _render_ID));

			(this->*build_frame_buffer_storage_function)(GL_DEPTH24_STENCIL8, _size);

			GL(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, _render_ID));
		}
		else
		{
			_custom_depth_buffer->attach_on_framebuffer(0);
		}	

		bool creation_suceed = (glCheckNamedFramebufferStatus(_ID, GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
		if(!creation_suceed)
		{
			err("Failed to create FrameBuffer");
		}

		this->unbind();
	}


	template<typename... RenderTypes>
	unsigned int DrawableFrameBuffer<RenderTypes...>::get_ID() const	
	{
		return _ID;
	}


	template<typename... RenderTypes>
	void DrawableFrameBuffer<RenderTypes...>::build_frame_buffer_storage(GLenum framebuffer_format, const glm::uvec2& size)
	{
		GL(glRenderbufferStorage(GL_RENDERBUFFER, framebuffer_format, size.x, size.y));
	}

	template<typename... RenderTypes>
	void DrawableFrameBuffer<RenderTypes...>::build_frame_buffer_multisample_storage(GLenum framebuffer_format, const glm::uvec2& size)
	{
		GL(glRenderbufferStorageMultisample(GL_RENDERBUFFER, ANTIALIASING, framebuffer_format, size.x, size.y));
	}

	template<typename... RenderTypes>
	void DrawableFrameBuffer<RenderTypes...>::attach_textures(const UniformTexture2D<RenderTypes> & ... uniform_textures)
	{
		uint i=0;

		((uniform_textures.get_texture().attach_on_framebuffer(i),
		  i++), ...);
	}

	template<typename... RenderTypes>
	void DrawableFrameBuffer<RenderTypes...>::attach_textures(const UniformTextureMultisample<RenderTypes> & ... uniform_textures)
	{
		uint i=0;

		((uniform_textures.get_texture().attach_on_framebuffer(i),
		  i++), ...);
	}

#define SSL_GENERATE_DRAWABLE_FRAMEBUFFER_CHECK_SIZE(FrameBufferType)\
	template<typename... RenderTypes>\
	glm::uvec2 DrawableFrameBuffer<RenderTypes...>::check_sizes(const FrameBufferType<RenderTypes> & ... uniform_textures)\
	{\
		if(sizeof...(uniform_textures) == 0)\
			err("Try to create a frambuffer without attachements");\
\
		if(sizeof...(uniform_textures) > 16)\
			err("A framebuffer cannot have more than 16 attachements (i am not sure, if not, modify the code here : " + SSL_FILE_AND_LINE + ")");\
\
		uint i=0;\
		glm::uvec2 s = glm::uvec2(0,0);\
		glm::uvec2 tex2D_size;\
		bool all_sames = true;\
		((	tex2D_size = glm::uvec2(uniform_textures.get_dimension_size().x, uniform_textures.get_dimension_size().y),\
			s          = (i==0) ? tex2D_size : s,\
			all_sames  = (tex2D_size == s) ? all_sames : false,\
			i++), ...);\
\
		if(!all_sames)\
			err("All the attachements of the frambuffer have not the same size");\
\
		if(s == glm::uvec2(0, 0))\
			err("The size of the framebuffer must be different from (0,0)");\
\
		return s;\
	}

	SSL_GENERATE_DRAWABLE_FRAMEBUFFER_CHECK_SIZE(UniformTexture2D)
	SSL_GENERATE_DRAWABLE_FRAMEBUFFER_CHECK_SIZE(UniformTextureMultisample)

	template<typename... RenderTypes>
	void DrawableFrameBuffer<RenderTypes...>::bind() const
	{
		GL(glBindFramebuffer(GL_FRAMEBUFFER, _ID));
		GL(glDrawBuffers(sizeof...(RenderTypes), _drawing_buffers));
	}

	template<typename... RenderTypes>
	void DrawableFrameBuffer<RenderTypes...>::unbind() const
	{
		GL(glBindFramebuffer(GL_FRAMEBUFFER, 0));
	}

	template<typename... RenderTypes>
	const glm::uvec2& DrawableFrameBuffer<RenderTypes...>::frame_size() const
	{
		return _size;
	}

	template<typename... RenderTypes>
	DrawableFrameBuffer<RenderTypes...>::~DrawableFrameBuffer()
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY, "Framebuffer deleted: " + std::to_string(_ID));
		}
		#endif
		
		if(_render_ID != 0)
		{
			GL(glDeleteRenderbuffers(1, &_render_ID));
		}
		GL(glDeleteFramebuffers(1, &_ID));
	}

	template<typename... RenderTypes>
	const std::vector<types_id>& DrawableFrameBuffer<RenderTypes...>::get_attachements_types() const
	{
		return _attachements_types;
	}

};

#endif //_DRAWABLE_FRAME_BUFFER_HPP_
