#ifndef _LABELED_RECORDER_HPP_
#define _LABELED_RECORDER_HPP_

#include <string>
#include <cstdint>
#include <map>

namespace ssl
{
	class LabeledRecorder
	{
	public:
		LabeledRecorder();

		void start(const std::string& name);
		void stop(const std::string& name);
		void gl_start(const std::string& name);
		void gl_stop(const std::string& name);

		void print() const;
		void print_list() const;
		void print_custom(const std::string& poststring) const;
		void release();
		void record();
		void reset_durations();
		bool is_recording() const;

	private:

		std::map<std::string, uint64_t> _times;
		std::map<std::string, uint64_t> _durations;
		bool _recording;
	};
};

#endif //_LABELED_RECORDER_HPP_
