#include "drawing_context.hpp"

#include <GL/glew.h>
#include "debug.hpp"

using namespace std;

namespace ssl
{
	DrawingContext::DrawingContext() : _initialized(false), _clear_color(glm::vec4(0.0))
	{

	}

	DrawingContext::~DrawingContext()
	{
		GL(glDeleteVertexArrays(1, &_VAO));
	}

	const glm::vec4& DrawingContext::get_clear_color()
	{
		return _clear_color;
	}

	void DrawingContext::set_clear_color(const glm::vec4& color)
	{
		_clear_color = color;
	}

	void DrawingContext::create()
	{
		if(_initialized)
		{
			err("DrawingContext already created");
		}
		_initialized=true;	
		GL(glGenVertexArrays(1, &_VAO)); 
		this->bind();
	}

	void DrawingContext::bind() const
	{
		if(!_initialized)
		{
			err("DrawingContext most be created before call to DrawingContext::bind()");
		}
		GL(glBindVertexArray(_VAO));
	}
};