#include "vvv.hpp"

using namespace glm;
using namespace std;
using namespace ssl;

namespace ssl
{
	static  glm::vec3 _x   = glm::vec3(1.0, 0.0, 0.0);
	static  glm::vec3 _y   = glm::vec3(0.0, 1.0, 0.0);
	static  glm::vec3 _z   = glm::vec3(0.0, 0.0, 1.0);
	static  glm::vec3 _xy  = glm::vec3(1.0, 1.0, 0.0);
	static  glm::vec3 _yz  = glm::vec3(0.0, 1.0, 1.0);
	static  glm::vec3 _xz  = glm::vec3(1.0, 0.0, 1.0);
	static  glm::vec3 _xyz = glm::vec3(1.0, 1.0, 1.0);

	const glm::vec3& vvv::x()   
	{
		return _x;
	}
	const glm::vec3& vvv::y()   
	{
		return _y;
	}
	const glm::vec3& vvv::z()   
	{
		return _z;
	}
	const glm::vec3& vvv::xy()  
	{
		return _xy;
	}
	const glm::vec3& vvv::yz()  
	{
		return _yz;
	}
	const glm::vec3& vvv::xz()  
	{
		return _xz;
	}
	const glm::vec3& vvv::xyz() 
	{
		return _xyz;
	}
};