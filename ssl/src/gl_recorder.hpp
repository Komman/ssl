#ifndef _GL_RECORDER_HPP_
#define _GL_RECORDER_HPP_

#include <string>
#include "basic_recorder.hpp"

namespace ssl
{
	class GlRecorder: public BasicRecorder
	{
	public:
		GlRecorder();
		void start() override;
		void stop() override;

		// In nanoseconds
		uint64_t get_last_duration();

	private:
		uint64_t    _nanosec;
	};
};

#endif //_GL_RECORDER_HPP_
