#ifndef _BASIC_RECORDER_HPP_
#define _BASIC_RECORDER_HPP_

#include <cstdint>

namespace ssl
{
	class BasicRecorder
	{
	public:
		virtual void start() =0;
		virtual void stop()  =0;
	};
};

#endif //_BASIC_RECORDER_HPP_
