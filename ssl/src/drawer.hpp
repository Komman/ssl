#ifndef _DRAWER_HPP_
#define _DRAWER_HPP_

#include "global_drawing.hpp"
#include "shader.hpp"

namespace ssl
{
	class Drawer : public Shader
	{
	public:
		struct stencilParams
		{
			bool   enable;       // glEnable/glDisable(GL_STENCIL_TEST)
			GLuint mask;         // glStencilMask(mask)
			GLenum function;     // glStencilFunc(func, ref, mask)
			GLint  func_ref;       
			GLuint func_mask;
			GLenum op_sfail;     // glStencilOp(sfail, dpfail, dppass)
			GLenum op_dpfail;   
			GLenum op_dppass;
		};

		struct drawParams
		{
			glm::uvec2    view_port_origin;
			glm::uvec2    view_port_size;
			bool          auto_view_port;
			bool          depth;
			bool          culling;
			bool          blending;
			bool          depth_mask;
			GLenum        depth_func;
			GLenum        blend_sfactor;
			GLenum        blend_dfactor;
			GLenum        blend_func_eq;
			stencilParams stencil_params;
		};

	public:
		Drawer(const BasicShaderParser& parser,
			   const std::string& vertex_shader_name,
			   const std::string& fragment_shader_name,
			// Functions called on th fragment result, they must have
			// the same argument count as fragment "out" arguments count,
			// and all of them must be "inout"
			// By default, no functions are called
			   const std::vector<std::string>& fragment_modifiers  =  {}
		);
		virtual ~Drawer() {}

		void draw(const VertexArrayObject& vao);
		void draw(const BasicFrameBuffer& target_fb, const VertexArrayObject& vao);

		template<typename... Buffers>
		void draw(const Buffers & ... buff);

		template<typename... Buffers>
		void draw_elements(const ElementBuffer& elements, const Buffers & ... buff);

		template<typename... Buffers>
		void draw(const BasicFrameBuffer& target_fb,
				  const Buffers & ... buff);
		template<typename... Buffers>
		void draw_elements(const BasicFrameBuffer& target_fb,
						   const ElementBuffer& elements,
						   const Buffers & ... buff);

		void draw_object(const DrawableObject& obj);
		void draw_object(const ElementDrawableObject& obj);
		void draw_object(const BasicFrameBuffer& target_fb, const DrawableObject& obj);
		void draw_object(const BasicFrameBuffer& target_fb, const ElementDrawableObject& obj);


		template<typename... Buffers>
		void draw_elements_in_framebuffer(const BasicFrameBuffer& target_fb,
										  const ElementBuffer& elements,
										  const Buffers & ... buff);

		template<typename... Buffers>
		void draw_in_framebuffer(const BasicFrameBuffer& target_fb,
								 const Buffers & ... buff);

		void depth(bool enable);
		void culling(bool enable);
		void blending(bool enable);
		void custom_view_port(const glm::uvec2& origin, const glm::uvec2& size);
		void disable_custom_view_port();
		//see openGL::glBlendFunc function
		void set_blend_function(GLenum sfactor = global_drawing::DEFAULT::SFACTOR,
								GLenum dfactor = global_drawing::DEFAULT::DFACTOR); 
		void set_blend_equation(GLenum mode); 
		void depth_mask(bool enable);
		void depth_function(GLenum func);
		void set_stencil_params(const stencilParams& params);
		void stencil_testing(bool enable);
		void stencil_mask(GLuint mask); // 8 bits
		void stencil_function(GLenum func, GLint ref, GLuint mask = 0xFF);
		void stencil_op(GLenum sfail, GLenum dpfail, GLenum dppass);

		void set_draws_params(const drawParams& params);
		const drawParams& get_draws_params() const;

	private:

		void target_framebuffer(const BasicFrameBuffer* framebuffer_ot_target);
		void target_screen();
		void check_attachements(const BasicFrameBuffer* framebuffer_ot_target);
		void set_all_params();

		drawParams _draw_params;
	};
};




namespace ssl
{
	template<typename... Buffers>
	void Drawer::draw_elements(const ElementBuffer& elements, const Buffers & ... buff)
	{
		this->target_screen();
		this->set_all_params();
		this->Shader::draw_elements(elements, buff...);
	}

	template<typename... Buffers>
	void Drawer::draw(const Buffers & ... buff)
	{
		this->target_screen();
		this->set_all_params();
		this->Shader::draw(buff...);
	}
	
	template<typename... Buffers>
	void Drawer::draw_elements(const BasicFrameBuffer& target_fb, const ElementBuffer& elements, const Buffers & ... buff)
	{
		this->target_framebuffer(&target_fb);
		this->set_all_params();
		this->Shader::draw_elements(elements, buff...);
	}

	template<typename... Buffers>
	void Drawer::draw(const BasicFrameBuffer& target_fb, const Buffers & ... buff)
	{
		this->target_framebuffer(&target_fb);
		this->set_all_params();
		this->Shader::draw(buff...);
	}

	template<typename... Buffers>
	void Drawer::draw_elements_in_framebuffer(const BasicFrameBuffer& target_fb, const ElementBuffer& elements, const Buffers & ... buff)
	{
		this->target_framebuffer(&target_fb);
		this->set_all_params();
		this->Shader::draw_elements(elements, buff...);
	}

	template<typename... Buffers>
	void Drawer::draw_in_framebuffer(const BasicFrameBuffer& target_fb, const Buffers & ... buff)
	{
		this->target_framebuffer(&target_fb);
		this->set_all_params();
		this->Shader::draw(buff...);
	}
};

#endif //_DRAWER_HPP_
