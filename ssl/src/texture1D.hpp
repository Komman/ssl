#ifndef _TEXTURE1D_HPP_
#define _TEXTURE1D_HPP_


#include "texture.hpp"

namespace ssl
{
	template<typename Type>
	class Texture1D : public Texture<Type>
	{
	public:
		Texture1D(unsigned int  size,
				  void* data          = NULL,
				  GLint minmag_filter = DEFAULT::MINMAG_FILTER,
				  GLint wrap          = DEFAULT::TEXTURE_WRAP);
		
		Texture1D(const glm::uvec3&  dimension_size,
				  void* data,
				  GLint minmag_filter,
				  GLint wrap);

		unsigned int size()            const;
		GLenum       get_gl_texture_type()     const override;	
		attachementType get_attachement_type() const override;

	protected:

		void change_data_if_bound(const glm::uvec3& dimension_size, void* data) override;

		void attach_on_framebuffer_if_bound(unsigned int attachement_index)   const override;
		void tex_image_if_bound(const glm::uvec3& dimension_size, void* data) const override;
		bool mipmaps_enable()                                                 const override;
		void definitive_resize_GPU(const glm::uvec3& new_dimension_size)                 const override;


	private:

	};
};


namespace ssl
{
	template<typename Type>
	Texture1D<Type>::Texture1D(unsigned int size,
							   void* data,
							   GLint minmag_filter,
							   GLint wrap)
		: Texture1D<Type>(glm::uvec3(size, 1, 1), data, minmag_filter, wrap)
	{
		
	}

	template<typename Type>
	Texture1D<Type>::Texture1D(const glm::uvec3&  dimension_size,
							   void* data,
							   GLint minmag_filter,
							   GLint wrap)
		: Texture<Type>(dimension_size, minmag_filter, wrap)
	{
		this->initialize(data);
	}

	template<typename Type>
	attachementType Texture1D<Type>::get_attachement_type() const
	{
		return COLOR_ATTACHEMENT;
	}

	template<typename Type>
	unsigned int Texture1D<Type>::size() const
	{
		return this->get_dimension_size().x;		
	}

	template<typename Type>
	void Texture1D<Type>::tex_image_if_bound(const glm::uvec3& dimension_size, void* data) const
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY_FLOW))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY_FLOW, "Creating texture1D data of texture id:" + std::to_string(this->get_ID()) 
				+ ", with data of size: "
				+ std::to_string(dimension_size.x));
		}
		#endif

		GL(glTexImage1D(this->get_gl_texture_type(),
			 			0,
			 			Texture<Type>::get_GL_internal_format(),
			 			dimension_size.x,
			 			0,
			 			Texture<Type>::get_GL_valid_format(),
			 			Texture<Type>::get_GL_data_type(),
			 			data));
	}

	template<typename Type>
	void Texture1D<Type>::change_data_if_bound(const glm::uvec3& dimension_size, void* data)
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY_FLOW))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY_FLOW, "Updating texture1D data of texture id:" + std::to_string(this->get_ID()));
		}
		#endif

		GL(glTexSubImage1D(
			this->get_gl_texture_type(),
			0,
			0,
			dimension_size.x,
			Texture<Type>::get_GL_valid_format(),
			Texture<Type>::get_GL_data_type(),
			data
		));
	}

	template<typename Type>
	GLenum Texture1D<Type>::get_gl_texture_type() const
	{
		return GL_TEXTURE_1D;
	}

	template<typename Type>
	void Texture1D<Type>::definitive_resize_GPU(const glm::uvec3& new_dimension_size) const
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY_FLOW))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY_FLOW, "Resizing texture1D id:" + std::to_string(this->get_ID()) 
				+ ", with size: "
				+ std::to_string(new_dimension_size.x));
		}
		#endif

		GL(glTextureStorage1D(this->get_ID(), 1, Texture<Type>::get_GL_internal_format(), new_dimension_size.x));
	}

	template<typename Type>
	bool Texture1D<Type>::mipmaps_enable() const
	{
		return true;
	}

	template<typename Type>
	void Texture1D<Type>::attach_on_framebuffer_if_bound(unsigned int attachement_index) const
	{
		GL(glFramebufferTexture1D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + attachement_index, this->get_gl_texture_type(), this->get_ID(), 0));
	}
}

#endif //_TEXTURE1D_HPP_
