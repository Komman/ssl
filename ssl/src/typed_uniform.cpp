#include "typed_uniform.hpp"

#include <glm/glm.hpp>

#include "texture1D.hpp"
#include "texture2D.hpp"
#include "image2D.hpp"
#include "texture3D.hpp"
#include "texture_multisample.hpp"
#include "depth_buffer.hpp"

using namespace std;
using namespace ssl;
using namespace glm;

#define UNIFORM_BUILDER_DECLARATION_GENERAL(type, str, layout)\
	template<>\
	std::string TypedUniform<type>::get_str_type() const\
	{\
		return str;\
	}\
	template<>\
	std::string TypedUniform<type>::get_layout() const\
	{\
		return layout;\
	}

#define UNIFORM_BUILDER_DECLARATION(type)     UNIFORM_BUILDER_DECLARATION_GENERAL(type,      #type, "")
#define UNIFORM_BUILDER_GLM_DECLARATION(type) UNIFORM_BUILDER_DECLARATION_GENERAL(glm::type, #type, "")

namespace ssl
{

	//TODO:
	// matIxJ

	UNIFORM_BUILDER_GLM_DECLARATION(mat4)
	UNIFORM_BUILDER_GLM_DECLARATION(mat3)
	UNIFORM_BUILDER_GLM_DECLARATION(mat2)

	UNIFORM_BUILDER_GLM_DECLARATION(dmat4)
	UNIFORM_BUILDER_GLM_DECLARATION(dmat3)
	UNIFORM_BUILDER_GLM_DECLARATION(dmat2)


	UNIFORM_BUILDER_DECLARATION(float)
	UNIFORM_BUILDER_DECLARATION(double)
	UNIFORM_BUILDER_DECLARATION(int)
	UNIFORM_BUILDER_DECLARATION(uint)
	UNIFORM_BUILDER_DECLARATION(bool)


	UNIFORM_BUILDER_GLM_DECLARATION(vec4)
	UNIFORM_BUILDER_GLM_DECLARATION(vec3)
	UNIFORM_BUILDER_GLM_DECLARATION(vec2)

	UNIFORM_BUILDER_GLM_DECLARATION(dvec4)
	UNIFORM_BUILDER_GLM_DECLARATION(dvec3)
	UNIFORM_BUILDER_GLM_DECLARATION(dvec2)

	UNIFORM_BUILDER_GLM_DECLARATION(bvec4)
	UNIFORM_BUILDER_GLM_DECLARATION(bvec3)
	UNIFORM_BUILDER_GLM_DECLARATION(bvec2)

	UNIFORM_BUILDER_GLM_DECLARATION(uvec4)
	UNIFORM_BUILDER_GLM_DECLARATION(uvec3)
	UNIFORM_BUILDER_GLM_DECLARATION(uvec2)

	UNIFORM_BUILDER_GLM_DECLARATION(ivec4)
	UNIFORM_BUILDER_GLM_DECLARATION(ivec3)
	UNIFORM_BUILDER_GLM_DECLARATION(ivec2)



	UNIFORM_BUILDER_DECLARATION_GENERAL(DepthBuffer, "sampler2D", "")

	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture1D<float>, "sampler1D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture1D<vec2>, "sampler1D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture1D<vec3>, "sampler1D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture1D<vec4>, "sampler1D", "")

	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture1D<int>, "isampler1D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture1D<ivec2>, "isampler1D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture1D<ivec3>, "isampler1D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture1D<ivec4>, "isampler1D", "")

	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture1D<uint>, "usampler1D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture1D<uvec2>, "usampler1D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture1D<uvec3>, "usampler1D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture1D<uvec4>, "usampler1D", "")

	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image1D<float>, "image1D", "layout(r32f)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image1D<vec2>, "image1D", "layout(rg32f)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image1D<vec3>, "image1D", "layout(rgba32f)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image1D<vec4>, "image1D", "layout(rgba32f)")

	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image1D<int>, "iimage1D", "layout(r32i)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image1D<ivec2>, "iimage1D", "layout(rg32i)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image1D<ivec3>, "iimage1D", "layout(rgba32i)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image1D<ivec4>, "iimage1D", "layout(rgba32i)")

	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image1D<uint>, "uimage1D", "layout(r32ui)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image1D<uvec2>, "uimage1D", "layout(rg32ui)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image1D<uvec3>, "uimage1D", "layout(rgba32ui)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image1D<uvec4>, "uimage1D", "layout(rgba32ui)")


	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture2D<float>, "sampler2D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture2D<vec2>, "sampler2D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture2D<vec3>, "sampler2D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture2D<vec4>, "sampler2D", "")

	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture2D<int>, "isampler2D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture2D<ivec2>, "isampler2D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture2D<ivec3>, "isampler2D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture2D<ivec4>, "isampler2D", "")

	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture2D<uint>, "usampler2D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture2D<uvec2>, "usampler2D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture2D<uvec3>, "usampler2D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture2D<uvec4>, "usampler2D", "")

	UNIFORM_BUILDER_DECLARATION_GENERAL(Image2D<float>, "image2D", "layout(r32f)")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Image2D<vec2>, "image2D", "layout(rg32f)")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Image2D<vec3>, "image2D", "layout(rgba32f)")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Image2D<vec4>, "image2D", "layout(rgba32f)")

	UNIFORM_BUILDER_DECLARATION_GENERAL(Image2D<int>, "iimage2D", "layout(r32i)")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Image2D<ivec2>, "iimage2D", "layout(rg32i)")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Image2D<ivec3>, "iimage2D", "layout(rgba32i)")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Image2D<ivec4>, "iimage2D", "layout(rgba32i)")

	UNIFORM_BUILDER_DECLARATION_GENERAL(Image2D<uint>, "uimage2D", "layout(r32ui)")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Image2D<uvec2>, "uimage2D", "layout(rg32ui)")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Image2D<uvec3>, "uimage2D", "layout(rgba32ui)")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Image2D<uvec4>, "uimage2D", "layout(rgba32ui)")


	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture3D<float>, "sampler3D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture3D<vec2>, "sampler3D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture3D<vec3>, "sampler3D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture3D<vec4>, "sampler3D", "")

	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture3D<int>, "isampler3D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture3D<ivec2>, "isampler3D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture3D<ivec3>, "isampler3D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture3D<ivec4>, "isampler3D", "")

	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture3D<uint>, "usampler3D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture3D<uvec2>, "usampler3D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture3D<uvec3>, "usampler3D", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(Texture3D<uvec4>, "usampler3D", "")

	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image3D<float>, "image3D", "layout(r32f)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image3D<vec2>, "image3D", "layout(rg32f)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image3D<vec3>, "image3D", "layout(rgba32f)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image3D<vec4>, "image3D", "layout(rgba32f)")

	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image3D<int>, "iimage3D", "layout(r32i)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image3D<ivec2>, "iimage3D", "layout(rg32i)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image3D<ivec3>, "iimage3D", "layout(rgba32i)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image3D<ivec4>, "iimage3D", "layout(rgba32i)")

	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image3D<uint>, "uimage3D", "layout(r32ui)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image3D<uvec2>, "uimage3D", "layout(rg32ui)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image3D<uvec3>, "uimage3D", "layout(rgba32ui)")
	// UNIFORM_BUILDER_DECLARATION_GENERAL(Image3D<uvec4>, "uimage3D", "layout(rgba32ui)")


	UNIFORM_BUILDER_DECLARATION_GENERAL(TextureMultisample<float>, "sampler2DMS", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(TextureMultisample<vec2>, "sampler2DMS", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(TextureMultisample<vec3>, "sampler2DMS", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(TextureMultisample<vec4>, "sampler2DMS", "")

	UNIFORM_BUILDER_DECLARATION_GENERAL(TextureMultisample<int>, "isampler2DMS", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(TextureMultisample<ivec2>, "isampler2DMS", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(TextureMultisample<ivec3>, "isampler2DMS", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(TextureMultisample<ivec4>, "isampler2DMS", "")

	UNIFORM_BUILDER_DECLARATION_GENERAL(TextureMultisample<uint>, "usampler2DMS", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(TextureMultisample<uvec2>, "usampler2DMS", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(TextureMultisample<uvec3>, "usampler2DMS", "")
	UNIFORM_BUILDER_DECLARATION_GENERAL(TextureMultisample<uvec4>, "usampler2DMS", "")

};
