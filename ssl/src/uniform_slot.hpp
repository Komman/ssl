#ifndef _UNIFORM_SLOT_HPP_
#define _UNIFORM_SLOT_HPP_

#include "debug.hpp"
#include "basic_uniform_slot.hpp"
#include "uniform_controller.hpp"

#include <vector>

namespace ssl
{
	template<typename Type>
	class UniformSlot : public BasicUniformSlot
	{
	public:
		UniformSlot(const std::string& name,
					const std::vector<Type>& vec_value);

		void add_value(const Type& addvalue);
		void change_value(const std::vector<Type>& value);
		void change_subvalue(uint indice, const Type& subvalue);

		const std::vector<Type>& get_value() const;

	protected:
		BasicUniformController* alloc_instance(GLuint shader_to_link, uint texture_count); 
		void* get_data() const;
		uint  get_data_count() const;

	private:

		std::vector<Type> _value;
	};
};

namespace ssl
{
	template<typename Type>
	UniformSlot<Type>::UniformSlot(const std::string& name, const std::vector<Type>& value)
		: BasicUniformSlot(name),
		 _value(value)
	{
		
	}

	template<typename Type>
	BasicUniformController* UniformSlot<Type>::alloc_instance(GLuint shader_to_link, uint texture_count)
	{
		BasicUniformController* typed_instance = new UniformController<Type>(this->get_name(), shader_to_link);
		return typed_instance;
	}

	template<typename Type>
	void UniformSlot<Type>::change_value(const std::vector<Type>& value)
	{
		_value = value;
		this->change_data();
	}

	template<typename Type>
	void UniformSlot<Type>::change_subvalue(uint indice, const Type& subvalue)
	{
		#ifdef SSL_DEBUG
		if(indice >= _value.size())
		{
			err("UniformSlot<Type>::change_subvalue(): Try to change a value out of range : " + std::to_string(indice) + "/" + std::to_string(_value.size()));
		}
		#endif

		_value[indice] = subvalue;
		this->change_data();
	}

	template<typename Type>
	void* UniformSlot<Type>::get_data() const
	{
		return (void*)(_value.data());
	}

	template<typename Type>
	uint  UniformSlot<Type>::get_data_count() const
	{
		return _value.size();
	}

	template<typename Type>
	const std::vector<Type>& UniformSlot<Type>::get_value() const
	{
		return _value;
	}

	template<typename Type>
	void UniformSlot<Type>::add_value(const Type& addvalue)
	{
		_value.push_back(addvalue);
		this->change_data();
	}

};

#endif //_UNIFORM_SLOT_HPP_
