#include "view.hpp"
#include "window.hpp"

#include <iostream>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/string_cast.hpp>

using namespace std;
using namespace ssl;
using namespace glm;

namespace ssl
{

	constexpr float UP_VIEW_SECURITY = 0.999;


	View::View(const vec3& initial_position,
			   float min_view_distance,
			   float max_view_distance,
			   float view_field)

	: _model(glm::mat4(1.0f)),
	  _projection(glm::perspective(view_field, (float)(window::size().x) / (float)(window::size().y), min_view_distance, max_view_distance)) ,
	  _position(initial_position),
	  _direction(vec3(1.0, 0.0, 0.0)),
	  _up_head(vec3(0.0, 1.0, 0.0)),
	  _view_field(view_field),
	  _min_view_distance(min_view_distance),
	  _max_view_distance(max_view_distance),
	  _view_changed(true)
	{
		this->compute_right_vector();
		
		mat4 view_tmp = glm::lookAt( 
		    _position, 
		    _position + _direction, 
		    _up_head 
		);
		_MVP = _projection * view_tmp * _model;
	}

	void View::look_at(const vec3& look_at_position)
	{
		vec3 dir = look_at_position - _position;

		if(glm::length(dir) < 0.0001f || glm::length(glm::cross(dir, _up_head)) < 0.0001f)
		{
			_direction = vec3(1,0,0);
		}
		else
		{
			_direction = glm::normalize(dir);
		}
		
		this->compute_right_vector();
		_view_changed = true;
	}

	void View::tp(const vec3& new_position)
	{
		_position = new_position;
		_view_changed = true;
	}

	void View::horizontal_rotate(float angle)
	{
		vec3 dir = glm::rotate(_direction, angle, _up_head);

		if(glm::length(dir) < 0.0001f)
		{
			_direction = vec3(1,0,0);
		}
		else
		{
			_direction = glm::normalize(dir);
		}

		this->compute_right_vector();
		_view_changed = true;
	}

	void View::vertical_rotate(float angle)
	{
		vec3 dir_temp = glm::rotate(_direction, angle, _right_vector); 
		
		if(glm::length(dir_temp) < 0.0001f)
		{
			dir_temp = vec3(1,0,0);
		}
		else
		{
			dir_temp = glm::normalize(dir_temp);
		}

		if(abs(dir_temp.y)<= UP_VIEW_SECURITY)
		{
			_direction = dir_temp;	
			this->compute_right_vector();
			_view_changed = true;
		}
	}

	void View::rotate(const vec2& angles)
	{
		horizontal_rotate(angles.x);
		vertical_rotate(angles.y);
	}

	mat4 View::compute_MVP() const
	{
		if(_view_changed)
		{
			mat4 view_tmp = glm::lookAt( 
			    _position, 
			    _position + _direction, 
			    _up_head 
			);

			_view_changed=false;
			return _projection * view_tmp * _model; 
		}

		return _MVP;
	}

	void View::compute_right_vector()
	{
		vec3 rive = cross(vec3(0.0,1.0,0.0), _direction);
		
		if(glm::length(rive) <= 0.00001f)
		{
			_right_vector = vec3(0.0,0.0,-1.0);
		}
		else
		{
			_right_vector=-normalize(rive);
		}
	}

	const glm::mat4& View::get_MVP() const
	{
		_MVP = this->compute_MVP();
		return _MVP;
	}

	const glm::vec3& View::get_position() const
	{
		return _position;
	}

	const glm::vec3& View::get_direction() const
	{
		return _direction;
	}

	const glm::vec3& View::get_right_vector() const
	{
		return _right_vector;
	}

	const glm::vec3& View::get_up_head() const
	{
		return _up_head;
	}	

	float View::get_view_field()        const
	{
		return _view_field; 
	}
	float View::get_min_view_distance() const
	{
		return _min_view_distance; 
	}
	float View::get_max_view_distance() const
	{
		return _max_view_distance; 
	}
};
