#ifndef _BASIC_SHADER_HPP_
#define _BASIC_SHADER_HPP_

#include <string>
#include <vector>
#include <GL/glew.h>

namespace ssl
{
	constexpr bool PRINT_SHADERS_LOAD = false; 
	constexpr bool PRINT_SHADERS_COMPILATION = true; 

	class BasicShader
	{
	public:
		BasicShader();
		virtual ~BasicShader();

		void load(const std::string& vertex_file_path, const std::string& fragment_file_path);
		void load_compute(const std::string& compute_path);

		
		void use() const;
		GLuint get_ID() const;

	protected:

		virtual std::string additionnal_name() const;

	private:

		struct err_loc
		{
			unsigned int file;
			unsigned int line;
			unsigned int character;
		};	


		static constexpr unsigned int LINE_PRINTING_COUT_ERROR = 3; 

		void	print_error_msg(const std::vector<char>& err_msg, const std::string& file_path) const;
		err_loc	get_error_location(const std::vector<char>& err_msg) const;
		
		std::vector<std::string> get_lines(const std::string& file_path, unsigned int begin, unsigned int end) const;

	private:

		GLuint _ID;
		static GLuint _CURRENT_SHADER_ID;
	};
};

#endif //_BASIC_SHADER_HPP_
