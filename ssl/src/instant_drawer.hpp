#ifndef _INSTANT_DRAWER_HPP_
#define _INSTANT_DRAWER_HPP_

#include "drawer.hpp"
#include "array_buffer.hpp"

namespace ssl
{

	// This class should be used only for 3D debgging

	class InstantDrawer : public Drawer
	{
	public:
		InstantDrawer(const BasicShaderParser& parser,
					  const std::string& vertex_shader_name,
					  const std::string& fragment_shader_name,
					  // Functions called on th fragment result, they must have
					  // the same argument count as fragment "out" arguments count,
					  // and all of them must be "inout"
					  // By default, no functions are called
					  const std::vector<std::string>& fragment_modifiers  =  {});
	
		void set_unicolor(const glm::vec4& color);
		void set_multicolor(const std::vector<glm::vec4>& colors);

		void ortho_square(const glm::vec3& p1, const glm::vec3& p2);
		void ortho_square(const glm::vec3& center, float radius);
		void sphere(const glm::vec3& center, float radius);
		void triangle(const glm::vec3& p1, const glm::vec3& p2, const glm::vec3& p3);

private:

	static constexpr int MAX_SHAPE_INDICES = 8;

	ArrayBuffer<glm::vec4> _colors;
	ArrayBuffer<glm::vec3> _triangle;
	ArrayBuffer<glm::vec3> _square;

	ElementBuffer _square_indices;

	};
};

#endif //_INSTANT_DRAWER_HPP_
