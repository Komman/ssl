#ifndef _FRAME_BUFFER_HPP_
#define _FRAME_BUFFER_HPP_

#include "drawable_frame_buffer.hpp"
#include "uniform_texture_2D.hpp"
#include "depth_buffer.hpp"
#include "depth_stencil_buffer.hpp"

namespace ssl
{
	template<typename... RenderTypes>
	class FrameBuffer : public DrawableFrameBuffer<RenderTypes...>
	{
		public:
			FrameBuffer(const UniformTexture2D<RenderTypes> * ... uniform_textures);
			FrameBuffer(const DepthBuffer* depth_buffer, const UniformTexture2D<RenderTypes> * ... uniform_textures);
			FrameBuffer(const DepthStencilBuffer* depth_buffer, const UniformTexture2D<RenderTypes> * ... uniform_textures);

		protected:
			//FrameBuffer(){}
			FrameBuffer(const FrameBuffer& f){}
			FrameBuffer& operator=(const FrameBuffer&){}
			FrameBuffer& operator=(FrameBuffer&& f){}

		private:

	};
};

namespace ssl
{
	template<typename... RenderTypes>
	FrameBuffer<RenderTypes...>::FrameBuffer(const UniformTexture2D<RenderTypes> * ... uniform_textures)
		: DrawableFrameBuffer<RenderTypes...>(uniform_textures...)
	{

	}
	template<typename... RenderTypes>
	FrameBuffer<RenderTypes...>::FrameBuffer(const DepthBuffer* depth_buffer, const UniformTexture2D<RenderTypes> * ... uniform_textures)
		: DrawableFrameBuffer<RenderTypes...>(depth_buffer, uniform_textures...)
	{

	}
	template<typename... RenderTypes>
	FrameBuffer<RenderTypes...>::FrameBuffer(const DepthStencilBuffer* depth_stencil_buffer, const UniformTexture2D<RenderTypes> * ... uniform_textures)
		: DrawableFrameBuffer<RenderTypes...>(depth_stencil_buffer, uniform_textures...)
	{

	}
};

#endif //_FRAME_BUFFER_HPP_
