#ifndef _VIEW_HPP_
#define _VIEW_HPP_

#include <glm/glm.hpp>

namespace ssl
{
	namespace DEFAULT
	{
		constexpr float VIEW_FIELD = 70.0f * 3.1415926535f/180.0f; //view field in degrees
	};

	class View
	{
	public:
		View(const glm::vec3& initial_position, float min_view_distance, float max_view_distance, float view_field = DEFAULT::VIEW_FIELD);

		void look_at(const glm::vec3& look_at_position);
		void tp(const glm::vec3& new_position);
		void horizontal_rotate(float angle); // angle in radian
		void vertical_rotate(float angle);   // angle in radian
		void rotate(const glm::vec2& angles);// angles.x : horizontal angle in randian
								             // angles.y : vertical angle in randian

		const glm::mat4& get_MVP()          const;
		const glm::vec3& get_position()     const;
		const glm::vec3& get_direction()    const;
		const glm::vec3& get_right_vector() const;
		const glm::vec3& get_up_head()      const;	

		float get_view_field()        const;
		float get_min_view_distance() const;
		float get_max_view_distance() const;

	private:
		void compute_right_vector();
		glm::mat4 compute_MVP() const;
		
		const glm::mat4 _model;
		const glm::mat4 _projection;
		mutable glm::mat4 _MVP;

		glm::vec3 _position;	
		glm::vec3 _direction;
		glm::vec3 _right_vector;
		glm::vec3 _up_head;

		float _view_field;
		float _min_view_distance;
		float _max_view_distance;

		mutable bool _view_changed;
	};
};


#endif //_VIEW_HPP_
