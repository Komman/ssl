#ifndef _CONTROL_CAMERA_HPP_
#define _CONTROL_CAMERA_HPP_

#include <vector>
#include <glm/glm.hpp> 

#include "basic_camera.hpp"

namespace ssl
{
	class ControlCamera : public BasicCamera
	{
	public:
		ControlCamera(const glm::vec3& initial_position, float min_view_distance, float max_view_distance, const std::vector<int>& control_keys, float view_field = DEFAULT::VIEW_FIELD);

		void frame_update();
		void set_new_control_keys(const std::vector<int>& control_keys);

	protected:
		// pressed_keys is the vector of indices of every
		// key pressed in the control_keys vector
		virtual void keys_reaction(const std::vector<unsigned int>& pressed_keys_indices)=0;
		// mouse_moving_pixel is the move of the mouse
		virtual void mouse_reaction(const glm::vec2& mouse_moving_pixel)=0;  

		virtual void frame_update_more_actions(){}

	private:
		std::vector<int> _control_keys;
		glm::dvec2       _set_mouse_pixel;
		bool             _block_mouse;
	};
};

#endif //_CONTROL_CAMERA_HPP_
