#include "shader_pre_builder.hpp"


using namespace std;

namespace ssl
{
	map<string, uniformInfo> ShaderPreBuilder::uniform_infos;

	static vector<string> textures_types={
		"sampler1D",
		"isampler1D",
		"usampler1D",
		"sampler2D",
		"isampler2D",
		"usampler2D",
		"sampler3D",
		"isampler3D",
		"usampler3D",

		"image1D",
		"iimage1D",
		"uimage1D",
		"image2D",
		"iimage2D",
		"uimage2D",
		"image3D",
		"iimage3D",
		"uimage3D",

		"sampler2DMS",
		"isampler2DMS",
		"usampler2DMS"
	};


	ShaderPreBuilder::ShaderPreBuilder(const BasicShaderParser& parser)
		: BasicShader(),
		  _parser(&parser)
	{
		if(!_parser->is_sourced())
		{
			err("The generator used to create the shader has been freed");
			// err("Shader source file must be sourced\n" 
				// + TERM::PURPLE + ">>use:" 
				// + TERM::GREEN  + " Shader"
				// + TERM::WHITE  + "::" 
				// + TERM::CYAN   + "source" 
				// + TERM::WHITE  + "("
				// + TERM::RED    + "const"
				// + TERM::WHITE  + " std::string& path_to_source_shader)");
		}
	}

	void ShaderPreBuilder::fill_dependency(const ShaderParser::functionInfo& start_info,
										   vector<string>& vars,
										   vector<string>& funcs,
										   vector<string>& uniforms)
	{
		for(auto& loc_var : start_info.local_vars)
		{
			if(!utils::in_vector(vars, loc_var))
				vars.push_back(loc_var);
		}

		for(auto& unif : start_info.uniforms)
		{
			if(!utils::in_vector(uniforms, unif))
				uniforms.push_back(unif);
		}

		for(auto& func : start_info.local_funcs)
		{
			if(!utils::in_vector(funcs, func))
			{
				funcs.push_back(func);
				fill_dependency(_parser->get_function(func), vars, funcs, uniforms);
			}			
		}
	}

	void ShaderPreBuilder::arrange_code(const string& in, string& out)
	{
		out.clear();
		out.reserve(in.size());
		string tab="\t";

		if(in.size()>0 && in[0] == ' ')
			out+='\t';

		for(unsigned int i = 0; i<in.size(); i++)
		{
			if(i<in.size() && utils::is_string_detected(in, i, ShaderParser::uniform_key))
				i+=ShaderParser::uniform_key.size();

			if(i<in.size() && utils::is_string_detected(in, i, ShaderParser::local_key))
				i+=ShaderParser::local_key.size();

			if(utils::is_string_detected(in, i, "\n "))
			{
				i+=1;
				out+='\n';
				out+=tab;
				continue;
			}

			if(utils::is_string_detected(in, i, "\n{\n"))
			{
				i+=2;
				out+='{';
				out+=tab;
				tab+='\t';
				continue;
			}
			
			if(utils::is_string_detected(in, i, "\n}\n") && out.size()>0)
			{
				i+=2;
				out[out.size()-1]='}';
				tab.pop_back();
				continue;
			}

			if(utils::is_string_detected(in, i, "\n\n"))
				continue;

			out+=in[i];
		}
	}

	void ShaderPreBuilder::write_local_function(std::ofstream& file, const ShaderParser::functionInfo& infos)
	{
		write_local_function_head(file, infos);

		file << "\n{\n";

		string clean_code;
		arrange_code(infos.function_core, clean_code);
		file << clean_code;

		file << "}" << endl;

	}

	void ShaderPreBuilder::write_local_function_head(std::ofstream& file, const ShaderParser::functionInfo& infos)
	{
		file << infos.function_type << " " << infos.function_name << "(";

		for(unsigned int i=0; i<infos.args.size(); i++)
		{
			auto& arg = infos.args[i];

			if(i!=0)
			{
				file << "\t";
			}

			if(arg.inout != "")
			{
				if(arg.inout == BasicShaderParser::instance_key)
				{
					file << "in ";
				}
				else
				{
					file << arg.inout << " ";
				}
			}

			file << arg.type << " "  << arg.name << ShaderParser::array_ext(arg.array_count);

			if(i != infos.args.size()-1)
			{
				file << "," << endl;
			}
			
		}
		file << ")";
	}

	void ShaderPreBuilder::write_local_function_declaration(std::ofstream& file, const ShaderParser::functionInfo& infos)
	{
		write_local_function_head(file, infos);
		file<<";"<<endl;
	}

	void ShaderPreBuilder::declare_uniform(const uniformInfo& infos)
	{
		#ifdef SSL_DEBUG
		if(uniform_infos.find(infos.name) != uniform_infos.end())
		{
			err("Uniform variable : " + TERM::BLUE + infos.str_type + " " + infos.name + ERR_COLOR + " already declared");
		}
		#endif

		uniform_infos[infos.name] = infos;
	}

	bool ShaderPreBuilder::is_uniform_texture(const std::string& uniform_name)
	{
		auto unif_it = uniform_infos.find(uniform_name);

		if(unif_it == uniform_infos.end())
		{
			err("bool is_uniform_texture(): requiring the uniform \"" + TERM::BLUE + uniform_name + ERR_COLOR + "\" that is not declared");
		}

		const string& type = unif_it->second.str_type;

		// Can be optimized
		if(std::find(textures_types.begin(), textures_types.end(), type) != textures_types.end())
		{
			return true;
		}


		return false;
	}

	uniformInfo ShaderPreBuilder::get_uniform_info(const std::string& uniform_name)
	{
		auto it = uniform_infos.find(uniform_name);

		#ifdef SSL_DEBUG
		if(it == uniform_infos.end())
		{
			err("Asked for an uniform that does not exist: uniform variable \"" + TERM::BLUE + uniform_name + ERR_COLOR + "\" undeclared");
		}
		#endif

		return it->second;
	}
	
	bool ShaderPreBuilder::is_uniform_declared(const std::string& uniform_name)
	{
		auto it = uniform_infos.find(uniform_name);

		if(it == uniform_infos.end())
		{
			return false;
		}

		return true;
	}



	void ShaderPreBuilder::write_uniforms(std::ofstream& file, const std::vector<std::string>& _uniforms)
	{
		for(auto&  unif : _uniforms)
		{
			if(uniform_infos.find(unif) == uniform_infos.end())
			{
				err("Uniform variable: " + TERM::BLUE + unif + ERR_COLOR + " undeclared");
			}
			file << uniform_infos[unif].layout << "uniform " << uniform_infos[unif].str_type << " " << unif << ShaderParser::array_ext(uniform_infos[unif].array_count) << ";" << endl;
		}
	}

	void ShaderPreBuilder::write_vars(std::ofstream& file, const std::vector<std::string>& vars)
	{
		for(int i=vars.size()-1; i>=0; i--)
		{
			auto& loc_var = vars[i];
			vector<string> var_dec_lines = _parser->get_var_dec(loc_var);

			for(auto& line : var_dec_lines)
				file << line << endl;
		}
	}

	void ShaderPreBuilder::write_funcs(std::ofstream& file, const std::vector<std::string>& funcs)
	{
		for(int i=funcs.size()-1; i>=0; i--)
		{
			auto& func = funcs[i];
			write_local_function_declaration(file, _parser->get_function(func));
			file << endl;
		}
		for(int i=funcs.size()-1; i>=0; i--)
		{
			auto& func = funcs[i];
			write_local_function(file, _parser->get_function(func));
		}
	}
};

