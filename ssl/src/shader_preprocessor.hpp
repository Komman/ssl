#ifndef _SHADER_PREPROCESSOR_HPP_
#define _SHADER_PREPROCESSOR_HPP_

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <unordered_set>

namespace ssl
{
	namespace str_operations
	{
		inline bool is_string_detected(const std::string& line, unsigned int indice, const std::string& to_detect);
	};

	using uniformRenaming = std::pair<std::string, std::string>;

	class ShaderPreprocessor
	{
	public:
		// Rename all uniform of the name of the first pair member
		// by the name of the second pair member

	public:
		ShaderPreprocessor(const std::string& main_file_path);
		ShaderPreprocessor(const std::string& main_file_path,
						   const std::vector<uniformRenaming>& uniform_renaming);

		void compile(const std::string& out_file_path);

		inline static const std::string KEY_INCLUDE   = "#include ";
		
		inline static const std::string line_comment  = "//";
		inline static const std::string comment_begin = "/*";
		inline static const std::string comment_end   = "*/";
		
		static inline std::string separators = " \t";

		static std::string glsl_version();

	private:
		void global_preprocess(std::ifstream& in_file, std::ofstream& out_file);
		void preprocess(std::ifstream& in_file, std::ofstream& out_file);
		
		// Write the perfect line on "perfect_line"
		// syntax is perfect when :
		//  - there is no comments
		//  - there is no dobble spaces or dobble tab
		//  - there is a '\n' BEFORE AND AFTER every '{' and '}'
		//  - every bracket ']' closure is not preceded by a space, tab or '\n'
		//  - every uniform name is replaced by its new name (uniform_renaming argument in constructor) 
		void perfect_line_syntax(const std::string& line, std::string& perfect_line);
		
		void preprocess_line(std::ofstream& out_file, const std::string& line);
		void include(std::ofstream& out_file, const std::string& included_file_path, const std::string& include_line);

private:
		std::string _in_file_name;
		std::string _in_file_directory;
		bool _is_commented_line;

		std::unordered_set<std::string> _already_included_files;

		std::vector<uniformRenaming> _uniform_renaming;
	};
};

#endif //_SHADER_PREPROCESSOR_HPP_
