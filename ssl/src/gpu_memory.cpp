#include "gpu_memory.hpp"

#include "debug.hpp"

namespace ssl
{
	void memory_barrier(GLbitfield type)
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_DRAW))
		{
			ssl::prints_msg(SSL_PRINTS_DRAW, "===== MEMORY BARRIER ====");
		}
		#endif

		GL(glMemoryBarrier(type));
	}
	
};

