#ifndef _VVV_HPP_
#define _VVV_HPP_

#include <glm/glm.hpp> 

namespace ssl
{
	class vvv
	{
	public:
		static const glm::vec3& x();
		static const glm::vec3& y();
		static const glm::vec3& z();
		static const glm::vec3& xy();
		static const glm::vec3& yz();
		static const glm::vec3& xz();
		static const glm::vec3& xyz();

	private:
		vvv();

	};
};

#endif //_VVV_HPP_
