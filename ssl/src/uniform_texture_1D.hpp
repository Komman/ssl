#ifndef _UNIFORM_TEXTURE_1D_HPP_
#define _UNIFORM_TEXTURE_1D_HPP_

#include "uniform_texture.hpp"
#include "texture1D.hpp"

namespace ssl
{
	template<typename Type>
	class UniformTexture1D : public UniformTexture<Type, Texture1D<Type>>
	{
	public:
		UniformTexture1D(const std::string& name,
						 unsigned int  size,
						 const std::vector<Type>& data,
						 GLint minmag_filter,
						 GLint wrap          = DEFAULT::TEXTURE_WRAP);

		UniformTexture1D(const std::string& name,
						 unsigned int  size,
						 GLint minmag_filter,
						 GLint wrap          = DEFAULT::TEXTURE_WRAP)
			: UniformTexture1D<Type>(name, size, std::vector<Type>(0), minmag_filter, wrap) {}

		UniformTexture1D(const std::string& name,
						 unsigned int  size,
						 const std::vector<Type>& data = std::vector<Type>(0))
			: UniformTexture1D<Type>(name, size, data, DEFAULT::MINMAG_FILTER, DEFAULT::TEXTURE_WRAP) {}

		virtual ~UniformTexture1D() {}

		unsigned int size() const;

		void change_data(const std::vector<Type>& data);

	};
};

namespace ssl
{
	template<typename Type>
	UniformTexture1D<Type>::UniformTexture1D(const std::string& name,
						 			   		 unsigned int  size,
						 			   		 const std::vector<Type>& data,
											 GLint minmag_filter,
											 GLint wrap)
		: UniformTexture<Type, Texture1D<Type>>(name, glm::uvec3(size, 1, 1), data.size() == 0 ? NULL : (void*) data.data(), minmag_filter, wrap)
	{
		if(data.size() != 0 && data.size() < size)
		{
			err("try to build a Texture1D of a size of " + std::to_string(size) 
				+ " but the data vector passed has a size of " + std::to_string(data.size()));
		}
	}

	template<typename Type>
	void UniformTexture1D<Type>::change_data(const std::vector<Type>& data)
	{
		if(data.size() > this->size())
		{
			err("UniformTexture1D<Type>::change_data(): try to put data out of bound: size of the data"
				+ std::to_string(data.size()) + ", and size of the texture: " + std::to_string(this->size()));
		}

		this->UniformTexture<Type, Texture1D<Type>>::change_data(glm::uvec3(data.size(), 1, 1), (void*)data.data());
	}


	template<typename Type>
	unsigned int UniformTexture1D<Type>::size() const
	{
		return this->get_dimension_size().x;
	}
};

#endif //_UNIFORM_TEXTURE_1D_HPP_
