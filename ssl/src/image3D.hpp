#ifndef _IMAGE3D_HPP_
#define _IMAGE3D_HPP_

#include "texture3D.hpp"

namespace ssl
{
	template<typename Type>
	class Image3D : public Texture3D<Type>
	{
	public:
		Image3D(const glm::uvec3&  size,
				void*              data          = NULL,
				GLint              minmag_filter = DEFAULT::MINMAG_FILTER,
				GLint              wrap          = DEFAULT::TEXTURE_WRAP);

		void bind() const override;

	protected:

	private:

	};
};


namespace ssl
{
	template<typename Type>
	Image3D<Type>::Image3D(const glm::uvec3& size,
						   void* data,
						   GLint minmag_filter,
						   GLint wrap)
		: Texture3D<Type>(size, data, minmag_filter, wrap)
	{

	}

	template<typename Type>
	void Image3D<Type>::bind() const
	{
		GL(glBindImageTexture(0, this->get_ID(), 0, GL_FALSE, 0, GL_READ_WRITE, this->get_GL_internal_format()));
	}
};

#endif //_IMAGE3D_HPP_
