#ifndef _DRAWABLE_OBJECT_HPP_
#define _DRAWABLE_OBJECT_HPP_

#include "basic_buffer.hpp"
#include "element_buffer.hpp"
#include "vertex_array_object.hpp"

#include <vector>
#include <limits>

namespace ssl
{
	class BasicDrawableObject
	{
	public:

		virtual GLenum  get_drawing_mode()   const {return GL_TRIANGLES;}
		virtual GLsizei get_draw_max_index() const {return std::numeric_limits<GLsizei>::max();}
	
		virtual VertexArrayObject compute_vao() const =0;
	};

	class DrawableObject : public BasicDrawableObject
	{
	public:
		virtual const std::vector<const BasicBuffer*>& get_buffers() const=0;
	
		virtual VertexArrayObject compute_vao() const override;		
	};

	class ElementDrawableObject : public BasicDrawableObject
	{
	public:
		virtual const std::vector<const BasicBuffer*>& get_buffers()  const=0;
		virtual const ElementBuffer&                   get_elements() const=0;
		
		virtual VertexArrayObject compute_vao() const override;		
	};
};

#endif //_DRAWABLE_OBJECT_HPP_
