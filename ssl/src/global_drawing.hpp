#ifndef _DRAWING_HPP_
#define _DRAWING_HPP_

#include "basic_frame_buffer.hpp"

namespace ssl
{
	namespace global_drawing
	{
		namespace DEFAULT
		{
			constexpr GLenum SFACTOR    = GL_SRC_ALPHA;
			constexpr GLenum DFACTOR    = GL_ONE_MINUS_SRC_ALPHA;
			constexpr GLenum BLEND_EQ   = GL_FUNC_ADD;
			constexpr GLenum DEPTH_FUNC = GL_LESS;
		};

		void depth(bool enable);
		void depth_function(GLenum func);
		void clear_color(const glm::vec4& color);
		void culling(bool enable);
		void blending(bool enable);
		void antialiasing(bool enable);
		void bind_framebuffer(const BasicFrameBuffer* framebuffer);
		void bind_screen();
		void target(const BasicFrameBuffer* framebuffer_ot_target);
		void set_view_port(const glm::uvec2& origin, const glm::uvec2& size);
		void target_screen();
		//see openGL::glBlendFunc function
		void set_blend_function(GLenum sfactor = DEFAULT::SFACTOR,
								GLenum dfactor = DEFAULT::DFACTOR);
		void set_blend_equation(GLenum mode);
		void depth_mask(bool enable);
		void stencil_testing(bool enable);
		void stencil_mask(GLuint mask); // 8 bits
		void stencil_function(GLenum func, GLint ref, GLuint mask);
		void stencil_op(GLenum sfail, GLenum dpfail, GLenum dppass);

		const glm::vec4& get_clear_color();


		// used by ssl
		void clear(GLbitfield mask = ssl::DEFAULT::CLEAR_MASK);
		void init();
		void free();
		void bind_default_vao();
	};
};

		


#endif //_DRAWING_HPP_
