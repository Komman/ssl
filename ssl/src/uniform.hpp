
#ifndef _UNIFORM_HPP_
#define _UNIFORM_HPP_

#include "typed_uniform.hpp"
#include "uniform_slot.hpp"
#include "basic_uniform_slot.hpp"

#include <string>
#include <vector>
#include <glm/glm.hpp> 

namespace ssl
{
	/*
		ONLY SUPORT GLSL TYPES
		DO NOT SUPPOR DOUBLE PRECISION (double, dvec2/3/4, dmat4, dmat3 and dmat2)
	*/

	template<typename Type>
	class Uniform : public TypedUniform<Type>
	{
	public:

		//class
		Uniform(const std::string& name,
				const Type& value);
		virtual ~Uniform() {}
		
		void change_value(const Type& value);
		
		const Type& get_value() const;

		Uniform<Type>& operator=(const Type& value);
		Uniform<Type>& operator=(const Uniform<Type>& u);
		Uniform<Type>& operator+=(const Type& value);
		Uniform<Type>& operator+=(const Uniform<Type>& u);
		Uniform<Type>& operator-=(const Type& value);
		Uniform<Type>& operator-=(const Uniform<Type>& u);
		Uniform<Type>& operator*=(const Type& value);
		Uniform<Type>& operator*=(const Uniform<Type>& u);
		Uniform<Type>& operator/=(const Type& value);
		Uniform<Type>& operator/=(const Uniform<Type>& u);

		operator const Type&() const;

	protected:
		Uniform(){}
		Uniform(const Uniform<Type>& u){}
		Uniform(Uniform<Type>&& u){}
		Uniform<Type>& operator=(Uniform<Type>&& u){}

		BasicUniformSlot* get_instance();

	private:
		UniformSlot<Type> _slot;
	};

};


// === TEMPLATIZED IMPLEMENTATION === 


#define SSL_APPLY_MACRO(M) M

#define SSL_UNIFORM_OPERATOR_OP_AFFECT_DECLARATION(optor, op)\
	template<typename Type>\
	Uniform<Type>& Uniform<Type>::SSL_APPLY_MACRO(operator)optor(const Type& value)\
	{\
			this->change_value(this->get_value() op value);\
			return *this;\
		}\
	template<typename Type>\
	Uniform<Type>& Uniform<Type>::SSL_APPLY_MACRO(operator)optor(const Uniform<Type>& u)\
	{\
			this->change_value(this->get_value() op u.get_value());\
			return *this;\
	}

namespace ssl
{	
	template<typename Type>
	Uniform<Type>::Uniform(const std::string& name,
						   const Type& value)
		: 	TypedUniform<Type>(name, BasicUniform::SINGLE_ELEMENT),
			_slot(name, std::vector<Type>(1, value))
	{
		this->set_slot();
	}

	template<typename Type>
	BasicUniformSlot* Uniform<Type>::get_instance()
	{
		return &_slot;
	}

	template<typename Type>
	void Uniform<Type>::change_value(const Type& value)
	{
		_slot.change_subvalue(0, value);
	}

	template<typename Type>
	const Type& Uniform<Type>::get_value() const
	{
		#ifdef SSL_DEBUG
		if(_slot.get_value().size() != 1)
		{
			err("Big problem occured here : " + SSL_FILE_AND_LINE);
		}
		#endif

		return _slot.get_value()[0];
	}

	template<typename Type>
	Uniform<Type>& Uniform<Type>::operator=(const Type& value)
	{
		this->change_value(value);
		return *this;
	}

	template<typename Type>
	Uniform<Type>& Uniform<Type>::operator=(const Uniform<Type>& u)
	{
		this->change_value(u.get_value());
		return *this;
	}

	SSL_UNIFORM_OPERATOR_OP_AFFECT_DECLARATION(+=, +)
	SSL_UNIFORM_OPERATOR_OP_AFFECT_DECLARATION(-=, -)
	SSL_UNIFORM_OPERATOR_OP_AFFECT_DECLARATION(*=, *)
	SSL_UNIFORM_OPERATOR_OP_AFFECT_DECLARATION(/=, /)

	template<typename Type>
	Uniform<Type>::operator const Type&() const
	{
		return this->get_value();
	}
};

#endif //_UNIFORM_HPP_
