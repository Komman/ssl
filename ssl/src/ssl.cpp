#include "../ssl.hpp"

#include "debug.hpp"
#include "basic_window.hpp"

namespace ssl
{

	static void initialize_ssl()
	{
		keyboard::init();
		global_drawing::init();
		buffer_builder::init();
	}

	void init(uint size_x, uint size_y, const std::string& name)
	{
		BasicWindow::create_context();	
		window::create(size_x, size_y, name);
		initialize_ssl();
	}
	void init(const std::string& name)
	{
		BasicWindow::create_context();	
		window::create(name);
		initialize_ssl();
	}
	void init(const std::string& name, unsigned int window_divisor)
	{
		BasicWindow::create_context();	
		window::create(name, window_divisor);
		initialize_ssl();
	}
	void close()
	{
		global_drawing::free();
		buffer_builder::free();
		window::close();
		BasicWindow::destroy_context();	
	}
};