#include "drawable_object.hpp"

namespace ssl
{
	VertexArrayObject DrawableObject::compute_vao() const
	{
		return VertexArrayObject(this->get_buffers());
	}	

	VertexArrayObject ElementDrawableObject::compute_vao() const
	{
		return VertexArrayObject(this->get_buffers(), &(this->get_elements()));
	}		
};

