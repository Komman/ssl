#include "gl_recorder.hpp"

#include <GL/glew.h>
#include "utils.hpp"

namespace ssl
{
	GlRecorder::GlRecorder()
		: _nanosec(0)
	{
		
	}
		
	void GlRecorder::start()
	{
		glFinish();
		_nanosec = utils::get_nanoseconds();
	}

	void GlRecorder::stop()
	{
		glFinish();
		_nanosec = utils::get_nanoseconds() - _nanosec;
	}

	uint64_t GlRecorder::get_last_duration()
	{
		return _nanosec;
	}
};

