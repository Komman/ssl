#include "labeled_recorder.hpp"

#include "utils.hpp"
#include "debug.hpp"

#include <GL/glew.h>

using namespace ssl;
using namespace std;

namespace ssl
{
	LabeledRecorder::LabeledRecorder()
		: _recording(true)
	{

	}

	void LabeledRecorder::release()
	{
		_recording = false;
	}
	void LabeledRecorder::record()
	{
		_recording = true;
	}

	void LabeledRecorder::start(const std::string& name)
	{
		if(!_recording) return;

		_times[name] = utils::get_nanoseconds();
		if(_durations.find(name) == _durations.end())
		{
			_durations[name] = 0;
		}	
	}

	void LabeledRecorder::reset_durations()
	{
		if(!_recording) return;

		for(auto& d : _durations)
		{
			d.second = 0;
		}
	}


	void LabeledRecorder::stop(const std::string& name)
	{
		if(!_recording) return;

		//TOOPT
		auto t = utils::get_nanoseconds();

		_durations[name] += (t - _times[name]);
		_times[name] = t;
	}

	void LabeledRecorder::gl_start(const std::string& name)
	{
		if(!_recording) return;
		glFinish();
		this->start(name);
	}

	void LabeledRecorder::gl_stop(const std::string& name)
	{
		if(!_recording) return;
		glFinish();
		this->stop(name);
	}

	void LabeledRecorder::print() const
	{
		this->print_custom("  ");
	}
	
	void LabeledRecorder::print_list() const
	{
		this->print_custom("\n");
	}

	void LabeledRecorder::print_custom(const std::string& poststring) const
	{
		if(!_recording)
		{
			// cout<<"{";
			// for(const auto& r : _durations)
			// {
				// cout<<r.first<<TERM::NOCOL<<", ";
			// }
			// cout<<"} : "<<TERM::RED<<"PAUSED"<<TERM::NOCOL<<endl;
		}
		else
		{
			uint64_t tot = 0;
			cout<<std::setprecision(2)<< std::fixed;
			for(const auto& r : _durations)
			{
				tot+=r.second;
				cout<<TERM::CYAN<<r.first<<": "<<TERM::NOCOL<<float(r.second)/1000000.0<<poststring;
			}
			cout<<TERM::RED<<"TOTAL: "<<TERM::NOCOL<<float(tot)/1000000.0<<"  ( ms )";
			cout<<endl;
		}
	}

	bool LabeledRecorder::is_recording() const
	{
		return _recording;
	}
}