#ifndef _COMPUTE_SHADER_HPP_
#define _COMPUTE_SHADER_HPP_

#include "compute_shader_builder.hpp"
#include "basic_uniform.hpp"

namespace ssl
{
	class ComputeShader : public ComputeShaderBuilder
	{
	public:
		ComputeShader(const BasicShaderParser& parser,
					  const std::string& compute_name, 
					  const glm::uvec3&  local_size);

		ComputeShader(const BasicShaderParser& parser,
					  const std::string& compute_name, 
					  const glm::uvec2&  local_size)
			: ComputeShader(parser, compute_name, glm::uvec3(local_size.x, local_size.y, 1)) {}

		ComputeShader(const BasicShaderParser& parser,
					  const std::string& compute_name, 
					  unsigned int       local_size)
			: ComputeShader(parser, compute_name, glm::uvec3(local_size, 1, 1)) {}


		void dispatch(const glm::uvec3&  block_count);
		void dispatch(const glm::uvec2&  block_count){this->dispatch(glm::uvec3(block_count.x, block_count.y, 1));}
		void dispatch(unsigned int       block_count){this->dispatch(glm::uvec3(block_count  , 1            , 1));}

	protected:
	
		std::vector<BasicUniform::reference> _uniform_used;

	private:

		void update_uniforms();

	};
};

#endif //_COMPUTE_SHADER_HPP_
