#include "instant_drawer.hpp"
#include "color.hpp"

using namespace glm;

namespace ssl
{
	InstantDrawer::InstantDrawer(const BasicShaderParser& parser,
								  const std::string& vertex_shader_name,
								  const std::string& fragment_shader_name,
								  // Functions called on th fragment result, they must have
								  // the same argument count as fragment "out" arguments count,
								  // and all of them must be "inout"
								  // By default, no functions are called
								  const std::vector<std::string>& fragment_modifiers)
		: Drawer(parser, vertex_shader_name, fragment_shader_name, fragment_modifiers),
		  _colors(std::vector<vec4>(MAX_SHAPE_INDICES, vec4(RED, 1.0)), DYNAMIC_DRAW),
		  _triangle(std::vector<vec3>(3, vec3(0.0)), DYNAMIC_DRAW),
		  _square(std::vector<vec3>(8, vec3(0.0)), DYNAMIC_DRAW),
		  _square_indices({1,0,2, 1,2,7, 0,3,5, 0,5,2, 3,6,4, 3,4,5, 6,1,7, 6,7,4, 5,4,7, 5,7,2, 0,1,6, 0,6,3}, DYNAMIC_DRAW)
	{

	}
	
	void InstantDrawer::set_unicolor(const glm::vec4& color)
	{		
		for(uint i=0;i<_colors.size();i++)
		{
			_colors.change_subvalue(i, color);
		}
	}

	void InstantDrawer::set_multicolor(const std::vector<glm::vec4>& colors)
	{
		for(uint i=0;i<_colors.size();i++)
		{
			_colors.change_subvalue(i, colors[ i % colors.size() ]);
		}
	}

	void InstantDrawer::ortho_square(const glm::vec3& p1, const glm::vec3& p2)
	{
		vec3 p1p2 = p2 - p1;

		_square.change_value({
			p1+vec3(0.0,0.0,0.0),
			p1+vec3(p1p2.x,0.0,0.0),
			p1+vec3(0.0,p1p2.y,0.0),
			p1+vec3(0.0,0.0,p1p2.z),
			p2-vec3(0.0,0.0,0.0),
			p2-vec3(p1p2.x,0.0,0.0),
			p2-vec3(0.0,p1p2.y,0.0),
			p2-vec3(0.0,0.0,p1p2.z)
		});

		this->draw_elements(_square_indices, _square, _colors);
	}

	void InstantDrawer::ortho_square(const glm::vec3& center, float radius)
	{
		this->ortho_square(center - vec3(radius)/2.0f, center + vec3(radius)/2.0f);
	}

	void InstantDrawer::sphere(const glm::vec3& center, float radius)
	{
		err("todo");
	}

	void InstantDrawer::triangle(const glm::vec3& p1, const glm::vec3& p2, const glm::vec3& p3)
	{
		_triangle.change_value({p1, p2, p3});

		this->draw(_triangle, _colors);
	}
}

