#ifndef _SHADER_GENERATOR_HPP_
#define _SHADER_GENERATOR_HPP_

#include "shader_parser.hpp"
#include "shader_preprocessor.hpp"

namespace ssl
{

	namespace DEFAULT
	{
		constexpr bool REMOVE_GENERATED_SHADERS = false;
	};

	/*
			/ ! \   NOT THREAD SAFE   / ! \  
	*/

	class ShaderGenerator : public ShaderParser
	{
	public:

		ShaderGenerator(const std::string& shader_file_path);
		ShaderGenerator(const std::string& shader_file_path,
						const std::vector<uniformRenaming>& uniform_renaming);

		virtual ~ShaderGenerator();

	private:

		static std::string generate_global_shader(const std::string& shader_file_path,
												  uint generated_id,
												  const std::vector<uniformRenaming>& uniform_renaming);

		static uint generated_id;

	private:
		
		std::string _folder_path;

	};
};

#endif //_SHADER_GENERATOR_HPP_
