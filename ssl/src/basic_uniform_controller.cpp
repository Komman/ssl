#include "basic_uniform_controller.hpp"
#include "debug.hpp"

#include <iostream>

using namespace std;

namespace ssl
{

	BasicUniformController::BasicUniformController(const std::string& name, GLuint shader_ID)
		: _updated(false)
	{
		this->link_to_shader(name, shader_ID);
	}

	GLuint BasicUniformController::get_ID() const
	{
		return _ID;
	}

	void BasicUniformController::up_to_date(bool updated)
	{
		_updated = updated;
	}

	bool BasicUniformController::is_up_to_date()
	{
		return _updated;
	}

	void BasicUniformController::link_to_shader(const std::string& name, GLuint shader_ID)
	{
		GL(_ID = glGetUniformLocation(shader_ID, name.c_str()));
	}

	void BasicUniformController::send_data_to_GPU(void* data, int array_count) const
	{
		this->send_typed_data_to_GPU(data, array_count);
	}

	void BasicUniformController::print() const
	{
		cout<<"(up:"<<to_string(_updated)<<", shid:"<<_ID<<")";
	}
};

