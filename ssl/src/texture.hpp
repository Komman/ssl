#ifndef _TEXTURE_HPP_
#define _TEXTURE_HPP_

#include <GL/glew.h>
#include <string>
#include <glm/glm.hpp>

#include "debug.hpp"
#include "utils.hpp"

namespace ssl
{
	namespace DEFAULT
	{
		// GL_NEAREST, GL_LINEAR
		constexpr GLint MINMAG_FILTER = GL_NEAREST;
		// GL_REPEAT , GL_MIRRORED_REPEAT, GL_CLAMP_TO_BORDER 
		constexpr GLint TEXTURE_WRAP  = GL_CLAMP_TO_BORDER; 
	};

	enum attachementType {COLOR_ATTACHEMENT, DEPTH_ATTACHMENT};

	using DepthAttachement = float;
	/* 
		Availables types:
	
		- float
		- vec2
		- vec3
		- vec4
		- int
		- ivec2
		- ivec3
		- ivec4
		- unsigned int
		- uvec2
		- uvec3
		- uvec4

		/!\  FLOATING POINT TYPES ARE SNORMED /!\

	*/

	template<typename Type>
	class Texture
	{
	public:
		//TODO: Can be fixed with function pointer, but currently :
		// EVRY TEXTURE THAT HERITS OF THIS CLASS MUST CALL TO Texture<Type>::initialize();
		Texture(const glm::uvec3& dimension_size,
				GLint             minmag_filter,
				GLint             wrap);
		~Texture();

		Texture(Texture&& tex);
		Texture& operator=(Texture&& tex);

		void copy(const Texture<Type>& tex);

		GLint get_minmag_filter() const;
		GLint get_wrap() const;

		virtual void      bind()                const;
		unsigned int      get_ID()              const;
		const glm::uvec3& get_dimension_size()  const;
		// Returns products dimensions size
		unsigned int      get_elements_amount() const;

		// Bind and attach
		void   attach_on_framebuffer(unsigned int attachement_index) const;

		void change_data(const glm::uvec3& dimension_size, void* data);
		virtual GLenum get_gl_texture_type() const =0;
		virtual attachementType get_attachement_type() const =0;

		void bind_on_texture_location(unsigned int texture_binding);

		std::vector<Type> download_data() const;
		void download_data(void* dst) const;

	protected:
		virtual void definitive_resize_GPU(const glm::uvec3& new_dimension_size) const =0;
		virtual void change_data_if_bound(const glm::uvec3& dimension_size, void* data)=0;
		virtual void tex_image_if_bound(const glm::uvec3& dimension_size, void* data)   const =0;
		virtual bool mipmaps_enable()                                       const =0;
		virtual void attach_on_framebuffer_if_bound(unsigned int attachement_index) const =0;

		virtual bool wrap_enable() const;
	

		void initialize(void* data);

		static GLint  get_GL_internal_format();
		static GLint  get_GL_valid_format();
		static GLenum get_GL_data_type();


	private:
		Texture(GLint minmag_filter, GLint wrap);

		static constexpr unsigned int ID_NO_TEXTURE = 0;

		Texture(const Texture& tex);
		Texture& operator=(const Texture& tex);

		void delete_texture();
		void move_texture(Texture&& tex);
		void generate_mipmaps();
		void set_params(GLint minmag_filter, GLint wrap);
		void allocate_new_size(const glm::uvec3& dimension_size);

	private:
		GLint        _minmag_filter;
		GLint        _wrap;
		glm::uvec3   _dimensions_size;
		unsigned int _ID;
	};
};


#include <glm/gtx/string_cast.hpp>

namespace ssl
{
	template<typename Type>
	Texture<Type>::Texture(GLint minmag_filter, GLint wrap)
		: _minmag_filter(minmag_filter),
		  _wrap(wrap)
	{
		GL(glGenTextures(1, &_ID));  
	}

	template<typename Type>  
	Texture<Type>::Texture(const glm::uvec3& dimension_size, GLint minmag_filter, GLint wrap)
		: Texture(minmag_filter, wrap)
	{
		_dimensions_size = dimension_size;

		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY, "Creating texture id:" + std::to_string(this->get_ID())
							+ " of dimension: "
							+ std::to_string(_dimensions_size.x) + "x"
							+ std::to_string(_dimensions_size.y) + "x"
							+ std::to_string(_dimensions_size.z));
		}
		#endif
	}

	template<typename Type>
	GLint Texture<Type>::get_minmag_filter() const
	{
		return _minmag_filter;
	}

	template<typename Type>
	GLint Texture<Type>::get_wrap() const
	{
		return _wrap;
	}


	template<typename Type>
	const glm::uvec3&  Texture<Type>::get_dimension_size() const
	{
		return _dimensions_size;
	}
	
	template<typename Type>
	void Texture<Type>::allocate_new_size(const glm::uvec3& dimension_size)
	{
		if(   dimension_size.x > this->get_dimension_size().x
		   || dimension_size.y > this->get_dimension_size().y
		   || dimension_size.z > this->get_dimension_size().z)
		{
			// err("Recreate to change size : " + glm::to_string(this->get_dimension_size()) + " to " + glm::to_string(dimension_size));
			this->bind();
			this->tex_image_if_bound(dimension_size, NULL);

			_dimensions_size = dimension_size;
		}
	}

	template<typename Type>
	void Texture<Type>::change_data(const glm::uvec3& dimension_size, void* data)
	{
		this->bind();

		if(   dimension_size.x > this->get_dimension_size().x
		   || dimension_size.y > this->get_dimension_size().y
		   || dimension_size.z > this->get_dimension_size().z)
		{
			// err("Recreate to change size : " + glm::to_string(this->get_dimension_size()) + " to " + glm::to_string(dimension_size));
			_dimensions_size = dimension_size;
			this->tex_image_if_bound(dimension_size, data);
		}
		else
		{
			this->change_data_if_bound(dimension_size, data);
		}

	}

	template<typename Type>
	void Texture<Type>::bind() const
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_PARAMS_SETTING))
		{
			ssl::prints_msg(SSL_PRINTS_PARAMS_SETTING, "glBindTexture( " 
				+ std::to_string(_ID) + " )");
		}
		#endif

		GL(glBindTexture(this->get_gl_texture_type(), _ID));
	}

	template<typename Type>
	unsigned int Texture<Type>::get_ID() const
	{
		return _ID;
	}

	template<typename Type>
	bool Texture<Type>::wrap_enable() const
	{
		return true;
	}

	template<typename Type>
	void Texture<Type>::set_params(GLint minmag_filter, GLint wrap)
	{
		if(this->mipmaps_enable())
		{
			GL(glTexParameteri(this->get_gl_texture_type(), GL_TEXTURE_MIN_FILTER, minmag_filter));
			GL(glTexParameteri(this->get_gl_texture_type(), GL_TEXTURE_MAG_FILTER, minmag_filter));
		}
		if(this->wrap_enable())
		{
		    GL(glTexParameteri(this->get_gl_texture_type(), GL_TEXTURE_WRAP_S, wrap));
        	GL(glTexParameteri(this->get_gl_texture_type(), GL_TEXTURE_WRAP_T, wrap));
		}
    }

	template<typename Type> 
	void Texture<Type>::initialize(void* data)
	{
		this->bind();	
		this->set_params(_minmag_filter, _wrap);
		this->tex_image_if_bound(this->get_dimension_size(), data);
		if(this->mipmaps_enable())
		{
			this->generate_mipmaps();
		}
	}	


	template<typename Type> 
	void Texture<Type>::generate_mipmaps()
	{
		this->bind();
		GL(glGenerateMipmap(this->get_gl_texture_type()));
	}	


	template<typename Type> 
	void Texture<Type>::bind_on_texture_location(unsigned int texture_binding) 
	{
		#ifdef SSL_DEBUG
		if(texture_binding >= 16)
		{
			err("More than 16 textures used in the sane shader (it is may be possible but TOTEST): here " + SSL_FILE_AND_LINE);
		}
		#endif

		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_PARAMS_SETTING))
		{
			ssl::prints_msg(SSL_PRINTS_PARAMS_SETTING, "glActiveTexture( GL_TEXTURE" + std::to_string(texture_binding) + " )");
		}
		#endif


		GL(glActiveTexture(GL_TEXTURE0 + texture_binding));
        this->bind();	
	}

	template<typename Type>
	void Texture<Type>::move_texture(Texture&& tex)
	{
		this->delete_texture();
		this->_ID = tex._ID;

		tex._ID = ID_NO_TEXTURE;
	}

	template<typename Type>
	Texture<Type>& Texture<Type>::operator=(Texture&& tex)
	{
		this->move_texture(tex);
		return *this;
	}

	template<typename Type>
	Texture<Type>::Texture(Texture&& tex)
	{
		this->_ID = tex._ID;

		tex._ID = ID_NO_TEXTURE;
	}

	template<typename Type>
	void Texture<Type>::delete_texture()
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY, "Texture deleted: " + std::to_string(_ID));
		}
		#endif

		if(this->_ID != ID_NO_TEXTURE)
		{
			#ifdef SSL_PRINTS
			if(ssl::are_prints_enable(SSL_PRINTS_MEMORY))
			{
				ssl::prints_msg(SSL_PRINTS_MEMORY, "Deleting texture id:" + std::to_string(this->get_ID()));
			}
			#endif

			GL(glDeleteTextures(1, &_ID));
		}
		_ID = ID_NO_TEXTURE;
	}

	template<typename Type>  
	void Texture<Type>::copy(const Texture<Type>& tex)
	{
		this->allocate_new_size(tex.get_dimension_size());

		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY_FLOW))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY_FLOW, "Copying texture id:" + std::to_string(tex.get_ID())
							+ " of dimension: "
							+ std::to_string(_dimensions_size.x) + "x"
							+ std::to_string(_dimensions_size.y) + "x"
							+ std::to_string(_dimensions_size.z)
							+ " to texture id:" + std::to_string(this->get_ID()) + " of the same size");
		}
		#endif

		GL(glCopyImageSubData(tex.get_ID(), this->get_gl_texture_type(), 0, 0, 0, 0,
						      _ID         , this->get_gl_texture_type(), 0, 0, 0, 0, tex.get_dimension_size().x, tex.get_dimension_size().y, tex.get_dimension_size().z));
		
		if(this->mipmaps_enable())
		{
			this->generate_mipmaps();
		}
	}

	template<typename Type>  
	void Texture<Type>::attach_on_framebuffer(unsigned int attachement_index) const
	{
		this->bind();
		this->attach_on_framebuffer_if_bound(attachement_index);
	}

	template<typename Type>  
	unsigned int Texture<Type>::get_elements_amount() const
	{
		return _dimensions_size.x*_dimensions_size.y*_dimensions_size.z;
	}

	template<typename Type>  
	std::vector<Type> Texture<Type>::download_data() const
	{
		std::vector<Type> ret(this->get_elements_amount());
		this->download_data(ret.data());
		return ret;
	}

	template<typename Type>  
	void Texture<Type>::download_data(void* dst) const
	{
		this->bind();
		GL(glGetTexImage(this->get_gl_texture_type(),
						 0,
						 Texture<Type>::get_GL_valid_format(),
						 Texture<Type>::get_GL_data_type(),
						 dst));
	}

	template<typename Type>  
	Texture<Type>::~Texture()
	{
		this->delete_texture();
	}
};

#endif //_TEXTURE_HPP_
