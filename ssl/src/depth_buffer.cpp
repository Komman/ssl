#include "depth_buffer.hpp"

using namespace std;
using namespace glm;

namespace ssl
{
	DepthBuffer::DepthBuffer(const glm::uvec2& size)
		: Texture(uvec3(size.x, size.y, 0.0), GL_NEAREST, GL_CLAMP_TO_BORDER)
	{
		this->initialize(NULL);
	}

	DepthBuffer::DepthBuffer(const glm::uvec3&  dimension_size)
		: DepthBuffer(uvec2(dimension_size.x, dimension_size.y))
	{

	}

	attachementType DepthBuffer::get_attachement_type() const
	{
		return DEPTH_ATTACHMENT;
	}

	GLenum DepthBuffer::get_gl_texture_type() const
	{
		return GL_TEXTURE_2D;
	}

	glm::uvec2 DepthBuffer::size() const
	{
		return glm::vec2(this->get_dimension_size().x, this->get_dimension_size().y);
	}

	void DepthBuffer::tex_image_if_bound(const glm::uvec3& dimension_size, void* data)   const
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY, "Depth texture created, id: " + std::to_string(this->get_ID())
							+ ", dimension: " + std::to_string(dimension_size.x) + "x" + std::to_string(dimension_size.y));
		}
		#endif

		GL(glTexImage2D(this->get_gl_texture_type(),
			 			0,
			 			GL_DEPTH_COMPONENT,
			 			dimension_size.x,
			 			dimension_size.y,
			 			0,
			 			GL_DEPTH_COMPONENT,
			 			GL_UNSIGNED_BYTE,
			 			NULL));
	}

	void DepthBuffer::change_data_if_bound(const glm::uvec3& dimension_size, void* data)
	{
		err("TODO: " + SSL_FILE_AND_LINE);
	}

	bool DepthBuffer::mipmaps_enable() const
	{
		return false;
	}

	void DepthBuffer::definitive_resize_GPU(const glm::uvec3& new_dimension_size)        const
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY, "Depth texture resize, id: " + std::to_string(this->get_ID())
							+ ", dimension: " + std::to_string(new_dimension_size.x) + "x" + std::to_string(new_dimension_size.y));
		}
		#endif

		GL(glTextureStorage2D(this->get_ID(), 1, GL_DEPTH_COMPONENT, new_dimension_size.x, new_dimension_size.y));
	}

	void DepthBuffer::attach_on_framebuffer_if_bound(uint attachement_index) const
	{
		GL(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, this->get_gl_texture_type(), this->get_ID(), 0));
	}

	bool DepthBuffer::wrap_enable()  const 
	{
		return false;
	}

};

