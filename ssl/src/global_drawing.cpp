#include "global_drawing.hpp"
#include "drawing_context.hpp"
#include "window.hpp"

#include "debug.hpp"
// #include <bitset>

namespace ssl
{
	namespace global_drawing
	{
		static DrawingContext* context;


		static void GLAPIENTRY openglDebugCallback(GLenum source, GLenum type, GLuint id,
		                                    GLenum severity, GLsizei length,
		                                    const GLchar *message, const void *userParam) {
			    if (severity == GL_DEBUG_SEVERITY_NOTIFICATION) {
			        return; // Ignore notifications
			    }

			    printf("OpenGL Debug Message:\n");
			    printf("Source: %d\n", source);
			    printf("Type: %d\n", type);
			    printf("ID: %d\n", id);
			    printf("Severity: %d\n", severity);
			    printf("Message: %s\n", message);

			    if (severity == GL_DEBUG_SEVERITY_HIGH) {
			        printf("This is a high-severity issue!\n");
			    }
		}

		void init()
		{
			context = new DrawingContext();
			context->create();

			// GL(glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE)); // antialiasing 4x 
		
			// GL(glEnable(GL_DEBUG_OUTPUT));
		    // GL(glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS));
		    // printf("%p\n", glDebugMessageCallbackARB);
		    // GL(glDebugMessageCallback(openglDebugCallback, NULL));
		    // GL(glDebugMessageControl( GL_DONT_CARE,  GL_DONT_CARE,  GL_DONT_CARE, 0, NULL, GL_TRUE));
		}

		void free()
		{
			delete context;
		}

		void bind_default_vao()
		{
			context->bind();
		}

		void antialiasing(bool enable)
		{
			if(enable)
			{
				GL(glEnable(GL_MULTISAMPLE));
			}
			else
			{
				GL(glDisable(GL_MULTISAMPLE));
			}
		}
		
		void bind_framebuffer(const BasicFrameBuffer* framebuffer)
		{
			#ifdef SSL_DEBUG
			if(framebuffer == NULL)
			{
				err("Try to bind a NULL framebuffer here: " + SSL_FILE_AND_LINE);
			}
			#endif

			framebuffer->bind();
		}

		void bind_screen()
		{
			GL(glBindFramebuffer(GL_FRAMEBUFFER, 0));
		}

		void target(const BasicFrameBuffer* framebuffer_ot_target)
		{
			if(framebuffer_ot_target == NULL)
			{
				target_screen();
			}
			else
			{
				framebuffer_ot_target->bind();
				set_view_port(glm::uvec2(0,0), glm::uvec2(framebuffer_ot_target->frame_size().x, framebuffer_ot_target->frame_size().y));
			}
		}

		void target_screen()
		{
			global_drawing::bind_screen();
			set_view_port(glm::uvec2(0,0), glm::uvec2(window::size().x, window::size().y));
			
		}

		void set_view_port(const glm::uvec2& origin, const glm::uvec2& size)
		{
			static glm::uvec2 old_origin = glm::uvec2(0);
			static glm::uvec2 old_size   = glm::uvec2(0);

			if(origin != old_origin || size != old_size)
			{
				old_origin = origin;
				old_size = size;
				
				GL(glViewport(old_origin.x ,old_origin.y , old_size.x, old_size.y));
			}
		}

		void clear(GLbitfield mask)
		{
			GL(glClear(mask));
		}

		void clear_color(const glm::vec4& color)
		{
			context->set_clear_color(color);
			GL(glClearColor(color.x, color.y, color.z, color.w));
		}

		const glm::vec4& get_clear_color()
		{
			return context->get_clear_color();
		}

		void depth(bool enable)
		{
			if(enable)
			{
				GL(glEnable(GL_DEPTH_TEST)); 
			}
			else
			{
				GL(glDisable(GL_DEPTH_TEST)); 
			}
		}

		void depth_mask(bool enable)
		{
			if(enable)
			{
				GL(glDepthMask(GL_TRUE));
			}
			else
			{
				GL(glDepthMask(GL_FALSE));
			}
		}

		void stencil_testing(bool enable)
		{
			if(enable)
			{
				GL(glEnable(GL_STENCIL_TEST));
			}
			else
			{
				GL(glDisable(GL_STENCIL_TEST));
			}
		}

		void stencil_mask(GLuint mask)
		{
			GL(glStencilMask(mask));
		}

		void stencil_function(GLenum func, GLint ref, GLuint mask)
		{
			GL(glStencilFunc(func, ref, mask));
		}

		void stencil_op(GLenum sfail, GLenum dpfail, GLenum dppass)
		{
			GL(glStencilOp(sfail, dpfail, dppass));
		}

		void depth_function(GLenum func)
		{
			GL(glDepthFunc(func));
		}

		void culling(bool enable)
		{
			if(enable)
			{
				GL(glEnable(GL_CULL_FACE));
			}
			else
			{
				GL(glDisable(GL_CULL_FACE));
			}
			
		}

		void blending(bool enable)
		{
			if(enable)
			{
				GL(glEnable(GL_BLEND)); 
			}
			else
			{
				GL(glDisable(GL_BLEND)); 
			}
		}

		void set_blend_function(GLenum sfactor, GLenum dfactor)
		{
			GL(glBlendFunc(sfactor, dfactor));
		}

		void set_blend_equation(GLenum mode)
		{
			GL(glBlendEquation(mode));
		}
	};
};