#include "depth_stencil_buffer_multisample.hpp"
#include "window.hpp"

using namespace glm;

namespace ssl
{
	DepthStencilBufferMultisample::DepthStencilBufferMultisample(const glm::uvec2& size)
		: Texture(uvec3(size.x, size.y, 0.0), GL_NEAREST, GL_CLAMP_TO_BORDER)
	{
		this->initialize(NULL);
	}

	DepthStencilBufferMultisample::DepthStencilBufferMultisample(const glm::uvec3&  dimension_size)
		: DepthStencilBufferMultisample(uvec2(dimension_size.x, dimension_size.y))
	{

	}

	GLenum DepthStencilBufferMultisample::get_gl_texture_type() const
	{
		return GL_TEXTURE_2D_MULTISAMPLE;
	}

	void DepthStencilBufferMultisample::tex_image_if_bound(const glm::uvec3& dimension_size, void* data)   const
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY, "DepthStencilBufferMultisample texture created, id: " + std::to_string(this->get_ID())
							+ ", dimension: " + std::to_string(dimension_size.x) + "x" + std::to_string(dimension_size.y));
		}
		#endif
		
		GL(glTexImage2DMultisample(this->get_gl_texture_type(),
			 			ANTIALIASING,
			 			GL_DEPTH24_STENCIL8,
			 			dimension_size.x,
			 			dimension_size.y,
			 			GL_TRUE));
	}

	void DepthStencilBufferMultisample::change_data_if_bound(const glm::uvec3& dimension_size, void* data)
	{
		err("TODO: " + SSL_FILE_AND_LINE);
	}

	void DepthStencilBufferMultisample::definitive_resize_GPU(const glm::uvec3& new_dimension_size)        const
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY, "DepthStencilBufferMultisample resize, id: " + std::to_string(this->get_ID())
							+ ", dimensions: " + std::to_string(new_dimension_size.x) + "x" + std::to_string(new_dimension_size.y));
		}
		#endif

		GL(glTextureStorage2DMultisample(this->get_ID(), ANTIALIASING, GL_DEPTH24_STENCIL8, new_dimension_size.x, new_dimension_size.y, GL_TRUE));
	}

	attachementType DepthStencilBufferMultisample::get_attachement_type() const
	{
		return DEPTH_ATTACHMENT;
	}

	glm::uvec2 DepthStencilBufferMultisample::size() const
	{
		return glm::vec2(this->get_dimension_size().x, this->get_dimension_size().y);
	}

	bool DepthStencilBufferMultisample::mipmaps_enable() const
	{
		return false;
	}

	void DepthStencilBufferMultisample::attach_on_framebuffer_if_bound(uint attachement_index) const
	{
		GL(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, this->get_gl_texture_type(), this->get_ID(), 0));
	}

	bool DepthStencilBufferMultisample::wrap_enable()  const 
	{
		return false;
	}
};

