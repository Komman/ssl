#ifndef _TEXTURE2D_HPP_
#define _TEXTURE2D_HPP_

#include "texture.hpp"

namespace ssl
{
	template<typename Type>
	class Texture2D : public Texture<Type>
	{
	public:
		Texture2D(const glm::uvec2& size,
				  void*              data          = NULL,
				  GLint              minmag_filter = DEFAULT::MINMAG_FILTER,
				  GLint              wrap          = DEFAULT::TEXTURE_WRAP);


		Texture2D(const glm::uvec3&  dimension_size,
				  void* data,
				  GLint minmag_filter,
				  GLint wrap);

		glm::uvec2 size()            const;
		attachementType get_attachement_type() const override;
		GLenum     get_gl_texture_type()       const override;	
	
	protected:
	
		void change_data_if_bound(const glm::uvec3& dimension_size, void* data) override;

		void attach_on_framebuffer_if_bound(uint attachement_index)            const override;
		void tex_image_if_bound(const glm::uvec3& dimension_size, void* data)  const override;
		bool mipmaps_enable()                                                  const override;
		void definitive_resize_GPU(const glm::uvec3& new_dimension_size)                  const override;


	private:

	};
};


namespace ssl
{
	template<typename Type>
	Texture2D<Type>::Texture2D(const glm::uvec2& size,
							   void* data,
							   GLint minmag_filter,
							   GLint wrap)
		: Texture2D<Type>(glm::uvec3(size.x, size.y, 1), data, minmag_filter, wrap)
	{
		
	}

	template<typename Type>
	Texture2D<Type>::Texture2D(const glm::uvec3&  dimension_size,
							   void* data,
							   GLint minmag_filter,
							   GLint wrap)
		: Texture<Type>(dimension_size, minmag_filter, wrap)
	{
		this->initialize(data);
	}

	template<typename Type>
	attachementType Texture2D<Type>::get_attachement_type() const
	{
		return COLOR_ATTACHEMENT;
	}


	template<typename Type>
	glm::uvec2 Texture2D<Type>::size() const
	{
		return glm::vec2(this->get_dimension_size().x, this->get_dimension_size().y);		
	}

	template<typename Type>
	void Texture2D<Type>::tex_image_if_bound(const glm::uvec3& dimension_size, void* data) const
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY_FLOW))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY_FLOW, "Creating texture2D data of texture id:" + std::to_string(this->get_ID()) 
				+ ", with data of size: "
				+ std::to_string(dimension_size.x) + "x"
				+ std::to_string(dimension_size.y));
		}
		#endif

		GL(glTexImage2D(this->get_gl_texture_type(),
			 			0,
			 			Texture<Type>::get_GL_internal_format(),
			 			dimension_size.x,
			 			dimension_size.y,
			 			0,
			 			Texture<Type>::get_GL_valid_format(),
			 			Texture<Type>::get_GL_data_type(),
			 			data));
	}

	template<typename Type>
	void Texture2D<Type>::change_data_if_bound(const glm::uvec3& dimension_size, void* data)
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY_FLOW))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY_FLOW, "Updating texture2D data of texture id:" + std::to_string(this->get_ID()));
		}
		#endif

		GL(glTexSubImage2D(
			this->get_gl_texture_type(),
			0,
			0,
			0,
			dimension_size.x,
			dimension_size.y,
			Texture<Type>::get_GL_valid_format(),
			Texture<Type>::get_GL_data_type(),
			data
		));
	}


	template<typename Type>
	GLenum Texture2D<Type>::get_gl_texture_type() const
	{
		return GL_TEXTURE_2D;
	}

	template<typename Type>
	void Texture2D<Type>::definitive_resize_GPU(const glm::uvec3& new_dimension_size) const
	{
		#ifdef SSL_PRINTS
		if(ssl::are_prints_enable(SSL_PRINTS_MEMORY_FLOW))
		{
			ssl::prints_msg(SSL_PRINTS_MEMORY_FLOW, "Resizing texture2D id:" + std::to_string(this->get_ID()) 
				+ ", with size: "
				+ std::to_string(new_dimension_size.x) + "x"
				+ std::to_string(new_dimension_size.y));
		}
		#endif

		GL(glTextureStorage2D(this->get_ID(), 1, Texture<Type>::get_GL_internal_format(), new_dimension_size.x, new_dimension_size.y));
	}

	template<typename Type>
	bool Texture2D<Type>::mipmaps_enable() const
	{
		return true;
	}

	template<typename Type>
	void Texture2D<Type>::attach_on_framebuffer_if_bound(uint attachement_index) const
	{
		GL(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + attachement_index, this->get_gl_texture_type(), this->get_ID(), 0));
	}
};

#endif //_TEXTURE2D_HPP_
