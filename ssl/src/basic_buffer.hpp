#ifndef _BASIC_BUFFER_HPP_
#define _BASIC_BUFFER_HPP_

#include <GL/glew.h>
#include <iostream>
#include "types_id.hpp"

namespace ssl
{
	enum draw_type {
		DYNAMIC_DRAW = GL_DYNAMIC_DRAW,
		STATIC_DRAW  = GL_STATIC_DRAW
	};

	class BasicBuffer
	{
	public:
		BasicBuffer();
		virtual ~BasicBuffer();
		
		BasicBuffer(BasicBuffer&& b);
		BasicBuffer& operator=(BasicBuffer&&  b); 

		// Update the data if it is necessary and bind the buffer
		void bind() const; 
		void instant_update_GPU() const;
		// Update the data if it is necessary
		void update_GPU() const;

		// Returns the number of location used
		virtual uint target_vertex_location(uint location) const =0;
		virtual uint size() const =0;
		virtual types_id get_self_type_id() const =0;

		uint bind_and_target_vertex_location(uint location) const;

	protected:
		
		// Call this method to signal that GPU data are not up to date
		// (every times the CPU data are changed)
		void data_has_changed();
		unsigned int get_ID() const;

		// The buffer must be bound to call this method
		virtual void update_GPU_data_if_bound() const =0;
		virtual void bind_buffer() const =0;

	private:
		void free();

		BasicBuffer& operator=(const BasicBuffer& b); 
		BasicBuffer(const BasicBuffer& b);

		mutable bool _data_has_changed;
		unsigned int _ID;
	};
};

#endif //_BASIC_BUFFER_HPP_
