#ifndef _SSL_HPP_
#define _SSL_HPP_

#include "src/debug.hpp"
#include "src/color.hpp"
#include "src/global_drawing.hpp"
#include "src/uniform_camera.hpp"
#include "src/uniform.hpp"
#include "src/uniform_array.hpp"
#include "src/array_buffer.hpp"
#include "src/element_buffer.hpp"
#include "src/frame_buffer.hpp"
#include "src/frame_buffer_multisample.hpp"
#include "src/uniform_texture_1D.hpp"
#include "src/uniform_texture_2D.hpp"
#include "src/uniform_texture_3D.hpp"
#include "src/autoprint_recorder.hpp"
#include "src/utils.hpp"
#include "src/shader_generator.hpp"
#include "src/instant_drawer.hpp"
#include "src/constants.hpp"
#include "src/vvv.hpp"
#include "src/uniform_image1D.hpp"
#include "src/uniform_image2D.hpp"
#include "src/uniform_image3D.hpp"
#include "src/compute_shader.hpp"
#include "src/gpu_memory.hpp"
#include "src/buffer_builder.hpp"
#include "src/object_merger.hpp"
#include "src/labeled_recorder.hpp"
#include "src/gl_recorder.hpp"
#include "src/vertex_array_object.hpp"

//MAKEMAKE_STATIC_LIBRARY

namespace ssl
{
	void init(unsigned int size_x,
			  unsigned int size_y,
			  const std::string& name);

	void init(const std::string& name);
	
	void init(const std::string& name,
		 	  unsigned int window_divisor);
	
	void close();
}

#endif //_SSL_HPP_
