#include "../example/motor/primary_hitbox.hpp"
#include "../example/motor/hitbox_fusion.hpp"

namespace hitbox
{
	using namespace glm;

	void sphere_sphere()
	{
		SphereHitbox s1(vec3(0), sphereInfo{vec3(0), 1});
		SphereHitbox s2(vec3(0), sphereInfo{vvv::x()*1.5f, 1});
		SphereHitbox s2t(vvv::x()*1.5f, sphereInfo{vec3(0), 1});
		SphereHitbox s3(vec3(0), sphereInfo{vvv::x()*3.0f, 1});
		SphereHitbox s3t(vvv::x()*3.0f, sphereInfo{vec3(0), 1});
		SphereHitbox s4t(vvv::x()*1.0f, sphereInfo{vec3(0), 1});

		TEST("unit collision", s1.in_collision(s2));
		TEST("unit collision mirror", s2.in_collision(s1));
		TEST("unit no collision", !s1.in_collision(s3));
		TEST("unit translated collision", s1.in_collision(s2t));
		TEST("unit translated no collision", !s1.in_collision(s3t));
		TEST("both translated collision", s2t.in_collision(s3t));
		TEST("both translated no collision", !s4t.in_collision(s3t));
	
		collisionImpact i;

		i = s1.impact(s2, vec3(1,0,0), 0.2);
		TEST("already impact", i.impact);

		i = s1.impact(s3, vec3(1,0,0), 0.8);
		TEST("no impact", !i.impact);

		i = s1.impact(s3, vec3(1,0,0), 1.1);
		TEST("impact", i.impact);
		TEST("impact normal", equalf(i.normalN, -vvv::x()));
		TEST("impact point", equalf(i.point, vvv::x()*2.0f));

		i = s1.impact(s3t, vec3(1,0,0), 1.1);
		TEST("translated impact", i.impact);
		TEST("translated impact normal", equalf(i.normalN, -vvv::x()));
		TEST("translated impact point", equalf(i.point, vvv::x()*2.0f));

		i = s1.impact(s3t, vec3(1,0,0), 0.8);
		TEST("translated no impact", !i.impact);
	}

	void prism_prism()
	{
		PrismHitbox p1(vec3(0), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, 2)
		}});

		PrismHitbox p2(vec3(0,0,1.5), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, 2)
		}});

		PrismHitbox p3(vec3(0,0,2.5), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, 2)
		}});

		PrismHitbox p4(vec3(0,0,2.5), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, -10)
		}});

		TEST("collision", p1.in_collision(p2));
		TEST("collision mirrored", p2.in_collision(p1));
		TEST("no collision", !p1.in_collision(p3));
		TEST("crossing collision", p1.in_collision(p4));
		TEST("crossing collision mirrored", p4.in_collision(p1));

		collisionImpact i;

		TEST("static impact", p1.impact(p2, vec3(1,0,0), 0).impact);
		TEST("static impact mirrored", p2.impact(p1, vec3(1,0,0), 0).impact);
		TEST("no static impact", !p1.impact(p3, vec3(1,0,0), 0).impact);
		TEST("crossing static impact", p1.impact(p4, vec3(1,0,0), 0).impact);
		TEST("crossing static impact mirrored", p4.impact(p1, vec3(1,0,0), 0).impact);
		
		i = p1.impact(p3, vec3(0,0,1), 0.6);
		TEST("impact", i.impact);
		TEST("impact normal", equalf(i.normalN, -vec3(0,0,1)));
		TEST("impact point", equalf(i.point, vec3(0.3,0.3,2.5)));

		i = p3.impact(p1, -vec3(0,0,1), 0.6);
		TEST("impact mirrored", i.impact);
		TEST("impact mirrored normal", equalf(i.normalN, vec3(0,0,1)));
		TEST("impact mirrored point", equalf(i.point, vec3(0.3,0.3,2)));

		i = p1.impact(p3, vec3(0,0,1), 0.4);
		TEST("no impact", !i.impact);
	}

	void fusion_one_hitbox()
	{
		auto p1old = std::make_unique<PrismHitbox>(vec3(0), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, 2)
		}});

		auto p2old = std::make_unique<PrismHitbox>(vec3(0,0,1.5), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, 2)
		}});

		auto p3old = std::make_unique<PrismHitbox>(vec3(0,0,2.5), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, 2)
		}});

		auto p4old = std::make_unique<PrismHitbox>(vec3(0,0,2.5), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, -10)
		}});

		HitboxFusion p1(vec3(5.0, 2.0, -9.0));
		p1.fusion_hitbox(std::move(p1old));
		HitboxFusion p2(vec3(5.0, 2.0, -9.0));
		p2.fusion_hitbox(std::move(p2old));
		HitboxFusion p3(vec3(5.0, 2.0, -9.0));
		p3.fusion_hitbox(std::move(p3old));
		HitboxFusion p4(vec3(5.0, 2.0, -9.0));
		p4.fusion_hitbox(std::move(p4old));


		TEST("collision", p1.in_collision(p2));
		TEST("collision mirrored", p2.in_collision(p1));
		TEST("no collision", !p1.in_collision(p3));
		TEST("crossing collision", p1.in_collision(p4));
		TEST("crossing collision mirrored", p4.in_collision(p1));

		collisionImpact i;
		
		i = p1.impact(p3, vec3(0,0,1), 0.6);
		TEST("impact", i.impact);
		TEST("impact normal", equalf(i.normalN, -vec3(0,0,1)));
		TEST("impact point", equalf(i.point, vec3(5.0, 2.0, -9.0) + vec3(0.3,0.3,2.5)));

		i = p3.impact(p1, -vec3(0,0,1), 0.6);
		TEST("impact mirrored", i.impact);
		TEST("impact mirrored normal", equalf(i.normalN, vec3(0,0,1)));
		TEST("impact mirrored point", equalf(i.point, vec3(5.0, 2.0, -9.0) + vec3(0.3,0.3,2)));

		i = p1.impact(p3, vec3(0,0,1), 0.4);
		TEST("no impact", !i.impact);
	}

	void fusion_moves()
	{
		vec3 dep1 = vec3(9,-12,1);
		auto p1old = std::make_unique<PrismHitbox>(dep1, prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, 2)
		}});

		vec3 dep2 = vec3(-5,-1,-10000);
		auto p2old = std::make_unique<PrismHitbox>(dep2 + vec3(0,0,1.5), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, 2)
		}});

		vec3 dep3 = vec3(1000, 1000, 10000);
		auto p3old = std::make_unique<PrismHitbox>(dep3 + vec3(0,0,2.5), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, 2)
		}});

		vec3 dep4 = vec3(1000, -108, -10000);
		auto p4old = std::make_unique<PrismHitbox>(dep4 + vec3(0,0,2.5), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, -10)
		}});

		HitboxFusion p1(vec3(5.0, 2.0, -9.0));
		BasicHitbox* npr1 = p1.fusion_hitbox(std::move(p1old));
		HitboxFusion p2(vec3(5.0, 2.0, -9.0));
		BasicHitbox* npr2 = p2.fusion_hitbox(std::move(p2old));
		HitboxFusion p3(vec3(5.0, 2.0, -9.0));
		BasicHitbox* npr3 = p3.fusion_hitbox(std::move(p3old));
		HitboxFusion p4(vec3(5.0, 2.0, -9.0));
		BasicHitbox* npr4 = p4.fusion_hitbox(std::move(p4old));

		p1.move(-dep1 + vec3(1,8,6));
		npr1->move(-vec3(1,8,6));
		p2.move(-dep2/2.0f);
		npr2->move(-dep2/2.0f);
		p3.move(-dep3);
		p4.move(-dep4);

		collisionImpact i;

		TEST("collision", p1.in_collision(p2));
		TEST("collision mirrored", p2.in_collision(p1));
		TEST("no collision", !p1.in_collision(p3));
		TEST("crossing collision", p1.in_collision(p4));
		TEST("crossing collision mirrored", p4.in_collision(p1));
		
		i = p1.impact(p3, vec3(0,0,1), 0.6);
		TEST("impact", i.impact);
		TEST("impact normal", equalf(i.normalN, -vec3(0,0,1)));
		TEST("impact point", equalf(i.point, vec3(5.0, 2.0, -9.0) + vec3(0.3,0.3,2.5)));

		i = p3.impact(p1, -vec3(0,0,1), 0.6);
		TEST("impact mirrored", i.impact);
		TEST("impact mirrored normal", equalf(i.normalN, vec3(0,0,1)));
		TEST("impact mirrored point", equalf(i.point, vec3(5.0, 2.0, -9.0) + vec3(0.3,0.3,2)));

		i = p1.impact(p3, vec3(0,0,1), 0.4);
		TEST("no impact", !i.impact);

		// Same but updating coords

		p1.move(vec3(1,8,6));
		npr1->move(-vec3(1,8,6));
		p2.move(-dep2/2.0f);
		npr2->move(+dep2/2.0f);
		p3.move(-vec3(5,-10,5));
		npr3->move(vec3(5,-10,5));
		p4.move(-dep4);
		npr4->move(dep4);


		TEST("updating collision", p1.in_collision(p2));
		TEST("updating collision mirrored", p2.in_collision(p1));
		TEST("updating no collision", !p1.in_collision(p3));
		TEST("updating crossing collision", p1.in_collision(p4));
		TEST("updating crossing collision mirrored", p4.in_collision(p1));
		
		i = p1.impact(p3, vec3(0,0,1), 0.6);
		TEST("updating impact", i.impact);
		TEST("updating impact normal", equalf(i.normalN, -vec3(0,0,1)));
		TEST("updating impact point", equalf(i.point, vec3(5.0, 2.0, -9.0) + vec3(0.3,0.3,2.5)));

		i = p3.impact(p1, -vec3(0,0,1), 0.6);
		TEST("updating impact mirrored", i.impact);
		TEST("updating impact mirrored normal", equalf(i.normalN, vec3(0,0,1)));
		TEST("updating impact mirrored point", equalf(i.point, vec3(5.0, 2.0, -9.0) + vec3(0.3,0.3,2)));

		i = p1.impact(p3, vec3(0,0,1), 0.4);
		TEST("no impact", !i.impact);
	}

	void fusion_moves_by_speed()
	{
		vec3 dep1 = vec3(9,-12,1);
		auto p1old = std::make_unique<PrismHitbox>(dep1, prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, 2)
		}});

		vec3 dep2 = vec3(-5,-1,-10000);
		auto p2old = std::make_unique<PrismHitbox>(dep2 + vec3(0,0,1.5), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, 2)
		}});

		vec3 dep3 = vec3(1000, 1000, 10000);
		auto p3old = std::make_unique<PrismHitbox>(dep3 + vec3(0,0,2.5), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, 2)
		}});

		vec3 dep4 = vec3(1000, -108, -10000);
		auto p4old = std::make_unique<PrismHitbox>(dep4 + vec3(0,0,2.5), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, -10)
		}});

		HitboxFusion p1(vec3(5.0, 2.0, -9.0));
		BasicHitbox* npr1 = p1.fusion_hitbox(std::move(p1old));
		HitboxFusion p2(vec3(5.0, 2.0, -9.0));
		BasicHitbox* npr2 = p2.fusion_hitbox(std::move(p2old));
		HitboxFusion p3(vec3(5.0, 2.0, -9.0));
		BasicHitbox* npr3 = p3.fusion_hitbox(std::move(p3old));
		HitboxFusion p4(vec3(5.0, 2.0, -9.0));
		BasicHitbox* npr4 = p4.fusion_hitbox(std::move(p4old));

		p1.set_speed(-dep1 + vec3(1,8,6));
		npr1->move(-vec3(1,8,6));
		p2.set_speed(-dep2/2.0f);
		npr2->move(-dep2/2.0f);
		p3.set_speed(-dep3);
		p4.set_speed(-dep4);

		collisionImpact i;

		i = p1.impact(p2, 1.0f);
		TEST("impact", i.impact);
		i = p2.impact(p1, 1.0f);
		TEST("impact mirrored", i.impact);
		i = p1.impact(p3, 1.0f);
		TEST("no impact", !i.impact);
		i = p1.impact(p4, 1.0f);
		TEST("crossing impact", i.impact);
		i = p4.impact(p1, 1.0f);
		TEST("crossing impact mirrored", i.impact);

		// Same but updating coords

		p1.move(vec3(1,8,6));
		npr1->move(-vec3(1,8,6));
		p2.move(-dep2/2.0f);
		npr2->move(+dep2/2.0f);
		p3.move(-vec3(5,-10,5));
		npr3->move(vec3(5,-10,5));
		p4.move(-dep4);
		npr4->move(dep4);

		i = p1.impact(p2, 1.0f);
		TEST("updating impact", i.impact);
		i = p2.impact(p1, 1.0f);
		TEST("updating impact mirrored", i.impact);
		i = p1.impact(p3, 1.0f);
		TEST("updating no impact", !i.impact);
		i = p1.impact(p4, 1.0f);
		TEST("updating crossing impact", i.impact);
		i = p4.impact(p1, 1.0f);
		TEST("updating crossing impact mirrored", i.impact);
	}

	void fusion_moves_by_speed_mirrored()
	{
		auto p1old = std::make_unique<PrismHitbox>(vec3(0), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, 2)
		}});

		auto p2old = std::make_unique<PrismHitbox>(vec3(0,0,1.5), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, 2)
		}});

		auto p3old = std::make_unique<PrismHitbox>(vec3(0,0,2.5), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, 2)
		}});

		auto p4old = std::make_unique<PrismHitbox>(vec3(0,0,2.5), prismInfo{.points={
			vec3(0, 0, 0),
			vec3(1, 0, 0),
			vec3(0, 1, 0),
			vec3(0.3, 0.3, -10)
		}});

		HitboxFusion p1(vec3(5.0, 2.0, -9.0));
		p1.fusion_hitbox(std::move(p1old));
		HitboxFusion p2(vec3(5.0, 2.0, -9.0));
		p2.fusion_hitbox(std::move(p2old));
		HitboxFusion p3(vec3(5.0, 2.0, -9.0));
		p3.fusion_hitbox(std::move(p3old));
		HitboxFusion p4(vec3(5.0, 2.0, -9.0));
		p4.fusion_hitbox(std::move(p4old));


		collisionImpact i;

		p1.set_speed(+vec3(0,0,1)*0.6f*1.0f);
		p3.set_speed(-vec3(0,0,1)*0.6f*9.0f);
		
		i = p1.impact(p3, 0.5);
		TEST("impact", i.impact);
		TEST("impact normal", equalf(i.normalN, -vec3(0,0,1)));
		TEST("impact point", equalf(i.point, vec3(5.0, 2.0, -9.0) + vec3(0.3,0.3,2.0 + 0.5*0.1f)));

		p1.set_speed(+vec3(0,0,1));
		p3.set_speed(-vec3(0,0,1));

		i = p3.impact(p1, 0.5);
		TEST("impact mirrored", i.impact);
		TEST("impact mirrored normal", equalf(i.normalN, vec3(0,0,1)));
		TEST("impact mirrored point", equalf(i.point, vec3(5.0, 2.0, -9.0) + vec3(0.3,0.3,2.25)));

		p1.set_speed(+vec3(0,0,1)*0.4f);
		p3.set_speed(-vec3(0,0,1)*0.4f);

		i = p1.impact(p3, 0.5);
		TEST("no impact", !i.impact);
	}
};
