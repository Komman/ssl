#ifndef _TEST_GEOM_HPP_
#define _TEST_GEOM_HPP_

#include "../example/utils/geom.hpp"

namespace geomtest
{
	using namespace glm;
	
	void sphere_segment_impact_noextrm()
	{
		sphereInfo s = {vec3(0.5,5,sqrt(2.0f)/2.0f), 1};
		collisionImpact i = geom::sphere_segment_impact_noextrm(s, vec3(0), vvv::x(), glm::normalize(-vvv::y()), 6);

		TEST("sphere segment impact collision", i.impact);
		TEST("sphere segment impact point", equalf(i.point, vec3(0.500000, 0.000000, 0.000000)));
		TEST("sphere segment impact normalize", equalf(i.normalN, vec3(0.000000, 0.707107, 0.707107)));
		TEST("sphere segment impact distance", equalf(i.distance, 4.0f + (1.0f-cos(sqrt(2.0f)/2.0f))));
	}
};

#endif //_TEST_GEOM_HPP_
