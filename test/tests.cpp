#include <iostream>
#include <string>
#include <vector>

#include <glm/gtx/string_cast.hpp>

#include "../ssl/ssl.hpp"

using namespace std;
using namespace ssl;


const string failed = TERM::RED   + "FAILED" + TERM::NOCOL;
const string passed = TERM::GREEN + "PASSED" + TERM::NOCOL;


#define TEST_MODULE(function)\
	cout<<endl<<" -Testing module "<<TERM::PURPLE<<#function<<"()"<<TERM::NOCOL<<":"<<endl;\
	function();

static int test_number=0;
static int test_passed=0;

void TEST(const std::string& title, bool condition)
{
	cout<<"    -"<<title<<" : "<<(condition ? (passed) : (failed))<<endl;
	
	test_number++;
	if(condition)
	{
		test_passed++;
	}
}

template<typename FloatType>
bool equalf(FloatType v, FloatType objective)
{
	return glm::length(objective-v) < 0.001f;
}

template<>
bool equalf(float v, float objective)
{
	return (objective-v) < 0.001f;
}

void section(const string&  title)
{
	cout<<endl<<TERM::BLUE<<"== Testing "<<title<<" =="<<TERM::NOCOL<<endl;
}

//========= TESTING FUNCTIONS ======

//Trivial
void always_true() {TEST("true" , true); }
void always_false(){TEST("false", false);}

#include "test_hitbox.hpp"
#include "test_geom.hpp"

//==========   MAIN   ==============

int main()
{
	cout<<endl<<TERM::ORANGE<<"=== ALL TESTS: ==="<<TERM::NOCOL<<endl;

	//======   TESTS   ======

	section("Trivial");
	{
		TEST_MODULE(always_true)
	}

	section("geom");
	{
		using namespace geomtest;

		TEST_MODULE(sphere_segment_impact_noextrm)
	}

	section("Hitboxes");
	{
		using namespace hitbox;

		TEST_MODULE(sphere_sphere)
		TEST_MODULE(prism_prism)
		TEST_MODULE(fusion_one_hitbox)
		TEST_MODULE(fusion_moves)
		TEST_MODULE(fusion_moves_by_speed)
		TEST_MODULE(fusion_moves_by_speed_mirrored)
	}

	//=======================

	cout<<endl<<TERM::CYAN<<"=== Summary: ==="<<TERM::NOCOL<<endl;
	cout<<"  -Passed: ("<<test_passed<<"/"<<test_number<<")"<<endl;
	cout<<"  -Global: "<<((test_passed==test_number) ? (passed) : (failed))<<endl;
	return 0;
}