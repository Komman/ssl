#include "walled_tower.hpp"

WalledTower::WalledTower(Drawer* drawer,
						 const glm::vec3& center,
			             const glm::vec3& direction,
			             const std::vector<stageRelief>& reliefs,
			             const glm::vec4& base_color,
						 float wall_height,
						 float wall_size,
						 float hat,
						 const glm::vec4& roof_color)
	: RoofedTower(drawer, center ,direction, reliefs, base_color, hat, roof_color),
	  _wall_prehead(10.0),
	  _wall_height(wall_height),
	  _wall_size(wall_size),
	  _end_achievement(utils::random_float(0.5, 1.0))
{
	
}

float WalledTower::wall_size() const
{
	return _wall_size;
}

float WalledTower::wall_height() const
{
	return _wall_height;
}



float WalledTower::get_start_achievement() const
{
	return 0.0;
}

float WalledTower::get_end_achievement() const
{
	return _end_achievement;
}
