#ifndef _SHOOTING_TOWER_HPP_
#define _SHOOTING_TOWER_HPP_

#include "walled_tower.hpp"
#include "tower_shooter.hpp"

class ShootingTower : public WalledTower
{
public:
	ShootingTower(Drawer* drawer,
				const glm::vec3& center,
			    const glm::vec3& direction,
			    const std::vector<stageRelief>& reliefs,
			    const glm::vec4& base_color,
				float wall_height,
				float wall_size,
				float hat,
				const glm::vec4& roof_color);

	void animate(float dt) override;

	void set_target(const BasicDirectedPoint* target);

private:
	TowerShooter _shooter;
};

#endif //_SHOOTING_TOWER_HPP_
