#include "l_base_buildor.hpp"
#include "../motor/gutils.hpp"

using namespace std;
using namespace glm;
using namespace utils;

/*
	POINT PARAMS
*/

LBaseBuildor::pointParams::pointParams(const glm::vec3& coord, const singleLpilerParam& piler)
	: point(coord),
	  is_piler(piler.size > 0.0f),
	  single(piler)
{

}


void LBaseBuildor::pointParams::occupy(const singleLpilerParam& piler)
{
	this->is_piler = (piler.size > 0.0f);
	this->single   = piler;
}

globalLpilers::globalLpilers(const generatedPilers& gen, int groupeid)
	: inter_pilers_minmax(gen.inter_pilers_minmax),
		height(gen.height),
		single({groupeid, gen.size, gen.color})
{

}

/*
	L BASE BUILDOR
*/

LBaseBuildor::LBaseBuildor(const glm::vec3 points[3],
				           PlanarSymmetry<glm::vec4>& symmetry,
				           const baseLparams& params)
	: _params(params),
	  _default_piler({-1, 0.0f, params.wall_color}),
	  _groupeid_current(0),
	  _symmetry(symmetry)
{
	_xpilers = {pointParams(points[0], _default_piler), pointParams(points[1], _default_piler)};
	_zpilers = {pointParams(points[1], _default_piler), pointParams(points[2], _default_piler)};
}

void LBaseBuildor::generate_pilers(const generatedPilers& genpilers_args)
{
	globalLpilers pilers_args(genpilers_args, _groupeid_current);

	this->occupy_empty_places(pilers_args);

	auto start = _xpilers.begin();
	auto last = std::prev(_xpilers.end());
	vec3 pointor = start->point;

	int i = 0;
	int max_iter = 200;
	auto& jminmax = pilers_args.inter_pilers_minmax;
	
	pointor.x -= start->single.size;
	pointor.x -= pilers_args.single.size;

	while(start != last && i<max_iter)
	{
		pointor.x -= utils::random_float(jminmax.x, jminmax.y);

		if(pointor.x - pilers_args.single.size > std::max(std::next(start)->point.x + std::next(start)->single.size, last->point.x + last->single.size))
		{
			_xpilers.insert(std::next(start), pointParams(pointor, pilers_args.single));
		}

		while(start != last && std::next(start)->point.x - std::next(start)->single.size > pointor.x + pilers_args.single.size)
		{
			start++;
		}
		
		vec3 pointor = start->point;
		pointor.x -= start->single.size;
		pointor.x -= pilers_args.single.size;

		i++;
	}

	if(i == max_iter)
	{
		err("LBaseBuildor::generate_pilers(): To many pilers added");
	}

	_groupeid_current++;
}

void LBaseBuildor::occupy_empty_places(const globalLpilers& pilers_args)
{
	for(auto& it : _xpilers)
	{
		if(!it.is_piler)
		{
			if(k_chance_on_n(2,5))
			{
				it.occupy(pilers_args.single);
			}
		}
	}
}
/*
     b1u---     |\
      | \       | \
      |  \      |  \
      |   \     |   \
     b1l  b2u-- |   |
       \   |    |   |   SYMMETRCICAL
        \  |     \  |
         \ |      \ |
         b2l---    \|
                     
*/

indexedLBase LBaseBuildor::finish_base()
{
	indexedLBase ret;
	for(auto it = std::prev(_xpilers.end()); it != _xpilers.begin(); it--)
	{
		const auto indices = this->build_local_Xpiler(it);
		if(!(indices.first == NOT_TOP_INDEXOR))
		{
			ret.top_pilers.push_back(indices.first);
			ret.bot_pilers.push_back(indices.second);
		}
	}

	return ret;
}

LBaseBuildor::pilerTopBotIndexor LBaseBuildor::build_directed_piler(const std::vector<pilerCorner>& places, int groupeid, bool link_last)
{
	float shadowcoeff = 0.5f;

	if(places.size() <= 0)
	{
		err("LBaseBuildor::build_directed_piler(): no places");
	}

	unsigned int indices[2*places.size()];

	for(int i=0; i<(int)places.size(); i++)
	{
		indices[2*i+0] = _symmetry.place_symmetrical(places[i].point, places[i].color*shadowcoeff);
		indices[2*i+1] = _symmetry.place_symmetrical(places[i].point + vvv::y()*_params.wall_height, places[i].color);
	}

	for(int i=0; i<(int)places.size()-1; i++)
	{
		_symmetry.link_rectangle(indices[2*i], indices[2*(i+1)], indices[2*(i+1)+1], indices[2*i+1]);
	}

	int l = 2*(int)places.size()-2;
	if(link_last)
	{
		_symmetry.link_rectangle(indices[l+1], indices[l], indices[0], indices[1]);
	}

	if(places.size() < 4)
	{
		return {NOT_TOP_INDEXOR, NOT_TOP_INDEXOR};
	}

	pilerTopIndexor ret_top;
	pilerTopIndexor ret_bot;

	ret_top.groupeid = groupeid;
	ret_bot.groupeid = groupeid;

	ret_top.north_east = indices[2*0+1];
	ret_top.north_west = indices[2*1+1];
	ret_top.south_west = indices[2*2+1];
	ret_top.south_east = indices[2*3+1];

	ret_bot.north_east = indices[2*0+0];
	ret_bot.north_west = indices[2*1+0];
	ret_bot.south_west = indices[2*2+0];
	ret_bot.south_east = indices[2*3+0];

	return {ret_top, ret_bot};
}


LBaseBuildor::pilerTopBotIndexor LBaseBuildor::build_local_Xpiler(const std::list<pointParams>::iterator& piler_it)
{
	float occlusion_coeff=0.4f;

	std::vector<pilerCorner> places;

	if(!piler_it->is_piler)
	{
		places = {{piler_it->point, piler_it->single.color}};
		return this->build_directed_piler(places, piler_it->single.groupeid, false);
	}
	else
	{
		if(piler_it == _xpilers.begin())
		{
			places={
				{piler_it->point + (vvv::x()+vvv::z())*piler_it->single.size , piler_it->single.color},
				{piler_it->point + (vvv::x()-vvv::z())*piler_it->single.size , piler_it->single.color},
				{piler_it->point + (-vvv::x()-vvv::z())*piler_it->single.size, piler_it->single.color},
				{piler_it->point + (-vvv::x()+vvv::z())*piler_it->single.size, piler_it->single.color*occlusion_coeff}
			};

			return this->build_directed_piler(places, piler_it->single.groupeid, true);
		}
		else if(piler_it == std::prev(_xpilers.end()))
		{
			places={
				{piler_it->point + ( vvv::x()+vvv::z())*piler_it->single.size, piler_it->single.color*occlusion_coeff},
				{piler_it->point + ( vvv::x()-vvv::z())*piler_it->single.size, piler_it->single.color},
				{piler_it->point + (-vvv::x()-vvv::z())*piler_it->single.size, piler_it->single.color},
				{piler_it->point + (-vvv::x()+vvv::z())*piler_it->single.size, piler_it->single.color}			};
		
			return this->build_directed_piler(places, piler_it->single.groupeid, true);
		}
		else
		{
			places={
				{piler_it->point + vvv::x()*piler_it->single.size            , piler_it->single.color*occlusion_coeff},
				{piler_it->point + (vvv::x()-vvv::z()*2.0f)*piler_it->single.size , piler_it->single.color},
				{piler_it->point + (-vvv::x()-vvv::z()*2.0f)*piler_it->single.size, piler_it->single.color},
				{piler_it->point - vvv::x()*piler_it->single.size            , piler_it->single.color*occlusion_coeff}
			};

			return this->build_directed_piler(places, piler_it->single.groupeid, false);
		}		
	}

}

