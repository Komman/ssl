#include "piler_wallet.hpp"

using namespace std;
using namespace glm;

PilerWallet::PilerWallet(const afterBaseInfo& infos, 
						 unsigned int first,
						 unsigned int last)
	: _infos(infos),
	  _groups(),
	  _piler_states(last+1 - first),
	  _first(first),
	  _last(last)

{
	for(int i=first; i<=(int)last; i++)
	{
		const auto& top = infos.pilers_top[i];
		bool found = false;
		int groupeid = top.groupeid;

		for(auto& p : _groups)
		{
			if(p.groupeid == groupeid)
			{
				p.pilers.push_back(i);
				found = true;
				break;
			}
		}

		if(!found)
		{
			_groups.push_back({groupeid, {(unsigned int)i}});
		}
	}

	for(const auto& g : _groups)
	{
		for(unsigned int p : g.pilers)
		{
			// cout<<p<<endl;
			_piler_states[p - first] = {g.groupeid, false, 0.0f};
		}
	}
}

PilerWallet::buildingSpots PilerWallet::get_spots_symmetrical(unsigned int arity)
{
	buildingSpots ret;

	std::vector<std::unordered_map<unsigned int, pilersSpot>> preret;
	preret.push_back({});

	for(unsigned int i=0; i<_piler_states.size(); i++)
	{
		const auto& state = _piler_states[i];
		auto& current = preret[preret.size()-1];

		if(!(state.occuped))
		{
			current[state.groupeid].groupeid = state.groupeid;
			current[state.groupeid].pilers.push_back(i);
		}
		else
		{
			if(current.size() != 0)
			{
				preret.push_back({});
			}
		}
	}

	for(const auto& ggroup : preret)
	{
		for(const auto& spot : ggroup)
		{
			if(spot.second.pilers.size() >= arity)
			{
				ret.push_back({spot.second.pilers, spot.second.groupeid});
			}
		}
	}

	for(auto& r : ret)
	{
		for(unsigned int i=0; i<r.pilers.size(); i++)
		{
			r.pilers[i] += _first;

			if(r.pilers[i]<_first || r.pilers[i]>_last)
			{
				err("PilerWallet::get_spots_symmetrical(): Wrong index conversion :\n" 
					+ to_string(_first) + " <= " + to_string(r.pilers[i]) + " <= " + to_string(_last));
			}
		}
	}

	return ret;
}

PilerWallet::buildingSpots PilerWallet::get_spots_asymmetrical(unsigned int arity)
{
	err("TODO");
	return {};
}



roofLinkor PilerWallet::connect(PilerConnectorInterface<SYMMPILER>& connector,
					   			const std::vector<unsigned int>& pilers,
					   			float height_base,
					   			float height_top,
					   			bool north_face_visible,
					   			bool south_face_visible)
{

	unsigned int pilermin = *std::min_element(pilers.begin(), pilers.end());
	unsigned int pilermax = *std::max_element(pilers.begin(), pilers.end());

	for(unsigned int i=pilermin; i<=pilermax; i++)
	{
		auto& state =  _piler_states[i - _first];

		if(state.occuped)
		{
			state.max_height = std::min(state.max_height, height_base);
		}
		else
		{
			state.max_height = height_base;
			state.occuped = true;
		}
	}

	return connector.connect(pilers, height_base, height_top, north_face_visible, south_face_visible);
}

void PilerWallet::connect(PilerConnectorInterface<ASYMPILER>& connector,
					   	  const std::vector<unsigned int>& pilers)
{

	err("TODO");


	connector.connect(pilers);
}

void PilerWallet::print() const
{
	cout<<"PilerWallet: {"<<endl;

	cout<<" first: "<<_first<<",  last: "<<_last<<endl;
	
	cout<<" groups: {"<<endl;;
	for(const auto& g: _groups)
	{
		cout<<"  {groupid: "<<g.groupeid<<", p:{";
		for(const auto p : g.pilers)
		{
			cout<<p<<", ";
		}
		cout<<"}"<<endl;
	}
	cout<<" }"<<endl;

	cout<<" states: {";	
	for(const auto& s : _piler_states)
	{
		if(s.occuped)
		{
			cout<<"X, ";
		}
		else
		{
			cout<<s.groupeid<<", ";
		}
	}
	cout<<"}"<<endl;

	cout<<"}"<<endl;
}


void PilerWallet::pilerSet::print() const
{
	cout<<"groupeid: "<<this->groupeid<<", ids: {";
	for(const auto& pi : this->pilers)
	{
		cout<<pi<<", ";
	}
	cout<<"}"<<endl;
}

void PilerWallet::buildingSpots::print() const
{
	cout<<"buildingSpots: {"<<endl;
	for(const auto p : (*this))
	{
		cout<<" ";
		p.print();
		cout<<","<<endl;
	}
	cout<<"}";
}

void PilerWallet::pilersSpot::print() const
{
	cout<<"  {groupid: "<<groupeid<<", p:{";
	for(const auto p : pilers)
	{
		cout<<p<<", ";
	}
	cout<<"}";
}