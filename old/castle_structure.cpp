#include "ingame.hpp"

#include "castle_structure.hpp"
#include "effects.hpp"

#include <glm/gtx/rotate_vector.hpp>

using namespace glm;
using namespace std;
using namespace utils;

CastleStructure::CastleStructure(SharedRessources& shared_ressources,
								 const BasicGround& ground,
								 StaticLigthor& slightor,
			   					 const castleStructureParameters& parameters,
			   					 DrawerPackController& drawer_controller)
	: _architecture(),
	  _inground(50.0),
	  _ground(ground),
	  _slightor(slightor),
	  _shared_ressources(shared_ressources),
	  _parameters(parameters),	
	  _center("castle_center", parameters.center),
	  _ambiant_color("castle_ambiant_color", vec3(1.0,0.5,0.3)/3.0f),
	  _light_radius("castle_light_radius", parameters.radius + 100.0f),
	  _ligths_activation("castle_ligths_activation", 0.0f),
	  _minminax_tower_height("castle_minminax_tower_height", vec2(parameters.center.y)),
	  _shader_generator("shaders/castle.glsl"),
	  _tower_drawer(_shader_generator, "simple_vertex", "tower_fragment",  {"all_lights_mesh_normal"}, drawer_controller),
	  _wall_drawer( _shader_generator, "simple_vertex", "wall_fragment"  , {"all_lights_mesh_normal"}, drawer_controller),
	  _lantern_drawer( _shader_generator, "lantern_vertex", "lantern_fragment"  , {"all_lights_mesh_normal"}, drawer_controller),
	  _tower_color(0.6, 0.6, 0.35, 1.0),
	  _time(0.0),
	  _total_achievement(0.0),
	  _start_stop_lanters(0.0, 0.6),
	  _tower_merger({}, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW),
	  _wall_merger({}, STATIC_DRAW, STATIC_DRAW, STATIC_DRAW),
	  _structure_merged(false),
	  _merge_call_count(0),
	  _wall_color(parameters.enclosure_count, vec3(_tower_color) ),
	  _enclosure_indices(parameters.enclosure_count-1, std::vector<unsigned int>(0))
{
	_tower_drawer.depth(true);	
	_wall_drawer.depth(true);
	_lantern_drawer.depth(true);

	this->build();
	this->prepare_primary_walls();
	
	this->start_dynamic_build();
}

void CastleStructure::prepare_primary_walls()
{
	for(int i=0; i<(int)_architecture.vertexs().size(); i++)
	{
		auto& storage = _architecture.vertexs()[i];

		for(auto& tower : storage.get_meshes())
		{
			if(tower->get_building_type() == VertexMesh::TOWER)
			{
				_tower_merger.add_object(tower.get());
			}
		}
		
	}

	for(int i=0; i<(int)_architecture.edges().size(); i++)
	{
		auto& w = _architecture.edges()[i];

		for(auto& wall : w.second.get_meshes())
		{
			if(wall->get_wall_type() == EdgeMesh::PRIMARY_WALL)
			{
				_wall_merger.add_object(wall.get());
			}
		}
	}
}

castleStructureParameters CastleStructure::default_parameters(const BasicGround& ground)
{
	return {
		ground.get_xyz(0.0, 0.0),
		150.0f,
		vec2(1.2f, 1.7f),
		10,
		4
	};
}

std::unique_ptr<Wall> CastleStructure::build_wall(unsigned int tower_i, unsigned int tower_j)
{
	vec3 center1 = _architecture[tower_i].get_center();
	vec3 center2 = _architecture[tower_j].get_center();
	float miny = std::min(center1.y, center2.y);
	float height1 = _architecture[tower_i].get_wall_height(); 
	float height2 = _architecture[tower_j].get_wall_height();
	float height =  std::min(center1.y + height1, center2.y + height2) - miny;
	float size   = std::min(_architecture[tower_i].get_wall_size(),   _architecture[tower_j].get_wall_size());
	
	center1.y = miny; 
	center2.y = miny;
	return std::make_unique<Wall>(&_wall_drawer, _ground, (std::vector<glm::vec3>){center1, center2},
							 	  _wall_color[_architecture[tower_i].get_enclosure()], height, size, 0.5f, size/6.0f,
							 	  wallPilerArgs({true, _parameters.radius/6.0f, sqrt(size)*2.0f, 10.0f}));
}


void CastleStructure::build()
{
	_slightor.set_ligths_activation(0.0);

	_wall_color[_parameters.enclosure_count-1] = WHITE*0.5f;
	this->build_enclosure(_parameters.enclosure_count-1, {6,  4.0f, 12.0f , 3, vec3(0.10, 0.10, 0.12)*2.5f, 4.0f},  &CastleStructure::simple_tower_builder);
	this->build_enclosure(_parameters.enclosure_count-2, {15, 3.0f, 15.0f , 2, vec3(0.10, 0.07, 0.20)*2.5f, 4.0f},  &CastleStructure::simple_tower_builder);
	this->build_enclosure(_parameters.enclosure_count-3, {10, 3.0f, 20.0f , 1, vec3(0.15, 0.07, 0.12)*2.5f, 4.0f},  &CastleStructure::simple_tower_builder);
}

void CastleStructure::set_target(const BasicDirectedPoint* target)
{
	for(auto& building_vector : _architecture.vertexs())
	{
		for(auto& building : building_vector.get_meshes())
		{
			if(building->get_drawer() != NULL && building->get_building_type()==VertexMesh::TOWER)
			{
				ShootingTower* tower = (ShootingTower*)(building.get());

				tower->set_target(target);
			}
		}
	}
}


std::unique_ptr<ShootingTower> CastleStructure::simple_tower_builder(const glm::vec3& center,
											        		       const glm::vec3& direction,
											        		       const towerBuildingParameters& params)
{
	std::vector<Tower::stageRelief> relief;

	relief.push_back({4.0f + _inground, 
							0.8f * params.size});
	relief.push_back({2.0,  0.7f * params.size});
	relief.push_back({params.min_base_heigth, 0.6f * params.size});
	
	float wall_height = 0.0f;
	for(auto& r : relief)
	{
		wall_height += r.heigth;
	}
	wall_height -= params.size/2.0f;

	relief.push_back({2.0f  * params.size/20.0f,  0.8f * params.size});
	relief.push_back({2.0f  * params.size/20.0f,  0.9f * params.size});
	relief.push_back({15.0f * params.size/20.0f,  1.0f * params.size});
	relief.push_back({0.5f  * params.size/20.0f,  1.1f * params.size});
	// relief.push_back({2.0f  * params.size/20.0f,  0.8f * params.size});

	vec4 roof_color(params.roof_color, -1.0);

	auto walledtower = std::make_unique<ShootingTower>(&_tower_drawer,
													 _ground.get_xyz(center) - vvv::y()*_inground,
													 direction,
													 relief,
													 _tower_color,
													 wall_height,
													 params.size/4.0f,
													 13.0f  * params.size/20.0f,
													 roof_color);
	walledtower->build_roof();

	return walledtower;
}

CastleStructure::enclosureRange CastleStructure::get_enclosure_range(int enclosure)
{
	#ifdef SSL_DEBUG
	if(enclosure >= _parameters.enclosure_count)
		err("enclosure out of bound: " + SSL_FILE_AND_LINE);
	if(enclosure == 0)
		err("enclosure 0 cannot be built");
	#endif

	float radius_by_eclosure = _parameters.radius / float(_parameters.enclosure_count);

	return {float(enclosure)*radius_by_eclosure, float(enclosure+1)*radius_by_eclosure};
}

float CastleStructure::get_enclosure_random_radius(int enclosure)
{
	auto range = this->get_enclosure_range(enclosure);
	
	float amplifior1 = 1.0f;
	float amplifior2 = 1.0f;
	if(enclosure == _parameters.enclosure_count-1)
	{
		amplifior1 = _parameters.last_enclosure_amplifior.x;
		amplifior2 = _parameters.last_enclosure_amplifior.y;
	}
	if(enclosure == 1)
	{
		amplifior1 = 0.6f;
	}
	
	return utils::random_float((range.first + range.second)/2.0f*amplifior1, range.second*amplifior2);
}

static float compute_min_base_height(const BasicGround& ground, const std::vector<glm::vec3>& positions, int index)
{
	vec3 maxypos1 = ground.get_max_y_position(positions[utils::circulator(index-1, positions.size())], 
											  positions[index],
											  4);
	vec3 maxypos2 = ground.get_max_y_position(positions[index], 
											  positions[utils::circulator(index+1, positions.size())],
											  4);
	float d1 = maxypos1.y - ground.get_y(positions[index]);
	float d2 = maxypos2.y - ground.get_y(positions[index]);
	return std::max(std::max(0.0f, d1), d2);
}

static inline vec3 tower_light_color(int enclosure)
{
	// if(enclosure == 1)
	// {
	// 	return vec3(0.2,0.2,1.0);
	// }
	// if(enclosure == 2)
	// {
	// 	return vec3(0.15,0.7,0.15);
	// }

	if(enclosure == 2)
	{
		return vec3(0.2,0.2,1.0);
	}
	if(enclosure == 1)
	{
		return vec3(1.0,0.15,0.15);
	}
	
	return vec3(0.8,0.4,0.15);
}

void CastleStructure::refine_minmax_tower(const glm::vec3& tower_middle)
{
	const vec2& minmax = _minminax_tower_height.get_value();

	vec2 new_minmax={
		std::min(minmax.x, _ground.get_y(tower_middle)),
		std::max(minmax.y, tower_middle.y)
	};

	_minminax_tower_height = new_minmax;
}

void CastleStructure::build_enclosure(int enclosure,
							 const towerBuildingParameters& tower_args,
							 towerBuilderFunction tower_builder)
{ 
	#ifdef SSL_DEBUG
	if(enclosure == 0)
	{
		err("enclosure 0 is dangerous to be built");
	}
	#endif

	float angle = 0.0;
	float subangle_divisor = 2.0f;
	auto  range = this->get_enclosure_range(enclosure);
	float perimeter_jump = float(tower_args.size) / (range.second * 2.0f*M_PI);
	float max_subangle = perimeter_jump * subangle_divisor * float(tower_args.tower_space) * 2.0f*M_PI;
	int start = _architecture.size();
	int last_vertex_index = start;

	vector<glm::vec3> positions;
	vector<glm::vec3> directions;

	vector<lanternDisposition> lantern_disposition;

	do
	{
		vec3 direction = glm::rotate(vvv::x(), angle, vvv::y());
		vec3 position  = _parameters.center + direction*this->get_enclosure_random_radius(enclosure);

		directions.push_back(direction);
		positions.push_back(position);
		lantern_disposition.push_back({vec3(0.0), -(float)(M_PI)/2.0f-angle});

		angle += utils::random_float(max_subangle/subangle_divisor, max_subangle);
	} 
	while(angle < 2.0f*M_PI - max_subangle/subangle_divisor);


	for(int i=0; i<(int)positions.size(); i++)
	{
		towerBuildingParameters args = tower_args;
		args.min_base_heigth += compute_min_base_height(_ground, positions, i);

		auto drtower = (*this.*tower_builder)(positions[i], directions[i], args);
		lantern_disposition[i].position = drtower->get_center() + vvv::y()*(drtower->wall_height() - args.under_torch) + directions[i]*tower_args.size/2.0f;
		
		vec3 light_pos = lantern_disposition[i].position+directions[i]*1.0f + vvv::y()*1.5f;
		_slightor.add_ligth(light_pos, {tower_light_color(enclosure), 30.0f, 100.0f});

		// effects::explobibos().add_instance({
		// 	.center   = light_pos,
		// 	.size     = vec2(2.0,0.5),
		// 	.color    = tower_light_color(enclosure),
		// 	.duration = -1.0f
		// });

		this->refine_minmax_tower(lantern_disposition[i].position);

		CastleVertex tower(enclosure, drtower->get_center(), drtower->wall_height(), drtower->wall_size());
		tower.add_drawable_object(std::move(drtower));
		// _ground.dark_color(positions[i], 0.0f);

		_enclosure_indices[enclosure-1].push_back(_architecture.size());
		last_vertex_index = _architecture.add_vertex(std::move(tower));

		if(last_vertex_index > start)
		{
			CastleEdge ws;
			ws.add_drawable_object(this->build_wall(last_vertex_index-1, last_vertex_index));

			_architecture.add_oriented_edge({last_vertex_index-1, last_vertex_index}, std::move(ws));
		}
	}

	this->add_lanterns(enclosure, lantern_disposition);

	CastleEdge ws;
	ws.add_drawable_object(this->build_wall(last_vertex_index, start));	

	_architecture.add_oriented_edge({last_vertex_index, start}, std::move(ws));
}

void CastleStructure::add_lanterns(int enclosure, const vector<lanternDisposition>& lantern_disposition)
{
	float global_lanter_size = 2.0f;
	vec3 lantern_size(global_lanter_size, global_lanter_size, global_lanter_size*2.0f);

	std::unique_ptr<Lantern> l;
	l=std::make_unique<Lantern>(_lantern_drawer, lantern_size, tower_light_color(enclosure), lantern_disposition, _start_stop_lanters);

	CastleVertex all_lanterns(enclosure, l->get_center(), 0.0,0.0);
	all_lanterns.add_drawable_object(std::move(l));

	_architecture.add_vertex(std::move(all_lanterns));
}

void CastleStructure::start_dynamic_build()
{
	_total_achievement = 0.0;
	_time = 0.0;
}

float CastleStructure::get_achievement(float ach_begin, float ach_end)
{
	return (_total_achievement - ach_begin)/(ach_end-ach_begin);
}

void CastleStructure::set_dynamic_lights()
{
	float start_start_global_light = 0.0f;
	float end_start_global_light   = 0.1f;
	float start_end_global_light   = std::max(_start_stop_lanters.y,  0.8f);
	float end_end_global_light     = start_end_global_light+0.1f;
	float start_castle_light       = end_end_global_light+0.1f;
	float stop_castle_light        = start_castle_light+0.5f;

	 if(_total_achievement < start_castle_light)
	 {
	 	_ambiant_color = vec3(0.7, 0.8, 1.0)/1.5f;
	 }
	 else
	 {
	 	_ambiant_color = vec3(1.0,0.5,0.3)/2.0f;
	 }

	if(_total_achievement < start_start_global_light)
	{
		_ligths_activation = 0.0f;
		_slightor.set_ligths_activation(_ligths_activation.get_value());
	}

	if(_total_achievement > stop_castle_light)
	{
		_ligths_activation = 1.0f;
		_slightor.set_ligths_activation(_ligths_activation.get_value());
	}

	_ligths_activation = linear_animator(_ligths_activation.get_value(),
										 0.0f,
										 1.0f,
										 start_start_global_light,
										 end_start_global_light,
										 _total_achievement);

	_ligths_activation = linear_animator(_ligths_activation.get_value(),
										 1.0f,
										 0.0f,
										 start_end_global_light,
										 end_end_global_light,
										 _total_achievement);

	_ligths_activation = linear_animator(_ligths_activation.get_value(),
										 0.0f,
										 1.0f,
										 start_castle_light,
										 stop_castle_light,
										 _total_achievement);

	if(_total_achievement>=start_castle_light)
	{
		_slightor.set_ligths_activation(_ligths_activation.get_value());
	}
}

void CastleStructure::set_time_build(float achievement)
{
	_time = 15.0f*(achievement) + 1.0f;
}

void CastleStructure::dynamic_build(float dt)
{
	_time += dt;
	float computed_total_achievement = (_time - 1.0)/15.0f;

	this->set_dynamic_build(computed_total_achievement);


	if(computed_total_achievement < 0.9)
		return;
	
	for(int i=0; i<(int)_architecture.vertexs().size(); i++)
	{
		_architecture[i].animate(dt);		
	}
}


void CastleStructure::set_dynamic_build(float achievement)
{
	_total_achievement = achievement;

	this->set_dynamic_lights();

	for(int i=0; i<(int)_architecture.vertexs().size(); i++)
	{
		auto& storage = _architecture.vertexs()[i];
		for(auto& tower : storage.get_meshes())
		{
			if(tower != NULL)
			{
				float start = tower->get_start_achievement();
				float end   = tower->get_end_achievement();
				float local_achievement = (_total_achievement - start)/(end - start);

				tower->set_build_achievement(local_achievement);
			}
		}		
	}
	for(int i=0; i<(int)_architecture.edges().size(); i++)
	{
		auto& w = _architecture.edges()[i];

		for(auto& wall : w.second.get_meshes())
		{
			if(wall != NULL)
			{
				float start = wall->get_start_achievement();
				float end   = wall->get_end_achievement();
				float local_achievement = (_total_achievement - start)/(end - start);
				
				wall->set_build_achievement(local_achievement);
			}	
		}	
	}
}

void CastleStructure::special_structure_drawing()
{
	for(auto& building : _architecture.vertexs())
	{
		for(auto& tower : building.get_meshes())
		{
			if(tower != NULL && tower->get_building_type() == VertexMesh::TOWER)
			{
				((TowerInterface*)(tower.get()))->put_in_massive_draw();
			}
		}
	}
	for(auto& wall_indexed : _architecture.edges())
	{
		auto& wall_set = wall_indexed.second.get_meshes();

		for(auto& wall : wall_set)
		{
			if(wall != NULL && wall->get_wall_type() == EdgeMesh::PRIMARY_WALL)
			{
				((Wall*)(wall.get()))->put_in_massive_draw();
			}
		}
	}
}


void CastleStructure::draw_merged_objects()
{
	if(_total_achievement > 1.0f && !_structure_merged)
	{
		_structure_merged = true;

		#ifdef SSL_DEBUG
		_merge_call_count++;
		if(_merge_call_count>1)
		{
			err("CastleStructure: _merge_call_count > 1: RIP Performances");
		}
		#endif
		// AutoPrintRecorder::START();
		this->special_structure_drawing();
		_tower_merger.merge_objects();
		_wall_merger.merge_objects();
		// AutoPrintRecorder::STOP();
	}
	if(_structure_merged)
	{
		// AutoPrintRecorder::GL_START();
		_shared_ressources.tools().game_drawer.draw_MS(_tower_drawer, (ElementDrawableObject&)_tower_merger);
		_shared_ressources.tools().game_drawer.draw_MS(_wall_drawer,  (ElementDrawableObject&)_wall_merger);
		// AutoPrintRecorder::STOP();
	}
}

void CastleStructure::draw_castle_storage(BuildingMesh& obj)
{
	if(obj.multisample_draw())
	{
		if(obj.bloom_draw())
		{	
			_shared_ressources.tools().game_drawer.draw_bloomed_MS(*(obj.get_drawer()), (ElementDrawableObject&)obj);	
		}
		else
		{
			_shared_ressources.tools().game_drawer.draw_MS(*(obj.get_drawer()), (ElementDrawableObject&)obj);	
		}
	}
	else
	{
		if(obj.bloom_draw())
		{
			_shared_ressources.tools().game_drawer.draw_bloomed(*(obj.get_drawer()), (ElementDrawableObject&)obj);
		}
		else
		{
			err("CastleStructure::draw_castle_storage(): a draw is not MS and do not override bloom MS texture (may be wanted but risqued)");
			_shared_ressources.tools().game_drawer.draw_normal_unsafe(*(obj.get_drawer()), (ElementDrawableObject&)obj);
		}
	}
}

void CastleStructure::draw_tower_shadows(VerticalShadower& shadower)
{
	ArrayBuffer<vec3> shadow_bases(vector<vec3>(4, vec3(0.0)), DYNAMIC_DRAW);
	ElementBuffer rectangle_indices({0,1,2, 0,2,3}, DYNAMIC_DRAW);

	for(auto& building_vector : _architecture.vertexs())
	{
		for(auto& building : building_vector.get_meshes())
		{
			if(building->get_drawer() != NULL && building->get_building_type()==VertexMesh::TOWER)
			{
				Tower* tower = (Tower*)(building.get());
				if(tower->get_reliefs().size() <= 0)
				{
					err("Tower without stages");
				}
				float size = tower->get_reliefs()[0].size;
				float over_occlusion_coeff = 2.0f;
				
				vec3 xdep = glm::normalize(building->get_direction());
				vec3 ydep = glm::cross(xdep, vvv::y());
				vec3 startpos = building->get_center() ;
				xdep*=size*over_occlusion_coeff/2.0f;
				ydep*=size*over_occlusion_coeff/2.0f;

				shadow_bases.change_subvalue(0, startpos - xdep - ydep);
				shadow_bases.change_subvalue(1, startpos + xdep - ydep);
				shadow_bases.change_subvalue(2, startpos + xdep + ydep);
				shadow_bases.change_subvalue(3, startpos - xdep + ydep);

				shadower.add_shadow_elements(&shadow_bases, rectangle_indices);
			}
		}
	}
}
void CastleStructure::draw_wall_shadows(VerticalShadower& shadower)
{
	ArrayBuffer<vec3> shadow_bases(vector<vec3>(4, vec3(0.0)), DYNAMIC_DRAW);
	ElementBuffer rectangle_indices({0,1,2, 0,2,3}, DYNAMIC_DRAW);

	for(auto& wall_indexed : _architecture.edges())
	{
		auto& wall_set = wall_indexed.second.get_meshes();

		for(auto& wall : wall_set)
		{
			if(wall != NULL && wall->get_wall_type()==EdgeMesh::PRIMARY_WALL)
			{
				Wall* realwall = (Wall*)(wall.get());
				if(realwall->get_steps().size() <= 1)
				{
					err("Wall is not linking two towers");
				}
				for(int i=1; i<(int)realwall->get_steps().size(); i++)
				{
					int starti = i-1;
					int endi =   i;
					float over_occlusion = 1.0f;
					const vec3& startw = realwall->get_steps()[starti];
					const vec3& endw   = realwall->get_steps()[endi];
					float size = realwall->get_size();

					vec3 xdep = glm::normalize(endw - startw);
					vec3 ydep = glm::cross(xdep, vvv::y());

					shadow_bases.change_subvalue(0, startw - xdep*over_occlusion - ydep*(over_occlusion));
					shadow_bases.change_subvalue(1, startw - xdep*over_occlusion + ydep*(size + over_occlusion));
					shadow_bases.change_subvalue(3, endw + xdep*over_occlusion - ydep*(over_occlusion));
					shadow_bases.change_subvalue(2, endw + xdep*over_occlusion + ydep*(size + over_occlusion));

					shadower.add_shadow_elements(&shadow_bases, rectangle_indices);
				}
			}
		}
	}
}

void CastleStructure::draw()
{
	if(_total_achievement <= 0.0f)
	{
		return;
	}

	this->draw_merged_objects();

	for(auto& building_vector : _architecture.vertexs())
	{
		for(auto& building : building_vector.get_meshes())
		{
			if(building->get_drawer() != NULL && !building->special_drawing())
			{
				this->draw_castle_storage(*building);
			}
		}
	}

	for(auto& wall_indexed : _architecture.edges())
	{
		auto& wall_set = wall_indexed.second.get_meshes();

		for(auto& wall : wall_set)
		{
			if(wall != NULL && wall->get_drawer() != NULL && (!wall->special_drawing()))
			{
				this->draw_castle_storage(*wall);
			}
		}
	}
}

bool CastleStructure::ready_for_tower_shadow() const
{
	return (_total_achievement > 0.05f);
}

bool CastleStructure::ready_for_wall_shadow() const
{
	return (_total_achievement > 0.7f);
}

const castleStructureParameters& CastleStructure::structure_parameters() const
{
	return _parameters;
}

const ShaderGenerator& CastleStructure::shader_generator() const
{
	return _shader_generator;
}

const std::vector<std::vector<unsigned int>>&  CastleStructure::get_enclosure_indices()
{
	return _enclosure_indices;
}