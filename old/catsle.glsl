void tower_fragment(in  vec3 frag_position, in vec4 frag_color,
                     out vec4 final_color) 
{
    final_color = vec4(clamp(frag_color.xyz, vec3(0.0) ,vec3(1.0)), 1.0);
    float vd = (frag_color.a > 0.0 ? 
                                     local::voronoi3D_distance(frag_position, 4.0f)
                                     :
                                     local::voronoi3D_distance(frag_position, 1.0f)*1.5f);

    final_color = final_color*(1.0f+vd/4.0f);
}

float sinusoide(float x)
{
    return (cos(x)+1.0)/2.0f;
}
void wall_fragment(in  vec3 frag_position, in vec4 frag_color,
                   out vec4 final_color) 
{
    final_color = vec4(clamp(frag_color.xyz, vec3(0.0) ,vec3(1.0)), 1.0);
    float vd = local::voronoi3D_distance(frag_position, 12.0f);
    final_color = final_color*(1.0f+vd/8.0f);
}

void donjon_vertex(in  vec3 in_position_s, in vec4 in_color_s,
                   out vec3 out_position_s, out vec4 out_color_s)
{
    vec3 new_pos = in_position_s + vec3(0.0, uniform::donjon_ydep0, 0.0);
    new_pos.x = mix(uniform::donjon_center0.x, new_pos.x, uniform::donjon_larger0);
    new_pos.z = mix(uniform::donjon_center0.z, new_pos.z, uniform::donjon_larger0);
    local::simple_vertex(new_pos,
                         in_color_s,
                         out_position_s,
                         out_color_s);
}
void donjon_fragment(in  vec3 frag_position, in vec4 frag_color,
                     out vec4 final_color)
{
    final_color = vec4(clamp(frag_color.xyz, vec3(0.0) ,vec3(1.0)), 1.0);
    float vd = local::voronoi3D_distance(frag_position, 12.0f);
    final_color = final_color*(1.0f+vd/8.0f);
}

void cathedral_vertex(in  vec3 vpos, in  vec4 vcolor,
                      out vec3 fpos, out vec4 fcolor)
{
    float ach = uniform::cathedral_acheivement;
    vec3 new_pos;

    if(ach < 1.0f)
    {
        new_pos = mix(uniform::cathedral_center, vpos, ach);
        vec3 dep = new_pos - uniform::cathedral_center;
        float dvor = local::voronoi3D_distance(new_pos, 20.0f);
        new_pos = mix(uniform::cathedral_center + dep*ach/(0.001+sin(dvor)*1.2f), new_pos, ach);
    }
    else
    {
        new_pos = vpos;
    }
    

    local::simple_vertex(new_pos, vcolor, fpos, fcolor);
}

void cathedral_fragment(in  vec3 frag_position, in  vec4 frag_color,
                        out vec4 final_color  , out vec4 bloom_color)
{
    final_color = vec4(clamp(frag_color.xyz, vec3(0.0) ,vec3(1.0)), 1.0);   
    float dtocenter = length(frag_position-uniform::cathedral_center);
    float vd = local::voronoi3D_distance(frag_position + dtocenter, 15.0f);
    float center_d_divisor = mix(dtocenter*dtocenter, 0.0f, uniform::cathedral_acheivement);
    final_color = final_color*(0.9f+vd/4.0f)*clamp((1.0f+center_d_divisor/500.0f), 0.0, 10.0f);

    if(uniform::cathedral_acheivement<1.0f && length(final_color) >= 2.2f)
    {
        bloom_color = final_color;
    }
    else
    {
        bloom_color = vec4(0.0);
    }
}
