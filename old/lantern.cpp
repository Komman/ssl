#include "lantern.hpp"

using namespace std;
using namespace glm;

Lantern::Lantern(ControlledDrawer& drawer,
		   const glm::vec3& size,
		   const glm::vec3& light_color,
		   const std::vector<lanternDisposition>& dispositions,
		   const glm::vec2& start_stop)
	: _center(0.0),
	  _size(size),
	  _light_color(light_color),
	  _start_stop(start_stop),
	  _indices(STATIC_DRAW),
	  _vertexs(STATIC_DRAW),
	  _colors(STATIC_DRAW),
	  _deps(STATIC_DRAW),
	  _angles(STATIC_DRAW),
	  _buffers({&_vertexs, &_colors, &_deps, &_angles}),
	  _drawer(drawer),
	  _achievement_step(1)  
{
	_drawer.depth(true);

	this->fill_instances(dispositions);

	this->build_lantern();

	_deps_save = _deps.get_value();
}

void Lantern::build_support(float support_coeff, float support_depth_coeff)
{
	float mindirection  = std::min(_size.x, _size.y);
	float support_size  = mindirection*support_coeff/2.0f;
	float support_depth = _size.z*support_depth_coeff - _size.z/2.0f;
	vec3  support_color = vec3(1.0)*0.2f;

	this->add_point(vec3(- support_size, - support_size, 0.0f - _size.z/2.0f), support_color*color_random()/2.0f);
	this->add_point(vec3(- support_size, + support_size, 0.0f - _size.z/2.0f), support_color*color_random()/2.0f);
	this->add_point(vec3(+ support_size, + support_size, 0.0f - _size.z/2.0f), support_color*color_random()/2.0f);
	this->add_point(vec3(+ support_size, - support_size, 0.0f - _size.z/2.0f), support_color*color_random()/2.0f);

	this->add_point(vec3(- support_size, - support_size, support_depth ), support_color*color_random());
	this->add_point(vec3(- support_size, + support_size, support_depth ), support_color*color_random());
	this->add_point(vec3(+ support_size, + support_size, support_depth ), support_color*color_random());
	this->add_point(vec3(+ support_size, - support_size, support_depth ), support_color*color_random());

	_indices.add_rectangle(0,1,2,3);
	// _indices.add_rectangle(7,6,5,4);

	for(int i=0; i<4; i++)
	{
		_indices.add_rectangle(4+i, 4+(5+i)%4, (i+1)%4, i);
	}
}

/*
10     ^13 ^     9

       6   5

11     7   4    8
                
         12
*/

float Lantern::color_random()
{
	return utils::random_float(0.5,1.5);
}

void Lantern::build_lantern()
{
	float support_size_coeff  = 0.3f;
	float support_depth_coeff = 0.6f;
 	this->build_support(support_size_coeff, support_depth_coeff);
	
 	vec3 torch_color = vec3(0.2,0.1,0.05)*1.0f;
 	float start_torch_z = (support_depth_coeff-0.5f) * _size.z;
	float mindirection  = std::min(_size.x, _size.y);
	float support_depth = _size.z*support_depth_coeff - _size.z/2.0f;
	float support_size  = mindirection*support_size_coeff/2.0f;

	vec3 top_triangle[3]={
		vec3(- _size.x/2.0f, + _size.y/2.0f, support_depth),
		vec3(+ _size.x/2.0f, + _size.y/2.0f, support_depth),
		vec3(0.0,  _size.y/2.0f, _size.z/2.0f)
	};

	this->add_point(vec3(- _size.x/2.0f, - support_size, support_depth), torch_color*color_random());
	this->add_point(top_triangle[0], torch_color*color_random());
	this->add_point(top_triangle[1], torch_color*color_random());
	this->add_point(vec3(+ _size.x/2.0f, - support_size, support_depth), torch_color*color_random());
	
 	this->add_point(vec3(0.0, -_size.y/2.0f, start_torch_z), torch_color*color_random());
 	this->add_point(top_triangle[2], torch_color*color_random());

 	_indices.add_triangle(8,11,12);
 	_indices.add_rectangle(8,9,10,11);
 	_indices.add_triangle(10,9,13);

 	_indices.add_triangle(11, 10, 13);
 	_indices.add_triangle(9 , 8 , 13);
 	_indices.add_triangle(12, 11, 13);
 	_indices.add_triangle(8 , 12, 13);

	float ligth_proportion  = 0.5;
	float ligth_heigth      = _size.z/5.0f;

 	vec3 top_triangle_center = (top_triangle[0] + top_triangle[1] + top_triangle[2])/3.0f;

 	vec3 ligth_triangle[3];
 	for(int i=0; i<3; i++)
 	{
 		ligth_triangle[i] = glm::mix(top_triangle[i], top_triangle_center, ligth_proportion);
 	}

 	int start_light = _vertexs.size();
 	for(int i=0; i<3; i++)
 	{
 		this->add_point(ligth_triangle[i], _light_color);
 	}
 	for(int i=0; i<3; i++)
 	{
 		this->add_point(ligth_triangle[i]+vvv::y()*ligth_heigth, _light_color);
 	}
 	for(int i=0; i<3; i++)
 	{
 		_indices.add_rectangle(start_light + i,
 							   start_light + i       + 3,
 							   start_light + (i+1)%3 + 3,
 							   start_light + (i+1)%3);
 	}
 	_indices.add_triangle(start_light+3+0,
 						  start_light+3+2,
 						  start_light+3+1);

}

int Lantern::add_point(const glm::vec3& coord, const glm::vec3 color)
{
	int ret = _vertexs.size();
	_vertexs.add_value(coord);
	_colors.add_value(color);
	return ret;
}

void Lantern::fill_instances(const std::vector<lanternDisposition>& dispositions)
{
	for(const auto& disp : dispositions)
	{
		_deps.add_value(disp.position);
		_angles.add_value(vec2(disp.angle, disp.size));
	}
}

void Lantern::set_coordinates(const std::vector<glm::vec3>& new_coord)
{
	if(new_coord.size() > _deps.size())
	{
		err("Lantern::set_coordinates: cannot put more coordinates than the number of lanterns");
	}

	for(int i=0; i<(int)new_coord.size(); i++)
	{
		_deps.change_subvalue(i, new_coord[i]);
	}
}

void  Lantern::set_build_achievement(float achievement)
{
	if(achievement>=0.0)
	{
		if(achievement<1.0)
		{
			_achievement_step = 0;

			for(int i=0; i<(int)_deps.size(); i++)
			{
				_deps.change_subvalue(i, vec3(_deps_save[i].x,
											  _deps_save[i].y*achievement,
											  _deps_save[i].z));
			}
		}
		else
		{
			if(_achievement_step != 1)
			{
				_achievement_step = 1;
				_deps.change_value(_deps_save);
			} 
		}
	}
	else
	{
		if(_achievement_step != -1)
		{
			_achievement_step = -1;

			for(int i=0; i<(int)_deps.size(); i++)
			{
				_deps.change_subvalue(i, vec3(_deps_save[i].x,
											  _deps_save[i].y*achievement,
											  _deps_save[i].z));
			}
		}
	}
}

float Lantern::get_start_achievement() const
{
	return _start_stop.x;
}
float Lantern::get_end_achievement()   const
{
	return _start_stop.y;
}


const glm::vec3& Lantern::get_center()    const
{
	return _center;
}

const glm::vec3& Lantern::get_direction() const
{
	return vvv::y();
}

Drawer* Lantern::get_drawer()
{
	return &_drawer;
}

bool Lantern::multisample_draw() const
{
	return true;
}

bool Lantern::bloom_draw() const
{
	return true;
}

const std::vector<const BasicBuffer*>& Lantern::get_buffers()  const
{
	return _buffers;
}

const ElementBuffer& Lantern::get_elements() const
{
	return _indices;
}

