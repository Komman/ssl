#ifndef _CATHEDRAL_
#define _CATHEDRAL_

#include "../motor/controlled_drawer.hpp"
#include "../motor/planar_symmetry.hpp"

#include "castle_graph.hpp"
#include "l_base_buildor.hpp"
#include "piler_connectors.hpp"

struct cathedralParams
{
	float relative_ext        = 0.02f;
	float floor_height        = 3.0f;

	std::string uniform_achievement_name = "cathedral_acheivement";
	std::string uniform_center_name = "cathedral_center";
};

class Cathedral : public VertexMesh
{
public:
	Cathedral(const ShaderGenerator& generator, 
		      const glm::vec3& plateform_top,
		      const glm::vec3& plateform_bottom,
		      const cathedralParams& geometric_params,
		      const glm::vec3& plateform_color,
		      const timeAndLast& time_and_last,
		      DrawerPackController& drawer_controller);


public: // virtual

	const glm::vec3& get_center()    const override;
	const glm::vec3& get_direction() const override;

	const std::vector<const BasicBuffer*>& get_buffers()  const;
	const ElementBuffer&                   get_elements() const;

	virtual void  set_build_achievement(float achievement) override;
	virtual float get_start_achievement() const override;
	virtual float get_end_achievement()   const override;
	virtual bool  special_drawing() const;

	virtual Drawer* get_drawer() override;
	virtual bool    multisample_draw() const override;
	virtual bool    bloom_draw() const override;

protected:

	void add_point(const glm::vec3& coord, const glm::vec3& color);

private:

	void build_floor();
	//  Fill the _afterbase_infos member
	void bluild_base();
	void build_upbase();

	float get_piler_x_center(const pilerTopIndexor& piler) const;
	void grow_piler(const pilerTopIndexor& piler_top, float height);
	void set_piler_hight(const pilerTopIndexor& piler_top, float height);
	void link_triangle_roofs(const glm::uvec2& front1, const glm::uvec2& front2);
	plateformIndices create_interplateform(const glm::vec3& lowX, const glm::vec3& greatX, float length, const glm::vec4& color);
	// bipartend must be from the outside to the inside of the cathedral 
	void set_roof_color(const std::vector<unsigned int>& bipartend, const std::vector<glm::vec4>& colors);
	// bipart2 must be at the right of bipart1 for a front culling 
	void link_bipartite(const std::vector<unsigned int>& bipart1, const std::vector<unsigned int>& bipart2);
	// returns the list of the indices of new vertexs projeceted on .x = x in  the same order as bipartend
	// x must lower than the vertexs.x in the list for front culling
	std::vector<unsigned int> link_discontinuous(const std::vector<unsigned int>& bipartend, float x);
	// Returns the available plateformes created (x: bottom index, y: top index)
	std::vector<glm::uvec2> random_cathedral_word(unsigned int first_piler, unsigned int last_piler);
	// std::vector<pilerSet> get_pilers_by_colors(unsigned int first, unsigned int last);
	void grow_interval(unsigned int first, unsigned int last, float height);

	unsigned int symm(unsigned int index);
	void check_piler_index(unsigned int piler_index);

private:

	Uniform<float> _achievement;
	Uniform<glm::vec3> _unif_center;

	ElementBuffer                   _indices; 
	ArrayBuffer<glm::vec3>          _vertexs;
	ArrayBuffer<glm::vec4>          _colors;
	std::vector<const BasicBuffer*> _buffers;

	ControlledDrawer _drawer;
	bool             _to_draw;

	cathedralParams _geometry;

	float _start_time;
	float _end_time;

	float _overbound;
	glm::vec3 _top;
	glm::vec3 _bottom;
	glm::vec3 _center;
	glm::vec3 _floorcolor;
	glm::vec4 _cathcolor;
	glm::vec4 _cathroofcolor;

	std::unique_ptr<PlanarSymmetry<glm::vec4>> _symmetry;
	afterBaseInfo  _afterbase_infos;
};

#endif //_CATHEDRAL_
