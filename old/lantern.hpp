#ifndef _LANTERN_HPP_
#define _LANTERN_HPP_

#include "../motor/controlled_drawer.hpp"

#include "castle_graph.hpp"

struct lanternDisposition
{
	glm::vec3 position;
	float     angle;
};

class Lantern : public VertexMesh
{
public:

	Lantern(ControlledDrawer& drawer,
			const glm::vec3& size,
			const glm::vec3& light_color,
			const std::vector<lanternDisposition>& dispositions,
			const glm::vec2& start_stop);

	const std::vector<const BasicBuffer*>& get_buffers()  const;
	const ElementBuffer&                   get_elements() const;

	const glm::vec3& get_center()    const override;
	const glm::vec3& get_direction() const override;

	Drawer* get_drawer()             override;
	bool    multisample_draw() const override;
	bool    bloom_draw()       const override;

	void    set_build_achievement(float achievement) override;
	float   get_start_achievement() const override;
	float   get_end_achievement()   const override;

	void set_coordinates(const std::vector<glm::vec3>& new_coord);

private:

	// Returns the index
	int add_point(const glm::vec3& coord, const glm::vec3 color);
	void fill_instances(const std::vector<lanternDisposition>& dispositions);

	void build_lantern();
	void build_support(float support_coeff, float support_depth_coeff);
	float color_random();

private:

	glm::vec3 _center;
	glm::vec3 _size;
	glm::vec3 _light_color;
	glm::vec2 _start_stop;

	ElementBuffer          _indices; 
	ArrayBuffer<glm::vec3> _vertexs;
	ArrayBuffer<glm::vec3> _colors;
	ArrayBuffer<glm::vec3> _deps;
	ArrayBuffer<float>     _angles;

	std::vector<const BasicBuffer*> _buffers;

	ControlledDrawer& _drawer;

	std::vector<glm::vec3> _deps_save;
	int _achievement_step;

};

#endif //_LANTERN_HPP_
