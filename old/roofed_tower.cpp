#include "roofed_tower.hpp"

using namespace glm;
using namespace std;

RoofedTower::RoofedTower(Drawer* drawer,
						 const glm::vec3& center,
						 const glm::vec3& direction,
						 const std::vector<stageRelief>& reliefs,
						 const glm::vec4& base_color,
						 float hat,
						 const glm::vec4& roof_color)
	: Tower(drawer, center, direction, reliefs, base_color),
	  _roof_spique_index(-1),
	  _hat(hat),
	  _roof_color(roof_color),
	  _start_height(center.y),
	  _animation_hat(2.0f * hat)
{

}

void RoofedTower::finalize_roof(const std::vector<int>& top_indices)
{
	if(_roof_spique_index != -1)
		err("RoofedTower has already been initialized");

	if(_hat > 0.0f && _vertexs.size()>=8)
	{
		_top_indices = top_indices;
		_roof_spique_index = _vertexs.size();
		vec3 hat_pos = this->get_center();
		hat_pos.y = _vertexs[top_indices[0]].y + _hat;
		this->add_point(hat_pos, _roof_color);
		this->build_roof_indices();

		for(int i=0; i<(int)top_indices.size(); i++)
		{
			_colors.change_subvalue(top_indices[i],   _roof_color/1.3f);
			_colors.change_subvalue(top_indices[i]-1, _roof_color/3.0f);
		
			_start_height = std::max(_start_height, _vertexs[top_indices[i]].y);
		}
	}
}

void RoofedTower::build_roof_indices()
{
	if(_top_indices.size() < 4)
	{
		err("Try to build roof indices while they are not finalized");
	}

	for(int i=0; i<(int)_top_indices.size(); i++)
	{
		int ipp = (i+1)%(_top_indices.size());
		_indices.add_triangle(_top_indices[i],
							  _top_indices[ipp],
							  _roof_spique_index);
	}
}

float RoofedTower::roof_start_time() const
{
	return 0.8f;
}

void  RoofedTower::set_roof_acheivement(float achievement)
{
	if(_roof_spique_index < 0)
	{
		err("call to RoofedTower::set_roof_acheivement while the tower is not finalized");
	}

	int i = _roof_spique_index;
	vec3 yaligned_center = _vertexs[i];
	yaligned_center.y = 0.0f;

	if(achievement < 0.0)
	{
		_vertexs.change_subvalue(i, yaligned_center + vvv::y()*_start_height);
	}
	else if(achievement > 1.0)
	{
		this->build_roof_indices();
		_vertexs.change_subvalue(i, yaligned_center + vvv::y()*_start_height + _hat);
	}
	else
	{
		this->build_roof_indices();
		_vertexs.change_subvalue(i, yaligned_center + vvv::y()*glm::mix(_start_height, _start_height + _hat, achievement));
	}
}
