#include "tower_interface.hpp"


TowerInterface::TowerInterface() 
	: _indices(DYNAMIC_DRAW),
	  _vertexs(DYNAMIC_DRAW), 
	  _colors(DYNAMIC_DRAW),
	  _buffers({&_vertexs, &_colors}),
	  _individual_drawing(true)
{

}

TowerInterface::~TowerInterface()
{

}

void TowerInterface::put_in_massive_draw()
{
	_individual_drawing = false;
}

const std::vector<const BasicBuffer*>& TowerInterface::get_buffers()  const 
{
	return _buffers;
}

const ElementBuffer&                   TowerInterface::get_elements() const 
{
	return _indices;
}

void TowerInterface::add_point(const glm::vec3& position, const glm::vec4& color)
{
	_vertexs.add_value(position);
	_colors.add_value(color);
}

unsigned int TowerInterface::vertexs_size() const
{
	return _vertexs.size();
}