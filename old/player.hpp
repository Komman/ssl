#ifndef _PLAYER_HPP_
#define _PLAYER_HPP_

#include "../motor/unstored_point.hpp"
#include "../motor/target.hpp"

#include "basic_ground.hpp"

class Player : public ssl::UniformCamera
{
public:

	static constexpr float NEAR = 0.05;
	static constexpr float FAR  = 2000.0;

	static inline std::string MVP_unif       = "MVP";
	static inline std::string position_unif  = "camera_position";
	static inline std::string direction_unif = "camera_direction";

public:

	Player(const glm::vec3&   start,
		   float speed,
		   float jump_speed);

	void change_ground(BasicGround const* ground);

	const BasicTarget& get_target(); 
	const BasicDirectedPoint& get_space_point() const;


protected:

	void frame_update_more_actions() override;
	
	//virtual void  keys_reaction(const std::vector<unsigned int>& pressed_keys_indices);
	//virtual void mouse_reaction(const glm::vec2& mouse_moving_pixel);

	BasicGround const* _ground;

private:
	void check_ground() const;

	glm::vec3 _position;
	glm::vec3 _vecspeed;
	PointerTarget _selftarget;

	float _speed;
	float _jump_speed;
	float _head_heigth;
	float _walk_slope;

	bool _on_ground;

	UnstoredDirectedPoint _space_point;
};

#endif //_PLAYER_HPP_
