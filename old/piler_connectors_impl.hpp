template<pilerRelationType SYMMETRY, unsigned int ARITY>
unsigned int GlobalPilerConnection<SYMMETRY, ARITY>::symm(unsigned int index)
{
	return _symmetry->symmetrical(index);
}

template<pilerRelationType SYMMETRY, unsigned int ARITY>
std::vector<unsigned int> GlobalPilerConnection<SYMMETRY, ARITY>::create_pilier_triangle(const pilerTopIndexor& piler, float triangle_h)
{
	std::vector<unsigned int> ret(TRPIILER_INDICES_COUNT,0);

	float tripiler_h = abs(_vertexs[piler.south_west].x - _vertexs[piler.north_west].x);

	// glm::vec3 piler_center = piler.center(_vertexs);
	// axe_center.y+=triangle_h;
	glm::vec3 north_center = (_vertexs[piler.north_east] + _symmetry->symmetrical(_vertexs[piler.north_east]))/2.0f;
	glm::vec3 south_center = (_vertexs[piler.south_east] + _symmetry->symmetrical(_vertexs[piler.south_east]))/2.0f;
	north_center.y+=triangle_h;
	south_center.y+=triangle_h;

	unsigned int north_tricenter_top = _symmetry->place_asymmetrical(north_center + vvv::y()*tripiler_h, _colors[piler.north_west]);
	unsigned int south_tricenter_top = _symmetry->place_asymmetrical(south_center + vvv::y()*tripiler_h, _colors[piler.south_west]);
	unsigned int north_tricenter_bot = _symmetry->place_asymmetrical(north_center, _colors[piler.north_east]/4.0f);
	unsigned int south_tricenter_bot = _symmetry->place_asymmetrical(south_center, _colors[piler.south_east]);

	_symmetry->link_rectangle(piler.south_west, south_tricenter_top, north_tricenter_top, piler.north_west);
	_symmetry->link_rectangle(piler.south_east, piler.north_east, north_tricenter_bot, south_tricenter_bot);
	_symmetry->link_rectangle(piler.north_east, piler.north_west, north_tricenter_top, north_tricenter_bot);
	_symmetry->link_rectangle(piler.south_east, south_tricenter_bot, south_tricenter_top, piler.south_west);

	ret[TRIPILER_NORTH_TOP] = north_tricenter_top;
	ret[TRIPILER_SOUTH_TOP] = south_tricenter_top;
	ret[TRIPILER_SOUTH_BOT] = south_tricenter_bot;
	ret[TRIPILER_NORTH_BOT] = north_tricenter_bot;

	return ret;
}



template<pilerRelationType SYMMETRY, unsigned int ARITY>
std::vector<unsigned int> GlobalPilerConnection<SYMMETRY, ARITY>::create_pilier_font_vertexs(const std::vector<unsigned int>& tripiler_indices, const pilerTopIndexor& piler, bool roof)
{
	std::vector<unsigned int> reti;

	glm::uvec2 ret(0);

	if(tripiler_indices.size() != 4)
	{
		err("GlobalPilerConnection<SYMMETRY, ARITY>::create_pilier_font_vertexs(): tripiler_indices ont of size 4 (the 4 pilers indices of the top of the triangle piler)");
	}


	glm::vec3 top   = (_vertexs[tripiler_indices[TRIPILER_SOUTH_BOT]] + _vertexs[tripiler_indices[TRIPILER_NORTH_BOT]])/2.0f;
	glm::vec3 right = (_vertexs[piler.south_east] + _vertexs[piler.north_east])/2.0f;

	glm::vec4 color_up;
	glm::vec4 color_down;

	if(roof)
	{
		color_up   = _cathroofcolor;
		color_down = _cathroofcolor;
	}
	else
	{
		color_up   = _cathcolor/2.0f;
		color_down = _cathcolor;
	}

	ret.x = _symmetry->place_asymmetrical(top, color_up);
	ret.y = _symmetry->place_symmetrical(right, color_down);
	
	
	if(right.y > _afterbase_infos.corners[0].y)
	{
		right.y = _afterbase_infos.corners[0].y;
		reti.push_back(_symmetry->place_symmetrical(right, color_down));
	}
	reti.push_back(ret.y);
	reti.push_back(ret.x);

	return reti;
}

template<pilerRelationType SYMMETRY, unsigned int ARITY>
void GlobalPilerConnection<SYMMETRY, ARITY>::link_piler_inside(unsigned int piler)
{
	this->check_piler_index(piler);

	if(	   _vertexs[_afterbase_infos.pilers_top[piler].south_east].x > _afterbase_infos.corners[0].x
		&& _vertexs[_afterbase_infos.pilers_top[piler].north_east].x < _afterbase_infos.corners[1].x)
	{
		this->link_piler_inside_indexor(_afterbase_infos.pilers_top[piler],
							     	    _afterbase_infos.pilers_bot[piler]);
	}	
}

template<pilerRelationType SYMMETRY, unsigned int ARITY>
void GlobalPilerConnection<SYMMETRY, ARITY>::link_piler_inside_indexor(const pilerTopIndexor& piler_top, const pilerTopIndexor& piler_bot)
{
	_symmetry->link_rectangle(piler_top.north_east, piler_top.south_east, piler_bot.south_east, piler_bot.north_east);
}

template<pilerRelationType SYMMETRY, unsigned int ARITY>
std::vector<unsigned int> GlobalPilerConnection<SYMMETRY, ARITY>::create_pilier_font_triangle(const std::vector<unsigned int>& tripiler_indices, const pilerTopIndexor& piler, bool roof, bool ext_face_inv)
{
	auto ret = this->create_pilier_font_vertexs(tripiler_indices, piler, roof);

	if(ret.size() < 2 || ret.size() > 3)
	{
		err("GlobalPilerConnection<SYMMETRY, ARITY>::create_pilier_font_triangle(): create_pilier_font_vertexs() returned wrong vector: size = " + std::to_string(ret.size()) + "!=2 ou 3");
	}

	if(ret.size() == 2)
	{
		if(ext_face_inv)
		{
			_symmetry->link_triangle(ret[0], ret[1], _symmetry->symmetrical(ret[0]));
		}
		else
		{
			_symmetry->link_triangle(ret[1], ret[0], _symmetry->symmetrical(ret[0]));
		}
	}
	if(ret.size() == 3)
	{
		if(ext_face_inv)
		{
			_symmetry->link_triangle(ret[1], ret[2], _symmetry->symmetrical(ret[1]));
			_symmetry->link_rectangle(ret[1], _symmetry->symmetrical(ret[1]), _symmetry->symmetrical(ret[0]), ret[0]);
		}
		else
		{
			_symmetry->link_triangle(ret[2], ret[1], _symmetry->symmetrical(ret[1]));
			_symmetry->link_rectangle(ret[1], ret[0], _symmetry->symmetrical(ret[0]), _symmetry->symmetrical(ret[1]));
		}
	}

	return ret;
}

template<pilerRelationType SYMMETRY, unsigned int ARITY>
void GlobalPilerConnection<SYMMETRY, ARITY>::grow_piler(const pilerTopIndexor& piler_top, float height)
{
	_vertexs.change_subvalue(piler_top.north_east, _vertexs[piler_top.north_east] + vvv::y()*height);
	_vertexs.change_subvalue(piler_top.north_west, _vertexs[piler_top.north_west] + vvv::y()*height);
	_vertexs.change_subvalue(piler_top.south_west, _vertexs[piler_top.south_west] + vvv::y()*height);
	_vertexs.change_subvalue(piler_top.south_east, _vertexs[piler_top.south_east] + vvv::y()*height);
	
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.north_east), _vertexs[_symmetry->symmetrical(piler_top.north_east)] + vvv::y()*height);
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.north_west), _vertexs[_symmetry->symmetrical(piler_top.north_west)] + vvv::y()*height);
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.south_west), _vertexs[_symmetry->symmetrical(piler_top.south_west)] + vvv::y()*height);
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.south_east), _vertexs[_symmetry->symmetrical(piler_top.south_east)] + vvv::y()*height);
}

template<pilerRelationType SYMMETRY, unsigned int ARITY>
void GlobalPilerConnection<SYMMETRY, ARITY>::set_piler_hight(const pilerTopIndexor& piler_top, float height)
{
	float diffs[4]={
		_vertexs[piler_top.north_east].y - _vertexs[piler_top.north_east].y,
		_vertexs[piler_top.north_west].y - _vertexs[piler_top.north_east].y,
		_vertexs[piler_top.south_west].y - _vertexs[piler_top.north_east].y,
		_vertexs[piler_top.south_east].y - _vertexs[piler_top.north_east].y
	};

	_vertexs.change_subvalue(piler_top.north_east, ytozero(_vertexs[piler_top.north_east]) + vvv::y()*(height+diffs[0]));
	_vertexs.change_subvalue(piler_top.north_west, ytozero(_vertexs[piler_top.north_west]) + vvv::y()*(height+diffs[1]));
	_vertexs.change_subvalue(piler_top.south_west, ytozero(_vertexs[piler_top.south_west]) + vvv::y()*(height+diffs[2]));
	_vertexs.change_subvalue(piler_top.south_east, ytozero(_vertexs[piler_top.south_east]) + vvv::y()*(height+diffs[3]));
	
	float diffsym[4]={
		_vertexs[_symmetry->symmetrical(piler_top.north_east)].y - _vertexs[_symmetry->symmetrical(piler_top.north_east)].y,
		_vertexs[_symmetry->symmetrical(piler_top.north_west)].y - _vertexs[_symmetry->symmetrical(piler_top.north_east)].y,
		_vertexs[_symmetry->symmetrical(piler_top.south_west)].y - _vertexs[_symmetry->symmetrical(piler_top.north_east)].y,
		_vertexs[_symmetry->symmetrical(piler_top.south_east)].y - _vertexs[_symmetry->symmetrical(piler_top.north_east)].y
	};

	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.north_east), ytozero(_vertexs[_symmetry->symmetrical(piler_top.north_east)]) + vvv::y()*(height+diffsym[0]));
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.north_west), ytozero(_vertexs[_symmetry->symmetrical(piler_top.north_west)]) + vvv::y()*(height+diffsym[1]));
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.south_west), ytozero(_vertexs[_symmetry->symmetrical(piler_top.south_west)]) + vvv::y()*(height+diffsym[2]));
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.south_east), ytozero(_vertexs[_symmetry->symmetrical(piler_top.south_east)]) + vvv::y()*(height+diffsym[3]));
}

template<pilerRelationType SYMMETRY, unsigned int ARITY>
void GlobalPilerConnection<SYMMETRY, ARITY>::flat_link_pilers(const pilerTopIndexor& piler_top)
{
	float height = abs(_vertexs[piler_top.north_east].x - _vertexs[piler_top.south_east].x);
	
	_vertexs.change_subvalue(piler_top.north_west, _vertexs[piler_top.north_west] + vvv::y()*height);
	_vertexs.change_subvalue(piler_top.south_west, _vertexs[piler_top.south_west] + vvv::y()*height);
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.north_west), _vertexs[_symmetry->symmetrical(piler_top.north_west)] + vvv::y()*height);
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.south_west), _vertexs[_symmetry->symmetrical(piler_top.south_west)] + vvv::y()*height);
	
	_symmetry->link_rectangle(piler_top.north_west, piler_top.south_west,  _symmetry->symmetrical(piler_top.south_west), _symmetry->symmetrical(piler_top.north_west));
	_symmetry->link_rectangle(piler_top.north_east, _symmetry->symmetrical(piler_top.north_east),  _symmetry->symmetrical(piler_top.south_east), piler_top.south_east);
	_symmetry->link_rectangle(piler_top.north_west, _symmetry->symmetrical(piler_top.north_west),  _symmetry->symmetrical(piler_top.north_east), piler_top.north_east);
	_symmetry->link_rectangle(piler_top.south_west, piler_top.south_east,  _symmetry->symmetrical(piler_top.south_east), _symmetry->symmetrical(piler_top.south_west));
}

template<pilerRelationType SYMMETRY, unsigned int ARITY>
void GlobalPilerConnection<SYMMETRY, ARITY>::fusion_pilers(const pilerTopIndexor& piler1, const pilerTopIndexor& piler2)
{
	float height = abs(_vertexs[piler1.north_east].x - _vertexs[piler1.south_east].x);
	
	_vertexs.change_subvalue(piler1.south_east, _vertexs[piler1.south_east] + vvv::y()*height);
	_vertexs.change_subvalue(piler1.south_west, _vertexs[piler1.south_west] + vvv::y()*height);
	_vertexs.change_subvalue(_symmetry->symmetrical(piler1.south_east), _vertexs[_symmetry->symmetrical(piler1.south_east)] + vvv::y()*height);
	_vertexs.change_subvalue(_symmetry->symmetrical(piler1.south_west), _vertexs[_symmetry->symmetrical(piler1.south_west)] + vvv::y()*height);
	_vertexs.change_subvalue(piler2.north_east, _vertexs[piler2.north_east] + vvv::y()*height);
	_vertexs.change_subvalue(piler2.north_west, _vertexs[piler2.north_west] + vvv::y()*height);
	_vertexs.change_subvalue(_symmetry->symmetrical(piler2.north_east), _vertexs[_symmetry->symmetrical(piler2.north_east)] + vvv::y()*height);
	_vertexs.change_subvalue(_symmetry->symmetrical(piler2.north_west), _vertexs[_symmetry->symmetrical(piler2.north_west)] + vvv::y()*height);
	
	_symmetry->link_rectangle(piler1.south_west, piler1.south_east, piler2.north_east, piler2.north_west);
	_symmetry->link_rectangle(piler1.north_west, piler2.south_west, piler2.south_east, piler1.north_east);
	_symmetry->link_rectangle(piler1.north_west, piler1.south_west, piler2.north_west, piler2.south_west);
	
	_symmetry->link_rectangle(piler1.north_east, piler2.south_east, piler2.north_east, piler1.south_east);
}


template<pilerRelationType SYMMETRY, unsigned int ARITY>
glm::uvec2 GlobalPilerConnection<SYMMETRY, ARITY>::fusion_square_pilers(const pilerTopIndexor& piler1, const pilerTopIndexor& piler2)
{
	glm::uvec2 ids;

	float height = abs(_vertexs[piler1.north_east].x - _vertexs[piler1.south_east].x);
	
	_vertexs.change_subvalue(piler1.south_west, _vertexs[piler1.south_west] + vvv::y()*height);
	_vertexs.change_subvalue(piler2.north_west, _vertexs[piler2.north_west] + vvv::y()*height);
	_vertexs.change_subvalue(_symmetry->symmetrical(piler1.south_west), _vertexs[_symmetry->symmetrical(piler1.south_west)] + vvv::y()*height);
	_vertexs.change_subvalue(_symmetry->symmetrical(piler2.north_west), _vertexs[_symmetry->symmetrical(piler2.north_west)] + vvv::y()*height);
	
	ids.x = _symmetry->place_symmetrical(_vertexs[piler1.north_east] + vvv::y()*height, _colors[piler1.south_west]);
	ids.y = _symmetry->place_symmetrical(_vertexs[piler2.south_east] + vvv::y()*height, _colors[piler2.north_west  ]);
	
	_symmetry->link_rectangle(piler1.south_west, ids.x, ids.y, piler2.north_west);
	_symmetry->link_rectangle(piler1.north_west, piler2.south_west, piler2.south_east, piler1.north_east);
	_symmetry->link_rectangle(piler1.north_west, piler1.south_west, piler2.north_west, piler2.south_west);
	_symmetry->link_rectangle(ids.x, piler1.north_east, piler2.south_east, ids.y);

	_symmetry->link_rectangle(piler1.south_west, _symmetry->symmetrical(piler1.south_west), _symmetry->symmetrical(ids.x), ids.x);
	_symmetry->link_rectangle(piler1.south_west, piler1.south_east, _symmetry->symmetrical(piler1.south_east), _symmetry->symmetrical(piler1.south_west));
	_symmetry->link_rectangle(piler1.south_east, piler1.north_east, _symmetry->symmetrical(piler1.north_east), _symmetry->symmetrical(piler1.south_east));
	_symmetry->link_rectangle(ids.x, _symmetry->symmetrical(ids.x), _symmetry->symmetrical(piler1.north_east), piler1.north_east);

	_symmetry->link_rectangle(piler2.north_west, ids.y                                    , _symmetry->symmetrical(ids.y)            , _symmetry->symmetrical(piler2.north_west));
	_symmetry->link_rectangle(piler2.north_west, _symmetry->symmetrical(piler2.north_west), _symmetry->symmetrical(piler2.north_east), piler2.north_east                        );
	_symmetry->link_rectangle(piler2.north_east, _symmetry->symmetrical(piler2.north_east), _symmetry->symmetrical(piler2.south_east), piler2.south_east                        );
	_symmetry->link_rectangle(ids.y            , piler2.south_east                        , _symmetry->symmetrical(piler2.south_east), _symmetry->symmetrical(ids.y)            );


	return ids;
}

template<pilerRelationType SYMMETRY, unsigned int ARITY>
void GlobalPilerConnection<SYMMETRY, ARITY>::end_piler(const pilerTopIndexor& piler_top)
{
	float height = abs(_vertexs[piler_top.north_east].x - _vertexs[piler_top.south_east].x);

	_vertexs.change_subvalue(piler_top.north_east  , _vertexs[piler_top.north_east  ] + vvv::y()*height);
	_vertexs.change_subvalue(piler_top.south_east, _vertexs[piler_top.south_east] + vvv::y()*height);
	
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.north_east)  , _vertexs[_symmetry->symmetrical(piler_top.north_east  )] + vvv::y()*height);
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.south_east), _vertexs[_symmetry->symmetrical(piler_top.south_east)] + vvv::y()*height);

	_symmetry->link_rectangle(piler_top.north_east, piler_top.north_west, piler_top.south_west, piler_top.south_east);
}


template<pilerRelationType SYMMETRY, unsigned int ARITY>
void GlobalPilerConnection<SYMMETRY, ARITY>::check_piler_index(unsigned int piler_index)
{
	if(piler_index >= _afterbase_infos.pilers_top.size())
	{
		err("GlobalPilerConnection<SYMMETRY, ARITY>::check_piler_index(): piler index out of range: " + std::to_string(piler_index) + "/" + std::to_string(_afterbase_infos.pilers_top.size()));
	}
}

template<pilerRelationType SYMMETRY, unsigned int ARITY>
std::vector<unsigned int> GlobalPilerConnection<SYMMETRY, ARITY>::create_piler_vertex_side(const pilerTopIndexor& piler, const glm::vec4& color_bot, const glm::vec4& color_top)
{
	std::vector<unsigned int> ret;

	glm::vec3 east_midle = (_vertexs[piler.north_east] + _vertexs[piler.south_east])/2.0f;

	glm::vec3 bot_east_midle = east_midle;
	bot_east_midle.y = _afterbase_infos.corners[0].y;

	ret.push_back(_symmetry->place_symmetrical(bot_east_midle, color_bot));
	ret.push_back(_symmetry->place_symmetrical(east_midle, color_top));

	return ret;
}

template<pilerRelationType SYMMETRY, unsigned int ARITY>
std::vector<unsigned int> GlobalPilerConnection<SYMMETRY, ARITY>::create_square_front_face(unsigned int piler, bool north_face_visible, bool south_face_visible, const glm::vec4& color_bot, const glm::vec4& color_top)
{
	auto& infos  = _afterbase_infos;
	auto& pilers = infos.pilers_top;

	auto face = this->create_piler_vertex_side(pilers[piler], color_bot, color_top);

	if(face.size() > 1)
	{
		if(north_face_visible)
		{
			_symmetry->link_rectangle(face[0], this->symm(face[0]), this->symm(face[1]), face[1]);
		}
		if(south_face_visible)
		{
			_symmetry->link_rectangle(face[0], face[1], this->symm(face[1]), this->symm(face[0]));
		}
	}
	
	return face;
}

template<pilerRelationType SYMMETRY, unsigned int ARITY>
roofLinkor GlobalPilerConnection<SYMMETRY, ARITY>::form_flat_link(unsigned int piler, float height_base, float height_top, bool north_face_visible, bool south_face_visible)
{
	auto& infos  = _afterbase_infos;
	auto& pilers = infos.pilers_top;

	this->set_piler_hight(pilers[piler], height_base);
	this->flat_link_pilers(pilers[piler]);
	this->link_piler_inside(piler);
	
	roofLinkor ret;
	ret.plateforms.clear();
	ret.north = this->create_piler_vertex_side(pilers[piler], _cathroofcolor, _cathroofcolor);
	ret.south = ret.north;

	this->create_square_front_face(piler, north_face_visible, south_face_visible, _cathcolor, _cathcolor/1.3f);

	ret.north.push_back(_symmetry->symmetrical(ret.north[ret.north.size()-1]));
	ret.south.push_back(_symmetry->symmetrical(ret.south[ret.south.size()-1]));

	return ret;
}

template<pilerRelationType SYMMETRY, unsigned int ARITY>
roofLinkor GlobalPilerConnection<SYMMETRY, ARITY>::form_square_fusion(unsigned int piler1, unsigned int piler2, float height_base, float height_top, bool north_face_visible, bool south_face_visible)
{
	auto& infos  = _afterbase_infos;
	auto& pilers = infos.pilers_top;

	float start_h = std::min(_vertexs[pilers[piler1].south_west].y, _vertexs[pilers[piler2].north_west].y);

	this->set_piler_hight(pilers[piler1], height_base);
	this->set_piler_hight(pilers[piler2], height_base);
	this->link_piler_inside(piler1);
	this->link_piler_inside(piler2);

	this->fusion_square_pilers(pilers[piler1], pilers[piler2]);
		
	roofLinkor ret;

	ret.north = this->create_square_front_face(piler1, false, false, _cathroofcolor, _cathroofcolor);
	ret.south = this->create_square_front_face(piler2, false, false, _cathroofcolor, _cathroofcolor);


	plateformLocation plat;
	plat.south_east = _vertexs[pilers[piler2].south_east];
	plat.south_east.y = start_h + height_base;

	plat.north_west = _symmetry->symmetrical(_vertexs[pilers[piler1].north_east]);
	plat.north_west.y = start_h + height_base;

	ret.plateforms.push_back(plat);

	float darkup_coeff = 0.8;

	std::vector<unsigned int> north_face;
	std::vector<unsigned int> south_face;
	if(north_face_visible)
	{
		north_face = this->create_square_front_face(piler1, true, false, _cathcolor, _cathcolor*darkup_coeff);
	}
	else
	{
		north_face = this->create_square_front_face(piler1, false, false, _cathcolor, _cathcolor*darkup_coeff);
	}
	if(south_face_visible)
	{
		south_face = this->create_square_front_face(piler2, false, true, _cathcolor, _cathcolor*darkup_coeff);
	}
	else
	{
		south_face = this->create_square_front_face(piler2, false, false, _cathcolor, _cathcolor*darkup_coeff);	
	}

	if(north_face.size() < 2 || south_face.size() < 2)
	{
		err("GlobalPilerConnection<SYMMETRY, ARITY>::form_square_fusion(): front face not enough vertexs");
	}

	_symmetry->link_rectangle(south_face[1], south_face[0], north_face[0], north_face[1]);
	_symmetry->link_rectangle(this->symm(south_face[0]),
							  this->symm(north_face[1]),
						      this->symm(north_face[1]),
						      this->symm(south_face[0]));
	
	ret.north.push_back(_symmetry->symmetrical(ret.north[ret.north.size()-1]));
	ret.south.push_back(_symmetry->symmetrical(ret.south[ret.south.size()-1]));

	return ret;
}


template<pilerRelationType SYMMETRY, unsigned int ARITY>
roofLinkor GlobalPilerConnection<SYMMETRY, ARITY>::form_triangular(unsigned int piler, float height_base, float height_top, bool north_face_visible, bool south_face_visible)
{
	auto& infos      = _afterbase_infos;
	auto& pilers     = infos.pilers_top;

	this->check_piler_index(piler);

	roofLinkor ret;
	ret.plateforms.clear();

	this->set_piler_hight(pilers[piler], height_base);
	auto tri_indices = this->create_pilier_triangle(pilers[piler], height_top);
	
	if(north_face_visible)
	{
		this->create_pilier_font_triangle(tri_indices, pilers[piler], false, false);
	}
	if(south_face_visible) 
	{
		this->create_pilier_font_triangle(tri_indices, pilers[piler], false , true);
	}
	if(north_face_visible || south_face_visible)
	{
		this->link_piler_inside(piler);
	}
	
	ret.north = this->create_pilier_font_vertexs(tri_indices, pilers[piler], true);
	ret.south = ret.north;

	return ret;
}

template<pilerRelationType SYMMETRY, unsigned int ARITY>
void GlobalPilerConnection<SYMMETRY, ARITY>::form_piler_end(unsigned int piler)
{
	this->check_piler_index(piler);
	this->end_piler(_afterbase_infos.pilers_top[piler]);
}

template<pilerRelationType SYMMETRY, unsigned int ARITY>
void GlobalPilerConnection<SYMMETRY, ARITY>::form_lateral_fusion(unsigned int piler1, unsigned int piler2)
{
	this->check_piler_index(piler1);
	this->check_piler_index(piler2);
	this->fusion_pilers(_afterbase_infos.pilers_top[piler1], _afterbase_infos.pilers_top[piler2]);
}

