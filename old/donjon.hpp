#ifndef _DONJON_HPP_
#define _DONJON_HPP_

#include "../motor/controlled_drawer.hpp"

#include "castle_structure.hpp"

class Donjon : public VertexMesh
{
public:

	static constexpr float END_ACHIEVEMENT = 0.8f; 

	Donjon(const ShaderGenerator& generator,
		   const BasicGround& ground,
		   Graph<CastleVertex, CastleEdge>&             architecture,
		   const std::vector<std::vector<unsigned int>>& enclosure_indices,
		   const glm::vec3& center,
		   const glm::vec3& color,
		   float height,
		   unsigned int precision,
		   DrawerPackController& drawer_controller);

	void 	show_grid_terminal() const;
	int 	grid_value(const glm::ivec2& coord);

	const std::vector<const BasicBuffer*>& get_buffers()  const;
	const ElementBuffer&                   get_elements() const;

	const glm::vec3& get_building_top()    const;
	const glm::vec3& get_building_bottom() const;

	

public: // virtual methods

	void set_build_achievement(float achievement);

	const glm::vec3& get_center()    const;
	const glm::vec3& get_direction() const;

	virtual float get_start_achievement() const override {return 0.0;}
	virtual float get_end_achievement()   const override {return END_ACHIEVEMENT;}

	virtual Drawer* get_drawer() override;
	virtual bool    multisample_draw() const override;
	virtual bool    bloom_draw() const override;

protected:

	void add_point(const glm::vec3& position, const glm::vec4& color);
	unsigned int vertexs_size() const;
	
	ElementBuffer          _indices; 
	ArrayBuffer<glm::vec3> _vertexs;
	ArrayBuffer<glm::vec4> _colors;

private:

	static unsigned int abs_donjon_cont;

	static constexpr int START_STONE = 4;
	static constexpr int STONE       = START_STONE + 1;

	void compute_grid_size();
	void build_grid();
	glm::uvec2 grid_coord(const CastleVertex& t);
	glm::uvec2 grid_coord(const glm::vec3& point);
	glm::uvec2 grid_coord(const glm::vec2& point);
	glm::vec3  coord_from_grid(const glm::uvec2& grid_case);
	void compute_ray_tracing(const CastleVertex& t1,
						     const CastleVertex& t2);
	unsigned int count_wall_neigbourgs(const glm::ivec2& pos);
	unsigned int count_stares(const glm::ivec2& pos);
	void build_borders();
	void place_base_wall(const glm::ivec2& pos, int stone);
	void set_grid_value(const glm::ivec2& coord, int value);
	void build_donjon_up();

private:
	const BasicGround&                            _ground;
	Graph<CastleVertex, CastleEdge>&             _architecture;
	const std::vector<std::vector<unsigned int>>& _enclosure_indices;

	std::vector<const BasicBuffer*> _buffers;

	std::vector<std::vector<int>> _grid;
	glm::vec2 _grid_case_size;
	glm::vec3 _grid_top;
	glm::vec3 _grid_bottom;
	glm::vec3 _up_top;
	glm::vec3 _up_bottom;

	glm::vec3 _position;
	glm::vec3 _direction;
	glm::vec4 _color;
	float     _height;
	float     _max_base_height;
	float     _down_dark_coeff;
	float     _up_dark_coeff;
	float     _larger_from;
	float     _random_color_coeff;

	int _donjon_id;
	int base_vertex_count;

	Uniform<float>     _ydep;
	Uniform<glm::vec3> _center;
	Uniform<float>     _larger;
	ControlledDrawer _drawer;
};


#endif //_DONJON_HPP_
