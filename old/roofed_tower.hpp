#ifndef _ROOFED_TOWER_HPP_
#define _ROOFED_TOWER_HPP_

#include "tower.hpp"

class RoofedTower : public Tower
{
public:
	RoofedTower(Drawer* drawer,
				const glm::vec3& center,
				const glm::vec3& direction,
				const std::vector<stageRelief>& reliefs,
				const glm::vec4& base_color,
				float hat,
				const glm::vec4& roof_color);

protected:

	virtual void  finalize_roof(const std::vector<int>& top_indices) override;
	virtual float roof_start_time() const override;
	virtual void  set_roof_acheivement(float achievement) override;

private:

	void build_roof_indices();

private:

	std::vector<int> _top_indices;

	int       _roof_spique_index;
	float     _hat;
	glm::vec4 _roof_color;

	float _start_height;
	float _animation_hat;
};

#endif //_ROOFED_TOWER_HPP_
