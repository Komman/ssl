#ifndef _TOWER_INTERFACE_HPP_
#define _TOWER_INTERFACE_HPP_

#include "castle_graph.hpp"

using namespace ssl;

class TowerInterface : public VertexMesh
{
public:
	TowerInterface();
	virtual ~TowerInterface();

	void put_in_massive_draw();
	virtual bool special_drawing()   const override {return !_individual_drawing;}
	virtual buildingType get_building_type() const override {return TOWER;}

public:
	enum buffers_name {VERTEXS, COLORS};

	const std::vector<const BasicBuffer*>& get_buffers()  const override;
	const ElementBuffer&                   get_elements() const override;

protected:

	void add_point(const glm::vec3& position, const glm::vec4& color);
	unsigned int vertexs_size() const;
	
	ElementBuffer         _indices; 
	ArrayBuffer<glm::vec3> _vertexs;
	ArrayBuffer<glm::vec4> _colors;

private:
	std::vector<const BasicBuffer*> _buffers;
	bool _individual_drawing;

};

#endif //_TOWER_INTERFACE_HPP_
