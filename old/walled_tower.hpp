#ifndef _WALLED_TOWER_HPP_
#define _WALLED_TOWER_HPP_

#include "roofed_tower.hpp"

class WalledTower : public RoofedTower
{
public:
	WalledTower(Drawer* drawer,
				const glm::vec3& center,
			    const glm::vec3& direction,
			    const std::vector<stageRelief>& reliefs,
			    const glm::vec4& base_color,
				float wall_height,
				float wall_size,
				float hat,
				const glm::vec4& roof_color);

	float get_start_achievement() const override;
	float get_end_achievement()   const override;

	float wall_size() const;
	float wall_height() const;

private:

	float _wall_prehead;
	float _wall_height;
	float _wall_size;

	float _end_achievement;
};

#endif //_WALLED_TOWER_HPP_
