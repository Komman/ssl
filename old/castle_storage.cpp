#include "castle_storage.hpp"


/*
	TOWER STORAGE
*/

CastleVertex::CastleVertex(int enclosure_index,
						   glm::vec3 center,
						   float wall_height,
						   float wall_size)
	: BasicCastleVertex(),
	  _center(center),
	  _wall_height(wall_height),
	  _wall_size(wall_size),
	  _enclosure(enclosure_index)
{

}

glm::vec3 CastleVertex::get_center() const
{
	return _center;
}

float CastleVertex::get_wall_size() const
{
	return _wall_size;	
}

float CastleVertex::get_wall_height() const
{
	return _wall_height;	
}

int CastleVertex::get_enclosure() const
{
	return _enclosure;
}


/*
	WALL STORAGE
*/

CastleEdge::CastleEdge() : BasicCastleVertex()
{

}
