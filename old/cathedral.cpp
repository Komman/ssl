#include "cathedral.hpp"

#include <glm/gtx/string_cast.hpp>

#include "../motor/gutils.hpp"

using namespace std;
using namespace glm;
using namespace utils;

static glm::vec3 _default_cathedral_direction = vec3(1.0, 0.0, 0.0);

Cathedral::Cathedral(const ShaderGenerator& generator, 
				 	 const glm::vec3& plateform_top,
				 	 const glm::vec3& plateform_bottom,
				 	 const cathedralParams& geometric_params,
				 	 const glm::vec3& plateform_color,
				 	 const timeAndLast& time_and_last,
				 	 DrawerPackController& drawer_controller)
	: _achievement(geometric_params.uniform_achievement_name, 0.0f),
	  _unif_center(geometric_params.uniform_center_name, (plateform_bottom+plateform_top)/2.0f),
	  _indices(DYNAMIC_DRAW),
	  _vertexs(DYNAMIC_DRAW),
	  _colors(DYNAMIC_DRAW),
	  _buffers({&_vertexs, &_colors}),
	  _drawer(generator, "cathedral_vertex", "cathedral_fragment", {"all_lights_mesh_normal"}, drawer_controller),
	  _to_draw(true),
	  _geometry(geometric_params),
	  _start_time(time_and_last.first),
	  _end_time(time_and_last.first + time_and_last.second),
	  _overbound(glm::length(plateform_top-plateform_bottom)*geometric_params.relative_ext),
	  _top(plateform_top),
	  _bottom(plateform_bottom),
	  _center((_top + _bottom)/2.0f),
	  _floorcolor(plateform_color),
	  _cathcolor(0.6,0.5,0.3, 1.0),
	  _cathroofcolor(0.3,0.6,0.5, 1.0)
{
	_drawer.depth(true);

	this->bluild_base();	
	this->build_floor();
	this->build_upbase();
}


unsigned int Cathedral::symm(unsigned int index)
{
	return _symmetry->symmetrical(index);
}

plateformIndices Cathedral::create_interplateform(const glm::vec3& lowX, const glm::vec3& greatX, float length, const glm::vec4& color)
{
	plateformIndices ret;

	float bot_shadow_coeff = 0.7;

	ret.low.x = _symmetry->place_symmetrical(lowX   + (-vvv::x() - vvv::z() - vvv::y()/2.0f)*length, color*bot_shadow_coeff);
	ret.low.y = _symmetry->place_symmetrical(greatX + ( vvv::x() - vvv::z() - vvv::y()/2.0f)*length, color*bot_shadow_coeff);
	ret.top.x = _symmetry->place_symmetrical(lowX   + (-vvv::x() - vvv::z() + vvv::y()/2.0f)*length, color);
	ret.top.y = _symmetry->place_symmetrical(greatX + ( vvv::x() - vvv::z() + vvv::y()/2.0f)*length, color);

	_symmetry->link_rectangle(ret.low.x, ret.top.x, ret.top.y, ret.low.y);
	_symmetry->link_rectangle(ret.top.x, ret.low.x, symm(ret.low.x),  symm(ret.top.x));
	_symmetry->link_rectangle(ret.top.y,  symm(ret.top.y), symm(ret.low.y), ret.low.y);
	_symmetry->link_rectangle(ret.top.x,  symm(ret.top.x), symm(ret.top.y), ret.top.y);
	_symmetry->link_rectangle(ret.low.x, ret.low.y, symm(ret.low.y),  symm(ret.low.x));

	return ret;
}

void Cathedral::grow_piler(const pilerTopIndexor& piler_top, float height)
{
	_vertexs.change_subvalue(piler_top.north_east, _vertexs[piler_top.north_east] + vvv::y()*height);
	_vertexs.change_subvalue(piler_top.north_west, _vertexs[piler_top.north_west] + vvv::y()*height);
	_vertexs.change_subvalue(piler_top.south_west, _vertexs[piler_top.south_west] + vvv::y()*height);
	_vertexs.change_subvalue(piler_top.south_east, _vertexs[piler_top.south_east] + vvv::y()*height);
	
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.north_east), _vertexs[_symmetry->symmetrical(piler_top.north_east)] + vvv::y()*height);
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.north_west), _vertexs[_symmetry->symmetrical(piler_top.north_west)] + vvv::y()*height);
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.south_west), _vertexs[_symmetry->symmetrical(piler_top.south_west)] + vvv::y()*height);
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.south_east), _vertexs[_symmetry->symmetrical(piler_top.south_east)] + vvv::y()*height);
}

static inline glm::vec3 ytozero(const glm::vec3& v)
{
	vec3 ret = v;
	ret.y=0;
	return ret;
}

void Cathedral::set_piler_hight(const pilerTopIndexor& piler_top, float height)
{
	float diffs[4]={
		_vertexs[piler_top.north_east].y - _vertexs[piler_top.north_east].y,
		_vertexs[piler_top.north_west].y - _vertexs[piler_top.north_east].y,
		_vertexs[piler_top.south_west].y - _vertexs[piler_top.north_east].y,
		_vertexs[piler_top.south_east].y - _vertexs[piler_top.north_east].y
	};

	_vertexs.change_subvalue(piler_top.north_east, ytozero(_vertexs[piler_top.north_east]) + vvv::y()*(height+diffs[0]));
	_vertexs.change_subvalue(piler_top.north_west, ytozero(_vertexs[piler_top.north_west]) + vvv::y()*(height+diffs[1]));
	_vertexs.change_subvalue(piler_top.south_west, ytozero(_vertexs[piler_top.south_west]) + vvv::y()*(height+diffs[2]));
	_vertexs.change_subvalue(piler_top.south_east, ytozero(_vertexs[piler_top.south_east]) + vvv::y()*(height+diffs[3]));
	
	float diffsym[4]={
		_vertexs[_symmetry->symmetrical(piler_top.north_east)].y - _vertexs[_symmetry->symmetrical(piler_top.north_east)].y,
		_vertexs[_symmetry->symmetrical(piler_top.north_west)].y - _vertexs[_symmetry->symmetrical(piler_top.north_east)].y,
		_vertexs[_symmetry->symmetrical(piler_top.south_west)].y - _vertexs[_symmetry->symmetrical(piler_top.north_east)].y,
		_vertexs[_symmetry->symmetrical(piler_top.south_east)].y - _vertexs[_symmetry->symmetrical(piler_top.north_east)].y
	};

	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.north_east), ytozero(_vertexs[_symmetry->symmetrical(piler_top.north_east)]) + vvv::y()*(height+diffsym[0]));
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.north_west), ytozero(_vertexs[_symmetry->symmetrical(piler_top.north_west)]) + vvv::y()*(height+diffsym[1]));
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.south_west), ytozero(_vertexs[_symmetry->symmetrical(piler_top.south_west)]) + vvv::y()*(height+diffsym[2]));
	_vertexs.change_subvalue(_symmetry->symmetrical(piler_top.south_east), ytozero(_vertexs[_symmetry->symmetrical(piler_top.south_east)]) + vvv::y()*(height+diffsym[3]));
}



void Cathedral::add_point(const glm::vec3& coord, const glm::vec3& color)
{
	_vertexs.add_value(coord);
	_colors.add_value(vec4(color, 1.0f));
}

void Cathedral::build_floor()
{
	unsigned int start_id = _vertexs.size();
	vec3 coords[4]={
		vec3(_top.x   , _bottom.y, _top.z   ) - vec3( 1.0, 0.0, 1.0)*_overbound,
		vec3(_top.x   , _bottom.y, _bottom.z) - vec3( 1.0, 0.0,-1.0)*_overbound,
		vec3(_bottom.x, _bottom.y, _bottom.z) - vec3(-1.0, 0.0,-1.0)*_overbound,
		vec3(_bottom.x, _bottom.y, _top.z   ) - vec3(-1.0, 0.0, 1.0)*_overbound
	};

	for(int i=0; i<4; i++)
	{
		this->add_point(coords[i], _floorcolor/1.6f);
	}
	for(int i=0; i<4; i++)
	{
		this->add_point(coords[i] + vvv::y()*_geometry.floor_height, _floorcolor);
	}

	_indices.add_rectangle(start_id+3,
					       start_id+2,
	    			       start_id+1,
	    			       start_id+0);
	_indices.add_rectangle(start_id+0+4,
						   start_id+1+4,
						   start_id+2+4,
						   start_id+3+4);

	for(int i=0; i<4; i++)
	{
		int ipp = (i+1)%4;
		_indices.add_rectangle(start_id + ipp,
							   start_id + ipp+4,
							   start_id + i+4,
							   start_id + i);
	}
}

void Cathedral::bluild_base()
{
	// CONSTANTS

	float rel_d = abs(_bottom.x - _top.x);
	float base_height = utils::random_float(rel_d/4.0f, rel_d/2.0f);
	float space_used_coeff = 0.7f;

	//----------


	float start_h = _bottom.y + _geometry.floor_height;

	vec3 bot_corner  = vec3(_bottom.x + _overbound*0.0, start_h, _bottom.z + _overbound*0.0);
	vec3 top_corner  = vec3(_top.x    - _overbound*0.0, start_h, _top.z    - _overbound*0.0);
	vec3 base_center = (bot_corner + top_corner)/2.0f;

	_symmetry = std::make_unique<PlanarSymmetry<vec4>>(infinitePlan({base_center, vec3(0.0,0.0,1.0)}), &_vertexs, &_indices, &_colors);

	/*  
	   corners[i]

              |
		 1 -- 2 
		 |    | 
		 |    |    
		 |    |  SYMMETRICAL
		 |    |    
		 0 -- 3
		      |
	*/

	vec3 basesize = (bot_corner - top_corner)*space_used_coeff;
	basesize.z /= 2.0f;
	basesize.y = base_height;

	_afterbase_infos.height = base_height;
	_afterbase_infos.corners[0] = base_center + vec3(-basesize.x/2.0f, 0.0f, -basesize.z);
	_afterbase_infos.corners[1] = base_center + vec3(+basesize.x/2.0f, 0.0f, -basesize.z);
	_afterbase_infos.corners[2] = base_center + vec3(+basesize.x/2.0f, 0.0f, 0.0f);
	_afterbase_infos.corners[3] = base_center + vec3(-basesize.x/2.0f, 0.0f, 0.0f);

	float shadowcoeff = 0.5f;

	unsigned int p1 = _symmetry->place_symmetrical(_afterbase_infos.corners[0], _cathcolor*shadowcoeff);
	unsigned int p2 = _symmetry->place_symmetrical(_afterbase_infos.corners[1], _cathcolor*shadowcoeff);
	unsigned int p3 = _symmetry->place_symmetrical(_afterbase_infos.corners[0] + vvv::y()*basesize.y, _cathcolor);
	unsigned int p4 = _symmetry->place_symmetrical(_afterbase_infos.corners[1] + vvv::y()*basesize.y, _cathcolor);

	_afterbase_infos.low_corners_indexs[0] = p1;
	_afterbase_infos.low_corners_indexs[1] = p2;
	_afterbase_infos.top_corners_indexs[0] = p3;
	_afterbase_infos.top_corners_indexs[1] = p4;

	_symmetry->link_rectangle(p1, p3, p4, p2);
	_symmetry->link_rectangle(p1, _symmetry->symmetrical(p1), _symmetry->symmetrical(p3), p3);
	_symmetry->link_rectangle(p4, _symmetry->symmetrical(p4), _symmetry->symmetrical(p2), p2);
	_symmetry->link_rectangle(p3, _symmetry->symmetrical(p3), _symmetry->symmetrical(p4), p4);
	_symmetry->link_rectangle(p2, _symmetry->symmetrical(p2), _symmetry->symmetrical(p1), p1);


	vec3 Lshape[3] = {_afterbase_infos.corners[1], _afterbase_infos.corners[0], _afterbase_infos.corners[3]};

	LBaseBuildor basebuildor(Lshape,
							 *_symmetry,
							 {base_height, _cathcolor});

	basebuildor.generate_pilers({vec2(0.02f,0.10f*4.0f)*rel_d, base_height, rel_d/70.0f, _cathcolor*1.5f});
	basebuildor.generate_pilers({vec2(0.01f,0.03f*4.0f)*rel_d, base_height, rel_d/100.0f, _cathcolor/2.0f});
	
	auto finished_indexed_base = basebuildor.finish_base();

	_afterbase_infos.pilers_top = finished_indexed_base.top_pilers;
	_afterbase_infos.pilers_bot = finished_indexed_base.bot_pilers;

	_afterbase_infos.corners[0].y += base_height;
	_afterbase_infos.corners[1].y += base_height;
	_afterbase_infos.corners[2].y += base_height;
	_afterbase_infos.corners[3].y += base_height;
}

float Cathedral::get_piler_x_center(const pilerTopIndexor& piler) const
{
	return ((_vertexs[piler.north_east] + _vertexs[piler.south_east])/2.0f).x;
}

void Cathedral::set_roof_color(const std::vector<unsigned int>& bipartend, const std::vector<glm::vec4>& colors)
{
	if(colors.size() == 0)
	{
		err("Cathedral::set_roof_color(): no colors passed");
	}

	glm::vec4 curcolor;
	for(unsigned int i=0; i<bipartend.size(); i++)
	{
		if(i < colors.size())
		{
			curcolor = colors[i];
		}
		_colors.change_subvalue(bipartend[i], curcolor);
	}
}


const glm::vec3& Cathedral::get_center()    const
{
	return _center;
}

const glm::vec3& Cathedral::get_direction() const
{
	return _default_cathedral_direction;
}

Drawer* Cathedral::get_drawer()
{
	if(_to_draw)
	{
		return &_drawer;
	}
	return NULL;
}

const std::vector<const BasicBuffer*>& Cathedral::get_buffers()  const
{
	return _buffers;
}

const ElementBuffer&                   Cathedral::get_elements() const
{
	return _indices;
}

void  Cathedral::set_build_achievement(float achievement)
{
	if(achievement < 0.0f)
	{
		_to_draw = false;
	}
	else 
	{
		_to_draw = true;
	}

	_achievement = glm::clamp(achievement, 0.0f, 1.0f);
}

float Cathedral::get_start_achievement() const 
{
	return _start_time;
}

float Cathedral::get_end_achievement()   const 
{
	return _end_time;
}

bool Cathedral::special_drawing() const
{
	return !_to_draw;
}

bool Cathedral::multisample_draw() const
{
	return true;
}

bool Cathedral::bloom_draw() const
{
	return true;
}