template<typename T>
void unordered_vector_pop(std::vector<T>& v, unsigned int index)
{
	#ifdef GRAPH_DEBUG
	if(v.size() == 0)
	{
		err("utils::unordered_vector_pop(): try to pop an empty vector");
	}
	if(index >= v.size())
	{
		err("utils::unordered_vector_pop(): index out of bound: " + std::to_string(index)
			+ "/" + std::to_string(v.size()));
	}
	#endif

	if(index != v.size())
	{
		T last = v[v.size() - 1];
		v[index] = last;
	}
	v.pop_back();
}

template<typename NodeType, typename EdgeContent>
Graph<NodeType, EdgeContent>::Graph()
{

}

template<typename NodeType, typename EdgeContent>
unsigned int Graph<NodeType, EdgeContent>::size()
{
	return _vertexs.size();
}

template<typename NodeType, typename EdgeContent>
typename Graph<NodeType, EdgeContent>::Index Graph<NodeType, EdgeContent>::add_vertex(const NodeType& content)
{
	unsigned int old_size = this->size();

	_vertexs.push_back(content);
	_adjacency_list.push_back({});
	_adjacency_edges.push_back({});

	return old_size;
}
template<typename NodeType, typename EdgeContent>
typename Graph<NodeType, EdgeContent>::Index Graph<NodeType, EdgeContent>::add_vertex(NodeType&& content)
{
	unsigned int old_size = this->size();

	_vertexs.push_back(std::move(content));
	_adjacency_list.push_back({});
	_adjacency_edges.push_back({});

	return old_size;
}


template<typename NodeType, typename EdgeContent>
void Graph<NodeType, EdgeContent>::check_edge_bound(const Edge& edge)
{
	#ifdef GRAPH_DEBUG
	if(edge.first >=_vertexs.size() || edge.second>=_vertexs.size())
	{
		this->err("Graph: add_oriented_edge() with edge out of bound: requested {" + std::to_string(edge.first) + ", " + std::to_string(edge.second)
			+ "} while size is " + std::to_string(_vertexs.size()));
	}
	#endif
}
template<typename NodeType, typename EdgeContent>
void Graph<NodeType, EdgeContent>::check_vertex_bound(Index i)
{
	#ifdef GRAPH_DEBUG
	if(i>=_vertexs.size())
	{
		this->err("Graph: operator[] with i out of bound: requested " + std::to_string(i)
			+ " while size is " + std::to_string(_vertexs.size()));
	}
	#endif
}
template<typename NodeType, typename EdgeContent>
void Graph<NodeType, EdgeContent>::check_edge_index_bound(EdgeIndex i)
{
	#ifdef GRAPH_DEBUG
	if(i>=_edges.size())
	{
		this->err("Graph: check_edge_index_bound() with i out of bound: requested " + std::to_string(i)
			+ " while size is " + std::to_string(_edges.size()));
	}
	#endif
}

template<typename NodeType, typename EdgeContent>
void Graph<NodeType, EdgeContent>::add_oriented_edge(const Edge& edge, const EdgeContent& content)
{
	this->check_edge_bound(edge);

	EdgeIndex i = _edges.size();
	_edges.push_back({edge, content});
	_adjacency_list[edge.first].push_back(edge.second);
	_adjacency_edges[edge.first].push_back(i);
	
}
template<typename NodeType, typename EdgeContent>
void Graph<NodeType, EdgeContent>::add_oriented_edge(const Edge& edge, EdgeContent&& content)
{
	this->check_edge_bound(edge);

	EdgeIndex i = _edges.size();
	_edges.push_back({edge, std::move(content)});
	_adjacency_list[edge.first].push_back(edge.second);
	_adjacency_edges[edge.first].push_back(i);
	
}

template<typename NodeType, typename EdgeContent>
void Graph<NodeType, EdgeContent>::add_edge(const Edge& edge, const EdgeContent& content)
{
	this->add_oriented_edge(edge, content);
	this->add_oriented_edge({edge.second, edge.first}, content);
}
template<typename NodeType, typename EdgeContent>
void Graph<NodeType, EdgeContent>::add_edge(const Edge& edge, EdgeContent&& content)
{
	this->add_oriented_edge(edge, std::move(content));
	this->add_oriented_edge({edge.second, edge.first}, std::move(content));
}

template<typename NodeType, typename EdgeContent>
NodeType& Graph<NodeType, EdgeContent>::operator[](Index i)
{
	this->check_vertex_bound(i);	
	return _vertexs[i];
}

template<typename NodeType, typename EdgeContent>
const std::vector<NodeType>& Graph<NodeType, EdgeContent>::vertexs()
{
	return _vertexs;
}

template<typename NodeType, typename EdgeContent>
const std::vector<typename Graph<NodeType, EdgeContent>::Index>& Graph<NodeType, EdgeContent>::neighbours(Index i)
{
	return _adjacency_list[i];
}


template<typename NodeType, typename EdgeContent>
void Graph<NodeType, EdgeContent>::err(const std::string& msg)
{
	std::cout<<"Graph error: "<<msg<<std::endl<<std::endl;
	throw std::runtime_error("graph error");
}

template<typename NodeType, typename EdgeContent>
typename Graph<NodeType, EdgeContent>::EdgeType& Graph<NodeType, EdgeContent>::operator()(EdgeIndex i)
{
	return this->edge(i);
}

template<typename NodeType, typename EdgeContent>
const std::vector<typename Graph<NodeType, EdgeContent>::EdgeType>& Graph<NodeType, EdgeContent>::edges()
{
	return _edges;
}

template<typename NodeType, typename EdgeContent>
const typename Graph<NodeType, EdgeContent>::EdgeType& Graph<NodeType, EdgeContent>::edge(EdgeIndex i)
{
	this->check_edge_index_bound(i);
	return _edges[i];
}

template<typename NodeType, typename EdgeContent>
const std::vector<typename Graph<NodeType, EdgeContent>::EdgeIndex>&   Graph<NodeType, EdgeContent>::connected_edges(Index i)
{
	this->check_vertex_bound(i);
	return _adjacency_edges[i];
}
