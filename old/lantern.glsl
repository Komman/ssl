#include "shaders.glsl"
#include "camera_light.glsl"

void lantern_vertex(in vec3 in_pos,
					in vec3 in_color,
					in::instance vec3  dep,
					in::instance float angle,
					out vec3 out_position,
					out vec4 out_color)
{
	float cosangle = cos(angle);
	float sinangle = sin(angle);
	vec3 rotated_coord =  vec3(in_pos.x*cosangle - in_pos.z*sinangle, in_pos.y, in_pos.z*cosangle + in_pos.x*sinangle);  
	local::simple_vertex(dep + rotated_coord, vec4(in_color, 1.0), out_position, out_color);
}

void lantern_fragment(in vec3  frag_pos,
					  in vec4  frag_color,
					  out vec4 final_color,
					  out vec4 bloom_color)
{
	final_color = frag_color;
	if(length(frag_color.xyzr) > 1.0)
	{
		bloom_color = frag_color*clamp(local::camera_distance(frag_pos)/2.0f, 8.0f, 80.0f)*uniform::lightor_activation;
		// bloom_color = frag_color*sqrt(max(local::camera_distance(frag_pos), 4.0f))*1.5f;
	}
}