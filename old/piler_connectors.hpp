#ifndef _PILER_CONNECTORS_HPP_
#define _PILER_CONNECTORS_HPP_

#include "piler_connector_interface.hpp"

template<pilerRelationType SYMMETRY, unsigned int ARITY>
class GlobalPilerConnection : public PilerConnectorArity<SYMMETRY, ARITY>
{
public:
	GlobalPilerConnection(const pilerBuildingInfos& infos) : PilerConnectorArity<SYMMETRY, ARITY>(infos) {}

protected:

	unsigned int symm(unsigned int index);
	std::vector<unsigned int> create_pilier_triangle(const pilerTopIndexor& piler, float triangle_h);
	std::vector<unsigned int> create_pilier_font_vertexs(const std::vector<unsigned int>& tripiler_indices, const pilerTopIndexor& piler, bool roof);
	void link_piler_inside(unsigned int piler);
	void link_piler_inside_indexor(const pilerTopIndexor& piler_top, const pilerTopIndexor& piler_bot);
	std::vector<unsigned int> create_pilier_font_triangle(const std::vector<unsigned int>& tripiler_indices, const pilerTopIndexor& piler, bool roof, bool ext_face_inv);
	void grow_piler(const pilerTopIndexor& piler_top, float height);
	void set_piler_hight(const pilerTopIndexor& piler_top, float height);
	void flat_link_pilers(const pilerTopIndexor& piler_top);
	void fusion_pilers(const pilerTopIndexor& piler1, const pilerTopIndexor& piler2);
	glm::uvec2 fusion_square_pilers(const pilerTopIndexor& piler1, const pilerTopIndexor& piler2);
	void end_piler(const pilerTopIndexor& piler_top);
	void check_piler_index(unsigned int piler_index);
	std::vector<unsigned int> create_piler_vertex_side(const pilerTopIndexor& piler, const glm::vec4& color_bot, const glm::vec4& color_top);
	std::vector<unsigned int> create_square_front_face(unsigned int piler, bool north_face_visible, bool south_face_visible, const glm::vec4& color_bot, const glm::vec4& color_top);
	roofLinkor form_flat_link(unsigned int piler, float height_base, float height_top, bool north_face_visible, bool south_face_visible);
	roofLinkor form_square_fusion(unsigned int piler1, unsigned int piler2, float height_base, float height_top, bool north_face_visible, bool south_face_visible);
	roofLinkor form_triangular(unsigned int piler, float height_base, float height_top, bool north_face_visible, bool south_face_visible);
	void form_piler_end(unsigned int piler);
	void form_lateral_fusion(unsigned int piler1, unsigned int piler2);	

	static inline glm::vec3 ytozero(const glm::vec3& v)
	{
		glm::vec3 ret = v;
		ret.y=0;
		return ret;
	}


	using PilerConnectorArity<SYMMETRY, ARITY>::_indices;
	using PilerConnectorArity<SYMMETRY, ARITY>::_vertexs;
	using PilerConnectorArity<SYMMETRY, ARITY>::_colors;
	using PilerConnectorArity<SYMMETRY, ARITY>::_top;
	using PilerConnectorArity<SYMMETRY, ARITY>::_bottom;
	using PilerConnectorArity<SYMMETRY, ARITY>::_center;
	using PilerConnectorArity<SYMMETRY, ARITY>::_floorcolor;
	using PilerConnectorArity<SYMMETRY, ARITY>::_cathcolor;
	using PilerConnectorArity<SYMMETRY, ARITY>::_cathroofcolor;
	using PilerConnectorArity<SYMMETRY, ARITY>::_afterbase_infos;
	using PilerConnectorArity<SYMMETRY, ARITY>::_symmetry;
};


template<pilerConnection::Relation RELATION, pilerRelationType SYMMETRY, unsigned int ARITY>
class TypedGlobalPilerConnection : public GlobalPilerConnection<SYMMETRY, ARITY>
{
public:
	TypedGlobalPilerConnection(const pilerBuildingInfos& infos) : GlobalPilerConnection<SYMMETRY, ARITY>(infos) {}

	pilerConnection::Relation relation() const override {return RELATION;} 

};

#include "piler_connectors_impl.hpp"


class TriangularPilerLink : public TypedGlobalPilerConnection<pilerConnection::TRIANGULAR, SYMMPILER, 1>
{
public: 
	TriangularPilerLink(const pilerBuildingInfos& infos) : TypedGlobalPilerConnection<pilerConnection::TRIANGULAR, SYMMPILER, 1>(infos) {}
protected:
	roofLinkor connect_goodsize(const std::vector<unsigned int>& pilers, float height_base, float height_top, bool north_face_visible, bool south_face_visible) override
	{
		return this->form_triangular(pilers[0], height_base, height_top, north_face_visible, south_face_visible);
	}
};
class FlatPilerLink : public TypedGlobalPilerConnection<pilerConnection::FLAT, SYMMPILER, 1>
{
public: 
	FlatPilerLink(const pilerBuildingInfos& infos) : TypedGlobalPilerConnection<pilerConnection::FLAT, SYMMPILER, 1>(infos) {}
protected:
	roofLinkor connect_goodsize(const std::vector<unsigned int>& pilers, float height_base, float height_top, bool north_face_visible, bool south_face_visible) override
	{
		return this->form_flat_link(pilers[0], height_base, height_top, north_face_visible, south_face_visible);
	}
};
class SquareFusionPilerLink : public TypedGlobalPilerConnection<pilerConnection::SQUARE_FUSION, SYMMPILER, 2>
{
public: 
	SquareFusionPilerLink(const pilerBuildingInfos& infos) : TypedGlobalPilerConnection<pilerConnection::SQUARE_FUSION, SYMMPILER, 2>(infos) {}
protected:
	roofLinkor connect_goodsize(const std::vector<unsigned int>& pilers, float height_base, float height_top, bool north_face_visible, bool south_face_visible) override
	{
		return this->form_square_fusion(pilers[0], pilers[1], height_base, height_top, north_face_visible, south_face_visible);
	}
};

class EndPilerLink : public TypedGlobalPilerConnection<pilerConnection::END, ASYMPILER, 1>
{
public: 
	EndPilerLink(const pilerBuildingInfos& infos) : TypedGlobalPilerConnection<pilerConnection::END, ASYMPILER, 1>(infos) {}
protected:
	void connect_goodsize(const std::vector<unsigned int>& pilers) override
	{
		this->form_piler_end(pilers[0]);
	}
};
class LateralFusionPilerLink : public TypedGlobalPilerConnection<pilerConnection::LATERAL_FUSION, ASYMPILER, 2>
{
public: 
	LateralFusionPilerLink(const pilerBuildingInfos& infos) : TypedGlobalPilerConnection<pilerConnection::LATERAL_FUSION, ASYMPILER, 2>(infos) {}
protected:
	void connect_goodsize(const std::vector<unsigned int>& pilers) override
	{
		this->form_lateral_fusion(pilers[0], pilers[1]);
	}
};

#endif //_PILER_CONNECTORS_HPP_
