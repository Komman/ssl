#include "player.hpp"


using namespace ssl;
using namespace std;
using namespace glm;

static constexpr float GRAVITY = 50.0;

Player::Player(const vec3&   start,
			   float speed,
			   float jump_speed)
	: UniformCamera(start,
					NEAR,
					FAR,
					speed,
					MVP_unif,
					position_unif,
					direction_unif),
	  _ground(NULL),
	  _position(this->get_position()),
	  _vecspeed(vec3(0)),
	  _selftarget(&_position, &_vecspeed),
	  _speed(speed),
	  _jump_speed(jump_speed),
	  _head_heigth(2.0),
	  _walk_slope(3.0),
	  _on_ground(false),
	  _space_point(&(this->get_position()), &(this->get_speed_vector()))
{

}

const BasicTarget& Player::get_target()
{
	return _selftarget;
}


void Player::change_ground(BasicGround const* ground)
{
	_ground = ground;
}

const BasicDirectedPoint& Player::get_space_point() const
{
	return _space_point;
}



// void Player::keys_reaction(const std::vector<unsigned int>& pressed_keys_indices)
// {
// 	glm::vec3 direction(0.0, 0.0, 0.0);

// 	//TOOPT (with function pointer)
// 	for(unsigned int k : pressed_keys_indices)
// 	{
// 		if(k == Camera::CAMERA_CONTROL_RIGHT)
// 		{
// 			direction+= this->get_right_vector();
// 		}
// 		if(k == Camera::CAMERA_CONTROL_LEFT)
// 		{
// 			direction-= this->get_right_vector();
// 		}
// 		if(k == Camera::CAMERA_CONTROL_UP && _on_ground)
// 		{
// 			_speed_vector+= vvv::y()*_jump_speed;
// 		}
// 		if(k == Camera::CAMERA_CONTROL_DOWN && _on_ground)
// 		{
// 			_speed_vector-= vvv::y()*_jump_speed;
// 		}
// 		if(k == Camera::CAMERA_CONTROL_FRONT)
// 		{
// 			direction+= glm::cross(this->get_up_head(), this->get_right_vector());
// 		}
// 		if(k == Camera::CAMERA_CONTROL_BACK)
// 		{
// 			direction-= glm::cross(this->get_up_head(), this->get_right_vector());
// 		}
// 	}


// 	if(glm::length(direction) > ssl::EPSILON)
// 	{
// 		direction = glm::normalize(direction);
// 	}
	
// 	vec3 total_dep = (direction*this->_speed + _speed_vector)*ssl::window::delta_time();
// 	vec3 new_position = this->get_position() + total_dep;

// 	float ground_y = _ground.get_y(new_position.x, new_position.z);

// 	if(  (ground_y + _head_heigth               > new_position.y) 
// 	  || (ground_y + _walk_slope + _head_heigth > new_position.y && glm::length(_speed_vector)<ssl::EPSILON))
// 	{
// 		new_position.y = ground_y + _head_heigth;
// 		_speed_vector = vec3(0.0);
// 		_on_ground = true;
// 	}
// 	else
// 	{
// 		_speed_vector -= vvv::y()*GRAVITY*window::delta_time();
// 		_on_ground = false;
// 	}

// 	this->tp(new_position);
// }

void Player::check_ground() const
{
	if(_ground == NULL)
	{
		err("Try to use ground while no set");
	}
}


void Player::frame_update_more_actions()
{
	this->check_ground();

	float h = 0.5f;

	if(this->get_position().y - h < _ground->get_y(this->get_position()))
	{
		this->tp(_ground->get_xyz(this->get_position()) + vvv::y()*h);
	}

	_position = this->get_position();
}
