#ifndef _CASTLE_STRUCTURE_HPP_
#define _CASTLE_STRUCTURE_HPP_

#include <memory>
#include "../../ssl/ssl.hpp"

#include "../utils/graph.hpp"

#include "../motor/post_processor.hpp"
#include "../motor/controlled_drawer.hpp"
#include "../motor/shared_ressources.hpp"
#include "../motor/vertical_shadower.hpp"
#include "../motor/static_lightor.hpp"

#include "basic_ground.hpp"
#include "shooting_tower.hpp"
#include "wall.hpp"
#include "castle_storage.hpp"
#include "lantern.hpp"

using namespace ssl;


struct castleStructureParameters
{
	glm::vec3 center;
	float radius;
	glm::vec2 last_enclosure_amplifior;
	int ext_tower_count;
	int enclosure_count;
};


class CastleStructure
{
public:
	CastleStructure(SharedRessources& shared_ressources,
					const BasicGround& ground,
					StaticLigthor& slightor,
		   			const castleStructureParameters& parameters,
		   			DrawerPackController& drawer_controller);

	void dynamic_build(float dt);
	void set_time_build(float achievement);
	void set_dynamic_build(float achievement);
	void start_dynamic_build();

	void draw();
	void draw_tower_shadows(VerticalShadower& shadower);
	void draw_wall_shadows(VerticalShadower& shadower);

	static castleStructureParameters default_parameters(const BasicGround& ground);

	bool ready_for_tower_shadow() const;
	bool ready_for_wall_shadow() const;

	void set_target(const BasicDirectedPoint* target);

public:
	using enclosureRange = std::pair<float, float>;

private:

	struct towerBuildingParameters	
	{
		int   tower_space;
		float size;
		float min_base_heigth;
		int   stage;
		glm::vec3  roof_color;
		float under_torch;
	};
	using towerBuilderFunction = std::unique_ptr<ShootingTower>  (CastleStructure::*)(const glm::vec3& center, const glm::vec3& direction, const towerBuildingParameters& params);


	void build(); 
	std::unique_ptr<Wall> build_wall(unsigned int tower_i, unsigned int tower_j);
	void prepare_primary_walls();

	void build_enclosure(int enclosure,
						 const towerBuildingParameters& tower_args,
						 towerBuilderFunction tower_builder);
	std::unique_ptr<ShootingTower> simple_tower_builder(const glm::vec3& center,
									        		  const glm::vec3& direction,
									        		  const towerBuildingParameters& params);
	void special_structure_drawing();
	void draw_castle_storage(BuildingMesh& obj);
	void draw_merged_objects();
	void add_lanterns(int enclosure, const std::vector<lanternDisposition>& lantern_disposition);
	void set_dynamic_lights();
	void refine_minmax_tower(const glm::vec3& tower_middle);

protected:

	enclosureRange get_enclosure_range(int enclosure);
	float          get_enclosure_random_radius(int enclosure);
	const std::vector<std::vector<unsigned int>>&  get_enclosure_indices();

	float get_achievement(float ach_begin, float ach_end);

	const castleStructureParameters& structure_parameters() const;
	const ShaderGenerator&  shader_generator()     const;	

protected:

	Graph<CastleVertex, CastleEdge>   _architecture;
	const float  _inground;
	
	const BasicGround&   _ground;
	StaticLigthor& _slightor;

private:
	SharedRessources& _shared_ressources;

	castleStructureParameters _parameters;
	Uniform<glm::vec3> _center;
	Uniform<glm::vec3> _ambiant_color;
	Uniform<float>     _light_radius;
	Uniform<float>     _ligths_activation;
	Uniform<glm::vec2> _minminax_tower_height;
  
	ShaderGenerator  _shader_generator;
	ControlledDrawer _tower_drawer;
	ControlledDrawer _wall_drawer;
	ControlledDrawer _lantern_drawer;

	glm::vec4 _tower_color;

	float _time;
	float _total_achievement;

	glm::vec2 _start_stop_lanters;

	ElementObjectMerger<glm::vec3, glm::vec4> _tower_merger;
	ElementObjectMerger<glm::vec3, glm::vec4> _wall_merger;
	bool _structure_merged;
	int  _merge_call_count;

	std::vector<glm::vec3> _wall_color;

	std::vector<std::vector<unsigned int>> _enclosure_indices;
};

#endif //_CASTLE_STRUCTURE_HPP_
