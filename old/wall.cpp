#include "wall.hpp"

#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/string_cast.hpp>

using namespace std;
using namespace glm;

Wall::Wall(Drawer* drawer,
		   const BasicGround& ground,
		   const std::vector<glm::vec3>& steps,
		   const glm::vec3& color,
	 	   float height,
	 	   float size,
	 	   float path_coeff,
	 	   float path_height,
	 	   const wallPilerArgs& pilers_args)
	: _ground(ground),
	  _drawer(drawer),
	  _vertexs(DYNAMIC_DRAW),
	  _colors(DYNAMIC_DRAW),
	  _indices(DYNAMIC_DRAW),
	  _buffers({&_vertexs, &_colors}),
	  _individual_drawing(true),
	  _vertexs_save(0),
	  _indices_save(0),
	  _steps(steps),
	  _color(color),
	  _path_color(color*0.8f),
	  _path_ground_color(color*0.5f),
	  _size(size),
	  _height(height),
	  _path_coeff(path_coeff),
	  _path_height(path_height),
	  _built(-1),
	  _pilers(pilers_args),
	  _extreme_piler_indices(-1, -1),
	  _pilers_time({})
{
	if(steps.size() < 2)
	{
		err("Wall::Wall() must takes at least 2 points, here, there is only " + to_string(steps.size()));
	}

	_achievement_times.x = utils::random_float(0.25, 0.7);
	_achievement_times.y = _achievement_times.x + utils::random_float(0.1, 0.3);

	this->build();

	_vertexs_save  = _vertexs.get_value();
	_indices_save = _indices.get_value();
}

void Wall::put_in_massive_draw()
{
	this->set_build_ended();
	_individual_drawing = false;
}

void Wall::build()
{
	for(int i=0; i<(int)_steps.size(); i++)
	{
		this->build_wall_connection(i);
	}

	for(int i=1; i<(int)_steps.size(); i++)
	{
		this->build_subwall_indices(i);
	}

	if(_pilers.inter_piler > 0.0f)
	{
		for(int i=0; i<(int)_steps.size()-1; i++)
		{
			this->build_pilers(i);
		}
	}
}

void Wall::build_pilers(unsigned int step_id)
{
	if(step_id >= _steps.size()-1)
	{
		err("Wall:build_pilers() step out of bound: " + to_string(step_id) + "-" + to_string(step_id+1)
			+ " on " + to_string(_steps.size()));
	}

	float inter_towers = glm::distance(_steps[step_id],_steps[step_id+1]);
	int piler_count = std::max(int(0), int(inter_towers/_pilers.inter_piler)); 
	vec3 wall_dir  = glm::normalize(_steps[step_id+1] - _steps[step_id]);
	vec3 piler_dir = glm::cross(wall_dir, vvv::y());

	_extreme_piler_indices.x = _vertexs.size();
	float piler_under_ground = 6.0f;

	for(int i = 0; i<piler_count; i++)
	{
		vec3 low_middle = glm::mix(_steps[step_id], _steps[step_id+1], float(i+1)/float(piler_count+1));
		vec3 middle = _ground.get_xyz(low_middle) - vvv::y()*piler_under_ground;
		float piler_height = _height - (middle.y - low_middle.y) + piler_under_ground;

		int first =
		this->add_point(middle + piler_dir*(_size/2.0f + _pilers.size*0.0f) + wall_dir*_pilers.size/2.0f , _color/1.4f);
		this->add_point(middle + piler_dir*(_size/2.0f + _pilers.size*1.0f) + wall_dir*_pilers.size/2.0f , _color);
		this->add_point(middle + piler_dir*(_size/2.0f + _pilers.size*1.0f) - wall_dir*_pilers.size/2.0f , _color);
		this->add_point(middle + piler_dir*(_size/2.0f + _pilers.size*0.0f) - wall_dir*_pilers.size/2.0f , _color/1.4f);

		this->add_point(middle + piler_dir*(_size/2.0f + _pilers.size*0.0f) + wall_dir*_pilers.size/2.0f + vvv::y()*(piler_height - _pilers.minus_heigth), _color/1.2f);
		this->add_point(middle + piler_dir*(_size/2.0f + _pilers.size*1.0f) + wall_dir*_pilers.size/2.0f + vvv::y()*(piler_height - _pilers.minus_heigth - _pilers.size), _color);
		this->add_point(middle + piler_dir*(_size/2.0f + _pilers.size*1.0f) - wall_dir*_pilers.size/2.0f + vvv::y()*(piler_height - _pilers.minus_heigth - _pilers.size), _color);
		this->add_point(middle + piler_dir*(_size/2.0f + _pilers.size*0.0f) - wall_dir*_pilers.size/2.0f + vvv::y()*(piler_height - _pilers.minus_heigth), _color/1.2f);
	
		_indices.add_rectangle(first + 7, first + 6, first + 5, first + 4);
		_indices.add_rectangle(first + 5, first + 1, first + 0, first + 4);
		_indices.add_rectangle(first + 7, first + 3, first + 2, first + 6);
		_indices.add_rectangle(first + 1, first + 5, first + 6, first + 2);
		_indices.add_rectangle(first + 0, first + 3, first + 7, first + 4);
	
		vec2 time_ex;
		time_ex.x = utils::random_float(0.0, 0.6);
		time_ex.y = utils::random_float(time_ex.x + 0.1, 1.0);
		
		_pilers_time.push_back(time_ex);
	}

	_extreme_piler_indices.y = _vertexs.size()-1;
}


void Wall::build_subwall_indices(unsigned int step_id)
{
	if(step_id <= 0 || step_id >= _steps.size())
		err("Wall::build_subwall(): cannot build step_id " + to_string(step_id) + " (must be between 1 and (_steps.size()-1)");

	unsigned int ii = this->wall_point_indice(step_id, FIRST_WALL_POINT);
	unsigned int i  = this->wall_point_indice(step_id-1, FIRST_WALL_POINT);

	_indices.add_rectangle(i+DOWN_OUT, ii+DOWN_OUT, ii+UP_OUT, i+UP_OUT);
	_indices.add_rectangle(i+DOWN_IN, i+UP_IN, ii+UP_IN, ii+DOWN_IN);
	
	_indices.add_rectangle(i+UP_OUT, ii+UP_OUT, ii+UP_OUT_PATH, i+UP_OUT_PATH);
	_indices.add_rectangle(i+UP_IN, i+UP_IN_PATH, ii+UP_IN_PATH, ii+UP_IN);
	
	_indices.add_rectangle(i+UP_OUT_PATH, ii+UP_OUT_PATH, ii+DOWN_OUT_PATH, i+DOWN_OUT_PATH);
	_indices.add_rectangle(i+UP_IN_PATH, i+DOWN_IN_PATH, ii+DOWN_IN_PATH, ii+UP_IN_PATH);

	_indices.add_rectangle(i+DOWN_IN_PATH, i+DOWN_OUT_PATH, ii+DOWN_OUT_PATH, ii+DOWN_IN_PATH);

	// For dynamic building, optionnal
	_indices.add_rectangle(ii+DOWN_OUT, ii+DOWN_IN, ii+UP_IN, ii+UP_OUT);
}	

unsigned int Wall::wall_point_indice(unsigned int step_id, wallConnectionPoint point)
{
	return N_CONNECTION_POINTS * step_id + point;
}

vec3 Wall::cur_to_succ(unsigned int step_id)
{
	return glm::normalize(_steps[step_id+1] - _steps[step_id]);
}

vec3 Wall::prev_to_cur(unsigned int step_id)
{
	return glm::normalize(_steps[step_id] - _steps[step_id-1]);
}

vec3 Wall::get_connection_direction(unsigned int step_id)
{
	if(step_id == 0)
	{
		return this->cur_to_succ(step_id);
	}
	if((int)(step_id) == (int)(_steps.size())-1)
	{
		return this->prev_to_cur(step_id);
	}
	vec3 dir1 = this->cur_to_succ(step_id);
	vec3 dir2 = this->prev_to_cur(step_id);
	return (dir1 + dir2)/(glm::length(dir1 + dir2)*sin((float(M_PI)-glm::angle(dir1, dir2))/2.0f));
}

void Wall::build_wall_connection(unsigned int step_id)
{
	if(step_id >= _steps.size())
	{
		err("Wall::build_wall_connection(): step_id out of bound : " + to_string(step_id) + " / " + to_string(_steps.size()));
	}

	vec3 direction = this->get_connection_direction(step_id);
	vec3 normal    = glm::cross(direction, vvv::y());

	this->add_point(_steps[step_id] + normal*_size/2.0f, _color*0.1f);
	this->add_point(_steps[step_id] - normal*_size/2.0f, _color*0.1f);
	this->add_point(_steps[step_id] - normal*_size/2.0f + vvv::y()*_height, _color);

	this->add_point(_steps[step_id] - normal*_size/2.0f*_path_coeff + vvv::y()*(_height), _path_color);
	this->add_point(_steps[step_id] - normal*_size/2.0f*_path_coeff + vvv::y()*(_height - _path_height), _path_ground_color);
	this->add_point(_steps[step_id] + normal*_size/2.0f*_path_coeff + vvv::y()*(_height - _path_height), _path_ground_color);
	this->add_point(_steps[step_id] + normal*_size/2.0f*_path_coeff + vvv::y()*(_height), _path_color);

	this->add_point(_steps[step_id] + normal*_size/2.0f + vvv::y()*_height, _color);


}

unsigned int Wall::add_point(const glm::vec3& vertex, const glm::vec3& color)
{
	unsigned int index = _vertexs.size();
	_vertexs.add_value(vertex);
	_colors.add_value(vec4(color, 1.0));
	
	return index;
}

const std::vector<const BasicBuffer*>& Wall::get_buffers()  const
{
	return _buffers;
}

const ElementBuffer&                   Wall::get_elements() const

{
	return _indices;
}


void Wall::set_build_ended()
{
	if(_built != 1)
	{
		_indices.change_value(_indices_save);
		_vertexs.change_value(_vertexs_save);
	}

	_built = 1;
}

void Wall::set_build_achievement(float achievement)
{
	if(achievement < 0.0)
	{
		_indices.change_value({});
		_built = -1;
		return;
	}
	if(_built != -1 && _indices.size()==0)
	{
		_indices.change_value(_indices_save);
		_vertexs.change_value(_vertexs_save);
	}
	if(achievement >= 0.999)
	{
		this->set_build_ended();
	}
	else
	{
		_built = 0;
		
		for(int p = FIRST_WALL_POINT; p<N_CONNECTION_POINTS; p++)
		{
			int i  = this->wall_point_indice(0, (wallConnectionPoint)p);
			int ii = this->wall_point_indice(1, (wallConnectionPoint)p);
			_vertexs.change_subvalue(ii, glm::mix(_vertexs_save[i], _vertexs_save[ii], std::max(achievement*2.0f-1.0, 0.0)));
		}

		for(int i=_extreme_piler_indices.x; i<=_extreme_piler_indices.y; i++)
		{
			if((i-_extreme_piler_indices.x)%8 >= 4)
			{
				int time_index = (i-_extreme_piler_indices.x)/8;
				if(time_index >= (int)_pilers_time.size())
				{
					err("Wall::set_build_achievement _pilers_time out of bound: " + to_string(time_index) + "/" + to_string(_pilers_time.size()));
				}
				float start = _pilers_time[time_index].x;
				float end   = _pilers_time[time_index].y;

				if(achievement >= start && achievement <= end)
				{
					float local_achievement = (achievement-start)/(end - start);
					_vertexs.change_subvalue(i, glm::mix(_vertexs_save[i-4], _vertexs_save[i], local_achievement));
				}
				else if(achievement>end)
				{
					_vertexs.change_subvalue(i, _vertexs_save[i]);
				}
				else
				{
					_vertexs.change_subvalue(i, _vertexs_save[i-4]);
				}
			}
		}
	}
}

const std::vector<glm::vec3>& Wall::get_steps() const
{
	return _steps;
}
float Wall::get_size() const
{
	return _size;
}

Drawer* Wall::get_drawer()
{
	return _drawer;
}

bool Wall::multisample_draw() const
{
	return true;
}

bool Wall::bloom_draw() const
{
	return false;
}

float Wall::get_start_achievement() const
{
	return _achievement_times.x;
}
float Wall::get_end_achievement() const
{
	return _achievement_times.y;
}