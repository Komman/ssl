#include "tower.hpp"

using namespace std;
using namespace glm;

Tower::Tower(Drawer* drawer,
			 const glm::vec3& center,
		     const glm::vec3& direction,
		     const std::vector<stageRelief>& reliefs,
		     const glm::vec4& base_color)
	: _drawer(drawer),
	  _center(center),
	  _direction(direction),
	  _base_color(base_color),
	  _reliefs(reliefs),
	  _max_relief_size(this->find_max_relief_size(reliefs)),
	  _max_stage_vertex_size({}),
	  _achievement_finished(false)
{
	this->build();
}

void Tower::build()
{
	vec3 stage_start = _center;
	for(int i=0; i<(int)_reliefs.size(); i++)
	{
		const auto& relief = _reliefs[i];
		this->build_stage(stage_start, _direction, _base_color * relief.size/_max_relief_size, i);
		stage_start += vvv::y()*relief.heigth;
	}

	int greater_vertex = this->vertexs_size();
	_top_indices = {greater_vertex - 1,
					greater_vertex - 3,
					greater_vertex - 5,
					greater_vertex - 7};

	_points_save = ((const GLBuffer<vec3>*)(this->get_buffers()[TowerInterface::VERTEXS]))->get_value();
	_indices_save = _indices.get_value();
}

void Tower::finalize_roof(const std::vector<int>& top_indices)
{
	if(_indices.size() < 8)
	{
		err("Tower::build_roof(): No roof can be build without base");
	}

	_indices.add_rectangle(top_indices[0], top_indices[1], top_indices[2], top_indices[3]);
}

float Tower::roof_start_time() const
{
	return 1.0f;
}

void  Tower::set_roof_acheivement(float achievement)
{

}

void Tower::build_roof()
{
	if(_top_indices.size() < 4)
	{
		err("_top_indices not large enough");
	}
	
	this->finalize_roof(_top_indices);

	_points_save = ((const GLBuffer<vec3>*)(this->get_buffers()[TowerInterface::VERTEXS]))->get_value();
	_indices_save = _indices.get_value();
}

void Tower::rebuild(const glm::vec3& center,
				 	const glm::vec3& direction,
				 	const std::vector<stageRelief>& reliefs,
				 	const glm::vec4& base_color)
{
	_center = center;
	_direction = direction;
	_reliefs = reliefs;
	_base_color = base_color;
	_max_relief_size = this->find_max_relief_size(reliefs);
	_max_stage_vertex_size.resize(0);
	_points_save.resize(0);
	_indices_save.resize(0);
	_max_stage_index.resize(0);
	_indices.change_value({});
	_vertexs.change_value({});
	_colors.change_value({});

	this->build();
}

float Tower::find_max_relief_size(const std::vector<stageRelief>& reliefs)
{
	float ret = 0.0;
	for(const auto& r : reliefs)	
	{
		if(r.size > ret)
		{
			ret = r.size;
		}
	}
	return ret;
}

void Tower::build_stage(const glm::vec3& center,
						const glm::vec3& direction,
						const glm::vec4& color,
						unsigned int relief_id)
{
	#ifdef SSL_DEBUG
	if(relief_id >= _reliefs.size())
	{
		err("call to Tower::build_stage() with a relief_id out of bound : "  + to_string(relief_id)
			+ "/" + to_string(_reliefs.size()));
	}
	#endif

	const auto& relief = _reliefs[relief_id];
	vec3 heigth = vvv::y()*relief.heigth;
	vec3 direction2 = glm::normalize(glm::cross(direction, heigth));
	vec3 base[4] = {
		center + (direction - direction2)*relief.size/2.0f,
		center + (direction + direction2)*relief.size/2.0f,
		center - (direction - direction2)*relief.size/2.0f,
		center - (direction + direction2)*relief.size/2.0f
	};

	unsigned int start_index = this->vertexs_size();

	for(int i=0; i<4; i++)
	{
		this->add_point(base[i], color);
		this->add_point(base[i] + heigth, color);
	}

	for(int i=0; i<4; i++)
	{
		_indices.add_rectangle(start_index + 2*i,
							   start_index + 2*i+1,
							   start_index + (2*(i+1))%8 + 1,
							   start_index + (2*(i+1))%8);
	}
	_max_stage_vertex_size.push_back(_vertexs.size());

	if(relief_id > 0)
	{
		unsigned int prev_top[4] = {
			start_index - 7,
			start_index - 5,
			start_index - 3,
			start_index - 1
		};
		unsigned int cur_base[4] = {
			start_index + 0,
			start_index + 2,
			start_index + 4,
			start_index + 6
		};

		float sizediff = _reliefs[relief_id].size - _reliefs[relief_id-1].size;
		if(abs(sizediff)>0.01)
		{
			for(int i=0; i<4; i++)
			{
				_indices.add_rectangle(prev_top[i], cur_base[i], cur_base[(i+1)%4], prev_top[(i+1)%4]);
			}
		}		
	}

	_max_stage_index.push_back(_indices.size());
}

void Tower::set_build_achievement(float achievement)
{
	if(achievement<0.0)
	{
		_indices.change_value({});	
		return;
	}
	if(achievement >= 0.999)
	{
		if(!_achievement_finished)
		{
			_indices.change_value(_indices_save);
			_vertexs.change_value(_points_save);
		}
		
		_achievement_finished = true;
	}
	else
	{
		_achievement_finished = false;
		float roof_start = this->roof_start_time();

		if(achievement < roof_start)
		{
			float body_achievement  = achievement / roof_start;
			int   stage             = int(body_achievement * (float)(_max_stage_vertex_size.size()));
			float stage_achievement = body_achievement * (float)(_max_stage_vertex_size.size()) - float(stage);

			if(stage >= (int)_max_stage_vertex_size.size())
			{
				if(_max_stage_vertex_size.size() == 0)
				{
					err("wesh pas marché ici :" + SSL_FILE_AND_LINE);
				}

				stage = (int)_max_stage_vertex_size.size()-1;
			}

			if(stage_achievement<0.0 || stage_achievement>1.0)
				err("pas normal ici: " + SSL_FILE_AND_LINE);			

			this->set_stage_achievement(stage, stage_achievement);
		}
		else
		{
			if(_max_stage_vertex_size.size() >= 1)
			{
				this->set_stage_achievement((int)_max_stage_vertex_size.size()-1, 1.0f);
			}

			this->set_roof_acheivement( (achievement - roof_start) / (1.0f - roof_start) );
		}
	}
}

void Tower::set_stage_achievement(int stage, float stage_achievement)
{
	for(int i=0; i<(int)_max_stage_index[std::max(stage-1, 0)]; i++)
	{
		_vertexs.change_subvalue(_indices_save[i], _points_save[_indices_save[i]]);
	}

	_indices.change_value(std::vector<unsigned int>(_indices_save.begin(),
													_indices_save.begin() + _max_stage_index[stage]));

	unsigned int prev_top[4] = {
		_max_stage_vertex_size[stage] - 7,
		_max_stage_vertex_size[stage] - 5,
		_max_stage_vertex_size[stage] - 3,
		_max_stage_vertex_size[stage] - 1
	};

	if(stage > 0)// && _reliefs[stage-1].size < _reliefs[stage].size)
	{
		float semi_achievement = stage_achievement*2.0;

		unsigned int prevprev_top[4] = {
			_max_stage_vertex_size[stage-1] - 7,
			_max_stage_vertex_size[stage-1] - 5,
			_max_stage_vertex_size[stage-1] - 3,
			_max_stage_vertex_size[stage-1] - 1
		};

		if(semi_achievement < 1.0)
		{
			this->set_stage_achievement_linear_keep_old_size(stage, semi_achievement, prev_top, prevprev_top);	
		}
		else
		{
			semi_achievement-=1.0;

			this->set_stage_achievement_from_old_size_to_new(stage, semi_achievement, prev_top, prevprev_top);
		}
	}
	else
	{
		this->set_stage_achievement_linear_heigth(stage, stage_achievement, prev_top);
	}

	_indices.add_rectangle(prev_top[3], prev_top[2], prev_top[1], prev_top[0]);
}

void Tower::set_stage_achievement_linear_heigth(int stage, float stage_achievement, unsigned int prev_top[4])
{
	for(int i=0;i<4;i++)
	{
		_vertexs.change_subvalue(prev_top[i], _points_save[prev_top[i]]*stage_achievement + _points_save[prev_top[i]-1]*(1.0f-stage_achievement));
	}
}
void Tower::set_stage_achievement_linear_keep_old_size(int stage, float stage_achievement,
													   unsigned int prev_top[4],
													   unsigned int prevprev_top[4])
{
	for(int i=0;i<4;i++)
	{
		vec3 new_top = glm::mix(_points_save[prevprev_top[i]],
							    _points_save[prevprev_top[i]] + vvv::y()*_reliefs[stage].heigth,
							    stage_achievement);

		_vertexs.change_subvalue(prev_top[i], new_top);
		_vertexs.change_subvalue(prev_top[i]-1, _points_save[prevprev_top[i]]);
	}
}

void Tower::set_stage_achievement_from_old_size_to_new(int stage, float stage_achievement,
													   unsigned int prev_top[4],
													   unsigned int prevprev_top[4])
{
	for(int i=0;i<4;i++)
	{
		vec3 new_top = glm::mix(_points_save[prevprev_top[i]] + vvv::y()*_reliefs[stage].heigth,
								_points_save[prev_top[i]],
								stage_achievement);
		vec3 new_bottom = glm::mix(_points_save[prevprev_top[i]],
								   _points_save[prev_top[i]-1],
								   stage_achievement);

		_vertexs.change_subvalue(prev_top[i],   new_top);
		_vertexs.change_subvalue(prev_top[i]-1, new_bottom);
	}
}

Drawer* Tower::get_drawer() 
{
	return _drawer;
}

bool Tower::multisample_draw() const
{
	return true;
}

bool Tower::bloom_draw() const
{
	return false;
}
const std::vector<glm::vec3>&    Tower::get_points_save()  const
{
	return _points_save;
}

const std::vector<unsigned int>& Tower::get_indices_save() const
{
	return _indices_save;
}
