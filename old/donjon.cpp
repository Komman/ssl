#include "donjon.hpp"

using namespace std;
using namespace glm;

unsigned int Donjon::abs_donjon_cont = 0;

Donjon::Donjon(const ShaderGenerator& generator,
			   const BasicGround& ground,
			   Graph<CastleVertex, CastleEdge>&             architecture,
		       const std::vector<std::vector<unsigned int>>& enclosure_indices,
		       const glm::vec3& center,
		       const glm::vec3& color,
		       float height,
		       unsigned int precision,
		       DrawerPackController& drawer_controller)
	: _indices(STATIC_DRAW),
	  _vertexs(STATIC_DRAW),
	  _colors(STATIC_DRAW),
	  _ground(ground),
	  _architecture(architecture),
	  _enclosure_indices(enclosure_indices),
	  _buffers({&_vertexs, &_colors}),
	  _grid(precision, std::vector<int>(precision, 0)),
	  _position(center),
	  _direction(1.0, 0.0, 0.0),
	  _color(vec4(color, 1.0)),
	  _height(height),
	  _max_base_height(ground.get_y(center)),
	  _down_dark_coeff(0.6f),
	  _up_dark_coeff(0.5f),
	  _larger_from(0.4f),
	  _random_color_coeff(0.6f),
	  _donjon_id(abs_donjon_cont),
	  _ydep("donjon_ydep" + to_string(_donjon_id), 0.0),
	  _center("donjon_center" + to_string(_donjon_id), _position),
	  _larger("donjon_larger" + to_string(_donjon_id), 1.0),
	  _drawer(generator, "donjon_vertex", "donjon_fragment", {"all_lights_mesh_normal"}, drawer_controller)
{
	_drawer.depth(true);
	this->compute_grid_size();
	this->build_grid();
	this->build_borders();
	base_vertex_count = _vertexs.size();
	this->build_donjon_up();

	// this->show_grid_terminal();

	abs_donjon_cont++;
}

void Donjon::build_donjon_up()
{
	float h = _max_base_height + _height;
	vec3 floor_min = _position;
	vec3 floor_max = _position;
	for(int i=0; i<base_vertex_count; i++)
	{
		vec3 new_top(_vertexs[i].x, h, _vertexs[i].z);
		floor_min = glm::min(floor_min, new_top);
		floor_max = glm::max(floor_max, new_top);
		this->add_point(new_top, _color*_down_dark_coeff* utils::random_float(_random_color_coeff,1.0));
	}

	for(int i=0; i<base_vertex_count; i++)
	{
		_indices.add_rectangle((i  ),
							   (i  ) + base_vertex_count,
							   (i+1) % base_vertex_count + base_vertex_count,
			  				   (i+1) % base_vertex_count);
	}

	floor_min.y = h;
	floor_max.y = h;
	vec3 extr1(floor_min.x, h, floor_max.z);
	vec3 extr2(floor_max.x, h, floor_min.z);
	float diagonal = glm::distance(floor_min, floor_max);

	this->add_point((extr1 + extr2)/2.0f - vvv::y()*_height/2.3f, _color*_random_color_coeff*_down_dark_coeff);

	unsigned int floor_first = _vertexs.size();

	vec3* floor_stage[4]={
		&floor_min,
		&extr1,
		&floor_max,
		&extr2
	};

	for(int i=0; i<4; i++)
	{
		vec3 coord = *floor_stage[i];
		this->add_point(coord, _color*_random_color_coeff*(1.0f+_random_color_coeff)/2.0f);
	}


	_up_top    = *floor_stage[0];
	_up_bottom = *floor_stage[0];
	for(int i=0; i<4; i++)
	{
		vec3 coord = *floor_stage[i] + vvv::y()*diagonal/70.0f;
		this->add_point(coord, _color*_up_dark_coeff);
		
		_up_top    = glm::min(_up_top   , coord);
		_up_bottom = glm::max(_up_bottom, coord);
	}
	

	for(int i=0; i<4; i++)
	{
		_indices.add_triangle(floor_first - 1,
						      floor_first + (i+1)%4,
						      floor_first + i);
	}

	_indices.add_rectangle(floor_first + 4 + 0,
						   floor_first + 4 + 1,
						   floor_first + 4 + 2,
						   floor_first + 4 + 3);

	for(int i=0; i<4; i++)
	{
		_indices.add_rectangle(floor_first + i,
						       floor_first + (i+1)%4,
						       floor_first + 4 + (i+1)%4,
						       floor_first + 4 + i);
	}
}

void Donjon::build_grid()
{
	for(int x=0; x<(int)_grid.size(); x++)
	{
		_grid[0][x] = 1;
		_grid[x][0] = 1;
		_grid[_grid.size() -1][x] = 1;
		_grid[x][_grid.size() -1] = 1;
	}

	for(int ei = 0; ei<(int)_enclosure_indices[0].size(); ei++)
	{
		int i  = _enclosure_indices[0][ei];
		int ii = _enclosure_indices[0][(ei+1)%_enclosure_indices[0].size()];

		this->compute_ray_tracing(_architecture[i ],
							      _architecture[ii]);

		uvec2 gcoord = this->grid_coord(_architecture[i]);
		_grid[gcoord.x][gcoord.y] = 1;
	}
}

int Donjon::grid_value(const glm::ivec2& coord)
{
	#ifdef SSL_DEBUG
	if(coord.x<0 || coord.y<0 || coord.x>=(int)_grid.size() || coord.y>=(int)_grid.size())
	{
		err("Donjon::grid_value: coord out of bound");
	}
	#endif

	return _grid[coord.x][coord.y];
}

void Donjon::set_grid_value(const glm::ivec2& coord, int value)
{
	#ifdef SSL_DEBUG
	if(coord.x<0 || coord.y<0 || coord.x>=(int)_grid.size() || coord.y>=(int)_grid.size())
	{
		err("Donjon::set_grid_value: coord out of bound");
	}
	#endif

	_grid[coord.x][coord.y] = value;
}

const ivec2& border_direction(int direction)
{
	static ivec2 all_border_directions[4]={
		ivec2(-1, 0),
		ivec2( 0,-1),
		ivec2( 1, 0),
		ivec2( 0, 1)
	};

	#ifdef SSL_DEBUG
	if(direction<0 || direction>=4)
	{
		err("border_direction out of bound: " + to_string(direction));
	}
	#endif
	
	return all_border_directions[direction];
}

unsigned int Donjon::count_wall_neigbourgs(const glm::ivec2& pos)
{
	unsigned int count = 0;

	for(int i=0; i<4; i++)
	{
		int value = this->grid_value(pos + border_direction(i));
		if(value < START_STONE && value > 0)
		{
			count++;
		}
	}

	return count;
}

unsigned int Donjon::count_stares(const glm::ivec2& pos)
{
	unsigned int count = 0;

	for(int i=0; i<4; i++)
	{
		int value = this->grid_value(pos + border_direction(i) + border_direction((i+1)%4));
		if(value >= START_STONE)
		{
			count++;
		}
	}

	return count;
}

void Donjon::place_base_wall(const glm::ivec2& pos, int stone)
{	
	if(this->grid_value(pos) == 0 && this->count_wall_neigbourgs(pos) < 3)
	{
		this->set_grid_value(pos, stone);
		vec3 new_point = this->coord_from_grid(pos);
		
		if(this->count_stares(pos) == 0 || rand() % 2 == 0)
		{
			this->add_point(new_point, _color * utils::random_float(_random_color_coeff,1.0));
		}
	}
}

void Donjon::build_borders()
{
	ivec2 current = this->grid_coord(_position);
	int direction = 0;

	while(this->grid_value(current + border_direction(direction)) == 0)
	{
		current += border_direction(direction);
	}
	direction          = (direction + 1)%4;
	int stone_to_place = START_STONE;

	for(int count=0; count<4*(int)_grid.size(); count++)
	{
		int   possible_dir   = utils::circulator(direction-1, 4);
		ivec2 possible       = border_direction(possible_dir);
		int   possible_block = this->grid_value(current + possible);
		if(possible_block == 0 || possible_block == STONE)
		{
			this->place_base_wall(current, stone_to_place);
			current += possible;
			direction = possible_dir;
			this->place_base_wall(current, stone_to_place);
		}
		else
		{
			if(possible_block == START_STONE)
			{
				this->place_base_wall(current, stone_to_place);
				break;
			}
			else
			{
				int next_on_grid = this->grid_value(current + border_direction(direction));
				if(next_on_grid == 0 || next_on_grid == STONE)
				{
					if(this->grid_value(current) == 0)
					{
						this->set_grid_value(current, stone_to_place);
					}
					current += border_direction(direction);
				}
				else
				{
					if(next_on_grid == START_STONE)
					{
						this->place_base_wall(current, stone_to_place);
						break;
					}
					else
					{
						direction = (direction + 1)%4;
						this->place_base_wall(current, stone_to_place);
					}
				}
			}
		}
		stone_to_place = STONE;
		// this->show_grid_terminal();
	}
}


void Donjon::compute_ray_tracing(const CastleVertex& t1,
						         const CastleVertex& t2)
{
	vec2 p1(t1.get_center().x, t1.get_center().z);
	vec2 p2(t2.get_center().x, t2.get_center().z);
	int step_count = 2 * int(glm::distance(p1, p2) / glm::length(_grid_case_size));

	for(int s = 1; s<step_count; s++)
	{
		float coeff = float(s)/float(step_count);
		uvec2 gcoord = this->grid_coord(glm::mix(p1, p2, coeff));
		
		if(_grid[gcoord.x][gcoord.y] == 0 || _grid[gcoord.x][gcoord.y] == 3)
		{
			_grid[gcoord.x][gcoord.y] = 2;
		}
		
		for(int dx = -1; dx<=1; dx++)
		{
			for(int dy = -1; dy<=1; dy++)
			{
				int nx = gcoord.x + dx;
				int ny = gcoord.y + dy;
				if(nx >= 0 && ny >=0 && nx <(int)_grid.size() && ny <(int)_grid.size())
				{
					if(_grid[nx][ny] == 0)
					{
						_grid[nx][ny] = 3;
					}
				}
			}
		}
	}	
}

glm::vec3 Donjon::coord_from_grid(const glm::uvec2& grid_case)
{
	vec2 dep = _grid_case_size * (vec2(grid_case) + vec2(0.5, 0.5));
	return _ground.get_xyz(_grid_top + vec3(dep.x, 0.0, dep.y));
}

glm::uvec2 Donjon::grid_coord(const CastleVertex& t)
{
	return this->grid_coord(t.get_center());
}
glm::uvec2 Donjon::grid_coord(const glm::vec3& point)
{
	return this->grid_coord(vec2(point.x, point.z));
}

glm::uvec2 Donjon::grid_coord(const glm::vec2& point)
{
	uvec2 coord;
	coord.x = (unsigned int)(int( (point.x - _grid_top.x - 0.5f)/_grid_case_size.x ));
	coord.y = (unsigned int)(int( (point.y - _grid_top.z - 0.5f)/_grid_case_size.y ));

	coord = glm::clamp(coord, uvec2(0, 0), uvec2(_grid.size(), _grid.size()));

	return coord;
}


void Donjon::compute_grid_size()
{
	#ifdef SSL_DEBUG
	if(_enclosure_indices.size() <= 0)
	{
		err("_enclosure_indices must has at least one enclosure to build a donjon");
	}
	if(_enclosure_indices[0].size() <=3)
	{
		err("_enclosure_indices must have at least 3 towers to build a donjon");
	}
	#endif

	_grid_top    = _ground.get_xyz(_architecture[_enclosure_indices[0][0]].get_center());
	_grid_bottom = _grid_top;

	for(auto i : _enclosure_indices[0])
	{
		_grid_top    = glm::min(_grid_top   , _architecture[i].get_center());
		_grid_bottom = glm::max(_grid_bottom, _architecture[i].get_center());
		
		_max_base_height = std::max(_max_base_height, _ground.get_y(_architecture[i].get_center()));
	}

	_grid_case_size.x = (_grid_bottom.x - _grid_top.x) / float(_grid.size());
	_grid_case_size.y = (_grid_bottom.z - _grid_top.z) / float(_grid.size());
}

void Donjon::show_grid_terminal() const
{
	for(int y=0; y<(int)_grid.size(); y++)
	{
		for(int x=0; x<(int)_grid.size(); x++)
		{
			if(_grid[x][y] == 0)
				cout<<TERMB::WHITE;
			if(_grid[x][y] == 1)
				cout<<TERMB::RED;
			if(_grid[x][y] == 2)
				cout<<TERMB::CYAN;
			if(_grid[x][y] == 3)
				cout<<TERMB::ORANGE;
			if(_grid[x][y] >= START_STONE)
				cout<<TERMB::BLACK;
			cout<<_grid[x][y]<< " ";
		}
		cout<<endl;
	}
	cout<<TERM::NOCOL<<endl;
}


void Donjon::add_point(const glm::vec3& position, const glm::vec4& color)
{
	_vertexs.add_value(position);
	_colors.add_value(color);
}

unsigned int Donjon::vertexs_size() const
{
	return _vertexs.size();
}
	
const std::vector<const BasicBuffer*>& Donjon::get_buffers()  const
{
	return _buffers;
}

const ElementBuffer& Donjon::get_elements() const
{
	return _indices;
}


void Donjon::set_build_achievement(float achievement)
{
	if(achievement < 0.0)
	{
		_larger = _larger_from;
		_ydep   = -(_height + _max_base_height);
	}
	else if(achievement > 1.0)
	{
		_larger = 1.0f;
		_ydep   = 0.0;
	}
	else
	{
		if(achievement < _larger_from)
		{
			float local_ach = achievement/_larger_from;
			_ydep = -(1.0 - local_ach)*(_height + _max_base_height);
			_larger = _larger_from;
		}
		else
		{
			_ydep = 0.0;
			_larger = achievement;
		}		
	}
}

Drawer* Donjon::get_drawer()
{
	return &_drawer;
}

const glm::vec3& Donjon::get_center()    const
{
	return _position;
}
const glm::vec3& Donjon::get_direction() const
{
	return _direction;
}

const glm::vec3& Donjon::get_building_top()    const
{
	return _up_top; 
}

const glm::vec3& Donjon::get_building_bottom() const
{
	return _up_bottom; 
}

bool Donjon::multisample_draw() const
{
	return true;
}

bool Donjon::bloom_draw() const
{
	return false;
}

