#include "shooting_tower.hpp"

ShootingTower::ShootingTower(Drawer* drawer,
				const glm::vec3& center,
			    const glm::vec3& direction,
			    const std::vector<stageRelief>& reliefs,
			    const glm::vec4& base_color,
				float wall_height,
				float wall_size,
				float hat,
				const glm::vec4& roof_color)
	: WalledTower(drawer,
		  	 	  center,
				  direction,
				  reliefs,
				  base_color,
				  wall_height,
				  wall_size,
				  hat,
				  roof_color),
	  _shooter(center + vvv::y()*wall_height, 50.0f)
{

}

void ShootingTower::animate(float dt)
{
	_shooter.animate(dt);
}

void ShootingTower::set_target(const BasicDirectedPoint* target)
{
	_shooter.set_target(target);
}