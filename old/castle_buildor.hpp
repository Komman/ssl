#ifndef _CASTLE_BUILDOR_HPP_
#define _CASTLE_BUILDOR_HPP_

#include "../motor/controlled_drawer.hpp"

#include "castle_structure.hpp"
#include "donjon.hpp"

struct castleBuildorParameters
{
	SharedRessources* shared_ressources;
	const BasicGround* ground;
	StaticLigthor* slightor;
	castleStructureParameters structure_params;
	DrawerPackController* drawer_controller;
};

class CastleBuildor : public CastleStructure
{
public:

	CastleBuildor(const castleBuildorParameters& params);

	static castleBuildorParameters default_buildings_parameters(SharedRessources& shared_ressources,
																const BasicGround& ground,
																StaticLigthor& slightor,
																DrawerPackController& drawer_controller);


private:

	castleBuildorParameters _builing_parameters;
};

#endif //_CASTLE_BUILDOR_HPP_
