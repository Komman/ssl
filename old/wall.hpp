#ifndef _WALL_HPP_
#define _WALL_HPP_

#include "../../ssl/ssl.hpp"

#include "castle_graph.hpp"
#include "ground.hpp"

using namespace ssl;

struct wallPilerArgs
{
	bool pilers;
	float inter_piler;
	float size;
	float minus_heigth;
};

class Wall : public EdgeMesh
{
public:
	Wall(Drawer* drawer,
		 const BasicGround& ground,
		 const std::vector<glm::vec3>& steps,
		 const glm::vec3& color,
		 float heigth,
		 float size,
		 float path_coeff,
		 float path_height,
		 const wallPilerArgs& pilers_args);

	const std::vector<const BasicBuffer*>& get_buffers()  const;
	const ElementBuffer&                   get_elements() const;

	virtual wallType get_wall_type() const override {return PRIMARY_WALL;}
	void set_build_achievement(float achievement) override;

	void put_in_massive_draw();
	virtual bool special_drawing()   const override {return !_individual_drawing;}

	float get_start_achievement() const override;
	float get_end_achievement()   const override;
	const std::vector<glm::vec3>& get_steps() const;
	float get_size() const;

	virtual Drawer* get_drawer() override;
	virtual bool    multisample_draw() const override;
	virtual bool    bloom_draw() const override;

protected:
	void build();
	void build_subwall_indices(unsigned int step_id);
	void build_wall_connection(unsigned int step_id);
	void build_pilers(unsigned int step_id);
private:

	enum wallConnectionPoint {FIRST_WALL_POINT = 0,
							  DOWN_OUT=FIRST_WALL_POINT, DOWN_IN,
							  UP_IN, UP_IN_PATH, DOWN_IN_PATH,
							  DOWN_OUT_PATH, UP_OUT_PATH, UP_OUT,

							  N_CONNECTION_POINTS};

	unsigned int wall_point_indice(unsigned int step_id, wallConnectionPoint point);
	glm::vec3 get_connection_direction(unsigned int step_id);
	glm::vec3 cur_to_succ(unsigned int step_id);
	glm::vec3 prev_to_cur(unsigned int step_id);
	void set_build_ended();

	unsigned int add_point(const glm::vec3& vertex, const glm::vec3& color);

	const BasicGround& _ground;
	Drawer* _drawer;
	
	ArrayBuffer<glm::vec3> _vertexs;
	ArrayBuffer<glm::vec4> _colors;
	ElementBuffer          _indices;
	std::vector<const BasicBuffer*> _buffers;
	bool _individual_drawing;

	std::vector<glm::vec3>       _vertexs_save;
	std::vector<unsigned int>    _indices_save;
	
	const std::vector<glm::vec3> _steps;
	glm::vec3  _color;
	glm::vec3  _path_color;
	glm::vec3  _path_ground_color;
	float      _size;
	float      _height;
	float      _path_coeff;
	float      _path_height;
	int        _built;

	wallPilerArgs      _pilers;
	glm::ivec2         _extreme_piler_indices;
	std::vector<glm::vec2> _pilers_time;

	glm::vec2 _achievement_times;

/*
		   <> _size x _path_coeff / 2.0

	7--6  3--2	 ^
	|  5--4  |   V _path_height
	|		 |
	|		 | _height
	|		 |
	0--------1
	  _size
*/

};

#endif //_WALL_HPP_
