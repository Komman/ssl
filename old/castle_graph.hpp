#ifndef _CASTLE_GRAPH_HPP_
#define _CASTLE_GRAPH_HPP_

#include "../../ssl/ssl.hpp"

using namespace ssl;

using timeAndLast = std::pair<float, float>;

class BuildingMesh : public ssl::ElementDrawableObject
{
public:

	virtual void    set_build_achievement(float achievement) {}
	virtual float   get_start_achievement() const {return 0.0;}
	virtual float   get_end_achievement()   const {return 1.0;}
	virtual bool    special_drawing() const     {return false;}
	virtual Drawer* get_drawer()=0;
	virtual bool    multisample_draw() const=0;
	virtual bool    bloom_draw() const=0;

	virtual ~BuildingMesh() {};
};


class VertexMesh: public BuildingMesh
{
public:

	enum buildingType {NO_TYPE=0, TOWER};

	virtual void animate(float dt) {}

public:

	virtual buildingType get_building_type() const {return NO_TYPE;}

	virtual const glm::vec3& get_center()    const=0;
	virtual const glm::vec3& get_direction() const=0;

	virtual ~VertexMesh() {};
};

class EdgeMesh: public BuildingMesh 
{
public:

	enum wallType {NO_TYPE=0, PRIMARY_WALL};
	
	virtual void animate(float dt) {}

public:

	virtual wallType get_wall_type() const {return NO_TYPE;}

	virtual ~EdgeMesh() {};
	
};

#endif //_CASTLE_GRAPH_HPP_
