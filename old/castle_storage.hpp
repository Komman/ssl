#ifndef _CASTLE_STORAGE_HPP_
#define _CASTLE_STORAGE_HPP_

#include <memory>

#include "../utils/graph.hpp"
#include "../motor/post_processor.hpp"

#include "basic_ground.hpp"
#include "walled_tower.hpp"
#include "wall.hpp"


template<typename NodeType>
struct BasicCastleVertex
{
public:
	using MeshVector = std::vector<std::unique_ptr<NodeType>>;

public:
	BasicCastleVertex();

	void add_drawable_object(std::unique_ptr<NodeType>&& obj);
	const MeshVector& get_meshes() const;

	virtual void animate(float dt);

protected:
	std::vector<std::unique_ptr<NodeType>> _meshes;
};


/*
	TOWER STORAGE
*/

struct CastleVertex : public BasicCastleVertex<VertexMesh>
{
public:
	CastleVertex(int enclosure_index,
				 glm::vec3 center,
				 float wall_height,
				 float wall_size);

	glm::vec3 get_center() const;
	float     get_wall_size() const;
	float     get_wall_height() const;
	int       get_enclosure() const;

private:

	glm::vec3 _center;
	float     _wall_height;
	float     _wall_size;
	int       _enclosure;
};

/*
	WALL STORAGE
*/

struct CastleEdge : public BasicCastleVertex<EdgeMesh>
{
public:
	CastleEdge();

private:

};

/*
	TEMMPLATE IMPLEMENTATION
*/

template<typename NodeType>
BasicCastleVertex<NodeType>::BasicCastleVertex()
	: _meshes(0)
{

}

template<typename NodeType>
void BasicCastleVertex<NodeType>::add_drawable_object(std::unique_ptr<NodeType>&& obj)
{
	_meshes.push_back(std::move(obj));
}

template<typename NodeType>
void BasicCastleVertex<NodeType>::animate(float dt)
{
	for(auto& m : _meshes)
	{
		m->animate(dt);
	}
}

template<typename NodeType>
const typename BasicCastleVertex<NodeType>::MeshVector& BasicCastleVertex<NodeType>::get_meshes() const
{
	return _meshes;
}


#endif //_CASTLE_STORAGE_HPP_
