#ifndef _GRAPH_HPP_
#define _GRAPH_HPP_

#include <vector>
#include <string>

#define GRAPH_DEBUG

template<typename T>
void unordered_vector_pop(std::vector<T>& v, unsigned int index);

/*
	GRAPH CLASS
*/
template<typename NodeType, typename EdgeContent = int>
class Graph
{
public:
	using Index     = unsigned int;
	using EdgeIndex = unsigned int;
	using Edge      = std::pair<Index, Index>;
	using EdgeType  = std::pair<Edge, EdgeContent>;

public:
	Graph();

	unsigned int size();
	// returns the index of the vertex
	Index add_vertex(const NodeType& content);
	Index add_vertex(NodeType&& content);

	void add_oriented_edge(const Edge& edge, const EdgeContent& content = EdgeContent());
	void add_oriented_edge(const Edge& edge, EdgeContent&&      content = EdgeContent());
	void add_edge(const Edge& edge,          const EdgeContent& content = EdgeContent());
	void add_edge(const Edge& edge,          EdgeContent&&      content = EdgeContent());
	// void  remove_oriented_edge(const Edge& edge);
	// void  remove_edge(const Edge& edge);

	NodeType& operator[](Index i);    // vertex i
	EdgeType& operator()(EdgeIndex i);// edge i

	const std::vector<NodeType>&  vertexs();
	const std::vector<EdgeType>&  edges();
	const EdgeType&               edge(EdgeIndex i);
	const std::vector<Index>&     neighbours(Index i);
	const std::vector<EdgeIndex>& connected_edges(Index i);

private:
	void err(const std::string& msg);
	void check_edge_bound(const Edge& edge);
	void check_edge_index_bound(EdgeIndex i);
	void check_vertex_bound(Index i);

	std::vector<NodeType>               _vertexs;
	std::vector<std::vector<Index>>     _adjacency_list;
	std::vector<std::vector<EdgeIndex>> _adjacency_edges;
	std::vector<EdgeType>               _edges;
};

#include "graph_impl.hpp"

#endif //_GRAPH_HPP_
