#ifndef _PILER_CONNECTOR_INTERFACE_HPP_
#define _PILER_CONNECTOR_INTERFACE_HPP_

#include <array>

#include "../motor/gutils.hpp"
#include "../motor/planar_symmetry.hpp"

#include "castle_graph.hpp"
#include "l_base_buildor.hpp"


/*	
	plateformIndices:

   	 top
	    1- |  
	    |  | SYMM
	    0- | 
  bottom
*/

struct plateformIndices
{
	glm::uvec2 low; // x: xmin, y: xmax
	glm::uvec2 top; // x: xmin, y: xmax
};

struct plateformLocation
{
	// NO SYMMETRY
	glm::vec3 south_east;
	glm::vec3 north_west;
};

struct roofLinkor
{
	// Both north and south vectors must have the same size
	std::vector<unsigned int>      north;
	std::vector<unsigned int>      south;
	std::vector<plateformLocation> plateforms;
};

/*  
   corners[i]

          |      ^ x
	 1 -- 2      |
	 |    |      . --> z
	 |    |    
	 |    |  SYMMETRICAL
	 |    |    
	 0 -- 3
	      |
*/

struct afterBaseInfo
{
	std::vector<pilerTopIndexor> pilers_top;
	std::vector<pilerTopIndexor> pilers_bot;
	
	float height;
	glm::vec3 corners[4];
	// For only corners 0 and 1
	unsigned int low_corners_indexs[2];
	unsigned int top_corners_indexs[2];
};


struct pilerBuildingInfos
{
	ElementBuffer         & _indices; 
	ArrayBuffer<glm::vec3>& _vertexs;
	ArrayBuffer<glm::vec4>& _colors;

	glm::vec3    & _top;
	glm::vec3    & _bottom;
	glm::vec3    & _center;
	glm::vec3    & _floorcolor;
	glm::vec4    & _cathcolor;
	glm::vec4    & _cathroofcolor;
	afterBaseInfo& _afterbase_infos;

	std::unique_ptr<PlanarSymmetry<glm::vec4>>& _symmetry;
};


struct pilerTriangleIndices
{
	unsigned int top_front;
	unsigned int top_back;
	unsigned int low_front;
	unsigned int low_back;
};


enum trianglePilerIndex {TRIPILER_NORTH_TOP=0,
						 TRIPILER_SOUTH_TOP,
						 TRIPILER_SOUTH_BOT,
						 TRIPILER_NORTH_BOT,
					TRPIILER_INDICES_COUNT};

enum pilerRelationType {SYMMPILER=0, ASYMPILER,  PILERSYM_COUNT};

namespace pilerConnection
{
	enum Relation {NO_TYPE=0,
				   TRIANGULAR,
				   FLAT,
				   SQUARE_FUSION,
				   END,
				   LATERAL_FUSION};
};

// Basic Interface
template<pilerRelationType SYMMETRY>
class PilerConnectorInterface {};

/*   === SPECIALIZATION OF BASIC INTERFACE ===  */

	template<>
	class PilerConnectorInterface<SYMMPILER>
	{
	public:

		virtual unsigned int arity() const =0;
		virtual pilerConnection::Relation relation() const=0;
		roofLinkor connect(const std::vector<unsigned int>& pilers, float height_base, float height_top, bool north_face_visible, bool south_face_visible)
		{
			if(pilers.size() != this->arity())
			{
				err("PilerConnectorInterface<SYMMPILER>::connect(): wrong size given: pilers.size()=" + std::to_string(pilers.size()) + " and arity=" + std::to_string(this->arity()));
			}
			
			return this->connect_goodsize(pilers, height_base, height_top, north_face_visible, south_face_visible);
		}

	protected:
		virtual roofLinkor connect_goodsize(const std::vector<unsigned int>& pilers, float height_base, float height_top, bool north_face_visible, bool south_face_visible) =0;

		PilerConnectorInterface() {}
	};

	template<>
	class PilerConnectorInterface<ASYMPILER>
	{
	public:

		virtual unsigned int arity() const =0;
		virtual pilerConnection::Relation relation() const=0;
		void connect(const std::vector<unsigned int>& pilers)
		{
			if(pilers.size() != this->arity())
			{
				err("PilerConnectorInterface<ASYMPILER>::connect(): wrong size given: pilers.size()=" + std::to_string(pilers.size()) + " and arity=" + std::to_string(this->arity()));
			}
			
			this->connect_goodsize(pilers);
		}

	protected:
		virtual void connect_goodsize(const std::vector<unsigned int>& pilers) =0;
		PilerConnectorInterface() {}
	};

/*   =========================================  */


// Arity Interface
template<pilerRelationType SYMMETRY, unsigned int ARITY>
class PilerConnectorArity : public PilerConnectorInterface<SYMMETRY>
{
public:
	unsigned int arity() const override {return ARITY;}

protected:
	PilerConnectorArity(const pilerBuildingInfos& infos) : PilerConnectorInterface<SYMMETRY>(),
	_indices(infos._indices),
	_vertexs(infos._vertexs),
	_colors(infos._colors),
	_top(infos._top),
	_bottom(infos._bottom),
	_center(infos._center),
	_floorcolor(infos._floorcolor),
	_cathcolor(infos._cathcolor),
	_cathroofcolor(infos._cathroofcolor),
	_afterbase_infos(infos._afterbase_infos),
	_symmetry(infos._symmetry) {}

	ElementBuffer         & _indices; 
	ArrayBuffer<glm::vec3>& _vertexs;
	ArrayBuffer<glm::vec4>& _colors;

	glm::vec3    & _top;
	glm::vec3    & _bottom;
	glm::vec3    & _center;
	glm::vec3    & _floorcolor;
	glm::vec4    & _cathcolor;
	glm::vec4    & _cathroofcolor;
	afterBaseInfo& _afterbase_infos;

	std::unique_ptr<PlanarSymmetry<glm::vec4>>& _symmetry;
};



#endif //_PILER_CONNECTOR_INTERFACE_HPP_
