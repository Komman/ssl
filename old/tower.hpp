#ifndef _TOWER_HPP_
#define _TOWER_HPP_

#include "../motor/gutils.hpp"

#include "tower_interface.hpp"


class Tower : public TowerInterface
{
public:
	struct stageRelief
	{
		float heigth;
		float size;	
	};

public:
	Tower(Drawer* drawer,
		  const glm::vec3& center,
		  const glm::vec3& direction,
		  const std::vector<stageRelief>& reliefs,
		  const glm::vec4& base_color);

	virtual void set_build_achievement(float achievement) override;

	void rebuild(const glm::vec3& center,
				 const glm::vec3& direction,
				 const std::vector<stageRelief>& reliefs,
				 const glm::vec4& base_color);

	GETTER_BUILDER(glm::vec3, center)
	GETTER_BUILDER(glm::vec3, direction)
	GETTER_BUILDER(glm::vec4, base_color)
	GETTER_BUILDER(std::vector<stageRelief>, reliefs)

	void build_roof();

	virtual Drawer* get_drawer() override;
	virtual bool    multisample_draw() const override;
	virtual bool    bloom_draw() const override;

protected:

	void build_stage(const glm::vec3& center,
					 const glm::vec3& direction,
					 const glm::vec4& color,
					 unsigned int relief_id);

	const std::vector<glm::vec3>&    get_points_save()  const;
	const std::vector<unsigned int>& get_indices_save() const;

	virtual void  finalize_roof(const std::vector<int>& top_indices);
	virtual float roof_start_time() const;
	virtual void  set_roof_acheivement(float achievement);

private:

	void build();

	void set_stage_achievement_linear_heigth(int stage, float stage_achievement, unsigned int prev_top[4]);
	void set_stage_achievement_linear_keep_old_size(int stage, float stage_achievement,
														   unsigned int prev_top[4],
														   unsigned int prevprev_top[4]);
	void set_stage_achievement_from_old_size_to_new(int stage, float stage_achievement,
													   unsigned int prev_top[4],
													   unsigned int prevprev_top[4]);
	void set_stage_achievement(int stage, float stage_achievement);
	float find_max_relief_size(const std::vector<stageRelief>& reliefs);
	
private:

	Drawer* _drawer;

	glm::vec3 _center;
	glm::vec3 _direction;
	glm::vec4 _base_color;

	std::vector<stageRelief> _reliefs;
	float  _max_relief_size;

	std::vector<glm::vec3>    _points_save;
	std::vector<unsigned int> _indices_save;
	std::vector<unsigned int> _max_stage_vertex_size;
	std::vector<unsigned int> _max_stage_index;

	bool  _achievement_finished;

	std::vector<int> _top_indices;
};

#endif //_TOWER_HPP_
