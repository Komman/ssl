#ifndef _L_BASE_BUILDOR_HPP_
#define _L_BASE_BUILDOR_HPP_

#include "../motor/planar_symmetry.hpp"

#include <list>

/*
      L:

    p1
    |             ^
    |           x |
    |             .--> z
    | 
    | 
    |
    p2----p3

*/

struct baseLparams
{
	float wall_height;
	glm::vec4 wall_color;
};

struct singleLpilerParam
{
	int groupeid;
	float size;
	glm::vec4 color;
};

struct generatedPilers
{
	glm::vec2 inter_pilers_minmax;
	float height;

	float size;
	glm::vec4 color;
};

struct globalLpilers
{
	globalLpilers(const generatedPilers& gen, int groupeid);

	glm::vec2 inter_pilers_minmax;
	float height;

	singleLpilerParam single;
};

struct pilerTopIndexor
{
	int groupeid;

	unsigned int north_east;
	unsigned int north_west;
	unsigned int south_west;
	unsigned int south_east;

	bool operator==(const pilerTopIndexor& p) const
	{return this->north_east == p.north_east
	     && this->north_west == p.north_west
	     && this->south_west == p.south_west
	     && this->south_east == p.south_east;}

	glm::vec3 center(const ArrayBuffer<glm::vec3>& pos)
	{
		return (pos[north_east] + pos[north_west] + pos[south_west] +pos[south_east])/4.0f;
	}
};


struct indexedLBase
{
	std::vector<pilerTopIndexor> top_pilers;
	std::vector<pilerTopIndexor> bot_pilers;
};



class LBaseBuildor
{
public:

	LBaseBuildor(const glm::vec3 points[3],
		       PlanarSymmetry<glm::vec4>& symmetry,
		       const baseLparams& params);

	void generate_pilers(const generatedPilers& genpilers_args);

	// Returns the x-increasing ordered vector of pilers top indices
	indexedLBase finish_base();

protected:

	struct pointParams
	{
		pointParams(const glm::vec3& coord, const singleLpilerParam& piler);
		void occupy(const singleLpilerParam& piler);

		glm::vec3 point;
		bool      is_piler;

		singleLpilerParam single;
	};

	static constexpr pilerTopIndexor NOT_TOP_INDEXOR = {-1,0,0,0,0};

	struct pilerCorner
	{
		glm::vec3 point;
		glm::vec4 color;
	};

	using pilerTopBotIndexor = std::pair<pilerTopIndexor, pilerTopIndexor>;

private:

	void occupy_empty_places(const globalLpilers& pilers_args);
	pilerTopBotIndexor build_local_Xpiler(const std::list<pointParams>::iterator& piler_it);
	pilerTopBotIndexor build_directed_piler(const std::vector<pilerCorner>& places, int groupeid, bool link_last);

private:
	baseLparams _params;
	singleLpilerParam _default_piler;
	int _groupeid_current;

	PlanarSymmetry<glm::vec4>& _symmetry;

	std::list<pointParams> _xpilers;
	std::list<pointParams> _zpilers;
};

#endif //_L_BASE_BUILDOR_HPP_
