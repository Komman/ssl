#ifndef _PILER_WALLET_HPP_
#define _PILER_WALLET_HPP_

#include "piler_connectors.hpp"
#include <vector>


using namespace ssl;

class PilerWallet
{
public:
	struct pilersSpot
	{
		std::vector<unsigned int> pilers;
		int groupeid;

		void print() const;
	};

	struct buildingSpots : public std::vector<pilersSpot>
	{
		void print() const;
	};

public:
	PilerWallet(const afterBaseInfo& infos, 
				unsigned int first,
				unsigned int last);

	// Returns pilersSpot of size at minimum arity
	buildingSpots get_spots_symmetrical(unsigned int arity);
	buildingSpots get_spots_asymmetrical(unsigned int arity);
	roofLinkor connect(PilerConnectorInterface<SYMMPILER>& connector,
					   const std::vector<unsigned int>& pilers,
					   float height_base,
					   float height_top,
					   bool north_face_visible,
					   bool south_face_visible);
	void connect(PilerConnectorInterface<ASYMPILER>& connector,
			     const std::vector<unsigned int>& pilers);

	void print() const;

protected:

	struct pilerSet
	{
		int groupeid;
		// Sorted
		std::vector<unsigned int> pilers;

		void print() const;
	};

	struct pilerState
	{
		int   groupeid;
		bool  occuped;
		float max_height;
	};

private:


	const afterBaseInfo&    _infos;
	std::vector<pilerSet>   _groups;
	std::vector<pilerState> _piler_states;

	unsigned int _first;
	unsigned int _last;
};

#endif //_PILER_WALLET_HPP_
