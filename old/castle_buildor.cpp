#include "castle_buildor.hpp"
#include "cathedral.hpp"

using namespace std;
using namespace glm;

CastleBuildor::CastleBuildor(const castleBuildorParameters& params)
	: CastleStructure(*(params.shared_ressources), *(params.ground), *(params.slightor), params.structure_params, *(params.drawer_controller)),
	  _builing_parameters(params)
{
	vec3 donjon_color = vec3(0.5,0.3,0.2);

	auto donjon_vert = std::make_unique<Donjon>(this->shader_generator(),
												_ground,
											    _architecture,
											    this->get_enclosure_indices(),
											    this->structure_parameters().center,
											    donjon_color,
											    50.0,
											    15,
											    *params.drawer_controller);
	

	auto cathedral_vert = std::make_unique<Cathedral>(this->shader_generator(),
													  donjon_vert->get_building_top()- vvv::x()*7.0f,
													  donjon_vert->get_building_bottom()+ vvv::x()*7.0f,
													  cathedralParams(),
													  donjon_color,
													  timeAndLast(Donjon::END_ACHIEVEMENT, 1.0f),
													  *params.drawer_controller);
	CastleEdge donjon_to_cathedral;

	vec3 donjon_center = (donjon_vert->get_building_top() + donjon_vert->get_building_bottom())/2.0f;

	CastleVertex donjon(0, donjon_center, 2.0, 4.0);
	donjon.add_drawable_object(std::move(donjon_vert));
	donjon.add_drawable_object(std::move(cathedral_vert));

	_architecture.add_vertex(std::move(donjon));
}	

castleBuildorParameters CastleBuildor::default_buildings_parameters(SharedRessources& shared_ressources,
																	const BasicGround& ground,
																	StaticLigthor& slightor,
																	DrawerPackController& drawer_controller)
{
	return {
		&shared_ressources,
		&ground,
		&slightor,
		CastleStructure::default_parameters(ground),
		&drawer_controller
	};
}
