#include "cathedral.hpp"

#include <map>

#include "../motor/gutils.hpp"

#include "piler_wallet.hpp"


using namespace std;
using namespace glm;
using namespace utils;


void Cathedral::link_triangle_roofs(const glm::uvec2& front1, const glm::uvec2& front2)
{
	_symmetry->link_rectangle(front1.y, front1.x, front2.x, front2.y);
}
/*
	bipart2 must be at the right of bipart1 for a front culling 
*/
void Cathedral::link_bipartite(const std::vector<unsigned int>& bipart1, const std::vector<unsigned int>& bipart2)
{
	if(bipart1.size()*bipart2.size() <= 1)
	{
		err("Cathedral::link_bipartite(): too few roof indices");
	}

	int i1 = 0;
	int i2 = 0;

	while(!(i1 == (int)bipart1.size()-1 && i2 == (int)bipart2.size()-1))
	{
		float q1 = (float)(i1+1) / (float)(bipart1.size());
		float q2 = (float)(i2+1) / (float)(bipart2.size());

		if(q1 < q2)
		{
			if(i1+1 == (int)bipart1.size())
			{
				err("The Cathedral::link_bipartite-impossible 1 happened here: " + SSL_FILE_AND_LINE);
			}

			_symmetry->link_triangle(bipart1[i1+1], bipart1[i1], bipart2[i2]);

			i1++;
		}
		else
		{
			if(i2+1 == (int)bipart2.size())
			{
				err("The Cathedral::link_bipartite-impossible 2 happened here: " + SSL_FILE_AND_LINE);
			}

			_symmetry->link_triangle(bipart1[i1], bipart2[i2], bipart2[i2+1]);

			i2++;
		}
	}
}

std::vector<unsigned int> Cathedral::link_discontinuous(const std::vector<unsigned int>& bipartend, float x)
{
	std::vector<unsigned int> ret;

	float oldx = 0.0f;
	if(bipartend.size() > 0)
	{
		oldx = _vertexs[bipartend[0]].x;
	}

	
	for(int i = 0; i<(int)bipartend.size(); i++)
	{
		const auto& index = bipartend[i];
		vec3 new_coord = _vertexs[index];
		new_coord.x = x;

		ret.push_back(_symmetry->place_symmetrical(new_coord, _colors[index]/1.4f));
	
		if(i > 0)
		{
			if(x > oldx)
			{
				_symmetry->link_rectangle(bipartend[i], ret[i], ret[i-1], bipartend[i-1]);
			}
			else
			{
				_symmetry->link_rectangle(bipartend[i], bipartend[i-1], ret[i-1], ret[i]);
			}
		}
	}

	return ret;
}



void Cathedral::build_upbase()
{	
	pilerBuildingInfos pilers_infos = {
		_indices, 
		_vertexs,
		_colors,
		_top,
		_bottom,
		_center,
		_floorcolor,
		_cathcolor,
		_cathroofcolor,
		_afterbase_infos,
		_symmetry
	};


	auto& infos  = _afterbase_infos;
	auto& pilers = infos.pilers_top;

	if(pilers.size() <= 1)
	{
		return;
	}

	std::vector<std::vector<unsigned int>> tripil_indices;
	std::vector<std::vector<unsigned int>> tri_front_indices;
	float triheight_bonus = utils::random_float(infos.height/5.0f, infos.height/2.0f);
	float triheight = triheight_bonus;
	float baseH = infos.corners[0].y;

	// for(int i=0; i<(int)pilers.size();i++)
	// {
	// 	this->grow_piler(pilers[i], triheight);
	// }

	TriangularPilerLink triformer(pilers_infos);

	unsigned int roof_i;
	int first_id = pilers[0].groupeid;
	for(roof_i=0; roof_i<pilers.size(); roof_i++)
	{
		if(pilers[roof_i].groupeid != first_id)
		{
			break;
		}
		if(k_chance_on_n(2, 3))
		{
			triheight+=triheight_bonus;
		}
		roofLinkor trilink;

		if(roof_i == 0)
		{
			trilink = triformer.connect({roof_i}, baseH, triheight, true, false);
		}
		else
		{
			trilink = triformer.connect({roof_i}, baseH, triheight, false, false);
		} 

		tri_front_indices.push_back(trilink.north);	
	}

	this->create_interplateform(_afterbase_infos.corners[0], _afterbase_infos.corners[1], 0.8f, _cathcolor/2.0f);

	for(int i=0; i<(int)tri_front_indices.size()-1; i++)
	{
		this->link_bipartite(tri_front_indices[i+1], tri_front_indices[i]);
	}

	if(roof_i != pilers.size())
	{
		const auto& lastri = tri_front_indices[tri_front_indices.size()-1];
		this->link_discontinuous(lastri, _vertexs[lastri[0]].x + 10.0f);
	}

	if((int)pilers.size() - 2 - (int)roof_i > 0)
	{
		this->random_cathedral_word(roof_i, (int)pilers.size() - 2);
	}

	// triformer.connect({(unsigned int)((int)pilers.size()-1)}, baseH, triheight, false, true);
}


void Cathedral::grow_interval(unsigned int first, unsigned int last, float height)
{
	for(unsigned int i=first; i<=last; i++)
	{
		this->grow_piler(_afterbase_infos.pilers_top[i], height);
	}
}

// Returns the available plateformes created (x: bottom index, y: top index)
std::vector<glm::uvec2> Cathedral::random_cathedral_word(unsigned int first_piler, unsigned int last_piler)
{
	pilerBuildingInfos pilers_infos = {
		_indices, 
		_vertexs,
		_colors,
		_top,
		_bottom,
		_center,
		_floorcolor,
		_cathcolor,
		_cathroofcolor,
		_afterbase_infos,
		_symmetry
	};

	auto& infos  = 	_afterbase_infos;
	auto& pilers = infos.pilers_top;
	std::vector<glm::uvec2>  ret;

	std::vector<std::unique_ptr<PilerConnectorInterface<SYMMPILER>>> symmetrical_relations;
	std::vector<std::unique_ptr<PilerConnectorInterface<ASYMPILER>>> asymmetrical_relations;
	
	symmetrical_relations.push_back(std::make_unique<TriangularPilerLink>  (pilers_infos));
	symmetrical_relations.push_back(std::make_unique<SquareFusionPilerLink>(pilers_infos));
	symmetrical_relations.push_back(std::make_unique<FlatPilerLink>        (pilers_infos));
	
	asymmetrical_relations.push_back(std::make_unique<EndPilerLink          >(pilers_infos));
	asymmetrical_relations.push_back(std::make_unique<LateralFusionPilerLink>(pilers_infos));

	PilerWallet wallet(infos, first_piler, last_piler);
	std::vector<std::unique_ptr<roofLinkor>> linking;
	for(unsigned int i=0; i<pilers.size(); i++)
	{
		linking.push_back(NULL);
	}

	float topH = _afterbase_infos.corners[0].y;
	std::vector<roofLinkor> roof_to_link;

	for(unsigned int i=0; i<pilers.size()*2/3; i++)
	{
		auto& rel = symmetrical_relations[ rand() % symmetrical_relations.size() ];

		PilerWallet::buildingSpots spots = wallet.get_spots_symmetrical(rel->arity());

		// cout<<"========================="<<TERM::ORANGE<<endl;
		// wallet.print();
		// cout<<TERM::BLUE<<"ARITY = "<<rel->arity()<<endl<<TERM::GREEN;
		// spots.print();
		// cout<<TERM::NOCOL<<endl;

		if(spots.size() == 0)
		{
			break;
		}

		auto rdspot = spots[rand() % spots.size()];
		vector<unsigned int> choosen_pilers = utils::extract_random_elements(rdspot.pilers, rel->arity());   
		
		std::sort(choosen_pilers.begin(), choosen_pilers.end());

		float h1 = utils::random_float(5.0f, 20.0f);
		float h2 = utils::random_float(5.0f, 40.0f);

		roof_to_link.push_back(wallet.connect(*rel, choosen_pilers, topH+4.0f*choosen_pilers[0], 5.0f, true, true)); 

		// TODO
		this->set_roof_color(roof_to_link[roof_to_link.size()-1].south, {_cathcolor});
		auto& rf = roof_to_link[roof_to_link.size()-1].south;
		this->link_discontinuous(rf, _vertexs[rf[rf.size()-1]].x+5.0f);
	}

	return ret;
}
