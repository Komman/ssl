
void vertex(in vec2 position, in vec3 color,
			out vec3 frag_color)
{
    gl_Position  = vec4(position.x, position.y, 0.0, 1.0);;
    frag_color = color;
}

void fragment(in vec3 frag_color, out vec3 color)
{
	color = frag_color;
}


void vertex_inst(in vec2 position, in vec3 color, in::instance vec2 offset,
			out vec3 frag_color)
{
    gl_Position  = vec4(position.x + offset.x, position.y + offset.y, 0.0, 1.0);;
    frag_color = color;
}