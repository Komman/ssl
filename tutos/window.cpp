#include <iostream>

#include "../ssl/ssl.hpp"

using namespace std;
using namespace glm;
using namespace ssl;

int main()
{
	ssl::init("bouh");

	while(!window::should_close() && !keyboard::key_pressed(SSL_KEY(F4)))
	{
		window::clear();
		
		window::flip();
	}	

	ssl::close();

	return 0;
}
