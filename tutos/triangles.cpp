#include <iostream>

#include "../ssl/ssl.hpp"

using namespace std;
using namespace ssl;
using namespace glm;

int main()
{
	ssl::init("bouh");

	ShaderGenerator generator("./simple_shaders.glsl");
	Drawer shader(generator, "vertex", "fragment");
	generator.free();

	ArrayBuffer<vec2> vertexs({
		vec2(0.0 , 0.0),
		vec2(1.0 , 0.0),
		vec2(0.0 , 1.0),

		vec2(-1.0,-1.0),
		vec2(0.0 ,-1.0),
		vec2(0.0 , 0.0)
	}, STATIC_DRAW);
	ArrayBuffer<vec3> colors({
		ssl::RED,
		vec3(0.9,0.2,0.8),
		vec3(0.1,0.8,0.3),

		vec3(0.2,0.2,0.6),
		vec3(0.5,0.2,0.1),
		vec3(0.1,0.5,0.3)
	}, STATIC_DRAW);

	ArrayBuffer<vec2> vertexs2({
		vec2(-1.0,-1.0)/2.0f,
		vec2(1.0 , 0.0)/2.0f,
		vec2(0.0 , 1.0)/2.0f
	}, STATIC_DRAW);
	ArrayBuffer<vec3> colors2({
		ssl::BLUE,
		ssl::GREEN,
		ssl::GREEN
	}, STATIC_DRAW);

	while(!window::should_close() && !keyboard::key_pressed(SSL_KEY(F4)))
	{
		window::clear();
		
		shader.draw(vertexs, colors);
		shader.draw(vertexs2, colors2);

		window::flip();
	}	

	ssl::close();

	return 0;
}
